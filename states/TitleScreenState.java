package states;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;

/**
 *
 *
 * @author Matthew Ludwig
 */
public class TitleScreenState extends SimpleAppState {

    public TitleScreenState() {
        super("Title Screen", true);
    }

    @Override
    public void attachToAbsoluteNode(Node node, AssetManager am) {
        node.attachChild(Utilities.getJMELoader().getPicture("Background", "Interface/General/TitleScreen.png", 0, 0, PictureLayer.BACKGROUND));
        node.attachChild(Utilities.getJMELoader().getCenteredText(ColorRGBA.Cyan, "PRESS " + Main.getUserInput().getKeyName("P1 Pause"), 300, 3));
    }

    @Override
    public void attachToPausableNode(Node node, AssetManager am) {
    }

    @Override
    public String getMusicPath() {
        return "Music/TitleMusic.ogg";
    }
    
    @Override
    public void update(float tpf) {
        super.update(tpf);
        if (Main.getUserInput().isKeyClicked("Pause")) {
            this.playSelectSound();
            this.switchStates(new MainMenuState());
        }
    }
}
