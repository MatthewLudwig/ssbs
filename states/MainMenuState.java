package states;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;
import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import java.util.LinkedList;
import java.util.List;
import states.game.strife.StrifeCharacterSelectState;

/**
 *
 *
 * @author Matthew Ludwig
 */
public class MainMenuState extends SimpleAppState {

    private List<Picture> buttonList;
    private boolean clearMode;
    private Node clearBox;

    public MainMenuState() {
        super("Main Menu", true);
        this.buttonList = new LinkedList<Picture>();
        this.clearMode = false;
        this.clearBox = new Node("Clear Box");
    }

    @Override
    public void attachToAbsoluteNode(Node node, AssetManager am) {
        node.attachChild(Utilities.getJMELoader().getPicture("Background", "Interface/MainMenu/Background.png", 0, 0, PictureLayer.BACKGROUND));
        this.buttonList.add(Utilities.getJMELoader().getPicture("Strife Button", "Interface/MainMenu/StrifeButton.png", 15, 355, PictureLayer.GUI));
        this.buttonList.add(Utilities.getJMELoader().getPicture("Solo Button", "Interface/MainMenu/SoloButton.png", 45, 245, PictureLayer.GUI));
        this.buttonList.add(Utilities.getJMELoader().getPicture("Credits Button", "Interface/MainMenu/CreditsButton.png", 75, 135, PictureLayer.GUI));
        this.buttonList.add(Utilities.getJMELoader().getPicture("Options Button", "Interface/MainMenu/OptionsButton.png", 105, 25, PictureLayer.GUI));
        this.buttonList.add(Utilities.getJMELoader().getPicture("Trophy Button", "Interface/MainMenu/TrophyButton.png", 415, 355, PictureLayer.GUI));
        this.buttonList.add(Utilities.getJMELoader().getPicture("Clear Button", "Interface/MainMenu/ClearButton.png", 445, 245, PictureLayer.GUI));
        this.buttonList.add(Utilities.getJMELoader().getPicture("Full Screen Button", "Interface/MainMenu/FullScreenButton.png", 475, 135, PictureLayer.GUI));

        for (Picture pic : this.buttonList) {
            node.attachChild(pic);
        }
        
        this.clearBox.attachChild(Utilities.getJMELoader().getPicture("Background", "Interface/AreYouSure.png", 0, 0, PictureLayer.GUI));
        this.clearBox.attachChild(Utilities.getJMELoader().getPicture("Yes Box", "Interface/Yes.png", 17, 4, PictureLayer.GUI));
        this.clearBox.attachChild(Utilities.getJMELoader().getPicture("No Box", "Interface/No.png", 108, 4, PictureLayer.GUI));
        this.clearBox.setLocalTranslation(220, 202, 0);
    }

    @Override
    public void attachToPausableNode(Node node, AssetManager am) {
    }

    @Override
    public String getMusicPath() {
        return "Music/MainMenuMusic.ogg";
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        if (Main.getUserInput().isLeftButtonClicked()) {
            if (this.clearMode) {
                if (Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition.subtract(220, 202), (Picture) this.clearBox.getChild("Yes Box"))) {
                    this.playSelectSound();
                    this.clearMode = false;
                    this.absoluteNode.detachChild(this.clearBox);
                    Main.resetSaveData();
                } else if (Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition.subtract(220, 202), (Picture) this.clearBox.getChild("No Box"))) {
                    this.playSelectSound();
                    this.clearMode = false;
                    this.absoluteNode.detachChild(this.clearBox);
                }
            } else {
                for (Picture button : this.buttonList) {
                    if (Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition, button)) {
                        this.buttonClicked(button.getName());
                    }
                }
            }
        }
    }

    public void buttonClicked(String name) {
        this.playSelectSound();
        
        if (name.equals("Strife Button")) {
            this.switchStates(new StrifeCharacterSelectState());
        } else if (name.equals("Solo Button")) {
            this.switchStates(new SoloMenuState());
        } else if (name.equals("Credits Button")) {
            this.switchStates(new CreditsState());
        } else if (name.equals("Options Button")) {
            this.switchStates(new OptionsMenuState());
        } else if (name.equals("Trophy Button")) {
        } else if (name.equals("Clear Button")) {
            this.clearMode = true;
            this.absoluteNode.attachChild(this.clearBox);
        } else if (name.equals("Full Screen Button")) {
            Main.getGameSettings().setFullscreen(!Main.getGameSettings().isFullscreen());
        } else {
            System.err.println("Incorrect buton name passed into function!  Name:  " + name);
        }
    }
}
