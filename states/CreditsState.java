package states;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import java.util.LinkedList;
import java.util.List;

/**
 *
 *
 * @author Matthew Ludwig
 */
public class CreditsState extends SimpleAppState {
    private Node textNode;
    private int scrollSpeed = 25;

    public CreditsState() {
        super("Title Screen", true);
        this.textNode = new Node("Credits Text");
    }

    @Override
    public void attachToAbsoluteNode(Node node, AssetManager am) {
        node.attachChild(Utilities.getJMELoader().getPicture("Background", "Interface/MatchSetup/chooseStage_back.png", 0, 0, PictureLayer.BACKGROUND));

        int count = 0;

        for (String credit : setUpCredits()) {
            this.textNode.attachChild(Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.Blue, credit, 320, count * -25));

            count++;
        }

        node.attachChild(this.textNode);
    }

    public static List<String> setUpCredits() {
        List<String> credits = new LinkedList<String>();

        credits.add("Design and Concept: Snuffysam");
        credits.add("\n");
        credits.add("Coding: Snuffysam, Chugwig");
        credits.add("\n");
        credits.add("Original Songs, characters, and ideas owned by Nintendo, Capcom, Sega, Namco,");
        credits.add("Gottlieb, Phillips, Valve, Fuji TV, Shueisha, Kodansha, Mojang, Office Create,");
        credits.add("Disney, Konami, 5th Cell, SquareEnix, The BBC, and Warner Bros.");
        credits.add("Songs re-made by Snuffysam.");
        credits.add("\n");
        credits.add("Sprites ripped/created by:");
        credits.add("OmegaKyogre");
        credits.add("Drshnapa");
        credits.add("Dimentio44");
        credits.add("Rock Candy");
        credits.add("Sploder");
        credits.add("CheDDar-X");
        credits.add("GregarLink10");
        credits.add("Yawackhary");
        credits.add("Xenowhirl");
        credits.add("ICEKnight");
        credits.add("Divine Insect");
        credits.add("Neweegee");
        credits.add("Verion");
        credits.add("TheEchidna");
        credits.add("Deathbringer");
        credits.add("Smithygcn");
        credits.add("Random Talking Bush");
        credits.add("Pite");
        credits.add("Pyroth");
        credits.add("Lexou");
        credits.add("RedBlueYellow");
        credits.add("MissingNo");
        credits.add("Keni Imatake");
        credits.add("Octohunter");
        credits.add("NGamer01");
        credits.add("Miles07");
        credits.add("ChaosMiles07");
        credits.add("Rogultgot");
        credits.add("Bacon");
        credits.add("Deathbringer");
        credits.add("B. Hopkins");
        credits.add("NightmareZero187");
        credits.add("NO Body");
        credits.add("Ragey");
        credits.add("Jesus lizard");
        credits.add("SemiJuggalo");
        credits.add("Frario");
        credits.add("Rydercloud");
        credits.add("Boo Mansion");
        credits.add("Davias");
        credits.add("Tonberry2K");
        credits.add("\n");
        credits.add("Sprites edited by:");
        credits.add("Snuffysam");
        credits.add("\n");
        credits.add("Backgrounds edited by:");
        credits.add("Snuffysam");
        credits.add("\n");
        credits.add("Game is based off of Super Smash Bros, copyright Nintendo.");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("\n");
        credits.add("Press " + Main.getUserInput().getKeyName("Pause") + " to exit.");

        return credits;
    }

    @Override
    public void attachToPausableNode(Node node, AssetManager am) {
    }

    @Override
    public String getMusicPath() {
        return "Music/TitleMusic.ogg";
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        if (this.textNode.getLocalTranslation().y < 2250) {
            this.textNode.move(0, Utilities.lockedTPF * this.scrollSpeed, 0);
        }

//        if(name.equals("Speed Up"))
//        {
//            if(isPressed)
//            {
//                this.scrollSpeed = 100;
//            }
//            else
//            {
//                this.scrollSpeed = 25;
//            }
//        }
//        
//        if(name.equals("Super Speed Up"))
//        {
//            if(isPressed)
//            {
//                this.scrollSpeed = 1000;
//            }
//            else
//            {
//                this.scrollSpeed = 25;
//            }
//        }

        if (Main.getUserInput().isKeyClicked("Pause")) {
            this.playSelectSound();
            this.switchStates(new MainMenuState());
        }
    }
}
