package states;

import com.jme3.scene.Node;
import com.jme3.ui.Picture;
import engine.PictureLayer;
import engine.utility.Utilities;

/**
 *
 * @author Matthew
 */
public class Slider {
    private Node displayNode;
    private Picture bar;
    private Picture slider;
    
    private float offset;
    
    public Slider(float xPos, float yPos) {
        this(xPos, yPos, .50f);
    }
    
    public Slider(float xPos, float yPos, float initialOffset) {
        this.displayNode = new Node("Slider Node");
        this.displayNode.setLocalTranslation(xPos, yPos, 0);
        
        this.bar = Utilities.getJMELoader().getPicture("Bar", "Interface/Options/volumeBar.png", 0, 0, PictureLayer.GUI);
        this.slider = Utilities.getJMELoader().getPicture("Slider", "Interface/Options/volumeSlider.png", 0, 0, PictureLayer.GUI);
        
        this.displayNode.attachChild(this.bar);
        this.displayNode.attachChild(this.slider);
        
        this.setOffsetPercent(initialOffset);
    }
    
    public final Node getNode() {
        return this.displayNode;
    }
    
    public final void setSliderPosition(float xPosition) {
        this.offset = xPosition - this.displayNode.getLocalTranslation().x - 7;
        this.setOffsetPercent(this.getOffsetPercent());
    }
    
    public final void setOffsetPercent(float percent) {
        this.offset = (320 * percent) - 7;
        this.slider.setPosition(this.offset, 0);
    }
    
    public final float getOffsetPercent() {
        float offsetPercent = (this.offset + 7) / 320;
        
        if (Utilities.getGeneralUtility().compareFloats(offsetPercent, 0, .02f)) {
            return 0;
        } else if (Utilities.getGeneralUtility().compareFloats(offsetPercent, 1, .02f)) {
            return 1;
        } else {
            return Math.max(Math.min(offsetPercent, 1), 0);
        }
    }
}
