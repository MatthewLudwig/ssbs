package states.game.targetTest;

import states.game.CharacterSelectState;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;

import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import states.SimpleAppState;
import states.SoloMenuState;
import states.game.GameRules;
import states.game.stages.TargetTest;
import engine.ssbs.CharacterList;

/**
 *
 *
 * @author Matthew Ludwig
 */
public class TargetTestCharacterSelectState extends CharacterSelectState {
    public TargetTestCharacterSelectState(PlayableCharacter... passedInCharacters) {
        super(1, 1, new GameRules(), passedInCharacters);
        this.limitedNumberOfLives = 1;
        this.allowControllerChange = false;
    }
    
    @Override
    public void attachToAbsoluteNode(Node node, AssetManager am) {
        node.attachChild(Utilities.getJMELoader().getPicture("Background", "Interface/MatchSetup/choosePlayer_back.png", 0, 0, PictureLayer.BACKGROUND));
        node.attachChild(Utilities.getJMELoader().getText(ColorRGBA.White, "Target Test", 100, 440));
        node.attachChild(this.getCharacterPortraits());
        node.attachChild(this.getSelectionPortraits().move(200, 0, 0));
        
        BitmapText text = Utilities.getJMELoader().getText(ColorRGBA.White, "--:--:--", 350, 125);
        text.setName("Time");
        node.attachChild(text);
    }
    
    @Override
    public void update(float tpf) {
        super.update(tpf);
        
        BitmapText text = (BitmapText) this.absoluteNode.getChild("Time");
        
        if (this.characters[0].isRemoved() || this.characters[0].getMapping().getTargetTestTime() == 99999) {
            text.setText("--:--:--");
        } else {
            float time = this.characters[0].getMapping().getTargetTestTime();
            
            String hours = Utilities.getGeneralUtility().getNumberAsShortenedString(((int)time) / 3600);
            String minutes = Utilities.getGeneralUtility().getNumberAsShortenedString((((int)time) % 3600) / 60);
            String seconds = Utilities.getGeneralUtility().getNumberAsShortenedString(((int)time) % 60);
                
            text.setText(hours + ":" + minutes + ":" + seconds);
        }
    } 
    
    @Override
    public SimpleAppState getNextState(PlayableCharacter[] players) {
        return new TargetTestGameState(new TargetTest(Main.getCharacterList().getCharacter(players[0])), players);
    }
    
    public void customUpdateBackButton(float xPosition, float yPosition) {
        if (this.absoluteNode.getChild("Back Button") == null) {
            this.absoluteNode.attachChild(Utilities.getJMELoader().getPicture("Back Button", "Interface/General/backButton.png", xPosition, yPosition, PictureLayer.GUI));
        } else if (Main.getUserInput().isLeftButtonClicked() && Utilities.getPhysicsUtility().complex2DCollision(Main.getUserInput().getMousePosition(), (Picture) this.absoluteNode.getChild("Back Button"))) {
            Utilities.getCustomLoader().getAudioNode("/Common Sounds/back.ogg").playInstance();
            this.switchStates(new SoloMenuState());
        }   
    }
}
