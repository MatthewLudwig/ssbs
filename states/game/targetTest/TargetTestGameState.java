package states.game.targetTest;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.texture.Texture2D;
import com.jme3.ui.Picture;

import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.general.Target;

import java.util.LinkedList;
import java.util.List;

import states.game.GameRules;
import states.game.GameState;
import states.game.stages.Stage;

/**
 *
 * @author Matthew
 */
public class TargetTestGameState extends GameState {
    private static Texture2D[] failureAnimation;
    private static Texture2D[] successAnimation;
    private static Texture2D[] newRecord;
    private static Texture2D[] countdown;
    
    private List<Target> targetList;
    private Node targetIcons;
    private Picture status;
    private Picture status2;
    private boolean successOrFailure;
    private int animationFrame;
    private int animationFrame2;
    
    private float timer;
    private float updateLimiter;
    private boolean playStart;
    
    private boolean isRecord;
    
    static {
        failureAnimation = new Texture2D[35];
        successAnimation = new Texture2D[35];
        newRecord = new Texture2D[35];
        countdown = new Texture2D[14];
        
        for (int count = 0; count < failureAnimation.length; count++) {
            failureAnimation[count] = Utilities.getJMELoader().getTexture("/Interface/Target Test/failure_" + Utilities.getGeneralUtility().getNumberAsString(count) + ".png");
        }
        
        for (int count = 0; count < successAnimation.length; count++) {
            successAnimation[count] = Utilities.getJMELoader().getTexture("/Interface/Target Test/success_" + Utilities.getGeneralUtility().getNumberAsString(count) + ".png");
        }
        
        for (int count = 0; count < newRecord.length; count++) {
        	newRecord[count] = Utilities.getJMELoader().getTexture("/Interface/Target Test/newRecord_" + Utilities.getGeneralUtility().getNumberAsString(count) + ".png");
        }
        
        for (int count = 0; count < countdown.length; count++) {
            countdown[count] = Utilities.getJMELoader().getTexture("/Interface/SoloBattle/READYGO_" + Utilities.getGeneralUtility().getNumberAsString(count) + ".png");
        }
    }
    
    public TargetTestGameState(Stage stage, PlayableCharacter[] players) {
        super(stage, new GameRules(), players);
        this.minZoom = 150;
        
        this.targetList = new LinkedList<Target>();
        this.targetIcons = new Node("Target Icons");
        this.status2 = Utilities.getJMELoader().getPicture("Status2", "/Sprites/Miscellaneous/blankSprite.png", 0, 0, PictureLayer.SPECIALLAYER);
        this.status = Utilities.getJMELoader().getPicture("Status", "/Sprites/Miscellaneous/blankSprite.png", 0, 0, PictureLayer.SPECIALLAYER);
        this.successOrFailure = true;
        this.animationFrame = -1;
        this.animationFrame2 = -1;
        this.timer = 0;
        this.updateLimiter = 0;
        this.playStart = true;
        
        if (this.animationFrame2 == -1) {
        	Main.getUserInput().setAcceptingInputs(false);
        	this.animationFrame2 = 0;
        }
        this.isRecord = false;
    }

    @Override
    public void spawnEntity(PhysicsEntity object) {
        super.spawnEntity(object);

        if (object instanceof Target) {
            this.targetList.add((Target) object);
        }
    }
    
    @Override
    public void attachToAbsoluteNode(Node node, AssetManager am) {
        super.attachToAbsoluteNode(node, am);
        node.attachChild(this.targetIcons);
        this.targetIcons.setLocalTranslation(0, 459, 10);
        node.attachChild(this.status);
        node.attachChild(this.status2);
        this.status.setLocalTranslation(0, 0, 10);
        this.status2.setLocalTranslation(0, 0, 10);
    }
    
    @Override
    public void update(float tpf) {        
        if (this.animationFrame != -1) {
            if (this.updateLimiter >= .04f) {
                Utilities.getJMELoader().changeTexture(this.status, this.successOrFailure ? successAnimation[this.animationFrame] : failureAnimation[this.animationFrame]);
                if (this.successOrFailure && isRecord){
                	Utilities.getJMELoader().changeTexture(this.status2, newRecord[this.animationFrame]);
                }
                this.animationFrame += 1;

                if (this.animationFrame >= 35) {
                    Main.getUserInput().setAcceptingInputs(true);
                    this.switchStates(new TargetTestCharacterSelectState(this.players[0]));
                    return;
                }
                
                this.updateLimiter -= .04f;
            } else {
                this.updateLimiter += Utilities.lockedTPF;
            }
            
            Utilities.lockedTPF /= 2;
        } else {
            this.timer += Utilities.lockedTPF;
        }
        
        if (this.animationFrame2 != -1 && this.playStart) {
            if (this.updateLimiter >= .04f) {
                 Utilities.getJMELoader().changeTexture(this.status, countdown[this.animationFrame2]);
                 this.animationFrame2 += 1;

                 if (this.animationFrame2 >= 9) {
                     Main.getUserInput().setAcceptingInputs(true);
                 }
                 if (this.animationFrame2 >= 14) {
                     this.animationFrame2 = -1;
                     this.playStart = false;
                     Utilities.getJMELoader().changeTexture(this.status, "/Sprites/Miscellaneous/blankSprite.png");
                 }
                 
                 this.updateLimiter -= .04f;
             } else {
                 this.updateLimiter += Utilities.lockedTPF;
             }
             
             Utilities.lockedTPF /= 2;
         }
        
        super.update(tpf);
    }
    
    @Override
    public void checkForFinish() {
        if (this.animationFrame == -1) {
            if (this.players[0].isDead()) {
                Main.getUserInput().setAcceptingInputs(false);
                this.setFreezeCamera(true);
                this.world.setDisableDeath(true);
                this.successOrFailure = false;
                this.animationFrame = 0;
            } else {
                this.targetIcons.detachAllChildren();

                int count = 0;

                for (Target t : this.targetList) {
                    if (!t.isDead() && !t.isDying()) {
                        this.targetIcons.attachChild(Utilities.getJMELoader().getPicture("Target Icon", "/Interface/targetIcon.png", 5 + (count * 21), 0, PictureLayer.GUI));
                        count++;
                    }
                }
                
                String hours = Utilities.getGeneralUtility().getNumberAsShortenedString(((int)this.timer) / 3600);
                String minutes = Utilities.getGeneralUtility().getNumberAsShortenedString((((int)this.timer) % 3600) / 60);
                String seconds = Utilities.getGeneralUtility().getNumberAsShortenedString(((int)this.timer) % 60);
                
                this.targetIcons.attachChild(Utilities.getJMELoader().getText(Utilities.Impact16, ColorRGBA.White, hours + ":" + minutes + ":" + seconds, 5, -25));

                if (count == 0) {
                    Main.getUserInput().setAcceptingInputs(false);
                    this.setFreezeCamera(true);
                    this.world.setDisableDeath(true);
                    this.successOrFailure = true;
                    this.animationFrame = 0;
                    
                    if (this.timer < Main.getCharacterList().getCharacter(this.players[0]).getTargetTestTime())
                    	isRecord = true;
                    
                    Main.getCharacterList().getCharacter(this.players[0]).setTargetTestTime(Math.round(this.timer));
                }
            }
        }
    }

}
