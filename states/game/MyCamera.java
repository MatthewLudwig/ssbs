package states.game;

import com.jme3.math.Vector2f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.texture.Image;
import com.jme3.ui.Picture;
import engine.Main;
import engine.utility.Utilities;
import java.util.logging.Level;
import physics.BoundingBox;

/**
 *
 * @author Matthew
 */
public class MyCamera {
    private Node screenNode;
    private Node worldNode;
    private Vector2f currentPosition;
    private Vector2f targetPosition;
    private float currentScale;
    private float targetScale;
    private BoundingBox limits;
    
    public MyCamera(Node node) {
        this(node, new Vector2f(0, 0), 1f);
    }
    
    public MyCamera(Node node, Vector2f initialPosition, float initialScale) {
        this.screenNode = node;
        this.worldNode = new Node("World Node");
        this.currentPosition = initialPosition;
        this.targetPosition = initialPosition;
        this.currentScale = initialScale;
        this.targetScale = initialScale;
    }
    
    public Node getWorldNode() {
        return this.worldNode;
    }
    
    public Node getScreenNode() {
        return this.screenNode;
    }
    
    public Vector2f getTargetPosition(){
    	return targetPosition;
    }
    
    public float getTargetScale(){
    	return targetScale;
    }
    
    public Vector2f getCurrentPosition(){
    	return currentPosition;
    }
    
    public float getCurrentScale(){
    	return currentScale;
    }
    
    public void forceCameraPosition(float xPos, float yPos){
    	this.currentPosition.set(xPos, yPos);
    	this.targetPosition = currentPosition;
    	updateCameraState();
    }
    
    public void setTargetPosition(float xPos, float yPos) {
        this.targetPosition.set(xPos, yPos);
    }
    
    public void forceCameraDimensions(float xDim, float yDim) {
        this.currentScale = Math.max(xDim / 640, yDim / 480);
        this.targetScale = currentScale;
        updateCameraState();
    }
    
    public void setTargetDimensions(float xDim, float yDim) {
        this.targetScale = Math.max(xDim / 640, yDim / 480);
    }
    
    public void setCameraLimits(BoundingBox box) {
        if (this.limits == null || !this.limits.getDimensions().equals(box.getDimensions())) {
            float limitResolution = box.getDimensions().x / box.getDimensions().y;
            float cameraResolution = 640 / 480;

            if (limitResolution > cameraResolution) {
                Main.log(Level.WARNING, "Limit resolution is greater than camera resolution, possible problems with camera scaling will be encountered.", null);
            }
        }
        
        this.limits = box;
    }
    
    public void updateCameraState() {   
    	this.updateCameraState(false);
    }

    public void updateCameraState(boolean upsideDown) {
    	
        if (!Utilities.getGeneralUtility().compareFloats(this.currentPosition.lengthSquared(), this.targetPosition.lengthSquared(), .1f)) {
            this.currentPosition = this.currentPosition.add(this.targetPosition.subtract(this.currentPosition).mult(Utilities.lockedTPF * 5));
        }
        
        if (!Utilities.getGeneralUtility().compareFloats(this.currentScale, this.targetScale, .1f)) {
            this.currentScale = this.currentScale + ((this.targetScale - this.currentScale) * Utilities.lockedTPF * 5);
        }

        Vector2f cameraDimensions = new Vector2f(640 * this.currentScale, 480 * this.currentScale);
        BoundingBox bounds = new BoundingBox(this.currentPosition.subtract(cameraDimensions.divide(2)), cameraDimensions);
        
        if (this.limits != null) {
            if (bounds.getDimensions().x > this.limits.getDimensions().x || bounds.getDimensions().y > this.limits.getDimensions().y) {
                bounds.scaleBounds(Math.min(this.limits.getDimensions().x / bounds.getDimensions().x, this.limits.getDimensions().y / bounds.getDimensions().y));
            }
            
            bounds = this.limits.fitInsideBoundingBox(bounds, false);
        }
        BoundingBox otherBounds = new BoundingBox(bounds.getLowerLeft(), bounds.getDimensions());
        if (upsideDown){
        	Vector2f topRight = new Vector2f(Main.getGameState().getWorld().getStage().getSizeX(), Main.getGameState().getWorld().getStage().getSizeY());
        	otherBounds = new BoundingBox(topRight.subtract(bounds.getLowerLeft()).subtract(bounds.getDimensions()), bounds.getDimensions());
        }
        
        this.screenNode.detachAllChildren();
        
        Vector2f position = Vector2f.ZERO.clone();
        Vector2f dimensions = Vector2f.ZERO.clone();
        BoundingBox testBox = new BoundingBox(position, dimensions);
        
        for (Spatial s : this.worldNode.getChildren()) {
        	Spatial alwaysDraw = null;
            if (s instanceof Picture) {
                Picture p = (Picture) s;
                
                position.set(p.getLocalTranslation().x, p.getLocalTranslation().y);
                Image i = p.getMaterial().getTextureParam("Texture").getTextureValue().getImage();
                dimensions.set(i.getWidth(), i.getHeight());
            } else if (s instanceof Node) {
            	alwaysDraw = ((Node) s).getChild("AlwaysDraw");
                Picture p = (Picture) ((Node) s).getChild("Picture");

                if (p != null) {
                    position.set(s.getLocalTranslation().x + p.getLocalTranslation().x, s.getLocalTranslation().y + p.getLocalTranslation().y);
                    Image i = p.getMaterial().getTextureParam("Texture").getTextureValue().getImage();
                    dimensions.set(i.getWidth(), i.getHeight());
                } else {
                    position.set(s.getLocalTranslation().x, s.getLocalTranslation().y);
                    dimensions.set(s.getLocalScale().x, s.getLocalScale().y);
                }
            }
            
            testBox.updateBounds(position, dimensions);
            
            if (alwaysDraw != null || bounds.isInBounds(testBox) || testBox.isInBounds(bounds) || (upsideDown && (otherBounds.isInBounds(testBox) || testBox.isInBounds(otherBounds)))) {
                this.screenNode.attachChild(s.clone());
            }
        }
        
        float xOff = (640 / 2) - (bounds.getExactCenter().x * (640 / bounds.getDimensions().x));
        float yOff = (480 / 2) - (bounds.getExactCenter().y * (640 / bounds.getDimensions().x));
        
        this.screenNode.setLocalTranslation(xOff, yOff, 0);
        this.screenNode.setLocalScale(640 / bounds.getDimensions().x);
    }
}
