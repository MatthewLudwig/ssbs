package states.game.events;

import states.game.GameRules;
import states.game.SelectedCharacter;
import states.game.stages.*;
import engine.Main;
import entities.PlayableCharacter;
import entities.Team;
//import entities.characters.*;

public class Event1 extends EventGameState {
	
	public Event1() {
		super(getPlayers(), getRules(), getStage());
	}
	
	@Override
	public void checkForFinish() {
		if (this.animationFrame == -1){
			if (players[0] == null || players[0].isDead()){
				this.successOrFailure = false;
				this.animationFrame = 0;
			} else{
				if ((players[1] == null || players[1].isDead()) && (players[2] == null || players[2].isDead())){
					this.successOrFailure = true;
					this.animationFrame = 0;
				}
			}
			super.checkForFinish();
		}
		//System.out.println(Main.getUserInput().isAcceptingInputs());
	}
	
	private static PlayableCharacter[] getPlayers(){
		return new PlayableCharacter[]{new SelectedCharacter(Main.getCharacterList().getCharacter(22), 6, 0, 0, Team.RED).getCharacter().changePlayerNumber(1),
				new SelectedCharacter(Main.getCharacterList().getCharacter(23), 1, 1, 0, Team.BLUE).getCharacter(), 
				new SelectedCharacter(Main.getCharacterList().getCharacter(4), 1, 1, 0, Team.YELLOW).getCharacter()};
	}
	
	private static Stage getStage(){
		return new CastleDedede();
	}
	
	private static GameRules getRules(){
		return new GameRules();
	}

	@Override
	public float getEnemyLives() {
		return 2;
	}

	@Override
	public float getEnemyLevel() {
		return 1;
	}

	@Override
	public float getPlayerStartLives() {
		return 6;
	}

	@Override
	public float getAllyStartLives() {
		return 0;
	}

	@Override
	public float getAllyLevel() {
		return 0;
	}

	@Override
	public float getPlayerPercentLives() {
		if (players[0] != null && !players[0].isDead()){
			return players[0].getNumberOfLives() * 100/getPlayerStartLives();
		}
		return 0;
	}

	@Override
	public float getAllyPercentLives() {
		return 0;
	}

	@Override
	public float getProportionGiantEnemies() {
		return 0;
	}

	@Override
	public float getProportionTinyEnemies() {
		return 0;
	}
	
	

}
