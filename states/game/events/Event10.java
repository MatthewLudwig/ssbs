package states.game.events;

import states.game.GameRules;
import states.game.SelectedCharacter;
import states.game.stages.*;
import engine.Main;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import entities.Team;
import entities.characters.*;

public class Event10 extends EventGameState {
	
	static boolean kirbyCol;
	static boolean nessCol;
	static boolean capCol;
	
	public Event10(PlayableCharacter player) {
		super(getPlayers(player), getRules(), getStage());
		Utilities.getCustomLoader().getSpriteSheet("Kirby_DEFAULT");
		Utilities.getCustomLoader().getSpriteSheet("Ness_DEFAULT");
		Utilities.getCustomLoader().getSpriteSheet("Captain Falcon_DEFAULT");
	}
	
	public void update(float tpf){
		super.update(tpf);
		if (players[1] != null && players[1].getNumberOfLives() < 2 && !(players[1] instanceof Kirby)){
			replaceCharacter(1, 22, kirbyCol, 1, 3, Team.BLUE);
		}
		if (players[2] != null && players[2].getNumberOfLives() < 2 && !(players[2] instanceof Ness)){
			replaceCharacter(2, 27, nessCol, 1, 3, Team.BLUE);
		}
		if (players[3] != null && players[3].getNumberOfLives() < 2 && !(players[3] instanceof CaptainFalcon)){
			replaceCharacter(3, 28, capCol, 1, 3, Team.BLUE);
		}
	}
	
	@Override
	public void checkForFinish() {
		if (this.animationFrame == -1){
			if (players[0] == null || players[0].isDead()){
				this.successOrFailure = false;
				this.animationFrame = 0;
			} else{
				if ((players[1] == null || players[1].isDead()) && (players[2] == null || players[2].isDead()) && (players[3] == null || players[3].isDead())){
					this.successOrFailure = true;
					this.animationFrame = 0;
				}
			}
			super.checkForFinish();
		}
		//System.out.println(Main.getUserInput().isAcceptingInputs());
	}
	
	private static PlayableCharacter[] getPlayers(PlayableCharacter player){
		player.team = Team.RED;
		player.setNumberOfLives(5);
		kirbyCol = false;
		nessCol = false;
		capCol = false;
		PlayableCharacter pc[] = {player,
				new SelectedCharacter(Main.getCharacterList().getCharacter(1), 2, 3, 0, Team.BLUE).getCharacter(), 
				new SelectedCharacter(Main.getCharacterList().getCharacter(15), 2, 3, 0, Team.BLUE).getCharacter(), 
				new SelectedCharacter(Main.getCharacterList().getCharacter(18), 2, 3, 0, Team.BLUE).getCharacter()};
		if (player.getColorNumber() == 0){
			if (player instanceof Mario){
				pc[1] = new SelectedCharacter(Main.getCharacterList().getCharacter(1), 2, 3, 1, Team.BLUE).getCharacter();
			} if (player instanceof Link){
				pc[2] = new SelectedCharacter(Main.getCharacterList().getCharacter(15), 2, 3, 1, Team.BLUE).getCharacter();
			} if (player instanceof Samus){
				pc[3] = new SelectedCharacter(Main.getCharacterList().getCharacter(18), 2, 3, 1, Team.BLUE).getCharacter();
			} if (player instanceof Kirby){
				Utilities.getCustomLoader().getSpriteSheet("Kirby_COLOR1");
				kirbyCol = true;
			} if (player instanceof Ness){
				Utilities.getCustomLoader().getSpriteSheet("Ness_COLOR1");
				nessCol = true;
			} if (player instanceof CaptainFalcon){
				Utilities.getCustomLoader().getSpriteSheet("Captain Falcon_COLOR1");
				capCol = true;
			}
		}
		//Utilities.getCustomLoader().getSpriteSheet("Kirby_COLOR1");
		//Utilities.getCustomLoader().getSpriteSheet("Ness_COLOR1");
		//Utilities.getCustomLoader().getSpriteSheet("Captain Falcon_COLOR1");
		return pc;
	}
	
	private static Stage getStage(){
		return new FinalDestination();
	}
	
	private static GameRules getRules(){
		GameRules rules = new GameRules();
		rules.setTeamBattleOn(true);
		return rules;
	}

	@Override
	public float getEnemyLives() {
		return 6;
	}

	@Override
	public float getEnemyLevel() {
		return 3;
	}

	@Override
	public float getPlayerStartLives() {
		return 5;
	}

	@Override
	public float getAllyStartLives() {
		return 0;
	}

	@Override
	public float getAllyLevel() {
		return 0;
	}

	@Override
	public float getPlayerPercentLives() {
		if (players[0] != null && !players[0].isDead()){
			return players[0].getNumberOfLives() * 100/getPlayerStartLives();
		}
		return 0;
	}

	@Override
	public float getAllyPercentLives() {
		return 0;
	}

	@Override
	public float getProportionGiantEnemies() {
		return 0;
	}

	@Override
	public float getProportionTinyEnemies() {
		return 0;
	}
	
	

}
