package states.game.events;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.texture.Texture2D;
import com.jme3.ui.Picture;

import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import entities.Team;
import states.game.GameRules;
import states.game.GameState;
import states.game.SelectedCharacter;
import states.game.stages.*;
import states.game.targetTest.TargetTestCharacterSelectState;

public abstract class EventGameState extends GameState{
	private static Texture2D[] failureAnimation;
    private static Texture2D[] successAnimation;
    private static Texture2D[] newRecord;
    private static Texture2D[] countdown;
    
    private Picture status;
    private Picture status2;
    protected boolean successOrFailure;
    protected int animationFrame;
    private int animationFrame2;
    
    protected float timer;
    protected float updateLimiter;
    protected boolean playStart;
    
    protected boolean isRecord;
    
    private float score;
    private String eventName;
    
    protected PlayableCharacter[] players;
	
	static {
        failureAnimation = new Texture2D[35];
        successAnimation = new Texture2D[35];
        newRecord = new Texture2D[35];
        countdown = new Texture2D[14];
        
        for (int count = 0; count < failureAnimation.length; count++) {
            failureAnimation[count] = Utilities.getJMELoader().getTexture("/Interface/Target Test/failure_" + Utilities.getGeneralUtility().getNumberAsString(count) + ".png");
        }
        
        for (int count = 0; count < successAnimation.length; count++) {
            successAnimation[count] = Utilities.getJMELoader().getTexture("/Interface/Target Test/success_" + Utilities.getGeneralUtility().getNumberAsString(count) + ".png");
        }
        
        for (int count = 0; count < newRecord.length; count++) {
        	newRecord[count] = Utilities.getJMELoader().getTexture("/Interface/Target Test/newRecord_" + Utilities.getGeneralUtility().getNumberAsString(count) + ".png");
        }
        
        for (int count = 0; count < countdown.length; count++) {
            countdown[count] = Utilities.getJMELoader().getTexture("/Interface/SoloBattle/READYGO_" + Utilities.getGeneralUtility().getNumberAsString(count) + ".png");
        }
    }
	
	public EventGameState(PlayableCharacter[] players, GameRules rules, Stage stage) {
		super(stage, rules, players);
		this.players = players;
		this.status2 = Utilities.getJMELoader().getPicture("Status2", "/Sprites/Miscellaneous/blankSprite.png", 0, 0, PictureLayer.SPECIALLAYER);
        this.status = Utilities.getJMELoader().getPicture("Status", "/Sprites/Miscellaneous/blankSprite.png", 0, 0, PictureLayer.SPECIALLAYER);
        this.successOrFailure = true;
        this.animationFrame = -1;
        this.animationFrame2 = -1;
        
        if (this.animationFrame2 == -1) {
        	Main.getUserInput().setAcceptingInputs(false);
        	this.animationFrame2 = 0;
        }
        
        this.isRecord = false;
        this.playStart = true;
        this.eventName = "";
	}
	
	public GameState setEventName(String name){
		this.eventName = name;
		return this;
	}
	
	public void attachToAbsoluteNode(Node node, AssetManager am) {
        super.attachToAbsoluteNode(node, am);
        node.attachChild(this.status);
        node.attachChild(this.status2);
        this.status.setLocalTranslation(0, 0, 10);
        this.status2.setLocalTranslation(0, 0, 10);
    }
	
	public void replaceCharacter(int num, int character, boolean altCol, int lives, int level, Team team){
		players[num].setDead(true);
		if (altCol)
			players[num] = new SelectedCharacter(Main.getCharacterList().getCharacter(character), lives, level, 1, team).getCharacter();
		else
			players[num] = new SelectedCharacter(Main.getCharacterList().getCharacter(character), lives, level, 0, team).getCharacter();
		players[num].setPosition(getWorld().getStage().getSizeX()/2f, getWorld().getStage().getSizeY()*2f/3f);
		Main.getGameState().getWorld().getPlayerList().set(num, players[num]);
		Main.getGameState().spawnEntity(players[num]);
		players[num].setPlayerNumber(num+1);
	}
	
	public void update(float tpf) {        
        if (this.animationFrame != -1) {
            if (this.updateLimiter >= .04f) {
                Utilities.getJMELoader().changeTexture(this.status, this.successOrFailure ? successAnimation[this.animationFrame] : failureAnimation[this.animationFrame]);
                if (this.successOrFailure && isRecord){
                	Utilities.getJMELoader().changeTexture(this.status2, newRecord[this.animationFrame]);
                }
                this.animationFrame += 1;

                if (this.animationFrame >= 35) {
                    Main.getUserInput().setAcceptingInputs(true);
                    this.switchStates(new EventSelectState());
                    return;
                }
                
                this.updateLimiter -= .04f;
            } else {
                this.updateLimiter += Utilities.lockedTPF;
            }
            
            Utilities.lockedTPF /= 2;
        } else {
            this.timer += Utilities.lockedTPF;
        }
        
        //System.out.println(this.playStart);
        if (this.animationFrame2 != -1 && this.playStart) {
            if (this.updateLimiter >= .04f) {
                 Utilities.getJMELoader().changeTexture(this.status, countdown[this.animationFrame2]);
                 this.animationFrame2 += 1;

                 if (this.animationFrame2 >= 9) {
                     Main.getUserInput().setAcceptingInputs(true);
                 }
                 if (this.animationFrame2 >= 14) {
                     this.animationFrame2 = -1;
                     this.playStart = false;
                     Utilities.getJMELoader().changeTexture(this.status, "/Sprites/Miscellaneous/blankSprite.png");
                 }
                 
                 this.updateLimiter -= .04f;
             } else {
                 this.updateLimiter += Utilities.lockedTPF;
             }
             
             Utilities.lockedTPF /= 2;
         }
        
        super.update(tpf);
    }
	
	public void checkForFinish() {
        if (this.animationFrame == 0) {
        	Main.getUserInput().setAcceptingInputs(false);
            this.setFreezeCamera(true);
            this.world.setDisableDeath(true);
            if (this.successOrFailure){
            	//System.out.println("ok");
            this.score = (float) (((getEnemyLives() * getEnemyLevel()) / (getPlayerStartLives() - getAllyStartLives() * getAllyLevel() * 0.0066f)) * getPlayerPercentLives() * (getAllyPercentLives() + 50) * 0.5 * Math.pow(2, getProportionGiantEnemies()) * Math.pow(0.5, getProportionTinyEnemies()) * 1.0579);
            if (this.timer < 300){
            	//this.score *= 10.08f*Math.pow(Math.E, Math.sqrt(300-this.timer));
            	this.score *= 300-timer;
            }
            //System.out.println(this.score);
            if (this.score > Main.getEventList().getEvent(eventName).getScore()){
            	isRecord = true;
            	//System.out.println("yay!");
            }
            
            Main.getEventList().getEvent(eventName).setScore(Math.round(this.score));
            }
        }
	}
	
	public abstract float getEnemyLives();
    public abstract float getEnemyLevel();
    public abstract float getPlayerStartLives();
    public abstract float getAllyStartLives();
    public abstract float getAllyLevel();
    public abstract float getPlayerPercentLives();
    public abstract float getAllyPercentLives();
    public abstract float getProportionGiantEnemies();
    public abstract float getProportionTinyEnemies();

}
