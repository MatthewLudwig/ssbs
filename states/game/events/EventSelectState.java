package states.game.events;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.ui.Picture;

import engine.Main;
import engine.PictureLayer;
import engine.ssbs.EventList.EventMapping;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import states.SimpleAppState;
import states.SoloMenuState;
import states.game.GameState;
import states.game.strife.StrifeCharacterSelectState;
import states.game.strife.StrifeGameState;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

/**
 *
 *
 * @author Matthew Ludwig
 */
public class EventSelectState extends SimpleAppState {
    private List<String> previewStrings;
    private String playerString;
    private String scoreString;
    private PlayableCharacter[] players;

    public EventSelectState(PlayableCharacter... passedInCharacters) {
        super("Event Select", true);
        previewStrings = new ArrayList<String>();
        playerString = "";
        players = passedInCharacters;
    }

    @Override
    public void attachToAbsoluteNode(Node node, AssetManager am) {
        //this.previewPath = "Interface/MatchSetup/noPreview.png";
        node.attachChild(Utilities.getJMELoader().getPicture("Background", "Interface/MatchSetup/chooseStage_back.png", 0, 0, PictureLayer.BACKGROUND));
        //node.attachChild(Utilities.getJMELoader().getPicture("Preview", this.previewPath, 435, 266, PictureLayer.GUI));
        node.attachChild(new Node("Description"));
        node.attachChild(Utilities.getJMELoader().getText(ColorRGBA.White, "Choose Event", 80, 440));
        node.attachChild(this.getEventIcons());
    }

    private Node getEventIcons() {
        Node node = new Node("Event Icons");
        
        List<Entry<Integer, EventMapping>> unlockedEvents = Main.getEventList().getUnlockedEvents();
        
        int width = 1;
        int height = 1;
        int rem = 0;
        while (width * height + rem < unlockedEvents.size()) {
            rem++;
            if (rem >= height) {
                rem -= height;
                width++;
            }
            if (height < width) {
                int tempHeight = height + 1;
                int tempWidth = width;
                while (tempWidth * tempHeight > width * height)
                    tempWidth--;
                rem = width * height + rem - tempWidth * tempHeight;
                width = tempWidth;
                height = tempHeight;
            }
        }
        
        float scale = Math.min((7f / (float) (width + (rem != 0 ? 1 : 0))), (6f / (float) height))*0.9f;
        float xOff = 28.5f * (7 - ((width + (rem != 0 ? 1 : 0)) * scale)) / 1.7f - 5;
        float yOff = 25 * (6 - (height * scale)) / 1.7f - 10;
        //scale *= 0.9;
        //System.out.println(scale*0.57f);
        /*float n = 1;
        if (scale > 1.17f){
        	n = scale/1.17f;
        	scale *= n;
        }*/
        
        node.setLocalTranslation(15 - (rem == 0 ? 24 * scale : 0) + (xOff), 350 - (25 * (scale - 1)) - (yOff), -1);
        //scale *= 0.99f;
        node.setLocalScale(scale);
        
        int x = 0, y = 0;
        
        for (Map.Entry<Integer, EventMapping> e : unlockedEvents) {
            int characterIndex = e.getKey();
            EventMapping mapping = e.getValue();
            
            if (rem != 0) {
            	Spatial sp = Utilities.getJMELoader().getPicture("Event Icon #" + characterIndex, mapping.getIcon(), (x * 57 * 1f), -(y * 50 * 1f), PictureLayer.GUI);
            	sp.setLocalScale(sp.getLocalScale().mult(0.6f));
                node.attachChild(sp);

                if (++x == (width + 1)) {
                    x = 0;
                    y++;
                    rem--;
                }
            } else {
            	Spatial sp = Utilities.getJMELoader().getPicture("Event Icon #" + characterIndex, mapping.getIcon(), 28.5f * 1f + (x * 57 * 1f), -(y * 50 * 1f), PictureLayer.GUI);
            	sp.setLocalScale(sp.getLocalScale().mult(0.6f));
                node.attachChild(sp);

                if (++x == width) {
                    x = 0;
                    y++;
                }
            }
        }
        
        return node;
    }

    @Override
    public void attachToPausableNode(Node node, AssetManager am) {
    }

    @Override
    public String getMusicPath() {
        return "Music/MainMenuMusic.ogg";
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        
        Node node = (Node) this.absoluteNode.getChild("Description");
        node.detachAllChildren();
        
        this.customUpdateBackButton(5, -15);

        /*if (Main.getUserInput().isKeyClicked("Pause")) {
            this.playSelectSound();
            this.switchStates(new StrifeGameState(Main.getStageList().getRandomStage().getObject(), this.players));
        }*/

        Node eventIcons = (Node) this.absoluteNode.getChild("Event Icons");
        
        previewStrings = new ArrayList<String>();
        playerString = "";
        scoreString = "";

        for (Spatial object : eventIcons.getChildren()) {
            if (Utilities.getPhysicsUtility().complex2DCollisionWithScale(this.cursorPosition.subtract(eventIcons.getLocalTranslation().x, eventIcons.getLocalTranslation().y), (Picture) object)) {
                EventMapping mapping = Main.getEventList().getEvent(Integer.valueOf(object.getName().split("#")[1]));

                if (mapping != null) {
                    if (Main.getUserInput().isLeftButtonClicked()) {
                        this.playSelectSound();
                        if (mapping.canChoose()){
                        	this.switchStates(new EventCharacterSelectState(mapping, players));
                        } else {
                        	this.switchStates(mapping.getObject());
                        }
                    } else {
                    	//System.out.println(this.playerString);
                    	try {
                    		Scanner s = new Scanner(new File(mapping.getDescription()));
                    		while (s.hasNextLine()){
                    			this.previewStrings.add(s.nextLine());
                    		}
							this.playerString = new Scanner(new File(mapping.getPlayerName())).nextLine();
							this.scoreString = "" + mapping.getScore();
						} catch (FileNotFoundException e) {
							System.out.println("no file");
						}
                    }
                }
            }
        }
        
        int count = 0;
        for (String previewString : previewStrings){
        	node.attachChild(Utilities.getJMELoader().getText(Utilities.Impact16, ColorRGBA.White, previewString, 400, 400 - count*25));
        	count++;
        }
        node.attachChild(Utilities.getJMELoader().getText(Utilities.Impact24, ColorRGBA.Black, "Fighter: " + playerString, 400, 160));
        node.attachChild(Utilities.getJMELoader().getText(Utilities.Impact24, ColorRGBA.White, "High Score: " + scoreString, 400, 100));
    }
    
    protected void customUpdateBackButton(float xPosition, float yPosition) {
        if (this.absoluteNode.getChild("Back Button") == null) {
            this.absoluteNode.attachChild(Utilities.getJMELoader().getPicture("Back Button", "Interface/General/backButton.png", xPosition, yPosition, PictureLayer.GUI));
        } else if (Main.getUserInput().isLeftButtonClicked() && Utilities.getPhysicsUtility().complex2DCollision(Main.getUserInput().getMousePosition(), (Picture) this.absoluteNode.getChild("Back Button"))) {
            Utilities.getCustomLoader().getAudioNode("/Common Sounds/back.ogg").playInstance();
            this.switchStates(new SoloMenuState());
        }   
    }
    
    
}
