package states.game.events;

import states.game.GameRules;
import states.game.SelectedCharacter;
import states.game.stages.*;
import engine.Main;
import entities.PlayableCharacter;
import entities.PlayableCharacter.Scale;
import entities.PlayableCharacter.Scales;
import entities.Team;
//import entities.characters.*;

public class Event3 extends EventGameState {
	
	boolean init;
	
	public Event3() {
		super(getPlayers(), getRules(), getStage());
		init = false;
	}
	
	public void update(float tpf){
		super.update(tpf);
		if (!init){
			init = true;
			players[1].addScale(new Scale(Scales.GIANT, Integer.MAX_VALUE, false));
			players[2].addScale(new Scale(Scales.GIANT, Integer.MAX_VALUE, false));
		}
	}
	
	@Override
	public void checkForFinish() {
		if (this.animationFrame == -1){
			if (players[0] == null || players[0].isDead()){
				this.successOrFailure = false;
				this.animationFrame = 0;
			} else{
				if ((players[1] == null || players[1].isDead()) && (players[2] == null || players[2].isDead())){
					this.successOrFailure = true;
					this.animationFrame = 0;
				}
			}
			super.checkForFinish();
		}
		//System.out.println(Main.getUserInput().isAcceptingInputs());
	}
	
	private static PlayableCharacter[] getPlayers(){
		return new PlayableCharacter[]{new SelectedCharacter(Main.getCharacterList().getCharacter(25), 6, 0, 0, Team.RED).getCharacter().changePlayerNumber(1),
				new SelectedCharacter(Main.getCharacterList().getCharacter(22), 1, 1, 0, Team.BLUE).getCharacter(), 
				new SelectedCharacter(Main.getCharacterList().getCharacter(22), 1, 1, 0, Team.BLUE).getCharacter()};
	}
	
	private static Stage getStage(){
		return new Corneria();
	}
	
	private static GameRules getRules(){
		GameRules rules = new GameRules();
		rules.setTeamBattleOn(true);
		return rules;
	}

	@Override
	public float getEnemyLives() {
		return 2;
	}

	@Override
	public float getEnemyLevel() {
		return 1;
	}

	@Override
	public float getPlayerStartLives() {
		return 6;
	}

	@Override
	public float getAllyStartLives() {
		return 0;
	}

	@Override
	public float getAllyLevel() {
		return 0;
	}

	@Override
	public float getPlayerPercentLives() {
		if (players[0] != null && !players[0].isDead()){
			return players[0].getNumberOfLives() * 100/getPlayerStartLives();
		}
		return 0;
	}

	@Override
	public float getAllyPercentLives() {
		return 0;
	}

	@Override
	public float getProportionGiantEnemies() {
		return 1;
	}

	@Override
	public float getProportionTinyEnemies() {
		return 0;
	}
	
	

}
