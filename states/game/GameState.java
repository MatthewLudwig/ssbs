package states.game;

import engine.ScreenshotAppState;
import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import physics.BoundingBox;
import states.SimpleAppState;
import engine.ssbs.ItemList;
import engine.ssbs.ItemList.ItemMapping;
import engine.ssbs.MusicList.MusicMapping;
import entities.ViolentEntity;

import java.util.logging.Level;

import states.MainMenuState;
import states.game.stages.Stage;
import states.game.strife.NoContestScreenState;
import states.game.strife.StrifeGameState;
import states.game.targetTest.TargetTestCharacterSelectState;
import states.game.targetTest.TargetTestGameState;

/**
 * 
 * 
 * @author Matthew Ludwig
 */
public abstract class GameState extends SimpleAppState {
	private static Random generator = new Random();

	protected World world;
	protected GameRules gameRules;
	protected PlayableCharacter[] players;
	protected float minZoom;

	private boolean freezeCamera;
	private boolean maxCamera;
	private MyCamera camera;
	private float itemTimer;

	private BitmapText musicText;
	private float musicTimer;
	private PhysicsEntity theSmashBall;

	private boolean paused;
	private String pausePlayer;
	private boolean refreshPause;
	private Node pauseScreen;
	private Map<Spatial, Node> unPauseNodes;
	private boolean showPauseInfo;
	private int pauseFocus;
	private Vector2f cameraPosSave;
	private float cameraDimSave;

	public GameState(Stage stage, GameRules rules, PlayableCharacter[] players) {
		super("Game State", false);
		this.world = new World(stage);
		this.gameRules = rules;
		this.players = players;
		this.minZoom = 100;
		this.paused = false;
		this.refreshPause = true;
		this.pausePlayer = "Pause";
		this.unPauseNodes = new HashMap<Spatial, Node>();
		this.showPauseInfo = true;
		this.pauseFocus = 0;

		for (PlayableCharacter player : players) {
			if (player != null) {
				Utilities.getCustomLoader().getSpriteSheet(
						player.getDefaultColor());
			}
		}
		Utilities.getCustomLoader().getSpriteSheet("Ring Out");
		Utilities.getCustomLoader().getSpriteSheet("Status Effect");
		//Utilities.getCustomLoader().getSpriteSheet("Explosions");
		Utilities.getCustomLoader().getSpriteSheet("Items");

		if (this.gameRules.isItemOn("Pokeball")) {
			Utilities.getCustomLoader().getSpriteSheet("Pokemon");
		}

		this.pauseScreen = new Node("Pause Screen");
		this.pauseScreen.attachChild(Utilities.getJMELoader().getPicture(
				"Marker", "Interface/pauseScreen.png", 0, 0, PictureLayer.GUI));
	}
	
	public MyCamera getCamera(){
		return camera;
	}

	protected void setFreezeCamera(boolean state) {
		this.freezeCamera = state;
	}

	public void setMaxCameraZoom(boolean state) {
		this.maxCamera = state;
	}

	public boolean isSmashBallActive(boolean livingBallMatters) {
		if (this.theSmashBall == null) {
			return false;
		} else if (this.theSmashBall.isDead()) {
			for (PlayableCharacter player : this.players) {
				if (player != null && player.isFinalSmashEnabled()) {
					return true;
				}
			}

			return false;
		} else {
			return livingBallMatters;
		}
	}

	public World getWorld() {
		return this.world;
	}

	public int getTotalNumberOfInitialLives() {
		int numberOfInitialLives = 0;

		for (PlayableCharacter player : this.world.getPlayerList()) {
			numberOfInitialLives += player.getInitialNumberOfLives();
		}

		return numberOfInitialLives;
	}

	public void spawnEntity(PhysicsEntity object) {
		if (object instanceof ViolentEntity) {
			if (((ViolentEntity) object).team == null) {
				Main.log(Level.SEVERE,
						"Violent Entity without team is being spawned!",
						new Exception());
			}
		}

		this.world.spawnObject(object, this.camera.getWorldNode(),
				this.absoluteNode, object.alwaysDraw);
	}

	@Override
	public void attachToAbsoluteNode(Node node, AssetManager am) {
		this.musicText = Utilities.getJMELoader().getText(Utilities.Impact24O,
				ColorRGBA.Yellow, "", 10, 440);
		this.absoluteNode.attachChild(this.musicText);
		this.musicTimer = 0;
	}

	@Override
	public void attachToPausableNode(Node node, AssetManager am) {
		this.freezeCamera = false;

		Node cameraNode = new Node("Camera");
		this.camera = new MyCamera(cameraNode);
		node.attachChild(cameraNode);

		this.world.setUpDisplay(this.camera.getWorldNode());

		for (int count = 0; count < this.players.length; count++) {
			if (this.players[count] != null) {
				Vector2f spawnPoint = this.world.getSpawnPoint(true, 0);
				this.players[count].setPosition(spawnPoint.x, spawnPoint.y);
				this.players[count].setPlayerNumber(count + 1);
				this.spawnEntity(this.players[count]);
			}
		}

		this.scaleCam();
	}

	public void scaleCam() {
		if (!this.freezeCamera) {
			if (this.maxCamera) {
				this.camera.setCameraLimits(this.world.stage.getCameraBounds());
				this.camera.setTargetPosition(this.world.stage
						.getCameraBounds().getExactCenter().x, this.world.stage
						.getCameraBounds().getExactCenter().y);
				this.camera.setTargetDimensions(this.world.stage
						.getCameraBounds().getDimensions().x, this.world.stage
						.getCameraBounds().getDimensions().y);
			} else {
				Vector2f lowerLeftPoint = new Vector2f(-1, -1);
				Vector2f upperRightPoint = new Vector2f(-1, -1);

				if (this.theSmashBall != null && !this.theSmashBall.isDead()) {
					if (lowerLeftPoint.x == -1) {
						lowerLeftPoint.setX(this.theSmashBall.getFullBounds()
								.getLowerMidpoint().x - this.minZoom);
					} else {
						lowerLeftPoint.setX(Math.min(this.theSmashBall
								.getFullBounds().getLowerMidpoint().x
								- this.minZoom, lowerLeftPoint.x));
					}

					if (lowerLeftPoint.y == -1) {
						lowerLeftPoint.setY(this.theSmashBall.getFullBounds()
								.getLowerMidpoint().y - this.minZoom);
					} else {
						lowerLeftPoint.setY(Math.min(this.theSmashBall
								.getFullBounds().getLowerMidpoint().y
								- this.minZoom, lowerLeftPoint.y));
					}

					if (upperRightPoint.x == -1) {
						upperRightPoint.setX(this.theSmashBall.getFullBounds()
								.getLowerMidpoint().x + this.minZoom);
					} else {
						upperRightPoint.setX(Math.max(this.theSmashBall
								.getFullBounds().getLowerMidpoint().x
								+ this.minZoom, upperRightPoint.x));
					}

					if (upperRightPoint.y == -1) {
						upperRightPoint.setY(this.theSmashBall
								.getAppropriateBounds().getLowerMidpoint().y
								+ this.minZoom);
					} else {
						upperRightPoint.setY(Math.max(this.theSmashBall
								.getAppropriateBounds().getLowerMidpoint().y
								+ this.minZoom, upperRightPoint.y));
					}
				}

				for (PlayableCharacter character : this.world.getPlayerList()) {
					if (!character.isDead()) {
						if (lowerLeftPoint.x == -1) {
							lowerLeftPoint.setX(character.getFullBounds()
									.getLowerMidpoint().x - this.minZoom);
						} else {
							lowerLeftPoint.setX(Math.min(character
									.getFullBounds().getLowerMidpoint().x
									- this.minZoom, lowerLeftPoint.x));
						}

						if (lowerLeftPoint.y == -1) {
							lowerLeftPoint.setY(character.getFullBounds()
									.getLowerMidpoint().y - this.minZoom);
						} else {
							lowerLeftPoint.setY(Math.min(character
									.getFullBounds().getLowerMidpoint().y
									- this.minZoom, lowerLeftPoint.y));
						}

						if (upperRightPoint.x == -1) {
							upperRightPoint.setX(character.getFullBounds()
									.getLowerMidpoint().x + this.minZoom);
						} else {
							upperRightPoint.setX(Math.max(character
									.getFullBounds().getLowerMidpoint().x
									+ this.minZoom, upperRightPoint.x));
						}

						if (upperRightPoint.y == -1) {
							upperRightPoint
									.setY(character.getAppropriateBounds()
											.getLowerMidpoint().y
											+ this.minZoom);
						} else {
							upperRightPoint.setY(Math.max(
									character.getAppropriateBounds()
											.getLowerMidpoint().y
											+ this.minZoom, upperRightPoint.y));
						}
					}
				}

				BoundingBox potentialCameraBounds = new BoundingBox(
						lowerLeftPoint,
						upperRightPoint.subtract(lowerLeftPoint));

				this.camera.setCameraLimits(this.world.stage.getCameraBounds());
				this.camera.setTargetPosition(
						potentialCameraBounds.getExactCenter().x,
						potentialCameraBounds.getExactCenter().y);
				this.camera.setTargetDimensions(
						potentialCameraBounds.getDimensions().x,
						potentialCameraBounds.getDimensions().y);
			}
		}

		this.camera.updateCameraState();
	}

	public void unlockMusic() {
		this.musicTimer = 4;

		MusicMapping mapping = Main.getMusicList().getRandomLockedMusic();
		mapping.setUnlocked(true);
		this.musicText.setText("Unlocked Music:  " + mapping.getName());
	}

	@Override
	public String getMusicPath() {
		StringBuilder builder = new StringBuilder("Stages/");
		builder.append(this.world.stage.getMusicName());
		builder.append("/Music_");

		float chance = generator.nextFloat();
		float cumulativeChance = 0;

		for (int count = 1; count <= this.world.stage.getNumberOfSongs(); count++) {
			float thisChance = ((float) Math.pow(2,
					this.world.stage.getNumberOfSongs() - count))
					/ ((float) Math.pow(2, this.world.stage.getNumberOfSongs()) - 1);

			if (chance < (cumulativeChance + thisChance)) {
				builder.append(count);
				break;
			} else {
				cumulativeChance += thisChance;
			}
		}

		builder.append(".ogg");

		return builder.toString();
	}

	@Override
	public void update(float tpf) {
		super.update(tpf);

		if (!Main.getUserInput().getKeyState(pausePlayer)) {
			this.refreshPause = true;
		}

		if (paused) {
			String pauseUp = pausePlayer.substring(0, 3) + "Up";
			String pauseDown = pausePlayer.substring(0, 3) + "Down";
			String pauseLeft = pausePlayer.substring(0, 3) + "Left";
			String pauseRight = pausePlayer.substring(0, 3) + "Right";
			String pauseAttack = pausePlayer.substring(0, 3) + "Attack";
			String pauseSpecial = pausePlayer.substring(0, 3) + "Special";
			String pauseShield = pausePlayer.substring(0, 3) + "Shield";
			if (this.showPauseInfo) {
				this.absoluteNode.attachChild(this.pauseScreen);
				this.absoluteNode.attachChild(Utilities.getJMELoader()
						.getCenteredText(Utilities.Impact32O, ColorRGBA.White,
								pausePlayer, 255, 425));
				this.absoluteNode.attachChild(Utilities.getJMELoader()
						.getCenteredText(
								Utilities.Impact16O,
								ColorRGBA.White,
								Main.getUserInput().getKeyName(pausePlayer)
										+ " : Resume", 129, 400));
				this.absoluteNode.attachChild(Utilities.getJMELoader()
						.getCenteredText(
								Utilities.Impact16O,
								ColorRGBA.White,
								Main.getUserInput().getKeyName(pauseShield)
										+ " + "
										+ Main.getUserInput().getKeyName(
												pauseAttack)
										+ " + "
										+ Main.getUserInput().getKeyName(
												pausePlayer) + " : End Match",
								129, 370));
				this.absoluteNode.attachChild(Utilities.getJMELoader()
						.getCenteredText(
								Utilities.Impact16O,
								ColorRGBA.White,
								Main.getUserInput().getKeyName(pauseUp)
										+ "/"
										+ Main.getUserInput().getKeyName(
												pauseLeft)
										+ "/"
										+ Main.getUserInput().getKeyName(
												pauseDown)
										+ "/"
										+ Main.getUserInput().getKeyName(
												pauseRight) + " : Pan", 500,
								170));
				this.absoluteNode.attachChild(Utilities.getJMELoader()
						.getCenteredText(
								Utilities.Impact16O,
								ColorRGBA.White,
								Main.getUserInput().getKeyName(pauseAttack)
										+ " + "
										+ Main.getUserInput().getKeyName(
												pauseUp)
										+ "/"
										+ Main.getUserInput().getKeyName(
												pauseDown) + " : Zoom", 500,
								140));
				this.absoluteNode.attachChild(Utilities.getJMELoader()
						.getCenteredText(
								Utilities.Impact16O,
								ColorRGBA.White,
								Main.getUserInput().getKeyName(pauseAttack)
										+ " + "
										+ Main.getUserInput().getKeyName(
												pauseLeft)
										+ "/"
										+ Main.getUserInput().getKeyName(
												pauseRight) + " : Focus", 500,
								110));
				this.absoluteNode.attachChild(Utilities.getJMELoader()
						.getCenteredText(
								Utilities.Impact16O,
								ColorRGBA.White,
								Main.getUserInput().getKeyName(pauseSpecial)
										+ " : Take Snapshot", 500, 80));
				this.absoluteNode.attachChild(Utilities.getJMELoader()
						.getCenteredText(
								Utilities.Impact16O,
								ColorRGBA.White,
								Main.getUserInput().getKeyName(pauseShield)
										+ " : Show/Hide Help", 500, 50));
			} else {
				this.absoluteNode.detachAllChildren();
			}
			if (Main.getUserInput().isKeyClicked(pauseShield)) {
				this.showPauseInfo = !this.showPauseInfo;
			}
			if (Main.getUserInput().isKeyClicked(pauseSpecial)) {
				this.absoluteNode.detachAllChildren();
				String dateString = new SimpleDateFormat("MM-dd-yyyy_HH-mm-ss")
						.format(new Date());
				ScreenshotAppState screenShotState = new ScreenshotAppState(
						System.getProperty("user.dir") + "/Snapshots/", "SSBS_"
								+ dateString);
				screenShotState.setIsNumbered(false);
				this.stateManager.attach(screenShotState);
				screenShotState.onAction(null, true, 0);
			}
			if (Main.getUserInput().getKeyState(pauseAttack)
					&& Main.getUserInput().isKeyClicked(pauseLeft)) {
				this.pauseFocus--;
				if (this.pauseFocus < 0) {
					this.pauseFocus = this.getWorld().getPlayerList().size();
				}
				if (this.pauseFocus > 0) {
					camera.forceCameraPosition(this.getWorld().getPlayerList()
							.get(pauseFocus - 1).getPosition().x, this
							.getWorld().getPlayerList().get(pauseFocus - 1)
							.getPosition().y);
					camera.forceCameraDimensions(160, 120);
				} else {
					camera.forceCameraPosition(this.getWorld().getStage()
							.getSizeX() / 2f, this.getWorld().getStage()
							.getSizeY() / 2f);
					camera.forceCameraDimensions(Integer.MAX_VALUE,
							Integer.MAX_VALUE);
				}
			} else if (Main.getUserInput().getKeyState(pauseAttack)
					&& Main.getUserInput().isKeyClicked(pauseRight)) {
				this.pauseFocus++;
				if (this.pauseFocus > this.getWorld().getPlayerList().size()) {
					this.pauseFocus = 0;
				}
				if (this.pauseFocus > 0) {
					camera.forceCameraPosition(this.getWorld().getPlayerList()
							.get(pauseFocus - 1).getPosition().x, this
							.getWorld().getPlayerList().get(pauseFocus - 1)
							.getPosition().y);
					camera.forceCameraDimensions(160, 120);
				} else {
					camera.forceCameraPosition(this.getWorld().getStage()
							.getSizeX() / 2f, this.getWorld().getStage()
							.getSizeY() / 2f);
					camera.forceCameraDimensions(this.getWorld().getStage()
							.getCameraBounds().getDimensions().x * 0.99f, this
							.getWorld().getStage().getCameraBounds()
							.getDimensions().y * 0.99f);
				}
			} else if (Main.getUserInput().getKeyState(pauseAttack)
					&& Main.getUserInput().getKeyState(pauseUp)) {
				if (camera.getCurrentScale() > 0.1f) {
					camera.forceCameraDimensions(
							camera.getCurrentScale() * 640 - 7,
							camera.getCurrentScale() * 480 - 7);
				}
			} else if (Main.getUserInput().getKeyState(pauseAttack)
					&& Main.getUserInput().getKeyState(pauseDown)) {
				if (camera.getCurrentScale() < this.getWorld().getStage()
						.getCameraBounds().getDimensions().x / 640f - 0.1
						&& camera.getCurrentScale() < this.getWorld()
								.getStage().getCameraBounds().getDimensions().y / 480f - 0.1) {
					camera.forceCameraDimensions(
							camera.getCurrentScale() * 640 + 7,
							camera.getCurrentScale() * 480 + 7);
				}
			} else {
				if (Main.getUserInput().getKeyState(pauseUp)) {
					camera.forceCameraPosition(camera.getCurrentPosition().x,
							camera.getCurrentPosition().y + 2);
				}
				if (Main.getUserInput().getKeyState(pauseDown)) {
					camera.forceCameraPosition(camera.getCurrentPosition().x,
							camera.getCurrentPosition().y - 2);
				}
				if (Main.getUserInput().getKeyState(pauseLeft)) {
					camera.forceCameraPosition(
							camera.getCurrentPosition().x - 2,
							camera.getCurrentPosition().y);
				}
				if (Main.getUserInput().getKeyState(pauseRight)) {
					camera.forceCameraPosition(
							camera.getCurrentPosition().x + 2,
							camera.getCurrentPosition().y);
				}
			}
			if (Main.getUserInput().getKeyState(pausePlayer)
					&& this.refreshPause) {
				this.absoluteNode.detachAllChildren();
				for (Map.Entry<Spatial, Node> entry : unPauseNodes.entrySet()) {
					entry.getValue().attachChild(entry.getKey());
				}
				paused = false;
				pausePlayer = "Pause";
				this.refreshPause = false;
				this.showPauseInfo = true;
				this.camera.forceCameraPosition(cameraPosSave.x,
						cameraPosSave.y);
				this.camera.forceCameraDimensions(cameraDimSave * 640,
						cameraDimSave * 480);
				if (Main.getUserInput().getKeyState(pauseShield)
						&& Main.getUserInput().getKeyState(pauseAttack)) {
					if (this instanceof TargetTestGameState) {
						this.switchStates(new TargetTestCharacterSelectState(
								this.getWorld().getPlayerList().get(0)));
					} else if (this instanceof StrifeGameState) {
						PlayableCharacter[] allPlayers = new PlayableCharacter[this
								.getWorld().getPlayerList().size()];
						for (int i = 0; i < allPlayers.length; i++) {
							allPlayers[i] = this.getWorld().getPlayerList()
									.get(i);
						}
						this.switchStates(new NoContestScreenState(allPlayers));
					} else {
						this.switchStates(new MainMenuState());
					}
				}
			}
			return;
		}

		this.scaleCam();
		if (this.refreshPause) {
			if (Main.getUserInput().getKeyState("P1 Pause")) {
				paused = true;
				pausePlayer = "P1 Pause";
				pauseFocus = 1;
			} else if (Main.getUserInput().getKeyState("P2 Pause")) {
				paused = true;
				pausePlayer = "P2 Pause";
				pauseFocus = 2;
			} else if (Main.getUserInput().getKeyState("P3 Pause")) {
				paused = true;
				pausePlayer = "P3 Pause";
				pauseFocus = 3;
			} else if (Main.getUserInput().getKeyState("P4 Pause")) {
				paused = true;
				pausePlayer = "P4 Pause";
				pauseFocus = 4;
			}

			if (paused) {
				this.refreshPause = false;

				for (Spatial s : this.absoluteNode.getChildren()) {
					unPauseNodes.put(s, absoluteNode);
				}
				this.absoluteNode.detachAllChildren();

				for (PlayableCharacter p : this.getWorld().getPlayerList()) {
					for (Spatial s : p.getDisplay().getChildren()) {
						if (!s.getName().equals("Picture"))
							unPauseNodes.put(s, p.getDisplay());
					}
					p.detachAllDisplayChildren();
				}

				cameraPosSave = this.camera.getTargetPosition();
				cameraDimSave = this.camera.getTargetScale();

				camera.forceCameraDimensions(160, 120);
				camera.forceCameraPosition(
						this.getWorld().getPlayerList().get(pauseFocus - 1)
								.getPosition().x, this.getWorld()
								.getPlayerList().get(pauseFocus - 1)
								.getPosition().y);
			}
		}

		this.itemTimer += Utilities.lockedTPF;

		if (this.itemTimer >= this.gameRules.getItemFrequency()
				.getFrequencyTime()) {
			this.itemTimer -= this.gameRules.getItemFrequency()
					.getFrequencyTime();

			float cumulativeChance = 0;

			for (ItemMapping i : ItemList.getItems()) {

				if (this.gameRules.isItemOn(i.getName())) {
					cumulativeChance += i.getChance();

					if (generator.nextInt(111) < i.getChance()) {
						Vector2f itemPosition = this.world.stage
								.getRandomItemPoint();

						if (i.getName().equals("Smash Ball")) {
							if (!this.isSmashBallActive(true)) {
								PhysicsEntity item = i.getObject();
								item.setPosition(itemPosition.x, itemPosition.y);
								this.spawnEntity(item);

								this.theSmashBall = item;
							}
						} else {
							PhysicsEntity item = i.getObject();
							item.setPosition(itemPosition.x, itemPosition.y);
							this.spawnEntity(item);
						}
					}
				} else if (i.getName().equals("CD")
						&& Main.getMusicList().getRandomLockedMusic() != null) {
					cumulativeChance /= 10f;

					if (generator.nextFloat() * 111 < cumulativeChance) {
						Vector2f itemPosition = this.world.stage
								.getRandomItemPoint();
						PhysicsEntity item = i.getObject();
						item.setPosition(itemPosition.x, itemPosition.y);
						this.spawnEntity(item);
					}
				}
			}
		}

		this.musicTimer = Math.max(this.musicTimer - Utilities.lockedTPF, 0);

		if (this.musicTimer == 0) {
			this.musicText.setText("");
		}

		this.world.update();
		this.checkForFinish();
	}

	public abstract void checkForFinish();
}