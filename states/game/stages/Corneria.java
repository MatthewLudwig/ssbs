package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Main;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class Corneria extends Stage {

    public Corneria() {
        super("Corneria", 1);
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(73, 93), new Vector2f(760, 570));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(25, 54), new Vector2f(851, 666));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(191, 447);
            case 1:
                return new Vector2f(364, 433);
            case 2:
                return new Vector2f(598, 524);
            case 3:
                return new Vector2f(727, 397);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 6;
    }
}
