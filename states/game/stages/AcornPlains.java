package states.game.stages;

import java.util.logging.Level;

import physics.BoundingBox;

import com.jme3.math.Vector2f;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;

import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;

/**
 *
 * @author Matthew
 */
public class AcornPlains extends Stage {
    private Picture stationaryBackground;
    private Picture firstBackground;
    private Picture secondBackground;
    private Picture thirdBackground;
    private Picture fourthBackground;
    
    private float xOffset;
    private float incrementedOffset;
    private int currentIncrement;
    
    public AcornPlains() {
        super("Acorn Plains", 1);
        this.stationaryBackground = Utilities.getJMELoader().getPicture("Stationary", "/Stages/Acorn Plains/Background.png", 0, 0, PictureLayer.BACKGROUND);
        this.firstBackground = Utilities.getJMELoader().getPicture("First", "/Stages/Acorn Plains/Background_000.png", 0, 0, PictureLayer.BACKGROUND);
        this.secondBackground = Utilities.getJMELoader().getPicture("Second", "/Stages/Acorn Plains/Background_001.png", 2484, 0, PictureLayer.BACKGROUND);
        this.thirdBackground = Utilities.getJMELoader().getPicture("Third", "/Stages/Acorn Plains/Background_002.png", 4968, 0, PictureLayer.BACKGROUND);
        this.fourthBackground = Utilities.getJMELoader().getPicture("Fourth", "/Stages/Acorn Plains/Background_003.png", 7452, 0, PictureLayer.BACKGROUND);

        this.xOffset = 0;
        this.incrementedOffset = 0;
        this.currentIncrement = 0;
    }
    
    @Override
    public void attachBackground(Node node) {
        node.attachChild(this.stationaryBackground);
        node.attachChild(this.firstBackground);
        node.attachChild(this.secondBackground);
        node.attachChild(this.thirdBackground);
        node.attachChild(this.fourthBackground);
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(16 + xOffset, 13), new Vector2f(472, 354));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(3 + xOffset, 3), new Vector2f(499, 373));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(109, 54);
            case 1:
                return new Vector2f(204, 54);
            case 2:
                return new Vector2f(299, 54);
            case 3:
                return new Vector2f(394, 54);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 4;
    }
    
    @Override
    public void update(float lockedTPF) {
        if (!Main.getGameState().isSmashBallActive(false)) {
            this.xOffset += 30 * lockedTPF;
            this.stationaryBackground.setLocalTranslation(this.xOffset, 0, this.stationaryBackground.getLocalTranslation().z);

            this.incrementedOffset += 30 * lockedTPF;

            if (this.incrementedOffset > 2484) {
                switch (this.currentIncrement) {
                    case 0:
                        this.firstBackground.move(9936, 0, 0);
                        break;
                    case 1:
                        this.secondBackground.move(9936, 0, 0);
                        break;
                    case 2:
                        this.thirdBackground.move(9936, 0, 0);
                        break;
                    case 3:
                        this.fourthBackground.move(9936, 0, 0);
                        break;
                }

                this.currentIncrement++;

                if (this.currentIncrement == 4) {
                    this.currentIncrement = 0;
                }

                this.incrementedOffset = 0;
            }
        }
    }
    
    @Override
    public boolean complex2DCollision(Vector2f point, float value) {
        Vector2f realPoint = point.clone();
        
        while (realPoint.x > 9936) {
            realPoint.subtractLocal(9936, 0);
        }
        
        return super.complex2DCollision(realPoint, value);
    }
}
