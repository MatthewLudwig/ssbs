package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Main;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import entities.general.JungleJapesOClaptrap;

import java.util.List;
import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 * 
 * @author Matthew
 */
public class JungleJapesO extends Stage {

	float multiplier, counter, timer;

	public JungleJapesO() {
		super("Jungle Japes", 1);
		multiplier = (float) (Math.random() * 3 - 1.5);
		counter = 0;
		timer = (float) (Math.random() * 2 + 3);
	}

	@Override
	public BoundingBox getCameraBounds() {
		return new BoundingBox(new Vector2f(158, 138), new Vector2f(735, 551));
	}

	@Override
	public BoundingBox getWorldBounds() {
		return new BoundingBox(new Vector2f(99, 122), new Vector2f(845, 592));
	}

	@Override
	public Vector2f getSpawnPoint() {
		switch (generator.nextInt(4)) {
		case 0:
			return new Vector2f(263, 353);
		case 1:
			return new Vector2f(524, 395);
		case 2:
			return new Vector2f(530, 329);
		case 3:
			return new Vector2f(791, 353);
		default:
			Main.log(Level.SEVERE, "Unknown spawn point generated!", null);
			return null;
		}
	}

	@Override
	public int getNumberOfSongs() {
		return 4;
	}

	@Override
	public void update(float lockedTPF) {
		List<PlayableCharacter> characters = Main.getGameState().getWorld()
				.getPlayerList();
		counter += Utilities.lockedTPF;
		if (counter > 2) {
			counter = 0;
			multiplier = (float) (Math.random() * 3 - 1.5);
		}
		timer -= Utilities.lockedTPF;
		if (timer < 0) {
			timer = (float) (Math.random() * 2 + 3);
			JungleJapesOClaptrap claptrap = new JungleJapesOClaptrap();
			double rand = Math.random();
			if (rand < 0.25) {
				claptrap.setPosition(162, 140);
			} else if (rand < 0.5) {
				claptrap.setPosition(348, 140);
			} else if (rand < 0.75) {
				claptrap.setPosition(704, 140);
			} else {
				claptrap.setPosition(902, 140);
			}
			Main.getGameState().spawnEntity(claptrap);
		}
		for (PlayableCharacter character : characters) {
			if (character.getAppropriateBounds().getExactCenter().y < 160) {
				character.setVelocity(-150 * multiplier, 0, false);
				if (!character.getDrowning()) {
					character.startDrowning();
				}
			}
		}
	}
}
