package states.game.stages;

import com.jme3.math.Vector2f;
import engine.Main;
import entities.Team;
import entities.general.EmblemMonster;
import entities.general.PyroLava;
import entities.general.RainbowLoop;

import java.util.logging.Level;
import physics.BoundingBox;

/**
 *
 * @author Matthew
 */
public class Pyrosphere extends Stage {
	private int time;
	private boolean initialized;
	private PyroLava lava;
	
    public Pyrosphere() {
        super("Pyrosphere", 1, .1f, 0);
        this.time = 0;
        this.initialized = false;
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(109, 42), new Vector2f(956, 491));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(57, 23), new Vector2f(1065, 573));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(357, 251);
            case 1:
                return new Vector2f(490, 251);
            case 2:
                return new Vector2f(665, 251);
            case 3:
                return new Vector2f(791, 251);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 4;
    }
    
    public void update(float lockedTPF) {
		super.update(lockedTPF);
		this.time++;
		float offset = -300f;
		float xOffset = 1175/2f;

		if (!initialized) {
			lava = new PyroLava();
			lava.setMortality(false);
			lava.team = Team.NONE;
			lava.setPosition(xOffset, offset);
			lava.setTarget(offset);
			Main.getGameState().spawnEntity(lava);

			this.initialized = true;
			this.time = 0;
		}
		
		if (initialized && time > 7.0/lockedTPF){
			double r = Math.random();
			if (r < 0.1)
				lava.setTarget(355+offset);
			else if (r < 0.3)
				lava.setTarget(285+offset);
			else if (r < 0.6)
				lava.setTarget(183+offset);
			else
				lava.setTarget(offset);
			this.time = 0;
		}
	}
}
