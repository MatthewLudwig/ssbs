package states.game.stages;

import com.jme3.math.Vector2f;
import engine.Main;
import java.util.logging.Level;
import physics.BoundingBox;

/**
 *
 * @author Matthew
 */
public class WilysCastle extends Stage {
    public WilysCastle() {
        super("Wilys Castle", 1);
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(85, 117), new Vector2f(764, 573));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(53, 55), new Vector2f(869, 757));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(303, 428);
            case 1:
                return new Vector2f(441, 446);
            case 2:
                return new Vector2f(577, 413);
            case 3:
                return new Vector2f(694, 382);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 4;
    }
}
