package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Main;
import engine.utility.Utilities;
import entities.general.SaffronCityOBuildingLeft;
import entities.general.SaffronCityOBuildingRight;
import entities.general.SaffronCityODoor;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class SaffronCityO extends Stage {
	private SaffronCityODoor door;
	boolean init;
	float timer;

	public SaffronCityO() {
		super("Saffron City O", 1, true);
		init = false;
		timer = 0;
		Utilities.getCustomLoader().getSpriteSheet("Explosions");
	}

	@Override
	public BoundingBox getCameraBounds() {
		return new BoundingBox(new Vector2f(114, 82), new Vector2f(664, 498));
	}

	@Override
	public BoundingBox getWorldBounds() {
		return new BoundingBox(new Vector2f(36, 53), new Vector2f(782, 544));
	}

	@Override
	public Vector2f getSpawnPoint() {
		switch (generator.nextInt(4)) {
		case 0:
			return new Vector2f(209, 240);
		case 1:
			return new Vector2f(427, 327);
		case 2:
			return new Vector2f(564, 400);
		case 3:
			return new Vector2f(707, 364);
		default:
			Main.log(Level.SEVERE, "Unknown spawn point generated!", null);
			return null;
		}
	}

	@Override
	public int getNumberOfSongs() {
		return 6;
	}

	public void update(float lockedTPF) {
		super.update(lockedTPF);
		if (!init) {
			init = true;
			
			SaffronCityOBuildingLeft left = new SaffronCityOBuildingLeft();
			left.setPosition(208, 0);
			left.yStart = 0;
			Main.getGameState().spawnEntity(left);
			addIrregular(left);
			
			SaffronCityOBuildingRight right = new SaffronCityOBuildingRight();
			right.setPosition(708, 8);
			right.yStart = 8;
			Main.getGameState().spawnEntity(right);
			addIrregular(right);
			
			door = new SaffronCityODoor();
			door.setPosition(553, 326);
			Main.getGameState().spawnEntity(door);
		}
		if (!door.isDoorOpen()){
			timer += lockedTPF;
		}
		if (timer > 15){
			timer = 0;
			door.openDoor();
		}
	}
}
