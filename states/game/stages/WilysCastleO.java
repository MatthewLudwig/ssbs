package states.game.stages;

import com.jme3.math.Vector2f;
import engine.Main;
import entities.general.DrWily;
import entities.general.ProtoMan;

import java.util.logging.Level;
import physics.BoundingBox;

/**
 * 
 * @author Matthew
 */
public class WilysCastleO extends Stage {
	private ProtoMan proto;
	private DrWily wily;
	private float timer;

	public WilysCastleO() {
		super("Wilys Castle", 1);
		timer = 0f;
	}

	@Override
	public BoundingBox getCameraBounds() {
		return new BoundingBox(new Vector2f(85, 117), new Vector2f(764, 573));
	}

	@Override
	public BoundingBox getWorldBounds() {
		return new BoundingBox(new Vector2f(53, 55), new Vector2f(869, 757));
	}

	@Override
	public Vector2f getSpawnPoint() {
		switch (generator.nextInt(4)) {
		case 0:
			return new Vector2f(303, 428);
		case 1:
			return new Vector2f(441, 446);
		case 2:
			return new Vector2f(577, 413);
		case 3:
			return new Vector2f(694, 382);
		default:
			Main.log(Level.SEVERE, "Unknown spawn point generated!", null);
			return null;
		}
	}

	@Override
	public int getNumberOfSongs() {
		return 4;
	}

	public void update(float lockedTPF) {
		super.update(lockedTPF);
		timer += lockedTPF;
		if (proto != null && !proto.isDead() && timer > 5f){
			timer = 0f;
		}
		if (wily != null && !wily.isDead() && timer > 5f){
			timer = 0f;
		}
		
		if (timer > 10f && (proto == null || proto.isDead())
				&& (wily == null || wily.isDead())) {
			timer = 0f;
			if (Math.random() < 0.67) {
				Vector2f spawnPoint = this.getRandomItemPoint();
				wily = new DrWily();
				wily.setPosition((spawnPoint.x + getSizeX() / 2f) / 2f,
						spawnPoint.y + 50);
				Main.getGameState().spawnEntity(wily);
			} else {

				Vector2f spawnPoint = this.getRandomItemPoint();
				proto = new ProtoMan();
				proto.setPosition((spawnPoint.x + getSizeX() / 2f) / 2f,
						spawnPoint.y);
				Main.getGameState().spawnEntity(proto);
			}
		}
	}
}
