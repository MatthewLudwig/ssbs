package states.game.stages;

import com.jme3.math.Vector2f;
import engine.Main;
import engine.utility.Utilities;
import entities.general.AxeKnight;
import entities.general.BandanaWaddleDee;
import entities.general.CastleDededeOElectricWall;
import entities.general.JavelinKnight;
import entities.general.MaceKnight;
import entities.general.TridentKnight;

import java.util.logging.Level;
import physics.BoundingBox;

/**
 *
 * @author Matthew
 */
public class CastleDededeO extends Stage {
	private boolean init;
	private float wallTimer;
	private float knightTimer;
	private int knightMax;
	private CastleDededeOElectricWall[] walls;
	
    public CastleDededeO() {
        super("Castle Dedede O", 1, .1f, 0, true);
        init = false;
        wallTimer = 0;
        knightTimer = 0;
        knightMax = (int) (Math.random()*6 + 30);
        //System.out.println(knightMax);
        walls = new CastleDededeOElectricWall[3];
        
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(46, 19), new Vector2f(435, 326));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(3, 3), new Vector2f(521, 387));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(86, 90);
            case 1:
                return new Vector2f(166, 57);
            case 2:
                return new Vector2f(366, 57);
            case 3:
                return new Vector2f(444, 90);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    public void update(float lockedTPF){
    	super.update(lockedTPF);
    	
    	if (!init){
    		init = true;
    		
    		BandanaWaddleDee waddle = new BandanaWaddleDee();
    		waddle.setPosition(316, 112);
    		Main.getGameState().spawnEntity(waddle);
    		
    		walls[0] = new CastleDededeOElectricWall();
    		walls[0].setPosition(75, 94);
    		Main.getGameState().spawnEntity(walls[0]);
    		
    		walls[1] = new CastleDededeOElectricWall();
    		walls[1].setPosition(267, 168);
    		Main.getGameState().spawnEntity(walls[1]);
    		
    		walls[2] = new CastleDededeOElectricWall();
    		walls[2].setPosition(456, 94);
    		Main.getGameState().spawnEntity(walls[2]);
    	}
    	
    	wallTimer += Utilities.lockedTPF;
    	knightTimer += Utilities.lockedTPF;
    	if (wallTimer > 12){
    		walls[(int) (Math.random()*3)].activate();
    		wallTimer = 0;
    	}
    	if (knightTimer > knightMax){
    		knightMax = (int) (Math.random()*6 + 30);
    		knightTimer = 0;
    		//System.out.println(knightMax);
    		
    		AxeKnight axe = new AxeKnight();
    		axe.setPosition(25, (float) (Math.random()*100 + 150));
    		Main.getGameState().spawnEntity(axe);
    		
    		MaceKnight mace = new MaceKnight();
    		mace.setPosition(25, (float) (Math.random()*100 + 150));
    		Main.getGameState().spawnEntity(mace);
    		
    		JavelinKnight javelin = new JavelinKnight();
    		javelin.setPosition(25, (float) (Math.random()*100 + 150));
    		Main.getGameState().spawnEntity(javelin);
    		
    		TridentKnight trident = new TridentKnight();
    		trident.setPosition(25, (float) (Math.random()*100 + 150));
    		Main.getGameState().spawnEntity(trident);
    	}
    }
    
    @Override
    public int getNumberOfSongs() {
        return 5;
    }
}
