package states.game.stages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import physics.BoundingBox;

import com.jme3.math.Vector2f;
import com.jme3.scene.Node;
import com.jme3.texture.Texture2D;
import com.jme3.texture.image.ImageRaster;
import com.jme3.ui.Picture;

import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import entities.IrregularObstacle;
import entities.MovingPlatform;
import entities.PhysicsEntity;
import entities.PlayableCharacter;

/**
 * 
 * 
 * @author Matthew Ludwig
 */
public abstract class Stage {
	protected static Random generator = new Random();
	private static Map<String, List<Vector2f>[]> itemLayerMap = new HashMap<String, List<Vector2f>[]>();

	protected String name;
	protected String musicName;
	protected float gravity;

	protected Picture background;
	protected Texture2D[] backgroundAnimation;

	protected ImageRaster[] collisionMaps;
	protected int currentMap;
	protected int previousMap;

	private List<Vector2f>[] itemLayers;
	private List<Vector2f> ledges;

	protected int totalFrames;
	protected float updateSpeed;
	protected int backgroundFrame;
	protected float updateCount;

	private List<IrregularObstacle> irregulars;

	public Stage(String name, float gravity, boolean isOmicron) {
		this(name, gravity, 0, 0, isOmicron);
	}

	public Stage(String name, float gravity) {
		this(name, gravity, 0, 0);
	}

	public Stage(String name, float gravity, float updateSpeed,
			int totalFrames, boolean isOmicron) {
		this(name, gravity, updateSpeed, totalFrames, 0, isOmicron);
	}

	public Stage(String name, float gravity, float updateSpeed, int totalFrames) {
		this(name, gravity, updateSpeed, totalFrames, 0, false);
		//System.out.println("name: " + this.collisionMaps[0].getWidth());
	}

	public Stage(String name, float gravity, float updateSpeed,
			int totalFrames, int totalMaps) {
		this(name, gravity, updateSpeed, totalFrames, totalMaps, false);
	}

	public Stage(String name, float gravity, float updateSpeed,
			int totalFrames, int totalMaps, boolean isOmicron) {
		this.name = name;
		if (isOmicron) {
			this.musicName = name.substring(0, name.length() - 2);
		} else {
			this.musicName = name;
		}
		this.gravity = gravity;

		this.irregulars = new ArrayList<IrregularObstacle>();

		this.background = Utilities.getJMELoader().getPicture("Background",
				"Sprites/Miscellaneous/blankSprite.png", 0, 0,
				PictureLayer.BACKGROUND);
		this.backgroundAnimation = new Texture2D[totalFrames + 1];

		for (int count = 0; count < this.backgroundAnimation.length; count++) {
			this.backgroundAnimation[count] = Utilities
					.getJMELoader()
					.getTexture(
							"Stages/"
									+ name
									+ (Main.getGameSettings().getDebugMode() ? "/CollisionMap_000.png"
											: "/Background_"
													+ Utilities
															.getGeneralUtility()
															.getNumberAsString(
																	count)
													+ ".png"));
		}

		Utilities.getJMELoader().changeTexture(this.background,
				this.backgroundAnimation[0]);

		this.collisionMaps = new ImageRaster[totalMaps + 1];

		for (int count = 0; count < this.collisionMaps.length; count++) {
			this.collisionMaps[count] = ImageRaster.create(Utilities
					.getJMELoader()
					.getTexture(
							"Stages/"
									+ name
									+ "/CollisionMap_"
									+ Utilities.getGeneralUtility()
											.getNumberAsString(count) + ".png")
					.getImage());
		}

		if (!itemLayerMap.containsKey(this.name)) {
			this.itemLayers = new LinkedList[this.collisionMaps.length];

			for (this.currentMap = 0; this.currentMap < this.itemLayers.length; this.currentMap++) {
				List<Vector2f> itemLayer = new LinkedList<Vector2f>();
				ImageRaster collisionMap = this.collisionMaps[this.currentMap];

				for (int x = 0; x < collisionMap.getWidth(); x++) {
					int y = 0;

					for (y = collisionMap.getHeight(); y > 0; y--) {
						if (this.complex2DCollision(x, y, 1)
								|| this.complex2DCollision(x, y, .5f)) {
							break;
						}
					}

					if (y != 0) {
						itemLayer.add(new Vector2f(x, y + 60));
					}
				}

				this.itemLayers[this.currentMap] = itemLayer;
			}

			itemLayerMap.put(this.name, this.itemLayers);
		} else {
			this.itemLayers = itemLayerMap.get(this.name);
		}

		this.currentMap = 0;

		this.totalFrames = totalFrames;
		this.updateSpeed = updateSpeed;
		this.backgroundFrame = 0;
		this.updateCount = 0;
		this.previousMap = -1;
		updateLedges();
	}
	
	public float scaleForRidley(){
		return (float) Math.sqrt(Math.min(getSizeX(), getSizeY())) / 29.02f;
	}

	public Vector2f getRandomItemPoint() {
		return this.itemLayers[this.currentMap].get(generator
				.nextInt(this.itemLayers[this.currentMap].size()));
	}

	public void addIrregular(IrregularObstacle i) {
		irregulars.add(i);
		List<Vector2f> itemLayer = itemLayers[0];
		for (int x = 0; x < getSizeX(); x+=8) {
			int y = 0;

			for (y = getSizeY(); y > 0; y-=4) {
				if (i.complex2DCollision(new Vector2f(x, y), 1)
						|| i.complex2DCollision(new Vector2f(x, y), .5f)) {
					break;
				}
			}

			if (y != 0) {
				itemLayer.add(new Vector2f(x, y + 60));
			}
		}
		itemLayers[0] = itemLayer;
		if (!(i instanceof MovingPlatform))
			updateLedges();
	}

	public void scrambleIrregulars() {
		Collections.shuffle(irregulars, new Random());
	}

	public List<IrregularObstacle> getIrregulars() {
		return irregulars;
	}

	public String getName() {
		return this.name;
	}

	public String getMusicName() {
		return this.musicName;
	}

	public int getSizeX() {
		return this.collisionMaps[this.currentMap].getWidth();
	}

	public int getSizeY() {
		return this.collisionMaps[this.currentMap].getHeight();
	}

	public void attachBackground(Node node) {
		node.attachChild(this.background);
	}

	public void update(float lockedTPF) {
		/*int x = (int) (Math.random()*getSizeX());
		int y = (int) (Math.random()*getSizeY());
		boolean bool = complex2DCollision(x, y, 1);
		if (bool == true){
			System.out.println(x + ", " + y + ": " + Main.getGameState().getWorld().getPlayerList().get(0).getPosition().distance(new Vector2f(x,y)));
		}*/
		irregulars.removeAll(Collections.singleton(null));
		List<IrregularObstacle> safeList = new ArrayList<IrregularObstacle>(
				this.irregulars);
		for (IrregularObstacle irr : safeList) {
			if (irr.isDead()) {
				irregulars.remove(irr);
			}
		}

		if (this.updateSpeed != 0) {
			if (this.updateCount >= this.updateSpeed) {
				this.updateCount -= this.updateSpeed;

				if (++this.backgroundFrame > this.totalFrames) {
					this.backgroundFrame = 0;
				}

				Utilities.getJMELoader().changeTexture(this.background,
						this.backgroundAnimation[this.backgroundFrame]);
			} else {
				this.updateCount += lockedTPF;
			}
		}
		// if (Math.random() < Utilities.lockedTPF * 800000
		// / (getSizeX() * getSizeY()))
		updateLedges();
		this.previousMap = this.currentMap;
	}

	public final float getGravity() {
		return this.gravity;
	}

	public boolean complex2DCollision(BoundingBox b, float value) {
		for (float x = b.getLowerLeft().x; x < b.getUpperRight().x; x++) {
			for (float y = b.getLowerLeft().y; y < b.getUpperRight().y; y++) {
				boolean n = complex2DCollision((int) x, (int) y, value);
				if (n == true)
					return true;
			}
		}
		return false;
	}

	/**
	 * Checks whether red value at the point is a precision of .02 to the
	 * required value. All values are from 0 to 1.
	 */
	public boolean complex2DCollision(Vector2f point, float value) {
		return complex2DCollision((int) point.x, (int) point.y, value);
	}

	/**
	 * Checks whether red value at the point is a precision of .02 to the
	 * required value. All values are from 0 to 1.
	 */
	private final boolean complex2DCollision(int x, int y, float value) {
		boolean returnValue = false;

		try {
			returnValue = Utilities.getGeneralUtility().compareFloats(
					this.collisionMaps[this.currentMap].getPixel(x, y)
							.getColorArray()[0], value, .02f);
		} catch (IllegalArgumentException e) {

		}

		return returnValue;
	}

	public boolean checkForLedge(Vector2f point) {
		return checkForLedge((int) point.x, (int) point.y);
	}

	private final boolean checkForLedge(int x, int y) {
		boolean returnValue = false;

		try {
			float[] arr = this.collisionMaps[this.currentMap].getPixel(x, y)
					.getColorArray();
			returnValue = Utilities.getGeneralUtility().compareFloats(
					arr[0] + arr[1] + arr[2], 3, .02f);
		} catch (IllegalArgumentException e) {

		}

		return returnValue;
	}

	private void updateLedges() {
		float leftX = getSizeX();
		float rightX = 0;
		float downY = getSizeY();
		float upY = 0;
		if (Main.getGameState() != null
				&& Main.getGameState().getWorld() != null
				&& Main.getGameState().getWorld().getPhysicsList() != null) {
			for (PhysicsEntity pc : Main.getGameState().getWorld()
					.getPhysicsList()) {
				if (pc.shouldCalculateLedge()){
				Vector2f vec = pc.getPosition();
				if (vec.x < leftX) {
					leftX = vec.x;
				}
				if (vec.x > rightX) {
					rightX = vec.x;
				}
				if (vec.y < downY) {
					downY = vec.y;
				}
				if (vec.y > upY) {
					upY = vec.y;
				}
				}
			}
			leftX -= 50;
			rightX += 50;
			downY -= 50;
			upY += 50;
		} else {
			leftX = 0;
			rightX = getSizeX();
			downY = 0;
			upY = getSizeY();
		}
		if (leftX < 0){
			leftX = 0;
		} if (rightX > getSizeX()){
			rightX = getSizeX();
		} if (downY < 0){
			downY = 0;
		} if (upY > getSizeY()){
			upY = getSizeY();
		}
		List<Vector2f> points = new LinkedList<Vector2f>();
		ledges = new ArrayList<Vector2f>();
		ImageRaster collisionMap = this.collisionMaps[this.currentMap];

		for (int x = (int) leftX; x < rightX; x++) {
			for (int y = (int) upY; y > downY; y--) {
				if (this.checkForLedge(x, y)) {
					points.add(new Vector2f(x, y));
				}
			}
		}

		/*for (IrregularObstacle i : irregulars) {
			if (i.getVelocity().lengthSquared() == 0) {
				float startx = i.getFullBounds().getLowerLeft().x;
				float endy = i.getFullBounds().getLowerLeft().y;
				float endx = i.getFullBounds().getUpperRight().x;
				float starty = i.getFullBounds().getUpperRight().y;
				if (leftX > startx)
					startx = leftX;
				if (downY > endy)
					endy = downY;
				if (rightX < endx)
					endx = rightX;
				if (upY < starty)
					starty = upY;
				for (int x = (int) startx; x < endx; x++) {
					for (int y = (int) starty; y > endy; y--) {
						if (i.checkForLedge(new Vector2f(x, y))) {
							points.add(new Vector2f(x, y));
						}
					}
				}
			}
		}*/

		for (Vector2f point : points) {
			ledges.add(point);
		}
	}

	public Vector2f getNearestLedge(Vector2f point) {
		Vector2f fin = null;
		float dist = Float.MAX_VALUE;
		for (Vector2f vec : ledges) {
			if (point.distanceSquared(vec) < dist) {
				dist = point.distanceSquared(vec);
				fin = vec;
			}
		}
		if (fin == null){
			fin = new Vector2f(point.x, point.y);
		}
		return fin;
	}

	public List<Vector2f> ledgesWithinDistance(Vector2f point, float distance) {
		List<Vector2f> fin = new ArrayList<Vector2f>();
		for (Vector2f vec : ledges) {
			if (point.distanceSquared(vec) < distance * distance * 3) {
				boolean direction = getLedgeDirection(vec);
				float dist = distance;
				if (point.x
						* Utilities.getGeneralUtility().getBooleanAsSign(
								!direction) < vec.x
						* Utilities.getGeneralUtility().getBooleanAsSign(
								!direction)) {
					dist = 0;
				}
				if (point.y < vec.y) {
					dist *= 1.5f;
				} else if (Math.abs(point.x - vec.x) < Math
						.abs(point.y - vec.y) * 2) {
					dist = 0;
				}
				/*
				 * if (Math.abs(point.x-vec.x) > distance/5){ dist = 0; }
				 */
				if (point.distance(vec) < dist) {
					fin.add(vec);
				}
			}
		}
		return fin;
	}

	public boolean getLedgeDirection(Vector2f ledge) {
		if (complex2DCollision(ledge.add(new Vector2f(1, 1)), 1)
				|| complex2DCollision(ledge.add(new Vector2f(1, 0)), 1)
				|| complex2DCollision(ledge.add(new Vector2f(1, -1)), 1)) {
			return true;
		}
		return false;
	}

	public abstract BoundingBox getCameraBounds();

	public abstract BoundingBox getWorldBounds();

	public abstract Vector2f getSpawnPoint();

	public abstract int getNumberOfSongs();

	public void cleanUpMatch() {

	}
}
