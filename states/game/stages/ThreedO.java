package states.game.stages;

import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;

import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import entities.general.ThreedOCarraige1;
import entities.general.ThreedOCarraige2;
import entities.general.ThreedOFace;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class ThreedO extends Stage {
    
	private boolean init;
	private ThreedOCarraige1 leftCar;
	private ThreedOCarraige1 rightCar;
	private ThreedOCarraige2 otherCar;
	private float counter;
	
    public ThreedO() {
        super("Threed O", 1, .5f, 0, true);
        init = false;
        counter = 0;
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(25, 6), new Vector2f(797, 598));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(13, 3), new Vector2f(819, 610));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(119, 140);
            case 1:
                return new Vector2f(246, 140);
            case 2:
                return new Vector2f(502, 332);
            case 3:
                return new Vector2f(758, 108);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 8;
    }
    
    @Override
    public void update(float lockedTPF) {
    	super.update(lockedTPF);
    	if (!init){
    		init = true;
    		ThreedOFace face = new ThreedOFace();
    		face.setPosition(522, 113);
    		Main.getGameState().spawnEntity(face);
    		
    		leftCar = new ThreedOCarraige1();
    		leftCar.setPosition(118, 34);
    		Main.getGameState().spawnEntity(leftCar);
    		this.addIrregular(leftCar);
    		
    		rightCar = new ThreedOCarraige1();
    		rightCar.setPosition(246, 34);
    		Main.getGameState().spawnEntity(rightCar);
    		this.addIrregular(rightCar);
    		
    		otherCar = new ThreedOCarraige2();
    		otherCar.setPosition(748, 4);
    		Main.getGameState().spawnEntity(otherCar);
    		this.addIrregular(otherCar);
    	}
    	counter += Utilities.lockedTPF;
    	if (leftCar != null && leftCar.isDead()){
    		leftCar = null;
    	}
    	if (rightCar != null && rightCar.isDead()){
    		rightCar = null;
    	}
    	if (otherCar != null && otherCar.isDead()){
    		otherCar = null;
    	}
    	if (counter > 5){
    		counter = 0;
    		double rand = Math.random();
    		if (rand < 0.15){
    			if (otherCar == null){
    				otherCar = new ThreedOCarraige2();
    	    		otherCar.setPosition(800, 4);
    	    		otherCar.setPureXVelocity(-10);
    	    		otherCar.limit = 748;
    	    		Main.getGameState().spawnEntity(otherCar);
    	    		this.addIrregular(otherCar);
    			} else {
    				otherCar.setPureXVelocity(10);
    			}
    		} else if (rand < 0.4){
    			if (leftCar == null && rightCar == null){
    				rand = Math.random();
    				rightCar = new ThreedOCarraige1();
					rightCar.setPosition(20, 34);
					rightCar.setPureXVelocity(15);
					rightCar.limit = 246;
    	    		Main.getGameState().spawnEntity(rightCar);
    	    		this.addIrregular(rightCar);
    				if (rand < 0.67){
    					leftCar = new ThreedOCarraige1();
    					leftCar.setPosition(20, 34);
    					leftCar.setPureXVelocity(10);
    					leftCar.limit = 118;
        	    		Main.getGameState().spawnEntity(leftCar);
        	    		this.addIrregular(leftCar);
    				}
    			} else if (leftCar == null){
    				rand = Math.random();
    				if (rand < 0.5){
    					rightCar.setPureXVelocity(-15);
    				} else {
    					leftCar = new ThreedOCarraige1();
    					leftCar.setPosition(20, 34);
    					leftCar.setPureXVelocity(10);
    					leftCar.limit = 118;
        	    		Main.getGameState().spawnEntity(leftCar);
        	    		this.addIrregular(leftCar);
    				}
    			} else {
    				rand = Math.random();
    				leftCar.setPureXVelocity(-10);
    				if (rand < 0.25){
    					rightCar.setPureXVelocity(-15);
    				}
    			}
    		}
    	}
    }
}
