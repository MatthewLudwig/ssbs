package states.game.stages;

import com.jme3.math.Vector2f;
import engine.Main;
import engine.utility.Utilities;
import java.util.logging.Level;
import physics.BoundingBox;

/**
 *
 * @author Matthew
 */
public class FrigateOrpheon extends Stage {
    private float randomCounter;
    private State state;
    private boolean direction;
    
    public FrigateOrpheon() {
        super("Frigate Orpheon", 1, .06f, 5);
        this.randomCounter = 0;
        this.state = State.STAY;
        this.direction = true;
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(70, 28), new Vector2f(604, 453));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(30, 19), new Vector2f(692, 558));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(216, 278);
            case 1:
                return new Vector2f(256, 190);
            case 2:
                return new Vector2f(416, 190);
            case 3:
                return new Vector2f(556, 190);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 4;
    }
    
    @Override
    public void update(float lockedTPF) {
        if (this.randomCounter >= 3) {
            this.randomCounter -= 3;
            this.state = State.values()[generator.nextInt(3)];
        } else {
            this.randomCounter += lockedTPF;
        }
        
        if (this.updateCount >= this.updateSpeed) {
            this.updateCount -= this.updateSpeed;

            switch (this.state) {
                case STAY:
                    return;
                case ANIMATEANDSTAY:
                    this.backgroundFrame += Utilities.getGeneralUtility().getBooleanAsSign(this.direction);
                    
                    if (this.backgroundFrame == Utilities.getGeneralUtility().getBooleanAsNumber(this.direction) * this.totalFrames) {
                        this.state = State.STAY;
                        this.direction = !this.direction;
                    }
                    
                    break;
                case ANIMATEANDBACK:
                    this.backgroundFrame += Utilities.getGeneralUtility().getBooleanAsSign(this.direction);
                    
                    if (this.backgroundFrame == Utilities.getGeneralUtility().getBooleanAsNumber(this.direction) * this.totalFrames) {
                        this.state = State.ANIMATEANDSTAY;
                        this.direction = !this.direction;
                    }
                    
                    break;
            }

            Utilities.getJMELoader().changeTexture(this.background, this.backgroundAnimation[this.backgroundFrame]);
        } else {
            this.updateCount += lockedTPF;
        }
    }
    
    private enum State {
        STAY(),
        ANIMATEANDSTAY(),
        ANIMATEANDBACK();
    }
}
