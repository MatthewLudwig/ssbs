package states.game.stages;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import physics.BoundingBox;

import com.jme3.math.Vector2f;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;

import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import entities.general.AcornPlainsOBlooper;
import entities.general.AcornPlainsOBulletBill;
import entities.general.AcornPlainsOBuzzyBeetle;
import entities.general.AcornPlainsOGoomba;
import entities.general.AcornPlainsOHammerBro;
import entities.general.AcornPlainsOKoopa;
import entities.general.AcornPlainsOLakitu;
import entities.general.AcornPlainsOLavaBubble;
import entities.general.AcornPlainsOParakoopa;
import entities.general.AcornPlainsOPiranhaPlant;

/**
 *
 * @author Matthew
 */
public class AcornPlainsO extends Stage {
    private Picture stationaryBackground;
    private Picture firstBackground;
    private Picture secondBackground;
    private Picture thirdBackground;
    private Picture fourthBackground;
    
    private float xOffset;
    private float incrementedOffset;
    private int currentIncrement;
    private final Vector2f[] piranhaSpawns = {new Vector2f(866, 118), new Vector2f(1211, 146), new Vector2f(2714, 118), new Vector2f(3523, 119), new Vector2f(4965, 292), new Vector2f(5113, 292), new Vector2f(6413, 117), new Vector2f(6700, 146), new Vector2f(6758, 118), new Vector2f(7191, 118)};
    private final float[] pitSpawns = {1456, 2006, 4216, 4533, 8246};
    
    private float counter;
    
    public AcornPlainsO() {
        super("Acorn Plains", 1);
        this.stationaryBackground = Utilities.getJMELoader().getPicture("Stationary", "/Stages/Acorn Plains/Background.png", 0, 0, PictureLayer.BACKGROUND);
        this.firstBackground = Utilities.getJMELoader().getPicture("First", "/Stages/Acorn Plains/Background_000.png", 0, 0, PictureLayer.BACKGROUND);
        this.secondBackground = Utilities.getJMELoader().getPicture("Second", "/Stages/Acorn Plains/Background_001.png", 2484, 0, PictureLayer.BACKGROUND);
        this.thirdBackground = Utilities.getJMELoader().getPicture("Third", "/Stages/Acorn Plains/Background_002.png", 4968, 0, PictureLayer.BACKGROUND);
        this.fourthBackground = Utilities.getJMELoader().getPicture("Fourth", "/Stages/Acorn Plains/Background_003.png", 7452, 0, PictureLayer.BACKGROUND);

        this.xOffset = 0;
        this.incrementedOffset = 0;
        this.currentIncrement = 0;
        
        this.counter = (float) (Math.random() + 0.1);
    }
    
    @Override
    public void attachBackground(Node node) {
        node.attachChild(this.stationaryBackground);
        node.attachChild(this.firstBackground);
        node.attachChild(this.secondBackground);
        node.attachChild(this.thirdBackground);
        node.attachChild(this.fourthBackground);
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(16 + xOffset, 13), new Vector2f(472, 354));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(3 + xOffset, 3), new Vector2f(499, 373));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(109, 54);
            case 1:
                return new Vector2f(204, 54);
            case 2:
                return new Vector2f(299, 54);
            case 3:
                return new Vector2f(394, 54);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 4;
    }
    
    @Override
    public void update(float lockedTPF) {
        if (!Main.getGameState().isSmashBallActive(false)) {
            this.xOffset += 40 * lockedTPF;
            this.stationaryBackground.setLocalTranslation(this.xOffset, 0, this.stationaryBackground.getLocalTranslation().z);

            this.incrementedOffset += 40 * lockedTPF;

            if (this.incrementedOffset > 2484) {
                switch (this.currentIncrement) {
                    case 0:
                        this.firstBackground.move(9936, 0, 0);
                        break;
                    case 1:
                        this.secondBackground.move(9936, 0, 0);
                        break;
                    case 2:
                        this.thirdBackground.move(9936, 0, 0);
                        break;
                    case 3:
                        this.fourthBackground.move(9936, 0, 0);
                        break;
                }

                this.currentIncrement++;

                if (this.currentIncrement == 4) {
                    this.currentIncrement = 0;
                }

                this.incrementedOffset = 0;
            }
            
            this.counter -= Utilities.lockedTPF;
            if (this.counter < 0){
            	this.counter = (float) (Math.random() + 0.1);
            	List<String> thingsToSpawn = new ArrayList<String>();
            	thingsToSpawn.add("Goomba");
            	thingsToSpawn.add("Koopa");
            	thingsToSpawn.add("Buzzy Beetle");
            	thingsToSpawn.add("Parakoopa");
            	thingsToSpawn.add("Bullet Bill");
            	thingsToSpawn.add("Hammer Bro");
            	thingsToSpawn.add("Lakitu");
            	for (int i = 0; i < 30; i++){
            		thingsToSpawn.add("Nothing");
            	}
            	
            	float xLeft = getCameraBounds().getUpperRight().x;
            	float xRight = getWorldBounds().getUpperRight().x;
            	float xMid = (xLeft+xRight)/2f;
            	float yRand = (float) (Math.random()*(getWorldBounds().getUpperRight().y - getCameraBounds().getLowerLeft().y) + getCameraBounds().getLowerLeft().y);
            	
            	for (int i = 0; i < piranhaSpawns.length; i++){
            		if (piranhaSpawns[i].x > xLeft && piranhaSpawns[i].x < xRight){
            			for (int k = 0; k < 30; k++){
            				thingsToSpawn.add("Piranha Plant");
            			}
            			//System.out.println("hm?");
            			break;
            		}
            	}
            	
            	for (int i = 0; i < pitSpawns.length; i++){
            		if (pitSpawns[i] + 10 > xLeft && pitSpawns[i] - 10 < xRight){
            			for (int k = 0; k < 40; k++){
            				thingsToSpawn.add("Lava Bubble");
            				thingsToSpawn.add("Blooper");
            			}
            			break;
            		}
            	}
            	
            	String s = thingsToSpawn.get((int) (thingsToSpawn.size()*Math.random()));
            	if (s.equals("Goomba")){
            		AcornPlainsOGoomba goomba = new AcornPlainsOGoomba();
            		goomba.setPosition(xMid, yRand);
            		Main.getGameState().spawnEntity(goomba);
            	} else if (s.equals("Koopa")){
            		AcornPlainsOKoopa koopa = new AcornPlainsOKoopa();
            		koopa.setPosition(xMid, yRand);
            		Main.getGameState().spawnEntity(koopa);
            	} else if (s.equals("Buzzy Beetle")){
            		AcornPlainsOBuzzyBeetle buzzy = new AcornPlainsOBuzzyBeetle();
            		buzzy.setPosition(xMid, yRand);
            		Main.getGameState().spawnEntity(buzzy);
            	} else if (s.equals("Bullet Bill")){
            		AcornPlainsOBulletBill bill = new AcornPlainsOBulletBill();
            		bill.setPosition(xMid, yRand);
            		Main.getGameState().spawnEntity(bill);
            	} else if (s.equals("Parakoopa")){
            		AcornPlainsOParakoopa para = new AcornPlainsOParakoopa();
            		para.setPosition(xMid, yRand);
            		Main.getGameState().spawnEntity(para);
            	} else if (s.equals("Lakitu")){
            		AcornPlainsOLakitu lakitu = new AcornPlainsOLakitu();
            		lakitu.setPosition(xMid, 280);
            		Main.getGameState().spawnEntity(lakitu);
            	} else if (s.equals("Hammer Bro")){
            		AcornPlainsOHammerBro hammer = new AcornPlainsOHammerBro();
            		hammer.setPosition(xMid, yRand);
            		Main.getGameState().spawnEntity(hammer);
            	} else if (s.equals("Piranha Plant")){
            		AcornPlainsOPiranhaPlant piranha = new AcornPlainsOPiranhaPlant();
            		for (int i = 0; i < piranhaSpawns.length; i++){
                		if (piranhaSpawns[i].x > xLeft && piranhaSpawns[i].x < xRight){
                			piranha.setPosition(piranhaSpawns[i].x, piranhaSpawns[i].y);
                			break;
                		}
                	}
            		Main.getGameState().spawnEntity(piranha);
            	} else if (s.equals("Lava Bubble")){
            		AcornPlainsOLavaBubble lava = new AcornPlainsOLavaBubble();
            		for (int i = 0; i < pitSpawns.length; i++){
                		if (pitSpawns[i] + 10 > xLeft && pitSpawns[i] - 10 < xRight){
                			lava.setPosition(pitSpawns[i], 20);
                			break;
                		}
                	}
            		Main.getGameState().spawnEntity(lava);
            	} else if (s.equals("Blooper")){
            		AcornPlainsOBlooper blooper = new AcornPlainsOBlooper();
            		for (int i = 0; i < pitSpawns.length; i++){
                		if (pitSpawns[i] + 10 > xLeft && pitSpawns[i] - 10 < xRight){
                			blooper.setPosition(pitSpawns[i], 20);
                			break;
                		}
                	}
            		Main.getGameState().spawnEntity(blooper);
            	}
            }
        }
    }
    
    @Override
    public boolean complex2DCollision(Vector2f point, float value) {
        Vector2f realPoint = point.clone();
        
        while (realPoint.x > 9936) {
            realPoint.subtractLocal(9936, 0);
        }
        
        return super.complex2DCollision(realPoint, value);
    }
}
