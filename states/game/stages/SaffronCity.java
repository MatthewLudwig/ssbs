package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Main;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class SaffronCity extends Stage {

    public SaffronCity() {
        super("Saffron City", 1);
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(114, 82), new Vector2f(664, 498));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(36, 53), new Vector2f(782, 544));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(209, 240);
            case 1:
                return new Vector2f(427, 327);
            case 2:
                return new Vector2f(564, 400);
            case 3:
                return new Vector2f(707, 364);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 6;
    }
}
