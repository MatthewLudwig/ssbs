package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Main;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class EmeraldHillZone extends Stage {

    public EmeraldHillZone() {
        super("Emerald Hill Zone", 1);
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(62, 1), new Vector2f(1113, 835));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(30, 1), new Vector2f(1169, 863));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(364, 264);
            case 1:
                return new Vector2f(877, 264);
            case 2:
                return new Vector2f(229, 115);
            case 3:
                return new Vector2f(993, 115);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 4;
    }
}
