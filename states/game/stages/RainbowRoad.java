package states.game.stages;

import com.jme3.math.Vector2f;
import engine.Main;
import entities.general.Block;
import entities.general.RainbowLoop;
import entities.general.Target;

import java.util.logging.Level;
import physics.BoundingBox;

/**
 * 
 * @author Matthew
 */
public class RainbowRoad extends Stage {
	private boolean initialized;

	public RainbowRoad() {
		super("Rainbow Road", 1, .1f, 0);
		initialized = false;
	}

	@Override
	public BoundingBox getWorldBounds() {
		return new BoundingBox(new Vector2f(59, 61), new Vector2f(943, 945));
	}

	@Override
	public BoundingBox getCameraBounds() {
		return new BoundingBox(new Vector2f(127, 127), new Vector2f(881, 881));
	}

	@Override
	public Vector2f getSpawnPoint() {
		switch (generator.nextInt(4)) {
		case 0:
			return new Vector2f(312, 761);
		case 1:
			return new Vector2f(398, 761);
		case 2:
			return new Vector2f(506, 761);
		case 3:
			return new Vector2f(608, 761);
		default:
			Main.log(Level.SEVERE, "Unknown spawn point generated!", null);
			return null;
		}
	}

	@Override
	public int getNumberOfSongs() {
		return 5;
	}

	public void update(float lockedTPF) {
		super.update(lockedTPF);

		if (!this.initialized) {
			RainbowLoop loop = new RainbowLoop();
			loop.setPosition(500, 200);
			Main.getGameState().spawnEntity(loop);
			addIrregular(loop);

			this.initialized = true;
		}
	}
}
