package states.game.stages;

import com.jme3.math.Vector2f;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;

import java.util.logging.Level;
import physics.BoundingBox;

/**
 * 
 * @author Matthew
 */
public class FrigateOrpheonO extends Stage {
	private float randomCounter;
	private float directionCounter;
	private State state;
	private boolean direction;
	private int section;
	private int firstFrame;
	private int lastFrame;

	public FrigateOrpheonO() {
		super("Frigate Orpheon O", 1, .06f, 23, 3, true);
		this.randomCounter = 0;
		this.directionCounter = 0;
		this.state = State.STAY;
		this.direction = true;
		this.section = 0;
		this.firstFrame = 0;
		this.lastFrame = 5;
	}

	@Override
	public BoundingBox getCameraBounds() {
		return new BoundingBox(new Vector2f(116, 116), new Vector2f(1049, 1049));
	}

	@Override
	public BoundingBox getWorldBounds() {
		return new BoundingBox(new Vector2f(58, 58), new Vector2f(1165, 1165));
	}

	@Override
	public Vector2f getSpawnPoint() {
		switch (generator.nextInt(4)) {
		case 0:
			return new Vector2f(467, 378);
		case 1:
			return new Vector2f(665, 281);
		case 2:
			return new Vector2f(536, 281);
		case 3:
			return new Vector2f(814, 281);
		default:
			Main.log(Level.SEVERE, "Unknown spawn point generated!", null);
			return null;
		}
	}

	@Override
	public int getNumberOfSongs() {
		return 4;
	}

	public void switchSection() {
		int sect = (int) (Math.random() * 4);
		int diff = (sect + 4 - section) % 4;
		if (diff == 1) {
			for (PhysicsEntity pc : Main.getGameState().getWorld()
					.getPhysicsList()) {
				Vector2f vec = new Vector2f(pc.getPosition().x,
						pc.getPosition().y);
				pc.setPosition(vec.y, vec.x);
			}
		} else if (diff == 2) {
			for (PhysicsEntity pc : Main.getGameState().getWorld()
					.getPhysicsList()) {
				Vector2f vec = new Vector2f(pc.getPosition().x,
						pc.getPosition().y);
				pc.setPosition(Main.getGameState().getWorld().getStage()
						.getSizeX()
						- vec.x, Main.getGameState().getWorld().getStage()
						.getSizeY()
						- vec.y);
			}
		} else if (diff == 3) {
			for (PhysicsEntity pc : Main.getGameState().getWorld()
					.getPhysicsList()) {
				Vector2f vec = new Vector2f(pc.getPosition().x,
						pc.getPosition().y);
				pc.setPosition(Main.getGameState().getWorld().getStage()
						.getSizeY()
						- vec.y, Main.getGameState().getWorld().getStage()
						.getSizeX()
						- vec.x);
			}
		}
		section = sect;
	}

	@Override
	public void update(float lockedTPF) {
		if (this.directionCounter >= 14) {
			this.directionCounter -= 14;
			switchSection();
		} else {
			this.directionCounter += Utilities.lockedTPF;
		}

		this.currentMap = section;
		if (section == 0) {
			this.firstFrame = 0;
			this.lastFrame = 5;
		} else if (section == 1) {
			this.firstFrame = 6;
			this.lastFrame = 11;
		} else if (section == 2) {
			this.firstFrame = 12;
			this.lastFrame = 17;
		} else if (section == 3) {
			this.firstFrame = 18;
			this.lastFrame = 23;
		}

		if (this.randomCounter >= 3) {
			this.randomCounter -= 3;
			this.state = State.values()[generator.nextInt(3)];
		} else {
			this.randomCounter += lockedTPF;
		}

		if (this.updateCount >= this.updateSpeed) {
			this.updateCount -= this.updateSpeed;

			switch (this.state) {
			case STAY:
				return;
			case ANIMATEANDSTAY:
				this.backgroundFrame += Utilities.getGeneralUtility()
						.getBooleanAsSign(this.direction);

				if (this.backgroundFrame == Utilities.getGeneralUtility()
						.getBooleanAsNumber(this.direction) * 5) {
					this.state = State.STAY;
					this.direction = !this.direction;
				}

				break;
			case ANIMATEANDBACK:
				this.backgroundFrame += Utilities.getGeneralUtility()
						.getBooleanAsSign(this.direction);

				if (this.backgroundFrame == Utilities.getGeneralUtility()
						.getBooleanAsNumber(this.direction) * 5) {
					this.state = State.ANIMATEANDSTAY;
					this.direction = !this.direction;
				}

				break;
			}
		} else {
			this.updateCount += lockedTPF;
		}
		Utilities.getJMELoader()
				.changeTexture(
						this.background,
						this.backgroundAnimation[this.backgroundFrame
								+ this.firstFrame]);
	}

	private enum State {
		STAY(), ANIMATEANDSTAY(), ANIMATEANDBACK();
	}
}
