package states.game.stages;

import com.jme3.math.Vector2f;
import com.jme3.texture.image.ImageRaster;

import engine.Main;
import engine.utility.Utilities;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class MuteCity extends Stage {
	private static final float UPDATESPEED = .1f;
	private static final float RAISINGSPEED = 200f;

	private float counter;
	private float offset;
	
    public MuteCity() {
        super("Mute City", 1, 0, 101, 5);
        this.offset = 0;
    }

    @Override
    public void update(float lockedTPF) {
        super.update(lockedTPF);
        
        if (this.counter >= 10) {
        	this.counter = 0;
        	this.offset = 250;
        	
        	if (++this.currentMap == this.collisionMaps.length) {
        		this.currentMap = 0;
        	}
        	
        	this.updateSpeed = UPDATESPEED;
        } else {
        	if (this.updateSpeed != 0) {
        		switch (this.currentMap) {
        			case 0:
        				if (this.backgroundFrame == 0) {
        					this.updateSpeed = 0;
        				}
        				break;
        			case 1:
        				if (this.backgroundFrame == 15) {
        					this.updateSpeed = 0;
        				}
        				break;
        			case 2:
        				if (this.backgroundFrame == 33) {
        					this.updateSpeed = 0;
        				}
        				break;
        			case 3:
        				if (this.backgroundFrame == 50) {
        					this.updateSpeed = 0;
        				}
        				break;
        			case 4:
        				if (this.backgroundFrame == 69) {
        					this.updateSpeed = 0;
        				}
        				break;
        			case 5:
        				if (this.backgroundFrame == 84) {
        					this.updateSpeed = 0;
        				}
        				break;
        		}
        	} else {
            	this.counter += Utilities.lockedTPF;
        	}
        	
        	if (this.offset != 0) {
        		this.offset -= RAISINGSPEED * Utilities.lockedTPF;
        		
        		if (this.offset < 0) {
        			this.offset = 0;
        		}
        	}
        }
    }
    
    @Override
    public boolean complex2DCollision(Vector2f point, float value) {
        return super.complex2DCollision(point.subtract(0, -this.offset), value);
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(30, 18), new Vector2f(576, 432));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(6, 18), new Vector2f(633, 441));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(165, 162);
            case 1:
                return new Vector2f(234, 162);
            case 2:
                return new Vector2f(303, 162);
            case 3:
                return new Vector2f(372, 162);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 4;
    }
}
