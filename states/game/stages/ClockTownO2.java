package states.game.stages;

import com.jme3.audio.AudioNode;
import com.jme3.math.Vector2f;

import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.general.ClockTown2Rain;
import entities.general.FinalPlatformO;
import entities.general.SkullKid;
import entities.general.Tingle;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class ClockTownO2 extends Stage {
	private boolean initialized;
	private AudioNode rainSound;
	private SkullKid skull;
	private Tingle tingle;
	private float counter2;
    public ClockTownO2() {
        super("Clock Town 2", 1, 1, 0, true);
        this.rainSound = Utilities.getCustomLoader().getAudioNode("/Common Sounds/rainstorm.ogg");
    	this.rainSound.setVolume(Main.getGameSettings().getSFXVolume()*0.25f);
    	counter2 = 0;
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(98, 38), new Vector2f(1452, 801));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(47, 23), new Vector2f(1548, 855));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(333, 154);
            case 1:
                return new Vector2f(640, 76);
            case 2:
                return new Vector2f(924, 67);
            case 3:
                return new Vector2f(1299, 175);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    public void cleanUpMatch(){
    	super.cleanUpMatch();
    	this.rainSound.setLooping(false);
    	this.rainSound.stop();
    }
    
    @Override
    public int getNumberOfSongs() {
        return 6;
    }
    
    public void update(float lockedTPF) {
    	super.update(lockedTPF);
    	if (!this.initialized) {
    		for (int i = -175; i <= this.getSizeX(); i += 350){
    			for (int k = 0; k < this.getSizeY(); k += 150){
    				ClockTown2Rain rain = new ClockTown2Rain();
    				rain.setPosition(i, k);
    				Main.getGameState().spawnEntity(rain);
    			}
    		}
    		this.rainSound.play();
    		this.rainSound.setLooping(true);
    		this.initialized = true;
    	}
    	for (PhysicsEntity e : Main.getGameState().getWorld().getPhysicsList()){
    		if (!(e instanceof ClockTown2Rain))
    			e.setPosition(e.getPosition().x+0.2f*((e.getPhysicsInfo().getWeight()-1)*-1+1), e.getPosition().y);
    	}
    	counter2 += Utilities.lockedTPF;
		if (skull != null && skull.isDead()) {
			skull = null;
		}
		if (tingle != null && tingle.isDead()) {
			tingle = null;
		}
		if (counter2 > 8.0f) {
			double rand = Math.random();
			if (rand < 1.0 / 3.0) {
				if (skull == null) {
					skull = new SkullKid();
					skull.setPosition(Main.getGameState().getCamera()
							.getCurrentPosition().x, Main.getGameState()
							.getCamera().getCurrentPosition().y + 40);
					Main.getGameState().spawnEntity(skull);
				}
			} else if (rand < 2.0 / 3.0) {
				if (tingle == null) {
					tingle = new Tingle();
					tingle.setPosition((float) (Math.random())
							* Main.getGameState().getWorld().getStage()
									.getSizeX()
							* 0.67f
							+ Main.getGameState().getWorld().getStage()
									.getSizeX() * 0.17f,
							(float) (Math.random())
									* Main.getGameState().getWorld().getStage()
											.getSizeY()
									* 0.50f
									+ Main.getGameState().getWorld().getStage()
											.getSizeY() * 0.17f);
					Main.getGameState().spawnEntity(tingle);
				}
			}
			counter2 = 0;
		}
    	
    }
}
