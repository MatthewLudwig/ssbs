package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Main;
import entities.PlayableCharacter;

import java.util.List;
import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 * 
 * @author Matthew
 */
public class JungleJapes extends Stage {

	public JungleJapes() {
		super("Jungle Japes", 1);
	}

	@Override
	public BoundingBox getCameraBounds() {
		return new BoundingBox(new Vector2f(158, 138), new Vector2f(735, 551));
	}

	@Override
	public BoundingBox getWorldBounds() {
		return new BoundingBox(new Vector2f(99, 122), new Vector2f(845, 592));
	}

	@Override
	public Vector2f getSpawnPoint() {
		switch (generator.nextInt(4)) {
		case 0:
			return new Vector2f(263, 353);
		case 1:
			return new Vector2f(524, 395);
		case 2:
			return new Vector2f(530, 329);
		case 3:
			return new Vector2f(791, 353);
		default:
			Main.log(Level.SEVERE, "Unknown spawn point generated!", null);
			return null;
		}
	}

	@Override
	public int getNumberOfSongs() {
		return 4;
	}

	@Override
	public void update(float lockedTPF) {
		List<PlayableCharacter> characters = Main.getGameState().getWorld()
				.getPlayerList();

		for (PlayableCharacter character : characters) {
			if (character.getAppropriateBounds().getExactCenter().y < 160) {
				character.setVelocity(-150, 0, false);
				if (!character.getDrowning()){
					character.startDrowning();
				}
			}
		}
	}
}
