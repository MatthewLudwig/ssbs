package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Main;
import entities.general.DarkLink;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class TempleO extends Stage {

	private boolean init;
	private DarkLink[] darkLinks;
	
    public TempleO() {
        super("Temple O", 1, true);
        init = false;
        darkLinks = new DarkLink[5];
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(170, 223), new Vector2f(2372, 1612));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(39, 39), new Vector2f(2652, 2069));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(530,1366);
            case 1:
                return new Vector2f(1200,1273);
            case 2:
                return new Vector2f(2094,1141);
            case 3:
                return new Vector2f(983,811);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 4;
    }
    
    public void update(float lockedTPF){
    	super.update(lockedTPF);
    	for (int i = 0; i < darkLinks.length; i++){
    		if ((darkLinks[i] == null || darkLinks[i].isDead()) && Math.random() < lockedTPF/15f){
    			darkLinks[i] = new DarkLink();
    			double rand = Math.random();
    			if (rand < 0.25){
    				darkLinks[i].setPosition(530,1366);
    			} else if (rand < 0.5){
    				darkLinks[i].setPosition(1200,1273);
    			} else if (rand < 0.75){
    				darkLinks[i].setPosition(2094,1141);
    			} else {
    				darkLinks[i].setPosition(983,811);
    			}
    			Main.getGameState().spawnEntity(darkLinks[i]);
    		}
    	}
    }
}
