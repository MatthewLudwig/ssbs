package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Document;
import engine.Main;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author Matthew
 */
public class TargetTestFormat {
    private Vector2f size;
    private Vector2f spawn;
    private List<Vector2f> blocks;
    private List<Vector2f> targets;
    
    public TargetTestFormat(String path) {
        Document doc = Document.loadFromAssets("/Stages/Target Test/Formats/" + path + ".txt");
        
        this.blocks = new LinkedList<Vector2f>();
        this.targets = new LinkedList<Vector2f>();
                
        boolean sizeToggle = false;
        boolean spawnToggle = false;
        boolean blockToggle = false;
        boolean targetToggle = false; //Change
        
        for (String line : doc.getLines()) {
        	if (!line.isEmpty()) {
        		if (line.contains("SIZE") && !line.equals("SIZE")) {
        			line = "SIZE";
        		}
        		
        		if (line.equals("SIZE")) {
        			sizeToggle = true;
        			spawnToggle = false;
        			blockToggle = false;
        			targetToggle = false;
        		} else if (line.equals("SPAWN")) {
        			sizeToggle = false;
        			spawnToggle = true;
        			blockToggle = false;
        			targetToggle = false;
        		} else if (line.equals("BLOCK")) {
        			sizeToggle = false;
        			spawnToggle = false;
        			blockToggle = true;
        			targetToggle = false;
        		} else if (line.equals("TARGET")) {
        			sizeToggle = false;
        			spawnToggle = false;
        			blockToggle = false;
        			targetToggle = true;
        		} else if (sizeToggle) {
        			this.size = new Vector2f(Integer.parseInt(line.split(" ")[0]), Integer.parseInt(line.split(" ")[1]));
        		} else if (spawnToggle) {
        			this.spawn = new Vector2f(Integer.parseInt(line.split(" ")[0]) + 16, this.size.y - Integer.parseInt(line.split(" ")[1]) - 32);
        		} else if (blockToggle) {
        			this.blocks.add(new Vector2f(Integer.parseInt(line.split(" ")[0]) + 16, this.size.y - Integer.parseInt(line.split(" ")[1]) - 32));
        		} else if (targetToggle) {
        			this.targets.add(new Vector2f(Integer.parseInt(line.split(" ")[0]) + 16, this.size.y - Integer.parseInt(line.split(" ")[1]) - 32));
        		} else {
        			Main.log(Level.SEVERE, "There is a mysterious symbol blocking me from completing my job!", null);
        		}
        	}
        }
    }
    
    public Vector2f getSize() {
        return this.size;
    }
    
    public Vector2f getSpawn() {
        return this.spawn;
    }
    
    public List<Vector2f> getBlocks() {
        return this.blocks;
    }
    
    public List<Vector2f> getTargets() {
        return this.targets;
    }
}
