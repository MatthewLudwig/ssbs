package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Main;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class ClockTown extends Stage {
    public ClockTown() {
        super("Clock Town", 1, 1, 0, false);
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(98, 38), new Vector2f(1452, 801));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(47, 23), new Vector2f(1548, 855));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(333, 154);
            case 1:
                return new Vector2f(640, 76);
            case 2:
                return new Vector2f(924, 67);
            case 3:
                return new Vector2f(1299, 175);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 6;
    }
}
