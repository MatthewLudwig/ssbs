package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Main;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class Temple extends Stage {

    public Temple() {
        super("Temple", 1);
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(150, 121), new Vector2f(1508, 1131));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(22, 24), new Vector2f(1769, 1379));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(353, 911);
            case 1:
                return new Vector2f(800, 849);
            case 2:
                return new Vector2f(1396, 761);
            case 3:
                return new Vector2f(655, 541);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 4;
    }
}
