package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Main;
import entities.general.MiiMakerEyebrow;
import entities.general.MiiMakerMovingPlatform;
import entities.general.MiiMakerRandomHead;
import entities.general.MiiMakerSlider;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class MiiMaker extends Stage {
	
	private boolean init;
	private MiiMakerSlider[] sliders;
	private float sliderCount;
	private MiiMakerEyebrow browLeft;
	private MiiMakerEyebrow browRight;
	
    public MiiMaker() {
        super("Mii Maker", 1);
        init = false;
        sliders = new MiiMakerSlider[5];
        sliderCount = (float) (Math.random()*10+2);
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(355, 315), new Vector2f(1417, 1330));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(169, 149), new Vector2f(1789, 1557));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(478, 828);
            case 1:
                return new Vector2f(844,674);
            case 2:
                return new Vector2f(1312,674);
            case 3:
                return new Vector2f(1658,828);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    public void update(float lockedTPF){
    	super.update(lockedTPF);
    	if (!init){
    		init = true;
    		MiiMakerMovingPlatform plat = new MiiMakerMovingPlatform();
    		plat.setPosition(900, 350);
    		Main.getGameState().spawnEntity(plat);
    		addIrregular(plat);
    		
    		MiiMakerRandomHead face = new MiiMakerRandomHead();
    		face.setPosition(594, 920);
    		Main.getGameState().spawnEntity(face);
    		
    		for (int i = 0; i < sliders.length; i++){
    			sliders[i] = new MiiMakerSlider();
    			sliders[i].setPosition(1179, 732 + i*130);
    			Main.getGameState().spawnEntity(sliders[i]);
    			addIrregular(sliders[i]);
    		}
    		
    		browLeft = new MiiMakerEyebrow();
    		browLeft.setPosition(596, 1054);
    		browLeft.setFacing(false);
    		Main.getGameState().spawnEntity(browLeft);
    		addIrregular(browLeft);
    		
    		browRight = new MiiMakerEyebrow();
    		browRight.setPosition(596, 1054);
    		browRight.setFacing(true);
    		Main.getGameState().spawnEntity(browRight);
    		addIrregular(browRight);
    	}
    	sliderCount -= lockedTPF;
    	if (sliderCount < 0){
    		sliderCount = (float) (Math.random()*10+2);
    		for (int i = 0; i < sliders.length; i++){
    			sliders[i].stopMoving();
    		}
    		int index = (int) (Math.random()*(sliders.length+1));
    		if (index < sliders.length)
    			sliders[index].setMoving();
    	}
    	
    	for (int i = 0; i < sliders.length; i++){
    		float f = (sliders[i].getPosition().x-964)/425f;
    		if (i == 4){
    			browLeft.setYDisp(f);
    			browRight.setYDisp(f);
    		} else if (i == 3){
    			browLeft.setXDisp(f);
    			browRight.setXDisp(f);
    		} else if (i == 2){
    			browLeft.setAngle(f);
    			browRight.setAngle(f);
    		} else if (i == 1){
    			browLeft.setScaling(f);
    			browRight.setScaling(f);
    		} else if (i == 0){
    			browLeft.setYScaling(f);
    			browRight.setYScaling(f);
    		}
    	}
    }
    
    @Override
    public int getNumberOfSongs() {
        return 4;
    }
}
