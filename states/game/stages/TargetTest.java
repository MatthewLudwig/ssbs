package states.game.stages;

import com.jme3.math.Vector2f;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture2D;
import com.jme3.texture.image.ImageRaster;
import com.jme3.texture.plugins.AWTLoader;
import engine.Main;
import engine.utility.Utilities;
import entities.general.Block;
import entities.general.Target;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import javax.imageio.ImageIO;
import physics.BoundingBox;
import engine.ssbs.CharacterList.CharacterMapping;

/**
 * 
 * @author Matthew
 */
public class TargetTest extends Stage {
	protected static final AWTLoader loader = new AWTLoader();

	private TargetTestFormat format;
	private boolean initialized;

	private String franchiseName;

	public TargetTest(CharacterMapping character) {
		super("Target Test/" + character.getAppropriateFranchise(), 1, 0.1f, 0);
		
		this.franchiseName = character.getAppropriateFranchise();
		this.format = new TargetTestFormat(character.getName());

		try {
			File folder = new File("assets/Stages/Target Test/" + this.franchiseName
					+ "/");
			String[] backs = folder.list(new NameFilter("Background_"));
			//System.out.println(backs.length);
			Texture2D textures[] = new Texture2D[backs.length];
			for (int i = 0; i < backs.length; i++) {
				String s = "00";
				if (i >= 10)
					s = "0";
				if (i >= 100)
					s = "";
				//System.out.println(s + i);
				BufferedImage image = Utilities.getGeneralUtility()
						.getScaledImage(
								ImageIO.read(Main
										.getGameSettings()
										.getClass()
										.getResourceAsStream(
												"/Stages/Target Test/"
														+ this.franchiseName
														+ "/Background_" + s
														+ i + ".png")),
								(int) this.format.getSize().x,
								(int) this.format.getSize().y);
				textures[i] = new Texture2D(loader.load(image, true));
				textures[i].setMagFilter(Texture.MagFilter.Nearest);
				textures[i].setMinFilter(Texture.MinFilter.NearestNoMipMaps);
			}

			BufferedImage collisionImage = Utilities
					.getGeneralUtility()
					.getScaledImage(
							ImageIO.read(Main
									.getGameSettings()
									.getClass()
									.getResourceAsStream(
											"/Stages/Target Test/"
													+ this.franchiseName
													+ "/CollisionMap_000.png")),
							(int) this.format.getSize().x,
							(int) this.format.getSize().y);
			Texture2D collisionTexture = new Texture2D(loader.load(
					collisionImage, true));
			collisionTexture.setMagFilter(Texture.MagFilter.Nearest);
			collisionTexture.setMinFilter(Texture.MinFilter.NearestNoMipMaps);

			this.totalFrames = textures.length-1;
			this.backgroundAnimation = textures;
			Utilities.getJMELoader().changeTexture(this.background,
					this.backgroundAnimation[0]);
			this.collisionMaps[0] = ImageRaster.create(collisionTexture
					.getImage());
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.initialized = false;
	}

	@Override
	public BoundingBox getCameraBounds() {
		return new BoundingBox(new Vector2f(0, 0), this.format.getSize());
	}

	@Override
	public BoundingBox getWorldBounds() {
		return new BoundingBox(new Vector2f(0, 0), this.format.getSize());
	}

	@Override
	public Vector2f getSpawnPoint() {
		return this.format.getSpawn();
	}

	@Override
	public int getNumberOfSongs() {
		return 1;
	}

	@Override
	public void update(float lockedTPF) {
		super.update(lockedTPF);

		if (!this.initialized) {
			for (Vector2f position : this.format.getBlocks()) {
				Block block = new Block(this.franchiseName);
				block.setPosition(position.x, position.y);
				Main.getGameState().spawnEntity(block);
			}

			for (Vector2f position : this.format.getTargets()) {
				Target target = new Target();
				target.setPosition(position.x, position.y);
				Main.getGameState().spawnEntity(target);
			}

			this.initialized = true;
		}
	}

	public class NameFilter implements FilenameFilter {

		String myName;

		public NameFilter(String name) {
			myName = name;
		}

		public boolean accept(File dir, String name) {
			return (name.startsWith(myName));
		}

	}
}
