package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Main;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class FinalDestination extends Stage {
    public FinalDestination() {
        super("Final Destination", 1, .1f, 17);
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(193, 163), new Vector2f(684, 513));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(97, 77), new Vector2f(873, 683));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(395, 389);
            case 1:
                return new Vector2f(495, 389);
            case 2:
                return new Vector2f(585, 389);
            case 3:
                return new Vector2f(685, 389);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 4;
    }
}
