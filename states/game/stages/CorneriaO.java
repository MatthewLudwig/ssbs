package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Main;
import engine.utility.Utilities;
import entities.MovingPlatform;
import entities.ViolentEntity;
import entities.general.CorneriaOArwing;
import entities.general.CorneriaOGreatFoxLaser;
import entities.general.CorneriaOWolfen;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class CorneriaO extends Stage {
	private MovingPlatform[] arwings;
	private float counter;

    public CorneriaO() {
        super("Corneria O", 1, true);
        counter = 0;
        arwings = new MovingPlatform[3];
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(66, 125), new Vector2f(1070, 650));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(25, 54), new Vector2f(1151, 799));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(255, 523);
            case 1:
                return new Vector2f(485, 507);
            case 2:
                return new Vector2f(797, 613);
            case 3:
                return new Vector2f(969, 465);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 6;
    }
    
    public void update(float lockedTPF){
    	super.update(lockedTPF);
    	counter += Utilities.lockedTPF;
    	boolean bool = true;
    	for (int i = 0; i < arwings.length; i++){
    		if (arwings[i] != null){
    			if (arwings[i].isDead()){
    				arwings[i] = null;
    			} else {
    				bool = false;
    			}
    		}
    	}
    	if (counter > 3){
    		counter = 0;
    		double rand = Math.random();
    		if (rand < 0.1){
    			CorneriaOGreatFoxLaser laser = new CorneriaOGreatFoxLaser();
    			laser.setPosition(299, 371);
    			Main.getGameState().spawnEntity(laser);
    		} else if (rand < 0.2 || !bool){
    			//nothing!
    		} else if (rand < 0.7) {
    			for (int i = 0; i < arwings.length; i++){
    				if (arwings[i] == null){
    					arwings[i] = new CorneriaOArwing();
    					arwings[i].setPosition(getCameraBounds().getUpperRight().x, (float) (Math.random()*131 + 533));
    					Main.getGameState().spawnEntity(arwings[i]);
    					addIrregular(arwings[i]);
    					break;
    				}
    			}
    		} else {
    			for (int i = 0; i < arwings.length; i++){
    				if (arwings[i] == null){
    					arwings[i] = new CorneriaOWolfen();
    					arwings[i].setPosition(getCameraBounds().getUpperRight().x, (float) (Math.random()*131 + 533));
    					Main.getGameState().spawnEntity(arwings[i]);
    					addIrregular(arwings[i]);
    					break;
    				}
    			}
    		}
    	}
    }
}
