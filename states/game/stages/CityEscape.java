package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import entities.PlayableCharacter;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class CityEscape extends Stage {
    public CityEscape() {
        super("City Escape", 1, 0.15f, 6);
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(120, 152), new Vector2f(1110, 781));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(51, 51), new Vector2f(1242, 932));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(570, 454);
            case 1:
                return new Vector2f(846, 444);
            case 2:
                return new Vector2f(705, 640);
            case 3:
                return new Vector2f(741, 771);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 4;
    }
    
    public void update (float lockedTPF){
    	super.update(lockedTPF);
    	float dmg = 14;
    	float knbk = 1;
    	for (PlayableCharacter pc : Main.getGameState().getWorld().getPlayerList()){
    		Vector2f vec = pc.getPosition();
    		if (vec.y < 357){
    			pc.addToDamage(dmg, knbk, 1, new Angle(90), false, false, false, false, 0, null);
    		} else if (vec.x < 335){
    			pc.addToDamage(dmg, knbk, 1, new Angle(0), false, false, false, false, 0, null);
    		} else if (vec.x > 1028){
    			pc.addToDamage(dmg, knbk, 1, new Angle(180), false, false, false, false, 0, null);
    		}
    	}
    }
}
