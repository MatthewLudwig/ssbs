package states.game.stages;

import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;

import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class Threed extends Stage {
    public Picture face;
    public float alpha;
    
    public Threed() {
        super("Threed", 1, .5f, 0);
        this.face = Utilities.getJMELoader().getPicture("Face", "/Stages/Threed/threedFace.png", 462, 173, PictureLayer.GUI);
        this.alpha = 0;
        
        Material mat = this.face.getMaterial();
        mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
        this.face.setMaterial(mat);

        this.updateFace();
    }
    
    @Override
    public void attachBackground(Node node) {
        super.attachBackground(node);
        node.attachChild(this.face);
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(25, 6), new Vector2f(797, 598));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(13, 3), new Vector2f(819, 610));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(119, 140);
            case 1:
                return new Vector2f(246, 140);
            case 2:
                return new Vector2f(502, 332);
            case 3:
                return new Vector2f(758, 108);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 8;
    }
    
    @Override
    public void update(float lockedTPF) {
        if (this.updateCount >= this.updateSpeed) {
            this.alpha += Utilities.getGeneralUtility().getBooleanAsSign(generator.nextBoolean()) * (generator.nextFloat() / 10);
            
            if (this.alpha < 0) {
                this.alpha = 0;
            } else if (this.alpha > 1) {
                this.alpha = 1;
            }
            
            this.updateFace();
            
            this.updateCount -= this.updateSpeed;
        } else {
            this.updateCount += lockedTPF;
        }
    }
    
    public final void updateFace() {
        Material mat = this.face.getMaterial();
        mat.setColor("Color", new ColorRGBA(1, 1, 1, alpha));
        this.face.setMaterial(mat);
    }
}
