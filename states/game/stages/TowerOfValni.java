package states.game.stages;

import com.jme3.math.Vector2f;
import engine.Main;
import entities.Team;
import entities.general.EmblemMonster;
import entities.general.RainbowLoop;

import java.util.logging.Level;
import physics.BoundingBox;

/**
 *
 * @author Matthew
 */
public class TowerOfValni extends Stage {
	int time;
	
    public TowerOfValni() {
        super("Tower Of Valni", 1, .1f, 0);
        this.time = 0;
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(52, 112), new Vector2f(1096, 1226));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(13, 32), new Vector2f(1173, 1376));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(281, 801);
            case 1:
                return new Vector2f(441, 721);
            case 2:
                return new Vector2f(762, 721);
            case 3:
                return new Vector2f(920, 801);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 8;
    }
    
    public void update(float lockedTPF) {
		super.update(lockedTPF);
		this.time++;

		if (this.time >= 12.0/lockedTPF) {
			EmblemMonster monster = new EmblemMonster();
			monster.team = Team.NONE;
			monster.setPosition(50, 838);
			Main.getGameState().spawnEntity(monster);

			this.time = 0;
		}
	}
}
