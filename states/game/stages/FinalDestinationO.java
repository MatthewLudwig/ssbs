package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Main;
import engine.utility.Utilities;
import entities.general.FinalPlatformO;
import entities.general.RainbowLoop;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 * 
 * @author Matthew
 */
public class FinalDestinationO extends Stage {
	private boolean initialized;
	private List<FinalPlatformO> platforms;
	private List<Float> counters;
	private List<Vector2f> positions;

	public FinalDestinationO() {
		super("Final Destination O", 1, .1f, 17, true);
		initialized = false;
		platforms = new ArrayList<FinalPlatformO>();
		counters = new ArrayList<Float>();
		positions = new ArrayList<Vector2f>();
	}

	@Override
	public BoundingBox getCameraBounds() {
		return new BoundingBox(new Vector2f(116, 92), new Vector2f(1048, 820));
	}

	@Override
	public BoundingBox getWorldBounds() {
		return new BoundingBox(new Vector2f(54, 46), new Vector2f(1172, 882));
	}

	@Override
	public Vector2f getSpawnPoint() {
		switch (generator.nextInt(4)) {
		case 0:
			return new Vector2f(300 - 30, 722 - 200);
		case 1:
			return new Vector2f(442 + 20, 542 - 100);
		case 2:
			return new Vector2f(632 + 50, 542 - 100);
		case 3:
			return new Vector2f(772 + 110, 722 - 200);
		default:
			Main.log(Level.SEVERE, "Unknown spawn point generated!", null);
			return null;
		}
	}

	@Override
	public int getNumberOfSongs() {
		return 4;
	}

	public void update(float lockedTPF) {
		super.update(lockedTPF);

		if (!this.initialized) {
			FinalPlatformO plat = new FinalPlatformO();
			plat.setPosition(209 + 63, 785 - 300);
			Main.getGameState().spawnEntity(plat);
			addIrregular(plat);
			platforms.add(plat);
			positions.add(new Vector2f(plat.getPosition().x, plat.getPosition().y));
			counters.add((float) (10 + Math.random() * 2 - 1));
			
			plat = new FinalPlatformO();
			plat.setPosition(349 + 114, 605 - 200);
			Main.getGameState().spawnEntity(plat);
			addIrregular(plat);
			platforms.add(plat);
			positions.add(new Vector2f(plat.getPosition().x, plat.getPosition().y));
			counters.add((float) (10 + Math.random() * 2 - 1));

			plat = new FinalPlatformO();
			plat.setPosition(539 + 146, 605 - 200);
			Main.getGameState().spawnEntity(plat);
			addIrregular(plat);
			platforms.add(plat);
			positions.add(new Vector2f(plat.getPosition().x, plat.getPosition().y));
			counters.add((float) (10 + Math.random() * 2 - 1));

			plat = new FinalPlatformO();
			plat.setPosition(679 + 197, 785 - 300);
			Main.getGameState().spawnEntity(plat);
			addIrregular(plat);
			platforms.add(plat);
			positions.add(new Vector2f(plat.getPosition().x, plat.getPosition().y));
			counters.add((float) (10 + Math.random() * 2 - 1));
			// scrambleIrregulars();

			this.initialized = true;
		} else {
			for (int i = 0; i < platforms.size(); i++) {
				//System.out.println("hey");
				if (platforms.get(i) != null
						&& platforms.get(i).getPosition().y > positions.get(i).y) {
					platforms.get(i).setPureYVelocity(0);
					platforms.get(i).setPosition(positions.get(i).x,
							positions.get(i).y);
				} else if (platforms.get(i) != null && platforms.get(i).getPosition().y < 47){
					platforms.get(i).setDead(true);
					//System.out.println(platforms.get(i).getDisplay().getParent().hasChild(platforms.get(i).getDisplay()));
					platforms.set(i,  null);
				}
			}

			for (int i = 0; i < counters.size(); i++) {
				//System.out.println("no");
				counters.set(i, counters.get(i) - Utilities.lockedTPF);
				if (counters.get(i) <= 0) {
					FinalPlatformO plat = platforms.get(i);
					if (plat == null || plat.isDead()) {
						//System.out.println("hm?");
						plat = new FinalPlatformO();
						plat.setPosition(positions.get(i).x, 47);
						plat.setPureYVelocity(15);
						Main.getGameState().spawnEntity(plat);
						addIrregular(plat);
						platforms.set(i, plat);
						counters.set(i, (float) (10 + Math.random() * 2 - 1));
					} else {
						boolean b = false;
						for (FinalPlatformO p : platforms) {
							if (p != null && p.getVelocity().length() == 0 && !p.isOnFire()) {
								b = true;
								break;
							}
						}
						if (b) {
							double rand = Math.random();
							if (rand < 1.0 / 3.0) {
								plat.setPureYVelocity(-20);
								counters.set(i, 12f);
							} else if (rand < 2.0 / 3.0) {
								plat.setFire();
								counters.set(i,
										(float) (10 + Math.random() * 2 - 1));
							} else {
								counters.set(i,
										(float) (10 + Math.random() * 2 - 1));
							}
						}
					}
				}
			}
		}
		//System.out.println("Update: " + (System.nanoTime() - time));
	}
}
