package states.game.stages;

import com.jme3.math.Vector2f;
import engine.Main;
import java.util.logging.Level;
import physics.BoundingBox;

/**
 *
 * @author Matthew
 */
public class CastleDedede extends Stage {
    public CastleDedede() {
        super("Castle Dedede", 1, .1f, 3);
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(46, 19), new Vector2f(435, 326));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(3, 3), new Vector2f(521, 387));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(86, 120);
            case 1:
                return new Vector2f(166, 57);
            case 2:
                return new Vector2f(366, 57);
            case 3:
                return new Vector2f(444, 120);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 5;
    }
}
