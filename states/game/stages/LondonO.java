package states.game.stages;

import com.jme3.math.Vector2f;

import engine.Main;
import engine.utility.Utilities;
import entities.general.LondonMovingPlatform;
import entities.general.LondonODonPaolo;
import entities.general.LondonOFerrisWheel;
import entities.general.LondonOMovingPlatform;

import java.util.logging.Level;

import physics.BoundingBox;
import static states.game.stages.Stage.*;

/**
 *
 * @author Matthew
 */
public class LondonO extends Stage {
	
	private boolean init;
	private LondonOFerrisWheel wheel;
	private LondonODonPaolo don;
	private float timer;
	
    public LondonO() {
        super("London", 1);
        init = false;
        timer = 0;
    }
    
    @Override
    public BoundingBox getCameraBounds() {
        return new BoundingBox(new Vector2f(64, 64), new Vector2f(980, 475));
    }
    
    @Override
    public BoundingBox getWorldBounds() {
        return new BoundingBox(new Vector2f(32, 32), new Vector2f(1050, 550));
    }
    
    @Override
    public Vector2f getSpawnPoint() {
        switch (generator.nextInt(4)) {
            case 0:
                return new Vector2f(379, 299);
            case 1:
                return new Vector2f(570, 257);
            case 2:
                return new Vector2f(631, 257);
            case 3:
                return new Vector2f(758, 276);
            default:
                Main.log(Level.SEVERE, "Unknown spawn point generated!", null); 
                return null;
        }
    }
    
    @Override
    public int getNumberOfSongs() {
        return 4;
    }
    
    public void update(float lockedTPF){
    	super.update(lockedTPF);
    	if (!init){
    		init = true;
    		LondonOMovingPlatform platform = new LondonOMovingPlatform();
    		platform.setPosition(platform.getStart().x, platform.getStart().y);
    		Main.getGameState().spawnEntity(platform);
    		addIrregular(platform);
    	}
    	timer += Utilities.lockedTPF;
    	if (timer > 3f){
    		timer = 0;
    		double rand = Math.random();
    		if (rand < 0.33){
    			if (wheel == null || wheel.isDead()){
    				wheel = new LondonOFerrisWheel();
    				wheel.setPosition(1000, 650);
    				Main.getGameState().spawnEntity(wheel);
    			}
    		} else if (rand < 0.67){
    			if (don == null || don.isDead()){
    				don = new LondonODonPaolo();
    				don.setPosition(200, 200);
    				Main.getGameState().spawnEntity(don);
    			}
    		}
    	}
    }
}
