package states.game;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import physics.BoundingBox;
import physics.CollisionInfo;
import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.utility.Utilities;
import entities.Obstacle;
import entities.PhysicsEntity;
import entities.ViolentEntity;

/**
 *
 * @author Matthew
 */
public class Chunk {
    private BoundingBox chunkBounds;
    private List<PhysicsEntity> obstacleList;
    private List<PhysicsEntity> entityList;

    public Chunk(int startX, int startY, int sizeX, int sizeY) {
        this.chunkBounds = new BoundingBox(new Vector2f(startX, startY), new Vector2f(sizeX, sizeY));
        this.obstacleList = new LinkedList<PhysicsEntity>();
        this.entityList = new LinkedList<PhysicsEntity>();
    }

    public void addEntity(PhysicsEntity entity) {
        if (entity instanceof Obstacle) {
            this.obstacleList.add(entity);
        } else {
            this.entityList.add(entity);
        }
    }
    
    public int getNumberOfEntities() {
        return this.entityList.size() + this.obstacleList.size();
    }

    public List<Chunk> splitChunk(int sizeLimit) {
        List<Chunk> chunkList = new LinkedList<Chunk>();

        if (this.getNumberOfEntities() > sizeLimit) {
            Chunk[] chunks = this.splitChunk();
            
            for (Chunk chunk : chunks) {
                chunkList.addAll(chunk.splitChunk(sizeLimit));
            }
        } else {
            chunkList.add(this);
        }

        return chunkList;
    }
    
    public Chunk[] splitChunk() {
        int startX = Math.round(this.chunkBounds.getLowerLeft().x);
        int startY = Math.round(this.chunkBounds.getLowerLeft().y);
        int halfX = Math.round(this.chunkBounds.getDimensions().x / 2);
        int halfY = Math.round(this.chunkBounds.getDimensions().y / 2);

        Chunk[] chunks = new Chunk[4];
        
        chunks[0] = new Chunk(startX, startY, halfX, halfY);
        chunks[1] = new Chunk(startX + halfX, startY, halfX, halfY);
        chunks[2] = new Chunk(startX, startY + halfY, halfX, halfY);
        chunks[3] = new Chunk(startX + halfX, startY + halfY, halfX, halfY);

        for (Chunk chunk : chunks) {
            for (PhysicsEntity entity : this.entityList) {
                if (chunk.chunkBounds.isAnyPartInBounds(entity.getFullBounds())) {
                    chunk.addEntity(entity);
                }
            }

            for (PhysicsEntity obstacle : this.obstacleList) {
                if (chunk.chunkBounds.isAnyPartInBounds(obstacle.getFullBounds())) {
                    chunk.addEntity(obstacle);
                }
            }
        }
        
        return chunks;
    }

    public void collideWithObstacles() {
        for (PhysicsEntity entity : this.entityList) {
            if (!entity.hasPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION)) {
                for (PhysicsEntity obstacle : this.obstacleList) {
                    this.checkForCollisionWith(obstacle, entity);
                }
            }
        }
    }

    public void collideWithObjects() {
        for (PhysicsEntity firstEntity : this.entityList) {
        	//long time = System.nanoTime();
            if (!firstEntity.hasPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION)) {
                for (PhysicsEntity secondEntity : this.entityList) {
                    if (!secondEntity.hasPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION)) {
                        this.checkForCollisionWith(firstEntity, secondEntity);
                    }
                }
            }
            //System.out.println("Update: " + (System.nanoTime() - time));
        }
    }

    private void checkForCollisionWith(PhysicsEntity firstEntity, PhysicsEntity secondEntity) {        
        CollisionInfo info = CollisionInfo.getCollisionInfo(firstEntity.getRealBounds(), secondEntity.getAppropriateBounds());

        if(firstEntity != secondEntity && info.isValid() && !(firstEntity instanceof ViolentEntity && secondEntity instanceof ViolentEntity && ((ViolentEntity) firstEntity).isInvincible() && ((ViolentEntity) secondEntity).isInvincible())) {
            BoundingBox collisionBox = firstEntity.getRealBounds().getCollisionWith(secondEntity.getAppropriateBounds());

            ArrayList<Float> powerList = new ArrayList<Float>();
            float totalPower = 0;
            int numberOfCollisions = 0;
            
            if (firstEntity instanceof ViolentEntity && 
            		secondEntity instanceof ViolentEntity && 
            		((ViolentEntity) secondEntity).isCountering() &&
            		((ViolentEntity) firstEntity).getDamageDealt() != 0) {
            	for (int y = Math.round(collisionBox.getLowerLeft().y); y < collisionBox.getUpperRight().y; y++) {
            		for (int x = Math.round(collisionBox.getLowerLeft().x); x < collisionBox.getUpperRight().x; x++) {
            			if (firstEntity.checkForCollision(x, y) && secondEntity.checkForCollision(x, y)) {
            				if (firstEntity instanceof ViolentEntity)
            				((ViolentEntity) secondEntity).setAttackCapability(
            						((ViolentEntity) firstEntity).getDamageDealt() * 1.5f,
            						((ViolentEntity) firstEntity).getKnockback() * 1.5f,
            						((ViolentEntity) firstEntity).getPriority() * 1.5f, false);
            			}
            		}
            	}
            } else {
            	for (int y = Math.round(collisionBox.getLowerLeft().y); y < collisionBox.getUpperRight().y; y++) {
            		for (int x = Math.round(collisionBox.getLowerLeft().x); x < collisionBox.getUpperRight().x; x++) {
            			if (firstEntity.checkForCollision(x, y) && secondEntity.checkForCollision(x, y)) {
            				totalPower += firstEntity.getSweetSpotPower(x, y);
            				powerList.add(firstEntity.getSweetSpotPower(x, y));
            				numberOfCollisions++;
            			}
            		}
            	}
            	
            	if (numberOfCollisions > 0) {
            		info.setPower((totalPower / numberOfCollisions + Mode(powerList)*2.0f)/3.0f);
            		firstEntity.onCollideWith(secondEntity, info);
            	}
            }
        }
    }
    
    private static float Mode(ArrayList<Float> n){
        float t = 0f;
        for(int i=0; i<n.size(); i++){
            for(int j=1; j<n.size()-i; j++){
                if(n.get(j-1) > n.get(j)){
                    t = n.get(j-1);
                    n.set(j-1,n.get(j));
                    n.set(j,t);
                }
            }
        }

        float mode = n.get(0);
        int temp = 1;
        int temp2 = 1;
        for(int i=1;i<n.size();i++){
            if(n.get(i-1) == n.get(i)){
                temp++;
            }
            else {
                temp = 1;
            }
            if(temp >= temp2){
                mode = n.get(i);
                temp2 = temp;
            }
        }
        return mode;
    }
}
