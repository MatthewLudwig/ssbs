package states.game;

import engine.TBS;
import engine.TBSBased;
import engine.ssbs.ItemList;
import engine.ssbs.ItemList.ItemMapping;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author Matthew
 */
public class GameRules implements TBSBased {
    private boolean hasChanged;
    
    private boolean teamBattle;
    private ItemFrequency itemFrequency;
    private Map<String, Boolean> itemList;
    
    public GameRules() {
        this.hasChanged = false;
        
        this.teamBattle = false;
        this.itemFrequency = ItemFrequency.NONE;
        this.itemList = new HashMap<String, Boolean>();
        
        for (ItemMapping i : ItemList.getItems()) {
            this.itemList.put(i.getName(), false);
        }

    }

    public TBS save() {
        TBS saveData = new TBS();
    
        saveData.writeBoolean("Team Battle", this.teamBattle);
        saveData.writeString("Item Frequency", this.itemFrequency.name());
        
        for (Entry<String, Boolean> e : this.itemList.entrySet()) {
            saveData.writeBoolean(e.getKey(), e.getValue());
        }
        
        return saveData;
    }

    public void load(TBS saveData) {
        this.teamBattle = saveData.getBoolean("Team Battle");
        this.itemFrequency = ItemFrequency.valueOf(saveData.getString("Item Frequency"));
        
        for (Entry<String, Boolean> e : this.itemList.entrySet()) {
            e.setValue(saveData.getBoolean(e.getKey()));
        }
    }
    
    public boolean hasChanged() {
        boolean oldState = this.hasChanged;
        this.hasChanged = false;
        return oldState;
    }
    
    public void setTeamBattleOn(boolean newState) {
        this.teamBattle = newState;
        this.hasChanged = true;
    }
    
    public boolean isTeamBattleOn() {
        return this.teamBattle;
    }
    
    public void setItemFrequency(ItemFrequency newFrequency) {
        this.itemFrequency = newFrequency;
        this.hasChanged = true;
    }
    
    public ItemFrequency getItemFrequency() {
        return this.itemFrequency;
    }
    
    public void setItemOn(String name, boolean newState) {
        this.itemList.put(name, newState);
        this.hasChanged = true;
    }
    
    public boolean isItemOn(String name) {
        return this.itemList.get(name);
    }
    
    public enum ItemFrequency {
        NONE(Float.MAX_VALUE),
        LOW(8),
        MEDIUM(4),
        HIGH(1),
        INSANE(.25f);
        
        private float frequencyTime;
        
        private ItemFrequency(float time) {
            this.frequencyTime = time;
        }
        
        public float getFrequencyTime() {
            return this.frequencyTime;
        }
    }
}
