package states.game;

import engine.Main;
import entities.characters.controllers.BasicAI;
import entities.characters.controllers.PlayerController;
import engine.ssbs.CharacterList.CharacterMapping;
import entities.PlayableCharacter;
import entities.Team;
import static entities.Team.BLUE;
import static entities.Team.GREEN;
import static entities.Team.RED;
import static entities.Team.YELLOW;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 *
 * @author Matthew
 */
public class SelectedCharacter {
    private static Map<String, boolean[]> colorGuards = new HashMap<String, boolean[]>();
    public static void clearColorGuards() {  colorGuards.clear();}
    
    private boolean hasChanged;
    private boolean isRemoved;
    
    private CharacterMapping mapping; 
    private int numberOfLives;
    private int controllerLevel;
    private int colorNumber;
    private Team team;

    public SelectedCharacter(Team team) {
        this(null, 3, 0, 0, team);
    }

    public SelectedCharacter(CharacterMapping mapping, int numberOfLives, int controllerLevel, int colorNumber, Team team) {
        this.hasChanged = true;
        this.isRemoved = mapping == null;
        this.mapping = mapping;
        this.numberOfLives = numberOfLives;
        this.controllerLevel = controllerLevel;
        this.colorNumber = colorNumber;
        this.team = team;
    }
    
    public boolean hasChanged() {
        boolean oldState = this.hasChanged;
        this.hasChanged = false;
        return oldState;
    }
    
    public void setRemoved(boolean state) {
        this.isRemoved = state;
        this.hasChanged = true;
    }
    
    public boolean isRemoved() {
        return this.isRemoved;
    }
    
    public CharacterMapping getMapping() {
        return this.isRemoved ? null : this.mapping;
    }
    
    public void setMapping(CharacterMapping mapping) {
        if (this.mapping != null && colorGuards.containsKey(this.mapping.getName())) {
            colorGuards.get(this.mapping.getName())[this.colorNumber] = false;
        }
        
        this.mapping = mapping;
        
        if (Main.getStrifeGameRules().isTeamBattleOn()) {
            switch(this.team) {
                case RED:
                    this.colorNumber = this.mapping.getRedColor();
                    break;
                case BLUE:
                    this.colorNumber = this.mapping.getBlueColor();
                    break;
                case GREEN:
                    this.colorNumber = this.mapping.getGreenColor();
                    break;
                case YELLOW:
                    this.team = Team.RED;
                    this.colorNumber = this.mapping.getRedColor();
                    break;
                default:
                    Main.log(Level.SEVERE, "Unexpected team is assigned with selected character!", null);
            }
        } else {
            if (colorGuards.containsKey(this.mapping.getName())) {
                for (int x = 0; x < 6; x++) {
                    if (!colorGuards.get(this.mapping.getName())[x]) {
                        this.colorNumber = x;
                        break;
                    }
                }
            } else {
                colorGuards.put(this.mapping.getName(), new boolean[]{false, false, false, false, false, false});
                this.colorNumber = 0;
            }

            colorGuards.get(this.mapping.getName())[this.colorNumber] = true;
        }
        
        this.hasChanged = true;
    }
    
    public int getNumberOfLives() {
        return this.numberOfLives;
    }
    
    public void setNumberOfLives(int numberOfLives) {
        this.numberOfLives = numberOfLives;
        this.hasChanged = true;
    }
    
    public void setAILevel(int aiLevel) {
        this.controllerLevel = aiLevel;
        this.hasChanged = true;
    }
    
    public int getAILevel() {
        return this.controllerLevel;
    }
    
    public void setTeamNumber(int number) {
        switch(number) {
            case 0:
                this.team = Team.RED;
                break;
            case 1:
                this.team = Team.BLUE;
                break;
            case 2:
                this.team = Team.YELLOW;
                break;
            case 3:
                this.team = Team.GREEN;
                break;
            default:
                Main.log(Level.SEVERE, "Unexpected team is assigned with selected character!", null);
        }
        
        this.hasChanged = true;
    }
    
    public void setColorNumber(int number) {
        this.colorNumber = number;
        this.hasChanged = true;
    }
    
    public void incrementColorNumber(int amount) {
        if (colorGuards.containsKey(this.mapping.getName())) {
            colorGuards.get(this.mapping.getName())[this.colorNumber] = false;
        }
        
        this.colorNumber += amount;
        
        if (this.colorNumber > 5) {
            this.colorNumber = 0;
        } else if (this.colorNumber < 0) {
            this.colorNumber = 5;
        }
        
        if (colorGuards.containsKey(this.mapping.getName())) {
            while (colorGuards.get(this.mapping.getName())[this.colorNumber]) {
                this.colorNumber += amount;
        
                if (this.colorNumber > 5) {
                    this.colorNumber = 0;
                } else if (this.colorNumber < 0) {
                    this.colorNumber = 5;
                }
            }
        } else {
            colorGuards.put(this.mapping.getName(), new boolean[]{false, false, false, false, false, false});
        }
        
        colorGuards.get(this.mapping.getName())[this.colorNumber] = true;
        this.hasChanged = true;
    }
    
    public int getColorNumber() {
        return this.colorNumber;
    }
    
    public void nextTeam() {
        switch(this.team) {
            case RED:
                this.team = Team.BLUE;
                this.colorNumber = this.mapping.getBlueColor();
                break;
            case BLUE:
                this.team = Team.GREEN;
                this.colorNumber = this.mapping.getGreenColor();
                break;
            case GREEN:
            case YELLOW:
                this.team = Team.RED;
                this.colorNumber = this.mapping.getRedColor();
                break;
            default:
                Main.log(Level.SEVERE, "Unexpected team is assigned with selected character!", null);
        }
            
        this.hasChanged = true;
    }
    
    public void prevTeam() {
        switch(this.team) {
            case RED:
                this.team = Team.GREEN;
                this.colorNumber = this.mapping.getGreenColor();
                break;
            case BLUE:
                this.team = Team.RED;
                this.colorNumber = this.mapping.getRedColor();
                break;
            case GREEN:
            case YELLOW:
                this.team = Team.BLUE;
                this.colorNumber = this.mapping.getBlueColor();
                break;
            default:
                Main.log(Level.SEVERE, "Unexpected team is assigned with selected character!", null);
        }
            
        this.hasChanged = true;
    }
    
    public Team getTeam() {
        return this.team;
    }

    public PlayableCharacter getCharacter() {
        PlayableCharacter player = this.mapping != null ? this.mapping.getObject() : null;
        
        if (player != null) {
            player.setNumberOfLives(this.numberOfLives);
            
            if (this.controllerLevel == 0) {
                player.setController(new PlayerController(player));
            } else {
                player.setController(new BasicAI(player, this.controllerLevel - 1));
            }
            
            if (this.mapping.getName().equals("Random")) {
                String name = Main.getCharacterList().getCharacter(player).getName();
                
                if (colorGuards.containsKey(name)) {
                    do {
                        double chance = Math.random();

                        if (chance < .375) {
                            this.colorNumber = 0;
                        } else if (chance < .50) {
                            this.colorNumber = 1;
                        } else if (chance < .625) {
                            this.colorNumber = 2;
                        } else if (chance < .75) {
                            this.colorNumber = 3;
                        } else if (chance < .875) {
                            this.colorNumber = 4;
                        } else {
                            this.colorNumber = 5;
                        }
                    } while (colorGuards.get(name)[this.colorNumber]);
                } else {
                    double chance = Math.random();

                    if (chance < .375) {
                        this.colorNumber = 0;
                    } else if (chance < .50) {
                        this.colorNumber = 1;
                    } else if (chance < .625) {
                        this.colorNumber = 2;
                    } else if (chance < .75) {
                        this.colorNumber = 3;
                    } else if (chance < .875) {
                        this.colorNumber = 4;
                    } else {
                        this.colorNumber = 5;
                    }
                    
                    colorGuards.put(name, new boolean[]{false, false, false, false, false, false});
                }
                
                colorGuards.get(name)[this.colorNumber] = true;
            }
            
            player.setColor(this.colorNumber);
            player.team = this.team;
        }

        return this.isRemoved ? null : player;
    }
}
