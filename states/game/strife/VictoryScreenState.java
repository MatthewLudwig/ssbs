package states.game.strife;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;

import engine.Main;
import engine.PictureLayer;
import engine.ssbs.CharacterList.CharacterMapping;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import states.SimpleAppState;

/**
 *
 *
 * @author Matthew Ludwig
 */
public class VictoryScreenState extends SimpleAppState {
    private PlayableCharacter[] sortedPlayers;
    private PlayableCharacter[] allPlayers;

    public VictoryScreenState(PlayableCharacter[] sortedPlayers, PlayableCharacter[] allPlayers) {
        super("Victory Screen", true);
        this.sortedPlayers = sortedPlayers;        
        this.allPlayers = allPlayers;
        this.hasMusic = false;
    }

    @Override
    public void attachToAbsoluteNode(Node node, AssetManager am) {
//        node.attachChild(Utilities.getJMELoader().getText(Utilities.defaultFont, ColorRGBA.Cyan, "PRESS " + Main.getUserInput().getKeyName("Pause"), 250, 3));
        node.attachChild(Utilities.getJMELoader().getText(Utilities.Impact24, ColorRGBA.Cyan, "This game's winner is...", 30, 420));
        
        /*First Place*/
        node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact24, ColorRGBA.Cyan, this.sortedPlayers[0].getWinMessage(), 400, 420));
        
        if (this.sortedPlayers[1] != null) {
            /*Second Place*/
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact24, ColorRGBA.LightGray, "2nd", 30, 140));
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact24, ColorRGBA.White, this.sortedPlayers[1].getWinMessage(), 30, 100));
        }

        if (this.sortedPlayers[2] != null) {
            /*Third Place*/
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact24, ColorRGBA.Gray, "3rd", 450, 140));
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact24, ColorRGBA.Gray, ":", 450, 100));
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact24, ColorRGBA.White, this.sortedPlayers[2].getWinMessage(), 450, 60));
        }

        if (this.sortedPlayers[3] != null) {
            /*Fourth Place*/
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact24, ColorRGBA.DarkGray, "4th", 240, 140));
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact24, ColorRGBA.DarkGray, ":", 240, 100));
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact24, ColorRGBA.DarkGray, ":", 240, 60));
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact24, ColorRGBA.White, this.sortedPlayers[3].getWinMessage(), 240, 20));
        }
        
        CharacterMapping winner = Main.getCharacterList().getCharacter(this.sortedPlayers[0]);
        
        Picture p = Utilities.getJMELoader().getPicture("Victory Pose", winner.getVictoryPose(this.sortedPlayers[0].getColorNumber()), 195f, 170, PictureLayer.GUI);
        p.scale(2);
        node.attachChild(p);
    }

    @Override
    public void attachToPausableNode(Node node, AssetManager am) {
    }

    @Override
    public String getMusicPath() {
        return "Music/TitleMusic.ogg";
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        if (Main.getUserInput().isKeyClicked("Pause")) {
            this.switchStates(new StrifeCharacterSelectState(allPlayers));
        }
    }
}
