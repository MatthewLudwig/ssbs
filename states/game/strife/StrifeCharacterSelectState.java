package states.game.strife;

import states.game.CharacterSelectState;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;

import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import entities.Team;
import states.SimpleAppState;
import states.game.SelectedCharacter;

/**
 *
 *
 * @author Matthew Ludwig
 */
public class StrifeCharacterSelectState extends CharacterSelectState {

    public StrifeCharacterSelectState(PlayableCharacter... passedInCharacters) {
        super(4, 2, Main.getStrifeGameRules(), passedInCharacters);
    }

    @Override
    public void attachToAbsoluteNode(Node node, AssetManager am) {
        node.attachChild(Utilities.getJMELoader().getPicture("Background", "Interface/MatchSetup/choosePlayer_back.png", 0, 0, PictureLayer.BACKGROUND));
        
        BitmapText text = Utilities.getJMELoader().getText(ColorRGBA.White, "", 100, 440);
        text.setName("Game Type");
        node.attachChild(text);
        
        this.updateTypeText();
        
        node.attachChild(this.getCharacterPortraits());
        node.attachChild(this.getSelectionPortraits());
        node.attachChild(this.getItemPortraits());
    }
    
    public final void updateTypeText() {
        BitmapText text = (BitmapText) this.absoluteNode.getChild("Game Type");
        
        if (this.gameRules.isTeamBattleOn()) {
            text.setText("Team Battle");
        } else {
            text.setText("Free For All");
        }
    }
    
    @Override
    public void update(float tpf) {
        super.update(tpf);
        
        if (Main.getUserInput().isLeftButtonClicked() && Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition.subtract(0, -35), (BitmapText) this.absoluteNode.getChild("Game Type"))) {
            this.gameRules.setTeamBattleOn(!this.gameRules.isTeamBattleOn());
            
            if (this.gameRules.isTeamBattleOn()) {
                SelectedCharacter.clearColorGuards();
                                
                for (SelectedCharacter character : this.characters) {
                    if (character.getMapping() != null) {
                        character.setMapping(character.getMapping());
                    }
                }
            } else {
                SelectedCharacter.clearColorGuards();
                
                for (int x = 0; x < this.characters.length; x++) {
                    this.characters[x].setTeamNumber(x);
                }
            }
            
            this.updateTypeText();
        }
    }
    
    @Override
    public SimpleAppState getNextState(PlayableCharacter[] players) {
        return new StageSelectState(players);
    }
}
