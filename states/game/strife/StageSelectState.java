package states.game.strife;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.ui.Picture;

import engine.Main;
import engine.PictureLayer;
import engine.ssbs.StageList.StageMapping;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import states.SimpleAppState;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 *
 * @author Matthew Ludwig
 */
public class StageSelectState extends SimpleAppState {
    private PlayableCharacter[] players;
    private String previewPath;

    public StageSelectState(PlayableCharacter[] players) {
        super("Stage Select", true);
        this.players = players;
    }

    @Override
    public void attachToAbsoluteNode(Node node, AssetManager am) {
        this.previewPath = "Interface/MatchSetup/noPreview.png";
        node.attachChild(Utilities.getJMELoader().getPicture("Background", "Interface/MatchSetup/chooseStage_back.png", 0, 0, PictureLayer.BACKGROUND));
        node.attachChild(Utilities.getJMELoader().getPicture("Preview", this.previewPath, 435, 266, PictureLayer.GUI));
        node.attachChild(Utilities.getJMELoader().getText(ColorRGBA.White, "Choose Your Stage", 80, 440));
        node.attachChild(this.getStageIcons());
    }

    private Node getStageIcons() {
        Node node = new Node("Stage Icons");
        
        List<Entry<Integer, StageMapping>> unlockedStages = Main.getStageList().getUnlockedStages();
        
        int width = 1;
        int height = 1;
        int rem = 0;
        while (width * height + rem < unlockedStages.size()) {
            rem++;
            if (rem >= height) {
                rem -= height;
                width++;
            }
            if (height < width) {
                int tempHeight = height + 1;
                int tempWidth = width;
                while (tempWidth * tempHeight > width * height)
                    tempWidth--;
                rem = width * height + rem - tempWidth * tempHeight;
                width = tempWidth;
                height = tempHeight;
            }
        }
        
        float scale = Math.min((7f / (float) (width + (rem != 0 ? 1 : 0))), (6f / (float) height));
        float xOff = 28.5f * (7 - ((width + (rem != 0 ? 1 : 0)) * scale));
        float yOff = 25 * (6 - (height * scale));
        
        node.setLocalTranslation(15 - (rem == 0 ? 24 * scale : 0) + (xOff), 350 - (25 * (scale - 1)) - (yOff), -1);
        node.setLocalScale(scale);
        
        int x = 0, y = 0;
        
        for (Map.Entry<Integer, StageMapping> e : unlockedStages) {
            int characterIndex = e.getKey();
            StageMapping mapping = e.getValue();
            
            if (rem != 0) {
                node.attachChild(Utilities.getJMELoader().getPicture("Stage Icon #" + characterIndex, mapping.getIcon(), (x * 57), -(y * 50), PictureLayer.GUI));

                if (++x == (width + 1)) {
                    x = 0;
                    y++;
                    rem--;
                }
            } else {
                node.attachChild(Utilities.getJMELoader().getPicture("Stage Icon #" + characterIndex, mapping.getIcon(), 28.5f + (x * 57), -(y * 50), PictureLayer.GUI));

                if (++x == width) {
                    x = 0;
                    y++;
                }
            }
        }
        
        return node;
    }

    @Override
    public void attachToPausableNode(Node node, AssetManager am) {
    }

    @Override
    public String getMusicPath() {
        return "Music/MainMenuMusic.ogg";
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        
        this.customUpdateBackButton(5, -15);

        if (Main.getUserInput().isKeyClicked("Pause")) {
            this.playSelectSound();
            this.switchStates(new StrifeGameState(Main.getStageList().getRandomStage().getObject(), this.players));
        }

        Node stageIcons = (Node) this.absoluteNode.getChild("Stage Icons");

        for (Spatial object : stageIcons.getChildren()) {
            if (Utilities.getPhysicsUtility().complex2DCollisionWithScale(this.cursorPosition.subtract(stageIcons.getLocalTranslation().x, stageIcons.getLocalTranslation().y), (Picture) object)) {
                StageMapping mapping = Main.getStageList().getStage(Integer.valueOf(object.getName().split("#")[1]));

                if (mapping != null) {
                    if (Main.getUserInput().isLeftButtonClicked()) {
                        this.playSelectSound();
                        if (mapping.getName().equals("Omicron")){
                        	Main.getStageList().toggleOmicron();
                        	this.absoluteNode.detachChild(stageIcons);
                        	stageIcons = getStageIcons();
                        	this.absoluteNode.attachChild(stageIcons);
                        } else {
                        	this.switchStates(new StrifeGameState(mapping.getObject(), this.players));
                        }
                    } else {
                        if (!mapping.getPreview().equals(this.previewPath)) {
                            this.previewPath = mapping.getPreview();
                            Utilities.getJMELoader().changeTexture((Picture) this.absoluteNode.getChild("Preview"), this.previewPath);
                        }
                    }
                }
            }
        }   
    }
    
    protected void customUpdateBackButton(float xPosition, float yPosition) {
        if (this.absoluteNode.getChild("Back Button") == null) {
            this.absoluteNode.attachChild(Utilities.getJMELoader().getPicture("Back Button", "Interface/General/backButton.png", xPosition, yPosition, PictureLayer.GUI));
        } else if (Main.getUserInput().isLeftButtonClicked() && Utilities.getPhysicsUtility().complex2DCollision(Main.getUserInput().getMousePosition(), (Picture) this.absoluteNode.getChild("Back Button"))) {
            Utilities.getCustomLoader().getAudioNode("/Common Sounds/back.ogg").playInstance();
            this.switchStates(new StrifeCharacterSelectState(this.players));
        }   
    }
}
