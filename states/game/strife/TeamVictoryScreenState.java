package states.game.strife;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;

import engine.Main;
import engine.PictureLayer;
import engine.ssbs.CharacterList.CharacterMapping;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import states.SimpleAppState;

/**
 *
 *
 * @author Matthew Ludwig
 */
public class TeamVictoryScreenState extends SimpleAppState {
    private PlayableCharacter[][] sortedTeams;
    private PlayableCharacter[] allPlayers;

    public TeamVictoryScreenState(PlayableCharacter[][] teams, PlayableCharacter[] allPlayers) {
        super("Victory Screen", true);
        this.sortedTeams = teams;        
        this.allPlayers = allPlayers;
        this.hasMusic = false;
    }

    @Override
    public void attachToAbsoluteNode(Node node, AssetManager am) {
        node.attachChild(Utilities.getJMELoader().getText(Utilities.Impact32, ColorRGBA.Cyan, "This game's winner is . . . ", 30, 420));
        
        /*First Place*/
        node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.Cyan, this.sortedTeams[0][0].team.displayName + " Team", 400, 420));
        
        String text = "";
        for (PlayableCharacter character : this.sortedTeams[0]) {
            if (text.isEmpty()) {
                text += "[ ";
            } else {
                text += " & ";
            }
            
            text += character.getTeamWinMessage();
        }
        text += " ]";
        
        node.attachChild(Utilities.getJMELoader().getCenteredColorCodedText(Utilities.Impact32, ColorRGBA.White, text, 320, 380));
        
        if (1 < this.sortedTeams.length && this.sortedTeams[1] != null) {
            /*Second Place*/
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.LightGray, "2nd", 30, 180));
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.White, this.sortedTeams[1][0].team.displayName + " Team", 30, 140));
            
            for (int x = 0; x < this.sortedTeams[1].length; x++) {
                node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.White, "[ " + this.sortedTeams[1][x].getTeamWinMessage() + " ]", 30, 100 - (40 * x)));
            }
        }

        if (2 < this.sortedTeams.length && this.sortedTeams[2] != null) {
            /*Third Place*/
//            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.Gray, "3rd", 450, 140));
//            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.Gray, ":", 450, 100));
//            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.White, this.sortedTeams[2][0].team.displayName, 450, 60));
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.Gray, "3rd", 240, 180));
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.Gray, ":", 240, 140));
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.White, this.sortedTeams[2][0].team.displayName + " Team", 240, 100));
        
            for (int x = 0; x < this.sortedTeams[2].length; x++) {
                node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.White, "[ " + this.sortedTeams[2][x].getTeamWinMessage() + " ]", 240, 60 - (40 * x)));
            }
        }

        if (3 < this.sortedTeams.length && this.sortedTeams[3] != null) {
            /*Fourth Place*/
//            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.DarkGray, "4th", 240, 140));
//            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.DarkGray, ":", 240, 100));
//            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.DarkGray, ":", 240, 60));
//            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.White, this.sortedTeams[3][0].team.displayName, 240, 20));
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.DarkGray, "4th", 450, 140));
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.DarkGray, ":", 450, 100));
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.DarkGray, ":", 450, 60));
            node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.White, this.sortedTeams[3][0].team.displayName + " Team", 450, 20));
        
            for (int x = 0; x < this.sortedTeams[3].length; x++) {
                node.attachChild(Utilities.getJMELoader().getColorCodedText(Utilities.Impact32, ColorRGBA.White, "[ " + this.sortedTeams[3][x].getTeamWinMessage() + " ]", 450, -20 - (40 * x)));
            }
        }
        
        float startingPosition = 195 - (125 * (this.sortedTeams[0].length - 1));
                
        for (int x = 0; x < this.sortedTeams[0].length; x++) {
            CharacterMapping winner = Main.getCharacterList().getCharacter(this.sortedTeams[0][x]);
            Picture p = Utilities.getJMELoader().getPicture("Victory Pose", winner.getVictoryPose(this.sortedTeams[0][0].getColorNumber()), startingPosition + (250 * x), 170, PictureLayer.GUI);
            p.scale(2);
            node.attachChild(p);
        }
        
    }

    @Override
    public void attachToPausableNode(Node node, AssetManager am) {
    }

    @Override
    public String getMusicPath() {
        return "Music/TitleMusic.ogg";
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        if (Main.getUserInput().isKeyClicked("Pause")) {
            this.switchStates(new StrifeCharacterSelectState(allPlayers));
        }
    }
}
