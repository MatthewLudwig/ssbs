package states.game.strife;

import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import entities.Team;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.texture.Texture2D;
import com.jme3.ui.Picture;

import states.game.GameState;
import states.game.stages.Stage;
import states.game.targetTest.TargetTestCharacterSelectState;

/**
 *
 * @author Matthew
 */
public class StrifeGameState extends GameState {
	protected static Texture2D[] gameAnimation;
	protected static Texture2D[] countdown;
	private int animationFrame;
	private int animationFrame2;
    private float updateLimiter;
    private Picture status;
    private boolean playStart;
    private PlayableCharacter[][] teamEndArray;
    private PlayableCharacter[] endArray;
	
	static {
		gameAnimation = new Texture2D[31];
        countdown = new Texture2D[40];
        
        for (int count = 0; count < gameAnimation.length; count++) {
        	gameAnimation[count] = Utilities.getJMELoader().getTexture("/Interface/GroupBattle/game_" + Utilities.getGeneralUtility().getNumberAsString(count) + ".png");
        }
        
        for (int count = 0; count < countdown.length; count++) {
            countdown[count] = Utilities.getJMELoader().getTexture("/Interface/GroupBattle/321GO_" + Utilities.getGeneralUtility().getNumberAsString(count) + ".png");
        }
    }
    
    public StrifeGameState(Stage stage, PlayableCharacter[] players) {
        super(stage, Main.getStrifeGameRules(), players);
        this.animationFrame = -1;
        this.animationFrame2 = -1;
        this.updateLimiter = 0;
        this.playStart = true;
        this.status = Utilities.getJMELoader().getPicture("Status", "/Sprites/Miscellaneous/blankSprite.png", 0, 0, PictureLayer.SPECIALLAYER);
        if (this.animationFrame == -1) {
        	Main.getUserInput().setAcceptingInputs(false);
        	this.animationFrame = 0;
        }
    }
    
    @Override
    public void checkForFinish() {
        if (this.gameRules.isTeamBattleOn()) {
            Map<Team, ArrayList<PlayableCharacter>> teams = new HashMap<Team, ArrayList<PlayableCharacter>>();
            Map<Team, ArrayList<PlayableCharacter>> allTeams = new HashMap<Team, ArrayList<PlayableCharacter>>();            

//            teams.put(Team.RED, new ArrayList<PlayableCharacter>());
//            teams.put(Team.BLUE, new ArrayList<PlayableCharacter>());
//            teams.put(Team.GREEN, new ArrayList<PlayableCharacter>());
//            teams.put(Team.YELLOW, new ArrayList<PlayableCharacter>());
            
            for (PlayableCharacter character : this.world.getPlayerList()) {
                if (!character.isDead()) {
                    if (!teams.containsKey(character.team)) {
                        teams.put(character.team, new ArrayList<PlayableCharacter>());
                    }
                    
                    teams.get(character.team).add(character);
                }
                
                if (!allTeams.containsKey(character.team)) {
                    allTeams.put(character.team, new ArrayList<PlayableCharacter>());
                }

                allTeams.get(character.team).add(character);
            }
            
            if (teams.isEmpty()) {
//                Wow this is pretty damn complicated!
            } else if (teams.size() == 1) {
//                PlayableCharacter[] sortedArray = new PlayableCharacter[this.players.length];
//                System.arraycopy(this.players, 0, sortedArray, 0, sortedArray.length);
//
//                Arrays.sort(sortedArray, new PlayerComparator());
//
//                this.switchStates(new VictoryScreenState(sortedArray, this.players));
                
                PlayableCharacter[][] sortedArray = new PlayableCharacter[allTeams.size()][];
                Iterator<ArrayList<PlayableCharacter>> i = allTeams.values().iterator();
                PlayableCharacter[] emptyArray = new PlayableCharacter[0];
                
                for (int x = 0; x < sortedArray.length; x++) {
                    sortedArray[x] = i.next().toArray(emptyArray);
                }
                
                Arrays.sort(sortedArray, new TeamComparator());
                teamEndArray = sortedArray;
                if (animationFrame2 == -1)
                	animationFrame2 = 0;
            }
        } else {
            int remainingPlayers = 0;
            PlayableCharacter winner = null;

            for(PlayableCharacter character : this.world.getPlayerList()) {
                if (!character.isDead()) {
                    remainingPlayers++;
                    winner = character;
                }
            }

            if (remainingPlayers == 0) {
                PlayableCharacter[] sortedArray = new PlayableCharacter[this.players.length];
                System.arraycopy(this.players, 0, sortedArray, 0, sortedArray.length);

                Arrays.sort(sortedArray, new PlayerComparator());
                endArray = sortedArray;
                int count;

                for (count = 0; count < sortedArray.length; count++) {
                    if (sortedArray[count] == null || sortedArray[count].getTimeOfDeath() != sortedArray[0].getTimeOfDeath()) {
                        break;
                    }
                }

                for (int newCount = 0; newCount < count; newCount++) {
                    sortedArray[newCount].reviveForSuddenDeath();
                }
            } else if (remainingPlayers == 1) {
                PlayableCharacter[] sortedArray = new PlayableCharacter[this.players.length];
                System.arraycopy(this.players, 0, sortedArray, 0, sortedArray.length);

                Arrays.sort(sortedArray, new PlayerComparator());
                endArray = sortedArray;

                if (animationFrame2 == -1)
                	animationFrame2 = 0;
            }
        }
    }
    
    private class PlayerComparator implements Comparator {
        public int compare(Object o1, Object o2) {
            if (o1 == null) {
                return 1;
            } else if (o2 == null) {
                return -1;
            }
            
            PlayableCharacter character1 = (PlayableCharacter) o1;
            PlayableCharacter character2 = (PlayableCharacter) o2;
                        
            return -Float.compare(character1.getTimeOfDeath(), character2.getTimeOfDeath());
        }
    }
    
    public void attachToAbsoluteNode(Node node, AssetManager am) {
        super.attachToAbsoluteNode(node, am);
        node.attachChild(this.status);
        this.status.setLocalTranslation(0, 0, 10);
    }
    
    public void update(float tpf) {     
    	if (this.animationFrame2 != -1) {
            if (this.updateLimiter >= .04f) {
            	Utilities.getJMELoader().changeTexture(this.status, gameAnimation[this.animationFrame2]);
                this.animationFrame2 += 1;

                if (this.animationFrame2 >= 30) {
                    Main.getUserInput().setAcceptingInputs(true);
                    if (teamEndArray != null)
                    	this.switchStates(new TeamVictoryScreenState(teamEndArray, this.players));
                    else 
                    	this.switchStates(new VictoryScreenState(endArray, this.players));
                    return;
                }
                
                this.updateLimiter -= .04f;
            } else {
                this.updateLimiter += Utilities.lockedTPF;
            }
            
            Utilities.lockedTPF /= 2;
        }
    	
        if (this.animationFrame != -1 && this.playStart) {
           if (this.updateLimiter >= .04f) {
                Utilities.getJMELoader().changeTexture(this.status, countdown[this.animationFrame]);
                this.animationFrame += 1;

                if (this.animationFrame >= 32) {
                    Main.getUserInput().setAcceptingInputs(true);
                }
                if (this.animationFrame >= 39) {
                    this.animationFrame = -1;
                    this.playStart = false;
                    Utilities.getJMELoader().changeTexture(this.status, "/Sprites/Miscellaneous/blankSprite.png");
                }
                
                this.updateLimiter -= .04f;
            } else {
                this.updateLimiter += Utilities.lockedTPF;
            }
            
            Utilities.lockedTPF /= 2;
        }
        
        super.update(tpf);
    }

    private class TeamComparator implements Comparator {
        public int compare(Object o1, Object o2) {
            if (o1 == null) {
                return 1;
            } else if (o2 == null) {
                return -1;
            }
            
            PlayableCharacter[] team1 = (PlayableCharacter[]) o1;
            PlayableCharacter[] team2 = (PlayableCharacter[]) o2;
            float team1Time = 0;
            float team2Time = 0;
            
            for (PlayableCharacter character : team1) {
                if (character != null) {
                    team1Time = Math.max(character.getTimeOfDeath(), team1Time);
                }
            }
            
            for (PlayableCharacter character : team2) {
                if (character != null) {
                    team2Time = Math.max(character.getTimeOfDeath(), team2Time);
                }
            }
            
            return -Float.compare(team1Time, team2Time);
        }
    }
}
