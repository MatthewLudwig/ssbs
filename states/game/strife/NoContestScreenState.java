package states.game.strife;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;

import engine.Main;
import engine.PictureLayer;
import engine.ssbs.CharacterList.CharacterMapping;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import states.SimpleAppState;

/**
 *
 *
 * @author Matthew Ludwig
 */
public class NoContestScreenState extends SimpleAppState {
    //private PlayableCharacter[][] sortedTeams;
    private PlayableCharacter[] allPlayers;
    private boolean refreshButtons;
    private boolean refreshPause;

    public NoContestScreenState(PlayableCharacter[] allPlayers) {
        super("Victory Screen", true);
        //this.sortedTeams = teams;        
        this.allPlayers = allPlayers;
        this.hasMusic = false;
        refreshButtons = true;
        refreshPause = false;
        if (Main.getUserInput().getKeyState("Pause")) {
        	refreshButtons = false;
        }
    }

    @Override
    public void attachToAbsoluteNode(Node node, AssetManager am) {
        node.attachChild(Utilities.getJMELoader().getCenteredText(Utilities.Impact32, ColorRGBA.Gray, "NO CONTEST", 320, 420));
        
        for (int x = 0; x < this.allPlayers.length; x++) {
            CharacterMapping winner = Main.getCharacterList().getCharacter(this.allPlayers[x]);
            Picture p = Utilities.getJMELoader().getPicture("Victory Pose", winner.getVictoryPose(this.allPlayers[x].getColorNumber()), 50 + (250 * x), 170, PictureLayer.GUI);
            p.scale(2);
            node.attachChild(p);
        }
        
    }

    @Override
    public void attachToPausableNode(Node node, AssetManager am) {
    }

    @Override
    public String getMusicPath() {
        return "Music/TitleMusic.ogg";
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);

        if (!Main.getUserInput().getKeyState("Pause")) {
        	refreshButtons = true;
        }
        if (Main.getUserInput().getKeyState("Pause") && refreshButtons) {
        	refreshPause = true;
        }
        if (Main.getUserInput().isKeyClicked("Pause") && refreshPause) {
            this.switchStates(new StrifeCharacterSelectState(allPlayers));
        }
    }
}
