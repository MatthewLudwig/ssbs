package states.game;

import com.jme3.math.Vector2f;
import com.jme3.scene.Node;

import physics.BoundingBox;
import engine.utility.Utilities;
import entities.IrregularObstacle;
import entities.Obstacle;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.ViolentEntity;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import physics.PhysicsModifier;
import states.game.stages.Stage;

/**
 *
 * @author Matthew
 */
public class World {
    protected Stage stage;
    
    private List<PhysicsEntity> entityList;
    private List<PlayableCharacter> playerList;
    private List<Obstacle> obstacleList;
    private int entityIndex;
    
    private List<Vector2f> usedSpawnPoints;
    
    private boolean disableDeath;

    public World(Stage stage) {
        this.stage = stage;

        this.entityList = new LinkedList<PhysicsEntity>();
        this.playerList = new LinkedList<PlayableCharacter>();
        this.obstacleList = new LinkedList<Obstacle>();
        this.entityIndex = 0;
        
        this.usedSpawnPoints = new LinkedList<Vector2f>();
        this.disableDeath = false;
    }
    
    public Stage getStage() {
        return this.stage;
    }
    
    public void setDisableDeath(boolean state) {
        this.disableDeath = state;
    }
    
    public Vector2f getSpawnPoint(boolean originalSpawn, int playerNum) {
        if (originalSpawn) {
            Vector2f spawnPoint = this.stage.getSpawnPoint();
            
            while (this.usedSpawnPoints.contains(spawnPoint)) {
                spawnPoint = this.stage.getSpawnPoint();
            }
            
            this.usedSpawnPoints.add(spawnPoint);
            
            return spawnPoint;
        } else {
            BoundingBox cameraBounds = this.stage.getCameraBounds();
            float yOff = (cameraBounds.getUpperMidpoint().y - cameraBounds.getLowerMidpoint().y) / 4;
            return cameraBounds.getExactCenter().addLocal((playerNum-2.5f)*60, yOff);
        }
    }

    public void spawnObject(PhysicsEntity object, Node displayNode, Node menuNode, boolean alwaysDraw) {
        this.entityList.add(object);
        
        if (object instanceof PlayableCharacter) {
            this.playerList.add((PlayableCharacter) object);
        }
        
        if (object instanceof Obstacle) {
            this.obstacleList.add((Obstacle) object);
        }
        
        object.setUpEntity(this.entityIndex++, displayNode, menuNode, alwaysDraw);
    }
    
    public List<PhysicsEntity> getPhysicsList() {
        return this.entityList;
    }
    
    public List<PlayableCharacter> getPlayerList() {
        return this.playerList;
    }
    
    public List<Obstacle> getObstacleList() {
        return this.obstacleList;
    }

    public PlayableCharacter getClosestPlayerToCharacter(PlayableCharacter character) {
        return this.getClosestPlayer(character.getAppropriateBounds().getLowerMidpoint(), character);
    }

    public PlayableCharacter getClosestPlayer(Vector2f target, PlayableCharacter... exclusions) {
        PlayableCharacter answer = null;
        float currentDistance = 0;
        List exclusionList = Arrays.asList(exclusions);

        for (PlayableCharacter possibility : this.playerList) {
            if (!possibility.isDead()) {
                float distance = possibility.getAppropriateBounds().getLowerMidpoint().distanceSquared(target);

                if (!exclusionList.contains(possibility) && (answer == null || distance < currentDistance)) {
                    currentDistance = distance;
                    answer = possibility;
                }
            }
        }

        return answer;
    }
    
    public Obstacle getClosestObstacle(Vector2f target, Obstacle... exclusions) {
        Obstacle answer = null;
        float currentDistance = 0;
        List exclusionList = Arrays.asList(exclusions);

        for (Obstacle possibility : this.obstacleList) {
            if (!possibility.isDead()) {
                float distance = possibility.getAppropriateBounds().getLowerMidpoint().distanceSquared(target);

                if (!exclusionList.contains(possibility) && (answer == null || distance < currentDistance)) {
                    currentDistance = distance;
                    answer = possibility;
                }
            }
        }

        return answer;
    }

    public PhysicsEntity getClosestObject(Vector2f target, PhysicsEntity... exclusions) {
        PhysicsEntity answer = null;
        float currentDistance = 0;
        List exclusionList = Arrays.asList(exclusions);

        for (PhysicsEntity possibility : this.entityList) {
            float distance = possibility.getAppropriateBounds().getLowerMidpoint().distanceSquared(target);

            if (!exclusionList.contains(possibility) && (answer == null || distance < currentDistance)) {
                currentDistance = distance;
                answer = possibility;
            }
        }

        return answer;
    }

    protected void setUpDisplay(Node displayNode) {
        this.stage.attachBackground(displayNode);
    }

    protected void update() {
        List<PhysicsEntity> safeList = new LinkedList<PhysicsEntity>(this.entityList);
        
        for (PhysicsEntity entity : safeList) {
            entity.updateEntity();
        }
        
        if (!this.disableDeath) {
            this.killEntities();
        }
        
        this.updatePhysics();
        this.removeDeadObjects();
        this.stage.update(Utilities.lockedTPF);
    }

    /**
     * Function that updates the physics for all objects within the physics
     * list. This includes gravity and collisions with walls.
     */
    private void updatePhysics() {
        Chunk initialChunk = new Chunk(0, 0, this.stage.getSizeX(), this.stage.getSizeY());
        
        for(PhysicsEntity entity : this.entityList)
        {
            if(!entity.hasPhysicsModifier(PhysicsModifier.NOGRAVITY))
            {
                entity.addToVelocity(0, -58.45f * this.stage.getGravity() * Utilities.lockedTPF, false);
            }
            
            initialChunk.addEntity(entity);
            
            if (!entity.hasPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION)) {
                entity.collideWithStage(this.stage);
                if (!entity.isOnGround())
                	for (IrregularObstacle irr : this.stage.getIrregulars()){
                		entity.collideWithIrregular(irr);
                		if (entity.isOnGround())
                			break;
                	}
            }
        }
     
        for (Chunk chunk : initialChunk.splitChunk()) {
            if (chunk.getNumberOfEntities() > 10) {
                for (Chunk miniChunk : chunk.splitChunk()) {
                    miniChunk.collideWithObstacles();
                    miniChunk.collideWithObjects();
                }
            } else {
                chunk.collideWithObstacles();
                chunk.collideWithObjects();
            }
        }
    }

    public void killEntities() {
    	List<PhysicsEntity> deadList = new LinkedList<PhysicsEntity>();
        for (PhysicsEntity entity : this.entityList) {
            if (entity instanceof ViolentEntity) {
                BoundingBox stageBounds = this.stage.getWorldBounds();
                Vector2f entityCenter = entity.getAppropriateBounds().getExactCenter();
                ViolentEntity character = (ViolentEntity) entity;
                
                if (entityCenter.y > stageBounds.getUpperMidpoint().y && character.canDieOffTop()) {
                    deadList.add(entity);
                }
                
                if (entityCenter.x > stageBounds.getRightMidpoint().x && character.canDieOffSides()) {
                	deadList.add(entity);
                }
                
                if (entityCenter.x < stageBounds.getLeftMidpoint().x && character.canDieOffSides()) {
                	deadList.add(entity);
                }
                
                if (entityCenter.y < stageBounds.getLowerMidpoint().y  && character.canDieOffBottom()) {
                	deadList.add(entity);
                }
            } 
            else if (!this.stage.getWorldBounds().isInBounds(entity.getAppropriateBounds().getExactCenter())) {
            	deadList.add(entity);
            }
        }
        for (PhysicsEntity entity : deadList){
        	entity.setDead(true);
        }
    }

    private void removeDeadObjects() {
        List<PhysicsEntity> safeList = new LinkedList<PhysicsEntity>(this.getPhysicsList());
            
        for(PhysicsEntity entity : safeList)
        {
            if(entity.isDead())
            {
                this.getPhysicsList().remove(entity);
                entity.cleanUpEntity();
            }
        }
    }
}
