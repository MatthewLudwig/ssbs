package states.game;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.ui.Picture;

import engine.Main;
import engine.ssbs.CharacterList.CharacterMapping;
import engine.PictureLayer;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import entities.Team;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import states.MainMenuState;
import states.SimpleAppState;
import states.game.GameRules.ItemFrequency;

/**
 *
 *
 * @author Matthew Ludwig
 */
public abstract class CharacterSelectState extends SimpleAppState {
    protected SelectedCharacter[] characters;
    protected int requiredCharacters;
    protected float updateLimiter;
    
    protected boolean wasLeftClicked;
    protected boolean wasRightClicked;
    protected boolean wasMiddleClicked;
    
    protected GameRules gameRules;
    
    protected int limitedNumberOfLives;
    protected boolean allowControllerChange;
    
    protected float clickDelay;

    public CharacterSelectState(int numberOfPlayers, int requiredCharacters, GameRules rules, PlayableCharacter... passedInCharacters) {
        super("Character Select", true);
        
        SelectedCharacter.clearColorGuards();
        
        this.characters = new SelectedCharacter[numberOfPlayers];
        this.requiredCharacters = requiredCharacters;
        this.updateLimiter = 0;
        
        this.wasLeftClicked = false;
        this.wasRightClicked = false;
        this.wasMiddleClicked = false;
        
        this.limitedNumberOfLives = 0;
        this.allowControllerChange = true;
        
        this.gameRules = rules;
        
        this.clickDelay = 0;
        
        for (int count = 0; count < this.characters.length; count++) {
            if (count < passedInCharacters.length && passedInCharacters[count] != null) {
                this.characters[count] = new SelectedCharacter(
                        passedInCharacters[count].isFromRandom() ? Main.getCharacterList().getCharacter(50) : Main.getCharacterList().getCharacter(passedInCharacters[count]), 
                        passedInCharacters[count].getInitialNumberOfLives(), 
                        passedInCharacters[count].getControllerName().equals("CPU") ? Integer.valueOf(passedInCharacters[count].getControllerInfo()) + 1 : 0,
                        passedInCharacters[count].getColorNumber(),
                        passedInCharacters[count].team);
            } else {
                this.characters[count] = new SelectedCharacter(Team.values()[count]);
            }
        }
    }
    
    protected final Node getCharacterPortraits() {
        Node node = new Node("Character Portraits");
        
        List<Entry<Integer, CharacterMapping>> unlockedCharacters = Main.getCharacterList().getUnlockedCharacters();
        
        int width = 1;
        int height = 1;
        int rem = 0;
        while (width * height + rem < unlockedCharacters.size()) {
            if (height < ((double) (width)) / 2.0) {
                int tempHeight = height + 1;
                int tempWidth = width;
                while (tempWidth * tempHeight > width * height)
                        tempWidth--;
                rem = width * height + rem - tempWidth * tempHeight;
                width = tempWidth;
                height = tempHeight;
            }
            rem++;
            if (rem >= height) {
                rem -= height;
                width++;
            }
        }
        
        float scale = Math.min((10f / (float) (width + (rem != 0 ? 1 : 0))), (5f / (float) height));
        float xOff = 24 * (10 - ((width + (rem != 0 ? 1 : 0)) * scale));
        float yOff = 18 * (5 - (height * scale));
        
        node.setLocalTranslation(80 - (rem == 0 ? 24 * scale : 0) + (xOff), 400 - (12 * (scale)) - (yOff), -1);
        node.setLocalScale(scale);
        
        int x = 0, y = 0;
        
        for (Entry<Integer, CharacterMapping> e : unlockedCharacters) {
            int characterIndex = e.getKey();
            CharacterMapping mapping = e.getValue();
            
            if (rem != 0) {
                node.attachChild(Utilities.getJMELoader().getPicture("Character Portrait #" + characterIndex, mapping.getPortraitPath(), (x * 48), -(y * 36), PictureLayer.GUI));

                if (++x == (width + 1)) {
                    x = 0;
                    y++;
                    rem--;
                }
            } else {
                node.attachChild(Utilities.getJMELoader().getPicture("Character Portrait #" + characterIndex, mapping.getPortraitPath(), 24 + (x * 48), -(y * 36), PictureLayer.GUI));

                if (++x == width) {
                    x = 0;
                    y++;
                }
            }
        }

        return node;
    }

    protected final Node getSelectionPortraits() {
        Node node = new Node("Player Portraits");

        for (int count = 1; count <= this.characters.length; count++) {
            Node portrait = new Node("Player Portrait #" + count);
            portrait.setLocalTranslation(5 + (count * 75), 50, 0);
            node.attachChild(portrait);
        }

        return node;
    }
    
    protected final Node getItemPortraits() {
        Node node = new Node("Item Portraits");
        
        node.attachChild(Utilities.getJMELoader().getText(Utilities.Impact16, ColorRGBA.White, "Items", 400, 200));
        node.attachChild(Utilities.getJMELoader().getPicture("None", 
                this.gameRules.getItemFrequency() == ItemFrequency.NONE ? "Interface/MatchSetup/noneSelected.png" : "Interface/MatchSetup/none.png", 400, 175, PictureLayer.GUI));
        node.attachChild(Utilities.getJMELoader().getPicture("Low", 
                this.gameRules.getItemFrequency() == ItemFrequency.LOW ? "Interface/MatchSetup/lowSelected.png" : "Interface/MatchSetup/low.png", 400, 150, PictureLayer.GUI));
        node.attachChild(Utilities.getJMELoader().getPicture("Medium", 
                this.gameRules.getItemFrequency() == ItemFrequency.MEDIUM ? "Interface/MatchSetup/mediumSelected.png" : "Interface/MatchSetup/medium.png", 400, 125, PictureLayer.GUI));
        node.attachChild(Utilities.getJMELoader().getPicture("High", 
                this.gameRules.getItemFrequency() == ItemFrequency.HIGH ? "Interface/MatchSetup/highSelected.png" : "Interface/MatchSetup/high.png", 400, 100, PictureLayer.GUI));
        node.attachChild(Utilities.getJMELoader().getPicture("Insane", 
                this.gameRules.getItemFrequency() == ItemFrequency.INSANE ? "Interface/MatchSetup/insaneSelected.png" : "Interface/MatchSetup/insane.png", 400, 75, PictureLayer.GUI));
        node.attachChild(Utilities.getJMELoader().getPicture("Smash Ball", 
                this.gameRules.isItemOn("Smash Ball")? "Interface/MatchSetup/smashBallSelected.png" : "Interface/MatchSetup/smashBall.png", 450, 175, PictureLayer.GUI));
        node.attachChild(Utilities.getJMELoader().getPicture("Pokeball", 
                this.gameRules.isItemOn("Pokeball")? "Interface/MatchSetup/pokeballSelected.png" : "Interface/MatchSetup/pokeball.png", 480, 175, PictureLayer.GUI));
        node.attachChild(Utilities.getJMELoader().getPicture("Super Mushroom", 
                this.gameRules.isItemOn("Super Mushroom")? "Interface/MatchSetup/superMushroomSelected.png" : "Interface/MatchSetup/superMushroom.png", 480, 55, PictureLayer.GUI));
        node.attachChild(Utilities.getJMELoader().getPicture("Thinker Statue", 
                this.gameRules.isItemOn("Thinker Statue")? "Interface/MatchSetup/thinkerStatueSelected.png" : "Interface/MatchSetup/thinkerStatue.png", 480, 25, PictureLayer.GUI));
        node.attachChild(Utilities.getJMELoader().getPicture("Warped Mirror", 
                this.gameRules.isItemOn("Warped Mirror")? "Interface/MatchSetup/warpedMirrorSelected.png" : "Interface/MatchSetup/warpedMirror.png", 510, 115, PictureLayer.GUI));
        node.attachChild(Utilities.getJMELoader().getPicture("Poison Mushroom", 
                this.gameRules.isItemOn("Poison Mushroom")? "Interface/MatchSetup/poisonMushroomSelected.png" : "Interface/MatchSetup/poisonMushroom.png", 510, 55, PictureLayer.GUI));
        node.attachChild(Utilities.getJMELoader().getPicture("Luigi Mushroom", 
                this.gameRules.isItemOn("Luigi Mushroom")? "Interface/MatchSetup/luigiMushroomSelected.png" : "Interface/MatchSetup/luigiMushroom.png", 540, 55, PictureLayer.GUI));

        return node;
    }

    @Override
    public void attachToPausableNode(Node node, AssetManager am) {
        
    }

    @Override
    public String getMusicPath() {
        return "Music/MainMenuMusic.ogg";
    }
    
    public void customUpdateBackButton(float xPosition, float yPosition){
    	this.updateBackButton(MainMenuState.class, xPosition, yPosition);
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        
        this.customUpdateBackButton(5, 420);
        
        this.clickDelay += tpf;
        
        for (int count = 1; count <= this.characters.length; count++) {
            SelectedCharacter character = this.characters[count - 1];
            Node player = (Node) ((Node) this.absoluteNode.getChild("Player Portraits")).getChild("Player Portrait #" + count);

            if (character.hasChanged()) {
                player.detachAllChildren();
                
                BitmapText lives, type, name;
                Picture portrait, pose, franchise;
                
                lives = Utilities.getJMELoader().getCenteredText(ColorRGBA.White, "x" + character.getNumberOfLives(), 25, 0);
                lives.setName("Lives");

                if (character.isRemoved()) {
                    type = Utilities.getJMELoader().getText(Utilities.Impact11, ColorRGBA.White, "", 2, 135);
                    portrait = Utilities.getJMELoader().getPicture("Portrait", "Interface/MatchSetup/selection_none.png", 0, 50, PictureLayer.GUI);
                } else if (character.getAILevel() != 0) {
                    type = Utilities.getJMELoader().getText(Utilities.Impact11, ColorRGBA.White, "CPU", 2, 132);
                    BitmapText cpuText = Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, "Level:  " + character.getAILevel(), 25, 150);
                    cpuText.setName("CPU Text");
                    player.attachChild(cpuText);
                    portrait = Utilities.getJMELoader().getPicture("Portrait", "Interface/MatchSetup/selection_cpu.png", 0, 50, PictureLayer.GUI);
                } else {
                    type = Utilities.getJMELoader().getText(Utilities.Impact11, ColorRGBA.White, "P" + count, 4, 132);
                    portrait = Utilities.getJMELoader().getPicture("Portrait", "Interface/MatchSetup/selection_" + character.getTeam().displayName.toLowerCase() + ".png", 0, 50, PictureLayer.GUI);
                }
                                
                if (character.getMapping() != null) {
                    String displayName = character.getMapping().getName()
                            .replace("Captain Falcon", "C. Falcon")
                            .replace("King Dedede", "Dedede")
                            .replace("King K Rool", "K. Rool")
                            .replace("Professor Layton", "P. Layton");
                    
                    name = Utilities.getJMELoader().getCenteredText(Utilities.Impact11, ColorRGBA.White, displayName, 25, 64);
                    
                    pose = Utilities.getJMELoader().getPicture("Select Pose", character.getMapping().getSelectedPath(character.getColorNumber()), 10, 80, PictureLayer.CURSOR);
                    pose.move((36 - pose.getLocalScale().x) / 2, 0, 0);

                    pose.move(character.getMapping().getDispX(), character.getMapping().getDispY(), 0);
                    
                    if (character.getAILevel() != 0) {
                        franchise = Utilities.getJMELoader().getPicture("Franchise", character.getMapping().getFranchisePath().replace("#", "cpu"), 5, 100, PictureLayer.GUI);
                    } else {
                        franchise = Utilities.getJMELoader().getPicture("Franchise", character.getMapping().getFranchisePath().replace("#", character.getTeam().displayName.toLowerCase()), 5, 100, PictureLayer.GUI);
                    }
                } else {
                    name = Utilities.getJMELoader().getText(Utilities.Impact11, ColorRGBA.White, "", 7, 66);
                    pose = Utilities.getJMELoader().getPicture("Select Pose", "Sprites/Miscellaneous/blankSprite.png", 10, 80, PictureLayer.SPECIALLAYER);
                    franchise = Utilities.getJMELoader().getPicture("Franchise", "Sprites/Miscellaneous/blankSprite.png", 5, 100, PictureLayer.GUI);
                }

                player.attachChild(portrait);
                player.attachChild(pose);
                player.attachChild(franchise);
                
                if (this.limitedNumberOfLives == 0) {
                    player.attachChild(lives);
                }
                
                player.attachChild(type);
                player.attachChild(name);
            }
        }
        
        if (this.gameRules.hasChanged()) {
            this.absoluteNode.detachChildNamed("Item Portraits");
            this.absoluteNode.attachChild(this.getItemPortraits());
        }
        
        if (this.updateLimiter >= .07f) {
            if (this.wasLeftClicked && this.wasRightClicked) {
                this.updateCharacterPortrait(3);
            } else {
                if (this.wasLeftClicked) {
                    this.updateCharacterPortrait(1);
                    this.dealWithInput(true);
                } else if (this.wasRightClicked) {
                    this.updateCharacterPortrait(2);
                    this.dealWithInput(false);
                } else if (this.wasMiddleClicked) {
                    this.updateCharacterPortrait(4);
                }
            }
            
            this.wasLeftClicked = false;
            this.wasRightClicked = false;
            this.wasMiddleClicked = false;
            this.updateLimiter -= .07f;
        } else {
            this.wasLeftClicked = this.wasLeftClicked ? true : Main.getUserInput().isLeftButtonClicked();
            this.wasRightClicked = this.wasRightClicked ? true : Main.getUserInput().isRightButtonClicked();
            this.wasMiddleClicked = this.wasMiddleClicked ? true : Main.getUserInput().isMiddleButtonClicked();
            this.updateLimiter += Utilities.lockedTPF;
        }
        
        for (int count = 0; count < this.characters.length; count++) {
            if (Main.getUserInput().isKeyClicked("P" + (count + 1) + " Shield") && 
                    this.characters[count].getMapping() != null) {
                this.playSelectSound();
                
                if (this.gameRules.isTeamBattleOn()) {
                    this.characters[count].nextTeam();
                } else {
                    this.characters[count].incrementColorNumber(1);
                }
            }
        }

        this.checkForCompleteness();
    }
    
    private void updateCharacterPortrait(int number) {
        if (number - 1 < this.characters.length) {
            Node characterPortraits = (Node) this.absoluteNode.getChild("Character Portraits");

            for (Spatial object : characterPortraits.getChildren()) {
                if (Utilities.getPhysicsUtility().complex2DCollisionWithScale(this.cursorPosition.subtract(characterPortraits.getLocalTranslation().x, characterPortraits.getLocalTranslation().y), (Picture) object)) {
                    this.playSelectSound();

                    CharacterMapping mapping = Main.getCharacterList().getCharacter(Integer.valueOf(object.getName().split("#")[1]));
                    
                    if (mapping != null && mapping.isUnlocked()) {
                        this.characters[number - 1].setMapping(mapping);
                        this.characters[number - 1].setRemoved(false);
                    }
                }
            }
        }
    }
    
    private void dealWithInput(boolean inputType) {
        Node playerPortraits = (Node) this.absoluteNode.getChild("Player Portraits");

        if (inputType) {
            for (int count = 0; count < this.characters.length; count++) {
                Node portrait = (Node) playerPortraits.getChildren().get(count);
                SelectedCharacter character = this.characters[count];

                if (Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition.subtract(new Vector2f(playerPortraits.getLocalTranslation().x + portrait.getLocalTranslation().x, playerPortraits.getLocalTranslation().y + portrait.getLocalTranslation().y)), (Picture) portrait.getChild("Select Pose"))) {
                    this.playSelectSound();
                    
                    if (this.gameRules.isTeamBattleOn()) {
                        this.characters[count].nextTeam();
                    } else {
                        this.characters[count].incrementColorNumber(1);
                    }
                } else if (Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition.subtract(new Vector2f(playerPortraits.getLocalTranslation().x + portrait.getLocalTranslation().x, playerPortraits.getLocalTranslation().y + portrait.getLocalTranslation().y)), (Picture) portrait.getChild("Portrait")) && this.allowControllerChange) {
                    this.playSelectSound();

                    if (character.isRemoved()) {
                        character.setRemoved(false);
                        character.setAILevel(0);
                    } else {
                        if (character.getAILevel() != 0) {
                            character.setRemoved(true);
                        } else {
                            character.setAILevel(3);
                        }
                    }
                } else if (!character.isRemoved() && character.getAILevel() != 0 && Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition.subtract(new Vector2f(playerPortraits.getLocalTranslation().x + portrait.getLocalTranslation().x, playerPortraits.getLocalTranslation().y + portrait.getLocalTranslation().y - 25)), (BitmapText) portrait.getChild("CPU Text")) && this.allowControllerChange) {
                    this.playSelectSound();
                    
                    if (character.getAILevel() + 1 == 10) {
                        character.setAILevel(1);
                    } else {
                        character.setAILevel(character.getAILevel() + 1);
                    }
                }

                if (this.limitedNumberOfLives == 0) {
                    if (Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition.subtract(new Vector2f(playerPortraits.getLocalTranslation().x + portrait.getLocalTranslation().x, playerPortraits.getLocalTranslation().y + portrait.getLocalTranslation().y - 50)), (BitmapText) portrait.getChild("Lives"))) {
                        this.playSelectSound();
                        
                        character.setNumberOfLives(Math.min(character.getNumberOfLives() + 1, 99));
                    }
                }
            }

            if (this.absoluteNode.getChild("Banner") != null && Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition, (Picture) this.absoluteNode.getChild("Banner"))) {
                this.switchToNextState();
            }
            
            if (this.absoluteNode.getChild("Item Portraits") != null) {
                for (Spatial s : ((Node) this.absoluteNode.getChild("Item Portraits")).getChildren()) {
                    if (s.getName() != null && Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition, (Picture) s)) {
                        this.playSelectSound();
                        
                        if (s.getName().equals("None")) {
                            this.gameRules.setItemFrequency(ItemFrequency.NONE);
                        } else if (s.getName().equals("Low")) {
                            this.gameRules.setItemFrequency(ItemFrequency.LOW);
                        } else if (s.getName().equals("Medium")) {
                            this.gameRules.setItemFrequency(ItemFrequency.MEDIUM);
                        } else if (s.getName().equals("High")) {
                            this.gameRules.setItemFrequency(ItemFrequency.HIGH);
                        } else if (s.getName().equals("Insane")) {
                            this.gameRules.setItemFrequency(ItemFrequency.INSANE);
                        } else {
                            this.gameRules.setItemOn(s.getName(), !this.gameRules.isItemOn(s.getName()));
                        }
                    }
                }
            }
        } else {
            for (int count = 0; count < this.characters.length; count++) {
                Node portrait = (Node) playerPortraits.getChildren().get(count);
                SelectedCharacter character = this.characters[count];
                
                if (Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition.subtract(new Vector2f(playerPortraits.getLocalTranslation().x + portrait.getLocalTranslation().x, playerPortraits.getLocalTranslation().y + portrait.getLocalTranslation().y)), (Picture) portrait.getChild("Select Pose"))) {
                    this.playSelectSound();
                    
                    if (this.gameRules.isTeamBattleOn()) {
                        this.characters[count].prevTeam();
                    } else {
                        this.characters[count].incrementColorNumber(-1);
                    }
                } else if (Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition.subtract(new Vector2f(playerPortraits.getLocalTranslation().x + portrait.getLocalTranslation().x, playerPortraits.getLocalTranslation().y + portrait.getLocalTranslation().y)), (Picture) portrait.getChild("Portrait")) && this.allowControllerChange) {
                    this.playSelectSound();
                    
                    if (character.isRemoved()) {
                        character.setRemoved(false);
                        character.setAILevel(3);
                    } else {
                        if (character.getAILevel() == 0) {
                            character.setRemoved(true);
                        } else {
                            character.setAILevel(0);
                        }
                    }
                } else if (character.getAILevel() != 0 && Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition.subtract(new Vector2f(playerPortraits.getLocalTranslation().x + portrait.getLocalTranslation().x, playerPortraits.getLocalTranslation().y + portrait.getLocalTranslation().y - 25)), (BitmapText) portrait.getChild("CPU Text")) && this.allowControllerChange) {
                    this.playSelectSound();
                    
                    if (character.getAILevel() - 1 == 0) {
                        character.setAILevel(9);
                    } else {
                        character.setAILevel(character.getAILevel() - 1);
                    }
                }

                if (this.limitedNumberOfLives == 0) {
                    if (Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition.subtract(new Vector2f(new Vector2f(playerPortraits.getLocalTranslation().x + portrait.getLocalTranslation().x, playerPortraits.getLocalTranslation().y + portrait.getLocalTranslation().y - 50))), (BitmapText) portrait.getChild("Lives"))) {
                        this.playSelectSound();
                        
                        character.setNumberOfLives(Math.max(character.getNumberOfLives() - 1, 1));
                    }
                }
            }
        }
    }
    
    private void checkForCompleteness() {
        if (this.gameRules.isTeamBattleOn()) {
            List<Team> usedTeams = new LinkedList<Team>();
            
            for (SelectedCharacter character : this.characters) {
                if (character.getMapping() != null && !usedTeams.contains(character.getTeam())) {
                    usedTeams.add(character.getTeam());
                }
            }
            
            if (usedTeams.size() >= this.requiredCharacters) {
                if (this.absoluteNode.getChild("Banner") == null) {
                    this.absoluteNode.attachChild(Utilities.getJMELoader().getPicture("Banner", "Interface/MatchSetup/banner.png", 0, 225, PictureLayer.GUI));
                }
            } else {
                if (this.absoluteNode.getChild("Banner") != null) {
                    this.absoluteNode.detachChildNamed("Banner");
                }
            }
        } else {
            int count = 0;

            for (SelectedCharacter character : this.characters) {
                if (character.getMapping() != null) {
                    count++;
                }
            }

            if (count >= this.requiredCharacters) {
                if (this.absoluteNode.getChild("Banner") == null) {
                    this.absoluteNode.attachChild(Utilities.getJMELoader().getPicture("Banner", "Interface/MatchSetup/banner.png", 0, 225, PictureLayer.GUI));
                }
            } else {
                if (this.absoluteNode.getChild("Banner") != null) {
                    this.absoluteNode.detachChildNamed("Banner");
                }
            }
        }

        if (Main.getUserInput().isKeyClicked("Pause") && this.absoluteNode.getChild("Banner") != null && this.clickDelay > 0.25f) {
            this.switchToNextState();
        }
    }

    public void switchToNextState() {
        PlayableCharacter[] players = new PlayableCharacter[this.characters.length];
        
        for (int count = 0; count < players.length; count++) {
            players[count] = this.characters[count].getCharacter();
            
            if (this.limitedNumberOfLives != 0) {
                players[count].setNumberOfLives(this.limitedNumberOfLives);
            }
            
            short counter = 0;
            
            for (int c = 0; c < count; c++) {
            	if (players[count] != null && players[count].getDisplayName().equals(players[c].getName()) && players[count].isOnSameTeamAs(players[c])) {
            		counter++;
            	}
            }
            
            if (counter == 1) {
            	players[count].tint = players[count].team.colorOne;
            } else if (counter == 2) {
            	players[count].tint = players[count].team.colorTwo;
            }
        }
        
        this.switchStates(this.getNextState(players));
    }
    
    public abstract SimpleAppState getNextState(PlayableCharacter[] players);
}
