package states;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;
import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import engine.input.UserInput;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

/**
 *
 *
 * @author Matthew Ludwig
 */
public class OptionsMenuState extends SimpleAppState {
    private boolean state;
    private BitmapText controlText, soundText;
    private Node controlNode, soundNode;

    private Map<String, BitmapText> controlList;
    private Node textNode;
    
    private Slider sfxSlider;
    private Slider musicSlider;
    private Slider voicesSlider;
    private int selectedSlider;
    
    public OptionsMenuState() {
        super("Options Menu", true);
        
        this.state = true;
        this.controlText = Utilities.getJMELoader().getText(ColorRGBA.White, "Controls:", 150, 380);
        this.soundText = Utilities.getJMELoader().getText(ColorRGBA.Gray, "Sounds:", 400, 380);
        this.controlNode = new Node("Control Node");
        this.soundNode = new Node("Sound Node");
        
        this.controlList = new HashMap<String, BitmapText>();
        this.textNode = new Node("Text");
        this.selectedSlider = 0;
    }

    @Override
    public void attachToAbsoluteNode(Node node, AssetManager am) {
        node.attachChild(Utilities.getJMELoader().getPicture("Background", "Interface/Options/Background.png", 0, 0, PictureLayer.BACKGROUND));        
        
        node.attachChild(this.controlText);
        node.attachChild(this.soundText);
        
        this.setUpControlNode();
        this.setUpSoundNode();
        
        node.attachChild(this.controlNode);
    }
    
    public void setUpControlNode() {
        UserInput ui = Main.getUserInput();

        this.controlNode.attachChild(Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, "Up", 50, 350));
        this.controlNode.attachChild(Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, "Right", 125, 350));
        this.controlNode.attachChild(Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, "Down", 200, 350));
        this.controlNode.attachChild(Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, "Left", 275, 350));
        this.controlNode.attachChild(Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, "Attack", 350, 350));
        this.controlNode.attachChild(Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, "Special", 425, 350));
        this.controlNode.attachChild(Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, "Shield", 500, 350));
        this.controlNode.attachChild(Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, "Pause", 575, 350));
        
        this.controlNode.attachChild(this.textNode);
        
        controlList.put("P1 Up", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P1 Up"), 50, 300));
        controlList.put("P1 Right", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P1 Right"), 125, 300));
        controlList.put("P1 Down", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P1 Down"), 200, 300));
        controlList.put("P1 Left", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P1 Left"), 275, 300));
        controlList.put("P1 Attack", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P1 Attack"), 350, 300));
        controlList.put("P1 Special", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P1 Special"), 425, 300));
        controlList.put("P1 Shield", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P1 Shield"), 500, 300));
        controlList.put("P1 Pause", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P1 Pause"), 575, 300));
        controlList.put("P2 Up", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P2 Up"), 50, 250));
        controlList.put("P2 Right", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P2 Right"), 125, 250));
        controlList.put("P2 Down", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P2 Down"), 200, 250));
        controlList.put("P2 Left", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P2 Left"), 275, 250));
        controlList.put("P2 Attack", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P2 Attack"), 350, 250));
        controlList.put("P2 Special", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P2 Special"), 425, 250));
        controlList.put("P2 Shield", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P2 Shield"), 500, 250));
        controlList.put("P2 Pause", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P2 Pause"), 575, 250));
        controlList.put("P3 Up", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P3 Up"), 50, 200));
        controlList.put("P3 Right", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P3 Right"), 125, 200));
        controlList.put("P3 Down", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P3 Down"), 200, 200));
        controlList.put("P3 Left", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P3 Left"), 275, 200));
        controlList.put("P3 Attack", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P3 Attack"), 350, 200));
        controlList.put("P3 Special", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P3 Special"), 425, 200));
        controlList.put("P3 Shield", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P3 Shield"), 500, 200));
        controlList.put("P3 Pause", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P3 Pause"), 575, 200));
        controlList.put("P4 Up", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P4 Up"), 50, 150));
        controlList.put("P4 Right", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P4 Right"), 125, 150));
        controlList.put("P4 Down", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P4 Down"), 200, 150));
        controlList.put("P4 Left", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P4 Left"), 275, 150));
        controlList.put("P4 Attack", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P4 Attack"), 350, 150));
        controlList.put("P4 Special", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P4 Special"), 425, 150));
        controlList.put("P4 Shield", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P4 Shield"), 500, 150));
        controlList.put("P4 Pause", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("P4 Pause"), 575, 150));

        //controlList.put("Pause", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("Pause"), 125, 100));
        controlList.put("Exit", Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName("Exit"), 350, 100));

        for (BitmapText text : this.controlList.values()) {
            this.controlNode.attachChild(text);
        }
    }
    
    public void setUpSoundNode() {
        this.sfxSlider = new Slider(150, 350, Main.getGameSettings().getSFXVolume());
        this.musicSlider = new Slider(150, 320, Main.getGameSettings().getMusicVolume());
        this.voicesSlider = new Slider(150, 290, Main.getGameSettings().getVoicesVolume());
        
        this.soundNode.attachChild(Utilities.getJMELoader().getText(Utilities.Impact16, ColorRGBA.White, "SFX", 50, 350));
        this.soundNode.attachChild(Utilities.getJMELoader().getText(Utilities.Impact16, ColorRGBA.White, "Music", 50, 320));
        this.soundNode.attachChild(Utilities.getJMELoader().getText(Utilities.Impact16, ColorRGBA.White, "Voices", 50, 290));
        this.soundNode.attachChild(this.sfxSlider.getNode());
        this.soundNode.attachChild(this.musicSlider.getNode());
        this.soundNode.attachChild(this.voicesSlider.getNode());
    }

    @Override
    public void attachToPausableNode(Node node, AssetManager am) {
    }

    @Override
    public String getMusicPath() {
        return "Music/MainMenuMusic.ogg";
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        this.updateBackButton(MainMenuState.class, 565, 420);

        UserInput ui = Main.getUserInput();

        if (this.state) {
            for (Entry<String, BitmapText> e : this.controlList.entrySet()) {
                if (ui.isLeftButtonClicked() && Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition.subtract(0, -25), e.getValue())) {
                    this.playSelectSound();
                    this.textNode.detachAllChildren();
                    ui.setControlToNextEvent(e.getKey());
                    this.textNode.attachChild(Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, "Press new " + e.getKey() + " button!", 400, 440));
                }

                if (!ui.isSettingControl()) {
                    this.textNode.detachAllChildren();
                    this.controlNode.detachChild(e.getValue());
                    e.setValue(Utilities.getJMELoader().getCenteredText(Utilities.Impact16, ColorRGBA.White, ui.getKeyName(e.getKey()), e.getValue().getLocalTranslation().x + (e.getValue().getLineWidth() / 2), e.getValue().getLocalTranslation().y - e.getValue().getLineHeight()));
                    this.controlNode.attachChild(e.getValue());
                }
            }
            
            if (ui.isLeftButtonClicked() && 
                    Utilities.getPhysicsUtility().complex2DCollision(ui.getMousePosition().subtract(0, -25), this.soundText)) {
                this.playSelectSound();

                this.state = false;
                this.absoluteNode.detachChild(this.controlNode);
                this.absoluteNode.attachChild(this.soundNode);
                this.controlText.setColor(ColorRGBA.Gray);
                this.soundText.setColor(ColorRGBA.White);
            }
        } else {
            if (ui.getLeftButtonState()) {
                switch (this.selectedSlider) {
                    case 0:
                        if (Utilities.getPhysicsUtility().complex2DCollision(
                                ui.getMousePosition().subtract(this.sfxSlider.getNode().getLocalTranslation().x, this.sfxSlider.getNode().getLocalTranslation().y), 
                                (Picture) this.sfxSlider.getNode().getChild("Bar"))) {
                            this.selectedSlider = 1;
                        } else if (Utilities.getPhysicsUtility().complex2DCollision(
                                ui.getMousePosition().subtract(this.musicSlider.getNode().getLocalTranslation().x, this.musicSlider.getNode().getLocalTranslation().y), 
                                (Picture) this.musicSlider.getNode().getChild("Bar"))) {
                            this.selectedSlider = 2;
                        } else if (Utilities.getPhysicsUtility().complex2DCollision(
                                ui.getMousePosition().subtract(this.voicesSlider.getNode().getLocalTranslation().x, this.voicesSlider.getNode().getLocalTranslation().y), 
                                (Picture) this.voicesSlider.getNode().getChild("Bar"))) {
                            this.selectedSlider = 3;
                        }
                        break;
                    case 1:
                        this.sfxSlider.setSliderPosition(ui.getMousePosition().x);
                        Main.getGameSettings().setSFXVolume(this.sfxSlider.getOffsetPercent());
                        break;
                    case 2:
                        this.musicSlider.setSliderPosition(ui.getMousePosition().x);
                        Main.getGameSettings().setMusicVolume(this.musicSlider.getOffsetPercent());
                        break;
                    case 3:
                        this.voicesSlider.setSliderPosition(ui.getMousePosition().x);
                        Main.getGameSettings().setVoicesVolume(this.voicesSlider.getOffsetPercent());
                        break;
                    default:
                        Main.log(Level.SEVERE, "Unknown slider selected:  " + this.selectedSlider, null);
                }
            } else if (ui.isLeftButtonClicked()) {
                switch (this.selectedSlider) {
                    case 0:
                        
                    case 1:
                        this.playSelectSound();
                        break;
                    case 2:
                        break;
                    case 3:
                        AudioNode marioSound = Utilities.getCustomLoader().getAudioNode("/Character Sounds/Mario/neutralA.ogg");
                        marioSound.setVolume(Main.getGameSettings().getVoicesVolume());
                        marioSound.playInstance();
                        break;
                    default:
                        Main.log(Level.SEVERE, "Unknown slider selected:  " + this.selectedSlider, null);
                }
                
                this.selectedSlider = 0;
            }
            
            if (ui.isLeftButtonClicked() && 
                    Utilities.getPhysicsUtility().complex2DCollision(ui.getMousePosition().subtract(0, -25), this.controlText)) {
                this.playSelectSound();

                this.state = true;
                this.absoluteNode.attachChild(this.controlNode);
                this.absoluteNode.detachChild(this.soundNode);
                this.controlText.setColor(ColorRGBA.White);
                this.soundText.setColor(ColorRGBA.Gray);
            }
        }
    }
}
