package states;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioNode;
import com.jme3.audio.AudioSource;
import com.jme3.math.Vector2f;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;
import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;

import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import states.game.GameState;

/**
 * Base class that all App States extend from.
 *
 * @author Matthew Ludwig
 */
public abstract class SimpleAppState extends AbstractAppState {
    private static AudioNode selectSound;
    private static AudioNode backSound;
    private static AudioNode backgroundMusic;
    private static String backgroundMusicPath;
    private static Picture backgroundSlitOne;
    private static Picture backgroundSlitTwo;
    private static Picture backgroundSlitThree;
    private static Picture backgroundSlitFour;
    
    protected AppStateManager stateManager;
    protected SimpleApplication app;
    protected Node absoluteNode;
    protected Node pausableNode;
    protected boolean hasMusic;
    protected boolean hasCursor;
    protected Vector2f cursorPosition;
    protected Picture cursor;
    
    private boolean mouseStatus;
    private float mouseEscapeTime;

    public SimpleAppState(String stateName, boolean hasCursor) {
        super();

        this.absoluteNode = new Node(stateName + " Absolute Node");
        this.pausableNode = new Node(stateName + " Pausable Node");

        this.hasMusic = true;
        this.hasCursor = hasCursor;
        
        this.cursorPosition = new Vector2f();
        this.mouseStatus = true;
        this.mouseEscapeTime = 0f;
    }

    @Override
    public final void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        
        if (selectSound == null && backSound == null) {
            selectSound = Utilities.getCustomLoader().getAudioNode("/Common Sounds/select.ogg");
            backSound = Utilities.getCustomLoader().getAudioNode("/Common Sounds/back.ogg");
        }

        this.stateManager = stateManager;
        this.app = (SimpleApplication) app;

        this.attachToAbsoluteNode(this.absoluteNode, this.app.getAssetManager());
        this.attachToPausableNode(this.pausableNode, this.app.getAssetManager());

        if (backgroundMusicPath == null || this.getMusicPath().equals(backgroundMusicPath)) {
            backgroundMusicPath = this.getMusicPath();
            
            if (backgroundMusic == null) {
                backgroundMusic = Utilities.getCustomLoader().getAudioNode(backgroundMusicPath);
            }
        } else {
            backgroundMusic.stop();
            backgroundMusicPath = this.getMusicPath();
            backgroundMusic = Utilities.getCustomLoader().getAudioNode(backgroundMusicPath);
        }
        
        backgroundMusic.setVolume(this.hasMusic ? Main.getGameSettings().getMusicVolume() : 0);
        backgroundMusic.setLooping(true);
        
        this.app.getGuiNode().attachChild(this.absoluteNode);

        if (this.hasCursor) {
            this.cursor = Utilities.getJMELoader().getPicture("Cursor", "Interface/General/Cursor.png", Main.getUserInput().getMousePosition().x, Main.getUserInput().getMousePosition().y, PictureLayer.CURSOR);
            this.absoluteNode.attachChild(this.cursor);
        }
        
        if (backgroundSlitOne == null) {
            backgroundSlitOne = Utilities.getJMELoader().getPicture("Slit One", "Interface/Slit.png", 0, 0, PictureLayer.SPECIALLAYER);
            backgroundSlitTwo = Utilities.getJMELoader().getPicture("Slit One", "Interface/Slit.png", 0, 0, PictureLayer.SPECIALLAYER);
            backgroundSlitThree = Utilities.getJMELoader().getPicture("Slit One", "Interface/Slit.png", 0, 0, PictureLayer.SPECIALLAYER);
            backgroundSlitFour = Utilities.getJMELoader().getPicture("Slit One", "Interface/Slit.png", 0, 0, PictureLayer.SPECIALLAYER);
        }

        this.setEnabled(true);
    }

    public abstract void attachToAbsoluteNode(Node node, AssetManager am);
    public abstract void attachToPausableNode(Node node, AssetManager am);
    public abstract String getMusicPath();

    @Override
    public final void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        if (enabled) {
            this.app.getGuiNode().attachChild(this.pausableNode);
        } else {
            this.app.getGuiNode().detachChild(this.pausableNode);
        }
    }

    @Override
    public void update(float tpf) {
        this.updateCursor();
        
        if (backgroundMusic.getStatus() != null && backgroundMusic.getStatus() != AudioSource.Status.Playing) {
            backgroundMusic.play();
        } else if (backgroundMusic.getVolume() != (this.hasMusic ? Main.getGameSettings().getMusicVolume() : 0)) {
            backgroundMusic.setVolume(this.hasMusic ? Main.getGameSettings().getMusicVolume() : 0);
        }
        
        if (Main.getGameSettings().isFullscreen()) {
            Vector2f resolution = new Vector2f(Display.getDesktopDisplayMode().getWidth(), Display.getDesktopDisplayMode().getHeight());
            
            float scale = Math.min(resolution.x / 640, resolution.y / 480);
            float xOff = (resolution.x - (640 * scale)) / 2;
            float yOff = (resolution.y - (480 * scale)) / 2;
            float slitWidth = xOff / scale;
            float slitHeight = yOff / scale;
            
            backgroundSlitOne.setLocalScale(xOff == 0 ? resolution.x : slitWidth, yOff == 0 ? resolution.y : slitHeight, 1);
            backgroundSlitTwo.setLocalScale(xOff == 0 ? resolution.x : slitWidth, yOff == 0 ? resolution.y : slitHeight, 1);
            backgroundSlitThree.setLocalScale(xOff == 0 ? resolution.x : slitWidth, yOff == 0 ? resolution.y : slitHeight, 1);
            backgroundSlitFour.setLocalScale(xOff == 0 ? resolution.x : slitWidth, yOff == 0 ? resolution.y : slitHeight, 1);
            
            backgroundSlitOne.setLocalTranslation(0 - (slitWidth), 0 - (slitHeight), 10);
            backgroundSlitTwo.setLocalTranslation((resolution.x / scale) - (2 * slitWidth), 0 - (slitHeight), 10);
            backgroundSlitThree.setLocalTranslation(0 - (slitWidth), 0 - (slitHeight), 10);
            backgroundSlitFour.setLocalTranslation((resolution.x / scale) - (2 * slitWidth), 0 - (slitHeight), 10);
            
            this.absoluteNode.setLocalScale(scale);
            this.pausableNode.setLocalScale(scale);
            this.absoluteNode.setLocalTranslation(xOff, yOff, 0);
            this.pausableNode.setLocalTranslation(xOff, yOff, 0);
            
            this.absoluteNode.attachChild(backgroundSlitOne);
            this.absoluteNode.attachChild(backgroundSlitTwo);
            this.pausableNode.attachChild(backgroundSlitThree);
            this.pausableNode.attachChild(backgroundSlitFour);
        } else {
            this.absoluteNode.setLocalScale(1);
            this.pausableNode.setLocalScale(1);
            this.absoluteNode.setLocalTranslation(0, 0, 0);
            this.pausableNode.setLocalTranslation(0, 0, 0);
            
            this.absoluteNode.detachChild(backgroundSlitOne);
            this.absoluteNode.detachChild(backgroundSlitTwo);
            this.pausableNode.detachChild(backgroundSlitThree);
            this.pausableNode.detachChild(backgroundSlitFour);
        }
    }

    @Override
    public final void cleanup() {
        super.cleanup();
        this.setEnabled(false);
        this.app.getGuiNode().detachChild(this.absoluteNode);
    }

    private void updateCursor() {
        this.cursorPosition = Main.getUserInput().getMousePosition();
        
        if (this.cursor != null) {
            this.cursor.setLocalTranslation(this.cursorPosition.x - (this.cursor.getLocalScale().x / 2), this.cursorPosition.y - (this.cursor.getLocalScale().y / 2), this.cursor.getLocalTranslation().z);
        }
        
        this.mouseEscapeTime = Math.max(this.mouseEscapeTime - Utilities.lockedTPF, 0);

        if (this.mouseStatus && (this.cursorPosition.x < 0 || this.cursorPosition.y < 0 || this.cursorPosition.x > Main.getRealResolution().x || this.cursorPosition.y > Main.getRealResolution().y)) {
            this.app.getInputManager().setCursorVisible(true);
            this.mouseStatus = false;
            Mouse.setCursorPosition((int) this.cursorPosition.x, (int) this.cursorPosition.y);
            this.mouseEscapeTime = .25f;
        } else if (this.mouseEscapeTime == 0 && (this.cursorPosition.x > 0 && this.cursorPosition.y > 0 && this.cursorPosition.x < Main.getRealResolution().x && this.cursorPosition.y < Main.getRealResolution().y)) {
            this.app.getInputManager().setCursorVisible(false);
            this.mouseStatus = true;
        }
    }

    protected final void updateBackButton(Class<? extends SimpleAppState> lastState, float xPosition, float yPosition) {
        this.updateBackButton(Utilities.getGeneralUtility().getConstructor(lastState), new Object[0], xPosition, yPosition);
    }
    
    protected final void updateBackButton(Constructor constructor, Object[] parameters, float xPosition, float yPosition) {
        if (this.absoluteNode.getChild("Back Button") == null) {
            this.absoluteNode.attachChild(Utilities.getJMELoader().getPicture("Back Button", "Interface/General/backButton.png", xPosition, yPosition, PictureLayer.GUI));
        } else if (Main.getUserInput().isLeftButtonClicked() && Utilities.getPhysicsUtility().complex2DCollision(Main.getUserInput().getMousePosition(), (Picture) this.absoluteNode.getChild("Back Button"))) {
            backSound.setVolume(Main.getGameSettings().getSFXVolume());
            backSound.playInstance();
            this.switchStates((SimpleAppState) Utilities.getGeneralUtility().createObject(constructor, parameters));
        }   
    }
    
    protected final void playSelectSound() {
        selectSound.setVolume(Main.getGameSettings().getSFXVolume());
        selectSound.playInstance();
    }

    protected final void switchStates(SimpleAppState state) {
    	if (this instanceof GameState && ((GameState)this).getWorld() != null && ((GameState)this).getWorld().getStage() != null){
    		((GameState)this).getWorld().getStage().cleanUpMatch();
    	}
        this.stateManager.attach(state);
        this.setEnabled(false);
        this.stateManager.detach(this);
    }
}
