package states;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;
import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import java.util.LinkedList;
import java.util.List;
import states.game.GameRules;
import states.game.events.EventSelectState;
import states.game.strife.StrifeCharacterSelectState;
import states.game.targetTest.TargetTestCharacterSelectState;

/**
 *
 *
 * @author Matthew Ludwig
 */
public class SoloMenuState extends SimpleAppState {

    private List<Picture> buttonList = new LinkedList<Picture>();

    public SoloMenuState() {
        super("Solo Menu", true);
    }

    @Override
    public void attachToAbsoluteNode(Node node, AssetManager am) {
        node.attachChild(Utilities.getJMELoader().getPicture("Background", "Interface/SoloMode/Background.png", 0, 0, PictureLayer.BACKGROUND));
        this.buttonList.add(Utilities.getJMELoader().getPicture("Target Test Button", "Interface/SoloMode/targetTest.png", 70, 325, PictureLayer.GUI));
        this.buttonList.add(Utilities.getJMELoader().getPicture("Classic Button", "Interface/SoloMode/classicMode.png", 425, 325, PictureLayer.GUI));
        this.buttonList.add(Utilities.getJMELoader().getPicture("Event Button", "Interface/SoloMode/eventMatch.png", 70, 100, PictureLayer.GUI));
        this.buttonList.add(Utilities.getJMELoader().getPicture("Adventure Button", "Interface/SoloMode/adventureMode.png", 425, 100, PictureLayer.GUI));

        for (Picture pic : this.buttonList) {
            node.attachChild(pic);
        }
    }

    @Override
    public void attachToPausableNode(Node node, AssetManager am) {
    }

    @Override
    public String getMusicPath() {
        return "Music/MainMenuMusic.ogg";
    }

    @Override
    public void update(float tpf) {
        super.update(tpf);
        
        this.updateBackButton(MainMenuState.class, 5, -15);

        if (Main.getUserInput().isLeftButtonClicked()) {
            for (Picture button : this.buttonList) {
                if (Utilities.getPhysicsUtility().complex2DCollision(this.cursorPosition, button)) {
                    this.buttonClicked(button.getName());
                }
            }
        }
    }

    public void buttonClicked(String name) {
        this.playSelectSound();
        
        if (name.equals("Target Test Button")) {
            this.switchStates(new TargetTestCharacterSelectState());
        } else if (name.equals("Classic Button")) {
            this.switchStates(new StrifeCharacterSelectState());
        } else if (name.equals("Event Button")) {
            this.switchStates(new EventSelectState());
        } else if (name.equals("Adventure Button")) {
            this.switchStates(new OptionsMenuState());
        } else {
            System.err.println("Incorrect buton name passed into function!  Name:  " + name);
        }
    }
}
