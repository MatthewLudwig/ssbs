/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package engine;

import com.jme3.math.Vector2f;

/**
 * Linear function in y = mx + b form.
 * 
 * @author Matthew
 */
public class LinearFunction {
    private float m;
    private float b;
    
    public LinearFunction(float m, float b) {
        this.m = m;
        this.b = b;
    }
    
    public LinearFunction(Vector2f initialPoint, Vector2f finalPoint) {
        Vector2f slope = finalPoint.subtract(initialPoint);
        this.m = slope.y / slope.x;
        this.b = initialPoint.y;
    }
    
    public float getY(float x) {
        return (this.m * x) + this.b;
    }
    
    @Override
    public String toString() {
        return "y = " + this.m + "x + " + this.b;
    }
}
