package engine;

/**
 *
 * @author Matthew
 */
public class Pair<X, Y> {
    public final X left;
    public final Y right;
    
    public Pair(X l, Y r) {
        this.left = l;
        this.right = r;
    }
    
    @Override
    public final boolean equals(Object o) {
        if (o instanceof Pair) {
            Pair pairToCompair = (Pair) o;
            return this.left.equals(pairToCompair.left) && this.right.equals(pairToCompair.right); 
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.left != null ? this.left.hashCode() : 0);
        hash = 59 * hash + (this.right != null ? this.right.hashCode() : 0);
        return hash;
    }
}
