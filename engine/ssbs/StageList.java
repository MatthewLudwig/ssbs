package engine.ssbs;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import states.game.stages.*;
import engine.TBS;
import engine.TBSBased;
import engine.utility.Utilities;

/**
 *
 * @author Matthew
 */
public class StageList implements TBSBased {
	private static boolean isOmicron;
	
    private final Map<Integer, StageMapping> stageMap = new LinkedHashMap<Integer, StageMapping>();
    private final Map<Integer, StageMapping> randomStageMap = new LinkedHashMap<Integer, StageMapping>();

    public TBS save() {
        TBS saveData = new TBS();
        
        for (StageMapping mapping : this.stageMap.values()) {
            saveData.writeBoolean(mapping.getName() + ":Unlocked", mapping.isUnlocked());
        }
        
        return saveData;
    }

    public void load(TBS saveData) {
        for (StageMapping mapping : this.stageMap.values()) {
            mapping.setUnlocked(saveData.getBoolean(mapping.getName() + ":Unlocked"));
        }
    }
    
    public List<Map.Entry<Integer, StageMapping>> getUnlockedStages() {
        List<Map.Entry<Integer, StageMapping>> list = new LinkedList<Map.Entry<Integer, StageMapping>>();
        
        for (Map.Entry<Integer, StageMapping> mapping : this.stageMap.entrySet()) {
            if (mapping.getValue().isUnlocked()) {
                list.add(mapping);
            }
        }
        
        return list;
    }
    
    public void toggleOmicron(){
    	isOmicron = !isOmicron;
    }

    public StageMapping getStage(int index) {
        return this.stageMap.get(index);
    }

    public StageMapping getRandomStage() {
        Random r = new Random();
        int randomIndex = r.nextInt(this.randomStageMap.size());
        return (StageMapping) this.randomStageMap.values().toArray()[randomIndex];
    }

    public StageList() {
//        this.stageMap.put(1, new StageMapping());
        this.stageMap.put(2, new StageMapping(FinalDestination.class, "Final Destination", true, FinalDestinationO.class));
        this.stageMap.put(3, new StageMapping(AcornPlains.class, "Acorn Plains", true, AcornPlainsO.class));
        this.stageMap.put(4, new StageMapping(RainbowRoad.class, "Rainbow Road", true, RainbowRoadO.class));
//        this.stageMap.put(5, new StageMapping());
        this.stageMap.put(6, new StageMapping(JungleJapes.class, "Jungle Japes", true, JungleJapesO.class));
        this.stageMap.put(7, new StageMapping(SaffronCity.class, "Saffron City", true, SaffronCityO.class));
        this.stageMap.put(8, new StageMapping(Temple.class, "Temple", true, TempleO.class));
        this.stageMap.put(9, new StageMapping(new Class[]{ClockTown.class, ClockTown2.class, ClockTown3.class}, "Clock Town", true, new Class[]{ClockTownO.class, ClockTownO2.class, ClockTownO3.class}));
//        this.stageMap.put(10, new StageMapping());
        this.stageMap.put(11, new StageMapping(Corneria.class, "Corneria", true, CorneriaO.class));
        this.stageMap.put(12, new StageMapping(FrigateOrpheon.class, "Frigate Orpheon", true, FrigateOrpheonO.class));
        this.stageMap.put(13, new StageMapping(Pyrosphere.class, "Pyrosphere", true, PyrosphereO.class));
        this.stageMap.put(14, new StageMapping(TowerOfValni.class, "Tower Of Valni", true));
        this.stageMap.put(15, new StageMapping(CastleDedede.class, "Castle Dedede", true, CastleDededeO.class));
//        this.stageMap.put(16, new StageMapping());
        this.stageMap.put(17, new StageMapping(MuteCity.class, "Mute City", true)); //remember to change event 9 to omega
        this.stageMap.put(18, new StageMapping(EmeraldHillZone.class, "Emerald Hill Zone", true));
        this.stageMap.put(19, new StageMapping(CityEscape.class, "City Escape", true, CityEscapeO.class));
        this.stageMap.put(20, new StageMapping(WilysCastle.class, "Wilys Castle", true, WilysCastleO.class));
//        this.stageMap.put(21, new StageMapping());
        this.stageMap.put(22, new StageMapping(MiiMaker.class, "Mii Maker", true, MiiMakerO.class));
//        this.stageMap.put(23, new StageMapping());
//        this.stageMap.put(24, new StageMapping());
//        this.stageMap.put(25, new StageMapping());
//        this.stageMap.put(26, new StageMapping());
//        this.stageMap.put(27, new StageMapping());
//        this.stageMap.put(28, new StageMapping());
//        this.stageMap.put(29, new StageMapping());
//        this.stageMap.put(30, new StageMapping());
        this.stageMap.put(31, new StageMapping(London.class, "London", true, LondonO.class));
//        this.stageMap.put(32, new StageMapping());
//        this.stageMap.put(33, new StageMapping());
//        this.stageMap.put(34, new StageMapping());
        this.stageMap.put(35, new StageMapping(Threed.class, "Threed", true, ThreedO.class));
//        this.stageMap.put(36, new StageMapping());
//        this.stageMap.put(37, new StageMapping());
//        this.stageMap.put(38, new StageMapping());
//        this.stageMap.put(39, new StageMapping());
//        this.stageMap.put(40, new StageMapping());
        this.stageMap.put(256, new StageMapping(true));
        this.stageMap.put(257, new StageMapping(false));

        this.randomStageMap.putAll(this.stageMap);
        this.randomStageMap.remove(256);
        this.randomStageMap.remove(257);
    }

    public class StageMapping {
    	private Class omicronStageClass;
        private Class stageClass;
        private Class[] stageClasses;
        private Class[] omicronClasses;
        private String stageName;
        private boolean isUnlocked;

        public StageMapping(boolean b) {
            this.stageClass = null;
            if (b)
            	this.stageName = "Random";
            else
            	this.stageName = "Omicron";
            this.isUnlocked = true;
        }
        
        public StageMapping(Class<? extends Stage>[] classes, String name, boolean defaultState, Class<? extends Stage>[] omicrons) {
        	this(classes[0], name, defaultState, omicrons[0]);
        	this.stageClasses = classes;
        	this.omicronClasses = omicrons;
        }
        
        public StageMapping(Class<? extends Stage>[] classes, String name, boolean defaultState) {
        	this(classes[0], name, defaultState);
        	this.stageClasses = classes;
        }
        
        public StageMapping(Class<? extends Stage> character, String name, boolean defaultState, Class<? extends Stage> omicron) {
        	this(character, name, defaultState);
        	this.omicronStageClass = omicron;
        }

        public StageMapping(Class<? extends Stage> character, String name, boolean defaultState) {
            this.stageClass = character;
            this.stageName = name;
            this.isUnlocked = defaultState;
        }
        
        public String getName() {
            return this.stageName;
        }

        public Stage getObject() {
        	if (stageClasses != null){
        		stageClass = stageClasses[(int)(Math.random()*stageClasses.length)];
        	}
        	if (omicronClasses != null){
        		omicronStageClass = omicronClasses[(int)(Math.random()*omicronClasses.length)];
        	}
        	
        	if (this.stageClass == null){
        		return getRandomStage().getObject();
        	} else {
        		if (isOmicron){
        			return (Stage) Utilities.getGeneralUtility().createObject(Utilities.getGeneralUtility().getConstructor(omicronStageClass));
        		} else {
        			return (Stage) Utilities.getGeneralUtility().createObject(Utilities.getGeneralUtility().getConstructor(stageClass));
        		}
        	}
        }

        public String getIcon() {
        	if (this.stageName.equals("Random")){
        		return "Interface/MatchSetup/randomIcon.png";
        	} else if (this.stageName.equals("Omicron")){
        		if (isOmicron)
        			return "Interface/MatchSetup/omicronIcon.png";
        		else
        			return "Interface/MatchSetup/omicronIconGrey.png";
        	} else{
        		return "Stages/" + this.stageName + "/Icon.png";
        	}
        }

        public String getPreview() {
        	if (this.stageName.equals("Random")){
        		return "Interface/MatchSetup/randomPreview.png";
        	} else if (this.stageName.equals("Omicron")){
        		return "Interface/MatchSetup/omicronPreview.png";
        	} else{
        		return "Stages/" + this.stageName + "/Preview.png";
        	}
        }

        public void setUnlocked(boolean newState) {
            this.isUnlocked = newState;
        }
        
        public boolean isUnlocked() {
            return this.isUnlocked;
        }
    }
}
