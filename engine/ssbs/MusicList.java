package engine.ssbs;

import com.jme3.audio.AudioNode;
import engine.TBS;
import engine.TBSBased;
import engine.utility.Utilities;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Matthew
 */
public class MusicList implements TBSBased{
    private static final Random generator = new Random();
    
    private final List<MusicMapping> musicMap = new LinkedList<MusicMapping>();
    private final List<MusicMapping> lockedMusicMap = new LinkedList<MusicMapping>();
    
    public TBS save() {
        TBS saveData = new TBS();
        
        for (MusicMapping mapping : this.musicMap) {
            saveData.writeBoolean(mapping.getName() + ":Unlocked", mapping.isUnlocked());
        }
        
        return saveData;
    }

    public void load(TBS saveData) {
        for (MusicMapping mapping : this.musicMap) {
            boolean isUnlocked = saveData.getBoolean(mapping.getName() + ":Unlocked");
            mapping.setUnlocked(isUnlocked);
            
            if (!isUnlocked) {
                this.lockedMusicMap.add(mapping);
            }
        }
    }
    
    public MusicMapping getRandomMusic() {               
        return this.musicMap.get(generator.nextInt(this.musicMap.size()));
    }
    
    public MusicMapping getRandomLockedMusic() {
        List<MusicMapping> newList = new LinkedList<MusicMapping> ();
        
        for (MusicMapping mapping : this.lockedMusicMap) {
            if (!mapping.isUnlocked) {
                newList.add(mapping);
            }
        }
        
        this.lockedMusicMap.clear();
        this.lockedMusicMap.addAll(newList);
        
        if (this.lockedMusicMap.isEmpty()) {
            return null;
        } else {
            return this.lockedMusicMap.get(generator.nextInt(this.lockedMusicMap.size()));
        }
    }
    
    public MusicList() {
        musicMap.add(new MusicMapping("Stages/Acorn Plains/Music_3", "Super Mario Land", false));
        musicMap.add(new MusicMapping("Stages/Frigate Orpheon/Music_3", "Escape", false));
        musicMap.add(new MusicMapping("Stages/Temple/Music_4", "Temple 64", false));
        musicMap.add(new MusicMapping("Stages/Castle Dedede/Music_4", "Halberd", false));
        musicMap.add(new MusicMapping("Stages/Castle Dedede/Music_5", "Gourmet Race", false));
        musicMap.add(new MusicMapping("Stages/Final Destination/Music_3", "Final Destination Brawl", false));
        musicMap.add(new MusicMapping("Stages/Rainbow Road/Music_4", "Mario Power Tennis", false));
        musicMap.add(new MusicMapping("Stages/Rainbow Road/Music_5", "Bowser Kart", false));
        musicMap.add(new MusicMapping("Stages/Threed/Music_3", "Having Friends", false));
        musicMap.add(new MusicMapping("Stages/Threed/Music_5", "Osohe Castle", false));
        musicMap.add(new MusicMapping("Stages/Threed/Music_7", "Saturn Valley", false));
        musicMap.add(new MusicMapping("Stages/Corneria/Music_3", "Venom", false));
        musicMap.add(new MusicMapping("Stages/Corneria/Music_6", "Lylat Cruise", false));
        musicMap.add(new MusicMapping("Stages/Jungle Japes/Music_3", "Stickerbrush Symphony", false));
        musicMap.add(new MusicMapping("Stages/Saffron City/Music_3", "Pokemon Stadium", false));
        musicMap.add(new MusicMapping("Stages/Saffron City/Music_4", "Gym Leader", false));
        musicMap.add(new MusicMapping("Stages/Saffron City/Music_5", "Pokemon Stadium 2", false));
        musicMap.add(new MusicMapping("Stages/Tower of Valni/Music_3", "Advance", false));
        musicMap.add(new MusicMapping("Stages/Tower of Valni/Music_5", "Origin Peak", false));
        musicMap.add(new MusicMapping("Stages/Tower of Valni/Music_7", "Lagdou Ruins", false));
        musicMap.add(new MusicMapping("Stages/Mute City/Music_3", "Devil's Forest", false));
        musicMap.add(new MusicMapping("Stages/Emerald Hill Zone/Music_3", "Casino Night Zone", false));
        musicMap.add(new MusicMapping("Stages/Clock Town/Music_3", "Stone Tower", false));
        musicMap.add(new MusicMapping("Stages/Clock Town/Music_5", "Dark World", false));
        musicMap.add(new MusicMapping("Stages/Wilys Castle/Music_3", "Maverick Fight", false));
        musicMap.add(new MusicMapping("Stages/Mii Maker/Music_3", "Wii Sports", false));
        musicMap.add(new MusicMapping("Stages/Pyrosphere/Music_3", "Dark Samus", false));
        musicMap.add(new MusicMapping("Stages/City Escape/Music_3", "Metal Harbor", false));
        musicMap.add(new MusicMapping("Stages/London/Music_3", "The Great Don Paolo", false));
        musicMap.add(new MusicMapping("Stages/Face Raiders/Music_3", "Wrecking Crew", false));
        musicMap.add(new MusicMapping("Stages/Waluigi Stadium/Music_4", "Mario Bros.", false));
        musicMap.add(new MusicMapping("Stages/Waluigi Stadium/Music_6", "Bowser's Castle", false));
//Edwin's Farm 3 (Capital City)
//Pac-Maze 4 (Pac//Mania)
//Ramen Ichiraku 3 (Go Go Go Naruto)
//Courthouse 4 (Examination)
//Dracula's Castle 4 (Poison Mind)
//Cave of Wonders 4 (This is Halloween)
//NPC Village 4 (Minecraft)
//Wall Maria 3 (Armored Titan)
//Palutena's Temple 4 (Underworld)
//Palutena's Temple 5 (Dark Pit's Theme)
//Flatgrass 3 (Never Gonna Give You Up)
//Dustbowl 3 (Meet the Spy)
//Hyperbolic Time Chamber 3 (Battle Z)
//TARDIS 7 (Ten)
//Mama's Kitchen 4 (What Shall We Cook Today?)
//Castle Lololo 3 (Rainbow Route)
//Castle Lololo 5 (Marx)
//Cube Pyramid 4 (Cube Pyramid (Rock Mix))
//Forbidden Forest 3 (Double Trouble)
//Koridai Castle 3 (The Faces of Evil)
//Battlefield 4 (Metal Mario)
    }

    public static class MusicMapping {
        private String musicPath;
        private String musicName;
        private boolean isUnlocked;

        public MusicMapping(String musicPath, String name, boolean initiallyUnlocked) {
            this.musicPath = musicPath;
            this.musicName = name;
            this.isUnlocked = initiallyUnlocked;
        }
        
        public String getName() {
            return this.musicName;
        }
        
        public AudioNode getObject() {
            return Utilities.getCustomLoader().getAudioNode(this.musicPath);
        }
        
        public void setUnlocked(boolean state) {
            this.isUnlocked = state;
        }
        
        public boolean isUnlocked() {
            return this.isUnlocked;
        }
    }
}
