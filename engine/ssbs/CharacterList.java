package engine.ssbs;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import engine.TBS;
import engine.TBSBased;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import entities.characters.*;

/**
 *
 * @author Matthew
 */
public class CharacterList implements TBSBased {

    private final Map<Integer, CharacterMapping> characterMap = new LinkedHashMap<Integer, CharacterMapping>();
    private final Map<Integer, CharacterMapping> randomCharacterMap = new LinkedHashMap<Integer, CharacterMapping>();

    public TBS save() {
        TBS saveData = new TBS();
        
        for (CharacterMapping mapping : this.characterMap.values()) {
            saveData.writeBoolean(mapping.getName() + ":Unlocked", mapping.isUnlocked());
            saveData.writeInt(mapping.getName() + ":Time", mapping.getTargetTestTime());
        }
        
        return saveData;
    }

    public void load(TBS saveData) {
        for (CharacterMapping mapping : this.characterMap.values()) {
            boolean isUnlocked = saveData.getBoolean(mapping.getName() + ":Unlocked");
            int targetTestTime = saveData.getInt(mapping.getName() + ":Time");

            if (targetTestTime != 0) {
                mapping.setUnlocked(isUnlocked);
                mapping.setTargetTestTime(targetTestTime);
            }
        }
    }
    
    public List<Entry<Integer, CharacterMapping>> getUnlockedCharacters() {
        List<Entry<Integer, CharacterMapping>> list = new LinkedList<Entry<Integer, CharacterMapping>>();
        
        for (Entry<Integer, CharacterMapping> mapping : this.characterMap.entrySet()) {
            if (mapping.getValue().isUnlocked()) {
                list.add(mapping);
            }
        }
        
        return list;
    }
    
    public CharacterMapping getCharacter(int index) {
        return this.characterMap.get(index);
    }
    
    public CharacterMapping getCharacter(PlayableCharacter lookup) {
        for (Entry<Integer, CharacterMapping> e : this.characterMap.entrySet()) {
            if (e.getValue().getName().equals(lookup.getDisplayName())) {
                return e.getValue();
            }
        }
        
        return null;
    }

    public CharacterMapping getRandomCharacter() {
        Random r = new Random();
        int randomIndex = r.nextInt(this.randomCharacterMap.size());
        return (CharacterMapping) this.randomCharacterMap.values().toArray()[randomIndex];
    }

    public CharacterList() {
        this.characterMap.put(1, new CharacterMapping(Mario.class, "Mario", Franchise.MARIO, 0, 5, 2, true, 0, -4));
        this.characterMap.put(2, new CharacterMapping(Luigi.class, "Luigi", Franchise.MARIO, 4, 0, 2, true, 0, -4));
        this.characterMap.put(3, new CharacterMapping(Toad.class, "Toad", Franchise.MARIO, 1, 5, 4, true, 0, -4));
        this.characterMap.put(4, new CharacterMapping(Waluigi.class, "Waluigi", Franchise.MARIO, 3, 2, 0, true, -2, -4));
        this.characterMap.put(5, new CharacterMapping(Kamek.class, "Kamek", Franchise.MARIO, 2, 1, 0, true, -1, -3));
        this.characterMap.put(13, new CharacterMapping(Geno.class, "Geno", Franchise.MARIO, 5, 4, 3, true, -3, -4));
        this.characterMap.put(14, new CharacterMapping(KingKRool.class, "King K Rool", Franchise.DK, 3, 0, 4, true, -1, -4));
        this.characterMap.put(15, new CharacterMapping(Link.class, "Link", Franchise.ZELDA, 1, 0, 2, true, 0, -4));
        this.characterMap.put(16, new CharacterMapping(Ghirahim.class, "Ghirahim", Franchise.ZELDA, 2, 4, 5, true, 4, -8));
        this.characterMap.put(18, new CharacterMapping(Samus.class, "Samus", Franchise.METROID, 3, 2, 1, true, 0, -4));
        this.characterMap.put(19, new CharacterMapping(Ridley.class, "Ridley", Franchise.METROID, 5, 2, 1, true, -3, -9));
        this.characterMap.put(21, new CharacterMapping(Marth.class, "Marth", Franchise.FIREEMBLEM, 4, 5, 0, true, -2, -9));
        this.characterMap.put(22, new CharacterMapping(Kirby.class, "Kirby", Franchise.KIRBY, 4, 3, 2, true, 0, -4));
        this.characterMap.put(23, new CharacterMapping(KingDedede.class, "King Dedede", Franchise.KIRBY, 4, 1, 2, true, -2, -9));
        this.characterMap.put(25, new CharacterMapping(Krystal.class, "Krystal", Franchise.STARFOX, 4, 3, 0, true, 0, -4));
        this.characterMap.put(26, new CharacterMapping(Mewtwo.class, "Mewtwo", Franchise.POKEMON, 5, 1, 2, true, -2, -4));
        this.characterMap.put(27, new CharacterMapping(Ness.class, "Ness", Franchise.MOTHER, 3, 5, 2, true, -2, -4));
        this.characterMap.put(28, new CharacterMapping(CaptainFalcon.class, "Captain Falcon", Franchise.FZERO, 2, 1, 0, true, 0, -4));
        this.characterMap.put(29, new CharacterMapping(Sonic.class, "Sonic", Franchise.SONIC, 2, 5, 0, true, 0, -4));
        this.characterMap.put(34, new CharacterMapping(MegaMan.class, "Mega Man", Franchise.MEGAMAN, 1, 4, 0, true, -1, -4));
        this.characterMap.put(38, new CharacterMapping(ProfessorLayton.class, "Professor Layton", Franchise.LAYTON, 3, 5, 1, true, 6, -9));
        this.characterMap.put(49, new CharacterMapping(Reggie.class, "Reggie", Franchise.NINTENDO, 4, 5, 2, true, -2, -4, "reggie"));
        this.characterMap.put(50, new CharacterMapping());

        this.randomCharacterMap.putAll(this.characterMap);
        this.randomCharacterMap.remove(50);
    }

    public enum Franchise {

        DK("dk"),
        FZERO("fZero"),
        STARFOX("starfox"),
        MARIO("mario"),
        METROID("metroid"),
        SONIC("sonic"),
        ZELDA("zelda"),
        KIRBY("kirby"),
        MEGAMAN("megaMan"),
        MOTHER("mother"),
        POKEMON("pokemon"),
        FIREEMBLEM("fireEmblem"),
        NINTENDO("nintendo"),
        LAYTON("layton");

        private String path;

        private Franchise(String path) {
            this.path = path;
        }

        public String getName() {
            return path;
        }
    }

    public class CharacterMapping {
        private Class<? extends PlayableCharacter> characterClass;
        private String characterName;
        private Franchise franchise;
        private String otherFranchise;
        
        private int redColor;
        private int greenColor;
        private int blueColor;
        
        private boolean isUnlocked;
        private int targetTestTime;
        
        private int dispX;
        private int dispY;

        public CharacterMapping() {
            this.characterClass = null;
            this.characterName = "Random";
            
            this.redColor = 0;
            this.greenColor = 0;
            this.blueColor = 0;
            
            this.isUnlocked = true;
            this.targetTestTime = 99999;
            
            this.dispX = 0;
            this.dispY = 0;
        }
        
        public CharacterMapping(Class<? extends PlayableCharacter> character, String name, Franchise franchise, int red, int green, int blue, boolean defaultState, int dispX, int dispY, String otherFranchise) {
        	this(character, name, franchise, red, green, blue, defaultState, dispX, dispY);
        	this.otherFranchise = otherFranchise;
        
        }

        public CharacterMapping(Class<? extends PlayableCharacter> character, String name, Franchise franchise, int red, int green, int blue, boolean defaultState, int dispX, int dispY) {
            this.characterClass = character;
            this.characterName = name;
            this.franchise = franchise;
            
            this.redColor = red;
            this.greenColor = green;
            this.blueColor = blue;
            
            this.isUnlocked = defaultState;
            this.targetTestTime = 99999;
            
            this.dispX = dispX;
            this.dispY = dispY;
            
            this.otherFranchise = "";
        }
        
        public CharacterMapping(Class<? extends PlayableCharacter> character, String name, Franchise franchise, int red, int green, int blue, boolean defaultState) {
        	this(character, name, franchise, red, green, blue, defaultState, 0, 0);
        }
        
        public int getDispX(){
        	return this.dispX;
        }
        
        public int getDispY(){
        	return this.dispY;
        }

        public String getName() {
            return this.characterName;
        }
        
        public String getFranchiseName() {
            return this.franchise.getName();
        }
        
        public String getAppropriateFranchise(){
	        if (this.otherFranchise.equals("")){
	        	return getFranchiseName();
	        } else {
	        	return this.otherFranchise;
	        }
        }
        
        public PlayableCharacter getObject() {
            if (this.characterClass == null) {
                PlayableCharacter randomCharacter = getRandomCharacter().getObject();
                randomCharacter.setFromRandom();
                return randomCharacter;
            } else {
                return (PlayableCharacter) Utilities.getGeneralUtility().createObject(Utilities.getGeneralUtility().getConstructor(characterClass));
            }
        }

        public String getPortraitPath() {
            return !this.characterName.equals("Random") ? "Sprites/" + this.characterName + "/portrait.png" : "Interface/MatchSetup/randomPortrait.png";
        }

        public String getSelectedPath(int colorNumber) {
            String selectPose = "/selectPose";
            
            if (colorNumber == 0) {
                selectPose += "_DEFAULT";
            } else {
                selectPose += "_COLOR" + colorNumber;
            }
            
            selectPose += ".png";
            
            return !this.characterName.equals("Random") ? "Sprites/" + this.characterName + selectPose : "Interface/MatchSetup/randomSelectPose.png";
        }
        
        public String getVictoryPose(int colorNumber) {
            String victoryPose = "/victoryPose";
            
            if (colorNumber == 0) {
                victoryPose += "_DEFAULT";
            } else {
                victoryPose += "_COLOR" + colorNumber;
            }
            
            victoryPose += ".png";
            
            return !this.characterName.equals("Random") ? "Sprites/" + this.characterName + victoryPose : "Interface/MatchSetup/randomSelectPose.png";
        }

        public String getFranchisePath() {
            return !this.characterName.equals("Random") ? "Interface/Franchise Icons/" + this.franchise.getName() + "_#.png" : "Sprites/Miscellaneous/blankSprite.png";
        }
        
        public int getRedColor() {
            return this.redColor;
        }
        
        public int getGreenColor() {
            return this.greenColor;
        }
        
        public int getBlueColor() {
            return this.blueColor;
        }
        
        public void setUnlocked(boolean state) {
            this.isUnlocked = state;
        }

        public boolean isUnlocked() {
            return this.isUnlocked;
        }
        
        public void setTargetTestTime(int newTime) {
        	if (newTime < getTargetTestTime())
        		this.targetTestTime = newTime;
        }
        
        public int getTargetTestTime() {
            return this.targetTestTime;
        }
    }
}
