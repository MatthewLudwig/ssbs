package engine.ssbs;

/**
 *
 *
 * @author Matthew Ludwig
 */
public class GameSettings {
    private float sfxVolume;
    private float musicVolume;
    private float voicesVolume;
    private boolean debugMode;
    private boolean fullscreen;
    
    public GameSettings() {
        this.sfxVolume = 1;
        this.musicVolume = 1;
        this.voicesVolume = 1;
        this.debugMode = false;
        this.fullscreen = false;
    }

    public void setSFXVolume(float newVolume) {
        this.sfxVolume = newVolume;
    }

    public float getSFXVolume() {
        return this.sfxVolume;
    }

    public void setMusicVolume(float newVolume) {
        this.musicVolume = newVolume;
    }

    public float getMusicVolume() {
        return this.musicVolume;
    }

    public void setVoicesVolume(float newVolume) {
        this.voicesVolume = newVolume;
    }

    public float getVoicesVolume() {
        return this.voicesVolume;
    }

    public void setDebugMode(boolean mode) {
        debugMode = mode;
    }

    public boolean getDebugMode() {
        return debugMode;
    }
    
    public void setFullscreen(boolean newState) {
        this.fullscreen = newState;
    }
    
    public boolean isFullscreen() {
        return this.fullscreen;
    }
}
