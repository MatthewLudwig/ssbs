package engine.ssbs;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import states.game.GameState;
import states.game.events.*;
import engine.TBS;
import engine.TBSBased;
import engine.utility.Utilities;

/**
 *
 * @author Matthew
 */
public class EventList implements TBSBased {
	private static boolean isOmicron;
	
    private final Map<Integer, EventMapping> eventMap = new LinkedHashMap<Integer, EventMapping>();

    public TBS save() {
        TBS saveData = new TBS();
        
        for (EventMapping mapping : this.eventMap.values()) {
            saveData.writeBoolean(mapping.getName() + ":Unlocked", mapping.isUnlocked());
            saveData.writeInt(mapping.getName() + ":Score", mapping.getScore());
        }
        
        return saveData;
    }

    public void load(TBS saveData) {
        for (EventMapping mapping : this.eventMap.values()) {
            mapping.setUnlocked(saveData.getBoolean(mapping.getName() + ":Unlocked"));
            mapping.setScore(saveData.getInt(mapping.getName() + ":Score"));
        }
    }
    
    public List<Map.Entry<Integer, EventMapping>> getUnlockedEvents() {
        List<Map.Entry<Integer, EventMapping>> list = new LinkedList<Map.Entry<Integer, EventMapping>>();
        
        for (Map.Entry<Integer, EventMapping> mapping : this.eventMap.entrySet()) {
            if (mapping.getValue().isUnlocked()) {
                list.add(mapping);
            }
        }
        
        return list;
    }
    
    public void toggleOmicron(){
    	isOmicron = !isOmicron;
    }

    public EventMapping getEvent(int index) {
        return this.eventMap.get(index);
    }
    
    public EventMapping getEvent(String name){
    	for (Map.Entry<Integer, EventMapping> mapping : this.eventMap.entrySet()){
    		if (mapping.getValue().getName().equals(name))
    			return mapping.getValue();
    	}
    	return null;
    }

    public EventList() {
    	this.eventMap.put(1, new EventMapping(Event1.class, "Two Trouble Meanies", true, false));
    	this.eventMap.put(2, new EventMapping(Event2.class, "Metroid Woman", true, false));
    	this.eventMap.put(3, new EventMapping(Event3.class, "Puffballsaur Planet", true, false));
    	this.eventMap.put(4, new EventMapping(Event4.class, "Mr EAD", true, false));
    	this.eventMap.put(5, new EventMapping(Event5.class, "Zelda Timelines", true, false));
    	this.eventMap.put(6, new EventMapping(Event6.class, "Your Place On The Throne", true, false));
    	this.eventMap.put(7, new EventMapping(Event7.class, "Blue Like You", true, false));
    	this.eventMap.put(8, new EventMapping(Event8.class, "Call For Mother", true, false));
    	this.eventMap.put(9, new EventMapping(Event9.class, "The Most Dangerous Game", true, false));
    	this.eventMap.put(10, new EventMapping(Event10.class, "Taking Names", true, true));
    }

    public class EventMapping {
        private Class gameStateClass;
        private String eventName;
        private boolean isUnlocked;
        private boolean canChoose;
        private int score;
        
        public EventMapping(Class<? extends GameState> gameState, String name, boolean defaultState, boolean canChoose) {
            this.eventName = name;
            this.isUnlocked = defaultState;
        	this.gameStateClass = gameState;
        	this.score = 0;
        	this.canChoose = canChoose;
        }
        
        public boolean canChoose(){
        	return canChoose;
        }
        
        public String getName() {
            return this.eventName;
        }
        
        public Class getEventClass(){
        	return gameStateClass;
        }

        public GameState getObject() {
        	
        	return ((EventGameState) Utilities.getGeneralUtility().createObject(Utilities.getGeneralUtility().getConstructor(gameStateClass))).setEventName(eventName);
        }

        public String getIcon() {
        	return "Events/" + this.eventName + "/Icon.png";
        }
        
        public String getDescription(){
        	return "assets/Events/" + this.eventName + "/Description.txt";
        }
        
        public String getPlayerName(){
        	return "assets/Events/" + this.eventName + "/PlayerName.txt";
        }

        public void setUnlocked(boolean newState) {
            this.isUnlocked = newState;
        }
        
        public boolean isUnlocked() {
            return this.isUnlocked;
        }
        
        public void setScore(int newScore) {
        	if (newScore > getScore())
        		this.score = newScore;
        }
        
        public int getScore() {
            return this.score;
        }
    }
}
