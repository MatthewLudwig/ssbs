package engine.ssbs;

import engine.Main;
import engine.utility.Utilities;
import entities.pokemon.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Matthew
 */
public class PokemonList {
    private static final Random generator = new Random();
    
    private static final List<PokemonMapping> pokemonMap = new LinkedList<PokemonMapping>();
    
    public static Pokemon getRandomPokemon() {
        int chance = generator.nextInt(110);
    	//int chance = generator.nextInt(1);
        int cumulativeChance = 0;
        
        for (PokemonMapping mapping : pokemonMap) {
            cumulativeChance += mapping.getChance();
            
            if ((!mapping.getName().equals("Mew") || Main.getMusicList().getRandomLockedMusic() != null) && chance < cumulativeChance) {
                return mapping.getObject();
            }
        }
        
        return null;
    }
    
    static {
        pokemonMap.add(new PokemonMapping(Jirachi.class, "Jirachi", 1));
        pokemonMap.add(new PokemonMapping(Mew.class, "Mew", 1));
        pokemonMap.add(new PokemonMapping(Porygon2.class, "Porygon2", 16));
        pokemonMap.add(new PokemonMapping(Goldeen.class, "Goldeen", 15));
        pokemonMap.add(new PokemonMapping(Lugia.class, "Lugia", 4));
        pokemonMap.add(new PokemonMapping(Latias.class, "Latias", 16));
        pokemonMap.add(new PokemonMapping(Entei.class, "Entei", 4));
        pokemonMap.add(new PokemonMapping(Torchic.class, "Torchic", 16));
        pokemonMap.add(new PokemonMapping(Groudon.class, "Groudon", 4));
        pokemonMap.add(new PokemonMapping(Chansey.class, "Chansey", 16));
        pokemonMap.add(new PokemonMapping(Wobbuffet.class, "Wobbuffet", 16));
    }

    public static class PokemonMapping {
        private Class pokemonClass;
        private String pokemonName;
        private int chance;

        public PokemonMapping(Class<? extends Pokemon> character, String name, int chance) {
            this.pokemonClass = character;
            this.pokemonName = name;
            this.chance = chance;
        }
        
        public String getName() {
            return this.pokemonName;
        }
        
        public Pokemon getObject() {
            return (Pokemon) Utilities.getGeneralUtility().createObject(Utilities.getGeneralUtility().getConstructor(pokemonClass));
        }
        
        public int getChance() {
            return this.chance;
        }
    }
}
