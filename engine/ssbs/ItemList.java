package engine.ssbs;

import engine.utility.Utilities;
import entities.items.*;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Matthew
 */
public class ItemList {
    private static final List<ItemMapping> itemMap = new LinkedList<ItemMapping>();
    
    public static List<ItemMapping> getItems() {
        return itemMap;
    }
    
    static {
        itemMap.add(new ItemMapping(ItemSmashBall.class, "Smash Ball", 1));
        itemMap.add(new ItemMapping(ItemPokeball.class, "Pokeball", 2));
        itemMap.add(new ItemMapping(ItemThinkerStatue.class, "Thinker Statue", 8));
        itemMap.add(new ItemMapping(ItemWarpedMirror.class, "Warped Mirror", 4));
        itemMap.add(new ItemMapping(ItemSuperMushroom.class, "Super Mushroom", 8));
        itemMap.add(new ItemMapping(ItemPoisonMushroom.class, "Poison Mushroom", 8));
        itemMap.add(new ItemMapping(ItemLuigiMushroom.class, "Luigi Mushroom", 8));
        itemMap.add(new ItemMapping(ItemCD.class, "CD", 0));
    }

    public static class ItemMapping {
        private Class itemClass;
        private String itemName;
        private int chance;

        public ItemMapping(Class<? extends Item> character, String name, int chance) {
            this.itemClass = character;
            this.itemName = name;
            this.chance = chance;
        }
        
        public String getName() {
            return this.itemName;
        }
        
        public Item getObject() {
            return (Item) Utilities.getGeneralUtility().createObject(Utilities.getGeneralUtility().getConstructor(itemClass));
        }
        
        public int getChance() {
            return this.chance;
        }
    }
}
