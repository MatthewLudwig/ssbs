package engine;

/**
 *
 * @author Matthew
 */
public enum PictureLayer {

    BACKGROUND(0),
    GUI(1),
    CURSOR(2),
    OTHERENTITY(1),
    CHARACTER(2),
    SPECIALLAYER(3);
    
    private int value;

    private PictureLayer(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
