package engine;

import engine.utility.Utilities;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import physics.ReversibleImage;

/**
 *
 * @author Matthew
 */
public class SpriteSheet {    
    private Map<String, ReversibleImage[]> animations;
    private String path;
    
    public SpriteSheet(String path) {
        this.animations = new HashMap<String, ReversibleImage[]>();
        this.path = path;
        
        Document doc = Document.loadFromAssets(this.path + "/SheetInfo.txt");
        
        BufferedImage image = null;
        BufferedImage collisionMap = null;
                
        try {
            image = ImageIO.read(getClass().getResourceAsStream(this.path + "/SpriteSheet.png"));
            collisionMap = ImageIO.read(getClass().getResourceAsStream(this.path + "/SweetSpotMap.png"));
        } catch (IOException e) {
            Main.log(Level.SEVERE, "Unable to find requested sprite sheet at:  " + this.path, e);
        }
        
        this.createSpriteSheet(doc, image, collisionMap);
    }
    
    public SpriteSheet(String path, String color) {
        this.animations = new HashMap<String, ReversibleImage[]>();
        this.path = path;
        
        Document doc = Document.loadFromAssets(this.path + "/SheetInfo.txt");
        
        BufferedImage image = null;
        BufferedImage collisionMap = null;
                
        try {
            image = ImageIO.read(getClass().getResourceAsStream(this.path + "/SpriteSheet_" + color + ".png"));
            collisionMap = ImageIO.read(getClass().getResourceAsStream(this.path + "/SweetSpotMap.png"));
        } catch (IOException e) {
            Main.log(Level.SEVERE, "Unable to find requested sprite sheet at:  " + this.path, e);
        }
        
        this.createSpriteSheet(doc, image, collisionMap);
    }
    
    public final void createSpriteSheet(Document doc, BufferedImage spriteSheet, BufferedImage collisionMap) {
        int spriteWidth = -1;
        int spriteHeight = -1;
        int sheetWidth = -1;
        int sheetHeight = -1;
        
        int spriteCount = 0;
        
        for (String line : doc.getLines()) {
            String[] info = line.split("-");
            
            if (spriteWidth == -1 && spriteHeight == -1) {
                spriteWidth = Integer.parseInt(info[0]);
                spriteHeight = Integer.parseInt(info[1]);
        
                if (spriteSheet.getWidth() % spriteWidth != 0 || spriteSheet.getHeight() % spriteHeight != 0) {
                    Main.log(Level.SEVERE, "Corrupt or changed sprite sheet at:  " + this.path, null);
                } else {
                    sheetWidth = (spriteSheet.getWidth() / spriteWidth);
                    sheetHeight = (spriteSheet.getHeight() / spriteHeight);
                }
            } else {                
                ReversibleImage[] animation = new ReversibleImage[Integer.parseInt(info[1])];
                
                for (int count = 0; count < animation.length; count++) {
                    int xOffset = (int) Math.floor(spriteCount % sheetWidth) * spriteWidth;
                    int yOffset = (int) Math.floor(spriteCount / sheetWidth) * spriteHeight;
                    int revX = (int) Math.floor((spriteCount + animation.length) % sheetWidth) * spriteWidth;
                    int revY = (int) Math.floor((spriteCount + animation.length) / sheetWidth) * spriteHeight;
                    
                    BufferedImage normalImage = Utilities.getGeneralUtility().deepCopy(spriteSheet.getSubimage(xOffset, yOffset, spriteWidth, spriteHeight));
                    BufferedImage reversedImage = Utilities.getGeneralUtility().deepCopy(spriteSheet.getSubimage(revX, revY, spriteWidth, spriteHeight));
                    BufferedImage normalCollisionMap = Utilities.getGeneralUtility().deepCopy(collisionMap.getSubimage(xOffset, yOffset, spriteWidth, spriteHeight));
                    BufferedImage reversedCollisionMap = Utilities.getGeneralUtility().deepCopy(collisionMap.getSubimage(revX, revY, spriteWidth, spriteHeight));
                    animation[count] = new ReversibleImage(normalImage, normalCollisionMap, reversedImage, reversedCollisionMap);
                    spriteCount++;
                }
                
                spriteCount += animation.length;
                
                this.animations.put(info[0], animation);
            }
        }
    }
    
    public ReversibleImage[] getAnimation(String name) {
        ReversibleImage[] animation = this.animations.get(name);
        
        if (animation == null) {
            Main.log(Level.SEVERE, "Missing texture (" + name + ") for spritesheet at path:  " + this.path, null);
        }
        
        return this.animations.get(name);
    }
}
