package engine.utility;

import com.jme3.audio.AudioNode;
import engine.Main;
import engine.SpriteSheet;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Matthew
 */
public class CustomLoader {
    private final Map<String, SpriteSheet> loadedSpriteSheets;
    private final Map<String, AudioNode> loadedAudio;
    
    public CustomLoader() {
        this.loadedSpriteSheets = new HashMap<String, SpriteSheet>();
        this.loadedAudio = new HashMap<String, AudioNode>();
    }
    
    /**
     * The key is also expected to be the part of the path that comes after "/Sprites/" if it is not already loaded.
     */
    public SpriteSheet getSpriteSheet(String key) {
        if (!loadedSpriteSheets.containsKey(key)) {
            Main.startLoadingResource("/Sprites/" + key);
            
            if (key.contains("_")) {
                loadedSpriteSheets.put(key, new SpriteSheet("/Sprites/" + key.split("_")[0], key.split("_")[1]));
            } else {
                loadedSpriteSheets.put(key, new SpriteSheet("/Sprites/" + key));
            }
            
            Main.finishLoadingResource("/Sprites/" + key);
        }
        
        return loadedSpriteSheets.get(key);
    }
    
    /**
     * The key happens to also be the path for the sprite sheet if it is not already loaded.
     */
    public AudioNode getAudioNode(String key) {        
        if (!loadedAudio.containsKey(key)) {
            Main.startLoadingResource(key);
            AudioNode node = new AudioNode(Utilities.am, key, false);
            node.setPositional(false);
            loadedAudio.put(key, node);
            Main.finishLoadingResource(key);
        }

        return loadedAudio.get(key);
    }
}
