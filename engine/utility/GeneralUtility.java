package engine.utility;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;

import com.jme3.math.Vector3f;

import engine.Main;

/**
 *
 * @author Matthew
 */
public class GeneralUtility {
	
	private static final Random generator = new Random();
    
    public GeneralUtility() {
        
    }
    
    public Vector3f generateRandomVector() {
    	return new Vector3f(generator.nextFloat() * 2 - 1, generator.nextFloat() * 2 - 1, generator.nextFloat() * 2 - 1);
    }
    
    public byte[] encryptByteArray(byte[] original, int key) {
        byte[] encrypted = recursivelyEncryptByteArray(original, key);
        
        if (Arrays.equals(original, encrypted)) {
            encrypted = recursivelyEncryptByteArray(original, (key % 10));
        }

        return encrypted;
    }
    
    private byte[] recursivelyEncryptByteArray(byte[] original, int key) {
        byte[] encrypted = new byte[original.length];
        
        for (int count = 0; count < original.length; count++) {
            if (count % (key % 10) != 0) {
                encrypted[count] = (byte) ~original[count];
            } else {
                encrypted[count] = original[count];
            }
        }
        
        key /= 10;
        
        return key == 0 ? encrypted : this.recursivelyEncryptByteArray(encrypted, key);
    }
    
    public int getBooleanAsSign(boolean b) {
        return b ? 1 : -1;
    }
    
    public boolean getSignAsBoolean(float i) {
        return i >= 0;
    }

    public int getBooleanAsNumber(boolean b) {
        return b ? 1 : 0;
    }
    
    public boolean compareFloats(float firstValue, float secondValue, float tolerance) {
        return Math.abs(firstValue - secondValue) <= tolerance;
    }

    public String getNumberAsString(int num) {
        if (num < 10) {
            return "00" + num;
        } else if (num < 100) {
            return "0" + num;
        } else {
            return String.valueOf(num);
        }
    }

    public String getNumberAsShortenedString(int num) {
        if (num < 10) {
            return "0" + num;
        } else if (num >= 100) {
            return "99";
        } else {
            return String.valueOf(num);
        }
    }
    
    public BufferedImage getScaledImage(BufferedImage source, int finalWidth, int finalHeight) {
        AffineTransform at = new AffineTransform();
        at.scale((float) finalWidth / (float) source.getWidth(), (float) finalHeight / (float) source.getHeight());
        AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
        return scaleOp.filter(source, null);
    }
    
    public BufferedImage deepCopy(BufferedImage source) {
        ColorModel cm = source.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = source.copyData(source.getRaster().createCompatibleWritableRaster());
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    /**
     * This method fills a short array with the numbers ranging from the first
     * number to the last number inclusive to both numbers.
     */
    public short[] getFramesFrom(int initialNumber, int finalNumber) {
        short[] frames = new short[finalNumber - initialNumber + 1];

        for (short num = (short) initialNumber; num <= finalNumber; num++) {
            frames[num - initialNumber] = num;
        }

        return frames;
    }

    public Constructor getConstructor(Class className, Object... parameters) {
        Class[] classes = new Class[parameters.length];

        for (int count = 0; count < parameters.length; count++) {
            classes[count] = parameters[count].getClass();
        }

        return getConstructor(className, classes);
    }

    public Constructor getConstructor(Class className, Class... classes) {
        Constructor theConstructor = null;

        try {
            theConstructor = className.getConstructor(classes);
        } catch (Exception e) {
            StringBuilder classNames = new StringBuilder("");

            for (int count = 0; count < classes.length; count++) {
                classNames.append(classes[count].getName()).append(", ");
            }
            
            Main.log(Level.SEVERE, "Constructor for class: " + className.getName() + " could not be made with parameters: " + classNames, e.getCause());
        }

        return theConstructor;
    }

    public Object createObject(Constructor constructor, Object... parameters) {
        Object theObject = null;

        try {
            theObject = constructor.newInstance(parameters);
        } catch (Exception e) {
            StringBuilder classNames = new StringBuilder("");

            for (int count = 0; count < parameters.length; count++) {
                if (parameters[count] != null) {
                    classNames.append(parameters[count].getClass().getName()).append(", ");
                    System.out.println(parameters[count].getClass().getName());
                }
            }
            
            Main.log(Level.SEVERE, "Object for class: " + constructor.getName() + " could not be made with parameters: " + classNames.toString(), e.getCause());
        }

        return theObject;
    }
}
