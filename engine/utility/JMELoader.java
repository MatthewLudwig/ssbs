package engine.utility;

import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.math.ColorRGBA;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture2D;
import com.jme3.ui.Picture;
import engine.PictureLayer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author Matthew
 */
public class JMELoader {
    
    public JMELoader() {
        
    }
    
    public Picture getPicture(String name, String path, float xPos, float yPos, PictureLayer layer) {
        Picture picture = new Picture(name);

        changeTexture(picture, path);
        picture.setLocalTranslation(xPos, yPos, layer.getValue());

        return picture;
    }

    public void changeTexture(Picture picture, String texturePath) {
        changeTexture(picture, getTexture(texturePath));
    }

    public Texture2D getTexture(String path) {
        Texture2D texture = (Texture2D) Utilities.am.loadTexture(path);
        texture.setMagFilter(Texture.MagFilter.Nearest);
        texture.setMinFilter(Texture.MinFilter.NearestNoMipMaps);
        return texture;
    }

    public void changeTexture(Picture picture, Texture2D texture) {
        picture.setTexture(Utilities.am, texture, true);
        picture.setWidth(texture.getImage().getWidth());
        picture.setHeight(texture.getImage().getHeight());
    }

    public BitmapText getText(ColorRGBA color, String text, float xPos, float yPos) {
        return getText(Utilities.Impact32, color, text, xPos, yPos);
    }

    public BitmapText getText(BitmapFont font, ColorRGBA color, String text, float xPos, float yPos) {
        BitmapText displayText = new BitmapText(font, false);
        displayText.setSize(font.getCharSet().getRenderedSize());
        displayText.setColor(color);
        displayText.setText(text);
        displayText.setLocalTranslation(xPos, yPos + displayText.getLineHeight(), PictureLayer.GUI.getValue());
        return displayText;
    }
    
    public BitmapText getColorCodedText(BitmapFont font, ColorRGBA baseColor, String text, float xPos, float yPos) {
        Map<Integer, ColorRGBA> indices = new LinkedHashMap<Integer, ColorRGBA>();
        
        int currentIndex = text.indexOf("[c:");
        
        while (currentIndex != -1) {
            String colorCode = text.substring(currentIndex, currentIndex + 15);
            String[] pieces = colorCode.replace("]", "").split(":");
            indices.put(currentIndex, new ColorRGBA(Float.parseFloat(pieces[1]) / 255, Float.parseFloat(pieces[2]) / 255, Float.parseFloat(pieces[3]) / 255, 1));
            text = text.replaceFirst(colorCode.replace("[", "\\[").replace("]", "\\]"), "");
            currentIndex = text.indexOf("[c:");
        }
        
        BitmapText displayText = getText(font, baseColor, text, xPos, yPos);
        
        List<Entry<Integer, ColorRGBA>> entries = new ArrayList<Entry<Integer, ColorRGBA>>();
        
        for (Entry<Integer, ColorRGBA> e : indices.entrySet()) {
            entries.add(e);
        }
        
        for (int count = 0; count < entries.size(); count++) {
            Entry<Integer, ColorRGBA> e1 = entries.get(count);
            int secondIndex;
            
            if (count + 1 < entries.size()) {
                secondIndex = entries.get(count + 1).getKey();
            } else {
                secondIndex = text.length();
            }
            displayText.setColor(e1.getKey(), secondIndex, e1.getValue());
        }
        
        return displayText;
    }

    public BitmapText getCenteredText(ColorRGBA color, String text, float xPos, float yPos) {
        BitmapText displayText = getText(color, text, xPos, yPos);
        displayText.move(-(displayText.getLineWidth() / 2), 0, 0);
        return displayText;
    }

    public BitmapText getCenteredText(BitmapFont font, ColorRGBA color, String text, float xPos, float yPos) {
        BitmapText displayText = getText(font, color, text, xPos, yPos);
        displayText.move(-(displayText.getLineWidth() / 2), 0, 0);
        return displayText;
    }
    
    public BitmapText getCenteredColorCodedText(BitmapFont font, ColorRGBA baseColor, String text, float xPos, float yPos) {
        BitmapText displayText = getColorCodedText(font, baseColor, text, xPos, yPos);
        displayText.move(-(displayText.getLineWidth() / 2), 0, 0);
        return displayText;
    }

}
