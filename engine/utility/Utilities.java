package engine.utility;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.system.JmeSystem;
import com.jme3.texture.Image;

/**
 * Utility class for commonly used functions.
 *
 * @author Matthew Ludwig
 */
public class Utilities {
	public static final float FLOATEPSILON = .00000000001f;
	public static BitmapFont Impact11;
	public static BitmapFont Impact11O;
	public static BitmapFont Impact16;
	public static BitmapFont Impact16O;
	public static BitmapFont Impact24;
	public static BitmapFont Impact24O;
	public static BitmapFont Impact32;
	public static BitmapFont Impact32O;

	public static float lockedTPF = 0;
	protected static AssetManager am;

	private static GeneralUtility generalUtility = new GeneralUtility();
	private static PhysicsUtility physicsUtility = new PhysicsUtility();
	private static CustomLoader customLoader = new CustomLoader();
	private static JMELoader jmeLoader = new JMELoader();

	public static void setup(AssetManager assetManager) {
		am = assetManager;
		// defaultFont =
		// assetManager.loadFont("Interface/Fonts/DefaultFont.fnt");
		// outlinedFont =
		// assetManager.loadFont("Interface/Fonts/OutlinedFont.fnt");
		// smallerOutlinedFont =
		// assetManager.loadFont("Interface/Fonts/SmallerOutlinedFont.fnt");
		// mediumFont = assetManager.loadFont("Interface/Fonts/MediumFont.fnt");
		// smallFont = assetManager.loadFont("Interface/Fonts/SmallFont.fnt");
		// littleFont = assetManager.loadFont("Interface/Fonts/LittleFont.fnt");

		Impact11 = assetManager.loadFont("Interface/Fonts/Impact11.fnt");
		Impact11O = assetManager.loadFont("Interface/Fonts/Impact11O.fnt");
		Impact16 = assetManager.loadFont("Interface/Fonts/Impact16.fnt");
		Impact16O = assetManager.loadFont("Interface/Fonts/Impact16O.fnt");
		Impact24 = assetManager.loadFont("Interface/Fonts/Impact24.fnt");
		Impact24O = assetManager.loadFont("Interface/Fonts/Impact24O.fnt");
		Impact32 = assetManager.loadFont("Interface/Fonts/Impact32.fnt");
		Impact32O = assetManager.loadFont("Interface/Fonts/Impact32O.fnt");

	}

	public static GeneralUtility getGeneralUtility() {
		return generalUtility;
	}

	public static PhysicsUtility getPhysicsUtility() {
		return physicsUtility;
	}

	public static CustomLoader getCustomLoader() {
		return customLoader;
	}

	public static JMELoader getJMELoader() {
		return jmeLoader;
	}

	public static void savePng(File f, Image img) {
		try {
			OutputStream out = new FileOutputStream(f);
			JmeSystem.writeImageFile(out, "png", img.getData(0),
					img.getWidth(), img.getHeight());
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
