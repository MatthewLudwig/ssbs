package engine.utility;

import com.jme3.font.BitmapText;
import com.jme3.math.Matrix4f;
import com.jme3.math.Vector2f;
import com.jme3.texture.image.ImageRaster;
import com.jme3.ui.Picture;
import engine.Angle;
import physics.BoundingBox;

/**
 *
 * @author Matthew
 */
public class PhysicsUtility {
    private BoundingBox box;
    
    public PhysicsUtility() {
        box = new BoundingBox(Vector2f.ZERO.clone(), Vector2f.ZERO.clone());
    }
    
    public boolean complex2DCollision(Vector2f point, Picture picture) {
        box.updateBounds(new Vector2f(picture.getLocalTranslation().x, picture.getLocalTranslation().y), new Vector2f(picture.getLocalScale().x, picture.getLocalScale().y));
        
        if (box.isStrictlyInBounds(point)) {
            ImageRaster imageRaster = ImageRaster.create(picture.getMaterial().getTextureParam("Texture").getTextureValue().getImage());

            if (imageRaster.getPixel((int) ((point.x - box.getLowerLeft().x)), (int) (((point.y - box.getLowerLeft().y)))).a != 0) {
                return true;
            }
        }

        return false;
    }
    
    public boolean complex2DCollisionWithScale(Vector2f point, Picture picture) {
        box.updateBounds(new Vector2f(picture.getLocalTranslation().x * picture.getParent().getLocalScale().x, picture.getLocalTranslation().y * picture.getParent().getLocalScale().y), new Vector2f(picture.getLocalScale().x * picture.getParent().getLocalScale().x, picture.getLocalScale().y * picture.getParent().getLocalScale().y));
        
        if (box.isStrictlyInBounds(point)) {
            ImageRaster imageRaster = ImageRaster.create(picture.getMaterial().getTextureParam("Texture").getTextureValue().getImage());

            if (imageRaster.getPixel((int) ((point.x - box.getLowerLeft().x) / picture.getParent().getLocalScale().x), (int) (((point.y - box.getLowerLeft().y)) / picture.getParent().getLocalScale().y)).a != 0) {
                return true;
            }
        }

        return false;
    }

    public boolean complex2DCollision(Vector2f point, BitmapText text) {
        box.updateBounds(new Vector2f(text.getLocalTranslation().x, text.getLocalTranslation().y), new Vector2f(text.getLineWidth(), text.getLineHeight()));

        return box.isStrictlyInBounds(point);
    }

    /**
     * Returns the correct angle between two points in Angle form.
     */
    public Angle calculateCorrectAngle(Vector2f outerPoint, Vector2f centerPoint) {
        Vector2f difference = outerPoint.subtract(centerPoint);
        float angle = (float) Math.toDegrees(Math.atan2(difference.y, difference.x));
        
        if (angle < -90) {
            angle += 360;
        }
        
        return new Angle(angle);
    }
}
