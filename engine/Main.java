package engine;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import states.TitleScreenState;
import states.game.GameRules;
import states.game.GameState;

import com.jme3.app.SimpleApplication;
import com.jme3.math.Vector2f;
import com.jme3.renderer.RenderManager;
import com.jme3.system.AppSettings;

import engine.input.UserInput;
import engine.ssbs.CharacterList;
import engine.ssbs.EventList;
import engine.ssbs.GameSettings;
import engine.ssbs.MusicList;
import engine.ssbs.StageList;
import engine.utility.Utilities;

/**
 * Main class for Super Smash Bros Strife.
 *
 * @author Matthew Ludwig
 */
public class Main extends SimpleApplication{
    private static final Logger coreLogger = Logger.getLogger(Main.class.getName());
    private static Main instance;
    
    private GameSettings gameSettings;
    private UserInput userInput;
    private GameRules strifeGameRules;
    private CharacterList characterList;
    private StageList stageList;
    private MusicList musicList;
    private EventList eventList;
    
    private int lockedFrames;
    
    private String resourcePath;
    private String systemInfo;

    public static void main(String[] args) {
        instance = new Main();
        
        AppSettings customSettings = new AppSettings(true);
        customSettings.setResolution(640, 480);
        customSettings.setFrameRate(60);
        customSettings.setTitle("Super Smash Bros Strife");
        customSettings.setSamples(0);
        customSettings.setUseJoysticks(false);
        
        instance.setShowSettings(false);
        instance.setSettings(customSettings);
        instance.start();
    }

    public Main() {
        super(new TitleScreenState());
        
        if (Document.userDirectory.startsWith("C:\\Users\\Matthew")) {
            coreLogger.setLevel(Level.ALL);
        } else {
            coreLogger.setLevel(Level.SEVERE);
        }
        
        this.resetData();
        this.loadData();
        
        this.lockedFrames = 0;
        this.resourcePath = "";
    }

    public static void log(Level level, String message, Throwable e) {
        coreLogger.log(level, message, e);
        
        if (level == Level.SEVERE) {
            File crashDirectory = new File(Document.userDirectory + "/Crash Reports");
            
            if (!crashDirectory.exists()) {
                crashDirectory.mkdir();
            }
            
            StringWriter stringWriter = new StringWriter();

            if (e != null) {
                e.printStackTrace(new PrintWriter(stringWriter));
            }
            
            Document doc = new Document("/Crash Reports/Crash_" + Sys.getTime());
            doc.addLine("Crash caused by:  " + message);
            
            if (e != null) {
                doc.addLine("Stack Trace:\n" + stringWriter);
            }
            
            doc.addLine(instance.systemInfo);
            doc.save();
            
            instance.stop();
        }
    }
    
    public final String getSystemInfo() {
        StringBuilder systemInfo = new StringBuilder("System Info:\n");
        
        systemInfo.append("LWJGL Version=").append(Sys.getVersion()).append("\n");
        systemInfo.append("Adapter=").append(Display.getAdapter()).append("\n");
        systemInfo.append("Driver Version=").append(Display.getVersion()).append("\n");
        systemInfo.append("OpenGL Vendor=").append(GL11.glGetString(GL11.GL_VENDOR)).append("\n");
        systemInfo.append("OpenGL Version=").append(GL11.glGetString(GL11.GL_VERSION)).append("\n");
        systemInfo.append("OpenGL Renderer=").append(GL11.glGetString(GL11.GL_RENDERER)).append("\n");
        systemInfo.append("OpenGL Texture Limit=").append(GL11.glGetInteger(GL11.GL_MAX_TEXTURE_SIZE)).append("\n");
        systemInfo.append("System Capabilities=").append(this.renderer.getCaps().toString()).append("\n");
        
        for (Entry e : System.getProperties().entrySet()) {
            systemInfo.append(e.toString()).append("\n");
        }
        
        return systemInfo.toString();
    }
    
    public static void resetSaveData() {
        instance.resetData();
    }
    
    public static Vector2f getRealResolution() {
        return instance.gameSettings.isFullscreen() ? 
                new Vector2f(Display.getDesktopDisplayMode().getWidth(), Display.getDesktopDisplayMode().getHeight()) :
                new Vector2f(640, 480);
    }

    public static GameState getGameState() {
        return instance.getStateManager().getState(GameState.class);
    }

    public static GameSettings getGameSettings() {
        return instance.gameSettings;
    }

    public static UserInput getUserInput() {
        return instance.userInput;
    }
    
    public static GameRules getStrifeGameRules() {
        return instance.strifeGameRules;
    }
    
    public static CharacterList getCharacterList() {
        return instance.characterList;
    }
    
    public static StageList getStageList() {
        return instance.stageList;
    }
    
    public static EventList getEventList() {
        return instance.eventList;
    }
    
    public static MusicList getMusicList() {
        return instance.musicList;
    }
    
    public static void startLoadingResource(String path) {
    	System.out.println(path);
        if (instance.resourcePath.isEmpty()) {
            instance.resourcePath = path;
//            log(Level.INFO, path, null);
        } else {
            log(Level.WARNING, "Cannot start loading a resource before finishing the previous one!", null);
        }
        
        instance.resourcePath = path;
    }
    
    public static void finishLoadingResource(String path) {
        if (instance.resourcePath.equals(path)) {
            instance.resourcePath = "";
        } else {
            log(Level.WARNING, "Cannot finish loading a resource that hasn't been started!", null);
        }
    }

    @Override
    public void simpleInitApp() {   
        Utilities.setup(this.assetManager);        
        this.inputManager.addRawInputListener(this.userInput);
        this.setDisplayStatView(false);
        this.setDisplayFps(false);
        this.systemInfo = this.getSystemInfo();
    }
    
    @Override
    public void simpleUpdate(float tpf) {
        Utilities.lockedTPF = Math.max(Math.min(tpf, .02f), .01f);
        
        if (tpf != Utilities.lockedTPF) {            
            this.lockedFrames += 1;

            if (this.lockedFrames > 30) {
                log(Level.WARNING, "Locking too many frames!", null);
                this.lockedFrames -= 10;
            }
        } else {
            this.lockedFrames = Math.max(--this.lockedFrames, 0);
        }

        if (this.userInput.isKeyClicked("Exit")) {
            this.stop();
        } else if (this.userInput.isKeyClicked("Debug Key")) {
            this.gameSettings.setDebugMode(!this.gameSettings.getDebugMode());
        } else if (this.userInput.isKeyClicked("Fullscreen Key")) {
            this.gameSettings.setFullscreen(!this.gameSettings.isFullscreen());
        }
        
        if (this.settings.isFullscreen() != this.gameSettings.isFullscreen()) {
            this.settings.setFullscreen(this.gameSettings.isFullscreen());
            this.settings.setResolution((int) getRealResolution().x, (int) getRealResolution().y);
            this.restart();
        }
    }

    @Override
    public void simpleRender(RenderManager rm) {}
    
    @Override
    public void destroy() {     
        this.resourcePath = "";
        this.saveData();
        
        super.destroy();
    }
    
    private void resetData() {
        if (this.inputManager != null) {
            this.inputManager.removeRawInputListener(this.userInput);
        }
        
        this.gameSettings = new GameSettings();
        this.userInput = new UserInput();
        this.strifeGameRules = new GameRules();
        this.characterList = new CharacterList();
        this.stageList = new StageList();
        this.eventList = new EventList();
        this.musicList = new MusicList();
        
        if (this.inputManager != null) {
            this.inputManager.addRawInputListener(this.userInput);
        }
    }
    
    private void loadData() {
        if ((new File(Document.userDirectory + "/SaveData.tbs")).exists()) {
            TBS saveData = TBS.load("/SaveData.tbs", 6);

            this.gameSettings.setSFXVolume(saveData.getFloat("SFX Volume"));
            this.gameSettings.setMusicVolume(saveData.getFloat("Music Volume"));
            this.gameSettings.setVoicesVolume(saveData.getFloat("Voices Volume"));
            this.gameSettings.setFullscreen(saveData.getBoolean("Fullscreen"));
            saveData.getTBSBasedData("User Input", this.userInput);
            saveData.getTBSBasedData("Strife Rules", this.strifeGameRules);
            saveData.getTBSBasedData("Character List", this.characterList);
            saveData.getTBSBasedData("Stage List", this.stageList);
            saveData.getTBSBasedData("Event List", this.eventList);
            saveData.getTBSBasedData("Music List", this.musicList);
        }
    }
    
    private void saveData() {
        TBS saveData = new TBS();
        
        saveData.writeFloat("SFX Volume", this.gameSettings.getSFXVolume());
        saveData.writeFloat("Music Volume", this.gameSettings.getMusicVolume());
        saveData.writeFloat("Voices Volume", this.gameSettings.getVoicesVolume());
        saveData.writeBoolean("Fullscreen", this.gameSettings.isFullscreen());
        saveData.writeTBSBasedData("User Input", this.userInput);
        saveData.writeTBSBasedData("Strife Rules", this.strifeGameRules);
        saveData.writeTBSBasedData("Character List", this.characterList);
        saveData.writeTBSBasedData("Stage List", this.stageList);
        saveData.writeTBSBasedData("Event List", this.eventList);
        saveData.writeTBSBasedData("Music List", this.musicList);
        
        saveData.save("/SaveData.tbs", 6);
    }
}
