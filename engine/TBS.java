package engine;

import engine.utility.Utilities;
import java.io.File;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

/**
 * A class that wraps around a Document and allows for organized saving of data.
 *
 * @author Matthew
 */
public class TBS {    
    private static final String TBSMARKER = "<!TBS!>";
    
    private Map<String, String> data;
    private Map<String, TBS> TBSData;
    
    public TBS() {
        this.data = new HashMap<String, String>();
        this.TBSData = new HashMap<String, TBS>();
    }
    
    public boolean containsData(String key) {
        return this.data.containsKey(key);
    }
    
    public boolean containsTBSBasedData(String key) {
        return this.TBSData.containsKey(key);
    }
    
//    Make sure to use the parse functions in all of these and not the valueOf.
//    valueOf returns boxed versions of the primitives which if returning an unboxed,
//    will then just be unboxed wasting some time.
    
    public void writeString(String key, String data) {
        this.data.put(key, data);
    }
    
    public String getString(String key) {
        return this.data.containsKey(key) ? this.data.get(key) : "";
    }
    
    public void writeInt(String key, int data) {
        this.data.put(key, String.valueOf(data));
    }
    
    public int getInt(String key) {
        return this.data.containsKey(key) ? Integer.parseInt(this.data.get(key)) : 0;
    }
    
    public void writeBoolean(String key, boolean data) {
        this.data.put(key, String.valueOf(data));
    }
    
    public boolean getBoolean(String key) {
        return this.data.containsKey(key) ? Boolean.parseBoolean(this.data.get(key)) : false;
    } 
    
    public void writeFloat(String key, float data) {
        this.data.put(key, String.valueOf(data));
    }
    
    public float getFloat(String key) {
        return this.data.containsKey(key) ? Float.parseFloat(this.data.get(key)) : 0;
    }
    
    public void writeTBSBasedData(String key, TBSBased data) {
        this.TBSData.put(key, data.save());
    }
    
    public TBSBased getTBSBasedData(String key, TBSBased data) {
        if (this.TBSData.containsKey(key)) {
            data.load(this.TBSData.get(key));
        }
        
        return data;
    }
    
    private String saveToString(int depth) {
        StringBuilder builder = new StringBuilder();
        String divider = "!>" + depth + "<!";
        String seperator = "<!" + depth + "!>";
        
        Iterator<Entry<String, String>> dataIterator = this.data.entrySet().iterator();
        
        while (dataIterator.hasNext()) {
            Entry<String, String> e = dataIterator.next();
            
            builder.append(e.getKey()).append(divider).append(e.getValue());
            
            if (dataIterator.hasNext()) {
                builder.append(seperator);
            }
        }
        
        if (!this.data.isEmpty() && !this.TBSData.isEmpty()) {
            builder.append(seperator);
        }
        
        Iterator<Entry<String, TBS>> TBSDataIterator = this.TBSData.entrySet().iterator();
        
        while (TBSDataIterator.hasNext()) {
            Entry<String, TBS> e = TBSDataIterator.next();
            
            builder.append(TBSMARKER).append(e.getKey()).append(divider).append(e.getValue().saveToString(depth + 1));
            
            if (TBSDataIterator.hasNext()) {
                builder.append(seperator);
            }
        }
        
        return builder.toString();
    }
    
    private void loadFromString(String saveData, int depth) {
        String divider = "!>" + depth + "<!";
        String seperator = "<!" + depth + "!>";
        String[] rawData = saveData.split(seperator);
        
        for (String pairedData : rawData) {
            String[] splitData = pairedData.split(divider);
            
            if (splitData[0].startsWith(TBSMARKER)) {
                TBS loadedTBS = new TBS();
                loadedTBS.loadFromString(splitData[1], depth + 1);
                this.TBSData.put(splitData[0].replace(TBSMARKER, ""), loadedTBS);
            } else {
                this.data.put(splitData[0], splitData[1]);
            }
        }
        
//        for (String pairedData : rawData[1].split(seperator)) {
//            String[] splitData = pairedData.split(divider);
//            
//            TBS loadedTBS = new TBS();
//            loadedTBS.loadFromString(splitData[1], depth + 1);
//            this.TBSData.put(splitData[0], loadedTBS);
//        }
    }
    
    public void save(String filePath, int encryptionKey) {
        try {
            byte[] bytes = this.saveToString(0).getBytes();
            bytes = Utilities.getGeneralUtility().encryptByteArray(bytes, encryptionKey);
            Files.write(new File(Document.userDirectory + filePath).toPath(), bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static TBS load(String filePath, int encryptionKey) {
        TBS loadedData = new TBS();
        
        File file = new File(Document.userDirectory + filePath);
        
        if (file.exists()) {
            try {
                byte[] bytes = Files.readAllBytes(file.toPath());
                bytes = Utilities.getGeneralUtility().encryptByteArray(bytes, encryptionKey);
                loadedData.loadFromString(new String(bytes), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Main.log(Level.WARNING, "Attempted to load non existant file with the path:  " + filePath, null);
        }
        
        return loadedData;
    }
}
