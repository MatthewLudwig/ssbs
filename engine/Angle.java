package engine;

/**
 * Simple class that represents an angle and allows easy use of all functions necessary for angles.
 * Angles start at -90 at the bottom and then go around to +270.
 * 
 * @author Matthew
 */
public class Angle {
    private float angle;
    
    public Angle(float initialValue) {
        this.angle = initialValue;
    }
    
    public void setValue(float value) {
        this.angle = value;
        this.checkAngle();
    }
    
    public float getValue() {
        return this.angle;
    }
    
    public float sin() {
        return (float) Math.sin(Math.toRadians(angle));
    }
    
    public float cos() {
        return (float) Math.cos(Math.toRadians(angle));
    }
    
    public float tan() {
        return (float) Math.tan(Math.toRadians(angle));
    }
    
    public Angle add(float rotateBy) {
        this.angle += rotateBy;
        this.checkAngle();
        return this;
    }
    
    public Angle subtract(float rotateBy) {
        this.angle -= rotateBy;
        this.checkAngle();
        return this;
    }
    
    public Angle multiply(float multiplyBy) {
        this.angle *= multiplyBy;
        this.checkAngle();
        return this;
    }
    
    public Angle divide(float divideBy) {
        this.angle /= divideBy;
        this.checkAngle();
        return this;
    }
   
    public void checkAngle() {
        boolean shouldContinue = true;
        
        while(shouldContinue) {
            if (this.angle > 270) {
                this.angle = this.angle - 360;
            } else if (this.angle < -90) {
                this.angle = 360 + this.angle;
            } else {
                shouldContinue = false;
            }
        }
    }
}
