package engine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

public class Document {
    public static final String userDirectory = System.getProperty("user.dir");
    private List<String> lines;
    private File file;
    
    /**
     * Temporary method present only for loading text files from the JME3 assets directory.
     * Documents produced through this method cannot be saved!
     * 
     * @return A Document object preloaded with information but with no file associated with it.
     */
    public static Document loadFromAssets(String path) {
        Document doc = new Document();
        
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(doc.getClass().getResourceAsStream(path)));

            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                doc.lines.add(line);
            }

            reader.close();
        } catch (Exception e) {
            Main.log(Level.SEVERE, "Error loading document at:  " + path, e);
        }
        
        return doc;
    }
    
    private Document() {
        this.lines = new LinkedList<String>();
    }

    public Document(String path) {
        this.lines = new LinkedList<String>();

        if (path.startsWith("/")) {
            this.file = new File(userDirectory + path);
        } else {
            this.file = new File(path);
        }
    }

    public void addLine(String par1) {
        this.lines.add(par1);
    }

    public String getLine(int par1) {
        return this.lines.get(par1);
    }

    public int getSize() {
        return this.lines.size();
    }

    public List<String> getLines() {
        return this.lines;
    }

    public void save() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(this.file));

            for (String line : this.lines) {
                writer.write(line);
                writer.newLine();
            }

            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void load() {
        if (this.file.exists()) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(this.file));

                for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                    this.lines.add(line);
                }

                reader.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Main.log(Level.WARNING, "Attempted to load non existant file with the path:  " + this.file.getPath(), null);
        }
    }
}
