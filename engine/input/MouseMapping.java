package engine.input;

/**
 *
 * @author Matthew
 */
public class MouseMapping {

    private int keyMapping;
    private boolean clicked;
    private boolean state;

    protected MouseMapping() {
        this.setState(false);
    }

    protected final void setMappingID(int mappingID) {
        this.keyMapping = mappingID;
    }

    protected final int getMappingID() {
        return this.keyMapping;
    }

    protected final void setState(boolean newState) {
        if (this.state == true && newState == false) {
            this.clicked = true;
        }
        
        this.state = newState;
    }
    
    protected final void setUnClicked() {
        this.clicked = false;
    }

    protected final boolean getState() {
        return this.state;
    }
    
    protected final boolean isClicked() {
        return this.clicked;
    }
}
