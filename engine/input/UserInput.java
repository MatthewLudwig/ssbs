package engine.input;

import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.RawInputListener;
import com.jme3.input.event.JoyAxisEvent;
import com.jme3.input.event.JoyButtonEvent;
import com.jme3.input.event.KeyInputEvent;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.input.event.MouseMotionEvent;
import com.jme3.input.event.TouchEvent;
import com.jme3.math.Vector2f;
import engine.Main;
import engine.TBS;
import engine.TBSBased;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

/**
 *
 * @author Matthew
 */
public class UserInput implements RawInputListener, TBSBased {        
    private Map<String, KeyMapping> keyMap;
    
    private Vector2f mousePosition;
    private MouseMapping leftMouseButton;
    private MouseMapping middleMouseButton;
    private MouseMapping rightMouseButton;
    
    private String controlToSet;
    
    private boolean acceptingInputs;

    public UserInput() {
        this.setUpKeyMap();
        this.mousePosition = new Vector2f(0, 0);
        this.leftMouseButton = new MouseMapping();
        this.middleMouseButton = new MouseMapping();
        this.rightMouseButton = new MouseMapping();
        this.controlToSet = "";
        this.acceptingInputs = true;
    }

    private void setUpKeyMap() {
        this.keyMap = new HashMap<String, KeyMapping>();

        this.keyMap.put("Fullscreen Key", new KeyMapping(KeyInput.KEY_F11));
        this.keyMap.put("Debug Key", new KeyMapping(KeyInput.KEY_F12));
        this.keyMap.put("Exit", new KeyMapping(KeyInput.KEY_ESCAPE));
        this.keyMap.put("P1 Up", new KeyMapping(KeyInput.KEY_W));
        this.keyMap.put("P1 Right", new KeyMapping(KeyInput.KEY_D));
        this.keyMap.put("P1 Down", new KeyMapping(KeyInput.KEY_S));
        this.keyMap.put("P1 Left", new KeyMapping(KeyInput.KEY_A));
        this.keyMap.put("P1 Attack", new KeyMapping(KeyInput.KEY_T));
        this.keyMap.put("P1 Special", new KeyMapping(KeyInput.KEY_Y));
        this.keyMap.put("P1 Shield", new KeyMapping(KeyInput.KEY_U));
        this.keyMap.put("P1 Pause", new KeyMapping(KeyInput.KEY_SPACE));
        this.keyMap.put("P2 Up", new KeyMapping(KeyInput.KEY_UP));
        this.keyMap.put("P2 Right", new KeyMapping(KeyInput.KEY_RIGHT));
        this.keyMap.put("P2 Down", new KeyMapping(KeyInput.KEY_DOWN));
        this.keyMap.put("P2 Left", new KeyMapping(KeyInput.KEY_LEFT));
        this.keyMap.put("P2 Attack", new KeyMapping(KeyInput.KEY_SLASH));
        this.keyMap.put("P2 Special", new KeyMapping(KeyInput.KEY_PERIOD));
        this.keyMap.put("P2 Shield", new KeyMapping(KeyInput.KEY_COMMA));
        this.keyMap.put("P2 Pause", new KeyMapping(KeyInput.KEY_RETURN));
        this.keyMap.put("P3 Up", new KeyMapping(-1));
        this.keyMap.put("P3 Right", new KeyMapping(-1));
        this.keyMap.put("P3 Down", new KeyMapping(-1));
        this.keyMap.put("P3 Left", new KeyMapping(-1));
        this.keyMap.put("P3 Attack", new KeyMapping(-1));
        this.keyMap.put("P3 Special", new KeyMapping(-1));
        this.keyMap.put("P3 Shield", new KeyMapping(-1));
        this.keyMap.put("P4 Up", new KeyMapping(-1));
        this.keyMap.put("P4 Right", new KeyMapping(-1));
        this.keyMap.put("P4 Down", new KeyMapping(-1));
        this.keyMap.put("P4 Left", new KeyMapping(-1));
        this.keyMap.put("P4 Attack", new KeyMapping(-1));
        this.keyMap.put("P4 Special", new KeyMapping(-1));
        this.keyMap.put("P4 Shield", new KeyMapping(-1));
    }

    public TBS save() {
        TBS saveData = new TBS();
        
        for (Entry<String, KeyMapping> e : this.keyMap.entrySet()) {
            saveData.writeInt(e.getKey(), e.getValue().getMappingID());
        }
        
        return saveData;
    }

    public void load(TBS saveData) {
        for (Entry<String, KeyMapping> e : this.keyMap.entrySet()) {
            if (saveData.containsData(e.getKey())) {
                e.setValue(new KeyMapping(saveData.getInt(e.getKey())));
            }
        }
    }
    
    public void beginInput() {
        for(KeyMapping mapping : this.keyMap.values()) {
            mapping.setUnClicked();
        }
        
        this.leftMouseButton.setUnClicked();
        this.middleMouseButton.setUnClicked();
        this.rightMouseButton.setUnClicked();
    }

    public void endInput() {
    }

    public void onJoyAxisEvent(JoyAxisEvent evt) {
        evt.setConsumed();
        int id = -(evt.getJoyIndex() + (10 * evt.getAxisIndex()) + (evt.getValue() > 0 ? 100 : 200)) * 10;
        this.onButtonPress(id, Math.abs(evt.getValue()) > .5f);
    }

    public void onJoyButtonEvent(JoyButtonEvent evt) {
        evt.setConsumed();
        int id = -(evt.getJoyIndex() + (10 * evt.getButtonIndex()));
        this.onButtonPress(id, evt.isPressed());
    }

    public void onTouchEvent(TouchEvent evt) {
        throw new UnsupportedOperationException("Sorry but we do not support touch screen input!");
    }

    public void onMouseMotionEvent(MouseMotionEvent evt) {
        evt.setConsumed();

        this.mousePosition.addLocal(evt.getDX(), evt.getDY());
        
        if (Main.getGameSettings().isFullscreen()) {
            this.mousePosition.setX(Math.max(Math.min(this.mousePosition.x, 640), 0));
            this.mousePosition.setY(Math.max(Math.min(this.mousePosition.y, 480), 0));
        }
    }

    public void onMouseButtonEvent(MouseButtonEvent evt) {
        evt.setConsumed();
                    
        if (this.acceptingInputs) {

            switch (evt.getButtonIndex()) {
                case MouseInput.BUTTON_LEFT:
                    this.leftMouseButton.setState(evt.isPressed());
                    break;
                case MouseInput.BUTTON_MIDDLE:
                    this.middleMouseButton.setState(evt.isPressed());
                    break;
                case MouseInput.BUTTON_RIGHT:
                    this.rightMouseButton.setState(evt.isPressed());
                    break;
                default:
                    throw new UnsupportedOperationException("Sorry we do not support the mouse button that you just pressed!");
            }
        }
    }

    public void onKeyEvent(KeyInputEvent evt) {
        evt.setConsumed();
        this.onButtonPress(evt.getKeyCode(), evt.isPressed());
    }
    
    public void onButtonPress(int buttonID, boolean isPressed) {
        if (this.acceptingInputs) {
            if (this.controlToSet.isEmpty()) {
                for (KeyMapping mapping : this.keyMap.values()) {
                    if (mapping.getMappingID() == buttonID) {
                        mapping.setState(isPressed);

                        return;
                    }
                }
            } else if (isPressed) {
                this.keyMap.get(this.controlToSet).setMappingID(buttonID);
                this.controlToSet = "";
            }
        }
    }

    public void setControlToNextEvent(String mappingName) {
        this.controlToSet = mappingName;
    }
    
    public boolean isSettingControl() {
        return !this.controlToSet.isEmpty();
    }
    
    public void setAcceptingInputs(boolean state) {
        this.acceptingInputs = state;
        
        for (KeyMapping mapping : this.keyMap.values()) {
            mapping.setState(false);
        }
    }
    
    public boolean isAcceptingInputs() {
        return this.acceptingInputs;
    }

    /**
     * Returns the state of the KeyMapping associated with the given string.
     * Will return true if the key is down. Will return false if the key is up
     * or if the given string is not associated with a KeyMapping.
     */
    public boolean getKeyState(String key) {
    	if (key.equals("Pause")){
    		return getKeyState("P1 Pause") || getKeyState("P2 Pause") || getKeyState("P3 Pause") || getKeyState("P4 Pause");
    	}
        KeyMapping mapping = this.keyMap.get(key);

        if (mapping != null) {
            return mapping.getState();
        } else {
            Main.log(Level.WARNING, "Attempting to check state of unknown mapping:  " + key, null);
            return false;
        }
    }
    
    public boolean isKeyClicked(String key) {
    	if (key.equals("Pause")){
    		return isKeyClicked("P1 Pause") || isKeyClicked("P2 Pause") || isKeyClicked("P3 Pause") || isKeyClicked("P4 Pause");
    	}
        KeyMapping mapping = this.keyMap.get(key);
        
        if (mapping != null) {
            return mapping.isClicked();
        } else {
            Main.log(Level.WARNING, "Attempting to check state of unknown mapping:  " + key, null);
            return false;
        }
    }

    public String getKeyName(String key) {
        KeyMapping mapping = this.keyMap.get(key);

        if (mapping != null) {
            switch (mapping.getMappingID()) {
                case -1:
                    return "N/A";
                case KeyInput.KEY_0:
                    return "0";
                case KeyInput.KEY_1:
                    return "1";
                case KeyInput.KEY_2:
                    return "2";
                case KeyInput.KEY_3:
                    return "3";
                case KeyInput.KEY_4:
                    return "4";
                case KeyInput.KEY_5:
                    return "5";
                case KeyInput.KEY_6:
                    return "6";
                case KeyInput.KEY_7:
                    return "7";
                case KeyInput.KEY_8:
                    return "8";
                case KeyInput.KEY_9:
                    return "9";
                case KeyInput.KEY_A:
                    return "A";
                case KeyInput.KEY_B:
                    return "B";
                case KeyInput.KEY_C:
                    return "C";
                case KeyInput.KEY_D:
                    return "D";
                case KeyInput.KEY_E:
                    return "E";
                case KeyInput.KEY_F:
                    return "F";
                case KeyInput.KEY_G:
                    return "G";
                case KeyInput.KEY_H:
                    return "H";
                case KeyInput.KEY_I:
                    return "I";
                case KeyInput.KEY_J:
                    return "J";
                case KeyInput.KEY_K:
                    return "K";
                case KeyInput.KEY_L:
                    return "L";
                case KeyInput.KEY_M:
                    return "M";
                case KeyInput.KEY_N:
                    return "N";
                case KeyInput.KEY_O:
                    return "O";
                case KeyInput.KEY_P:
                    return "P";
                case KeyInput.KEY_Q:
                    return "Q";
                case KeyInput.KEY_R:
                    return "R";
                case KeyInput.KEY_S:
                    return "S";
                case KeyInput.KEY_T:
                    return "T";
                case KeyInput.KEY_U:
                    return "U";
                case KeyInput.KEY_V:
                    return "V";
                case KeyInput.KEY_W:
                    return "W";
                case KeyInput.KEY_X:
                    return "X";
                case KeyInput.KEY_Y:
                    return "Y";
                case KeyInput.KEY_Z:
                    return "Z";
                case KeyInput.KEY_UP:
                    return "UP";
                case KeyInput.KEY_RIGHT:
                    return "RIGHT";
                case KeyInput.KEY_DOWN:
                    return "DOWN";
                case KeyInput.KEY_LEFT:
                    return "LEFT";
                case KeyInput.KEY_ESCAPE:
                    return "ESCAPE";
                case KeyInput.KEY_RETURN:
                    return "ENTER";
                case KeyInput.KEY_SPACE:
                    return "SPACEBAR";
                case KeyInput.KEY_PERIOD:
                    return ".";
                case KeyInput.KEY_COMMA:
                    return ",";
                case KeyInput.KEY_END:
                    return "END";
                case KeyInput.KEY_SLASH:
                    return "/";
                case KeyInput.KEY_NUMPAD0:
                    return "NUM0";
                case KeyInput.KEY_NUMPAD1:
                    return "NUM1";
                case KeyInput.KEY_NUMPAD2:
                    return "NUM2";
                case KeyInput.KEY_NUMPAD3:
                    return "NUM3";
                case KeyInput.KEY_NUMPAD4:
                    return "NUM4";
                case KeyInput.KEY_NUMPAD5:
                    return "NUM5";
                case KeyInput.KEY_NUMPAD6:
                    return "NUM6";
                case KeyInput.KEY_NUMPAD7:
                    return "NUM7";
                case KeyInput.KEY_NUMPAD8:
                    return "NUM8";
                case KeyInput.KEY_NUMPAD9:
                    return "NUM9";
                default:
                    if (mapping.getMappingID() < 0) {
                        return "CNTLR";
                    } else {
                        return "(ID: " + mapping.getMappingID() + ")";
                    }
            }
        } else {
            Main.log(Level.WARNING, "Attempting to obtain the key name of unknown mapping:  " + key, null);
            return "ERROR";
        }
    }
    
    public Vector2f getMousePosition() {
        return this.mousePosition;
    }
    
    public boolean getLeftButtonState() {
        return this.leftMouseButton.getState();
    }

    public boolean isLeftButtonClicked() {
        return this.leftMouseButton.isClicked();
    }
    
    public boolean getMiddleButtonState() {
        return this.middleMouseButton.getState();
    }

    public boolean isMiddleButtonClicked() {
        return this.middleMouseButton.isClicked();
    }
    
    public boolean getRightButtonState() {
        return this.rightMouseButton.getState();
    }

    public boolean isRightButtonClicked() {
        return this.rightMouseButton.isClicked();
    }
}
