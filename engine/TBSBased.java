package engine;

/**
 *
 * @author Matthew
 */
public interface TBSBased {
    public TBS save();
    public void load(TBS saveData);
}
