package entities;

import com.jme3.math.Vector3f;

/**
 *
 * @author Matthew
 */
public enum Team {
    RED("Red", new Vector3f(1, .5f, .5f), new Vector3f(1, .25f, .25f)), 
    BLUE("Blue", new Vector3f(.5f, .5f, 1), new Vector3f(.25f, .25f, 1)), 
    YELLOW("Yellow", new Vector3f(1, 1, 1), new Vector3f(1, 1, 1)), 
    GREEN("Green", new Vector3f(.5f, 1, .5f), new Vector3f(.25f, 1, .25f)), 
    NONE("None", new Vector3f(1, 1, 1), new Vector3f(1, 1, 1));
    
    public final String displayName;
    public final Vector3f colorOne;
    public final Vector3f colorTwo;
    
    private Team(String display, Vector3f color1, Vector3f color2) {
        this.displayName = display;
        this.colorOne = color1;
        this.colorTwo = color2;
    }
}
