package entities.pokemon;

import engine.Main;
import engine.utility.Utilities;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

public class Entei extends Pokemon {
    
    private float updateCounter;
    private boolean hasAttacked;
    
    public Entei() {
        this.updateCounter = 0;
        this.hasAttacked = false;
        this.setAttackCapability(8.03f, 1.04f, 99, false);
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.hasAttacked) {
            if (this.updateCounter >= 5.75f) {
                this.updateCounter -= 5.75f;
                this.setDead(true);
            }
        } else {
            if (this.updateCounter >= .5f) {
                this.hasAttacked = true;
                this.updateCounter -= .5f;
                
                EnteiFireSpin fireSpin = new EnteiFireSpin();
                fireSpin.team = this.team;
                Main.getGameState().spawnEntity(fireSpin);
                fireSpin.setPosition(this.getAppropriateBounds().getLowerMidpoint().x, this.getAppropriateBounds().getLowerMidpoint().y);
                fireSpin.setFacing(this.getFacing());
                fireSpin.setSpawner(this.getSpawner());
            }
        }
        
        this.updateCounter += Utilities.lockedTPF;
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "entei", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
