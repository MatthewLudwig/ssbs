package entities.pokemon;

import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.ViolentEntity;
import physics.CollisionInfo;

public abstract class Pokemon extends ViolentEntity {
    
    private PhysicsEntity spawner; 
    
    public Pokemon() {
        super("Pokemon", 1, new PhysicsInfo(0, 1, 1, 1), 1, 1, 1);
        this.setHasSuperArmor(true);
    }
    
    protected PhysicsEntity getSpawner() {
        return this.spawner;
    }
    
    public void setSpawner(PhysicsEntity entity) {
        this.spawner = entity;
    }
    
    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
        if (!entity.equals(this.spawner)) {
            super.onCollideWith(entity, info);
        }
    }
    
    @Override
    public boolean reflect(PhysicsEntity reflecter) {
        return false;
    }
    
    @Override
    public void setFinalSmash(boolean state) {
        ((ViolentEntity) this.spawner).setFinalSmash(true);
    }
    
    @Override
    protected final String getDefaultAction() {
        return "Default Action";
    }
}
