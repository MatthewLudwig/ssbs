package entities.pokemon;

import engine.utility.Utilities;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

public class Torchic extends Pokemon {
    
    private float updateCounter;
    private boolean hasAttacked;
    
    public Torchic() {
        this.updateCounter = 0;
        this.hasAttacked = false;
        this.scale = (.5f);
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.updateCounter >= 1.0f) {
            if (this.hasAttacked) {
                this.setDead(true);
            } else {
                this.setCurrentAction(this.actionMap.get("Attack Action"));
                this.setAttackCapability(6.49f, 1.09f, 99, true);
                this.hasAttacked = true;
            }
            
            this.updateCounter -= 1.0f;
        }
        
        if (this.currentAction.equals(this.actionMap.get("Default Action"))) {
            this.updateCounter += Utilities.lockedTPF;
            this.setAttackCapability(0, 0, 0, false);
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "torchic", StayingPower.BARELY, 1f));
        actionMap.put("Attack Action", new CharacterAction(this, "torchicEmber", StayingPower.LOW, 1f));
        return actionMap;
    }
}
