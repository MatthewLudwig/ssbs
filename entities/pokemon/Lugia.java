package entities.pokemon;

import com.jme3.math.Vector2f;
import engine.Main;
import engine.utility.Utilities;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class Lugia extends Pokemon {
    private float updateCounter;
    private short state;
    
    public Lugia() {
        this.updateCounter = 0;
        this.state = 0;
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        switch (this.state) {
            case 0:
                if (this.updateCounter >= 1.00f) {
                    this.updateCounter -= 1.00f;
                    this.state++;
                    
                    Vector2f oppositeCorner = this.getFacing() ? Main.getGameState().getWorld().getStage().getCameraBounds().getUpperLeft() :
                            Main.getGameState().getWorld().getStage().getCameraBounds().getUpperRight();
                    
                    this.setPosition(oppositeCorner.x, oppositeCorner.y);
                }
                
                break;
            case 50:
                this.setDead(true);
                break;
            default:
                if (this.updateCounter >= .25f) {
                    this.updateCounter -= .25f;
                    this.state++;
                
                    Vector2f oppositeCorner = this.getFacing() ? Main.getGameState().getWorld().getStage().getCameraBounds().getLowerRight() :
                                Main.getGameState().getWorld().getStage().getCameraBounds().getLowerLeft();
                    Vector2f ringVelocity = oppositeCorner.subtract(this.getAppropriateBounds().getLowerMidpoint()).normalize().mult(90);

                    LugiaRing ring = new LugiaRing();
                    ring.team = this.team;
                    Main.getGameState().spawnEntity(ring);
                    ring.setPosition(this.getAppropriateBounds().getLowerMidpoint().x, this.getAppropriateBounds().getLowerMidpoint().y);
                    ring.setFacing(this.getFacing());
                    ring.setSpawner(this.getSpawner());
                    ring.setVelocity(ringVelocity.x, ringVelocity.y, true);
                }
                
                break;
        }
        
        this.updateCounter += Utilities.lockedTPF;
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "lugia", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
