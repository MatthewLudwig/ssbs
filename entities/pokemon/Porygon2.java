package entities.pokemon;

import engine.utility.Utilities;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class Porygon2 extends Pokemon {
    
    private float updateCounter;
    
    public Porygon2() {
        this.updateCounter = 0;
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.updateCounter <= .5f) {
            this.setAttackCapability(0, 0, 0, false);
        } else if (this.updateCounter <= 1.5f) {
            this.setAttackCapability(6, .8f, 99, false);
            this.setPureXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.getFacing()) * 18);
        } else if (this.updateCounter <= 2.0f) {
            this.setAttackCapability(0, 0, 0, false);
            this.setPureXVelocity(0);
        } else if (this.updateCounter >= 2.0f) {
            this.setDead(true);
        }
        
        this.updateCounter += Utilities.lockedTPF;
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "porygon2", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
