package entities.pokemon;

import engine.Angle;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.ViolentEntity;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.CollisionInfo;

public class Wobbuffet extends Pokemon {
    
    private float updateCounter;
    private float pivotTimer;
    private boolean pivotDirection;
    private Angle pivotAngle;
    
    public Wobbuffet() {
        this.updateCounter = 0;
        this.pivotTimer = 0;
        this.pivotDirection = true;
        this.pivotAngle = new Angle(0);
        
        this.scale = (.75f);
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.updateCounter >= 10) {
            this.setDead(true);
        } else if (this.pivotTimer > 0) {
            this.pivotTimer -= Utilities.lockedTPF;
            
            float halfHeight = this.getAppropriateBounds().getDimensions().y / 2;
            
            this.addToPosition(Utilities.getGeneralUtility().getBooleanAsSign(this.facing) * this.pivotAngle.sin() * halfHeight, -this.pivotAngle.cos() * halfHeight);                
            this.pivotAngle.add(Utilities.getGeneralUtility().getBooleanAsSign(this.pivotDirection) * 1200 * Utilities.lockedTPF);

            if (this.pivotTimer <= 0) {
                this.setAttackCapability(0, 0, 0, false);
                this.pivotAngle = new Angle(0);
            }
            
            if (this.pivotAngle.getValue() < -30 || this.pivotAngle.getValue() > 30) {
                this.pivotDirection = !this.pivotDirection;
            }
            
            this.rotateUpTo(this.pivotAngle);
            this.addToPosition(Utilities.getGeneralUtility().getBooleanAsSign(this.facing) * -this.pivotAngle.sin() * halfHeight, this.pivotAngle.cos() * halfHeight);
        }
        
        this.updateCounter += Utilities.lockedTPF;
    }
    
    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
        if (entity instanceof ViolentEntity && !((ViolentEntity) entity).isOnSameTeamAs(this) && ((ViolentEntity) entity).getDamageDealt() > 0) {
            this.pivotTimer = .75f;
            this.setAttackCapability(2 * ((ViolentEntity) entity).getDamageDealt(), 2 * ((ViolentEntity) entity).getKnockback(), 99, false);
        } else {
            super.onCollideWith(entity, info);
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "wobbuffet", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
