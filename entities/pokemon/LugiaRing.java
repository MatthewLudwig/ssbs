package entities.pokemon;

import engine.Main;
import engine.utility.Utilities;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class LugiaRing extends Pokemon {
    
    private float updateCounter;
    private float times;
    
    public LugiaRing() {
        this.updateCounter = 0;
        this.times = 0;

        this.setAttackCapability(10.06f, 1.21f, 99f, true);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
    }
    
    @Override
    public void onDeath() {
        super.onDeath();
        System.out.println("Yo");
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.updateCounter >= .1f) {
            this.updateCounter -= .1f;
            this.setScale(.2f + (10f * (this.getVelocity().mult(this.times).length() / Main.getGameState().getWorld().getStage().getCameraBounds().getDimensions().length())));
            this.times += 1;
        }
        
        this.updateCounter += Utilities.lockedTPF;
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "lugiaWind", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
