package entities.pokemon;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.items.ItemChanseyEgg;
import java.util.HashMap;
import java.util.Map;

public class Chansey extends Pokemon {
    
    private float updateCounter;
    private int updates;
    
    public Chansey() {
        this.updateCounter = 0;
        this.updates = 0;
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.updateCounter >= 1.0f) {
            this.updateCounter -= 1.0f;
            this.updates++;
            ItemChanseyEgg egg = new ItemChanseyEgg();
            Main.getGameState().spawnEntity(egg);
            egg.setPosition(this.getAppropriateBounds().getLowerMidpoint().x, this.getAppropriateBounds().getLowerMidpoint().y);
            egg.setFacing(this.getFacing());
            
            Angle angle = new Angle((float) Math.random() * 180);
            egg.setVelocity(30 * angle.cos(), 30 * angle.sin(), true);
        } 
        
        if (this.updates >= 5) {
            this.setDead(true);
        }
        
        this.updateCounter += Utilities.lockedTPF;
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "chansey", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
