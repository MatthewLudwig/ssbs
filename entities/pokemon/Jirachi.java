package entities.pokemon;

import engine.Main;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.items.ItemOneUp;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class Jirachi extends Pokemon {
    
    private boolean hasSpawnedOneUp;
    
    public Jirachi() {
        this.hasSpawnedOneUp = false;
        
        this.scale = (.5f);
        
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        this.setPureYVelocity(30);
        
        if (!this.hasSpawnedOneUp) {
            ItemOneUp oneUp = new ItemOneUp();
            Main.getGameState().spawnEntity(oneUp);
            oneUp.setPosition(this.getAppropriateBounds().getLowerMidpoint().x, this.getAppropriateBounds().getLowerMidpoint().y);
            oneUp.setFacing(this.getFacing());
            this.hasSpawnedOneUp = true;
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "jirachi", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
