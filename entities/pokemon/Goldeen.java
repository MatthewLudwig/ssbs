package entities.pokemon;

import engine.utility.Utilities;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

public class Goldeen extends Pokemon {
    
    private float updateCounter;
    private int updates;
    
    public Goldeen() {
        this.updateCounter = 0;
        this.updates = 0;
        this.scale = (.5f);
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.updates == 4) {
            this.setDead(true);
        } else {
            if (this.updateCounter >= .75f) {
                this.setFacing(!this.getFacing());
                this.updateCounter -= .75f;
                this.updates++;
            } else {
                this.updateCounter += Utilities.lockedTPF;
            }
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "goldeen", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
