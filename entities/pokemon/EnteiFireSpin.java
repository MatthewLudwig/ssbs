package entities.pokemon;

import com.jme3.math.Vector2f;
import engine.Main;
import engine.utility.Utilities;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.BoundingBox;

public class EnteiFireSpin extends Pokemon {
    
    private static final float TIMETOFULLSCALE = 3.75f;
    
    private float updateCounter;
    private float scaleFactor;
    private float times;
    
    public EnteiFireSpin() {
        this.updateCounter = 0;
        this.scaleFactor = 0;
        this.times = 0;
        
        this.setAttackCapability(8.03f, 1.04f, 99, true);
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.times >= 100) {
            this.setDead(true);
        } else {
            if (this.scaleFactor == 0) {
                BoundingBox cameraBounds = Main.getGameState().getWorld().getStage().getCameraBounds();
                BoundingBox myBounds = this.getAppropriateBounds();
                
                float distanceToLeft = Math.abs(cameraBounds.getLeftMidpoint().x - myBounds.getExactCenter().x);
                float distanceToRight = Math.abs(cameraBounds.getRightMidpoint().x - myBounds.getExactCenter().x);
                float distanceToTop = Math.abs(cameraBounds.getUpperMidpoint().y - myBounds.getExactCenter().y);
                float distanceToBottom = Math.abs(cameraBounds.getLowerMidpoint().y - myBounds.getExactCenter().y);
                
                Vector2f newDimensions = new Vector2f(Math.min(distanceToLeft, distanceToRight) * 2, Math.min(distanceToTop, distanceToBottom) * 2);
                float requiredScale = Math.min(newDimensions.x / myBounds.getDimensions().x, newDimensions.y / myBounds.getDimensions().y);
                this.scaleFactor = (requiredScale / TIMETOFULLSCALE) / 10;
            } 
            
            if (this.updateCounter >= .05f) {
                this.updateCounter -= .05f;
                this.times += 1;
                
                if (this.times <= 15) {
                    this.setScale(1 + (this.scaleFactor * this.times));
                }
            }
        }
        
        this.display.setLocalTranslation(this.display.getLocalTranslation().x, this.display.getLocalTranslation().y, -2);
        
        this.updateCounter += Utilities.lockedTPF;
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "enteiFireSpin", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
