package entities.pokemon;

import com.jme3.math.Vector2f;
import engine.Main;
import engine.utility.Utilities;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class Latios extends Pokemon {
    
    private float updateCounter;
    private short state;
    
    public Latios() {
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.scale = (.75f);
        
        this.updateCounter = 0;
        this.state = 0;
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        this.setFacing(true);
        
        switch (this.state) {
            case 0:
                if (this.updateCounter >= .75f) {
                    this.updateCounter -= .75f;
                    this.state++;
                }
                break;
            case 1:
                if (Main.getGameState().getWorld().getStage().getCameraBounds().isInBounds(this.realBounds.getExactCenter())) {
                    Vector2f difference = Main.getGameState().getWorld().getStage().getCameraBounds().getUpperRight()
                            .subtract(this.realBounds.getExactCenter()).normalize().mult(36);
                    this.setVelocity(difference.x, difference.y, true);
                } else {
                    this.setVelocity(0, 0, false);
                    Vector2f lowerLeft = Main.getGameState().getWorld().getStage().getCameraBounds().getLowerLeft();
                    this.setPosition(lowerLeft.x, lowerLeft.y);
                    this.setAttackCapability(6.96f, 1.01f, 99, false);
                    this.state++;
                }
                break;
            case 2:
                if (this.updateCounter >= 5.0f) {
                    this.updateCounter -= 5.0f;
                    this.setDead(true);
                } else {
                    if (Main.getGameState().getWorld().getStage().getCameraBounds().isInBounds(this.realBounds.getExactCenter())) {
                        Vector2f difference = Main.getGameState().getWorld().getStage().getCameraBounds().getUpperRight()
                                    .subtract(this.realBounds.getExactCenter()).normalize().mult(96);
                        this.setVelocity(difference.x, difference.y, true);
                    } else {
                        Vector2f lowerLeft = Main.getGameState().getWorld().getStage().getCameraBounds().getLowerLeft();
                        this.setPosition(lowerLeft.x, lowerLeft.y);
                    }
                }
        }
        
        this.updateCounter += Utilities.lockedTPF;
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "latios", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
