package entities.pokemon;

import engine.Main;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.items.ItemCD;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class Mew extends Pokemon {
    
    private boolean hasSpawnedCD;
    
    public Mew() {
        this.hasSpawnedCD = false;
        
        this.scale = (.5f);
        
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
    }
    
    @Override
    public void updateEntity() {                                      
        super.updateEntity();
        
        this.setPureYVelocity(30);
        
        if (!this.hasSpawnedCD) {
            ItemCD cd = new ItemCD();
            Main.getGameState().spawnEntity(cd);
            cd.setPosition(this.getAppropriateBounds().getLowerMidpoint().x, this.getAppropriateBounds().getLowerMidpoint().y);
            cd.setFacing(this.getFacing());
            this.hasSpawnedCD = true;
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "mew", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
