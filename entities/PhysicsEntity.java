package entities;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import physics.BoundingBox;
import physics.CollisionInfo;
import physics.PhysicsModifier;
import states.game.stages.Stage;

import com.jme3.math.Vector2f;
import com.jme3.scene.Node;

import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;

/**
 * 
 * @author Matthew
 */
public abstract class PhysicsEntity extends AnimatedEntity {
	private static final float SPEEDLIMIT = 150 * 150;

	protected PhysicsInfo physicsInfo;
	protected Vector2f position;
	protected Vector2f velocity;
	protected Set<PhysicsModifier> physicsModifiers;
	protected boolean isDead;
	protected boolean isImmortal;
	protected BoundingBox fullBounds;
	protected BoundingBox realBounds;
	protected BoundingBox disjointBounds;
	protected boolean isOnThinGround;

	public boolean isDisjoint;

	public float gravityModifier;

	protected List<PhysicsEntity> grabbedEntities;
	public Vector2f grabOffset;
	
	protected boolean calculateLedge;

	public PhysicsEntity(String name, float imageSpeed, PhysicsInfo info) {
		super(name, imageSpeed);

		this.physicsInfo = info;
		this.position = new Vector2f();
		this.velocity = new Vector2f();
		this.physicsModifiers = new HashSet<PhysicsModifier>();
		this.isDead = false;
		this.fullBounds = new BoundingBox(this.position, new Vector2f(0, 0));
		this.realBounds = new BoundingBox(this.position, new Vector2f(0, 0));
		this.disjointBounds = new BoundingBox(this.position, new Vector2f(0, 0));
		this.isOnThinGround = false;

		this.isDisjoint = false;

		this.gravityModifier = 1f;

		this.grabbedEntities = new LinkedList<PhysicsEntity>();
		this.grabOffset = new Vector2f(0, 0);
		
		this.calculateLedge = false;
	}

	public void setFacing(boolean b) {
		this.facing = b;
	}
	
	public boolean shouldCalculateLedge(){
		return this.calculateLedge;
	}

	@Override
	public void setUpEntity(int id, Node displayNode, Node guiNode, boolean alwaysDraw) {
		super.setUpEntity(id, displayNode, guiNode, alwaysDraw);
		this.updateBounds();
	}

	public void setOnGround(boolean isOnGround) {
		this.physicsInfo.setOnGround(isOnGround);
		//System.out.println(name + " " + isOnGround);

		if (isOnGround) {
			this.setYVelocity(0, false);
		}
	}

	public boolean isOnGround() {
		return this.physicsInfo.isOnGround();
	}

	public boolean isOnThinGround() {
		return this.isOnThinGround;
	}

	public void addPhysicsModifier(PhysicsModifier modifier) {
		this.physicsModifiers.add(modifier);

		if (modifier.equals(PhysicsModifier.ISSTICKY)) {
			this.grabOffset.zero();
		}
	}

	public void removePhysicsModifier(PhysicsModifier modifier) {
		this.physicsModifiers.remove(modifier);

		if (modifier == PhysicsModifier.ISSTICKY) {
			this.grabbedEntities.clear();
		}
	}

	public boolean hasPhysicsModifier(PhysicsModifier modifier) {
		return this.physicsModifiers.contains(modifier);
	}

	public void setPosition(float newX, float newY) {
		this.position.set(newX, newY);
		this.updateBounds();
	}

	public void addToPosition(float deltaX, float deltaY) {
		this.position.addLocal(deltaX, deltaY);
		this.updateBounds();
	}
	
	public void setPureVelocity(float newX, float newY){
		this.setPureXVelocity(newX);
		this.setPureYVelocity(newY);
	}
	
	public void setPureVelocity(Vector2f newVelocity){
		setPureVelocity(newVelocity.x, newVelocity.y);
	}

	/**
	 * Sets the velocity using the built in setXVelocity and setYVelocity
	 * methods and so it takes the same input and uses them the same way.
	 */
	public void setVelocity(float newX, float newY, boolean jumping) {
		this.setXVelocity(newX);
		this.setYVelocity(newY, jumping);
	}

	/**
	 * Adds to the velocity and multiplies the inputs by their respective
	 * modifiers. If jumping is true, jumpSpeed modifier is used for deltaY,
	 * else fallSpeed modifier is used.
	 */
	public void addToVelocity(float deltaX, float deltaY, boolean jumping) {
		this.setPureXVelocity(this.velocity.getX()
				+ (deltaX * this.physicsInfo.getMoveSpeed()));

		if (jumping) {
			this.setPureYVelocity(this.velocity.getY()
					+ (deltaY * this.physicsInfo.getJumpSpeed()));
		} else {
			this.setPureYVelocity(this.velocity.getY()
					+ (deltaY * this.physicsInfo.getFallSpeed() * this.gravityModifier));
		}
	}

	/**
	 * Sets the x velocity to the input value multiplied by the moveSpeed
	 * modifier.
	 */
	public void setXVelocity(float newX) {
		this.setPureXVelocity(newX * this.physicsInfo.getMoveSpeed());
	}

	public void setPureXVelocity(float newX) {
		this.velocity.setX(newX);
	}

	/**
	 * Sets the y velocity to the input value multiplied by the jumpSpeed
	 * modifier if jumping is true. If jumping is false then the input value is
	 * multiplied by the fallSpeed modifer.
	 */
	public void setYVelocity(float newY, boolean jumping) {
		if (jumping) {
			this.setPureYVelocity(newY * this.physicsInfo.getJumpSpeed());
		} else {
			this.setPureYVelocity(newY * this.physicsInfo.getFallSpeed()
					* this.gravityModifier);
		}
	}

	public void setPureYVelocity(float newY) {
		this.velocity.setY(newY);
	}

	public Vector2f getVelocity() {
		return this.velocity;
	}

	public void setDead(boolean isDead) {
		if (isImmortal)
			isDead = false;
		this.isDead = isDead;

		if (isDead) {
			this.onDeath();
		}
	}

	public void setDead() {
		setDead(true);
	}

	public boolean isDead() {
		return this.isDead;
	}

	public abstract void onDeath();

	public boolean checkForCollision(int x, int y) {
		return this.getSweetSpotPower(x, y) != -1;
	}

	public float getSweetSpotPower(int x, int y) {
		x -= this.fullBounds.getLowerLeft().x /* Used to be a - 1 here */;
		y -= this.fullBounds.getLowerLeft().y /* Used to be a - 1 here */;

		if (x < 0 || y < 0) {
			x += this.fullBounds.getLowerLeft().x;
			y += this.fullBounds.getLowerLeft().y;
		}
		
		//float yDisp = (float) (-this.getDimensions().y*Math.log(this.getScale())/Math.log(2));
		//System.out.println(0.25f*this.getDimensions().y*Math.log(this.getScale())/Math.log(2));
		float yDisp = (float) (0.25f*this.getDimensions().y*Math.log(this.getScale())/Math.log(2));
		float xDisp = (float) (Utilities.getGeneralUtility().getBooleanAsSign(this.facing)*0.0625f*this.getDimensions().x*Math.log(this.getScale())/Math.log(2));
		y += yDisp/1.12f;
		x += xDisp/1.12f;

		if (x < this.getDimensions().x && y < this.getDimensions().y && x >= 0
				&& y >= 0) {
			float returnValue = -1;

			try {
				returnValue = this.facing ? this.currentAction.getFrame()
						.collisionCheck(x, y, this.scale) : this.currentAction
						.getFrame().reversedCollisionCheck(x, y, this.scale);
			} catch (Exception e) {
				Main.log(
						Level.SEVERE,
						"Checked for collision within entity outside of bounds at ("
								+ x + ", " + y + ") with bounds "
								+ this.getDimensions() + "!", e);
			} finally {
				return returnValue;
			}
		} else {
			//System.out.println("hello");
			return -1;
		}
	}

	public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
		this.physicsInfo.setHasCollided(true);

		if (this.physicsModifiers.contains(PhysicsModifier.ISSTICKY)) {
			this.grabbedEntities.add(entity);

			if (entity instanceof PlayableCharacter) {
				((PlayableCharacter) entity).setVectoring(false);
			}

			entity.velocity.zero();
		}
	}

	public boolean hasCollided() {
		return this.physicsInfo.hasCollided();
	}

	public boolean hasCollidedWithObstacle(boolean sideOrSurface) {
		return this.physicsInfo.hasCollidedWithObstacle(sideOrSurface);
	}

	public BoundingBox getFullBounds() {
		return this.fullBounds;
	}

	public BoundingBox getRealBounds() {
		return this.realBounds;
	}

	public BoundingBox getAppropriateBounds() {
		return this.isDisjoint ? this.disjointBounds : this.realBounds;
	}

	public PhysicsInfo getPhysicsInfo() {
		return this.physicsInfo;
	}

	public void collideWithIrregular(IrregularObstacle obstacle) {
		//System.out.println(obstacle.name + obstacle.getPosition().x);

		this.physicsInfo.setHasCollided(false);
		this.physicsInfo.setHasCollidedWithObstacle(false, true);
		this.physicsInfo.setHasCollidedWithObstacle(false, false);

		if (this instanceof IrregularObstacle)
			return;

		boolean shouldHitThinPlatforms = this.getVelocity().y <= 0
				&& !this.physicsModifiers
						.contains(PhysicsModifier.CANGOTHROUGHTHINPLATORMS);

		boolean rightWall, floor, leftWall, ceiling;

		int n = 0;

		if (obstacle.complex2DCollision(this.getAppropriateBounds()
				.getLowerMidpoint(), 1)
				&& this.name.equals("Kirby_DEFAULT")) {
			// System.out.println(this.name);
		}
		

		do {
			Vector2f direction = new Vector2f();
			n++;
			// System.out.println("Loop! " + n);

			rightWall = obstacle.complex2DCollision(this.getAppropriateBounds()
					.getRightMidpoint(), 1)
					|| (shouldHitThinPlatforms && obstacle
							.complex2DCollision(this.getAppropriateBounds()
									.getRightMidpoint(), .5f));
			floor = obstacle.complex2DCollision(this.getAppropriateBounds()
					.getLowerMidpoint(), 1)
					|| (shouldHitThinPlatforms && obstacle
							.complex2DCollision(this.getAppropriateBounds()
									.getLowerMidpoint(), .5f));
			leftWall = obstacle.complex2DCollision(this.getAppropriateBounds()
					.getLeftMidpoint(), 1)
					|| (shouldHitThinPlatforms && obstacle.complex2DCollision(
							this.getAppropriateBounds().getLeftMidpoint(), .5f));
			ceiling = obstacle.complex2DCollision(this.getAppropriateBounds()
					.getUpperMidpoint(), 1)
					|| (shouldHitThinPlatforms && obstacle
							.complex2DCollision(this.getAppropriateBounds()
									.getUpperMidpoint(), .5f));

			if (rightWall) {
				direction.addLocal(-1, 0);
			}

			if (leftWall) {
				direction.addLocal(1, 0);
			}

			if (floor) {
				direction.addLocal(0, 1);
			}

			if (ceiling) {
				direction.addLocal(0, -1);
			}

			if ((rightWall && leftWall) && !(floor || ceiling)) {
				direction.addLocal(0,
						Math.copySign(1, (float) Math.random() - .5f));
			} else if ((floor && ceiling) && !(rightWall || leftWall)) {
				direction.addLocal(
						Math.copySign(1, (float) Math.random() - .5f), 0);
			} else if (floor && ceiling && rightWall && leftWall) {
				direction.addLocal(1, 1);
			}

			if (direction.x != 0) {
				this.physicsInfo.setHasCollidedWithObstacle(true, true);
				this.setPureXVelocity(0);
			}

			if (direction.y != 0) {
				this.physicsInfo.setHasCollidedWithObstacle(true, false);
				//System.out.println("oh!");
				this.setPureYVelocity(0);
			}

			this.addToPosition(direction.x, direction.y);
		} while (rightWall || floor || leftWall || ceiling);

		boolean normalPlatforms = (obstacle.complex2DCollision(this
				.getAppropriateBounds().getLowerLeft().subtract(0, 1), 1)
				|| obstacle.complex2DCollision(this.getAppropriateBounds()
						.getLowerMidpoint().subtract(0, 1), 1) || obstacle
				.complex2DCollision(this.getAppropriateBounds().getLowerRight()
						.subtract(0, 1), 1));

		boolean thinPlatforms = shouldHitThinPlatforms
				&& (obstacle.complex2DCollision(this.getAppropriateBounds()
						.getLowerLeft().subtract(0, 1), .5f)
						|| obstacle.complex2DCollision(this
								.getAppropriateBounds().getLowerMidpoint()
								.subtract(0, 1), .5f) || obstacle
							.complex2DCollision(this.getAppropriateBounds()
									.getLowerRight().subtract(0, 1), .5f));
		this.isOnThinGround = thinPlatforms;

		//System.out.println(normalPlatforms);
		//System.out.println(this.isOnGround());
		if (this.velocity.y <= 0 && (normalPlatforms || thinPlatforms)) {
			//System.out.println("a");
			this.setOnGround(true);
			if (obstacle instanceof MovingPlatform){
				this.addToPosition(((MovingPlatform) obstacle).getDisplacement(), ((MovingPlatform) obstacle).getYDisplacement()*6);
			}
		} else {
			//System.out.println("b");
			this.setOnGround(false);
		}

	}

	public void collideWithStage(Stage stage) {
		this.physicsInfo.setHasCollided(false);
		this.physicsInfo.setHasCollidedWithObstacle(false, true);
		this.physicsInfo.setHasCollidedWithObstacle(false, false);

		boolean shouldHitThinPlatforms = this.getVelocity().y <= 0
				&& !this.physicsModifiers
						.contains(PhysicsModifier.CANGOTHROUGHTHINPLATORMS);

		boolean rightWall, floor, leftWall, ceiling;

		do {
			Vector2f direction = new Vector2f();

			rightWall = stage.complex2DCollision(this.getAppropriateBounds()
					.getRightMidpoint(), 1)
					|| (shouldHitThinPlatforms && stage.complex2DCollision(this
							.getAppropriateBounds().getRightMidpoint(), .5f));
			floor = stage.complex2DCollision(this.getAppropriateBounds()
					.getLowerMidpoint(), 1)
					|| (shouldHitThinPlatforms && stage.complex2DCollision(this
							.getAppropriateBounds().getLowerMidpoint(), .5f));
			leftWall = stage.complex2DCollision(this.getAppropriateBounds()
					.getLeftMidpoint(), 1)
					|| (shouldHitThinPlatforms && stage.complex2DCollision(this
							.getAppropriateBounds().getLeftMidpoint(), .5f));
			ceiling = stage.complex2DCollision(this.getAppropriateBounds()
					.getUpperMidpoint(), 1)
					|| (shouldHitThinPlatforms && stage.complex2DCollision(this
							.getAppropriateBounds().getUpperMidpoint(), .5f));

			if (rightWall) {
				direction.addLocal(-1, 0);
			}

			if (leftWall) {
				direction.addLocal(1, 0);
			}

			if (floor) {
				direction.addLocal(0, 1);
			}

			if (ceiling) {
				direction.addLocal(0, -1);
			}

			if ((rightWall && leftWall) && !(floor || ceiling)) {
				direction.addLocal(0,
						Math.copySign(1, (float) Math.random() - .5f));
			} else if ((floor && ceiling) && !(rightWall || leftWall)) {
				direction.addLocal(
						Math.copySign(1, (float) Math.random() - .5f), 0);
			} else if (floor && ceiling && rightWall && leftWall) {
				direction.addLocal(1, 1);
			}

			if (direction.x != 0) {
				this.physicsInfo.setHasCollidedWithObstacle(true, true);
				this.setPureXVelocity(0);
			}

			if (direction.y != 0) {
				this.physicsInfo.setHasCollidedWithObstacle(true, false);
				this.setPureYVelocity(0);
			}

			this.addToPosition(direction.x, direction.y);
			/*if ((rightWall || floor || leftWall || ceiling)){
				System.out.println("yes");
			}*/
		} while (rightWall || floor || leftWall || ceiling);

		boolean normalPlatforms = (stage.complex2DCollision(this
				.getAppropriateBounds().getLowerLeft().subtract(0, 1), 1)
				|| stage.complex2DCollision(this.getAppropriateBounds()
						.getLowerMidpoint().subtract(0, 1), 1) || stage
				.complex2DCollision(this.getAppropriateBounds().getLowerRight()
						.subtract(0, 1), 1));

		boolean thinPlatforms = shouldHitThinPlatforms
				&& (stage.complex2DCollision(this.getAppropriateBounds()
						.getLowerLeft().subtract(0, 1), .5f)
						|| stage.complex2DCollision(this.getAppropriateBounds()
								.getLowerMidpoint().subtract(0, 1), .5f) || stage
							.complex2DCollision(this.getAppropriateBounds()
									.getLowerRight().subtract(0, 1), .5f));
		this.isOnThinGround = thinPlatforms;

		boolean obstacles = false;

		Obstacle closestObstacle = Main
				.getGameState()
				.getWorld()
				.getClosestObstacle(
						this.getAppropriateBounds().getLowerMidpoint());

		if (closestObstacle != null) {
			if (closestObstacle.getAppropriateBounds().isInBounds(
					this.getAppropriateBounds().getLowerLeft().subtract(0, 1))
					|| closestObstacle.getAppropriateBounds().isInBounds(
							this.getAppropriateBounds().getLowerMidpoint()
									.subtract(0, 1))
					|| closestObstacle.getAppropriateBounds().isInBounds(
							this.getAppropriateBounds().getLowerRight()
									.subtract(0, 1))) {
				obstacles = true;
			}
		}

		//System.out.println(this.isOnGround());
		if (this.velocity.y <= 0
				&& (normalPlatforms || thinPlatforms || obstacles)) {
			this.setOnGround(true);
		} else {
			this.setOnGround(false);
		}
	}

	@Override
	public void updateEntity() {
		super.updateEntity();
		this.updateBounds();
		this.display.setLocalTranslation(
				this.getRealBounds().getExactCenter().x, this.getRealBounds()
						.getExactCenter().y,
				this.display.getLocalTranslation().z);
		
		if (!this.hasPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION)
				&& this.velocity.lengthSquared() > SPEEDLIMIT) {
			this.addToPosition(this.velocity.mult(Utilities.lockedTPF * 5).x,
					this.velocity.mult(Utilities.lockedTPF * 5).y);
			this.collideWithStage(Main.getGameState().getWorld().getStage());
			if (!this.isOnGround())
				for (IrregularObstacle irr : Main.getGameState().getWorld()
					.getStage().getIrregulars()) {
					this.collideWithIrregular(irr);
					if (this.isOnGround())
						break;
				}
			this.addToPosition(this.velocity.mult(Utilities.lockedTPF * 5).x,
					this.velocity.mult(Utilities.lockedTPF * 5).y);
		} else {
			this.addToPosition(this.velocity.mult(Utilities.lockedTPF * 10).x,
					this.velocity.mult(Utilities.lockedTPF * 10).y);
		}

		if (!this.grabbedEntities.isEmpty()) {
			float xOff = Utilities.getGeneralUtility().getBooleanAsSign(
					this.facing)
					* ((this.getRealBounds().getDimensions().x / 2) + grabOffset.x);
			float yOff = (this.getRealBounds().getDimensions().y / 2)
					+ grabOffset.y;

			for (PhysicsEntity entity : this.grabbedEntities) {
				entity.setPosition(this.position.x + xOff, this.position.y
						+ yOff);
			}

			this.display.getChild("Picture").setLocalTranslation(
					this.display.getChild("Picture").getLocalTranslation().x,
					this.display.getChild("Picture").getLocalTranslation().y,
					PictureLayer.SPECIALLAYER.getValue());
		} else {
			this.display.getChild("Picture").setLocalTranslation(
					this.display.getChild("Picture").getLocalTranslation().x,
					this.display.getChild("Picture").getLocalTranslation().y,
					this instanceof PlayableCharacter ? PictureLayer.CHARACTER
							.getValue() : PictureLayer.OTHERENTITY.getValue());
		}
		/*if (this.isOnGround()){
			System.out.println("yes");
		}*/
	}

	public void updateBounds() {
		if (this.entityID != -1) {
			this.fullBounds.updateBounds(
					this.position.subtract(this.getDimensions().x / 2, 0),
					this.getDimensions());
			this.realBounds = (this.facing ? this.currentAction.getFrame()
					.getRealBounds() : this.currentAction.getFrame()
					.getReversedRealBounds()).clone();
			this.disjointBounds = (this.facing ? this.currentAction.getFrame()
					.getDisjointBounds() : this.currentAction.getFrame()
					.getReversedDisjointBounds()).clone();

			Vector2f realOffset = this.position.subtract(this.getDimensions().x
					/ (2 * this.scale), 0);
			this.realBounds.moveBounds(realOffset.x, realOffset.y);
			this.disjointBounds.moveBounds(realOffset.x, realOffset.y);

			this.realBounds.scaleBounds(this.scale);
			this.disjointBounds.scaleBounds(this.scale);
		}
	}

	public void sweetRotateUpTo() {

	}

	public Vector2f getPosition() {
		return position;
	}
}
