package entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import physics.CollisionInfo;
import physics.PhysicsModifier;
import states.game.stages.Stage;

import com.jme3.audio.AudioNode;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.texture.Texture2D;
import com.jme3.texture.image.ImageRaster;
import com.jme3.ui.Picture;

import engine.Angle;
import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import entities.characters.Kirby;
import entities.characters.Kirby.KirbyAbility;
import entities.characters.actions.ActionFallWhenFinished;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.CharacterActionFactory;
import entities.characters.actions.StayingPower;
import entities.characters.controllers.Controller;
import entities.general.RingOut;
import entities.items.Item;

/**
 * A character within SSBS.
 * 
 * @author Matthew Ludwig
 */
public abstract class PlayableCharacter extends ViolentEntity {
	private static final float MAX_SHIELD_DURABILITY = 50.0f;
	private static final float SHIELD_RECOVERY_PER_SECOND = 3.8f;
	private static final float SHIELD_LOSS_PER_SECOND = 7.7f;

	private static Texture2D[] smashFire;

	private boolean fromRandom = false;

	private Map<String, Boolean> controlMap;
	private Map<String, Boolean> refreshMap;
	private List<String> controlsToUnrefresh;
	private float sprintCooldown;

	private float timeOfDeath;

	protected float recoveryForce;
	private float updateLimiter;
	private int numberOfLives;
	private int numberOfInitialLives;
	private boolean isFalling;
	private boolean canJumpAgain;
	private boolean vectoring;
	private boolean jumpDash;
	private boolean finalSmashEnabled;
	private int smashFireFrame;

	private boolean isShieldOut;
	private float shieldDurability;
	private boolean shieldBreakStart;
	private float shieldStun;

	private boolean canDirectionalAirDodge;

	public Item heldItem;
	private Controller controller;
	private int playerNumber;

	private Node marker;
	private Node shield;
	private Node playerInformation;
	private Node respawnPlatform;

	private boolean isRespawned;
	private float respawnFrame;

	private float invincibilityTime;

	private List<Scale> scales;
	private float pitch;
	protected float[] statModifiers;

	private boolean getLedgeInvincibility;
	public float ledgeTime;
	public float ledgeUpdate;
	
	public PlayableCharacter grabbedPlayer;
	public boolean isGrabbing;
	public boolean isGrabbed;
	
	private float[] throwDamages;
	private float[] throwKnockbacks;
	public float grabHitbox;
	public float grabOffset;
	public float directionalGrabOffset;
	public boolean grabIsTether;
	public Vector2f lastWorkingGrabPoint;

	protected PlayableCharacter(String name, PhysicsInfo info, float priority,
			float knockback, float damage, float imageSpeed, float recoveryForce) {
		super(name, imageSpeed, info, priority, knockback, damage);

		if (smashFire == null) {
			smashFire = new Texture2D[18];

			for (int count = 0; count < smashFire.length; count++) {
				smashFire[count] = Utilities.getJMELoader().getTexture(
						"/Interface/Strife/smashFire_"
								+ Utilities.getGeneralUtility()
										.getNumberAsString(count) + ".png");
			}
		}

		this.controlMap = new HashMap<String, Boolean>();
		this.refreshMap = new HashMap<String, Boolean>();
		this.controlsToUnrefresh = new LinkedList<String>();
		this.sprintCooldown = 0;

		this.timeOfDeath = Float.MAX_VALUE;

		this.recoveryForce = recoveryForce;
		this.updateLimiter = 0;
		this.numberOfLives = 3;
		this.numberOfInitialLives = 3;
		this.isFalling = false;
		this.canJumpAgain = true;
		this.vectoring = false;
		this.jumpDash = false;

		this.isShieldOut = false;
		this.shieldDurability = MAX_SHIELD_DURABILITY;
		this.shieldBreakStart = false;
		this.shieldStun = 0f;

		this.canDirectionalAirDodge = true;

		this.heldItem = null;
		this.controller = null;
		this.playerNumber = -1;

		this.marker = new Node("Player Marker");
		this.shield = new Node("Player Shield");
		this.respawnPlatform = new Node("Player Respawn Platform");
		this.playerInformation = new Node("Player Information");
		this.playerInformation.setLocalTranslation(
				this.playerInformation.getLocalTranslation().x,
				this.playerInformation.getLocalTranslation().y, 5);

		this.isRespawned = false;
		this.respawnFrame = 0;

		scales = new ArrayList<Scale>();
		pitch = 1.0f;
		statModifiers = new float[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		ledgeUpdate = 0;
		
		throwDamages = new float[] {5f, 5f, 5f, 5f};
		throwKnockbacks = new float[] {2f, 2f, 2f, 2f};
		grabHitbox = 1;
		grabOffset = 0;
		directionalGrabOffset = 0;
		grabIsTether = false;
		
		this.calculateLedge = true;
	}
	
	public void setThrowPower(float[] dmg, float[] knbk){
		throwDamages = dmg;
		throwKnockbacks = knbk;
	}

	public abstract KirbyAbility getKirbyAbility();

	public final String getDefaultColor() {
		return this.getDisplayName() + "_DEFAULT";
	}

	public float getPitch() {
		return pitch;
	}

	@Override
	public String getDisplayName() {
		return this.name.split("_")[0];
	}

	public final void setColor(int colorNumber) {
		if (colorNumber == 0) {
			this.name += "_DEFAULT";
		} else {
			this.name += "_COLOR" + colorNumber;
		}
	}

	public final int getColorNumber() {
		String colorString = this.name.split("_")[1];

		if (colorString.equals("DEFAULT")) {
			return 0;
		} else {
			return Integer.parseInt(colorString.replace("COLOR", ""));
		}
	}

	public final void setFromRandom() {
		this.fromRandom = true;
	}

	public final boolean isFromRandom() {
		return this.fromRandom;
	}
	
	public void setDead(boolean state){
		super.setDead(state);
		if (this.getNumberOfLives() == 0){
			this.playerInformation.detachAllChildren();
		}
	}

	public final void setPlayerNumber(int newNumber) {
		if (this.playerNumber == -1) {
			this.playerNumber = newNumber;
			this.playerInformation.attachChild(Utilities.getJMELoader()
					.getCenteredText(
							Utilities.Impact32O,
							ColorRGBA.White,
							this.controller.getName().replace("layer", "")
									+ this.playerNumber + ":",
							80 + ((this.playerNumber - 1) * 150), 40));
		} else {
			Main.log(Level.WARNING,
					"You may not set the player number once it has been set!",
					null);
		}
	}
	
	public PlayableCharacter changePlayerNumber(int newNumber){
		setPlayerNumber(newNumber);
		return this;
	}

	public void forceBreakShield(float dmg, float knbk) {
		// System.out.println((this.getDamage()+dmg)*knbk/Utilities.lockedTPF);
		this.forceCurrentAction(CharacterActionFactory.getShieldBreak(
				this,
				(short) (Math.sqrt((this.getDamage() + dmg) * knbk) / Utilities.lockedTPF)));
	}

	public final int getPlayerNumber() {
		return this.playerNumber;
	}

	public void setController(Controller c) {
		this.controller = c;
	}

	public String getControllerName() {
		return this.controller.getName();
	}

	public String getControllerInfo() {
		return this.controller.getAdditionalInfo();
	}

	@Override
	public void setUpEntity(int id, Node displayNode, Node guiNode,
			boolean alwaysDrawn) {
		super.setUpEntity(id, displayNode, guiNode, alwaysDrawn);

		if (getPosition().x > Main.getGameState().getWorld().getStage()
				.getSizeX() / 2)
			facing = false;

		Picture shieldPic;

		if (Main.getStrifeGameRules().isTeamBattleOn()) {
			this.marker
					.attachChild(Utilities
							.getJMELoader()
							.getPicture(
									"Marker",
									"Sprites/Miscellaneous/marker_"
											+ (this.controller.getName()
													.equals("CPU") ? "cpu"
													: this.playerNumber)
											+ "_"
											+ this.team.displayName
													.toLowerCase() + ".png",
									-(this.realBounds.getDimensions().x * .25f),
									this.realBounds.getDimensions().y * .75f,
									PictureLayer.GUI));
			shieldPic = Utilities.getJMELoader().getPicture(
					"Shield Picture",
					"Sprites/Miscellaneous/shield_"
							+ this.team.displayName.toLowerCase() + ".png", 0,
					0, PictureLayer.SPECIALLAYER);
		} else {
			this.marker
					.attachChild(Utilities
							.getJMELoader()
							.getPicture(
									"Marker",
									"Sprites/Miscellaneous/marker_"
											+ (this.controller.getName()
													.equals("CPU") ? "cpu"
													: this.playerNumber
															+ "_"
															+ this.team.displayName
																	.toLowerCase())
											+ ".png",
									-(this.realBounds.getDimensions().x * .25f),
									this.realBounds.getDimensions().y * .75f,
									PictureLayer.GUI));
			shieldPic = Utilities.getJMELoader().getPicture(
					"Shield Picture",
					"Sprites/Miscellaneous/shield_"
							+ (this.controller.getName().equals("CPU") ? "cpu"
									: this.team.displayName.toLowerCase())
							+ ".png", 0, 0, PictureLayer.SPECIALLAYER);
		}

		Material m = shieldPic.getMaterial();
		m.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		m.setColor("Color", new ColorRGBA(1, 1, 1, .50f));
		shieldPic.setMaterial(m);
		this.shield.attachChild(shieldPic);

		this.display.attachChild(this.marker);
		
		guiNode.attachChild(this.playerInformation);
		this.playerInformation.attachChild(Utilities.getJMELoader()
				.getCenteredText(
						Utilities.Impact32O,
						ColorRGBA.White,
						this.controller.getName().replace("layer", "")
								+ this.playerNumber + ":",
						80 + ((this.playerNumber - 1) * 150), 40));
		this.updateText();

		this.refreshMap.put("Up", true);
		this.refreshMap.put("Right", true);
		this.refreshMap.put("Down", true);
		this.refreshMap.put("Left", true);
		this.refreshMap.put("Attack", true);
		this.refreshMap.put("Special", true);
		this.refreshMap.put("Shield", true);
	}

	@Override
	public void cleanUpEntity() {
		super.cleanUpEntity();
		this.updateText();
	}

	@Override
	public void rotateUpTo(Angle angle) {
		super.rotateUpTo(angle);
		this.marker.rotateUpTo(new Vector3f(Utilities.getGeneralUtility()
				.getBooleanAsSign(this.facing) * angle.sin(), angle.cos(), 0));
		// this.marker.getChild("Marker").rotateUpTo(new
		// Vector3f(Utilities.getGeneralUtility().getBooleanAsSign(this.facing)
		// * angle.sin(), angle.cos(), 0));
	}

	@Override
	public void setFinalSmash(boolean state) {
		this.finalSmashEnabled = state;

		if (state) {
			this.display.attachChild(Utilities.getJMELoader().getPicture(
					"Smash Fire", "/Sprites/Miscellaneous/blankSprite.png", 0,
					0, PictureLayer.BACKGROUND));

			float smashFireScale = (Math.min(this.getDimensions().x,
					this.getDimensions().y) / 250) * 3;

			if (this.getDisplayName().equals("Mega Man")) {
				smashFireScale = (this.getDefaultHeight() / 250) * 3;
			}

			this.display.getChild("Smash Fire").setLocalTranslation(
					-50 * smashFireScale, -50 * smashFireScale, 0);
			this.display.getChild("Smash Fire").scale(smashFireScale);
			this.smashFireFrame = 0;
		} else {
			this.display.detachChildNamed("Smash Fire");
		}
	}

	public boolean isFinalSmashEnabled() {
		return this.finalSmashEnabled;
	}

	public void updateText() {
		if (!this.isDead()){
		this.playerInformation.detachChildNamed("Information");
		float r = 1;
		float g = 1;
		float b = 1;
		float d = this.getDamage();
		if (d < 50) {
			r = 1;
			g = (-2.54f * d + 255) / 255f;
			b = (-5.1f * d + 255) / 255f;
		} else if (d < 100) {
			r = (-1.02f * d + 306) / 255f;
			g = (-2.54f * d + 255) / 255f;
			b = 0;
		} else if (d < 300) {
			r = (-0.7f * d + 274) / 255f;
			g = 0;
			b = 0;
		} else {
			r = 64f / 255f;
			g = 0;
			b = 0;
		}
		BitmapText info = Utilities.getJMELoader().getCenteredText(
				Utilities.Impact32O, new ColorRGBA(r, g, b, 1),
				Math.round(this.getDamage()) + "% X" + this.numberOfLives,
				80 + ((this.playerNumber - 1) * 150), 10);
		info.setLocalTranslation(info.getLocalTranslation().x,
				info.getLocalTranslation().y, 0);
		info.setName("Information");
		this.playerInformation.attachChild(info);
		} else {
			//System.out.println("hello" + this.name);
			this.playerInformation.detachChildNamed("Information");
		}
	}

	@Override
	protected final Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();

		actionMap.put("Stand", CharacterActionFactory.getStand(this, 1));
		actionMap.put("Crouch", CharacterActionFactory.getCrouch(this, 1));
		actionMap.put("Walk Right", CharacterActionFactory.getWalk(this, true));
		actionMap.put("Walk Left", CharacterActionFactory.getWalk(this, false));
		actionMap.put("Sprint Right",
				CharacterActionFactory.getSprint(this, true));
		actionMap.put("Sprint Left",
				CharacterActionFactory.getSprint(this, false));
		actionMap.put("Jump 1", CharacterActionFactory.getJumpOne(this, 1));
		actionMap.put("Jump 2", CharacterActionFactory.getJumpTwo(this, 1));
		actionMap
				.put("Falling",
						CharacterActionFactory
								.getAnimation(this, "jump2",
										StayingPower.EXTREME, 1)
								.addModifier(new ActionWaitTillOnGround())
								.addModifier(
										new ActionToggleAttribute(
												ActionToggleAttribute.Attribute.VECTORING)));
		actionMap.put("Neutral A", this.getNeutralA());
		actionMap.put("Side A", this.getSideA());
		actionMap.put("Up A", this.getUpA());
		actionMap.put("Down A", this.getDownA());
		actionMap.put("Neutral B", this.getNeutralB());
		actionMap.put("Side B", this.getSideB());
		actionMap.put(
				"Up B",
				this.getUpB()
						.addModifier(
								new ActionMovement(25f, 90f, 0f, false)
										.setIsRecovery())
						.addModifier(new ActionFallWhenFinished()));
		actionMap.put("Down B", this.getDownB());
		actionMap.put("Final Smash", this.getFinalSmash());

		if (actionMap.get("Final Smash") != null) {
			actionMap.get("Final Smash").addModifier(
					new ActionToggleAttribute(Attribute.SUPERARMOR));
		}

		actionMap.put("Drowning", CharacterActionFactory.getDrowning(this, 1));

		this.overrideDefaults(actionMap);

		return actionMap;
	}

	protected abstract CharacterAction getNeutralA();

	protected abstract CharacterAction getSideA();

	protected abstract CharacterAction getUpA();

	protected abstract CharacterAction getDownA();

	protected abstract CharacterAction getNeutralB();

	protected abstract CharacterAction getSideB();

	protected abstract CharacterAction getUpB();

	protected abstract CharacterAction getDownB();

	protected abstract CharacterAction getFinalSmash();

	protected abstract void overrideDefaults(
			Map<String, CharacterAction> actionMap);

	@Override
	protected final String getDefaultAction() {
		return "Stand";
	}

	public void setYVelocity(float newY, boolean jumping, boolean recovery) {
		if (recovery) {
			this.setPureYVelocity(newY * this.recoveryForce);
		} else {
			super.setYVelocity(newY, jumping);
		}
	}

	@Override
	public boolean addToDamage(float deltaDamage, float attackKnockback,
			float modifier, Angle angleBetweenPlayers, boolean paralyze,
			boolean bury, boolean meteor, boolean freeze, int section, ViolentEntity hitObj) {
		if (this.isInvincible && this.invincibilityTime <= 0) {
			this.shieldDurability -= deltaDamage * Utilities.lockedTPF;
			float knockback = deltaDamage * 18f * .0833f * attackKnockback;
			shieldStun = 0.9f * (knockback + (modifier)) * Utilities.lockedTPF;
		}

		if (this.currentAction.equals(CharacterActionFactory.getShieldBreak(
				this, (short) 1))) {
			this.shieldDurability = MAX_SHIELD_DURABILITY;
			attackKnockback *= 1.2;
		}

		return super.addToDamage(deltaDamage, attackKnockback, modifier,
				angleBetweenPlayers, paralyze, bury, meteor, freeze, section, hitObj);
	}

	@Override
	public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
		if (entity instanceof Item && this.heldItem == null
				&& this.checkControl("Attack", true)) {
			this.heldItem = (Item) entity;
			this.controlsToUnrefresh.remove("Attack");

			if (!this.heldItem.pickUpItem(this)) {
				this.heldItem = null;
				super.onCollideWith(entity, info);
			} else {
				this.refreshMap.put("Attack", false);
			}
		} else {
			if (entity instanceof PlayableCharacter
					&& ((PlayableCharacter) entity).ledgeTime < this.ledgeTime
					&& ((PlayableCharacter) entity).ledgeTime > 0) {
				getTrumped();
			}
			super.onCollideWith(entity, info);
		}
	}

	public void setInvincibility(float seconds) {
		if (seconds > this.invincibilityTime)
			this.invincibilityTime = seconds;
	}
	
	

	public short shieldBreakHitstunMult() {
		if (!shieldBreakStart)
			return 1;
		return 10000;
	}

	@Override
	public void updateEntity() {

		if (this.isOnGround()) {
			this.canDirectionalAirDodge = true;
		}

		for (int i = 0; i < scales.size(); i++) {
			scales.get(i).decrementTime();
			if (scales.get(i).checkTime()) {
				if (i == 0) {
					removeScale();
					i--;
				} else {
					scales.remove(i);
					i--;
				}
			}
		}

		if (this.currentAction.equals(this.actionMap.get("Stand"))) {
			if (this.isOnGround()) {
				this.setXVelocity(0);
			} else {
				this.vectoring = true;
			}
		}

		if (this.isOnGround() && this.shieldBreakStart) {
			this.forceCurrentAction(CharacterActionFactory.getShieldBreak(
					this,
					(short) ((480f - this.getDamage()) / (60f * Utilities.lockedTPF))));
			this.shieldBreakStart = false;
		}

		if (this.isShieldOut) {
			// this.setCurrentAction(CharacterActionFactory.getShield(this, 1));
			float scaleToCharacter = Math.max(this.getAppropriateBounds()
					.getDimensions().x, this.getAppropriateBounds()
					.getDimensions().y) / 50.0f;
			float shieldScale = scaleToCharacter
					* (this.shieldDurability / (MAX_SHIELD_DURABILITY * this
							.getScale()));
			float offset = -16;

			this.shield.setLocalScale(shieldScale);
			this.shield.setLocalTranslation(
					shieldScale
							* (offset - this.getAppropriateBounds()
									.getDimensions().x / 2), shieldScale
							* (offset - this.getAppropriateBounds()
									.getDimensions().y / 2), 10);

			this.shieldDurability -= (Utilities.lockedTPF * SHIELD_LOSS_PER_SECOND);

			if (this.shieldDurability < 0 && !this.shieldBreakStart) {
				this.shieldBreakStart = true;
				this.shieldDurability = 0;
				this.isInvincible = false;
				this.addToDamage(this.getDamageModifier() * 6.8f,
						this.getKnockbackModifier(), 1f, new Angle(90), false,
						false, false, false, 0, this);
			} else if (this.shieldDurability > 0) {
				this.isInvincible = true;
			}
		} else if (this.shieldDurability < MAX_SHIELD_DURABILITY) {
			this.shieldDurability += (Utilities.lockedTPF * SHIELD_RECOVERY_PER_SECOND);
		} else if (this.shieldDurability > MAX_SHIELD_DURABILITY) {
			this.shieldDurability = MAX_SHIELD_DURABILITY;
		}

		if (this.heldItem != null) {
			this.heldItem.setPosition(this.getAppropriateBounds()
					.getExactCenter().x, this.getAppropriateBounds()
					.getExactCenter().y + 20);
		}

		for (String control : this.controlsToUnrefresh) {
			this.refreshMap.put(control, false);
		}

		this.controlsToUnrefresh.clear();

		for (Entry<String, Boolean> e : this.controller.update().entrySet()) {
			this.controlMap.put(e.getKey(), e.getValue());

			if (!e.getValue()) {
				this.refreshMap.put(e.getKey(), true);
			}
		}

		if (this.updateLimiter >= .05f) {
			this.updateLimiter -= .05f;

			if (this.finalSmashEnabled) {
				Utilities.getJMELoader().changeTexture(
						(Picture) this.display.getChild("Smash Fire"),
						smashFire[this.smashFireFrame++]);

				float smashFireScale = (Math.min(this.getDimensions().x,
						this.getDimensions().y) / 250) * 3;

				if (this.getDisplayName().equals("Mega Man")) {
					smashFireScale = (this.getDefaultHeight() / 250) * 3;
				}

				this.display.getChild("Smash Fire").setLocalTranslation(
						-50 * smashFireScale, -50 * smashFireScale, 0);
				this.display.getChild("Smash Fire").scale(smashFireScale);

				Material mat = ((Picture) this.display.getChild("Smash Fire"))
						.getMaterial();
				mat.setColor("Color", new ColorRGBA(1, 1, 1, .5f));
				this.display.getChild("Smash Fire").setMaterial(mat);

				if (this.smashFireFrame >= 18) {
					this.smashFireFrame = 0;
				}
			}

			this.processInput();
		} else {
			this.updateLimiter += Utilities.lockedTPF;
			this.sprintCooldown = Math.max(this.sprintCooldown
					- Utilities.lockedTPF, 0);
		}

		this.updateText();
		super.updateEntity();

		if (this.invincibilityTime > 0) {
			this.isInvincible = true;
			Material mat = ((Picture) this.display.getChild("Picture"))
					.getMaterial();
			mat.setColor("Color", new ColorRGBA(this.tint.x, this.tint.y,
					this.tint.z, 0.75f));
			((Picture) this.display.getChild("Picture")).setMaterial(mat);
			this.invincibilityTime -= Utilities.lockedTPF;
		} else if (this.invincibilityTime <= 0) {
			this.invincibilityTime = 0;
			this.isInvincible = false;
			Material mat = ((Picture) this.display.getChild("Picture"))
					.getMaterial();
			mat.setColor("Color", new ColorRGBA(this.tint.x, this.tint.y,
					this.tint.z, 1f));
			((Picture) this.display.getChild("Picture")).setMaterial(mat);
		}

		if (this.vectoring) {
			float vectoringVelocity;

			if (this.checkControl("Right", false)) {
				vectoringVelocity = this.jumpDash ? 50 : 25;
				vectoringVelocity *= this.physicsInfo.getMoveSpeed();
				this.addToPosition(
						vectoringVelocity * Utilities.lockedTPF * 10, 0);
			} else if (this.checkControl("Left", false)) {
				vectoringVelocity = this.jumpDash ? -50 : -25;
				vectoringVelocity *= this.physicsInfo.getMoveSpeed();
				this.addToPosition(
						vectoringVelocity * Utilities.lockedTPF * 10, 0);
			}
		}

		if (this.isFalling) {
			this.forceCurrentAction(this.actionMap.get("Falling"));
			this.isFalling = false;
		}

		if (this.isRespawned) {
			this.respawnFrame += 2 * Utilities.lockedTPF;
			this.respawnPlatform.detachAllChildren();
			if (this.respawnFrame >= 10
					|| (this.respawnFrame > 0 && this.checkControl("*", true))) {
				this.isRespawned = false;
				this.removePhysicsModifier(PhysicsModifier.NOGRAVITY);
				this.display.detachChild(this.respawnPlatform);
				this.setInvincibility(this.respawnFrame * -0.22f + 4);
				// System.out.println(this.invincibilityTime);
			} else {
				this.respawnPlatform.attachChild(Utilities.getJMELoader()
						.getPicture(
								"Respawn Platform",
								"Sprites/Miscellaneous/recoveryPlatform_00"
										+ (int) (this.respawnFrame) + ".png",
								-36,
								-32 - this.getRealBounds().getDimensions().y
										/ 2, PictureLayer.GUI));
				this.forceCurrentAction(this.actionMap.get("Stand"));
				this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
				this.setVelocity(0, 0, false);
				this.setInvincibility(1);
			}
		}

		if (!isOnGround()) {
			ledgeUpdate += Utilities.lockedTPF;
			if (ledgeUpdate > 0.1) {
				ledgeUpdate = 0;
				Stage stage = Main.getGameState().getWorld().getStage();
				Vector2f pos = getAppropriateBounds().getUpperMidpoint();
				/*
				 * if (facing) pos = getAppropriateBounds().getUpperRight();
				 */
				List<Vector2f> possibleLedges = stage.ledgesWithinDistance(pos,
						17);
				for (Vector2f ledge : possibleLedges) {
					Angle a = new Angle(pos.angleBetween(ledge))
							.subtract(getVelocity().getAngle());
					a.checkAngle();
					boolean direction = stage.getLedgeDirection(ledge);
					if (a.getValue() < 7
							&& !this.currentAction
									.equals(CharacterActionFactory.getHurt(
											this, (short) 0, 0, new Angle(0)))) {
						if (getLedgeInvincibility)
							this.setInvincibility(5);
						// System.out.println(ledge.x - pos.x);
						this.forceCurrentAction(CharacterActionFactory
								.getLedgeHang(this, 1));
						this.facing = direction;
						this.getLedgeInvincibility = false;
						break;
					}
				}
			}
		} else {
			getLedgeInvincibility = true;
		}

		if (this.currentAction.equals(CharacterActionFactory.getLedgeHang(this,
				1))) {
			this.ledgeTime += Utilities.lockedTPF;
		} else {
			this.ledgeTime = 0;
		}
		
		if (this.isDead()){
			//System.out.println("goodbye" + this.name);
			this.playerInformation.detachChildNamed("Information");
		}
	}

	public float getGroundDodge() {
		return (float) ((48.39f * Math.sqrt(Math.sqrt(getPhysicsInfo()
				.getMoveSpeed())) - 26.61f) / 2.1f);
	}

	public float getAirDodge() {
		return (float) ((35.48f * Math.sqrt(Math.sqrt(getPhysicsInfo()
				.getMoveSpeed())) - 19.52f) / 2.1f);
	}

	public short getStationaryDodge() {
		return (short) ((1.61f / Math.sqrt(Math.sqrt(getPhysicsInfo()
				.getMoveSpeed())) - 0.87f) * 0.5f / Utilities.lockedTPF);
	}

	public void getTrumped() {
		this.invincibilityTime = Utilities.lockedTPF;
		this.ledgeTime = 0;
		this.forceCurrentAction(this.actionMap.get("Stand"));
		this.setPosition(getPosition().x - 9
				* Utilities.getGeneralUtility().getBooleanAsSign(facing),
				getPosition().y);
		this.setPureXVelocity(-13
				* Utilities.getGeneralUtility().getBooleanAsSign(facing));
	}

	private void processInput() {
		// System.out.println(this.physicsInfo.isOnGround());
		if (this.isGrabbed){
			this.forceCurrentAction(CharacterActionFactory.getGrabbed(this));
		} else if (this.currentAction.equals(CharacterActionFactory.getLedgeHang(this,
				1))) {
			this.canJumpAgain = true;
			if (this.checkControl("Up", true) && this.ledgeTime > 0.2f) {
				this.invincibilityTime = Utilities.lockedTPF;
				this.forceCurrentAction(this.actionMap.get("Jump 1"));
				this.ledgeTime = 0;
			} else if (this.checkControl("Down", true) && this.ledgeTime > 0.2f) {
				this.invincibilityTime = Utilities.lockedTPF;
				this.ledgeTime = 0;
				this.forceCurrentAction(this.actionMap.get("Stand"));
				this.setPosition(getPosition().x
						- 4.5f
						* Utilities.getGeneralUtility()
								.getBooleanAsSign(facing), getPosition().y - 22);
				this.setPureYVelocity(-11);
				this.setPureXVelocity(-9
						* Utilities.getGeneralUtility()
								.getBooleanAsSign(facing));
			} else if (this.checkControl("Right", true)
					&& this.ledgeTime > 0.2f) {
				this.invincibilityTime = Utilities.lockedTPF;
				this.ledgeTime = 0;
				this.forceCurrentAction(this.actionMap.get("Stand"));
				if (facing) {
					Vector2f newVec = getPosition().add(
							getAppropriateBounds().getDimensions().mult(1.5f));
					this.setPosition(newVec.x, newVec.y);
				} else {
					this.setPosition(getPosition().x + 10, getPosition().y);
					this.setPureXVelocity(15);
				}
			} else if (this.checkControl("Left", true) && this.ledgeTime > 0.2f) {
				this.invincibilityTime = Utilities.lockedTPF;
				this.ledgeTime = 0;
				this.forceCurrentAction(this.actionMap.get("Stand"));
				if (!facing) {
					Vector2f bounds = getAppropriateBounds().getDimensions()
							.mult(1.5f);
					Vector2f newVec = getPosition().subtract(
							new Vector2f(bounds.x, -bounds.y));
					this.setPosition(newVec.x, newVec.y);
				} else {
					this.setPosition(getPosition().x - 10, getPosition().y);
					this.setPureXVelocity(-15);
				}
			} else if (this.checkControl("Attack", true)
					&& this.ledgeTime > 0.2f) {
				this.invincibilityTime = Utilities.lockedTPF;
				this.ledgeTime = 0;
				this.forceCurrentAction(this.actionMap.get("Up A"));
				Vector2f bounds = getAppropriateBounds().getDimensions().mult(
						1.5f);
				Vector2f newVec = getPosition().add(
						new Vector2f(bounds.x
								* Utilities.getGeneralUtility()
										.getBooleanAsSign(facing), bounds.y));
				this.setPosition(newVec.x, newVec.y);
			} else if (this.checkControl("Shield", true)
					&& this.ledgeTime > 0.2f) {
				this.invincibilityTime = Utilities.lockedTPF;
				this.ledgeTime = 0;
				Vector2f bounds = getAppropriateBounds().getDimensions().mult(
						1.5f);
				Vector2f newVec = getPosition().add(
						new Vector2f(bounds.x
								* Utilities.getGeneralUtility()
										.getBooleanAsSign(facing),
								bounds.y * 0.9f));
				this.setPosition(newVec.x, newVec.y);
				Angle ang = new Angle(0);
				if (!facing)
					ang = new Angle(180);
				facing = !facing;
				this.forceCurrentAction(CharacterActionFactory.getDodge(this,
						ang, (short) 0, getGroundDodge()));
			}
		} else if (grabbedPlayer != null) {
			if (this.checkControl("Up", false)) {
				if (facing){
					this.forceCurrentAction(CharacterActionFactory.getThrow(this, new Angle(80), throwDamages[2]*getDamageModifier(), throwKnockbacks[2]*knockbackModifier));
				} else {
					this.forceCurrentAction(CharacterActionFactory.getThrow(this, new Angle(110), throwDamages[2]*getDamageModifier(), throwKnockbacks[2]*knockbackModifier));
				}
			} else if (this.checkControl("Down", false)) {
				if (facing){
					this.forceCurrentAction(CharacterActionFactory.getThrow(this, new Angle(30), throwDamages[3]*getDamageModifier(), throwKnockbacks[3]*knockbackModifier));
				} else {
					this.forceCurrentAction(CharacterActionFactory.getThrow(this, new Angle(150), throwDamages[3]*getDamageModifier(), throwKnockbacks[3]*knockbackModifier));
				}
			} else if (this.checkControl("Right", false)) {
				if (facing){
					this.forceCurrentAction(CharacterActionFactory.getThrow(this, new Angle(10), throwDamages[0]*getDamageModifier(), throwKnockbacks[0]*knockbackModifier));
				} else {
					this.forceCurrentAction(CharacterActionFactory.getThrow(this, new Angle(20), throwDamages[1]*getDamageModifier(), throwKnockbacks[1]*knockbackModifier));
				}
			} else if (this.checkControl("Left", false)) {
				if (facing){
					this.forceCurrentAction(CharacterActionFactory.getThrow(this, new Angle(160), throwDamages[1]*getDamageModifier(), throwKnockbacks[1]*knockbackModifier));
				} else {
					this.forceCurrentAction(CharacterActionFactory.getThrow(this, new Angle(170), throwDamages[0]*getDamageModifier(), throwKnockbacks[0]*knockbackModifier));
				}
			} else {
				this.setCurrentAction(CharacterActionFactory.getHold(this));
			}
		} else {
			if (this.checkControl("Attack", true)) {
				if (this.checkControl("Shield", false)) {
					if (this.heldItem == null) {
						if (this.isOnGround()){
							this.setCurrentAction(CharacterActionFactory.getGrab(this));
						} else if (this.grabIsTether){
							this.setCurrentAction(CharacterActionFactory.getTether(this));
						}
					} else if (this.heldItem.throwItem(this)) {
						this.heldItem.drop(this);
					}
				} else if (this.checkControl("Up", false)) {
					if (this.heldItem == null) {
						this.setCurrentAction(this.actionMap.get("Up A"));
					} else if (this.heldItem.throwItem(this)) {
						this.refreshMap.put("Up", false);
						this.heldItem.throwUp(this, this.physicsInfo.getJumpSpeed());
					}
				} else if (this.checkControl("Right", false)) {
					this.setFacing(true);

					if (this.heldItem == null) {
						this.setCurrentAction(this.actionMap.get("Side A"));
					} else if (this.heldItem.throwItem(this)) {
						this.heldItem.throwForward(this, Utilities.getGeneralUtility()
								.getBooleanAsSign(this.facing)
								* this.physicsInfo.getMoveSpeed());
					}
				} else if (this.checkControl("Down", false)) {
					if (this.heldItem == null) {
						this.setCurrentAction(this.actionMap.get("Down A"));
					} else if (this.heldItem.throwItem(this)) {
						this.refreshMap.put("Down", false);
						this.heldItem.throwDown(this, this.physicsInfo.getJumpSpeed());
					}
				} else if (this.checkControl("Left", false)) {
					this.setFacing(false);

					if (this.heldItem == null) {
						this.setCurrentAction(this.actionMap.get("Side A"));
					} else if (this.heldItem.throwItem(this)) {
						this.heldItem.throwForward(this, Utilities.getGeneralUtility()
								.getBooleanAsSign(this.facing)
								* this.physicsInfo.getMoveSpeed());
					}
				} else {
					if (this.heldItem == null) {
						this.setCurrentAction(this.actionMap.get("Neutral A"));
					} else if (this.heldItem.throwItem(this)) {
						this.heldItem.throwNormal(this, Utilities.getGeneralUtility()
								.getBooleanAsSign(this.facing)
								* this.physicsInfo.getMoveSpeed());
					}
				}
			} else if (this.checkControl("Special", true)) {
				if (this.checkControl("Up", false)) {
					this.setCurrentAction(this.actionMap.get("Up B"));
				} else if (this.checkControl("Right", false)) {
					this.setFacing(true);
					this.setCurrentAction(this.actionMap.get("Side B"));
				} else if (this.checkControl("Down", false)) {
					this.setCurrentAction(this.actionMap.get("Down B"));
				} else if (this.checkControl("Left", false)) {
					this.setFacing(false);
					this.setCurrentAction(this.actionMap.get("Side B"));
				} else {
					if (this.finalSmashEnabled) {
						this.setCurrentAction(this.actionMap.get("Final Smash"));
						this.setFinalSmash(false);
					} else {
						this.setCurrentAction(this.actionMap.get("Neutral B"));
					}
				}
			} else if (this.checkControl("Shield", true)) {
				// System.out.println(this.isOnGround());
				if (this.isOnGround()) {
					if (this.checkControl("Right", false)) {
						this.setCurrentAction(CharacterActionFactory
								.getDodge(this, new Angle(0), (short) 0,
										getGroundDodge()));
						this.facing = false;
						// Roll Right.

					} else if (this.checkControl("Left", false)) {
						this.setCurrentAction(CharacterActionFactory.getDodge(
								this, new Angle(180), (short) 0,
								getGroundDodge()));
						this.facing = true;
						// Roll Left.

					} else if (this.checkControl("Down", false)) {
						this.setCurrentAction(CharacterActionFactory.getDodge(
								this, new Angle(0), getStationaryDodge(), 0));
						// Sidestep

					} else {
						this.setCurrentAction(CharacterActionFactory.getShield(
								this, 1));
						this.isShieldOut = true;
						this.isInvincible = true;
						this.display.attachChild(this.shield);
					}
				} else {
					if (this.canDirectionalAirDodge
							&& (this.checkControl("Right", false)
									|| this.checkControl("Left", false)
									|| this.checkControl("Up", false) || this
										.checkControl("Down", false))) {
						// System.out.println("how?");
						boolean r = this.checkControl("Right", false);
						boolean l = this.checkControl("Left", false);
						boolean u = this.checkControl("Up", false);
						boolean d = this.checkControl("Down", false);
						int vert = 0;
						int hor = 0;

						if (r && l) {
							hor = 0;
						} else if (r) {
							hor = 1;
						} else if (l) {
							hor = -1;
						}

						if (u && d) {
							vert = 0;
						} else if (u) {
							vert = 1;
						} else if (d) {
							vert = -1;
						}

						float ang = 0;
						if (hor == -1) {
							this.facing = true;
							if (vert == -1) {
								ang = 225;
							} else if (vert == 1) {
								ang = 135;
							} else {
								ang = 180;
							}
						} else if (hor == 1) {
							this.facing = false;
							if (vert == -1) {
								ang = -45;
							} else if (vert == 1) {
								ang = 45;
							} else {
								ang = 0;
							}
						} else {
							if (vert == -1) {
								ang = -90;
							} else if (vert == 1) {
								ang = 90;
							}
						}

						this.setCurrentAction(CharacterActionFactory.getDodge(
								this, new Angle(ang), (short) 0, getAirDodge()));

						this.canDirectionalAirDodge = false;
						// Directional Air Dodge
					} else {
						// System.out.println("why?");
						// this.setPureXVelocity(0);
						this.setCurrentAction(CharacterActionFactory.getDodge(
								this, new Angle(0), getStationaryDodge(), 0));
						// Normal Air Dodge
					}
				}
			} else {
				if (this.checkControl("Up", true)) {
					if (this.physicsInfo.isOnGround()) {
						if (this.currentAction.equals(this.actionMap
								.get("Sprint Left"))
								|| this.currentAction.equals(this.actionMap
										.get("Sprint Right"))) {
							this.jumpDash = true;
						}

						this.setCurrentAction(this.actionMap.get("Jump 1"));
					} else if (this.canJumpAgain) {
						if (this.currentAction.equals(this.actionMap
								.get("Sprint Left"))
								|| this.currentAction.equals(this.actionMap
										.get("Sprint Right"))) {
							this.jumpDash = true;
						}

						if (this.setCurrentAction(this.actionMap.get("Jump 2"))) {
							this.canJumpAgain = false;
						}
					}
				} else if (this.checkControl("Down", false)) {
					this.setCurrentAction(this.actionMap.get("Crouch"));
				} else if (this.checkControl("Right", false)) {
					if (this.physicsInfo.isOnGround()) {
						if (this.sprintCooldown == 0) {
							this.setCurrentAction(this.actionMap
									.get("Walk Right"));

							if (this.checkControl("Right", true)) {
								this.sprintCooldown = .66f;
							}
						} else if (this.checkControl("Right", true)) {
							this.setCurrentAction(this.actionMap
									.get("Sprint Right"));
						}
					}
				} else if (this.checkControl("Left", false)) {
					if (this.physicsInfo.isOnGround()) {
						if (this.sprintCooldown == 0) {
							this.setCurrentAction(this.actionMap
									.get("Walk Left"));

							if (this.checkControl("Left", true)) {
								this.sprintCooldown = .66f;
							}
						} else if (this.checkControl("Left", true)) {
							this.setCurrentAction(this.actionMap
									.get("Sprint Left"));
						}
					}
				} else {
					this.setCurrentAction(this.actionMap.get("Stand"));
				}
			}
		}

		if (shieldStun > 0 && this.display.hasChild(this.shield)) {
			shieldStun -= Utilities.lockedTPF;
			// this.setCurrentAction(CharacterActionFactory.getShield(this, 1));
		} else if (shieldStun > 0) {
			shieldStun = 0;
		}

		if (this.display.hasChild(this.shield)
				&& !this.currentAction.equals(CharacterActionFactory.getShield(
						this, 1))) {
			this.isShieldOut = false;
			this.isInvincible = false;
			this.display.detachChild(this.shield);
		}
	}

	public boolean checkShieldStun() {
		return shieldStun <= 0;
	}

	/**
	 * Checks whether the PlayableCharacter has pressed the specified control.
	 * Passing in an asterisk (*) will check if any control has been pressed.
	 * 
	 * @param controlName
	 * @param keyMustBeRefreshed
	 * @return
	 */
	public boolean checkControl(String controlName, boolean keyMustBeRefreshed) {
		boolean result = false;

		if (controlName.equals("*")) {
			for (boolean b : this.controlMap.values()) {
				result = result ? true : b;
			}
		} else {
			result = this.controlMap.containsKey(controlName)
					&& this.controlMap.get(controlName)
					&& (!keyMustBeRefreshed || this.refreshMap.get(controlName));
		}

		if (keyMustBeRefreshed && result) {
			this.controlsToUnrefresh.add(controlName);
		}

		return result;
	}

	@Override
	public void setOnGround(boolean isOnGround) {
		super.setOnGround(isOnGround);

		if (isOnGround) {
			this.canJumpAgain = true;
			this.vectoring = false;
			this.jumpDash = false;

			if (this.currentAction == this.actionMap.get(this
					.getDefaultAction())) {
				this.setXVelocity(0);
			}
		}
	}

	public float getRecoveryForce() {
		return this.recoveryForce;
	}

	public void addNumberOfLives(int newLives) {
		this.numberOfLives += newLives;
	}

	public void setNumberOfLives(int newLives) {
		this.numberOfLives = newLives;
		this.numberOfInitialLives = newLives;
	}

	public int getNumberOfLives() {
		return this.numberOfLives;
	}

	public int getInitialNumberOfLives() {
		return this.numberOfInitialLives;
	}

	@Override
	public void onDeath() {
		super.onDeath();

		RingOut r = new RingOut(team, getControllerName(), this.getVelocity());
		Vector2f pos = this.getPosition().subtract(
				this.getVelocity().mult(0.32f));
		pos = pos.add(new Vector2f((Main.getGameState().getWorld().getStage()
				.getSizeX() / 2f - pos.x) / 3f, (Main.getGameState().getWorld()
				.getStage().getSizeY() / 2f - pos.y) / 3f));
		r.setPosition(pos.x, pos.y);
		Main.getGameState().spawnEntity(r);

		this.numberOfLives--;
		this.isShieldOut = false;
		this.isInvincible = false;

		if (this.numberOfLives > 0) {
			this.setDead(false);
			this.setVelocity(0, 0, false);
			this.forceCurrentAction(this.actionMap.get(this.getDefaultAction()));

			Vector2f spawnPoint = Main
					.getGameState()
					.getWorld()
					.getSpawnPoint(
							false,
							Main.getGameState().getWorld().getPlayerList()
									.indexOf(this));
			this.setPosition(spawnPoint.x, spawnPoint.y);
			this.respawnPlatform.attachChild(Utilities.getJMELoader()
					.getPicture("Respawn Platform",
							"Sprites/Miscellaneous/recoveryPlatform_000.png",
							-36,
							-32 - this.getRealBounds().getDimensions().y / 2,
							PictureLayer.GUI));
			this.isRespawned = true;
			this.display.attachChild(this.respawnPlatform);
			this.respawnFrame = 0;
		} else {
			this.timeOfDeath = System.nanoTime();
		}
	}

	public float getTimeOfDeath() {
		return this.timeOfDeath;
	}

	public void reviveForSuddenDeath() {
		this.numberOfLives = 1;

		this.setDead(false);
		this.setVelocity(0, 0, false);
		this.forceCurrentAction(this.actionMap.get(this.getDefaultAction()));

		Vector2f spawnPoint = Main.getGameState().getWorld()
				.getSpawnPoint(true, 0);
		this.setPosition(spawnPoint.x, spawnPoint.y);
	}

	public void endSprint() {
		this.sprintCooldown = .1f;
	}

	public void setFalling(boolean state) {
		this.isFalling = state;
	}

	public void setVectoring(boolean enabled) {
		this.vectoring = enabled;
	}

	@Override
	protected void updateFrame() {
		super.updateFrame();

		this.marker.setLocalScale(1 / this.getScale());
		this.respawnPlatform.setLocalScale(0.67f / this.getScale());
	}

	public boolean canDieOffTop() {
		if (!canDie)
			return false;
		return (this.currentAction.equals(this.actionMap.get("Stand"))
				|| this.currentAction.equals(this.actionMap.get("Crouch")) || this.currentAction.actionName
					.equals("Hurt"));
	}

	public boolean canDieOffSides() {
		if (!canDie)
			return false;
		return this.getDamageDealt() <= 0;
	}

	public boolean canDieOffBottom() {
		if (!canDie)
			return false;
		return true;
	}

	public String getWinMessage() {
		String playerColorCode = "";

		if (this.controller.getName().equals("CPU")) {
			playerColorCode = "[c:128:128:128]";
		} else {
			switch (this.team) {
			case RED:
				playerColorCode = "[c:255:000:000]";
				break;
			case BLUE:
				playerColorCode = "[c:000:000:255]";
				break;
			case GREEN:
				playerColorCode = "[c:000:255:000]";
				break;
			case YELLOW:
				playerColorCode = "[c:255:255:000]";
				break;
			}
		}

		return this.getDisplayName() + " [c:255:255:255][ " + playerColorCode
				+ this.controller.getName().replace("layer", "")
				+ this.playerNumber + "[c:255:255:255] ]";
	}

	/**
	 * Returns the win message for this character formatted for the team victory
	 * screen state. That means there will be no brackets around the player
	 * type.
	 * 
	 * @return
	 */
	public String getTeamWinMessage() {
		String playerColorCode = "";

		if (this.controller.getName().equals("CPU")) {
			playerColorCode = "[c:128:128:128]";
		} else {
			switch (this.team) {
			case RED:
				playerColorCode = "[c:255:000:000]";
				break;
			case BLUE:
				playerColorCode = "[c:000:000:255]";
				break;
			case GREEN:
				playerColorCode = "[c:000:255:000]";
				break;
			case YELLOW:
				playerColorCode = "[c:255:255:000]";
				break;
			}
		}

		return this.getDisplayName() + " " + playerColorCode
				+ this.controller.getName().replace("layer", "")
				+ this.playerNumber + "[c:255:255:255]";
	}

	public void startDrowning() {
		this.forceCurrentAction(CharacterActionFactory.getDrowning(this, 1));
		this.position = new Vector2f(this.position.x, this.position.y + 5);
		this.canJumpAgain = true;
		this.isFalling = false;
	}

	public boolean getDrowning() {
		return (this.currentAction.equals(this.actionMap.get("Drowning")));
	}
	
	public PlayableCharacter changeScale(Scale s){
		addScale(s);
		return this;
	}

	public void addScale(Scale s) {
		scales.add(0, s);
		if (scales.size() > 1) {
			Scale s2 = scales.get(1);
			/*
			 * if (s2.getType().equals(Scales.TALL) &&
			 * (s.getType().equals(Scales.GIANT) ||
			 * s.getType().equals(Scales.NORMAL))) { s = new Scale(Scales.TALL,
			 * s.time, s.playSound); }
			 */
			if ((s2.getType().equals(Scales.GIANT) && s.getType().equals(
					Scales.TINY))
					|| (s2.getType().equals(Scales.TALL) && s.getType().equals(
							Scales.TINY))) {
				s = new Scale(Scales.NORMAL, s.time, s.playSound, 2);
			}
			if ((s2.getType().equals(Scales.TINY) && s.getType().equals(
					Scales.GIANT))
					|| (s2.getType().equals(Scales.TINY) && s.getType().equals(
							Scales.TALL))) {
				s = new Scale(Scales.NORMAL, s.time, s.playSound, 1);
			}
		}
		s.playBig();
		setScale(s);
	}

	private void setScale(Scale s) {
		PhysicsInfo p = this.physicsInfo;
		p.setWeight(p.getWeight() / statModifiers[0]);
		p.setFallSpeed(p.getFallSpeed() / statModifiers[1]);
		p.setJumpSpeed(p.getJumpSpeed() / statModifiers[2]);
		p.setMoveSpeed(p.getMoveSpeed() / statModifiers[3]);
		this.priorityModifier = this.priorityModifier / statModifiers[4];
		this.knockbackModifier = this.knockbackModifier / statModifiers[5];
		this.setDamageModifier(this.getDamageModifier() / statModifiers[6]);
		this.recoveryForce = this.recoveryForce / statModifiers[8];
		this.setScale(1);
		this.pitch = 1;
		if (s.xScale == s.yScale) {
			this.setScale(s.xScale);
			if (s.xScale == 2) {
				statModifiers = new float[] { 1.5f, 1.5f, 1.2f, 1.2f, 1f, 1.5f,
						1.5f, 1f, 1f };
			} else if (s.xScale == 1) {
				statModifiers = new float[] { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
			} else if (s.xScale == 0.5) {
				statModifiers = new float[] { 0.67f, 0.67f, 0.83f, 0.83f, 1f,
						0.67f, 0.67f, 1f, 1f };
			}
		} else {
			this.setScale(s.xScale, s.yScale);
			if (s.getType().equals(Scales.TALL)) {
				statModifiers = new float[] { 0.67f, 0.9f, 1.1f, 0.9f, 1, 1.5f,
						0.75f, 1, 1 };
			} else {
				statModifiers = new float[] { (s.xScale + 2f) / 3f, 1f,
						(1f / s.yScale + 2f) / 3f, (1f / s.xScale + 2f) / 3f,
						1, (s.yScale + 2f) / 3f, (s.yScale + 2f) / 3f, 1, 1 };
			}
		}
		p.setWeight(p.getWeight() * statModifiers[0]);
		p.setFallSpeed(p.getFallSpeed() * statModifiers[1]);
		p.setJumpSpeed(p.getJumpSpeed() * statModifiers[2]);
		p.setMoveSpeed(p.getMoveSpeed() * statModifiers[3]);
		this.priorityModifier = this.priorityModifier * statModifiers[4];
		this.knockbackModifier = this.knockbackModifier * statModifiers[5];
		this.setDamageModifier(this.getDamageModifier() * statModifiers[6]);
		this.recoveryForce = this.recoveryForce * statModifiers[8];
		this.pitch = s.pitch;
	}

	private void removeScale() {
		scales.get(0).playSmall();
		scales.remove(0);
		if (scales.size() > 0)
			this.setScale(scales.get(0));
		else
			this.setScale(new Scale(Scales.NORMAL, Utilities.lockedTPF, false));
	}

	public Vector2f findGreen() {
		// Vector2f start = getFullBounds().getLowerLeft();
		Vector2f start = new Vector2f(0, 0);
		Vector2f size = getFullBounds().getDimensions();
		for (int i = (int) (start.x); i < start.x + size.x; i++) {
			for (int k = (int) (start.y); k < start.y + size.y; k++) {
				// System.out.println("Green: X=" + i + ", Y=" + k);
				if (checkForGreen(i, k)) {
					// System.out.println("Green: X=" + i + ", Y=" + k);
					return new Vector2f(i, k);
				}
			}
		}
		return null;
	}

	public boolean checkForGreen(float x, float y) {
		ImageRaster raster = ImageRaster.create(((Picture) (this.display
				.getChild("Mapping"))).getMaterial().getTextureParam("Texture")
				.getTextureValue().getImage());
		if (x >= raster.getWidth() || y >= raster.getHeight())
			return false;
		boolean returnValue = Utilities.getGeneralUtility().compareFloats(
				raster.getPixel((int) (x), (int) (y)).getColorArray()[1], 1,
				.02f);
		return returnValue;
	}
	
	public Vector2f findWhite() {
		// Vector2f start = getFullBounds().getLowerLeft();
		Vector2f start = new Vector2f(0, 0);
		Vector2f size = getFullBounds().getDimensions();
		for (int i = (int) (start.x); i < start.x + size.x; i++) {
			for (int k = (int) (start.y); k < start.y + size.y; k++) {
				// System.out.println("Green: X=" + i + ", Y=" + k);
				if (checkForWhite(i, k)) {
					// System.out.println("Green: X=" + i + ", Y=" + k);
					return new Vector2f(i, k);
				}
			}
		}
		return null;
	}

	public boolean checkForWhite(float x, float y) {
		ImageRaster raster = ImageRaster.create(((Picture) (this.display
				.getChild("Mapping"))).getMaterial().getTextureParam("Texture")
				.getTextureValue().getImage());
		if (x >= raster.getWidth() || y >= raster.getHeight())
			return false;
		float[] colArray = raster.getPixel((int) (x), (int) (y)).getColorArray();
		boolean returnValue = Utilities.getGeneralUtility().compareFloats(
				colArray[1] + colArray[2] + colArray[3], 3,
				.02f);
		return returnValue;
	}

	public static class Scale {
		private float xScale;
		private float yScale;
		private float time;
		private float pitch;
		private Scales type;
		private boolean playSound;
		private AudioNode bigSound;
		private AudioNode smallSound;

		public Scale(Scales type, float time, boolean playSound) {
			this(type, time, playSound, 0);
		}

		public Scale(Scales type, float time, boolean playSound, int normalDir) {
			this.playSound = playSound;
			this.time = time;
			this.type = type;
			if (type.equals(Scales.GIANT)) {
				this.xScale = 2f;
				this.yScale = 2f;
				this.pitch = 0.75f;
				bigSound = Utilities.getCustomLoader().getAudioNode(
						"/Common Sounds/mushroomUp.ogg");
				smallSound = Utilities.getCustomLoader().getAudioNode(
						"/Common Sounds/mushroomDown.ogg");
			} else if (type.equals(Scales.TINY)) {
				this.xScale = 0.5f;
				this.yScale = 0.5f;
				this.pitch = 1.5f;
				smallSound = Utilities.getCustomLoader().getAudioNode(
						"/Common Sounds/mushroomUp.ogg");
				bigSound = Utilities.getCustomLoader().getAudioNode(
						"/Common Sounds/mushroomDown.ogg");
			} else if (type.equals(Scales.TALL)) {
				this.xScale = 0.75f;
				this.yScale = 1.5f;
				this.pitch = 0.9f;
				bigSound = Utilities.getCustomLoader().getAudioNode(
						"/Common Sounds/mushroomUp.ogg");
				smallSound = Utilities.getCustomLoader().getAudioNode(
						"/Common Sounds/mushroomDown.ogg");
			} else if (type.equals(Scales.WARPED)) {
				this.xScale = (float) Math.pow(2, Math.random() * 2.2 - 1.1);
				this.yScale = (float) Math.pow(2, Math.random() * 2.2 - 1.1);
				this.pitch = (2f / (xScale + yScale) + 1f) / 2f;
				if (xScale < 1 && yScale < 1) {
					smallSound = Utilities.getCustomLoader().getAudioNode(
							"/Common Sounds/mushroomUp.ogg");
					bigSound = Utilities.getCustomLoader().getAudioNode(
							"/Common Sounds/mushroomDown.ogg");
				} else {
					bigSound = Utilities.getCustomLoader().getAudioNode(
							"/Common Sounds/mushroomUp.ogg");
					smallSound = Utilities.getCustomLoader().getAudioNode(
							"/Common Sounds/mushroomDown.ogg");
				}
			} else if (type.equals(Scales.NORMAL)) {
				this.xScale = 1f;
				this.yScale = 1f;
				this.pitch = 1f;
				if (normalDir == 1) {
					bigSound = Utilities.getCustomLoader().getAudioNode(
							"/Common Sounds/mushroomUp.ogg");
					smallSound = Utilities.getCustomLoader().getAudioNode(
							"/Common Sounds/mushroomDown.ogg");
				} else if (normalDir == 2) {
					bigSound = Utilities.getCustomLoader().getAudioNode(
							"/Common Sounds/mushroomDown.ogg");
					smallSound = Utilities.getCustomLoader().getAudioNode(
							"/Common Sounds/mushroomUp.ogg");
				}
			}

			if (bigSound != null) {
				this.bigSound.setVolume(Main.getGameSettings().getSFXVolume());
				this.smallSound
						.setVolume(Main.getGameSettings().getSFXVolume());
			}
		}

		public float getScaleX() {
			return xScale;
		}

		public float getScaleY() {
			return yScale;
		}

		public float getPitch() {
			return pitch;
		}

		public Scales getType() {
			return type;
		}

		public void decrementTime() {
			time -= Utilities.lockedTPF;
			if (time > Integer.MAX_VALUE / 2f) {
				time = Integer.MAX_VALUE / 2f;
			}
		}

		public boolean checkTime() {
			// System.out.println(time);
			return (time <= 0);
		}

		public void playBig() {
			if (bigSound != null && playSound)
				bigSound.playInstance();
		}

		public void playSmall() {
			if (smallSound != null && playSound)
				smallSound.playInstance();
		}
	}

	public static enum Scales {
		GIANT, TINY, TALL, WARPED, NORMAL;
	}
}
