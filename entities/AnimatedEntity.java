package entities;

import java.util.Map;

import physics.BoundingBox;

import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.ui.Picture;

import engine.Angle;
import engine.PictureLayer;
import engine.utility.Utilities;
import entities.characters.actions.ActionOverrideFalling;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.CharacterActionFactory;

/**
 * 
 * @author Matthew
 */
public abstract class AnimatedEntity {
	protected String name;
	protected float imageSpeed;
	protected int entityID = -1;
	protected Node display;
	protected float scale;
	protected float xScale;
	protected Vector2f dimensions;
	protected boolean facing;
	protected Map<String, CharacterAction> actionMap;
	protected CharacterAction currentAction;

	public boolean alwaysDraw;

	public Vector3f tint;
	public boolean randomTint;
	public float alpha;

	public AnimatedEntity(String name, float imageSpeed) {
		this.name = name;
		this.imageSpeed = imageSpeed;
		this.scale = 1f;
		this.xScale = 1f;
		this.dimensions = new Vector2f();
		this.facing = true;

		this.tint = new Vector3f(1, 1, 1);
		this.randomTint = false;
		this.alwaysDraw = false;
		this.alpha = 1;
	}

	public void setUpEntity(int id, Node displayNode, Node guiNode,
			boolean alwaysDraw) {
		this.entityID = id;

		this.display = new Node("Entitiy #" + this.entityID);

		if (this instanceof PlayableCharacter) {
			this.display.attachChild(Utilities.getJMELoader().getPicture(
					"Picture", "/Sprites/Miscellaneous/blankSprite.png", 0, 0,
					PictureLayer.CHARACTER));
			this.display.attachChild(Utilities.getJMELoader().getPicture(
					"Mapping", "/Sprites/Miscellaneous/blankSprite.png", 0, 0,
					PictureLayer.CHARACTER));
		} else {
			this.display.attachChild(Utilities.getJMELoader().getPicture(
					"Picture", "/Sprites/Miscellaneous/blankSprite.png", 0, 0,
					PictureLayer.OTHERENTITY));
			this.display.attachChild(Utilities.getJMELoader().getPicture(
					"Mapping", "/Sprites/Miscellaneous/blankSprite.png", 0, 0,
					PictureLayer.OTHERENTITY));
		}

		if (alwaysDraw) {
			this.display.attachChild(new Node("AlwaysDraw"));
		}

		Material mat = ((Picture) this.display.getChild("Mapping"))
				.getMaterial();
		mat.setColor("Color", new ColorRGBA(1, 1, 1, 0f));
		((Picture) this.display.getChild("Mapping")).setMaterial(mat);

		displayNode.attachChild(this.display);
		this.actionMap = this.getActionMap();

		this.currentAction = this.actionMap.get(this.getDefaultAction());
		this.updateFrame();
	}

	public Node getDisplay() {
		return this.display;
	}
	
	public Map<String, CharacterAction> seeActionMap(){
		return actionMap;
	}

	public void detachAllDisplayChildren() {
		Picture temp = (Picture) this.display.getChild("Picture");
		Picture temp2 = (Picture) this.display.getChild("Mapping");
		Spatial temp3 = this.display.getChild("AlwaysDraw");
		this.display.detachAllChildren();
		this.display.attachChild(temp);
		this.display.attachChild(temp2);
		if (temp3 != null) {
			this.display.attachChild(temp3);
		}
	}

	public void cleanUpEntity() {
		if (this.display != null && this.display.getParent() != null)
		this.display.getParent().detachChild(this.display);
	}

	protected abstract Map<String, CharacterAction> getActionMap();

	protected abstract String getDefaultAction();

	public CharacterAction getCurrentAction() {
		return currentAction;
	}

	public String getName() {
		return this.name;
	}

	public String getDisplayName() {
		return this.name;
	}

	public float getImageSpeed() {
		return this.imageSpeed;
	}

	public void setScale(float x, float y) {
		this.scale = y;
		this.xScale = x / y;
		this.updateFrame();
	}

	public void setScale(float newScale) {
		this.scale = newScale;
		this.xScale = 1;
		this.updateFrame();
	}

	public float getScale() {
		return this.scale;
	}

	public float getXScale() {
		return this.xScale;
	}

	public Vector2f getDimensions() {
		return this.dimensions;
	}

	public void setFacing(boolean newFacing) {
		this.facing = newFacing;
		this.updateFrame();
	}

	public boolean getFacing() {
		return this.facing;
	}

	public float getDefaultHeight() {
		return this.actionMap.get(this.getDefaultAction()).animations[0][0]
				.getRealBounds().getDimensions().y * this.scale;
	}

	public void updateEntity() { //problem is somewhere in here? possibly updateFrame?
		if (this.currentAction.update()) {
			if (this.currentAction.finish(false)) {
				this.currentAction = this.actionMap
						.get(this.getDefaultAction());
				this.updateFrame();
			}
		}

		if (this.randomTint) {
			// Random tint seems to cause a little bit of lag. And by little bit
			// I mean quite a lot at random intervals.
			this.tint.addLocal(Utilities.getGeneralUtility()
					.generateRandomVector().mult(Utilities.lockedTPF * 10));
		}
		
		Material mat = ((Picture) this.display.getChild("Picture"))
				.getMaterial();
		mat.setColor("Color", new ColorRGBA(this.tint.x, this.tint.y,
				this.tint.z, alpha));
		((Picture) this.display.getChild("Picture")).setMaterial(mat);

		Material mat2 = ((Picture) this.display.getChild("Mapping"))
				.getMaterial();
		mat2.setColor("Color", new ColorRGBA(this.tint.x, this.tint.y,
				this.tint.z, 0));
		((Picture) this.display.getChild("Mapping")).setMaterial(mat2);
		
		if (this.currentAction.hasFrameChanged()) {
			this.updateFrame();
		}
	}

	protected final boolean setCurrentAction(CharacterAction newAction) {
		if (this.currentAction.equals(this.actionMap.get("Falling"))) {
			ActionOverrideFalling f = null;
			if (newAction != null)
				f = newAction.doesOverrideFalling();

			if (f != null) {
				this.forceCurrentAction(newAction);
				f.wasFalling = true;
				return true;
			}
		}

		if (newAction != null
				&& this.currentAction.getStayingPower() < newAction
						.getStayingPower()
				&& !(this instanceof PlayableCharacter
						&& this.currentAction.equals(CharacterActionFactory
								.getLedgeHang((PlayableCharacter) this, 1)) && newAction
							.equals(actionMap.get("Falling")))) {
			this.forceCurrentAction(newAction);
			return true;
		} else {
			return false;
		}
	}

	protected final void forceCurrentAction(CharacterAction newAction) {
		if (!(this instanceof PlayableCharacter
				&& this.currentAction.equals(CharacterActionFactory
						.getLedgeHang((PlayableCharacter) this, 1)) && newAction
					.equals(actionMap.get("Falling")))) {
			this.currentAction.finish(true);
			this.currentAction = newAction;
			this.currentAction.init();
			this.updateFrame();
		}
	}

	protected void updateFrame() {
		Utilities.getJMELoader().changeTexture(
				((Picture) this.display.getChild("Picture")),
				this.facing ? this.currentAction.getFrame().getImage()
						: this.currentAction.getFrame().getReversedImage());
		Utilities.getJMELoader().changeTexture(
				((Picture) this.display.getChild("Mapping")),
				this.facing ? this.currentAction.getFrame().getCollisionMap()
						: this.currentAction.getFrame()
								.getReversedCollisionMap());
		// this.display.setLocalScale(this.scale);
		this.display.setLocalScale(this.xScale * this.scale, this.scale, 1);
		this.dimensions.set(this.scale
				* this.currentAction.getFrame().getWidth(), this.scale
				* this.currentAction.getFrame().getHeight());
		BoundingBox bounds = (this.facing ? this.currentAction.getFrame()
				.getRealBounds() : this.currentAction.getFrame()
				.getReversedRealBounds()).clone();
		bounds.scaleBounds(this.scale);
		this.display.getChild("Picture").setLocalTranslation(
				-bounds.getExactCenter().x, -bounds.getExactCenter().y, 0);
		this.display.getChild("Mapping").setLocalTranslation(
				-bounds.getExactCenter().x, -bounds.getExactCenter().y, 0);
	}

	public void rotateUpTo(Angle angle) {
		this.display.rotateUpTo(new Vector3f(Utilities.getGeneralUtility()
				.getBooleanAsSign(!this.facing) * angle.sin(), angle.cos(), 0));
	}

	@Override
	public boolean equals(Object o) {
		if (o != null && o instanceof AnimatedEntity) {
			return this == o || this.entityID == ((AnimatedEntity) o).entityID;
		} else {
			return false;
		}
	}
}
