package entities;

import physics.BoundingBox;
import physics.CollisionInfo;
import physics.CollisionInfo.CollisionDirection;
import physics.PhysicsModifier;

import com.jme3.math.Vector2f;
import com.jme3.texture.Image;
import com.jme3.texture.image.ImageRaster;
import com.jme3.ui.Picture;

import engine.Angle;
import engine.utility.Utilities;

public abstract class IrregularObstacle extends PhysicsEntity {
	
	private Angle rotAngle;
	
	public IrregularObstacle(String name, PhysicsInfo info, Angle startAng) {
		super(name, 1, info);
		rotAngle = new Angle(-startAng.getValue());
		this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
	}
	
	/*public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
        super.onCollideWith(entity, info);
        //entity.collideWithObstacle(this);
    }*/
	public void onDeath(){
		//this.cleanUpEntity();
	}
	
	public void setRotAngle(Angle n){
		rotAngle = new Angle(-n.getValue());
	}
	
	public boolean complex2DCollision(BoundingBox b, float value){
    	for (float x = b.getLowerLeft().x; x < b.getUpperRight().x; x++){
    		for (float y = b.getLowerLeft().y; y < b.getUpperRight().y; y++){
        		boolean n = complex2DCollision((int)x, (int)y, value);
        		if (n == true)
        			return true;
        	}
    	}
    	return false;
    }

    /**
     * Checks whether red value at the point is a precision of .02 to the required value.
     * All values are from 0 to 1.
     */
    public boolean complex2DCollision(Vector2f point, float value) {
        return complex2DCollision((int) point.x, (int) point.y, value);
    }
    
    /**
     * Checks whether red value at the point is a precision of .02 to the required value.
     * All values are from 0 to 1.
     */
    private final boolean complex2DCollision(int x, int y, float value) {
        boolean returnValue = false;
        
        Vector2f v = new Vector2f(x, y).subtract(this.getFullBounds().getExactCenter());
        float[][] rotMat = {{rotAngle.cos(), -rotAngle.sin()}, {rotAngle.sin(), rotAngle.cos()}};
        float temp1 = rotMat[0][0]*v.x + rotMat[0][1]*v.y;
        float temp2 = rotMat[1][0]*v.x + rotMat[1][1]*v.y;
        v = new Vector2f(temp1, temp2).add(this.getFullBounds().getExactCenter());
        v = v.subtract(this.getFullBounds().getLowerLeft());

        try {
        	ImageRaster raster = ImageRaster.create(((Picture)(this.display.getChild("Mapping"))).getMaterial().getTextureParam("Texture").getTextureValue().getImage());
            returnValue = Utilities.getGeneralUtility().compareFloats(raster.getPixel((int)(v.x), (int)(v.y)).getColorArray()[0], value, .02f);
        } catch (IllegalArgumentException e) {
            
        }

        return returnValue;
    }
    
    public boolean checkForLedge(Vector2f point) {
        return checkForLedge((int) point.x, (int) point.y);
    }
    
    private final boolean checkForLedge(int x, int y){
    	boolean returnValue = false;
    	
    	Vector2f v = new Vector2f(x, y).subtract(this.getFullBounds().getExactCenter());
        float[][] rotMat = {{rotAngle.cos(), -rotAngle.sin()}, {rotAngle.sin(), rotAngle.cos()}};
        float temp1 = rotMat[0][0]*v.x + rotMat[0][1]*v.y;
        float temp2 = rotMat[1][0]*v.x + rotMat[1][1]*v.y;
        v = new Vector2f(temp1, temp2).add(this.getFullBounds().getExactCenter());
        v = v.subtract(this.getFullBounds().getLowerLeft());

        try {
        	ImageRaster raster = ImageRaster.create(((Picture)(this.display.getChild("Mapping"))).getMaterial().getTextureParam("Texture").getTextureValue().getImage());
        	float[] arr = raster.getPixel((int)(v.x), (int)(v.y)).getColorArray();
            returnValue = Utilities.getGeneralUtility().compareFloats(arr[0] + arr[1] + arr[2], 3, .02f);
        } catch (IllegalArgumentException e) {
            
        }

        return returnValue;
    }

}
