package entities;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.utility.Utilities;
import entities.characters.actions.ActionReflect.ReflectType;
import entities.characters.actions.CharacterActionFactory;
import entities.items.Item;
import entities.projectiles.Projectile;
import physics.CollisionInfo;

/**
 * 
 * @author Matthew
 */
public abstract class ViolentEntity extends PhysicsEntity {
	private float damage;
	private float damageCooldown;
	protected boolean isInvincible;
	private boolean hasSuperArmor;
	private boolean hasHeavyArmor;
	private boolean willParalyze;
	private boolean willBury;
	private boolean willMeteor;
	private boolean willFreeze;
	private float damageDealt;
	private float attackKnockback;
	private float priority;
	private boolean isEnergyAttack;
	protected float priorityModifier;
	protected float knockbackModifier;
	private float damageModifier;
	private boolean canReflect;
	private float reflectCooldown;
	private boolean canOnlyHitPlayers;
	private boolean isCountering;
	protected boolean canDie;
	private boolean currentlyParalyzed;
	private boolean currentlyBuried;
	private boolean currentlyFrozen;

	private boolean collisionWasSuccessful;
	private float mostRecentPower;

	protected float reflectPower;
	protected boolean flipsPlayers;
	protected ReflectType reflectType;

	private int mostRecentSection;
	private ViolentEntity mostRecentObject;
	protected short[] sectionFrames;
	private int attackSection;

	private boolean destroyProjectiles;

	public Team team;

	public ViolentEntity(String name, float imageSpeed, PhysicsInfo info,
			float priorityModifier, float knockbackModifier,
			float damageModifier) {
		super(name, imageSpeed, info);

		this.damage = 0;
		this.damageCooldown = 0;
		this.hasSuperArmor = false;
		this.hasHeavyArmor = false;
		this.willParalyze = false;
		this.currentlyParalyzed = false;
		this.willBury = false;
		this.willFreeze = false;
		this.currentlyBuried = false;
		this.currentlyFrozen = false;

		this.isInvincible = false;

		this.damageDealt = 0;
		this.attackKnockback = 0;
		this.priority = 0;
		this.priorityModifier = priorityModifier;
		this.knockbackModifier = knockbackModifier;
		this.setDamageModifier(damageModifier);

		this.setCantReflect();
		this.reflectCooldown = 0;
		this.canOnlyHitPlayers = false;

		this.isCountering = false;

		this.collisionWasSuccessful = false;
		this.mostRecentPower = 0.0f;
		this.canDie = true;

		mostRecentSection = -1;
	}

	public boolean isOnSameTeamAs(ViolentEntity entity) {
		return this.team.equals(Team.NONE) ? false : this.team
				.equals(((ViolentEntity) entity).team);
	}
	
	public boolean isInvincible(){
		return isInvincible;
	}

	public boolean canDieOffTop() {
		return canDie;
	}

	public boolean canDieOffSides() {
		return canDie;
	}

	public boolean canDieOffBottom() {
		return canDie;
	}

	public void setMortality(boolean b) {
		canDie = b;
	}

	public void setCurrentlyParalyzed(boolean b) {
		this.currentlyParalyzed = b;
	}

	public void setCurrentlyBuried(boolean b) {
		this.currentlyBuried = b;
	}
	
	public void setCurrentlyFrozen(boolean b){
		this.currentlyFrozen = b;
	}

	public boolean isCurrentlyBuried() {
		return this.currentlyBuried;
	}
	
	public boolean isCurrentlyFrozen() {
		return this.currentlyFrozen;
	}

	public abstract void setFinalSmash(boolean state);

	public void healEntity(float amountToHealBy) {
		this.damage = Math.max(this.damage - amountToHealBy, 0);
	}

	public boolean frameIsSection(int frame) {
		for (int i = 0; i < sectionFrames.length; i++) {
			if (frame == sectionFrames[i]) {
				return true;
			}
		}
		return false;
	}

	public boolean addToDamage(float deltaDamage, float attackKnockback,
			float modifier, Angle angleBetweenPlayers, boolean paralyzed,
			boolean buried, boolean meteored, boolean frozen, int section, ViolentEntity hitObj) {
		boolean returnValue = false;

		if (this.damageCooldown <= 0) {
			mostRecentObject = null;
			mostRecentSection = -1;
			this.damageCooldown = 0;
			// System.out.println(mostRecentSection);
		}

		if (this.damageCooldown <= 0.45f
				&& (hitObj != mostRecentObject || section != mostRecentSection)) {
			// System.out.println(section);
			mostRecentObject = hitObj;
			mostRecentSection = section;
			this.damageCooldown = 0;
		}

		if (this.damageCooldown <= 0 && !this.isInvincible) {
			this.damage = Math.min(this.damage + deltaDamage, 999);
			float pieceOne = (float) (Math.pow(this.damage, 1.43)
					* (this.physicsInfo.getWeight() + 1 + (1.4f * Math.pow(deltaDamage, 0.7))));
			float pieceTwo = 10f * (this.physicsInfo.getWeight() + 1);

			float knockback = (float) (((Math.pow(pieceOne, 0.75) / pieceTwo) + 18f) * .0833f
					* Math.pow(attackKnockback, 1.33));

			if (this.hasHeavyArmor) {
				knockback -= 1.2f;
				if (knockback < 0) {
					knockback = 0.0f;
				}
			}
			
			short mult = 1;
			if (this instanceof PlayableCharacter)
				mult = ((PlayableCharacter)this).shieldBreakHitstunMult();
			//System.out.println(mult);

			if (knockback != 0 && (!this.hasSuperArmor || (hitObj.isEnergyAttack && currentlyFrozen))) {
				float knockbackSpeed = 3.2f * (knockback + (modifier));

				if (deltaDamage != 0 && !paralyzed && !buried && (!frozen || Math.random()*100 > this.damage)) {
					if (!meteored) {
						this.forceCurrentAction(CharacterActionFactory.getHurt(
								this, (short) (Math.sqrt(knockbackSpeed)*6*mult), knockbackSpeed,
								angleBetweenPlayers));
					} else {
						this.forceCurrentAction(CharacterActionFactory.getHurt(
								this, (short) (Math.sqrt(knockbackSpeed)*6),
								knockbackSpeed * 2, new Angle(270)));
					}
					this.currentlyParalyzed = false;
					this.currentlyBuried = false;
					this.currentlyFrozen = false;
				} else if (deltaDamage != 0 && paralyzed) {
					if (!this.currentlyParalyzed) {
						this.forceCurrentAction(CharacterActionFactory
								.getParalyzed(this,
										(short) (knockbackSpeed * 32)));
						this.currentlyParalyzed = true;
					}
				} else if (deltaDamage != 0 && buried) {
					if (!this.currentlyBuried) {
						this.forceCurrentAction(CharacterActionFactory.getBuried(
								this,
								(short) ((Math.sqrt(knockbackSpeed) + knockbackSpeed) * 20),
								knockbackSpeed));
					}
				} else if (deltaDamage != 0 && frozen) {
					if (!this.currentlyFrozen) {
						this.forceCurrentAction(CharacterActionFactory.getFrozen(
								this, (short) (Math.sqrt(knockbackSpeed)*30*mult), knockbackSpeed,
								angleBetweenPlayers));
					}
				} else {
					Angle finalAngle = Utilities
							.getPhysicsUtility()
							.calculateCorrectAngle(
									new Vector2f(
											(angleBetweenPlayers.cos() * 2) / 3,
											((angleBetweenPlayers.sin() * 2) + 1) / 3),
									Vector2f.ZERO);
					float xVelocity = (float) (knockbackSpeed * finalAngle
							.cos());
					float yVelocity = (float) (knockbackSpeed * finalAngle
							.sin());
					this.addToPosition(xVelocity, yVelocity);
				}
			}

			this.damageCooldown = 0.6f;
			returnValue = true;
		}

		return returnValue;
	}

	public boolean wasCollisionSuccesful() {
		return this.collisionWasSuccessful;
	}

	public float getDamageModifier() {
		return this.damageModifier;
	}

	public float getKnockbackModifier() {
		return this.knockbackModifier;
	}

	public float getDamage() {
		return this.damage;
	}

	public void setHasSuperArmor(boolean hasSuperArmor) {
		this.hasSuperArmor = hasSuperArmor;
	}

	public boolean hasSuperArmor() {
		return this.hasSuperArmor;
	}

	public void setHasHeavyArmor(boolean hasHeavyArmor) {
		this.hasHeavyArmor = hasHeavyArmor;
	}

	public boolean hasHeavyArmor() {
		return this.hasHeavyArmor;
	}

	public void setCountering(boolean isCountering) {
		this.isCountering = isCountering;
	}

	public boolean isCountering() {
		return this.isCountering;
	}

	public void setAttackCapability(float damageDealt, float knockback,
			float priority, boolean isEnergy, short[] sectionFrames) {
		this.damageDealt = damageDealt * this.getDamageModifier();
		this.attackKnockback = knockback;
		this.priority = priority * this.priorityModifier;
		this.isEnergyAttack = isEnergy;
		this.sectionFrames = sectionFrames;
	}

	public void setAttackCapability(float damageDealt, float knockback,
			float priority, boolean isEnergy) {
		setAttackCapability(damageDealt, knockback, priority, isEnergy,
				new short[] { 0 });
	}

	public float getDamageDealt() {
		return this.damageDealt;
	}

	public float getKnockback() {
		return this.attackKnockback;
	}

	public float getPriority() {
		return this.priority;
	}

	public void setCanReflect(float strength, boolean flips, ReflectType type) {
		this.canReflect = true;
		this.reflectPower = strength;
		this.flipsPlayers = flips;
		this.reflectType = type;
	}

	public void setCantReflect() {
		this.canReflect = false;
		this.reflectPower = 1;
		this.flipsPlayers = false;
		this.reflectType = ReflectType.BOTH;

	}

	public boolean canReflect() {
		return this.canReflect;
	}

	public void setCanOnlyHitPlayers(boolean state) {
		this.canOnlyHitPlayers = state;
	}

	public void setParalyze(boolean state) {
		this.willParalyze = state;
	}

	public boolean getParalyze() {
		return this.willParalyze;
	}

	public void setBury(boolean state) {
		this.willBury = state;
	}
	
	public void setFreeze(boolean state){
		this.willFreeze = state;
	}

	public boolean getBury() {
		return this.willBury;
	}
	
	public boolean getFreeze(){
		return this.willFreeze;
	}

	public void multiplyByReflect(ViolentEntity reflecter) {
		this.physicsInfo.multiplyMoveSpeed(reflecter.reflectPower);
		this.setDamageModifier(this.getDamageModifier() * reflecter.reflectPower);
	}

	public boolean reflect(PhysicsEntity reflecter) {
		if (this.reflectCooldown == 0) {
			this.setFacing(!this.getFacing());
			this.velocity.multLocal(new Vector2f(-1, -1));
			this.reflectCooldown = .5f;

			return true;
		}

		return false;
	}

	@Override
	public void onDeath() {
		this.damage = 0;
	}

	public float whatIsPower() {
		return this.mostRecentPower;
	}

	public void setDamageDealt(float dealt) {
		this.damageDealt = dealt;
	}

	public void setKnockbackDealt(float kb) {
		this.attackKnockback = kb;
	}
	
	public void heal(int dmg){
		if (dmg < this.damage){
			this.damage -= dmg;
		} else {
			this.damage = 0;
		}
	}

	@Override
	public void onCollideWith(PhysicsEntity object, CollisionInfo info) {
		if (!this.canOnlyHitPlayers || object instanceof PlayableCharacter) {
			super.onCollideWith(object, info);

			boolean block = false;
			if (destroyProjectiles
					&& object instanceof Projectile
					&& ((this.priority * info.getPower()) * 10 > ((ViolentEntity) object)
							.getPriority())) {
				block = true;
			}

			if (((object instanceof ViolentEntity)
					&& !this.team.equals(((ViolentEntity) object).team) && ((this.priority * info
					.getPower()) > ((ViolentEntity) object).getPriority()))
					|| block) {
				//System.out.println(getPosition().distance(object.getPosition()));
				float modifiedDamageDealt = this.damageDealt * info.getPower();
				float modifiedKnockback = this.attackKnockback
						* info.getPower();
				if (sectionFrames.length == 0) {
					attackSection = 0;
				} else {
					for (int i = 0; i < sectionFrames.length; i++) {
						if (i == sectionFrames.length - 1) {
							if (this.currentAction.getCurrentFrameNumber() >= sectionFrames[i]) {
								if (i != 0
										&& sectionFrames[i] == sectionFrames[0]
										&& attackSection == i) {
									attackSection = 0;
									break;
								} else {
									attackSection = i;
									break;
								}
							}
						} else {
							if (this.currentAction.getCurrentFrameNumber() >= sectionFrames[i]
									&& this.currentAction
											.getCurrentFrameNumber() < sectionFrames[i + 1]) {
								if (sectionFrames[i + 1] == sectionFrames[i]
										&& attackSection == i) {
									attackSection = i + 1;
									break;
								} else {
									attackSection = i;
									break;
								}
							}
						}
					}
				}

				if (!(object instanceof Item) || !((Item) object).isOnGround()) {
					this.collisionWasSuccessful = this.collisionWasSuccessful
							|| ((ViolentEntity) object)
									.addToDamage(
											modifiedDamageDealt,
											modifiedKnockback,
											this.knockbackModifier,
											Utilities
													.getPhysicsUtility()
													.calculateCorrectAngle(
															object.getAppropriateBounds()
																	.getLowerMidpoint(),
															this.getAppropriateBounds()
																	.getLowerMidpoint()),
											this.getParalyze(), this.getBury(),
											this.willMeteor, this.getFreeze(), attackSection,
											this);
				}

				if (this.canReflect) {
					if (object instanceof Projectile) {
						if (this.reflectType == ReflectType.BOTH) {
							this.collisionWasSuccessful = this.collisionWasSuccessful
									|| ((ViolentEntity) object).reflect(this);
						} else {
							if (((Projectile) object).isEnergyProjectile()) {
								if (this.reflectType == ReflectType.ENERGY) {
									this.collisionWasSuccessful = this.collisionWasSuccessful
											|| ((ViolentEntity) object)
													.reflect(this);
								}
							} else {
								if (this.reflectType == ReflectType.PHYSICAL) {
									this.collisionWasSuccessful = this.collisionWasSuccessful
											|| ((ViolentEntity) object)
													.reflect(this);
								}
							}
						}
					} else if (this.flipsPlayers) {
						this.collisionWasSuccessful = this.collisionWasSuccessful
								|| ((ViolentEntity) object).reflect(this);
					}
				}
			}
		}
		if (this.collisionWasSuccessful)
			this.mostRecentPower = info.getPower();
		else
			this.mostRecentPower = 0.0f;
	}

	@Override
	public void updateEntity() {
		super.updateEntity();
		
		this.damageCooldown -= Utilities.lockedTPF;
		this.reflectCooldown = (float) Math.max(this.reflectCooldown
				- Utilities.lockedTPF, 0);
		this.collisionWasSuccessful = false;
	}

	public boolean getEnergy() {
		return isEnergyAttack;
	}

	public void setMeteor(boolean b) {
		this.willMeteor = b;
	}

	public void setBlockProjectile(boolean b) {
		this.destroyProjectiles = b;
	}

	public void setDamageModifier(float damageModifier) {
		this.damageModifier = damageModifier;
	}
}
