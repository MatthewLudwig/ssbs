package entities.items;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;
import physics.PhysicsModifier;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.MegaMan;
import entities.characters.Toad;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.projectiles.SonicEmerald;

public class ItemTurnip extends Item {
	private PlayableCharacter thrower;
	private int frame;
	
    public ItemTurnip() {
        this.name = "Toad_DEFAULT";
        //this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        frame = getFrame();
        //System.out.println(frame);
        lifeLimit /= 2;        
    }
    
    public int getFrame(){
    	double rand = Math.random();
    	if (rand < 0.24){
    		return 0;
    	} else if (rand < 0.42){
    		return 1;
    	} else if (rand < 0.56){
    		return 2;
    	} else if (rand < 0.67){
    		return 3;
    	} else if (rand < 0.77){
    		return 4;
    	} else if (rand < 0.86){
    		return 5;
    	} else if (rand < 0.94){
    		return 6;
    	}
    	return 7;
    }
    
    @Override
    public boolean onPickedUp(PlayableCharacter player) {
        return true;
    }
    
    @Override
    public boolean onThrown(PlayableCharacter player) {
    	this.thrower = player;
    	this.setAttackCapability(Toad.DMG[7] * (frame+1)/8f, Toad.KNBK[7] * (frame+1)/8f, Toad.PRTY[7]*(frame+1)/8f, false, Toad.SECT[7]);
        return true;
    }
    
    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
    	if (!entity.equals(this.thrower)) {
    		super.onCollideWith(entity, info);
    		/*if (this.getDamageDealt() > 0)
    			this.setDead(true);*/
    	}

    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "veggies", StayingPower.BARELY, 1).addModifier(new CustomAction()).addModifier(new ActionPlaySound("Common Sounds/hit_09.ogg", -1)));
        return actionMap;
    }
    
    public void updateEntity(){
    	super.updateEntity();
    	//currentAction.setCurrentFrame(frame);
    	if (this.isOnGround() && this.thrower != null)
			this.setDead(true);
    	this.display.setLocalTranslation(this.display.getLocalTranslation().x, this.display.getLocalTranslation().y, this.display.getLocalTranslation().z+1);
    }
    
    public class CustomAction extends ActionModifier {
        public CustomAction() {
            
        }

        @Override
        public void onInit() {}

        @Override
        public boolean onUpdate() {
            this.action.setCurrentFrame(frame);

            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
}
