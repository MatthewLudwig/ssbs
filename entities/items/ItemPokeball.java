package entities.items;

import engine.Main;
import engine.ssbs.PokemonList;
import entities.PlayableCharacter;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.pokemon.Pokemon;

import java.util.HashMap;
import java.util.Map;

public class ItemPokeball extends Item {
    private PlayableCharacter thrower;
    private int numberOfBounces;
    
    public ItemPokeball() {
        this.numberOfBounces = 3;
    }
    
    @Override
    public boolean onPickedUp(PlayableCharacter player) {
        return true;
    }
    
    @Override
    public boolean onThrown(PlayableCharacter player) {
        this.setFacing(player.getFacing());
        this.thrower = player;
        return true;
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.isOnGround()) {
            if (this.thrower != null) {
                this.setDead(true);
                
                Pokemon pokemon = null;
                while (pokemon == null)
                	pokemon = PokemonList.getRandomPokemon();
                pokemon.team = this.thrower.team;
                
                Main.getGameState().spawnEntity(pokemon);
                pokemon.setPosition(this.getAppropriateBounds().getLowerMidpoint().x, this.getAppropriateBounds().getLowerMidpoint().y);
                pokemon.setFacing(this.getFacing());
                pokemon.setSpawner(this.thrower);
            } else if (this.numberOfBounces != 0) {
                this.addToPosition(0, 1);
                this.setOnGround(false);
                this.setPureYVelocity(-this.getVelocity().y);
                this.numberOfBounces--;
            }
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "pokeball", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
