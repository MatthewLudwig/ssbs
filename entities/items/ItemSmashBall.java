package entities.items;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

import physics.BoundingBox;
import physics.CollisionInfo;
import physics.PhysicsModifier;

public class ItemSmashBall extends Item {
    private ViolentEntity lastAttacker;
    private float speed;
    private Angle movementDirection;
    
    private float ghostLikeTimer;
    private boolean wasJustGhostLike;
    
    public ItemSmashBall() {
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.speed = 0;
        this.movementDirection = new Angle(this.generator.nextInt(360) - 90);
        this.lifeLimit = 100;
    }
    
    @Override
    public boolean onPickedUp(PlayableCharacter player) {
        return false;
    }
    
    @Override
    public boolean onThrown(PlayableCharacter player) {
        return false;
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        BoundingBox box = Main.getGameState().getWorld().getStage().getCameraBounds();
        
        if (!box.isInBounds(this.realBounds.getLowerLeft()) || !box.isInBounds(this.realBounds.getUpperLeft()) ||
                !box.isInBounds(this.realBounds.getUpperRight()) || !box.isInBounds(this.realBounds.getLowerRight())) {
            this.movementDirection.add(180);
        }
        
        this.ghostLikeTimer -= Utilities.lockedTPF;
        
        if (this.ghostLikeTimer <= 0) {
            if (this.generator.nextInt(4) == 0) {
                this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
            } else {
                this.removePhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
            }
            
            this.ghostLikeTimer = 2;
        }
        
//        if (this.hasCollidedWithObstacle(true) || this.hasCollidedWithObstacle(false)) {                    
//            if (this.wasJustGhostLike) {
//                this.ghostLikeTimer = 2;
//            } else {
//                if (this.generator.nextInt(4) == 0) {
//                    this.ghostLikeTimer = 2;
//                } else {
//                    this.movementDirection.add(180);
//                }
//            }
//        }
        
        if (this.getDamage() >= 25) {
            this.setDead(true);
            this.lastAttacker.setFinalSmash(true);
        }
        
        if (this.lifeTimer >= 60f) {
            box = Main.getGameState().getWorld().getStage().getWorldBounds();
            
            float distanceFromLeft = this.getAppropriateBounds().getExactCenter().x - box.getLeftMidpoint().x;
            float distanceFromRight = box.getRightMidpoint().x - this.getAppropriateBounds().getExactCenter().x;
            float distanceFromBottom = this.getAppropriateBounds().getExactCenter().y - box.getLowerMidpoint().y;
            float distanceFromTop = box.getUpperMidpoint().y - this.getAppropriateBounds().getExactCenter().y;
            
            if (distanceFromLeft < distanceFromRight) {
                if (distanceFromBottom < distanceFromTop) {
                    if (distanceFromLeft < distanceFromBottom) {
                        this.movementDirection = new Angle(180);
                    } else {
                        this.movementDirection = new Angle(-90);
                    }
                } else {
                    if (distanceFromLeft < distanceFromTop) {
                        this.movementDirection = new Angle(180);
                    } else {
                        this.movementDirection = new Angle(90);
                    }
                }
            } else {
                if (distanceFromBottom < distanceFromTop) {
                    if (distanceFromRight < distanceFromBottom) {
                        this.movementDirection = new Angle(0);
                    } else {
                        this.movementDirection = new Angle(-90);
                    }
                } else {
                    if (distanceFromRight < distanceFromTop) {
                        this.movementDirection = new Angle(0);
                    } else {
                        this.movementDirection = new Angle(90);
                    }
                }
            }
        } else {
            this.movementDirection.add(this.generator.nextInt(16) - 8);
        }
        
        this.setVelocity(this.speed * movementDirection.cos(), this.speed * movementDirection.sin(), false);
        
        if (this.speed != 11.25f) {
            this.speed = Math.max(this.speed - (11.25f * Utilities.lockedTPF), 11.25f);
        }
    }

    @Override
    public void onCollideWith(PhysicsEntity object, CollisionInfo info) {
        super.onCollideWith(object, info);
        
        if (object instanceof ViolentEntity && ((ViolentEntity) object).getDamageDealt() != 0) {
            this.lastAttacker = (ViolentEntity) object;
            
            float pieceOne = this.getDamage() * (this.physicsInfo.getWeight() + 1 + (1.4f * this.lastAttacker.getDamageDealt()));
            float pieceTwo = 10f * (this.physicsInfo.getWeight() + 1);
            
            float knockback = ((pieceOne / pieceTwo) + 18f) * .0833f * this.lastAttacker.getKnockback();
            
            if (knockback >= 15f && this.speed == 11.25f) {
                this.movementDirection = Utilities.getPhysicsUtility().calculateCorrectAngle(this.getAppropriateBounds().getLowerMidpoint(), this.lastAttacker.getAppropriateBounds().getLowerMidpoint());
                this.speed = 45f;
            }
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "smashBall", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
