package entities.items;

import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.PlayableCharacter.Scale;
import entities.PlayableCharacter.Scales;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;

public class ItemWarpedMirror extends Item {
    public ItemWarpedMirror() {
    	super();
    	this.scale = 0.5f;
    }
    
    @Override
    public boolean onPickedUp(PlayableCharacter player) {
    	player.addScale(new Scale(Scales.WARPED, 15f, true));
        this.setDead(true);
        return false;
    }
    
    @Override
    public boolean onThrown(PlayableCharacter player) {
        return true;
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "warpedMirror", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
