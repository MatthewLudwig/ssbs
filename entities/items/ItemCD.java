package entities.items;

import engine.Main;
import entities.PlayableCharacter;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

public class ItemCD extends Item {
    public ItemCD() {
        this.lifeLimit = 15;
    }
    
    @Override
    public boolean onPickedUp(PlayableCharacter player) {
        Main.getGameState().unlockMusic();
        this.setDead(true);
        return false;
    }
    
    @Override
    public boolean onThrown(PlayableCharacter player) {
        return true;
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "cd", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
