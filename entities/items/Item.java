package entities.items;

import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.ui.Picture;

import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.Team;
import entities.ViolentEntity;

import java.util.Random;

public abstract class Item extends ViolentEntity {
    protected Random generator;
    protected float lifeTimer;
    protected float lifeLimit;
    
    protected boolean isCurrentlyHeld;
    
    public Item() {
        super("Items", 1, new PhysicsInfo(0, 0.5f, 1, 1), 1, 1, 1);
        this.generator = new Random();
        this.lifeTimer = 0;
        this.lifeLimit = 30;
        this.team = Team.NONE;
        
        this.setHasSuperArmor(true);
    }
    
    public void throwUp(PlayableCharacter player, float speed){
    	this.setPureYVelocity(25*speed);
    	throwItem(player);
    	player.heldItem = null;
    }
    
    public void drop(PlayableCharacter player){
    	this.setPureYVelocity(0);
    	this.setPureXVelocity(0);
    	throwItem(player);
    	player.heldItem = null;
    }
    
    public void throwForward(PlayableCharacter player, float speed){
    	this.setPureXVelocity(20*speed);
    	this.setPureYVelocity(0);
    	throwItem(player);
    	player.heldItem = null;
    }
    
    public void throwNormal(PlayableCharacter player, float speed){
    	this.setPureXVelocity(15*speed);
    	this.setPureYVelocity(0);
    	throwItem(player);
    	player.heldItem = null;
    }
    
    public void throwDown(PlayableCharacter player, float speed){
    	this.setPureYVelocity(-25*speed);
    	throwItem(player);
    	player.heldItem = null;
    }
    
    public final boolean pickUpItem(PlayableCharacter player) {
        if (!this.isCurrentlyHeld && this.onPickedUp(player)) {
            this.isCurrentlyHeld = true;
            
            Material mat = ((Picture) this.display.getChild("Picture")).getMaterial();
            mat.setColor("Color", new ColorRGBA(1, 1, 1, 1));
            this.display.getChild("Picture").setMaterial(mat);
            
            return true;
        } else {
            return false;
        }
    }
    
    protected abstract boolean onPickedUp(PlayableCharacter player);
    
    public boolean throwItem(PlayableCharacter player) {
        if (this.isCurrentlyHeld && this.onThrown(player)) {
            this.isCurrentlyHeld = false;
            return true;
        } else {
            return false;
        }
    }
    
    protected abstract boolean onThrown(PlayableCharacter player);
    
    @Override
    public void updateEntity() {
        super.updateEntity();
        
        if (!this.isCurrentlyHeld) {
        	if (this.isOnGround()){
        		this.setPureXVelocity(0);
        	}
            if (this.lifeTimer >= this.lifeLimit) {
                this.setDead(true);
            } else if (this.lifeTimer >= this.lifeLimit - 5) {
                Material mat = ((Picture) this.display.getChild("Picture")).getMaterial();

                float difference = this.lifeTimer - (float) Math.floor(this.lifeTimer);

                if (difference <= .25f) {
                    mat.setColor("Color", new ColorRGBA(1, 1, 1, .25f));
                } else if (difference <= .50f) {
                    mat.setColor("Color", new ColorRGBA(1, 1, 1, 1));
                } else if (difference <= .75f) {
                    mat.setColor("Color", new ColorRGBA(1, 1, 1, .25f));
                } else {
                    mat.setColor("Color", new ColorRGBA(1, 1, 1, 1));
                }

                this.display.getChild("Picture").setMaterial(mat);
            }
        
            this.lifeTimer += Utilities.lockedTPF;
        }
    }
    
    @Override
    public boolean reflect(PhysicsEntity reflecter) {
        return false;
    }
    
    @Override
    public void setFinalSmash(boolean state) {}
    
    @Override
    protected final String getDefaultAction() {
        return "Default Action";
    }
}
