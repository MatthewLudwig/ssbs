package entities.items;

import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.PlayableCharacter.Scale;
import entities.PlayableCharacter.Scales;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;

public class ItemSuperMushroom extends Item {
    public ItemSuperMushroom() {
    	super();
    	if (Math.random() < 0.5){
    		this.facing = false;
    	} else {
    		this.facing = true;
    	}
    }
    
    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
        if (!this.isDead && entity instanceof PlayableCharacter) {
            ((PlayableCharacter) entity).addScale(new Scale(Scales.GIANT, 15f, true));
            this.setDead(true);
        } else {
            super.onCollideWith(entity, info);
        }
    }
    
    @Override
    public boolean onPickedUp(PlayableCharacter player) {
        return false;
    }
    
    @Override
    public boolean onThrown(PlayableCharacter player) {
        return true;
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.isOnGround()) {
            this.setPureXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.getFacing()) * 20);
        } else {
            this.setPureXVelocity(0);
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "superMushroom", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
