package entities.items;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//import java.time.*;



import java.util.SimpleTimeZone;
import java.util.TimeZone;

import javax.swing.SwingWorker;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

import physics.CollisionInfo;
import physics.PhysicsModifier;
import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.MegaMan;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class ItemThinkerStatue extends Item {
	private PlayableCharacter thrower;
	private float angle;
	private Voice helloVoice;
	private boolean startedTalking;
	
    public ItemThinkerStatue() {
        //this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        angle = 0;
        this.scale = 0.35f;
    }
    
    @Override
    public boolean onPickedUp(PlayableCharacter player) {
        return true;
    }
    
    public void throwNormal(PlayableCharacter player, float speed){
    	if (this.angle == 0){
    		this.angle = 480*Utilities.lockedTPF;
    	}
    }
    
    public void throwForward(PlayableCharacter player, float speed){
    	throwNormal(player, speed);
    }
    
    public void throwUp(PlayableCharacter player, float speed){
    	throwNormal(player, speed);
    }
    public void throwDown(PlayableCharacter player, float speed){
    	throwNormal(player, speed);
    }
    
    @Override
    public boolean onThrown(PlayableCharacter player) {
    	this.thrower = player;
    	this.setAttackCapability(player.getDamageModifier()*9.28f, player.getKnockbackModifier()*1f, 1f, false, new short[]{0});
        return true;
    }
    
    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
    	if (!entity.equals(this.thrower)) {
    		super.onCollideWith(entity, info);
    		if (entity instanceof PlayableCharacter && angle != 0 && !startedTalking){
    			String voiceName = "kevin16";
    			VoiceManager voiceManager = VoiceManager.getInstance();
    	        helloVoice = voiceManager.getVoice(voiceName);
    	        SwingWorker worker = new MyWorker();
    	        worker.execute();
    	        startedTalking = true;
    		}
    	}

    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "thinkerStatue", StayingPower.BARELY, 0).addModifier(new ActionPlaySound("Common Sounds/hit_01.ogg", -1)));
        return actionMap;
    }
    
    public void updateEntity(){
    	super.updateEntity();
    	if (angle == 0 || angle > 360 + 480*Utilities.lockedTPF){
    		angle = 0;
    		this.setAttackCapability(0, 0, 0, false, new short[]{0});
    		startedTalking = false;
    	} else {
    		this.isCurrentlyHeld = true;
    		this.rotateUpTo(new Angle(angle));
    		angle += 480*Utilities.lockedTPF;
    	}
    }
    
    public class MyWorker extends SwingWorker<Integer, String> {

    	@Override
    	protected Integer doInBackground() throws Exception {
    		helloVoice.setRate(120);
	        helloVoice.allocate();
	        Timestamp ts = new Timestamp(System.currentTimeMillis());
	        String timestamp = ts.toString();
	        //System.out.println(timestamp);
	        String str = timestamp.substring(11, 18);
	        //System.out.println(str);
	        String[] sect = str.split(":");
	        int hour = ((Integer.parseInt(sect[0])+8)%12)+1;
	        String minute = sect[1];
	        //System.out.println(helloVoice.getVolume());
	        //System.out.println(Main.getGameSettings().getVoicesVolume()*3);
	        helloVoice.setVolume(Main.getGameSettings().getVoicesVolume()*3);
	        helloVoice.speak("I think the time is" + hour + " " + minute);
	        helloVoice.deallocate();
    		return 1;
    	}
    	  
    	@Override
    	protected void process(List< String> chunks) {
    		// Messages received from the doInBackground() (when invoking the publish() method)
    	}
    }
}
