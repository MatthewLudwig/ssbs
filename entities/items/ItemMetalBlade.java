package entities.items;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;
import physics.PhysicsModifier;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.characters.MegaMan;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class ItemMetalBlade extends Item {
	private PlayableCharacter thrower;
	
    public ItemMetalBlade() {
        this.name = "Mega Man_DEFAULT";
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
    }
    
    @Override
    public boolean onPickedUp(PlayableCharacter player) {
        return true;
    }
    
    @Override
    public boolean onThrown(PlayableCharacter player) {
    	this.thrower = player;
    	this.setAttackCapability(MegaMan.DMG[4] * 1.2f, MegaMan.KNBK[4] * 1.2f, MegaMan.PRTY[4]*1.2f, false, MegaMan.SECT[4]);
        return true;
    }
    
    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
    	if (!entity.equals(this.thrower)) {
    		super.onCollideWith(entity, info);
    	}

    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "metalBlade", StayingPower.BARELY, 0));
        return actionMap;
    }
}
