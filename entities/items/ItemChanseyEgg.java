package entities.items;

import entities.PlayableCharacter;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

public class ItemChanseyEgg extends Item {
    
    public ItemChanseyEgg() {
        this.name = "Pokemon";
    }
    
    @Override
    public boolean onPickedUp(PlayableCharacter player) {
        if (!this.isDead) {
            player.healEntity(5);
            this.setDead(true);
        }
        
        return false;
    }
    
    @Override
    public boolean onThrown(PlayableCharacter player) {
        return false;
    }
    
    @Override
    public void updateEntity() {
        super.updateEntity();
        
        if (this.isOnGround()) {
            this.setVelocity(0, 0, false);
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("Default Action", new CharacterAction(this, "chanseyEgg", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
