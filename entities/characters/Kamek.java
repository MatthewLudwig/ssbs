package entities.characters;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionFallWhenFinished;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.projectiles.KamekFireShot;
import entities.projectiles.KamekGravityEffect;
import entities.projectiles.KamekShapes;
import entities.projectiles.KamekSpikeballShot;

/**
 * The character Kamek within SSBS.
 *
 * @author Matthew Ludwig
 */
public class Kamek extends PlayableCharacter {
	public static final float[] STAT = CharacterStats.KamekStats;
	public static final float[] DMG = CharacterStats.KamekDamage;
	public static final float[] KNBK = CharacterStats.KamekKnockback;
	public static final float[] PRTY = CharacterStats.KamekPriority;
	public static final float[] ISPD = CharacterStats.KamekImageSpeed;
	public static final short[][] SECT = CharacterStats.KamekSectionFrames;
	
    public Kamek() {
        super("Kamek", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        this.scale = 1.24f;
        grabHitbox = 50;
        grabOffset = 25.2f;
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.MIRROR;
    }

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kamek/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], true, SECT[1]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kamek/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_02.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], true, SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kamek/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_14.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], true, SECT[3]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kamek/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_04.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new NeutralBAction(this));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kamek/neutralB1.ogg", 0));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kamek/neutralB2.ogg", 9));
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionSpawnEntity(new short[]{1}, 99, KamekFireShot.class, this).setXOffset(30, true));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kamek/sideB.ogg", 0));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addModifier(new UpBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Kamek/upB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_15.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new DownBAction(this));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kamek/downB.ogg", 0));
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Kamek/finalSmash.ogg", 0));
        action.addModifier(new FinalSmashAction());
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {
        actionMap.put("Up B", this.getUpB().addModifier(new ActionFallWhenFinished()));
    }
    
    public class NeutralBAction extends ActionModifier {
        private Constructor entityConstructor;
        private Object[] entityParameters;
        private float chargeTime;
        private boolean spawned;
        
        public NeutralBAction(Object... parameters) {
            this.entityConstructor = Utilities.getGeneralUtility().getConstructor(KamekShapes.class, parameters);
            this.entityParameters = parameters;
            this.chargeTime = 0;
            this.spawned = false;
        }
        
        @Override
        public void onInit() {
            this.chargeTime = 0;
            this.action.updateLimit = .25f * (3.14f * this.action.entity.getImageSpeed());
            this.spawned = false;
        }

        @Override
        public boolean onUpdate() {
            if (((PlayableCharacter) this.action.entity).checkControl("Special", false)) {
                if (this.action.getCurrentFrameNumber() == 8) {
                    this.action.setCurrentFrame(0);
                }
                
                this.chargeTime += Utilities.lockedTPF;
                
                if (this.chargeTime < 2) {
                    this.action.updateLimit /= 1.01;
                }
            } else {                
                if (!this.spawned) {
                    this.action.setCurrentFrame(9);
                    
                    KamekShapes entity = (KamekShapes) Utilities.getGeneralUtility().createObject(this.entityConstructor, this.entityParameters);
                    entity.setPosition(this.action.entity.getAppropriateBounds().getLowerMidpoint().x, this.action.entity.getAppropriateBounds().getLowerMidpoint().y);
                    entity.setChargeTime(this.chargeTime);
                    
                    Main.getGameState().spawnEntity(entity);
                    this.spawned = true;
                    float offset = Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * 30;
                    entity.addToPosition(offset, ((this.action.animations[0][0].getRealBounds().getDimensions().y * this.action.entity.getScale()) - entity.getAppropriateBounds().getDimensions().y) / 2);
                }
            }
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
    
    public class UpBAction extends ActionModifier {
        private float counter;
        private Angle angle;
        private boolean shouldFinish;

        public UpBAction() {
            this.counter = 0;
            this.shouldFinish = false;
        }

        @Override
        public void onInit() {
            this.angle = new Angle(63.44f);
        }

        @Override
        public boolean onUpdate() {
            if (this.counter < 3) {
                boolean pressingLeft = ((PlayableCharacter) this.action.entity).checkControl("Left", false);
                boolean pressingRight = ((PlayableCharacter) this.action.entity).checkControl("Right", false);
                
                if (pressingLeft && pressingRight) {
                    //This is what happens when you press both.  Nothing!
                } else if (pressingLeft) {
                    if (this.action.entity.getFacing()) {
                        this.angle.add(120 * Utilities.lockedTPF);
                    } else {
                        this.angle.add(-300 * Utilities.lockedTPF);
                    }
                } else if (pressingRight) {
                    if (this.action.entity.getFacing()) {
                        this.angle.add(-300 * Utilities.lockedTPF);
                    } else {
                        this.angle.add(120 * Utilities.lockedTPF);
                    }
                }
                                
                ((Kamek) this.action.entity).rotateUpTo(this.angle);
                this.action.entity.setPureXVelocity(
                        Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * this.angle.cos() * 27f);
                this.action.entity.setPureYVelocity(this.angle.sin() * 27f);
                this.counter += Utilities.lockedTPF;
                
                if (this.action.entity.hasCollidedWithObstacle(true) || this.action.entity.hasCollidedWithObstacle(false)) {
                    this.shouldFinish = true;
                }
            } else {
                this.shouldFinish = true;
            }
            
            return this.shouldFinish;
        }

        @Override
        public boolean shouldFinish() {
            return this.shouldFinish;
        }

        @Override
        public void cleanUp() {
            this.counter = 0;
            this.shouldFinish = false;
            this.action.entity.setPureXVelocity(0);
            this.action.entity.setPureYVelocity(0);
            ((Kamek) this.action.entity).rotateUpTo(new Angle(0));
        }
    }
    
    public class DownBAction extends ActionSpawnEntity {
        private Kamek shooter;
        
        public DownBAction(Kamek shooter) {
            super(new short[]{0}, 99, KamekSpikeballShot.class, shooter);
            this.setXOffset(30, true);
            this.shooter = shooter;
        }
        
        @Override
        public boolean onUpdate() {
            super.onUpdate();
            return !this.shooter.checkControl("Down", false) || !this.shooter.checkControl("Special", false);
        }
        
        @Override
        public boolean shouldFinish() {
            return !this.shooter.checkControl("Down", false) || !this.shooter.checkControl("Special", false);
        }

        @Override
        public void cleanUp() {
            super.cleanUp();
            
            ((KamekSpikeballShot) this.lastSpawnedEntity).transform();
        }
    }

    public class FinalSmashAction extends ActionModifier {    
        private float moveTimer;
        private boolean done;
        
        public FinalSmashAction() {
            this.moveTimer = 1;
            this.done = false;
        }

        @Override
        public void onInit() {
            this.moveTimer = 1;
            this.done = false;
        }

        @Override
        public boolean onUpdate() {
            if (this.moveTimer <= 0 && !this.done) {
                List<PlayableCharacter> characters = Main.getGameState().getWorld().getPlayerList();
                
                for (PlayableCharacter character : characters) {
                    if (!((PlayableCharacter) this.action.entity).equals(character)) {
                        KamekGravityEffect effect = new KamekGravityEffect((Kamek) this.action.entity, character);
                        effect.setPosition(character.getAppropriateBounds().getLowerMidpoint().x, 
                                    character.getAppropriateBounds().getLowerMidpoint().y - (effect.getDimensions().y / 2) + (character.getAppropriateBounds().getDimensions().y / 2));
                        Main.getGameState().spawnEntity(effect);
                    }
                }
                
                this.done = true;
            }
            
            this.moveTimer -= Utilities.lockedTPF;
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
        }
    }
}
