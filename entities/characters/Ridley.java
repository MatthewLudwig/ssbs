package entities.characters;

import java.util.HashMap;
import java.util.Map;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.TransformingCharacter;
import entities.TransformingCharacter.ActionTransform;
import entities.characters.Krystal.ActionFinalSmash;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionFallWhenFinished;
import entities.characters.actions.ActionFinalSmashFreeze;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionOnlyUsableOnGround;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionReflect;
import entities.characters.actions.ActionSetFacing;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionWaitTillControl;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.projectiles.KrystalSmashLaser;
import entities.projectiles.RidleyFireball;
import entities.projectiles.RidleySmashFireball;
import entities.projectiles.RidleyTail;
//import entities.general.RidleyCoin;
//import entities.projectiles.RidleyFinalFireball;
//import entities.projectiles.RidleyFireball;

/**
 * The character Ridley within SSBS.
 *
 * @author Matthew Ludwig
 */
public class Ridley extends TransformingCharacter {
	public static final float[] STAT = CharacterStats.RidleyStats;
	public static final float[] DMG = CharacterStats.RidleyDamage;
	public static final float[] KNBK = CharacterStats.RidleyKnockback;
	public static final float[] PRTY = CharacterStats.RidleyPriority;
	public static final float[] ISPD = CharacterStats.RidleyImageSpeed;
	public static final short[][] SECT = CharacterStats.RidleySectionFrames;
	
	private boolean init;
	
    public Ridley() {
        super("Ridley", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
        grabHitbox = 75;
        grabOffset = 0;
        init = false;
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.SUPLEX;
    }
    
    @Override
    protected Map<String, CharacterAction> setUpSecondActionMap() {
    	Map<String, CharacterAction> map = new HashMap<String, CharacterAction>();
        
        CharacterAction firstAction = new CharacterAction(this, "metaRidleyStand", StayingPower.NONE, ISPD[8]);
        map.put("Stand", firstAction);
        
        CharacterAction secondAction = new CharacterAction(this, "metaRidleyFly", StayingPower.BARELY, ISPD[8]);
        secondAction.addModifier(new ActionSetFacing(true));
        secondAction.addModifier(new ActionMovement(25f, 0f, 0f, true));
        secondAction.addModifier(new ActionWaitTillControl("Right", false, false));
        map.put("Walk Right", secondAction);
        map.put("Sprint Right", secondAction);
        
        CharacterAction thirdAction = new CharacterAction(this, "metaRidleyFly", StayingPower.BARELY, ISPD[8]);
        thirdAction.addModifier(new ActionSetFacing(false));
        thirdAction.addModifier(new ActionMovement(25f, 0f, 0f, true));
        thirdAction.addModifier(new ActionWaitTillControl("Left", false, false));
        map.put("Walk Left", thirdAction);
        map.put("Sprint Left", thirdAction);
        
        CharacterAction jumpAction = new CharacterAction(this, "metaRidleyFly", StayingPower.BARELY, ISPD[8]);
        jumpAction.addModifier(new ActionMovement(37f, 90f, 0f, false)).addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        map.put("Jump 1", jumpAction);
        map.put("Jump 2", jumpAction);
        
        CharacterAction fourthAction = new CharacterAction(this, "metaRidleyShoot", StayingPower.BARELY, ISPD[8]);
//        fourthAction.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/finalSmash2.ogg", 0));
        //fourthAction.addModifier(new ActionAttack(15.17f, 2.79f, 99.99f));
        fourthAction.addModifier(new ActionSpawnEntity(new short[]{0}, 99, RidleySmashFireball.class, this).setXOffset(60, true).setYOffset(45));
        map.put("Neutral A", fourthAction);
        map.put("Neutral B", fourthAction);
        
        CharacterAction fifthAction = new CharacterAction(this, "metaRidleyStand", StayingPower.EXTREME, ISPD[8]);
//        fifthAction.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/finalSmash3.ogg", 0));
        fifthAction.addModifier(new ActionTransform(0));
        fifthAction.addModifier(new ActionFinalSmashFreeze());
        fifthAction.addModifier(new UnTransformAction());
        map.put("Transform", fifthAction);
        
        return map;
    }
    
    public void updateEntity(){
    	super.updateEntity();
    	if (!init){
    		init = true;
    		this.scale = Main.getGameState().getWorld().getStage().scaleForRidley();
    		System.out.println(1/this.scale);
    		this.setScale(this.scale);
    		PhysicsInfo p = this.physicsInfo;
    		grabHitbox *= scale;
    		grabOffset = -622.32f/scale + 201.66f/(scale*scale) + 430.65f;
    		//System.out.println(grabHitbox);
    		p.setWeight((float) (STAT[0] * Math.sqrt(this.scale)));
    		p.setMoveSpeed((float) (STAT[3] / Math.sqrt(this.scale)));
    	}
    	//System.out.println("Ridley: " + this.display.getLocalTranslation().x + ", " + this.display.getLocalTranslation().y);
    }

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ridley/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_01.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], true, SECT[1]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ridley/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_00.ogg", -1));
        //action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ridley/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_08.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ridley/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_10.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new ActionSpawnEntity(new short[]{5}, 99, RidleyFireball.class, this).setXOffset(30, true).setYOffset(15));
        action.addAnimation(1, "neutralB1");
        action.addAnimation(2, "neutralB2");
        action.addModifier(new NeutralBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Ridley/neutralB.ogg", 9));
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionAttack(DMG[5], KNBK[5], PRTY[5], SECT[5]));
        action.addModifier(new ActionFallWhenFinished());
        action.addModifier(new SideBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Ridley/sideB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_09.ogg", -1));
        //action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ridley/upB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_10.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        //action.addModifier(new ActionAttack(DMG[7], KNBK[7], PRTY[7], SECT[7]));
        action.addModifier(new DownBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Ridley/downB.ogg", 0));
        action.addModifier(new ActionOnlyUsableOnGround());
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Ridley/finalSmash.ogg", 0));
        action.addModifier(new ActionFinalSmashFreeze());
        action.addModifier(new FinalSmashAction());
        action.addModifier(new ActionTransform(12));
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {}
    
    public class NeutralBAction extends ActionModifier {
        public NeutralBAction() {
        }

        @Override
        public void onInit() {
        	
        }

        @Override
        public boolean onUpdate() {
        	int n = this.action.getCurrentFrameNumber();
        	if (((PlayableCharacter) this.action.entity).checkControl("Up", false) && !((PlayableCharacter) this.action.entity).checkControl("Down", false) && n < 4 && this.action.getCurrentAnimationNumber() == 0){
        		this.action.setCurrentAnimation(1);
        		this.action.setCurrentFrame(n);
    		} else if (!((PlayableCharacter) this.action.entity).checkControl("Up", false) && ((PlayableCharacter) this.action.entity).checkControl("Down", false) && n < 4 && this.action.getCurrentAnimationNumber() == 0){
    			this.action.setCurrentAnimation(2);
    			this.action.setCurrentFrame(n);
    		}
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
        	//((PlayableCharacter) this.action.entity).setFinalSmash(true);
        }
    }
    
    public class SideBAction extends ActionModifier {
    	Angle angle;
        public SideBAction() {
        }

        @Override
        public void onInit() {
            angle = new Angle(0);
            if (!this.action.entity.getFacing()){
            	angle = new Angle(180);
            }
            this.action.entity.addToPosition(0,-20);
        }

        @Override
        public boolean onUpdate() {
        	if (((PlayableCharacter) this.action.entity).checkControl("Up", false) && !((PlayableCharacter) this.action.entity).checkControl("Down", false)){
    			angle.add(30*Utilities.lockedTPF);
    		} else if (!((PlayableCharacter) this.action.entity).checkControl("Up", false) && ((PlayableCharacter) this.action.entity).checkControl("Down", false)){
    			angle.add(-30*Utilities.lockedTPF);
    		}
        	
        	if (this.action.entity.getFacing()){
        		this.action.entity.rotateUpTo(angle);
        		this.action.entity.setPureXVelocity(angle.cos()*25);
        		this.action.entity.setPureYVelocity(angle.sin()*25);
        	} else {
        		Angle ang = new Angle(angle.getValue()-180);
        		this.action.entity.rotateUpTo(ang);
        		this.action.entity.setPureXVelocity(ang.cos()*-25);
        		this.action.entity.setPureYVelocity(ang.sin()*25);
        	}
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
        	this.action.entity.setPureXVelocity(0);
    		this.action.entity.setPureYVelocity(0);
            this.action.entity.rotateUpTo(new Angle(0));
        }
    }
    
    public class DownBAction extends ActionModifier {
    	RidleyTail tail;
        public DownBAction() {
        }

        @Override
        public void onInit() {
        	tail = new RidleyTail((Ridley) this.action.entity);
        	tail.setPosition(this.action.entity.getPosition().x + 15*Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()), this.action.entity.getPosition().y-5);
        	Main.getGameState().spawnEntity(tail);
        }

        @Override
        public boolean onUpdate() {
        	if (tail != null && !tail.isDead() && this.action.getCurrentFrameNumber() >= 13){
        		this.action.setCurrentFrame(13);
        	}
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
        	if (tail != null && !tail.isDead())
        		tail.setDead(true);
        }
    }
    
    public class FinalSmashAction extends ActionModifier {
    	boolean init;
        public FinalSmashAction() {
        }

        @Override
        public void onInit() {
        	init = false;
        }

        @Override
        public boolean onUpdate() {
        	if (!init){
        		this.action.entity.setScale(this.action.entity.getScale()*2);
        		init = true;
        	}
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
        	
        }
    }
    
    public class UnTransformAction extends ActionModifier {
    	boolean init;
        public UnTransformAction() {
        }

        @Override
        public void onInit() {
        	init = false;
        }

        @Override
        public boolean onUpdate() {
        	if (!init){
        		this.action.entity.setScale(this.action.entity.getScale()*0.5f);
        		init = true;
        	}
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
        	
        }
    }
}
