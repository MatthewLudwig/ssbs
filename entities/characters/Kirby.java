package entities.characters;

import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;

import physics.CollisionInfo;
import physics.PhysicsModifier;

import com.jme3.audio.AudioNode;
import com.jme3.math.Vector2f;

import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionFinalSmashFreeze;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionReflect.ReflectType;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.general.KirbyAbilityStar;
import entities.projectiles.KirbyFinalCutter;
import entities.projectiles.KirbyPlasmaShot;

/**
 * The character Kirby within SSBS.
 *
 * @author Matthew Ludwig
 */
public class Kirby extends PlayableCharacter {
    private KirbyAbility currentAbility;
    public static final float[] STAT = CharacterStats.KirbyStats;
	public static final float[] DMG = CharacterStats.KirbyDamage;
	public static final float[] KNBK = CharacterStats.KirbyKnockback;
	public static final float[] PRTY = CharacterStats.KirbyPriority;
	public static final float[] ISPD = CharacterStats.KirbyImageSpeed;
	public static final short[][] SECT = CharacterStats.KirbySectionFrames;
    
    public Kirby() {
        super("Kirby", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        //this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.scale = .97f;
        setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
        grabHitbox = 20;
    }
    
    @Override
    public KirbyAbility getKirbyAbility() {
        return null;
    }
    
    @Override
    public void onDeath() {
        super.onDeath();
        
        this.currentAbility = null;
    }
    
    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
        if (entity instanceof ViolentEntity && !((ViolentEntity) entity).isOnSameTeamAs(this) && 
                ((ViolentEntity) entity).getDamageDealt() > 0 && ((ViolentEntity) entity).getPriority() > this.getPriority()) {
            float chance = (5 * ((ViolentEntity) entity).getDamageDealt()) / 100;
            
            if (Math.random() < chance) {
                this.currentAbility = null;
            }
        }
        
        super.onCollideWith(entity, info);
    }
    
    @Override
    public void updateEntity() {
        super.updateEntity();
        
        if (this.checkControl("Special", false) && this.checkControl("Shield", false) && this.currentAbility != null) {
        	KirbyAbilityStar star = new KirbyAbilityStar(this.getFacing());
        	star.setPosition(this.getAppropriateBounds().getLowerMidpoint().x, this.getAppropriateBounds().getLowerMidpoint().y);
        	Main.getGameState().spawnEntity(star);
        	
            this.currentAbility = null;
            
            this.forceCurrentAction(this.actionMap.get(this.getDefaultAction()));
        }
    }

    @Override
    protected CharacterAction getNeutralA() {
    	CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kirby/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_15.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
    	CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], true, SECT[1]));
        action.addModifier(new ActionMovement(32f, 0f, 0, true));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kirby/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
    	CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kirby/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_07.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
    	CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kirby/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_04.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
    	CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addAnimation(1, "neutralB0");
        action.addAnimation(2, "neutralB1");
        
        for (KirbyAbility ability : KirbyAbility.values()) {
            action.addAnimation(ability.getAnimationIndex(), ability.animationName);
            action.addModifier(new ActionPlaySound("/Character Sounds/Kirby/" + ability.soundName + ".ogg", 0, ability.getAnimationIndex()));
        }
        
        action.addModifier(new NeutralBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Kirby/suck.ogg", 1));
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
    	CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionAttack(DMG[5], KNBK[5], PRTY[5], SECT[5]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kirby/sideB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_05.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
    	CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new UpBAction());
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kirby/upB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_08.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
    	CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionToggleAttribute(Attribute.HEAVYARMOR));
        action.addModifier(new DownBAction());
        action.addModifier(new ActionAttack(DMG[7], KNBK[7], PRTY[7], SECT[7]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Kirby/downB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_02.ogg", -1));
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
    	CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
//        action.addModifier(new ActionPlaySound("/Character Sounds/Kirby/finalSmash.ogg", 0));
        action.addModifier(new ActionFinalSmashFreeze());
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {
        actionMap.get("Jump 2").addModifier(new JumpingAction());    	
    }
    
    public class JumpingAction extends ActionModifier {
        private AudioNode audio;
        private int numberOfJumpsRemaining;

        public JumpingAction() {
            audio = Utilities.getCustomLoader().getAudioNode("/Character Sounds/Kirby/jump2.ogg");
            this.numberOfJumpsRemaining = 5;
        }

        @Override
        public void onInit() {
            this.numberOfJumpsRemaining = 5;
        }

        @Override
        public boolean onUpdate() {
            if (((PlayableCharacter) this.action.entity).checkControl("Up", true) && this.numberOfJumpsRemaining > 0) {
                this.action.entity.setYVelocity(25 - (12.5f / this.numberOfJumpsRemaining), true);
                this.action.setCurrentFrame(0);
                this.numberOfJumpsRemaining--;
                this.audio.playInstance();
            }

            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
    
    public class NeutralBAction extends ActionModifier {
    	private final float DAMAGE = 	DMG[4];
    	private final float KNOCKBACK = 	KNBK[4];
    	private final float PRIORITY = 	PRTY[4];
    	private final short[] SECTIONS = SECT[4];
    	
        public Vector2f grabRange;
        public ArrayList<PlayableCharacter> exceptionList;
        private PlayableCharacter theTarget;
        private boolean shouldFinish;
        private boolean hadAbility;
                
        private float initialLimit = -1;
        private float fData1;
        private float fData2;
        private float fData3;
        private boolean bData;
        
        public NeutralBAction() {
            this.exceptionList = new ArrayList<PlayableCharacter>();
        }
        
        @Override
        protected void onInit() {
            this.grabRange = this.action.entity.getAppropriateBounds().getDimensions().mult(3);
            this.exceptionList.clear();
            this.exceptionList.add((Kirby) this.action.entity);
            this.theTarget = null;
            this.shouldFinish = false;
            this.hadAbility = ((Kirby) this.action.entity).currentAbility == null;
            
            if (this.initialLimit == -1) {
            	this.initialLimit = this.action.updateLimit;
            }
            
            this.fData1 = 0;
            this.fData2 = 0;
            this.fData3 = .75f;
            this.bData = true;
            
            this.action.updateLimit = this.initialLimit;
        }

        @Override
        protected boolean onUpdate() {
            Kirby kirby = (Kirby) this.action.entity;
            
            if (this.hadAbility) {
                if (this.theTarget == null) {
                    PlayableCharacter target = null;

                    while (target == null) {
                        target = Main.getGameState().getWorld().getClosestPlayer(kirby.getAppropriateBounds().getExactCenter(), this.exceptionList.toArray(new PlayableCharacter[0]));

                        if (target == null) {
                            break;
                        } else if (target.isOnSameTeamAs(kirby)) {
                            this.exceptionList.add(target);
                        } else {
                            Vector2f difference = target.getAppropriateBounds().getExactCenter().subtract(kirby.getAppropriateBounds().getExactCenter());

                            if (Math.abs(difference.x) < this.grabRange.x && Math.abs(difference.y) < this.grabRange.y) {
                                Vector2f pull = difference.normalize().mult(-3);
                                target.addToPosition(pull.x, pull.y);

                                if (target.getAppropriateBounds().isInBounds(kirby.getAppropriateBounds())) {
                                    target.setPureYVelocity(20);
                                    this.theTarget = target;
                                    this.action.setCurrentAnimation(1);
                                }
                            } else {
                                break;
                            }
                        }
                    }
                } else if (this.action.getCurrentAnimationNumber() == 2) {
                	if (this.fData1 >= .5f) {
                		this.shouldFinish = true;
                		return true;
                	} else {
                		this.fData1 += Utilities.lockedTPF;
                		if (kirby.currentAbility != null)
                			this.action.setCurrentFrame(kirby.currentAbility.ordinal());
                    	AudioNode sound = Utilities.getCustomLoader().getAudioNode("/Character Sounds/Kirby/getAbility.ogg");
                    	sound.setVolume(Main.getGameSettings().getVoicesVolume());
                	}
                } else {
                    kirby.currentAbility = this.theTarget.getKirbyAbility();
                    
                    if (kirby.currentAbility == null) {
                    	this.shouldFinish = true;
                    	return true;
                    }
                    
                    this.action.setCurrentAnimation(2);
                }
                                
                if (!kirby.checkControl("Special", false)) {
                    this.shouldFinish = true;
                    return true;
                } else {
                    return false;
                }
            } else {
        		this.action.setCurrentAnimation(kirby.currentAbility.getAnimationIndex());
                switch (kirby.currentAbility) {
                    case FIRE:
                    	if (kirby.checkControl("Special", false) && this.action.getCurrentFrameNumber() == 12) {
                    		this.action.setCurrentFrame(10);
                    	}
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, true, SECTIONS);
                    	break;
                    case SWORD:
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, false, SECTIONS);
                        break;
                    case PLASMA:
                    	if (this.bData) {
                    		PhysicsEntity object = new KirbyPlasmaShot(kirby);
                    		object.setPosition(kirby.getAppropriateBounds().getLowerMidpoint().x, kirby.getAppropriateBounds().getLowerMidpoint().y);
                    		Main.getGameState().spawnEntity(object);
                    		
                    		object.addToPosition(0, ((this.action.animations[0][0].getRealBounds().getDimensions().y) / 2) - ((object.getAppropriateBounds().getDimensions().y) / 2));
                    		this.bData = false;
                    	}
                        break;
                    case FIGHTER:
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, false, SECTIONS);
                        break;
                    case WHEEL:
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, false, SECTIONS);

                        if (this.bData) {
                            if (this.fData1 <= 0) {
                                this.fData1 = 1;
                                
                                this.fData2 += 30;
                            } else {
                                this.fData1 -= Utilities.lockedTPF;
                            }
                            
                            this.fData3 = Math.min(this.fData3 + (.875f * Utilities.lockedTPF), 2.5f);
                            this.action.updateLimit = (float) Math.max(this.action.updateLimit / 1.01, .05);
                        
                            if (!kirby.checkControl("Special", false)) {
                                this.bData = false;
                                this.fData1 = 0;
                            }
                            
                            return false;
                        } else {
                            if (this.fData1 >= this.fData3) {
                                this.shouldFinish = true;
                                return true;
                            } else {
                                this.fData1 += Utilities.lockedTPF;
                                
                                if (kirby.checkControl("Left", true)) {
                                    this.action.entity.setFacing(false);
                                } else if (kirby.checkControl("Right", true)) {
                                    this.action.entity.setFacing(true);
                                }
                                
                                this.action.entity.setPureXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(kirby.getFacing()) * this.fData2);
                                return false;
                            }
                        }
                    case HAMMER:
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, false, SECTIONS);
                        break;
                    case SUPLEX:
                    	if (this.action.getCurrentFrameNumber() >= 3) {
                    		kirby.setXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(kirby.getFacing()) * 45);
                    	}
                    	
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, false, SECTIONS);
                        break;
                    case ICE:
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, false, SECTIONS);
                        break;
                    case BEAM:
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, false, SECTIONS);
                        break;
                    case PAINT:
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, false, SECTIONS);
                        break;
                    case YOYO:
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, false, SECTIONS);
                        break;
                    case CUTTER:
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, false, SECTIONS);
                        break;
                    case JET:
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, false, SECTIONS);
                        break;
                    case PARASOL:
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, false, SECTIONS);
                        break;
                    case MIRROR:
                    	kirby.setAttackCapability(0, 0, 10, false, new short[]{0});
                    	kirby.setCanReflect(1f, false, ReflectType.BOTH);
                        break;
                    case NINJA:
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, false, SECTIONS);
                        break;
                    case BOMB:
                    	kirby.setAttackCapability(DAMAGE, KNOCKBACK, PRIORITY, false, SECTIONS);
                        break;
                    default:
                        Main.log(Level.SEVERE, "Unknown kirby ability is being used!  " + kirby.currentAbility.name(), null);
                }
                
                this.shouldFinish = true;
                return false;
            }
        }

        @Override
        protected boolean shouldFinish() {
            return this.shouldFinish;
        }

        @Override
        protected void cleanUp() {
            Kirby kirby = (Kirby) this.action.entity;
        	kirby.setAttackCapability(0, 0, 0, false);
        	kirby.setCantReflect();
        	kirby.setXVelocity(0);
        }
    }
    
    public class UpBAction extends ActionModifier {
        public UpBAction() {
            
        }
        
        @Override
        protected void onInit() {
        }

        @Override
        protected boolean onUpdate() {
        	if (this.action.getCurrentFrameNumber() == 4 && this.action.entity.getVelocity().y > 0) {
        		this.action.setCurrentFrame(3);
        	}

        	if (this.action.getCurrentFrameNumber() == 8 && !this.action.entity.isOnGround()) {
        		this.action.setCurrentFrame(7);
        	}
        	
        	return false;
        }

        @Override
        protected boolean shouldFinish() {
        	return true;
        }

        @Override
        protected void cleanUp() {
        	if (this.action.entity.isOnGround()) {
        		KirbyFinalCutter cutter = new KirbyFinalCutter((Kirby) this.action.entity);
        		cutter.setPosition(this.action.entity.getAppropriateBounds().getLowerMidpoint().x, this.action.entity.getAppropriateBounds().getLowerMidpoint().y);
        		cutter.team = ((Kirby) this.action.entity).team;
        		Main.getGameState().spawnEntity(cutter);
        		cutter.setFacing(this.action.entity.getFacing());
        	}
        }
    }
    
    public class DownBAction extends ActionModifier {
        private float timer = 0;
        
        public DownBAction() {
        
        }

        @Override
        protected void onInit() {
            this.timer = 0;
        }

        @Override
        protected boolean onUpdate() {
            if (this.action.getCurrentFrameNumber() < 7) {
                this.action.entity.setPureXVelocity(0);
                this.action.entity.setPureYVelocity(0);
            } else {
                if (((PlayableCharacter) this.action.entity).checkControl("Special", true)) {
                    this.action.setCurrentFrame(9);
                    this.action.freezeFrame(false);
                    this.timer = 3;
                }
                
                if (this.action.getCurrentFrameNumber() == 7) {
                    if (this.action.entity.isOnGround()) {
                        this.action.entity.gravityModifier = 1f;
                        this.action.setCurrentFrame(8);
                        this.action.freezeFrame(false);
                    } else {
                        this.action.entity.gravityModifier = 2.5f;
                        this.action.freezeFrame(true);
                    }
                } else if (this.action.getCurrentFrameNumber() == 8) {
                	this.action.freezeFrame(true);
                    if (this.timer >= 2.5f) {
                        this.action.setCurrentFrame(9);
                        this.action.freezeFrame(false);
                    } else {
                    	this.timer += Utilities.lockedTPF;
                    }
                }
            }
            
            return false;
        }

        @Override
        protected boolean shouldFinish() {
            return true;
        }

        @Override
        protected void cleanUp() {
            this.action.entity.setPureYVelocity(5);
            this.action.entity.gravityModifier = 1f;
            this.action.freezeFrame(false);
        }
    }
    
    public enum KirbyAbility {
        FIRE("neutralB2", "fire"),
        SWORD("neutralB3", "sword"),
        PLASMA("neutralB4", "plasma"),
        FIGHTER("neutralB5", "fighting"),
        WHEEL("neutralB6", "wheel"),
        HAMMER("neutralB7", "fighting"),
        SUPLEX("neutralB8", "fighting"),
        ICE("neutralB9", "fire"), /*Wrong Animation*/
        BEAM("neutralB9", "fire"), /*Wrong Animation*/
        PAINT("neutralB9", "fire"), /*Wrong Animation*/
        YOYO("neutralB9", "fire"), /*Wrong Animation*/
        CUTTER("neutralB9", "fire"), /*Wrong Animation*/
        JET("neutralB9", "fire"), /*Wrong Animation*/
        PARASOL("neutralB9", "fire"), /*Wrong Animation*/
        MIRROR("neutralB9", "getAbility"), /*Wrong Sound*/
        NINJA("neutralB9", "fire"), /*Wrong Animation*/
        BOMB("neutralB9", "fire"); /*Wrong Animation*/
        
        public final String animationName;
        public final String soundName;
        
        private KirbyAbility(String name, String soundName) {
            this.animationName = name;
            this.soundName = soundName;
        }
        
        private final int getAnimationIndex() {
            return 3 + this.ordinal();
        }
    }
}
