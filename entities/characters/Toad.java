package entities.characters;

import java.util.Map;

import com.jme3.audio.AudioNode;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionFinalSmashFreeze;
import entities.characters.actions.ActionHalfHop;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionOnlyUsableOnGround;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionReflect;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionStallThenFall;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.items.ItemTurnip;
import entities.projectiles.ToadIceball;
import entities.projectiles.ToadSoccerBall;

/**
 * The character Toad within SSBS.
 *
 * @author Matthew Ludwig
 */
public class Toad extends PlayableCharacter {
	public static final float[] STAT = CharacterStats.ToadStats;
	public static final float[] DMG = CharacterStats.ToadDamage;
	public static final float[] KNBK = CharacterStats.ToadKnockback;
	public static final float[] PRTY = CharacterStats.ToadPriority;
	public static final float[] ISPD = CharacterStats.ToadImageSpeed;
	public static final short[][] SECT = CharacterStats.ToadSectionFrames;
	
    public Toad() {
        super("Toad", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
        this.scale = 0.78f;
        grabHitbox = 20;
        grabOffset = -20;
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.FIRE;
    }
    
    public void updateEntity(){
    	super.updateEntity();
    	//System.out.println("Toad: " + this.display.getLocalTranslation().x + ", " + this.display.getLocalTranslation().y);
    }

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Toad/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_14.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], SECT[1]));
        action.addModifier(new ActionToggleAttribute(Attribute.HEAVYARMOR));
        action.addModifier(new ActionPlaySound("/Character Sounds/Toad/sideA1.ogg", 0));
        action.addModifier(new ActionPlaySound("/Character Sounds/Toad/sideA2.ogg", 62));
        action.addModifier(new ActionMovement(10f, 0f, 0, true, 23, 57));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_14.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Toad/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_00.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionHalfHop());
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        action.addModifier(new ActionToggleAttribute(Attribute.METEORS));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addModifier(new ActionWaitTillOnGround(5));
        action.addModifier(new ActionStallThenFall(0.2f));
        action.addModifier(new ActionPlaySound("/Character Sounds/Toad/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_04.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new ActionSpawnEntity(new short[]{2}, 99, ToadIceball.class, this).setXOffset(5, true));
        action.addModifier(new ActionPlaySound("/Character Sounds/Toad/neutralB.ogg", 2));
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionSpawnEntity(new short[]{10}, 99, ToadSoccerBall.class, this).setXOffset(25, false).setYOffset(15).setXOffset(25, true));
        action.addModifier(new ActionPlaySound("/Character Sounds/Toad/sideB.ogg", 10));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addModifier(new ActionMovement(33f*STAT[8], 60f, 0, true, 1, -1).setIsRecovery());
        action.addModifier(new UpBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Toad/upB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_15.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionOnlyUsableOnGround());
        action.addModifier(new DownBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Toad/downB.ogg", 3));
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new FinalSmashAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Toad/finalSmash.ogg", 10));
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {}
    
    public class DownBAction extends ActionModifier {
    	private boolean init;
    	
        public DownBAction() {
        }

        @Override
        public void onInit() {
            init = false;
        }

        @Override
        public boolean onUpdate() {
            if (this.action.getCurrentFrameNumber() >= 4 && !init){
            	//System.out.println("hello");
            	init = true;
            	if (((PlayableCharacter) this.action.entity).heldItem == null){
            		ItemTurnip turnip = new ItemTurnip();
            		turnip.setPosition(this.action.entity.getPosition().x, this.action.entity.getPosition().y);
            		Main.getGameState().spawnEntity(turnip);
            		turnip.pickUpItem(((PlayableCharacter) this.action.entity));
            		((PlayableCharacter) this.action.entity).heldItem = turnip;
            	} else {
            		((PlayableCharacter) this.action.entity).heldItem.throwUp(((PlayableCharacter) this.action.entity), this.action.entity.getPhysicsInfo().getJumpSpeed());
            	}
            }
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
            
        }
    }
    
    public class UpBAction extends ActionModifier {
    	private boolean init;
    	
        public UpBAction() {
        }

        @Override
        public void onInit() {
        	init = false;
        }

        @Override
        public boolean onUpdate() {            
            if (!init){
            	init = true;
            	if (((PlayableCharacter)(this.action.entity)).checkControl("Left", false) && this.action.entity.getFacing()){
            		this.action.entity.setFacing(false);
            	} else if (((PlayableCharacter)(this.action.entity)).checkControl("Right", false) && !this.action.entity.getFacing()){
            		this.action.entity.setFacing(true);
            	}
            }
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
        	return true;
        }

        @Override
        public void cleanUp() {
        	//((PlayableCharacter)(this.action.entity)).setFinalSmash(true);
        }
    }
    
    public class FinalSmashAction extends ActionModifier {
    	private boolean init, startedOnGround, beginning;
    	private AudioNode audio;
    	
        public FinalSmashAction() {
        	audio = Utilities.getCustomLoader().getAudioNode("/Common Sounds/coin.ogg");
        }

        @Override
        public void onInit() {
        	init = false;
        	beginning = true;
            startedOnGround = false;
        }

        @Override
        public boolean onUpdate() {
        	if (beginning){
        		startedOnGround = this.action.entity.isOnGround();
        		beginning = false;
        		this.action.entity.gravityModifier = 0.2f;
        	}
        	if (this.action.getCurrentFrameNumber() >= 20 && !this.action.entity.isOnGround() && !init && !startedOnGround){
        		this.action.setCurrentFrame(20);
        		this.action.entity.gravityModifier = 4;
        	}
        	if (this.action.getCurrentFrameNumber() >= 21 && !init){
        		this.action.entity.gravityModifier = 1;
            	init = true;
            	for (PhysicsEntity entity : Main.getGameState().getWorld().getPhysicsList()){
            		if (entity instanceof ViolentEntity){
            			((ViolentEntity) entity).addToDamage(DMG[8]*STAT[6], KNBK[8]*STAT[5], 1, new Angle(90), false, false, false, false, 0, (ViolentEntity) this.action.entity);
            			this.audio.playInstance();
            		}
            	}
            }
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
        	return true;
        }

        @Override
        public void cleanUp() {
        	
        }
    }
}
