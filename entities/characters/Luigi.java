package entities.characters;

import java.util.Map;

import physics.BoundingBox;
import engine.Main;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionFallWhenFinished;
import entities.characters.actions.ActionFinalSmashFreeze;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.projectiles.LuigiFireball;
import entities.projectiles.LuigiKingBoo;

/**
 * The character Luigi within SSBS.
 *
 * @author Matthew Ludwig
 */
public class Luigi extends PlayableCharacter {
	public static final float[] STAT = CharacterStats.LuigiStats;
	public static final float[] DMG = CharacterStats.LuigiDamage;
	public static final float[] KNBK = CharacterStats.LuigiKnockback;
	public static final float[] PRTY = CharacterStats.LuigiPriority;
	public static final float[] ISPD = CharacterStats.LuigiImageSpeed;
	public static final short[][] SECT = CharacterStats.LuigiSectionFrames;
	
    public Luigi() {
        super("Luigi", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        this.scale = .81f;
        setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
        grabHitbox = 15;
        this.grabOffset = -10;
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.FIRE;
    }

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Luigi/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_02.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], SECT[1]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Luigi/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Luigi/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_07.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Luigi/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_05.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new ActionSpawnEntity(new short[]{9}, 99, LuigiFireball.class, this).setXOffset(15, true));
        action.addModifier(new ActionPlaySound("/Character Sounds/Luigi/neutralB.ogg", 9));
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionAttack(DMG[5], KNBK[5], PRTY[5], SECT[5]));
        action.addModifier(new ActionMovement(40, 0, 0, true));
        action.addModifier(new ActionFallWhenFinished());
        action.addModifier(new ActionPlaySound("/Character Sounds/Luigi/sideB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_14.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addModifier(new ActionPlaySound("/Character Sounds/Luigi/upB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/coin.ogg", -3));
        action.addModifier(new ActionPlaySound("/Common Sounds/ping.ogg", -2));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionAttack(DMG[7], KNBK[7], PRTY[7], SECT[7]));
        action.addModifier(new DownBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Luigi/downB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_02.ogg", -1));
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Luigi/finalSmash.ogg", 0));
        action.addModifier(new ActionFinalSmashFreeze());
        action.addModifier(new FinalSmashAction());
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {}
    
    public class DownBAction extends ActionModifier {
    	
        public DownBAction() {
        
        }

        @Override
        protected void onInit() {
        	//((ViolentEntity) this.action.entity).setFinalSmash(true);

        }

        @Override
        protected boolean onUpdate() {
            PlayableCharacter character = ((PlayableCharacter) this.action.entity);
            
            if (character.checkControl("Right", false)) {
                character.setPureXVelocity(30);
            } else if (character.checkControl("Left", false)) {
                character.setPureXVelocity(-30);
            } else {
                character.setPureXVelocity(0);
            }
            
            if (character.checkControl("Special", true)) {
                character.setPureYVelocity(15);
            }
            
            return false;
        }

        @Override
        protected boolean shouldFinish() {
        	if (!this.action.entity.isOnGround()) {
                ((PlayableCharacter) this.action.entity).setFalling(true);
                ((PlayableCharacter) this.action.entity).setVelocity(0, 0, false);
        	}
        	
            return true;
        }

        @Override
        protected void cleanUp() {

        }
    }
    
    public class FinalSmashAction extends ActionModifier {
    	
        public FinalSmashAction() {
        
        }

        @Override
        protected void onInit() {
        	LuigiKingBoo boo = new LuigiKingBoo((Luigi) this.action.entity);
        	BoundingBox box = Main.getGameState().getWorld().getStage().getCameraBounds();
        	
        	if (this.action.entity.getFacing()) {
        		boo.setPosition(box.getRightMidpoint().x - 10, box.getRightMidpoint().y);
        	} else {
        		boo.setPosition(box.getLeftMidpoint().x + 10, box.getLeftMidpoint().y);
        	}
        	
        	Main.getGameState().spawnEntity(boo);
        	
        	boo.setFacing(this.action.entity.getFacing());
        	boo.setScale(box.getDimensions().y / 28);
        }

        @Override
        protected boolean onUpdate() {
            return false;
        }

        @Override
        protected boolean shouldFinish() {
            return true;
        }

        @Override
        protected void cleanUp() {

        }
    }
}
