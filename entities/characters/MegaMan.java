package entities.characters;

import java.util.Map;

import physics.BoundingBox;
import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.LinearFunction;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionFinalSmashFreeze;
import entities.characters.actions.ActionHalfHop;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionModifyPhysicsModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionOverrideFalling;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.general.MegaManRushCoil;
import entities.projectiles.MegaManCrashBomb;
import entities.projectiles.MegaManLaser;
import entities.projectiles.MegaManMetalBlade;
import entities.projectiles.MegaManPellet;

/**
 * The character Mega Man within SSBS.
 *
 * @author Matthew Ludwig
 */
public class MegaMan extends PlayableCharacter {
	public static final float[] STAT = CharacterStats.MegaManStats;
	public static final float[] DMG = CharacterStats.MegaManDamage;
	public static final float[] KNBK = CharacterStats.MegaManKnockback;
	public static final float[] PRTY = CharacterStats.MegaManPriority;
	public static final float[] ISPD = CharacterStats.MegaManImageSpeed;
	public static final short[][] SECT = CharacterStats.MegaManSectionFrames;
	
    public MegaMan() {
        super("Mega Man", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        this.scale = (1.27f);
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.PLASMA;
    }

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionSpawnEntity(new short[]{1,3,5}, 99, MegaManPellet.class, this).setXOffset(30, true));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mega Man/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_12.ogg", -1));
        action.addModifier(new ActionOverrideFalling());
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], SECT[1]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mega Man/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_14.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        action.addModifier(new ActionOverrideFalling());
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionHalfHop());
        action.addModifier(new ActionPlaySound("/Character Sounds/Mega Man/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_15.ogg", -1));
        action.addModifier(new ActionOverrideFalling());
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionMovement(55*0.8f, 0, 0, true));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mega Man/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_03.ogg", -1));
        action.addModifier(new ActionOverrideFalling());
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new NeutralBAction(this));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mega Man/neutralB.ogg", 0));
        action.addModifier(new ActionOverrideFalling());
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionSpawnEntity(new short[]{0}, 1, MegaManCrashBomb.class, this));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mega Man/sideB1.ogg", 0));
        action.addModifier(new ActionOverrideFalling());
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addModifier(new ActionToggleAttribute(Attribute.SUPERARMOR));
        action.addModifier(new ActionSpawnEntity(new short[]{0}, 1, MegaManRushCoil.class));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mega Man/upB.ogg", 0));
        //action.addModifier(new ActionPlaySound("/Common Sounds/coin.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionAttack(DMG[7], KNBK[7], PRTY[7], SECT[7]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mega Man/downB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_01.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        action.addModifier(new ActionToggleAttribute(Attribute.BLOCKSPROJECTILES));
        action.addModifier(new ActionOverrideFalling());
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Mega Man/finalSmash.ogg", 0));
        action.addModifier(new ActionModifyPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION, true));
        action.addModifier(new ActionFinalSmashFreeze());
        action.addModifier(new FinalSmashAction());
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {}

    public class NeutralAAction extends ActionModifier {
        private float timer;
        private boolean firstPellet;
        private boolean secondPellet;
        private boolean thirdPellet;
        
        public NeutralAAction(MegaMan player) {
        }
        
        @Override
        protected void onInit() {
            this.timer = 0;
            this.firstPellet = false;
            this.secondPellet = false;
            this.thirdPellet = false;
        }

        @Override
        protected boolean onUpdate() {
        	this.timer += Utilities.lockedTPF;
        	
        	if (this.timer >= .83f) {
         		if (!this.thirdPellet) {
         			this.spawnPellet();
         			this.thirdPellet = true;
         		}
         	} else if (this.timer >= .47f) {
        		if (!this.secondPellet) {
        			this.spawnPellet();
        			this.secondPellet = true;
        		}
        	} else if (this.timer >= .11f) {
        		if (!this.firstPellet) {
        			this.spawnPellet();
        			this.firstPellet = true;
        		}
        	}
        	
            return false;
        }
        
        private void spawnPellet() {
        	MegaManPellet star = new MegaManPellet((MegaMan) this.action.entity);
            float distance = Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * this.action.entity.getRealBounds().getDimensions().y / 2;
            star.setPosition(this.action.entity.getRealBounds().getExactCenter().x + distance, this.action.entity.getRealBounds().getExactCenter().y);
            Main.getGameState().spawnEntity(star);
        }

        @Override
        protected boolean shouldFinish() {
        	return true;
        }

        @Override
        protected void cleanUp() {

        }
    }
    
    public class NeutralBAction extends ActionModifier {
        private MegaMan player;
        private MegaManMetalBlade blade;
        private boolean spawned;
        
        private float timer;
        
        public NeutralBAction(MegaMan player) {
            this.player = player;
            this.blade = null;
            this.spawned = false;
            
            this.timer = 0;
        }
        
        @Override
        protected void onInit() {
            if (this.blade != null && this.blade.isDead()) {
                this.blade = null;
            }
            
            this.spawned = false;
            this.timer = 0;
        }

        @Override
        protected boolean onUpdate() {
            if (!this.spawned) {
                if (this.blade == null) {
                    this.blade = new MegaManMetalBlade(this.player);
                    this.blade.setPosition(this.player.getRealBounds().getExactCenter().x, this.player.getRealBounds().getExactCenter().y);
                    Main.getGameState().spawnEntity(this.blade);
                    
                    this.spawned = true;
                } else {
                    this.timer = 1;
                    return true;
                }
            } else if (this.timer < .156f) {
                if (this.player.checkControl("Up", false)) {
                    this.blade.setDirection(90);
                }
                
                if (this.player.checkControl("Down", false)) {
                    this.blade.setDirection(-90);
                }
                
                if (this.player.checkControl("Left", false)) {
                    this.blade.setDirection(180);
                } 
                
                if (this.player.checkControl("Right", false)) {
                    this.blade.setDirection(0);
                }
                
                this.timer += Utilities.lockedTPF;
            }
            
            return false;
        }

        @Override
        protected boolean shouldFinish() {
            return this.timer > .13f;
        }

        @Override
        protected void cleanUp() {

        }
    }
    
	public class FinalSmashAction extends ActionModifier {
		private static final float CONSTANT = 50;
		
        private float moveTimer;
        private float damageTimer;
        private MegaManLaser[] lasers;
        private Angle angle;
		
        public FinalSmashAction() {
        
        }

        @Override
        protected void onInit() {
        	this.moveTimer = 6f;
        	this.damageTimer = 0;
        	this.angle = new Angle(0);
        	Main.getGameState().setMaxCameraZoom(true);
        	this.action.entity.addToPosition(0, -(this.action.entity.getDimensions().y - (6 * this.action.entity.getDefaultHeight())) / 2);
        }
        
        private void spawnLaser() {
        	int x = Math.round(Main.getGameState().getWorld().getStage().getWorldBounds().getDimensions().x / CONSTANT);
            lasers = new MegaManLaser[x];
            
            for (int count = 0; count < lasers.length; count++) {
            	MegaManLaser laser = new MegaManLaser((MegaMan) this.action.entity);
                Main.getGameState().spawnEntity(laser);
                
                float normalXOffset = (this.action.entity.getFacing() ? 5 : -10) + (laser.getRealBounds().getDimensions().x / 2) + (this.action.entity.getRealBounds().getDimensions().x / 2);
                float normalYOffset = -12 - (laser.getRealBounds().getDimensions().y / 2);
                float xOffset = (count * CONSTANT * this.angle.cos());
                float yOffset = (this.angle.sin() * count * CONSTANT) + (this.angle.sin() * laser.getRealBounds().getDimensions().x / 2);
                laser.setPosition(
                        this.action.entity.getRealBounds().getExactCenter().x + Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * (normalXOffset + xOffset), 
                        this.action.entity.getRealBounds().getExactCenter().y + normalYOffset + yOffset);
                laser.rotateUpTo(this.angle);
                
                lasers[count] = laser;
            }
        }

        @Override
        protected boolean onUpdate() {
        	if (this.action.getCurrentFrameNumber() == 15) {
        		if (this.moveTimer == 6f) {
        			this.action.freezeFrame(true);
        			this.spawnLaser();
        		} 
    			
        		this.updateLaser();
        	}
        	
        	return this.moveTimer <= 0;
        }
        
        private void updateLaser() {
            this.moveTimer -= Utilities.lockedTPF;
            
            int count = 0;
            
            for (MegaManLaser laser : this.lasers) {
            	float normalXOffset = (this.action.entity.getFacing() ? 5 : -10) + (laser.getRealBounds().getDimensions().x / 2) + (this.action.entity.getRealBounds().getDimensions().x / 2);
                float normalYOffset = -12 - (laser.getRealBounds().getDimensions().y / 2);
                float xOffset = (count * CONSTANT * this.angle.cos());
                float yOffset = (this.angle.sin() * count * CONSTANT) + (this.angle.sin() * laser.getRealBounds().getDimensions().x / 2);
                laser.setPosition(
                        this.action.entity.getRealBounds().getExactCenter().x + Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * (normalXOffset + xOffset), 
                        this.action.entity.getRealBounds().getExactCenter().y + normalYOffset + yOffset);
                laser.rotateUpTo(this.angle);
                count++;
            }
                        
            if (this.damageTimer >= .18f) {
                Vector2f lowerLeft = new Vector2f(
                        Math.min(this.lasers[0].getRealBounds().getExactCenter().x, this.lasers[count - 1].getRealBounds().getExactCenter().x),
                        Math.min(this.lasers[0].getRealBounds().getExactCenter().y, this.lasers[count - 1].getRealBounds().getExactCenter().y));
                Vector2f upperRight = new Vector2f(
                        Math.max(this.lasers[0].getRealBounds().getExactCenter().x, this.lasers[count - 1].getRealBounds().getExactCenter().x),
                        Math.max(this.lasers[0].getRealBounds().getExactCenter().y, this.lasers[count - 1].getRealBounds().getExactCenter().y));
                BoundingBox box = new BoundingBox(lowerLeft, upperRight.subtract(lowerLeft));

                for (PhysicsEntity entity : Main.getGameState().getWorld().getPhysicsList()) {
                    if (box.isInBounds(entity.getRealBounds()) && entity instanceof ViolentEntity) {
                        LinearFunction f1 = new LinearFunction(this.lasers[0].getRealBounds().getUpperMidpoint(), this.lasers[count - 1].getRealBounds().getUpperMidpoint());
                        LinearFunction f2 = new LinearFunction(this.lasers[0].getRealBounds().getLowerMidpoint(), this.lasers[count - 1].getRealBounds().getLowerMidpoint());
                        
                        float y1 = f1.getY(entity.getRealBounds().getLeftMidpoint().x - this.lasers[0].getRealBounds().getExactCenter().x);
                        float y2 = f1.getY(entity.getRealBounds().getRightMidpoint().x - this.lasers[0].getRealBounds().getExactCenter().x);
                        float y3 = f2.getY(entity.getRealBounds().getLeftMidpoint().x - this.lasers[0].getRealBounds().getExactCenter().x);
                        float y4 = f2.getY(entity.getRealBounds().getRightMidpoint().x - this.lasers[0].getRealBounds().getExactCenter().x);
                        
                        boolean checkY1 = (y1 > entity.getRealBounds().getLowerMidpoint().y) && (y1 < entity.getRealBounds().getUpperMidpoint().y);
                        boolean checkY2 = (y2 > entity.getRealBounds().getLowerMidpoint().y) && (y2 < entity.getRealBounds().getUpperMidpoint().y);
                        boolean checkY3 = (y3 > entity.getRealBounds().getLowerMidpoint().y) && (y3 < entity.getRealBounds().getUpperMidpoint().y);
                        boolean checkY4 = (y4 > entity.getRealBounds().getLowerMidpoint().y) && (y4 < entity.getRealBounds().getUpperMidpoint().y);
                        
                        Angle damageAngle = new Angle(this.action.entity.getFacing() ? 0 : 180);
                        
                        if (checkY1 || checkY2 || checkY3 || checkY4) {
                            ((ViolentEntity) entity).addToDamage(MegaMan.DMG[8], MegaMan.KNBK[8], 1.0f, damageAngle, false, false, false, false, 0, this.lasers[0]);
                        }
                    }
                }
                
                this.damageTimer -= .18f;
            } else {
                this.damageTimer += Utilities.lockedTPF;
            }
                
            if (((PlayableCharacter) this.action.entity).checkControl("Up", false)) {
                this.angle.add(5 * Utilities.lockedTPF);
            } else if (((PlayableCharacter) this.action.entity).checkControl("Down", false)) {
                this.angle.subtract(5 * Utilities.lockedTPF);
            }
        }

        @Override
        protected boolean shouldFinish() {
            return true;
        }

        @Override
        protected void cleanUp() {
        	Main.getGameState().setMaxCameraZoom(false);
        	this.action.entity.addToPosition(0, (this.action.entity.getDimensions().y - (6 * this.action.entity.getDefaultHeight())) / 2);

            for (MegaManLaser laser : this.lasers) {
                laser.setDead(true);
            }
            
			this.action.freezeFrame(false);
        }
    }
}
