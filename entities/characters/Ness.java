package entities.characters;

import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionFinalSmashFreeze;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionReflect;
import entities.characters.actions.ActionReflect.ReflectType;
import entities.characters.actions.ActionTeleport;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.projectiles.NessFlash;
import entities.projectiles.NessRockin1;
import entities.projectiles.NessRockin2;
import entities.projectiles.NessRockin3;

import java.util.Map;

import com.jme3.audio.AudioNode;
import com.jme3.math.Vector2f;

import physics.BoundingBox;
import physics.CollisionInfo;

/**
 * The character Ness within SSBS.
 *
 * @author Matthew Ludwig
 */
public class Ness extends PlayableCharacter {
	public static final float[] STAT = CharacterStats.NessStats;
	public static final float[] DMG = CharacterStats.NessDamage;
	public static final float[] KNBK = CharacterStats.NessKnockback;
	public static final float[] PRTY = CharacterStats.NessPriority;
	public static final float[] ISPD = CharacterStats.NessImageSpeed;
	public static final short[][] SECT = CharacterStats.NessSectionFrames;
	
	private boolean isFranklin;
    public Ness() {
    	super("Ness", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]),
				STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        this.scale = 0.72f;
        isFranklin = false;
        setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
        grabHitbox = 25;
        this.grabOffset = -65;
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.PLASMA;
    }
    
    public void onCollideWith(PhysicsEntity object, CollisionInfo info) {
		if (!isFranklin || !(object instanceof PlayableCharacter) || !((ViolentEntity)(object)).getEnergy()){
			super.onCollideWith(object, info);
		} else {
			this.setAttackCapability(((ViolentEntity) object).getDamageDealt() * 2, ((ViolentEntity) object).getKnockback() * 2, 999f, true);
			((ViolentEntity) object).setDamageDealt(0);
			((ViolentEntity) object).setKnockbackDealt(0);
			isFranklin = false;
		}
	}
    
    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ness/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_02.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], SECT[1]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ness/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/ping.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ness/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_00.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ness/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_03.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Ness/neutralB.ogg", 7));
        action.addAnimation(1, "neutralB2");
        action.addAnimation(2, "neutralB3");
        action.addModifier(new NeutralBAction());
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Ness/sideB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_05.ogg", -1));
        action.addModifier(new SideBAction());
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB1", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addAnimation(1, "upB2");
        action.addModifier(new ActionTeleport(0f));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ness/upB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionAttack(DMG[7], KNBK[7], PRTY[7], true, SECT[7]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ness/downB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/psychic_01.ogg", -1));
        action.addModifier(new ActionReflect(2f, false, ReflectType.ENERGY));
        action.addModifier(new DownBAction());
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new ActionAttack(DMG[8], 999f, PRTY[8], true));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ness/finalSmash.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/electric_01.ogg", -1));
        action.addModifier(new ActionPlaySound("/Common Sounds/ping.ogg", -2));
        //action.addModifier(new ActionFinalSmashFreeze());
        action.addModifier(new ActionToggleAttribute(Attribute.PARALYZE));
        action.addModifier(new FinalSmashAction());
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {}
    
    public class NeutralBAction extends ActionModifier {
        float counter;
        int frame;
        int anim;
        boolean shot;
        private AudioNode audio;
        boolean b;
        
    	public NeutralBAction() {
    		audio = Utilities.getCustomLoader().getAudioNode("/Character Sounds/Ness/neutralB.ogg");
        }
        
        @Override
        protected void onInit() {
            counter = 0f;
            frame = 0;
            anim = 0;
            shot = false;
            b = ((PlayableCharacter) this.action.entity).checkControl("Special", false);
        }

        @Override
        protected boolean onUpdate() {
        	frame = this.action.getCurrentFrameNumber();
        	if (b){
        		b = ((PlayableCharacter) this.action.entity).checkControl("Special", false);
        	}
        	if (frame >= 4 && b && anim == 0){
        		this.action.setCurrentFrame(4);
        		counter += Utilities.lockedTPF;
        	} if (frame >= 0 && b && anim > 0){
        		this.action.setCurrentFrame(0);
        		counter += Utilities.lockedTPF;
        	} if (counter > 0.67f && anim == 0 && b){
        		anim = 1;
        		this.action.setCurrentAnimation(1);
        		this.action.setCurrentFrame(0);
        	} if (counter > 1.67f && anim == 1 && b){
        		anim = 2;
        		this.action.setCurrentAnimation(2);
        		this.action.setCurrentFrame(0);
        	}
        	if (!shot && !b){
        		float x = this.action.entity.getPosition().x;
    			float y = this.action.entity.getPosition().y;
        		if (anim == 2 && frame >= 3){
        			shot = true;
        			this.audio.playInstance();
        			NessRockin3 rockin3 = new NessRockin3((Ness) this.action.entity);
					rockin3.setPosition(x, y-48);
					Main.getGameState().spawnEntity(rockin3);
        		} else if (anim == 1 && frame >= 3){
        			shot = true;
        			this.audio.playInstance();
        			NessRockin2 rockin2 = new NessRockin2((Ness) this.action.entity);
					rockin2.setPosition(x, y);
					Main.getGameState().spawnEntity(rockin2);
        		} else if (frame >= 7) {
        			shot = true;
        			NessRockin1 rockin = new NessRockin1((Ness) this.action.entity);
					rockin.setPosition(x, y+4);
					Main.getGameState().spawnEntity(rockin);
        		}
        	}
            return false;
        }

        @Override
        protected boolean shouldFinish() {
            return true;
        }

        @Override
        protected void cleanUp() {
            
        }
    }
    
    public class SideBAction extends ActionModifier {
    	private NessFlash flash;
    	private boolean gone, done;
    	private float counter;
    	
        public SideBAction() {
            
        }
        
        @Override
        protected void onInit() {
        	float x = this.action.entity.getPosition().x;
			float y = this.action.entity.getPosition().y;
        	flash = new NessFlash((Ness) this.action.entity);
        	flash.setPosition(x, y+5);
			Main.getGameState().spawnEntity(flash);
			gone = false;
			done = false;
			counter = 0f;
        }

        @Override
        protected boolean onUpdate() {
        	if (flash.isDead() && !gone){
        		gone = true;
        	}
        	if (gone){
        		counter += Utilities.lockedTPF;
        	}
        	if (counter > 0.5f){
        		done = true;
        		return true;
        	}
            return false;
        }

        @Override
        protected boolean shouldFinish() {
        	return done;
        }

        @Override
        protected void cleanUp() {
            
        }
    }
    
    
    public class DownBAction extends ActionModifier {
        public DownBAction() {
        
        }

        @Override
        protected void onInit() {
            isFranklin = true;
        }

        @Override
        protected boolean onUpdate() {
            return false;
        }

        @Override
        protected boolean shouldFinish() {
            return true;
        }

        @Override
        protected void cleanUp() {
            isFranklin = false;
            //((PlayableCharacter)this.action.entity).setFinalSmash(true);
        }
    }
    
    public class FinalSmashAction extends ActionModifier {
        public FinalSmashAction() {
        
        }

        @Override
        protected void onInit() {
        	
        }

        @Override
        protected boolean onUpdate() {
        	if (this.action.getCurrentFrameNumber() >= 26 && ((ViolentEntity) this.action.entity).getParalyze()){
        		((ViolentEntity) this.action.entity).setParalyze(false);
        		((ViolentEntity) this.action.entity).setAttackCapability(DMG[8], KNBK[8], PRTY[8], true);
        	}
            return false;
        }

        @Override
        protected boolean shouldFinish() {
            return true;
        }

        @Override
        protected void cleanUp() {
        	
        }
    }
}
