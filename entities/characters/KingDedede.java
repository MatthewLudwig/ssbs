package entities.characters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import physics.PhysicsModifier;

import com.jme3.audio.AudioNode;
import com.jme3.math.Vector2f;
import com.jme3.scene.Node;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.TransformingCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionSetFacing;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.ActionWaitTillControl;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.projectiles.KingDededeBreathStar;
import entities.projectiles.KingDededeGordo;
import entities.projectiles.KingDededeJumpStar;

/**
 * The character Samus within SSBS.
 *
 * @author Matthew Ludwig
 */
public class KingDedede extends TransformingCharacter {
    public static Random gen = new Random();
    public static final float[] STAT = CharacterStats.KingDededeStats;
	public static final float[] DMG = CharacterStats.KingDededeDamage;
	public static final float[] KNBK = CharacterStats.KingDededeKnockback;
	public static final float[] PRTY = CharacterStats.KingDededePriority;
	public static final float[] ISPD = CharacterStats.KingDededeImageSpeed;
	public static final short[][] SECT = CharacterStats.KingDededeSectionFrames;
	
    public KingDedede() {
        super("King Dedede", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
        this.transformationTimer = 0.0f;
        this.scale = .83f;
        this.grabHitbox = 25;
        this.grabOffset = -20;
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.HAMMER;
    }
    
    @Override
    protected Map<String, CharacterAction> setUpSecondActionMap() {
    	Map<String, CharacterAction> map = new HashMap<String, CharacterAction>();
        
        CharacterAction firstAction = new CharacterAction(this, "finalSmash2", StayingPower.NONE, ISPD[8]);
        map.put("Stand", firstAction);
        
        CharacterAction secondAction = new CharacterAction(this, "finalSmash2", StayingPower.BARELY, ISPD[8]);
        secondAction.addModifier(new ActionSetFacing(true));
        secondAction.addModifier(new ActionMovement(25f, 0f, 0f, true));
        secondAction.addModifier(new ActionWaitTillControl("Right", false, false));
        map.put("Walk Right", secondAction);
        map.put("Sprint Right", secondAction);
        
        CharacterAction thirdAction = new CharacterAction(this, "finalSmash2", StayingPower.BARELY, ISPD[8]);
        thirdAction.addModifier(new ActionSetFacing(false));
        thirdAction.addModifier(new ActionMovement(25f, 0f, 0f, true));
        thirdAction.addModifier(new ActionWaitTillControl("Left", false, false));
        map.put("Walk Left", thirdAction);
        map.put("Sprint Left", thirdAction);
        
        CharacterAction jumpAction = new CharacterAction(this, "finalSmash3", StayingPower.BARELY, ISPD[8]);
        jumpAction.addModifier(new ActionMovement(25f, 90f, 0f, false)).addModifier(new ActionToggleAttribute(Attribute.VECTORING)).addModifier(new ActionWaitTillOnGround());
        map.put("Jump 1", jumpAction);
        
        CharacterAction fourthAction = new CharacterAction(this, "finalSmash4", StayingPower.BARELY, ISPD[8]);
        fourthAction.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/finalSmash2.ogg", 0));
        fourthAction.addModifier(new ActionAttack(DMG[8], KNBK[8], PRTY[8], true, SECT[8]));
        fourthAction.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        map.put("Neutral A", fourthAction);
        map.put("Neutral B", fourthAction);
        
        CharacterAction fifthAction = new CharacterAction(this, "finalSmash5", StayingPower.EXTREME, ISPD[8]);
        fifthAction.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/finalSmash3.ogg", 0));
        fifthAction.addModifier(new ActionTransform(0));
        map.put("Transform", fifthAction);
        
        return map;
    }
    
    private void finishSucking() {
        if (this.currentAction.equals(this.actionMap.get("Neutral B"))) {
            this.forceCurrentAction(this.actionMap.get(this.getDefaultAction()));
        }
    }

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        action.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_02.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], SECT[1]));
        action.addModifier(new ActionMovement(20f, 0f, 0, true));
        action.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_01.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_02.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new ActionWaitTillControl("Special", false, false));
        action.addModifier(new SuckingAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/neutralB.ogg", 0));
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionSpawnEntity(new short[]{0}, 1, KingDededeGordo.class, this));
        action.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/upA.ogg", 0));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addModifier(new ActionToggleAttribute(Attribute.SUPERARMOR));
        action.addModifier(new UpBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/upB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_08.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionAttack(DMG[7], KNBK[7], PRTY[7], SECT[7]));
        action.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/downB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1));
        //action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/finalSmash1.ogg", 0));
        action.addModifier(new ActionTransform(12));
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {
        actionMap.get("Jump 2").addModifier(new JumpingAction());
    }

    public class JumpingAction extends ActionModifier {
        private AudioNode audio;
        private int numberOfJumpsRemaining;

        public JumpingAction() {
            audio = Utilities.getCustomLoader().getAudioNode("/Character Sounds/King Dedede/jump2.ogg");
            this.numberOfJumpsRemaining = 4;
        }

        @Override
        public void onInit() {
            this.numberOfJumpsRemaining = 4;
        }

        @Override
        public boolean onUpdate() {
            if (((PlayableCharacter) this.action.entity).checkControl("Up", true) && this.numberOfJumpsRemaining > 0) {
                this.action.entity.setYVelocity(25 - (12.5f / this.numberOfJumpsRemaining), true);
                this.action.setCurrentFrame(0);
                this.numberOfJumpsRemaining--;
                this.audio.playInstance();
            }

            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }

    public class SuckingAction extends ActionModifier {
        private List<KingDededeBreathStar> spawnedStars;
        private List<PhysicsEntity> effectedEntities;
        private float updateCount;
        
        private float moveTimer;
        
        public SuckingAction() {
            this.spawnedStars = new LinkedList<KingDededeBreathStar>();
            this.effectedEntities = new ArrayList<PhysicsEntity>();
            this.updateCount = 0;
            this.moveTimer = 2;
        }

        @Override
        public void onInit() {
            this.updateCount = 0;
            this.moveTimer = 2;
        }

        @Override
        public boolean onUpdate() {
            if (gen.nextInt(4) == 3) {
                this.spawnStar();
            }
            
            boolean attackThisFrame = false;
            
            if (this.updateCount >= .25) {
                attackThisFrame = true;
                this.updateCount -= .25;
            } else {
                this.updateCount += Utilities.lockedTPF;
            }
            
            this.effectedEntities.clear();
            
            for (KingDededeBreathStar star : this.spawnedStars) {
                PhysicsEntity target = star.getTarget();
                
                if (target != null && !this.effectedEntities.contains(target)) {
                    Vector2f endingPosition = this.action.entity.getAppropriateBounds().getLowerMidpoint().subtract(Utilities.getGeneralUtility().getBooleanAsSign(!this.action.entity.getFacing()) * 50, 0);
                    
                    if (endingPosition.distance(target.getAppropriateBounds().getLowerMidpoint()) > 5) {
                        Angle angle = Utilities.getPhysicsUtility().calculateCorrectAngle(endingPosition, target.getAppropriateBounds().getLowerMidpoint());
                        target.addToPosition((float) (7 * angle.cos()), (float) (7 * angle.sin()));
                    } else {
                        target.setPosition(endingPosition.x, endingPosition.y);
                    }
                    
                    if (attackThisFrame && target instanceof ViolentEntity) {
                        ((ViolentEntity) target).addToDamage(DMG[4], KNBK[4], PRTY[4], new Angle(0), false, false, false, false, 0, star);
                    }
                    
                    if (target instanceof PlayableCharacter) {
                        if (((PlayableCharacter) target).checkControl("*", true)) {
                            this.moveTimer -= .07;
                        }
                    }
                    
                    this.effectedEntities.add(target);
                }
            }
            
            if (this.moveTimer <= 0) {
                ((KingDedede) this.action.entity).finishSucking();
                return true;
            } else {
                this.moveTimer -= Utilities.lockedTPF;
                return false;
            }
        }
        
        public void spawnStar() {
            if (this.spawnedStars.size() > 100) {
                this.cleanUpStars();
            }
            
            KingDededeBreathStar star = new KingDededeBreathStar((KingDedede) this.action.entity);
            Angle randomAngle = new Angle(gen.nextInt(60) - 30);
            int distance = Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * (40 + gen.nextInt(60));
            star.setPosition(this.action.entity.getAppropriateBounds().getExactCenter().x + (distance * randomAngle.cos()), this.action.entity.getAppropriateBounds().getExactCenter().y + (distance * randomAngle.sin()));
            Main.getGameState().spawnEntity(star);
            this.spawnedStars.add(star);
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
            this.cleanUpStars();
        }
        
        public void cleanUpStars() {
            for (PhysicsEntity entity : this.spawnedStars) {
                entity.setDead(true);
            }
            
            this.spawnedStars.clear();
        }
    }
    
    public class UpBAction extends ActionModifier {
        private boolean starsCreated;
        
        public UpBAction() {
            this.starsCreated = false;
        }

        @Override
        public void onInit() {
            this.starsCreated = false;
        }

        @Override
        public boolean onUpdate() {
            if (this.action.getCurrentFrameNumber() >= 6) {
                if (!this.action.entity.isOnGround()) {
                    if (this.action.entity.getVelocity().y > 0) {
                        this.action.setCurrentFrame(6);
                    }
                } else {
                    if (!this.starsCreated) {
                        KingDededeJumpStar leftStar = new KingDededeJumpStar((KingDedede) this.action.entity);
                        leftStar.setPosition(this.action.entity.getAppropriateBounds().getLowerMidpoint().x - 40, this.action.entity.getAppropriateBounds().getLowerMidpoint().y);
                        Main.getGameState().spawnEntity(leftStar);
                        
                        KingDededeJumpStar rightStar = new KingDededeJumpStar((KingDedede) this.action.entity);
                        rightStar.setPosition(this.action.entity.getAppropriateBounds().getLowerMidpoint().x + 40, this.action.entity.getAppropriateBounds().getLowerMidpoint().y);
                        Main.getGameState().spawnEntity(rightStar);
                        
                        this.starsCreated = true;
                    }
                }
            }
            
            if (this.action.entity.getVelocity().y < 0) {
                this.action.setCurrentFrame(7);
                ((ViolentEntity) this.action.entity).setBury(true);
            }

            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
        	((ViolentEntity) this.action.entity).setBury(false);
        }
    }
}
