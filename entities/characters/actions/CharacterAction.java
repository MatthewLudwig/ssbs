package entities.characters.actions;

import java.util.LinkedList;
import java.util.List;

import physics.ReversibleImage;
import engine.utility.Utilities;
import entities.PhysicsEntity;

/**
 * Basic CharacterAction that can have modifiers added to it to increase it's complexity.
 * Starts off as just a simple animation.
 *
 * @author Matthew Ludwig
 */
public class CharacterAction {    
    public final PhysicsEntity entity;
    public final ReversibleImage[][] animations;
    public final String actionName;
    public final StayingPower stayingPower;
    
    private List<ActionModifier> modifierList;
    
    public float updateLimit;

    private int currentAnimation;
    private int currentFrame;
    private float updateLimiter;
    private boolean frameChange;
    private boolean isFrameFrozen;
    protected boolean doesOverrideFalling;

    public CharacterAction(PhysicsEntity entity, String actionName, StayingPower stayingPower, float updateLimit) {
        this.entity = entity;
        this.animations = new ReversibleImage[20][];
        this.animations[0] = Utilities.getCustomLoader().getSpriteSheet(entity.getName()).getAnimation(actionName);
        this.actionName = actionName;
        this.stayingPower = stayingPower;
        
        this.modifierList = new LinkedList<ActionModifier>();

        this.currentAnimation = 0;
        this.currentFrame = 0;
        this.updateLimiter = 0;
        this.updateLimit = scaleUpdateLimit(updateLimit);
        this.frameChange = true;
    }
    
    public final CharacterAction addModifier(ActionModifier modifier) {
        modifier.setCharacterAction(this);
        this.modifierList.add(modifier);
        return this;
    }
    
    public float scaleUpdateLimit(float updateLimit){
    	return .77f / (updateLimit * this.entity.getImageSpeed());
    }
    
    public final ActionOverrideFalling doesOverrideFalling() {
    	if (this.doesOverrideFalling) {
    		for (ActionModifier m : this.modifierList) {
    			if (m instanceof ActionOverrideFalling) {
    				return (ActionOverrideFalling) m;
    			}
    		}
    	}
    	
    	return null;
    }
    
    public final void addAnimation(int slot, String animationName) {
        this.animations[slot] = Utilities.getCustomLoader().getSpriteSheet(this.entity.getName()).getAnimation(animationName);
    }
    
    public final void setCurrentAnimation(int number) {
    	if (this.currentAnimation != number) {
    		this.currentAnimation = number;
    		this.updateLimiter = 0;
    		this.currentFrame = 0;
    		this.frameChange = true;
    	}
    }
    
    public final int getCurrentAnimationNumber() {
    	return this.currentAnimation;
    }
    
    public final void setCurrentFrame(int frameNumber) {
        this.currentFrame = frameNumber;
        this.updateLimiter = 0;
        this.frameChange = true;
    }
    
    public final void freezeFrame(boolean value) {
    	this.isFrameFrozen = value;
    }

    public final boolean hasFrameChanged() {
        return this.frameChange;
    }
    
    public final int getCurrentFrameNumber() {
        return this.currentFrame;
    }

    public final ReversibleImage getFrame() {
        return this.animations[this.currentAnimation][this.currentFrame >= this.animations[this.currentAnimation].length ? this.animations[this.currentAnimation].length - 1 : this.currentFrame];
    }
    
    public int getTotalNumberOfFrames(){
    	return this.animations[this.currentAnimation].length;
    }
    
    public int getStayingPower() {
        return this.stayingPower.ordinal();
    }
    /**
     * Function that is called when the action is added as an entity's current
     * action.
     * 
     * Will call the onInit() method.
     */
    public final void init() {
        this.setCurrentAnimation(0);
        
        this.currentFrame = 0;
        this.frameChange = true;

        for (ActionModifier modifier : this.modifierList) {
            modifier.onInit();
        }
    }

    /**
     * Function for updating the action. Will return true when finished.
     */
    public final boolean update() {
    	if (!this.isFrameFrozen) {
    		this.updateLimiter += Utilities.lockedTPF * 10;
    	}

        if (this.updateLimiter >= this.updateLimit) {
            this.updateLimiter -= this.updateLimit;
            this.currentFrame++;
            this.frameChange = true;
        } else {
            this.frameChange = false;
        }
        
        boolean returnValue = false;
        
        for (ActionModifier modifier : this.modifierList) {
            if (modifier.onUpdate()) {
                returnValue = true;
            }
        }

        return returnValue || this.currentFrame >= this.animations[this.currentAnimation].length;
    }

    /**
     * Function called upon this action finishing which happens when the update method returns true.
     * Will return true if the function is done and should be replaced by the default action.
     * Will return false if this action should stay and be repeated for another cycle.
     * 
     * @param force true if the action should be forced to run the {@link #cleanUp()} method.
     */
    public final boolean finish(boolean force) {
        this.updateLimiter = 0;
        this.currentFrame = 0;
        this.frameChange = true;
        
        boolean shouldFinish = true;
        
        for (ActionModifier modifier : this.modifierList) {
            if (!modifier.shouldFinish()) {
                shouldFinish = false;
            }
        }
        
        if (force || shouldFinish) {
            for (ActionModifier modifier : this.modifierList) {
                modifier.cleanUp();
            }
            
            return true;
        }
        
        return false;
    }
    
    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof CharacterAction) {
            CharacterAction comparedTo = (CharacterAction) o;

            boolean entity = this.entity.equals(comparedTo.entity);
            boolean name = this.actionName.equals(comparedTo.actionName);
            boolean list = this.modifierList.equals(comparedTo.modifierList);
            
            return entity && name && list;
        } else {
            return false;
        }
    }
}
