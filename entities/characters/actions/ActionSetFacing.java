package entities.characters.actions;

import entities.PhysicsEntity;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Set the facing of a {@link entities.PhysicsEntity}.
 *
 * @author Matthew
 */
public class ActionSetFacing extends ActionModifier {
    private boolean newFacing;
    
    public ActionSetFacing(boolean newFacing) {
        this.newFacing = newFacing;
    }
    
    @Override
    public void onInit() {
        ((PhysicsEntity) this.action.entity).setFacing(newFacing);
    }

    @Override
    public boolean onUpdate() {      
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {}
}
