package entities.characters.actions;

import com.jme3.math.Vector2f;


/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Kill the associated entity upon finishing the animation.
 *
 * @author Matthew
 */
public class ActionHurt extends ActionModifier {
	private Vector2f prevSpeed;
    public ActionHurt() {
    	prevSpeed = new Vector2f(0,0);
    }

    @Override
    public void onInit() {
    }

    @Override
    public boolean onUpdate() {
    	if (this.action.entity.getVelocity().y != 0)
    		prevSpeed.setY(this.action.entity.getVelocity().y);
    	if (this.action.entity.getVelocity().x != 0)
    		prevSpeed.setY(this.action.entity.getVelocity().x);
        if (this.action.entity.hasCollidedWithObstacle(true)) {
            this.action.entity.setPureXVelocity(-prevSpeed.x*0.8f);
            //this.action.entity.addToPosition(-prevSpeed.x/Math.abs(prevSpeed.x), prevSpeed.y/Math.abs(prevSpeed.y));
        } else if (this.action.entity.hasCollidedWithObstacle(false)) {
            this.action.entity.setPureYVelocity(-prevSpeed.y*0.8f);
            //this.action.entity.addToPosition(prevSpeed.x/Math.abs(prevSpeed.x), -prevSpeed.y/Math.abs(prevSpeed.y));
        }
        
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
    	prevSpeed = new Vector2f(0,0);
    }
}
