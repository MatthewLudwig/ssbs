package entities.characters.actions;

import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Set the facing of a {@link entities.PhysicsEntity}.
 *
 * @author Matthew
 */
public class ActionStallThenFall extends ActionModifier {
    private float maxCount;
    private float count;
    private boolean isComplete;
    
    public ActionStallThenFall(float secondsToStall) {
        this.isComplete = false;
        this.maxCount = secondsToStall;
        this.count = 0;
    }
    
    @Override
    public void onInit() {
        this.isComplete = false;
        count = 0;
    }

    @Override
    public boolean onUpdate() {      
        if (count > maxCount) {
            this.action.entity.gravityModifier = 3;
        } else {
        	this.action.entity.gravityModifier = 0.33f;
        }
        count += Utilities.lockedTPF;
        
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
        this.action.entity.gravityModifier = 1;
    }
}
