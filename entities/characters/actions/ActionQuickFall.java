package entities.characters.actions;

import engine.Main;
import entities.PhysicsEntity;
import entities.PlayableCharacter;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Set the facing of a {@link entities.PhysicsEntity}.
 *
 * @author Matthew
 */
public class ActionQuickFall extends ActionModifier {
    
    private boolean isComplete;
    
    public ActionQuickFall() {
        this.isComplete = false;
    }
    
    @Override
    public void onInit() {
        this.isComplete = false;
    }

    @Override
    public boolean onUpdate() {      
        if (!this.isComplete && ((PlayableCharacter) this.action.entity).checkControl("Down", false)) {
            this.action.entity.gravityModifier = 2;
            this.isComplete = true;
        }
        
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
        this.action.entity.gravityModifier = 1;
    }
}
