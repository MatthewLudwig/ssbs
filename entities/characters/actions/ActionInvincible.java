package entities.characters.actions;

import entities.PlayableCharacter;

/**
 * 
 * 
 * @author Matthew
 */
public class ActionInvincible extends ActionModifier {
	float initialSeconds;
	
    public ActionInvincible(float seconds) {
        initialSeconds = seconds;
    }
    
    @Override
    public void onInit() {
        if (this.action.entity instanceof PlayableCharacter){
        	//System.out.println("eh?");
        	((PlayableCharacter)this.action.entity).setInvincibility(initialSeconds);
        }
    }

    @Override
    public boolean onUpdate() {
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
    	
    }
}
