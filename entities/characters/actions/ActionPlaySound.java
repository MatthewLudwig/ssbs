package entities.characters.actions;

import com.jme3.audio.AudioNode;
import engine.Main;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import entities.ViolentEntity;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Play a sound on the specified frame.
 *
 * @author Matthew
 */
public class ActionPlaySound extends ActionModifier {    
    private AudioNode sound;
    private int frameToPlayOn;
    private int animationToPlayOn;
    private boolean hasBeenPlayed;
    private float cooldown;
    
    /**
     * @param path The path to the audio file that will be played.
     * @param frame The frame number that the sound effect should play on, -1 if it is a hit sound effect, -2 if it is a sweet-spot hit sound effect, -3 if it is a sour-spot hit sound effect.
     */
    public ActionPlaySound(String path, int frame, float volume){
    	this(path, frame, 0);
    	this.sound.setVolume((frame >= 0 ? Main.getGameSettings().getVoicesVolume() : Main.getGameSettings().getSFXVolume())*volume);
    }
    
    public ActionPlaySound(String path, int frame) {
    	this(path, frame, 0);
    }
    
    public ActionPlaySound(String path, int frame, int animationID) {
    	this.sound = Utilities.getCustomLoader().getAudioNode(path);
    	this.sound.setVolume(frame >= 0 ? Main.getGameSettings().getVoicesVolume() : Main.getGameSettings().getSFXVolume());
    	this.frameToPlayOn = frame;
    	this.animationToPlayOn = animationID;
    	this.hasBeenPlayed = false;
    	this.cooldown = 0;
    }
    
    @Override
    public void onInit() {
        this.hasBeenPlayed = false;
        this.cooldown = 0;
        if (frameToPlayOn >= 0 && this.action.entity instanceof PlayableCharacter){
    		this.sound.setPitch(((PlayableCharacter)this.action.entity).getPitch());
    	}
    }

    @Override
    public boolean onUpdate() {
        if (this.frameToPlayOn == -1) {
            if (this.action.entity instanceof ViolentEntity) {
                if (((ViolentEntity) this.action.entity).wasCollisionSuccesful()) {
                    this.sound.playInstance();
                    this.cooldown = .1f;
                }
            } else {
                if (this.action.entity.hasCollided()) {
                    this.sound.playInstance();
                    this.cooldown = .1f;
                }
            }
        } else if (this.frameToPlayOn == -3) {
            if (this.action.entity instanceof ViolentEntity) {
                if (((ViolentEntity) this.action.entity).wasCollisionSuccesful() && ((ViolentEntity) this.action.entity).whatIsPower() < 0.75) {
                    this.sound.playInstance();
                    this.cooldown = .1f;
                }
            } else {
                if (this.action.entity.hasCollided()) {
                    this.sound.playInstance();
                    this.cooldown = .1f;
                }
            }
        } else if (this.frameToPlayOn == -2) {
            if (this.action.entity instanceof ViolentEntity) {
                if (((ViolentEntity) this.action.entity).wasCollisionSuccesful() && ((ViolentEntity) this.action.entity).whatIsPower() >= 0.75) {
                    this.sound.playInstance();
                    this.cooldown = .1f;
                }
            }
        } else if (!this.hasBeenPlayed && 
        		this.action.getCurrentAnimationNumber() == this.animationToPlayOn && 
        		this.action.getCurrentFrameNumber() == this.frameToPlayOn) {
            this.sound.playInstance();
            this.hasBeenPlayed = true;
        }
        
        this.cooldown = Math.max(this.cooldown - Utilities.lockedTPF, 0);
        
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
        this.cooldown = 0f;
    }
}
