package entities.characters.actions;

import entities.PlayableCharacter;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to: Keep
 * looping until the specified control is in the specified state.
 * 
 * @author Matthew
 */
public class ActionWaitTillControl extends ActionModifier {
	private String controlWaitingOn;
	private boolean stateWaitingOn;
	private boolean shouldBeRefreshed;

	public ActionWaitTillControl(String controlToWaitOn, boolean stateToWaitOn,
			boolean shouldBeRefreshed) {
		this.controlWaitingOn = controlToWaitOn;
		this.stateWaitingOn = stateToWaitOn;
		this.shouldBeRefreshed = shouldBeRefreshed;
	}

	@Override
	public void onInit() {
	}

	@Override
	public boolean onUpdate() {
		if (this.action.entity instanceof PlayableCharacter)
			return ((PlayableCharacter) this.action.entity).checkControl(
					this.controlWaitingOn, this.shouldBeRefreshed) == this.stateWaitingOn;
		else
			return false;
	}

	@Override
	public boolean shouldFinish() {
		if (this.action.entity instanceof PlayableCharacter)
			return ((PlayableCharacter) this.action.entity).checkControl(
					this.controlWaitingOn, this.shouldBeRefreshed) == this.stateWaitingOn;
		else
			return true;
	}

	@Override
	public void cleanUp() {
	}
}
