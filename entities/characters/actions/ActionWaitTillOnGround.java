package entities.characters.actions;

import entities.PhysicsEntity;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Keep looping until the specified control is in the specified state.
 *
 * @author Matthew
 */
public class ActionWaitTillOnGround extends ActionModifier {
	
	int frameToWaitOn;
	
    public ActionWaitTillOnGround() {
        frameToWaitOn = -1;
    }
    
    public ActionWaitTillOnGround(int frameToWaitOn){
    	this.frameToWaitOn = frameToWaitOn;
    }
    
    @Override
    public void onInit() {
        ((PhysicsEntity) this.action.entity).setOnGround(false);
    }

    @Override
    public boolean onUpdate() {
    	if (frameToWaitOn != -1 && this.action.getCurrentFrameNumber() >= frameToWaitOn && !((PhysicsEntity) this.action.entity).isOnGround())
    		this.action.setCurrentFrame(frameToWaitOn);
    	if (frameToWaitOn == -1)
    		return ((PhysicsEntity) this.action.entity).isOnGround(); 
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return ((PhysicsEntity) this.action.entity).isOnGround();
    }

    @Override
    public void cleanUp() {}
}
