package entities.characters.actions;

import physics.BoundingBox;

import com.jme3.math.Vector2f;

import engine.Main;
import engine.utility.Utilities;
import entities.PlayableCharacter;

public class ActionTeleport extends ActionModifier {
	boolean end;
	boolean init;
	boolean start;
	BoundingBox b;
	float momentum;
	float dxSave;
	float dySave;
    public ActionTeleport(float m) {
        momentum = m;
    }
    
    @Override
    protected void onInit() {
        this.action.entity.setPureXVelocity(this.action.entity.getVelocity().x*momentum);
        this.action.entity.setPureYVelocity(this.action.entity.getVelocity().y*momentum);
        end = false;
        init = false;
        b = this.action.entity.getRealBounds();
        dxSave = 0;
        dySave = 0;
        start = true;
    }

    @Override
    protected boolean onUpdate() {
    	if (start){
    		this.action.entity.setPureXVelocity(this.action.entity.getVelocity().x*momentum);
    		this.action.entity.setPureYVelocity(this.action.entity.getVelocity().y*momentum);
    		start = false;
    	}
        if (end && !init){
        	init = true;
        	boolean up = ((PlayableCharacter) this.action.entity).checkControl("Up", false);
        	boolean down = ((PlayableCharacter) this.action.entity).checkControl("Down", false);
        	boolean left = ((PlayableCharacter) this.action.entity).checkControl("Left", false);
        	boolean right = ((PlayableCharacter) this.action.entity).checkControl("Right", false);
        	if (up && down){
        		up = false;
        		down = false;
        	}
        	if (left && right){
        		left = false;
        		right = false;
        	}
        	float dx = 0f;
        	float dy = 0f;
        	float diag = (float)(Math.sqrt(2))/2f;
        	if (up && right){
        		dx = diag;
        		dy = diag;
        	} else if (up && left){
        		dx = -diag;
        		dy = diag;
        	} else if (down && right){
        		dx = diag;
        		dy = -diag;
        	} else if (down && left){
        		dx = -diag;
        		dy = -diag;
        	} else if (right) {
        		dx = 1;
        		dy = 0;
        	} else if (left){
        		dx = -1;
        		dy = 0;
        	} else if (down){
        		dx = 0;
        		dy = -1;
        	} else {
        		dx = 0;
        		dy = 1;
        	}
        	
        	float max = ((PlayableCharacter)this.action.entity).getRecoveryForce()*100;
        	for (int i = 1; i <= max; i++){
        		BoundingBox b2 = new BoundingBox(new Vector2f(b.getLowerLeft().x+dx*i, b.getLowerLeft().y+dy*i), b.getDimensions());
        		if (Main.getGameState().getWorld().getStage().complex2DCollision(b2, 1f)){
        			dy *= (i-1);
        			dx *= (i-1);
        			break;
        		}
        		if (i > max-1){
        			dx *= max;
        			dy *= max;
        			break;
        		}
        	}
        	this.action.entity.addToPosition(dx, dy);
        	System.out.println(dx + ", " + dy);
        	if (dx != 0)
        		this.action.entity.setFacing(Utilities.getGeneralUtility().getSignAsBoolean(dx));
        	dxSave = dx;
        	dySave = dy;
        }
        return false;
    }

    @Override
    protected boolean shouldFinish() {
        if (!end){
        	this.action.setCurrentAnimation(1);
        	this.action.entity.setVelocity(((PlayableCharacter)this.action.entity).getRecoveryForce()*momentum*100*dxSave, ((PlayableCharacter)this.action.entity).getRecoveryForce()*momentum*100*dySave, false);
        	end = true;
        	return false;
        }
        return true;
    }

    @Override
    protected void cleanUp() {
    }
}
