package entities.characters.actions;

import entities.PlayableCharacter;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Set whether a {@link entities.PlayableCharacter} is falling.
 *
 * @author Matthew
 */
public class ActionFallWhenFinished extends ActionModifier {
    public ActionFallWhenFinished() {
    }
    
    @Override
    public void onInit() {
    }

    @Override
    public boolean onUpdate() {      
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
        ((PlayableCharacter) this.action.entity).setFalling(true);
    }
}
