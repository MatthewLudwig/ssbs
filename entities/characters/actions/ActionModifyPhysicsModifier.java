package entities.characters.actions;

import entities.PhysicsEntity;
import physics.PhysicsModifier;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Add or remove physics modifiers from an entity (which is assumed to be a {@link entities.PhysicsEntity}).
 *
 * @author Matthew
 */
public class ActionModifyPhysicsModifier extends ActionModifier {
    private PhysicsModifier modifierToAddOrRemove;
    private boolean addOrRemove;
    
    public ActionModifyPhysicsModifier(PhysicsModifier modifierToAddOrRemove, boolean addOrRemove) {
        this.modifierToAddOrRemove = modifierToAddOrRemove;
        this.addOrRemove = addOrRemove;
    }
    
    @Override
    public void onInit() {
        if(this.addOrRemove) {
            ((PhysicsEntity) this.action.entity).addPhysicsModifier(modifierToAddOrRemove);
        } else {
            ((PhysicsEntity) this.action.entity).removePhysicsModifier(modifierToAddOrRemove);
        }
    }

    @Override
    public boolean onUpdate() {
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
        if(!this.addOrRemove) {
            ((PhysicsEntity) this.action.entity).addPhysicsModifier(modifierToAddOrRemove);
        } else {
            ((PhysicsEntity) this.action.entity).removePhysicsModifier(modifierToAddOrRemove);
        }
    }
}
