package entities.characters.actions;

import com.jme3.audio.AudioNode;
import engine.Main;
import engine.utility.Utilities;
import entities.PlayableCharacter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Play a sound on the specified frame.
 *
 * @author Matthew
 */
public class ActionPlaySoundForMarth extends ActionModifier { 
    private static final List<List<AudioNode>> activeSounds;
    
    private int soundNumber;
    private AudioNode sound;
    private int frameToPlayOn;
    private boolean hasBeenPlayed;
    private float cooldown;
    
    static {
        activeSounds = new ArrayList<List<AudioNode>>();
        activeSounds.add(new LinkedList<AudioNode>());
        activeSounds.add(new LinkedList<AudioNode>());
        activeSounds.add(new LinkedList<AudioNode>());
        activeSounds.add(new LinkedList<AudioNode>());
        activeSounds.add(new LinkedList<AudioNode>());
        activeSounds.add(new LinkedList<AudioNode>());
    }
    
    /**
     * @param path The path to the audio file that will be played.
     * @param frame The frame number that the sound effect should play on, -1 if it is a hit sound effect.
     */
    public ActionPlaySoundForMarth(String path, int frame) {
        this.soundNumber = Integer.parseInt(path.split("_")[1].replace(".ogg", ""));
        
        this.sound = Utilities.getCustomLoader().getAudioNode(path);
        this.sound.setVolume(frame != -1 ? Main.getGameSettings().getVoicesVolume() : Main.getGameSettings().getSFXVolume());

        activeSounds.get(this.soundNumber).add(this.sound);
        
        this.frameToPlayOn = frame;
        this.hasBeenPlayed = false;
        this.cooldown = 0;
    }
    
    @Override
    public void onInit() {
        this.hasBeenPlayed = false;
        this.cooldown = 0;
        if (frameToPlayOn >= 0 && this.action.entity instanceof PlayableCharacter){
    		this.sound.setPitch(((PlayableCharacter)this.action.entity).getPitch());
    	}
    }

    @Override
    public boolean onUpdate() {
        if (!this.hasBeenPlayed && this.action.getCurrentFrameNumber() == this.frameToPlayOn) {
            for (AudioNode node : activeSounds.get(this.soundNumber)) {
                node.stop();
            }
            
            this.sound.play();
            this.hasBeenPlayed = true;
        }
        
        this.cooldown = Math.max(this.cooldown - Utilities.lockedTPF, 0);
        
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
        this.cooldown = 0f;
    }
}
