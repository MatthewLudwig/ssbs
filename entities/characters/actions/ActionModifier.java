package entities.characters.actions;

/**
 * Base class for all Action Modifiers that makes the creation of
 * more advanced {@link CharacterAction} objects easier.
 * @author Matthew
 */
public abstract class ActionModifier {
    protected CharacterAction action;
    
    protected void setCharacterAction(CharacterAction action) {
        this.action = action;
    }
    
    @Override
    public boolean equals(Object o) {
        return this.getClass().isInstance(o);
    }
    
    /**
     * Function called upon the initialization of an action and it's modifiers.
     */
    protected abstract void onInit();
    
    /**
     * Function called when an action and it's modifiers are being updated.
     * @return true when the action is complete.
     */
    protected abstract boolean onUpdate();
    
    /**
     * Function called when an action and it's modifiers are attempting to finish.
     * @return true if the action is finished and false if it should be repeated.
     */
    protected abstract boolean shouldFinish();
    
    /**
     * Function called upon an action finishing in order to clean up modifiers.
     */
    protected abstract void cleanUp();
}
