package entities.characters.actions;

import engine.Main;
import entities.PhysicsEntity;
import entities.PlayableCharacter;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to: Set the
 * facing of a {@link entities.PhysicsEntity}.
 * 
 * @author Matthew
 */
public class ActionShortHop extends ActionModifier {

	private float initialYValue;
	private boolean isComplete;

	public ActionShortHop() {
		this.initialYValue = 0;
		this.isComplete = false;
	}

	@Override
	public void onInit() {
		this.initialYValue = this.action.entity.getAppropriateBounds()
				.getLowerMidpoint().y;
		this.isComplete = false;
	}

	@Override
	public boolean onUpdate() {
		if (this.action.entity instanceof PlayableCharacter
				&& !((PlayableCharacter) this.action.entity).checkControl("Up",
						false)) {
			if (!this.isComplete
					&& this.action.entity.getVelocity().y > 0
					&& this.action.entity.getAppropriateBounds()
							.getLowerMidpoint().y - this.initialYValue < (.8f * this.action.entity
							.getDefaultHeight())) {
				this.action.entity.setPureYVelocity(this.action.entity
						.getVelocity().y / 2);
			}

			this.isComplete = true;
		}

		return false;
	}

	@Override
	public boolean shouldFinish() {
		return true;
	}

	@Override
	public void cleanUp() {
	}
}
