package entities.characters.actions;

import physics.PhysicsModifier;

/**
 * 
 * 
 * @author Matthew
 */
public class ActionFinalSmashFreeze extends ActionModifier {
    public ActionFinalSmashFreeze() {
        
    }
    
    @Override
    public void onInit() {
        this.action.entity.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.action.entity.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
        this.action.entity.setVelocity(0, 0, true);
    }

    @Override
    public boolean onUpdate() {
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
        this.action.entity.removePhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.action.entity.removePhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
    }
}
