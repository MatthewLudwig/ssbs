package entities.characters.actions;

import entities.PlayableCharacter;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Act as a sprinting action.
 *
 * @author Matthew
 */
public class ActionSprint extends ActionModifier {
    public ActionSprint() {

    }

    @Override
    public void onInit() {
    }

    @Override
    public boolean onUpdate() {
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
        if (this.action.entity instanceof PlayableCharacter) {
            ((PlayableCharacter) this.action.entity).endSprint();
        }
    }
}
