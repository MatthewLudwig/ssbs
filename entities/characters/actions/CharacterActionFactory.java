package entities.characters.actions;

import physics.BoundingBox;
import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.Waluigi;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.general.BuryEffect;
import entities.general.IceEffect;
import entities.general.ShieldBreakEffect1;
import entities.general.ShieldBreakEffect2;
import entities.general.TestEntity;
import entities.general.WaluigiCoin;

/**
 * 
 * @author Matthew
 */
public class CharacterActionFactory {
	public static CharacterAction getAnimation(PhysicsEntity entity,
			String animationName, StayingPower stayingPower, float updateLimit) {
		CharacterAction action = new CharacterAction(entity, animationName,
				stayingPower, updateLimit);

		return action;
	}

	public static CharacterAction getDeathAnimation(PhysicsEntity entity,
			String animationName, float updateLimit) {
		CharacterAction action = getAnimation(entity, animationName,
				StayingPower.BARELY, updateLimit);
		action.addModifier(new ActionKillEntity());
		return action;
	}

	/**
	 * Angle must be passed in in degrees.
	 */
	public static CharacterAction getHurt(PhysicsEntity entity,
			short framesOfHitstun, float knockbackSpeed, Angle knockbackAngle) {
		CharacterAction action = getAnimation(entity, "hurt", StayingPower.MAX,
				1);

		action.addModifier(new ActionCompleteNumberOfTicks(framesOfHitstun));
		if (entity.isOnGround()) {
			if (knockbackAngle.getValue() == -90
					|| knockbackAngle.getValue() == 270) {
				knockbackAngle = new Angle(90);
				knockbackSpeed *= 0.75f;
			} else if (knockbackAngle.getValue() < 0) {
				knockbackAngle = new Angle(Math.abs(knockbackAngle.getValue()));
				knockbackSpeed *= 0.75f;
			} else if (knockbackAngle.getValue() > 180) {
				knockbackAngle = new Angle(360 - knockbackAngle.getValue());
				knockbackSpeed *= 0.75f;
			}
		}
		action.addModifier(new ActionMovement(knockbackSpeed, Utilities
				.getPhysicsUtility().calculateCorrectAngle(
						new Vector2f((knockbackAngle.cos() * 2) / 3,
								((knockbackAngle.sin() * 2) + 1) / 3),
						Vector2f.ZERO), 0f, false));
		action.addModifier(new ActionHurt());

		if (entity instanceof PlayableCharacter) {
			if (entity.getDisplayName().equals("Marth")) {
				action.addModifier(new ActionPlaySoundForMarth(
						"/Character Sounds/" + entity.getDisplayName()
								+ "/hurt_"
								+ ((PlayableCharacter) entity).getColorNumber()
								+ ".ogg", 0));
			} else {
				action.addModifier(new ActionPlaySound("/Character Sounds/"
						+ entity.getDisplayName() + "/hurt.ogg", 0));
			}
		}

		return action;
	}

	public static CharacterAction getDodge(PhysicsEntity entity,
			Angle dodgeAngle, short dodgeTime, float dodgeDist) {
		CharacterAction action = getAnimation(entity, "crouch",
				StayingPower.EXTREME, 1);
		if (dodgeDist == 0) {
			action.addModifier(new ActionCompleteNumberOfTicks(dodgeTime));
			action.addModifier(new ActionMovement(0, dodgeAngle, 0, false));
			action.addModifier(new ActionModifyGravity(0.5f));
			action.addModifier(new ActionInvincible(dodgeTime * 0.8f
					* Utilities.lockedTPF));
		} else {
			action.addModifier(new ActionCompleteNumberOfTicks((int) dodgeDist));
			action.addModifier(new ActionMovement(dodgeDist, dodgeAngle, 0,
					false));
			// action.addModifier(new ActionFallWhenFinished());
			action.addModifier(new ActionModifyPhysicsModifier(
					PhysicsModifier.NOGRAVITY, true));
			action.addModifier(new ActionInvincible(dodgeDist * 0.8f
					* Utilities.lockedTPF));
		}
		return action;
	}

	public static CharacterAction getParalyzed(PhysicsEntity entity,
			short framesOfHitstun) {
		CharacterAction action = getAnimation(entity, "hurt", StayingPower.MAX,
				0);

		action.addModifier(new ParalyzeAction(framesOfHitstun));

		if (entity.getDisplayName().equals("Marth")) {
			action.addModifier(new ActionPlaySoundForMarth("/Character Sounds/"
					+ entity.getDisplayName() + "/hurt_"
					+ ((PlayableCharacter) entity).getColorNumber() + ".ogg", 0));
		} else {
			action.addModifier(new ActionPlaySound("/Character Sounds/"
					+ entity.getDisplayName() + "/hurt.ogg", 0));
		}

		return action;
	}

	public static CharacterAction getShieldBreak(PhysicsEntity entity,
			short framesOfStun) {
		CharacterAction action = getAnimation(entity, "hurt", StayingPower.MAX,
				0.5f);

		action.addModifier(new ShieldBreakAction(framesOfStun));
		action.addModifier(new ActionFinalSmashFreeze());

		if (entity.getDisplayName().equals("Marth")) {
			action.addModifier(new ActionPlaySoundForMarth("/Character Sounds/"
					+ entity.getDisplayName() + "/hurt_"
					+ ((PlayableCharacter) entity).getColorNumber() + ".ogg", 0));
		} else {
			action.addModifier(new ActionPlaySound("/Character Sounds/"
					+ entity.getDisplayName() + "/hurt.ogg", 0));
		}

		return action;
	}

	public static CharacterAction getBuried(PhysicsEntity entity,
			short framesOfHitstun, float knockbackSpeed) {
		CharacterAction action = getAnimation(entity, "hurt", StayingPower.MAX,
				0);
		action.addModifier(new BuryAction(framesOfHitstun, knockbackSpeed));

		/*
		 * if (entity.getDisplayName().equals("Marth")) { action.addModifier(new
		 * ActionPlaySoundForMarth("/Character Sounds/" +
		 * entity.getDisplayName() + "/hurt_" + ((PlayableCharacter)
		 * entity).getColorNumber() + ".ogg", 0)); } else {
		 * action.addModifier(new ActionPlaySound("/Character Sounds/" +
		 * entity.getDisplayName() + "/hurt.ogg", 0)); }
		 */

		return action;
	}

	public static CharacterAction getFrozen(PhysicsEntity entity,
			short framesOfHitstun, float knockbackSpeed, Angle knockbackAngle) {
		CharacterAction action = getAnimation(entity, "hurt", StayingPower.MAX,
				0);
		action.addModifier(new ActionMovement(knockbackSpeed, Utilities
				.getPhysicsUtility().calculateCorrectAngle(
						new Vector2f((knockbackAngle.cos() * 2) / 3,
								((knockbackAngle.sin() * 2) + 1) / 3),
						Vector2f.ZERO), 0f, false));
		action.addModifier(new FreezeAction(framesOfHitstun));

		/*
		 * if (entity.getDisplayName().equals("Marth")) { action.addModifier(new
		 * ActionPlaySoundForMarth("/Character Sounds/" +
		 * entity.getDisplayName() + "/hurt_" + ((PlayableCharacter)
		 * entity).getColorNumber() + ".ogg", 0)); } else {
		 * action.addModifier(new ActionPlaySound("/Character Sounds/" +
		 * entity.getDisplayName() + "/hurt.ogg", 0)); }
		 */

		return action;
	}

	public static CharacterAction getStand(PhysicsEntity entity,
			float updateLimit) {
		CharacterAction action = getAnimation(entity, "stand",
				StayingPower.NONE, updateLimit);

		return action;
	}

	public static CharacterAction getGrabbed(PhysicsEntity entity) {
		CharacterAction action = getAnimation(entity, "hurt",
				StayingPower.NONE, 0);

		return action;
	}

	public static CharacterAction getGrab(PhysicsEntity entity) {
		CharacterAction action = getAnimation(entity, "grab",
				StayingPower.EXTREME, 0.75f);
		action.addModifier(new GrabAction());
		return action;
	}

	public static CharacterAction getTether(PhysicsEntity entity) {
		CharacterAction action = getAnimation(entity, "grab",
				StayingPower.EXTREME, 0.8f);
		action.addModifier(new ActionHalfHop());
		action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
		action.addModifier(new TetherAction());
		return action;
	}

	public static CharacterAction getHold(PhysicsEntity entity) {
		CharacterAction action = getAnimation(entity, "grab",
				StayingPower.EXTREME, 0.75f);
		action.addModifier(new HoldAction());
		return action;
	}

	public static CharacterAction getThrow(PhysicsEntity entity, Angle angle,
			float damage, float knockback) {
		CharacterAction action = getAnimation(entity, "grab",
				StayingPower.EXTREME, 0.75f);
		action.addModifier(new ThrowAction(angle, damage, knockback));
		return action;
	}

	public static CharacterAction getCrouch(PhysicsEntity entity,
			float updateLimit) {
		CharacterAction action = getAnimation(entity, "crouch",
				StayingPower.MEDIUM, updateLimit);
		action.addModifier(new ActionWaitTillControl("Down", false, false))
				.addModifier(
						new ActionModifyPhysicsModifier(
								PhysicsModifier.CANGOTHROUGHTHINPLATORMS, true));
		return action;
	}

	public static CharacterAction getShield(PhysicsEntity entity,
			float updateLimit) {
		CharacterAction action = getAnimation(entity, "crouch",
				StayingPower.MEDIUM, updateLimit);
		// action.addModifier(new ActionWaitTillControl("Shield", false,
		// false));
		action.addModifier(new ShieldAction());
		return action;
	}

	public static CharacterAction getLedgeHang(PlayableCharacter entity,
			float updateLimit) {
		CharacterAction action = getAnimation(entity, "ledgeHang",
				StayingPower.EXTREME, updateLimit);
		action.addModifier(new ActionCompleteNumberOfTicks(
				(int) (10f / Utilities.lockedTPF)));
		// action.addModifier(new ActionWaitTillControl("Shield", false,
		// false));
		action.addModifier(new LedgeHangAction());
		return action;
	}

	public static CharacterAction getJumpOne(PhysicsEntity entity,
			float updateLimit) {
		CharacterAction action = getAnimation(entity, "jump1",
				StayingPower.MEDIUM, updateLimit);
		action.addModifier(new ActionMovement(25f, 90f, 0f, false))
				.addModifier(new ActionToggleAttribute(Attribute.VECTORING))
				.addModifier(new ActionWaitTillOnGround());
		action.addModifier(new ActionPlaySound("/Character Sounds/"
				+ entity.getDisplayName() + "/jump1.ogg", 0));
		action.addModifier(new ActionShortHop());
		action.addModifier(new ActionQuickFall());
		return action;
	}

	public static CharacterAction getJumpTwo(PhysicsEntity entity,
			float updateLimit) {
		CharacterAction action = getAnimation(entity, "jump2",
				StayingPower.HIGH, updateLimit);
		action.addModifier(new ActionMovement(25f, 90f, 0f, false))
				.addModifier(new ActionToggleAttribute(Attribute.VECTORING))
				.addModifier(new ActionWaitTillOnGround());
		action.addModifier(new ActionPlaySound("/Character Sounds/"
				+ entity.getDisplayName() + "/jump2.ogg", 0));
		action.addModifier(new ActionQuickFall());
		return action;
	}

	public static CharacterAction getDrowning(PhysicsEntity entity,
			float updateLimit) {
		CharacterAction action = getAnimation(entity, "hurt",
				StayingPower.MEDIUM, updateLimit);
		return action;
	}

	public static CharacterAction getWalk(PhysicsEntity entity,
			boolean direction) {
		CharacterAction action = getAnimation(entity, "walk",
				StayingPower.BARELY, .75f);

		action.addModifier(new ActionSetFacing(direction));
		action.addModifier(new ActionMovement(25f, 0f, 0f, true));

		if (direction) {
			action.addModifier(new ActionWaitTillControl("Right", false, false));
		} else {
			action.addModifier(new ActionWaitTillControl("Left", false, false));
		}

		return action;
	}

	public static CharacterAction getSprint(PhysicsEntity entity,
			boolean direction) {
		CharacterAction action = getAnimation(entity, "walk", StayingPower.LOW,
				1.5f);

		action.addModifier(new ActionSetFacing(direction));
		action.addModifier(new ActionMovement(50f, 0f, 0f, true));
		action.addModifier(new ActionSprint());

		if (direction) {
			action.addModifier(new ActionWaitTillControl("Right", false, false));
		} else {
			action.addModifier(new ActionWaitTillControl("Left", false, false));
		}

		return action;
	}

	public static class ParalyzeAction extends ActionModifier {
		private float ticksToComplete;
		private int ticksCompleted;
		private boolean dir;
		private boolean sw;

		public ParalyzeAction(int ticksToComplete) {
			this.ticksToComplete = ticksToComplete;
			this.ticksCompleted = 0;
		}

		@Override
		protected void onInit() {
			this.ticksCompleted = 0;
			dir = false;
			sw = false;
		}

		@Override
		protected boolean onUpdate() {
			int d = Utilities.getGeneralUtility().getBooleanAsSign(dir);
			int f = Utilities.getGeneralUtility().getBooleanAsSign(
					this.action.entity.getFacing());
			if (sw) {
				this.action.entity.setPosition(
						this.action.entity.getPosition().x + 4 * d * f,
						this.action.entity.getPosition().y);
				dir = !dir;
			}
			sw = !sw;
			if (((PlayableCharacter) this.action.entity)
					.checkControl("*", true)) {
				this.ticksToComplete -= 0.7 * Utilities.lockedTPF;
			}
			this.ticksCompleted++;
			return this.ticksCompleted >= this.ticksToComplete;
		}

		@Override
		protected boolean shouldFinish() {
			return this.ticksCompleted >= this.ticksToComplete;
		}

		@Override
		protected void cleanUp() {
			((ViolentEntity) this.action.entity).setCurrentlyParalyzed(false);
		}
	}

	public static class ShieldBreakAction extends ActionModifier {
		private float ticksToComplete;
		private int ticksCompleted;
		private ShieldBreakEffect1 break1;
		private ShieldBreakEffect2 break2;

		public ShieldBreakAction(int ticksToComplete) {
			this.ticksToComplete = ticksToComplete;
			this.ticksCompleted = 0;
		}

		@Override
		protected void onInit() {
			this.ticksCompleted = 0;
			break1 = new ShieldBreakEffect1(
					(PlayableCharacter) this.action.entity);
			break2 = new ShieldBreakEffect2(
					(PlayableCharacter) this.action.entity);
			Vector2f pos = new Vector2f(this.action.entity.getPosition().x,
					this.action.entity.getPosition().y
							+ this.action.entity.getRealBounds()
									.getDimensions().y * 0.75f);
			break1.setPosition(pos.x, pos.y);
			break2.setPosition(pos.x, pos.y);
			Main.getGameState().spawnEntity(break1);
			Main.getGameState().spawnEntity(break2);
		}

		@Override
		protected boolean onUpdate() {
			if (((PlayableCharacter) this.action.entity)
					.checkControl("*", true)) {
				this.ticksToComplete -= 0.7 * Utilities.lockedTPF;
			}
			this.ticksCompleted++;
			return this.ticksCompleted >= this.ticksToComplete;
		}

		@Override
		protected boolean shouldFinish() {
			return this.ticksCompleted >= this.ticksToComplete;
		}

		@Override
		protected void cleanUp() {
			break1.setDead();
			break2.setDead();
		}
	}

	public static class ShieldAction extends ActionModifier {
		public ShieldAction() {
		}

		@Override
		protected void onInit() {
		}

		@Override
		protected boolean onUpdate() {
			return ((PlayableCharacter) this.action.entity).checkControl(
					"Shield", false) == false
					&& ((PlayableCharacter) this.action.entity)
							.checkShieldStun();
		}

		@Override
		protected boolean shouldFinish() {
			return ((PlayableCharacter) this.action.entity).checkControl(
					"Shield", false) == false
					&& ((PlayableCharacter) this.action.entity)
							.checkShieldStun();
		}

		@Override
		protected void cleanUp() {
		}
	}

	public static class LedgeHangAction extends ActionModifier {
		Vector2f go;
		boolean init;

		public LedgeHangAction() {
			init = false;
		}

		@Override
		protected void onInit() {
			// init = false;
			go = this.action.entity.getPosition();
			// System.out.println("oh");
		}

		@Override
		protected boolean onUpdate() {
			if (!init) {
				Vector2f vec = ((PlayableCharacter) this.action.entity)
						.findGreen();
				if (vec != null) {
					Vector2f bounds = this.action.entity.getRealBounds()
							.getDimensions().mult(-0.97f);
					bounds = new Vector2f(bounds.x
							* Utilities.getGeneralUtility().getBooleanAsSign(
									this.action.entity.getFacing()) * 0.5f,
							bounds.y);
					go = Main.getGameState().getWorld().getStage()
							.getNearestLedge(this.action.entity.getPosition())
							.add(bounds);
					init = true;
				}
			}
			this.action.entity.setPosition(go.x, go.y);
			this.action.entity.setVelocity(0, 0, false);
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return false;
		}

		@Override
		protected void cleanUp() {
		}
	}

	public static class BuryAction extends ActionModifier {
		private float ticksToComplete;
		private int ticksCompleted;
		private float speed;
		private int location;
		private boolean bounced;
		private BuryEffect burier;
		private Vector2f velocity;

		public BuryAction(int ticksToComplete, float speed) {
			this.ticksToComplete = ticksToComplete;
			this.speed = speed;
			this.ticksCompleted = 0;
		}

		@Override
		protected void onInit() {
			this.ticksCompleted = 0;
			bounced = false;
			if (this.action.entity.isOnThinGround()) {
				location = 1;
			} else if (this.action.entity.isOnGround()) {
				location = 0;
			} else {
				location = 2;
			}

			if (location == 1) {
				this.action.entity
						.addPhysicsModifier(PhysicsModifier.CANGOTHROUGHTHINPLATORMS);
			}
			if (location > 0) {
				this.action.entity
						.setPureYVelocity(-speed * (location * 3 + 1));
				velocity = new Vector2f(0, -speed * (location * 3 + 1));
			} else if (location == 0) {
				((ViolentEntity) this.action.entity).setCurrentlyBuried(true);
				((ViolentEntity) this.action.entity).setHasHeavyArmor(true);
				burier = new BuryEffect((PlayableCharacter) this.action.entity);
				burier.setPosition(this.action.entity.getRealBounds()
						.getLowerMidpoint().x, this.action.entity
						.getRealBounds().getLowerMidpoint().y - 1);
				Main.getGameState().spawnEntity(burier);
			}
			if (location == 2)
				this.ticksToComplete /= 4;
		}

		@Override
		protected boolean onUpdate() {
			if (location == 1 && ticksCompleted * 2 > ticksToComplete) {
				this.action.entity
						.removePhysicsModifier(PhysicsModifier.CANGOTHROUGHTHINPLATORMS);
			}
			if (location == 0) {
				if (((PlayableCharacter) this.action.entity).checkControl("*",
						true)) {
					this.ticksToComplete -= 0.7 * Utilities.lockedTPF;
				}
			}
			if (location == 2) {
				if (this.action.entity.hasCollidedWithObstacle(true)) {
					this.action.entity.setPureXVelocity(-velocity.x);
					bounced = true;
				} else if (this.action.entity.hasCollidedWithObstacle(false)) {
					this.action.entity.setPureYVelocity(-velocity.y);
					bounced = true;
				}
				if (this.action.entity.getVelocity().y >= 0.1f) {
					velocity = this.action.entity.getVelocity();
				}
			}
			if (location < 2 || bounced) {
				this.ticksCompleted++;
			}
			return ticksCompleted >= ticksToComplete;
		}

		@Override
		protected boolean shouldFinish() {
			return ticksCompleted >= ticksToComplete;
		}

		@Override
		protected void cleanUp() {
			this.ticksCompleted = 0;
			if (location == 0) {
				burier.setDead();
				((ViolentEntity) this.action.entity).setCurrentlyBuried(false);
				((ViolentEntity) this.action.entity).setHasHeavyArmor(false);
			}
		}
	}

	public static class FreezeAction extends ActionModifier {
		private float ticksToComplete;
		private int ticksCompleted;
		private boolean bounced, init;
		private IceEffect ice;
		private Vector2f velocity;
		private float startDamage;

		public FreezeAction(int ticksToComplete) {
			this.ticksToComplete = ticksToComplete;
			this.ticksCompleted = 0;
		}

		@Override
		protected void onInit() {
			this.ticksCompleted = 0;
			bounced = false;
			init = false;
			this.startDamage = ((ViolentEntity) this.action.entity).getDamage();
			((ViolentEntity) this.action.entity).setCurrentlyFrozen(true);
			((ViolentEntity) this.action.entity).setHasSuperArmor(true);
			ice = new IceEffect((PlayableCharacter) this.action.entity);
			Main.getGameState().spawnEntity(ice);
		}

		@Override
		protected boolean onUpdate() {
			this.ticksCompleted++;
			velocity = this.action.entity.getVelocity();
			if (!init) {
				init = true;
				this.action.entity
						.setPureYVelocity((Math.abs(velocity.y) + 5) * 2);
			}
			if (this.action.entity.hasCollidedWithObstacle(true)) {
				this.action.entity.setPureXVelocity(-velocity.x);
			}
			if ((this.action.entity.hasCollidedWithObstacle(false) || this.action.entity
					.isOnGround()) && !bounced && this.ticksCompleted > 2) {
				bounced = true;
				this.ticksCompleted = 0;
				this.action.entity.setPureXVelocity(0);
				this.action.entity.setPureYVelocity(0);
			}
			if (((PlayableCharacter) this.action.entity)
					.checkControl("*", true)) {
				this.ticksToComplete -= 0.7 * Utilities.lockedTPF;
			}
			if (!((ViolentEntity) this.action.entity).isCurrentlyFrozen()
					|| ((ViolentEntity) this.action.entity).getDamage()
							- startDamage > 35)
				ticksToComplete = 0;
			return ticksCompleted >= ticksToComplete;
		}

		@Override
		protected boolean shouldFinish() {
			return ticksCompleted >= ticksToComplete;
		}

		@Override
		protected void cleanUp() {
			this.ticksCompleted = 0;
			ice.setDead();
			((ViolentEntity) this.action.entity).setCurrentlyFrozen(false);
			((ViolentEntity) this.action.entity).setHasSuperArmor(false);
		}
	}

	public static class GrabAction extends ActionModifier {

		public GrabAction() {

		}

		@Override
		protected void onInit() {
		}

		@Override
		protected boolean onUpdate() {
			Vector2f vec = ((PlayableCharacter) this.action.entity).findWhite();
			// System.out.println(vec == null);
			if (vec != null){
				((PlayableCharacter) this.action.entity).lastWorkingGrabPoint = vec;
			}
			if (vec == null && this.action.getCurrentFrameNumber() > 0) {
				if (((PlayableCharacter) this.action.entity).lastWorkingGrabPoint == null) {
					vec = new Vector2f(this.action.entity
							.getAppropriateBounds().getDimensions().x
							* Utilities.getGeneralUtility().getBooleanAsNumber(
									this.action.entity.getFacing()),
							this.action.entity.getAppropriateBounds()
									.getDimensions().y);
				} else {
					vec = ((PlayableCharacter) this.action.entity).lastWorkingGrabPoint;
				}
			}
			if (vec != null) {
				Vector2f vec2 = this.action.entity
						.getFullBounds()
						.getLowerLeft()
						.add(vec)
						.add(new Vector2f(
								((PlayableCharacter) this.action.entity).grabOffset
										+ ((PlayableCharacter) this.action.entity).directionalGrabOffset
										* Utilities.getGeneralUtility()
												.getBooleanAsSign(
														this.action.entity
																.getFacing()),
								0));
				float dim = ((PlayableCharacter) this.action.entity).grabHitbox;
				BoundingBox grabBounds = new BoundingBox(
						vec2.subtract(new Vector2f(dim / 2f, dim / 2f)),
						new Vector2f(dim, dim));
				//TestEntity test = new TestEntity();
				//test.setPosition(vec2.x, vec2.y);
				//Main.getGameState().spawnEntity(test);
				// System.out.println(vec2.x + ", " + vec2.y);
				if (((PlayableCharacter) this.action.entity).grabbedPlayer == null) {
					for (PlayableCharacter pc : Main.getGameState().getWorld()
							.getPlayerList()) {
						BoundingBox b = pc.getAppropriateBounds();
						// System.out.println(b.isInBounds(vec2) && pc !=
						// this.action.entity);
						if (b.isAnyPartInBounds(grabBounds)
								&& pc != this.action.entity && !pc.isGrabbed
								&& !pc.isGrabbing && !pc.isInvincible()) {
							((PlayableCharacter) this.action.entity).grabbedPlayer = pc;
							pc.isGrabbed = true;
							((PlayableCharacter) this.action.entity).isGrabbing = true;
							break;
						}
					}
				} else {
					((PlayableCharacter) this.action.entity).grabbedPlayer
							.setPosition(
									vec2.x,
									vec2.y
											- ((PlayableCharacter) this.action.entity).grabbedPlayer
													.getAppropriateBounds()
													.getDimensions().y / 4f);
				}
			}
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {
		}
	}

	public static class HoldAction extends ActionModifier {
		float timeToRelease;

		public HoldAction() {

		}

		@Override
		protected void onInit() {
			timeToRelease = 5f;
		}

		@Override
		protected boolean onUpdate() {
			this.action
					.setCurrentFrame(this.action.getTotalNumberOfFrames() - 1);
			Vector2f vec = ((PlayableCharacter) this.action.entity).findWhite();
			// System.out.println(vec == null);
			if (vec == null && this.action.getCurrentFrameNumber() > 0) {
				if (((PlayableCharacter) this.action.entity).lastWorkingGrabPoint == null) {
					vec = new Vector2f(this.action.entity
							.getAppropriateBounds().getDimensions().x
							* Utilities.getGeneralUtility().getBooleanAsNumber(
									this.action.entity.getFacing()),
							this.action.entity.getAppropriateBounds()
									.getDimensions().y);
				} else {
					vec = ((PlayableCharacter) this.action.entity).lastWorkingGrabPoint;
				}
			}
			if (vec != null) {
				Vector2f vec2 = this.action.entity
						.getFullBounds()
						.getLowerLeft()
						.add(vec)
						.add(new Vector2f(
								((PlayableCharacter) this.action.entity).grabOffset
										+ ((PlayableCharacter) this.action.entity).directionalGrabOffset
										* Utilities.getGeneralUtility()
												.getBooleanAsSign(
														this.action.entity
																.getFacing()),
								0));
				// TestEntity test = new TestEntity();
				// test.setPosition(vec2.x, vec2.y);
				// Main.getGameState().spawnEntity(test);
				// System.out.println(vec.x + ", " + vec.y);
				((PlayableCharacter) this.action.entity).grabbedPlayer
						.setPosition(
								vec2.x,
								vec2.y
										- ((PlayableCharacter) this.action.entity).grabbedPlayer
												.getAppropriateBounds()
												.getDimensions().y / 4f);
				if (((PlayableCharacter) this.action.entity).grabbedPlayer
						.checkControl("*", true))
					timeToRelease -= 0.03f;
			}
			timeToRelease -= Utilities.lockedTPF;
			if (timeToRelease < 0) {
				((PlayableCharacter) this.action.entity).grabbedPlayer.isGrabbed = false;
				((PlayableCharacter) this.action.entity).grabbedPlayer = null;
				((PlayableCharacter) this.action.entity).isGrabbing = false;
				((PlayableCharacter) this.action.entity).lastWorkingGrabPoint = null;
				return true;
			}
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {
		}
	}

	public static class ThrowAction extends ActionModifier {
		float timeToRelease;
		Angle angle;
		float damage;
		float knockback;

		public ThrowAction(Angle angle, float damage, float knockback) {
			this.angle = angle;
			this.damage = damage;
			this.knockback = knockback;
		}

		@Override
		protected void onInit() {
			timeToRelease = 0.0f;
		}

		@Override
		protected boolean onUpdate() {
			this.action
					.setCurrentFrame(this.action.getTotalNumberOfFrames() - 1);
			Vector2f vec = ((PlayableCharacter) this.action.entity).findWhite();
			if (vec == null && this.action.getCurrentFrameNumber() > 0) {
				if (((PlayableCharacter) this.action.entity).lastWorkingGrabPoint == null) {
					vec = new Vector2f(this.action.entity
							.getAppropriateBounds().getDimensions().x
							* Utilities.getGeneralUtility().getBooleanAsNumber(
									this.action.entity.getFacing()),
							this.action.entity.getAppropriateBounds()
									.getDimensions().y);
				} else {
					vec = ((PlayableCharacter) this.action.entity).lastWorkingGrabPoint;
				}
			}
			if (vec != null) {
				Vector2f vec2 = this.action.entity
						.getFullBounds()
						.getLowerLeft()
						.add(vec)
						.add(new Vector2f(
								((PlayableCharacter) this.action.entity).grabOffset
										+ ((PlayableCharacter) this.action.entity).directionalGrabOffset
										* Utilities.getGeneralUtility()
												.getBooleanAsSign(
														this.action.entity
																.getFacing()),
								0));
				((PlayableCharacter) this.action.entity).grabbedPlayer
						.setPosition(
								vec2.x,
								vec2.y
										- ((PlayableCharacter) this.action.entity).grabbedPlayer
												.getAppropriateBounds()
												.getDimensions().y / 4f);
			}
			timeToRelease -= Utilities.lockedTPF;
			if (timeToRelease < 0) {
				if (angle.getValue() > 90 || angle.getValue() < -90) {
					this.action.entity.setFacing(false);
				} else {
					this.action.entity.setFacing(true);
				}
				// System.out.println("hey");
				((PlayableCharacter) this.action.entity).grabbedPlayer.isGrabbed = false;
				((PlayableCharacter) this.action.entity).grabbedPlayer
						.addToDamage(damage, knockback, 1, angle, false, false,
								false, false, 0,
								(ViolentEntity) this.action.entity);
				((PlayableCharacter) this.action.entity).grabbedPlayer = null;
				((PlayableCharacter) this.action.entity).isGrabbing = false;
				((PlayableCharacter) this.action.entity).lastWorkingGrabPoint = null;
				return true;
			}
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {
		}
	}

	public static class TetherAction extends ActionModifier {
		float delay;

		public TetherAction() {

		}

		@Override
		protected void onInit() {
			delay = 0;
		}

		@Override
		protected boolean onUpdate() {
			Vector2f vec = ((PlayableCharacter) this.action.entity).findWhite();
			// System.out.println(vec == null);
			if (vec != null) {
				Vector2f vec2 = this.action.entity
						.getFullBounds()
						.getLowerLeft()
						.add(vec)
						.add(new Vector2f(
								((PlayableCharacter) this.action.entity).grabOffset,
								0));
				float dim = ((PlayableCharacter) this.action.entity).grabHitbox;
				BoundingBox grabBounds = new BoundingBox(
						vec2.subtract(new Vector2f(dim / 2f, dim / 2f)),
						new Vector2f(dim, dim));
				// TestEntity test = new TestEntity();
				// test.setPosition(vec2.x, vec2.y);
				// Main.getGameState().spawnEntity(test);
				// System.out.println(vec2.x + ", " + vec2.y);
				Vector2f ledge = Main.getGameState().getWorld().getStage()
						.getNearestLedge(vec2);

				if (ledge != null && vec2.distanceSquared(ledge) < 1600) {
					Vector2f pos = this.action.entity.getPosition();
					this.action.entity.setPureXVelocity(ledge.x - pos.x);
					this.action.entity.setPureYVelocity(ledge.y - pos.y);
				}

				if (delay > 0.5f) {
					delay = 0;
					for (PlayableCharacter pc : Main.getGameState().getWorld()
							.getPlayerList()) {
						BoundingBox b = pc.getAppropriateBounds();
						// System.out.println(b.isInBounds(vec2) && pc !=
						// this.action.entity);
						if (b.isAnyPartInBounds(grabBounds)
								&& pc != this.action.entity
								&& !pc.isInvincible()) {
							pc.addToDamage(
									4f * ((ViolentEntity) this.action.entity)
											.getDamageModifier(),
									0.6f * ((ViolentEntity) this.action.entity)
											.getKnockbackModifier(),
									1,
									Utilities
											.getPhysicsUtility()
											.calculateCorrectAngle(
													pc.getAppropriateBounds()
															.getLowerMidpoint(),
													vec2), false, false, false,
									false, 0,
									(ViolentEntity) this.action.entity);
						}
					}
				} else {
					delay += 0.08;
				}
			}
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {
		}
	}
}
