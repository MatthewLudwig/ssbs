package entities.characters.actions;

import entities.PlayableCharacter;
import entities.ViolentEntity;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Act as a sprinting action.
 *
 * @author Matthew
 */
public class ActionReflect extends ActionModifier {
    private float reflectStrength;
    private boolean canFlipPlayers;
    private ReflectType reflectType;
    
    public ActionReflect(float strength, boolean flips, ReflectType type) {
        this.reflectStrength = strength;
        this.canFlipPlayers = flips;
        this.reflectType = type;
    }

    @Override
    public void onInit() {
        ((ViolentEntity) this.action.entity).setCanReflect(this.reflectStrength, this.canFlipPlayers, this.reflectType);
    }

    @Override
    public boolean onUpdate() {
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
        ((ViolentEntity) this.action.entity).setCantReflect();
    }
    
    public enum ReflectType {
        PHYSICAL(), ENERGY(), BOTH();
    }
}
