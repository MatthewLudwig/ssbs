package entities.characters.actions;

/**
 * Set of enums to represent the staying power of different character actions.
 * 
 * @author Matthew
 */
public enum StayingPower {
    NONE(),
    BARELY(),
    LOW(),
    MEDIUM(),
    HIGH(),
    EXTREME(),
    MAX(),
    OVERRIDE();
}
