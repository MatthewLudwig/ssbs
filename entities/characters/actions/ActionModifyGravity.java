package entities.characters.actions;

import engine.Main;
import entities.PhysicsEntity;
import entities.PlayableCharacter;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Set the facing of a {@link entities.PhysicsEntity}.
 *
 * @author Matthew
 */
public class ActionModifyGravity extends ActionModifier {
    
	float scale;
	
    public ActionModifyGravity(float scale) {
    	this.scale = scale;
    }
    
    @Override
    public void onInit() {
    	 this.action.entity.gravityModifier = scale;
    }

    @Override
    public boolean onUpdate() {
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
        this.action.entity.gravityModifier = 1;
    }
}
