package entities.characters.actions;

import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.ViolentEntity;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Only continue to the next phase if the button is pressed.
 *
 * @author Matthew
 */
public class ActionPressMultiHit extends ActionModifier {
	private String button;
	private float inputLag, waitTime;
	private boolean limit;
    
    public ActionPressMultiHit(String button) {
		this.button = button;
    }
    
    @Override
    public void onInit() {
    	inputLag = 0;
    	waitTime = 0;
    	limit = false;
    }

    @Override
    public boolean onUpdate() {
    	if (limit && !((ViolentEntity)this.action.entity).frameIsSection(this.action.getCurrentFrameNumber())){
    		this.action.setCurrentFrame(this.action.getCurrentFrameNumber()-1);
    	}
    	if (inputLag > 0){
    		inputLag -= Utilities.lockedTPF;
    	}
    	if (waitTime > 0){
    		waitTime -= Utilities.lockedTPF;
    	}
    	if (((PlayableCharacter) this.action.entity).checkControl(this.button, false)){
    		inputLag = 0.02f;
    	}
    	if (waitTime <= 0 && inputLag <= 0 && limit){
    		return true;
    	} else if (waitTime <= 0){
    		limit = false;
    	}
    	if (this.action.getCurrentFrameNumber() != 0 && ((ViolentEntity)this.action.entity).frameIsSection(this.action.getCurrentFrameNumber()) && inputLag <= 0){
    		if (!limit){
    			waitTime = 0.02f;
    			limit = true;
    		}
    	}
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {}
}
