package entities.characters.actions;

import entities.PhysicsEntity;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Set the facing of a {@link entities.PhysicsEntity}.
 *
 * @author Matthew
 */
public class ActionHalfHop extends ActionModifier {
    
    public ActionHalfHop() {
    }
    
    @Override
    public void onInit() {
        if (this.action.entity.isOnGround()) {
            ((PhysicsEntity) this.action.entity).setYVelocity(17.5f, true);
        } else {
            this.action.entity.setPureYVelocity(0);
        }
    }

    @Override
    public boolean onUpdate() {
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {}
}
