package entities.characters.actions;

import entities.PlayableCharacter;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Act as a counter, switching to a second animation upon damage being dealt.
 *
 * @author Matthew
 */
public class ActionOverrideFalling extends ActionModifier {
	
	public boolean wasFalling;
	
    public ActionOverrideFalling() {
    	this.wasFalling = false;
    }
    
    @Override
    protected void setCharacterAction(CharacterAction action) {
        super.setCharacterAction(action);
        action.doesOverrideFalling = true;
    }
    
    @Override
    public void onInit() {
    }

    @Override
    public boolean onUpdate() {
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
    	if (this.wasFalling) {
    		this.wasFalling = false;
            ((PlayableCharacter) this.action.entity).setFalling(true);
    	}
    }
}
