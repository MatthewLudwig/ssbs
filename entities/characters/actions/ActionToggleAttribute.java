package entities.characters.actions;

import engine.Main;
import entities.PlayableCharacter;
import entities.ViolentEntity;

import java.util.logging.Level;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Toggle the specified value for the associated entity.
 *
 * @author Matthew
 */
public class ActionToggleAttribute extends ActionModifier {
    private Attribute attribute;
    
    public ActionToggleAttribute(Attribute attribute) {
        this.attribute = attribute;
    }
    
    @Override
    public void onInit() {
        switch (this.attribute) {
            case VECTORING:
                ((PlayableCharacter) this.action.entity).setVectoring(true);
                break;
            case SUPERARMOR:
                ((ViolentEntity) this.action.entity).setHasSuperArmor(true);
                break;
            case HEAVYARMOR:
                ((ViolentEntity) this.action.entity).setHasHeavyArmor(true);
                break;
            case CANONLYHITPLAYERS:
                ((ViolentEntity) this.action.entity).setCanOnlyHitPlayers(true);
                break;
            case PARALYZE:
                ((ViolentEntity) this.action.entity).setParalyze(true);
                break;
            case BURIES:
                ((ViolentEntity) this.action.entity).setBury(true);
                break;
            case FREEZES:
                ((ViolentEntity) this.action.entity).setFreeze(true);
                break;
            case METEORS:
                ((ViolentEntity) this.action.entity).setMeteor(true);
                break;
            case BLOCKSPROJECTILES:
                ((ViolentEntity) this.action.entity).setBlockProjectile(true);
                break;
            case DISJOINT:
                this.action.entity.isDisjoint = true;
                break;
            default:
                Main.log(Level.WARNING, "Unknown attribute being set!", null);
                break;
        }
    }

    @Override
    public boolean onUpdate() {      
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
        switch (this.attribute) {
            case VECTORING:
                ((PlayableCharacter) this.action.entity).setVectoring(false);
                break;
            case SUPERARMOR:
                ((ViolentEntity) this.action.entity).setHasSuperArmor(false);
                break;
            case HEAVYARMOR:
                ((ViolentEntity) this.action.entity).setHasHeavyArmor(false);
                break;
            case CANONLYHITPLAYERS:
                ((ViolentEntity) this.action.entity).setCanOnlyHitPlayers(false);
                break;
            case PARALYZE:
                ((ViolentEntity) this.action.entity).setParalyze(false);
                break;
            case BURIES:
                ((ViolentEntity) this.action.entity).setBury(false);
                break;
            case FREEZES:
                ((ViolentEntity) this.action.entity).setFreeze(false);
                break;
            case METEORS:
                ((ViolentEntity) this.action.entity).setMeteor(false);
                break;
            case BLOCKSPROJECTILES:
                ((ViolentEntity) this.action.entity).setBlockProjectile(false);
                break;
            case DISJOINT:
                this.action.entity.isDisjoint = false;
                break;
            default:
                Main.log(Level.WARNING, "Unknown attribute being set!", null);
                break;
        }
    }
    
    public enum Attribute {
        VECTORING(),
        SUPERARMOR(),
        HEAVYARMOR(),
        CANONLYHITPLAYERS(),
        DISJOINT(),
        PARALYZE(),
        BURIES(),
        METEORS(),
        BLOCKSPROJECTILES(),
        FREEZES();
    }
}
