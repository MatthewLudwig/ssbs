package entities.characters.actions;

import entities.PhysicsEntity;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Set the facing of a {@link entities.PhysicsEntity}.
 *
 * @author Matthew
 */
public class ActionOnlyUsableOnGround extends ActionModifier {
    boolean end;
    
    public ActionOnlyUsableOnGround() {
    }
    
    @Override
    public void onInit() {
        if (!this.action.entity.isOnGround()) {
        	end = true;
        	this.action.finish(true);
        } else {
        	end = false;
        }
    }

    @Override
    public boolean onUpdate() {
    	return end;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {}
}
