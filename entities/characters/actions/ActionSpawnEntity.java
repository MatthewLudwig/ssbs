package entities.characters.actions;

import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.ViolentEntity;
import java.lang.reflect.Constructor;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Spawn an entity.
 *
 * @author Matthew
 */
public class ActionSpawnEntity extends ActionModifier {
    protected short[] spawnFrames;
    protected boolean[] spawnFrameStatus;
    protected PhysicsEntity[] spawnedEntities;
    protected PhysicsEntity lastSpawnedEntity;
    protected Constructor entityConstructor;
    protected Object[] entityParameters;
    
    private float xOffset;
    private boolean facingMatters;
    private float yOffset;
    
    public ActionSpawnEntity(short[] spawnFrames, int spawnLimit, Class entityClass, Object... parameters) {
        this.spawnFrames = spawnFrames;
        this.spawnFrameStatus = new boolean[this.spawnFrames.length];

        for (int count = 0; count < this.spawnFrameStatus.length; count++) {
            this.spawnFrameStatus[count] = false;
        }

        this.spawnedEntities = new PhysicsEntity[spawnLimit];
        this.lastSpawnedEntity = null;
        this.entityConstructor = Utilities.getGeneralUtility().getConstructor(entityClass, parameters);
        this.entityParameters = parameters;
        
        this.xOffset = 0;
    }
    
    public ActionSpawnEntity setXOffset(float offset, boolean facingMatters) {
        this.xOffset = offset;
        this.facingMatters = facingMatters;
        return this;
    }
    
    public ActionSpawnEntity setYOffset(float offset) {
        this.yOffset = offset;
        return this;
    }
    
    @Override
    public void onInit() {}

    @Override
    public boolean onUpdate() {      
        for(int count = 0; count < this.spawnFrames.length; count++)
        {
            if(this.action.getCurrentFrameNumber() == this.spawnFrames[count] && !this.spawnFrameStatus[count])
            {
                this.spawnEntity();
                this.spawnFrameStatus[count] = true;
            }
        }

        return false;   
    }
    
    protected void spawnEntity() {
        this.cleanUpSpawns();
        
        int freeSlot = this.findFreeSpot();
        
        if (freeSlot != -1) {
            this.spawnedEntities[freeSlot] = (PhysicsEntity) Utilities.getGeneralUtility().createObject(this.entityConstructor, this.entityParameters);
            this.spawnedEntities[freeSlot].setPosition(this.action.entity.getAppropriateBounds().getLowerMidpoint().x, this.action.entity.getAppropriateBounds().getLowerMidpoint().y);
            
            if (this.spawnedEntities[freeSlot] instanceof ViolentEntity) {
                ((ViolentEntity) this.spawnedEntities[freeSlot]).team = ((ViolentEntity) this.action.entity).team;
            }
            
            this.lastSpawnedEntity = this.spawnedEntities[freeSlot];
            
            Main.getGameState().spawnEntity(this.spawnedEntities[freeSlot]);
            float offset = this.facingMatters ? Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * this.xOffset : this.xOffset;
            this.spawnedEntities[freeSlot].addToPosition(offset, yOffset + ((this.action.animations[0][0].getRealBounds().getDimensions().y * this.action.entity.getScale()) - this.spawnedEntities[freeSlot].getAppropriateBounds().getDimensions().y) / 2);
        }
    }
        
    private void cleanUpSpawns() {
        for (int count = 0; count < spawnedEntities.length; count++) {
            if (this.spawnedEntities[count] != null && this.spawnedEntities[count].isDead()) {
                this.spawnedEntities[count] = null;
            }
        }
    }

    private int findFreeSpot() {
        for (int count = 0; count < spawnedEntities.length; count++) {
            if (this.spawnedEntities[count] == null) {
                return count;
            }
        }

        return -1;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
        for (int count = 0; count < this.spawnFrameStatus.length; count++) {
            this.spawnFrameStatus[count] = false;
        }
    }
}
