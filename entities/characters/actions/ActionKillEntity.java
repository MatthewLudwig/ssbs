package entities.characters.actions;

import entities.PhysicsEntity;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Kill the associated entity upon finishing the animation.
 *
 * @author Matthew
 */
public class ActionKillEntity extends ActionModifier {
    public ActionKillEntity() {

    }

    @Override
    public void onInit() {
    }

    @Override
    public boolean onUpdate() {
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
        ((PhysicsEntity) this.action.entity).setDead(true);
    }
}
