package entities.characters.actions;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Loop through the animation the required amount of times.
 *
 * @author Matthew
 */
public class ActionCompleteNumberOfTicks extends ActionModifier {
    private int ticksToComplete;
    private int ticksCompleted;
    
    public ActionCompleteNumberOfTicks(int ticksToComplete) {
        this.ticksToComplete = ticksToComplete;
        this.ticksCompleted = 0;
    }
    
    @Override
    public void onInit() {
    	this.ticksCompleted = 0;
    }

    @Override
    public boolean onUpdate() {
    	this.ticksCompleted++;
        return this.ticksCompleted >= this.ticksToComplete;
    }

    @Override
    public boolean shouldFinish() {
        return this.ticksCompleted >= this.ticksToComplete;
    }

    @Override
    public void cleanUp() {
        this.ticksCompleted = 0;
    }
}
