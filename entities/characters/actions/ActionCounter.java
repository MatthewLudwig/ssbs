package entities.characters.actions;

import entities.ViolentEntity;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Act as a counter, switching to a second animation upon damage being dealt.
 *
 * @author Matthew
 */
public class ActionCounter extends ActionModifier {
	
    public ActionCounter() {
    }
    
    @Override
    public void onInit() {
        ((ViolentEntity) this.action.entity).setCountering(true);
    }

    @Override
    public boolean onUpdate() {   
    	if (this.action.getCurrentAnimationNumber() == 0 && ((ViolentEntity) this.action.entity).getDamageDealt() != 0) {
    		this.action.setCurrentAnimation(1);
    	}
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
        ((ViolentEntity) this.action.entity).setCountering(false);
        ((ViolentEntity) this.action.entity).setAttackCapability(0, 0, 0, false, new short[]{0});
    }
}
