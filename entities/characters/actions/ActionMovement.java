package entities.characters.actions;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Move at a certain velocity and angle.
 *
 * @author Matthew
 */
public class ActionMovement extends ActionModifier {
    private float speed;
    private Angle angle;
    private float midAirEffect;
    private boolean facingMatters;
    private boolean isRecovery;
    private boolean keepMomentum;
    private boolean init;
    private int startFrame, endFrame;
    private boolean hasEnded;
    
    /**
     * Angle must be passed in in degrees.
     */
    public ActionMovement(float speed, float angle, float midAirEffect, boolean facingMatters) {
        this(speed, new Angle(angle), midAirEffect, facingMatters);
    }
    
    public ActionMovement(float speed, float angle, float midAirEffect, boolean facingMatters, int startFrame, int endFrame){
    	this(speed, new Angle(angle), midAirEffect, facingMatters, startFrame, endFrame);
    }
    
    public ActionMovement(float speed, Angle angle, float midAirEffect, boolean facingMatters) {
    	this(speed, angle, midAirEffect, facingMatters, 0, -1);
    }
    
    public ActionMovement(float speed, Angle angle, float midAirEffect, boolean facingMatters, int startFrame, int endFrame) {
        this.speed = speed;
        this.angle = angle;
        this.midAirEffect = midAirEffect;
        this.facingMatters = facingMatters;
        this.isRecovery = false;
        this.keepMomentum = false;
        this.startFrame = startFrame;
        this.endFrame = endFrame;
        this.hasEnded = false;
    }
    
    public ActionMovement setIsRecovery() {
        this.isRecovery = true;
        return this;
    }
    
    public ActionMovement setKeepMomentum() {
        this.keepMomentum = true;
        return this;
    }
    
    public void setSpeed(){
    	if(this.facingMatters) {
            Angle finalAngle = this.angle.add(((PhysicsEntity) this.action.entity).isOnGround() ? 0 : this.midAirEffect);
            float xVelocity = Utilities.getGeneralUtility().getBooleanAsSign(((PhysicsEntity) this.action.entity).getFacing()) * (float) (this.speed * finalAngle.cos());
            float yVelocity = (float) (this.speed * finalAngle.sin());
            Vector2f v = ((PhysicsEntity) this.action.entity).getVelocity();

            if(xVelocity != 0) {
            	if (!this.keepMomentum){
            		((PhysicsEntity) this.action.entity).setXVelocity(xVelocity);
            	} else {
            		((PhysicsEntity) this.action.entity).setXVelocity(xVelocity + v.x);
            	}
            }

            if(yVelocity != 0) {
                if (this.isRecovery) {
                    ((PlayableCharacter) this.action.entity).setYVelocity(yVelocity, true, true);
                } else {
                	if (!this.keepMomentum){
                		((PhysicsEntity) this.action.entity).setYVelocity(yVelocity, true);
                	} else {
                		((PhysicsEntity) this.action.entity).setYVelocity(yVelocity + v.y, true);
                	}
                }
            }
        } else {
            Angle finalAngle = this.angle.add(((PhysicsEntity) this.action.entity).isOnGround() ? 0 : this.midAirEffect);
            float xVelocity = (float) (this.speed * finalAngle.cos());
            float yVelocity = (float) (this.speed * finalAngle.sin());

            if(xVelocity != 0) {
                ((PhysicsEntity) this.action.entity).setXVelocity(xVelocity);
            }

            if (this.isRecovery) {
                ((PlayableCharacter) this.action.entity).setYVelocity(yVelocity, true, true);
            } else {
                ((PhysicsEntity) this.action.entity).setYVelocity(yVelocity, true);
            }
        }
    }
    
    @Override
    public void onInit() {
    	this.init = false;
    	if (this.action.getCurrentFrameNumber() >= startFrame){
    		this.init = true;
    		setSpeed();
    	}
    }

    @Override
    public boolean onUpdate() {   
    	if (this.action.getCurrentFrameNumber() >= startFrame && !init){
    		this.init = true;
    		setSpeed();
    	}
    	if (this.action.getCurrentFrameNumber() > endFrame && endFrame != -1 && !keepMomentum){
    		this.action.entity.setPureXVelocity(0);
    		if (!this.hasEnded)
    			this.action.entity.setPureYVelocity(0);
    		this.hasEnded = true;
    	}
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return true;
    }

    @Override
    public void cleanUp() {
    	if (!this.keepMomentum)
    		((PhysicsEntity) this.action.entity).setXVelocity(0);
    }
}
