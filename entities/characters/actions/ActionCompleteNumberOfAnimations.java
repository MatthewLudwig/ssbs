package entities.characters.actions;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to:  
 * Loop through the animation the required amount of times.
 *
 * @author Matthew
 */
public class ActionCompleteNumberOfAnimations extends ActionModifier {
    private int animationsToComplete;
    private int animationsCompleted;
    
    public ActionCompleteNumberOfAnimations(int animationsToComplete) {
        this.animationsToComplete = animationsToComplete;
        this.animationsCompleted = 0;
    }
    
    @Override
    public void onInit() {}

    @Override
    public boolean onUpdate() {      
        return false;
    }

    @Override
    public boolean shouldFinish() {
        return this.animationsCompleted++ == this.animationsToComplete;
    }

    @Override
    public void cleanUp() {
        this.animationsCompleted = 0;
    }
}
