package entities.characters.actions;

import entities.ViolentEntity;

/**
 * {@link ActionModifier} that allows for a {@link CharacterAction} to: Act as
 * an attack.
 * 
 * @author Matthew
 */
public class ActionAttack extends ActionModifier {
	private float damageDealt;
	private float knockback;
	private float priority;
	private boolean energy;
	private short[] sectionFrames;

	public ActionAttack(float damageDealt, float knockback, float priority) {
		this(damageDealt, knockback, priority, false, new short[]{0});
	}
	
	public ActionAttack(float damageDealt, float knockback, float priority, short[] sectionFrames) {
		this(damageDealt, knockback, priority, false, sectionFrames);
	}

	public ActionAttack(float damageDealt, float knockback, float priority,
			boolean energy) {
		this(damageDealt, knockback, priority, energy, new short[]{0});
	}
	
	public ActionAttack(float damageDealt, float knockback, float priority,
			boolean energy, short[] sectionFrames) {
		this.damageDealt = damageDealt;
		this.knockback = knockback;
		this.priority = priority;
		this.energy = energy;
		this.sectionFrames = sectionFrames;
	}

	@Override
	public void onInit() {
		((ViolentEntity) this.action.entity).setAttackCapability(
				this.damageDealt, this.knockback, this.priority, this.energy, this.sectionFrames);
	}

	@Override
	public boolean onUpdate() {
		return false;
	}

	@Override
	public boolean shouldFinish() {
		return true;
	}
	
	public boolean isEnergy() {
		return energy;
	}

	@Override
	public void cleanUp() {
		((ViolentEntity) this.action.entity).setAttackCapability(0, 0, 0, false, new short[]{0});
	}
}
