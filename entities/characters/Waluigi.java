package entities.characters;

import java.util.Map;

import physics.CollisionInfo;

import com.jme3.audio.AudioNode;

import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.KingDedede.JumpingAction;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.general.WaluigiCoin;
import entities.general.WaluigiDiceBlock;
import entities.projectiles.NessFlash;
import entities.projectiles.WaluigiBlueShell;
import entities.projectiles.WaluigiBobOmb;

/**
 * The character Waluigi within SSBS.
 * 
 * @author Matthew Ludwig
 */
public class Waluigi extends PlayableCharacter {
	public static final float[] STAT = CharacterStats.WaluigiStats;
	public static final float[] DMG = CharacterStats.WaluigiDamage;
	public static final float[] KNBK = CharacterStats.WaluigiKnockback;
	public static final float[] PRTY = CharacterStats.WaluigiPriority;
	public static final float[] ISPD = CharacterStats.WaluigiImageSpeed;
	public static final short[][] SECT = CharacterStats.WaluigiSectionFrames;

	private float diceIndex;
	private boolean vacuum;
	private PlayableCharacter target;
	private int hitNum;

	public Waluigi() {
		super("Waluigi", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]),
				STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
		setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
		this.scale = 1.19f;
		this.grabHitbox = 15;
        this.grabOffset = 17.67f;
	}

	@Override
	public Kirby.KirbyAbility getKirbyAbility() {
		return Kirby.KirbyAbility.FIGHTER;
	}

	public void onCollideWith(PhysicsEntity object, CollisionInfo info) {
		super.onCollideWith(object, info);

		if (this.currentAction == this.actionMap.get("Down B")
				&& object instanceof PlayableCharacter
				&& this.currentAction.getCurrentFrameNumber() == 0
				&& this.getDamageDealt() * info.getPower() > 0) {
			vacuum = true;
			target = (PlayableCharacter) object;
		}

		if (this.currentAction == this.actionMap.get("Neutral A")
				&& object instanceof PlayableCharacter
				&& this.getDamageDealt() * info.getPower() > 0) {
			if ((this.currentAction.getCurrentFrameNumber() >= 0
					&& this.currentAction.getCurrentFrameNumber() < 4 && hitNum < 1)
					|| (this.currentAction.getCurrentFrameNumber() >= 4
							&& this.currentAction.getCurrentFrameNumber() < 8 && hitNum < 2)
					|| (this.currentAction.getCurrentFrameNumber() >= 8
							&& this.currentAction.getCurrentFrameNumber() < 13 && hitNum < 3)) {
				hitNum++;
			}
		}
	}

	@Override
	protected CharacterAction getNeutralA() {
		CharacterAction action = new CharacterAction(this, "neutralA",
				StayingPower.EXTREME, ISPD[0]);
		action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
		action.addModifier(new ActionPressMultiHit("Attack"));
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Waluigi/neutralA.ogg", 0));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_00.ogg", -1));
		action.addModifier(new NeutralAAction());
		return action;
	}

	@Override
	protected CharacterAction getSideA() {
		CharacterAction action = new CharacterAction(this, "sideA",
				StayingPower.EXTREME, ISPD[1]);
		action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], true, SECT[1]));
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Waluigi/sideA.ogg", 0));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_05.ogg", -1));
		action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
		return action;
	}

	@Override
	protected CharacterAction getUpA() {
		CharacterAction action = new CharacterAction(this, "upA",
				StayingPower.EXTREME, ISPD[2]);
		action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Waluigi/upA.ogg", 0));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_08.ogg", -1));
		action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
		return action;
	}

	@Override
	protected CharacterAction getDownA() {
		CharacterAction action = new CharacterAction(this, "downA",
				StayingPower.EXTREME, ISPD[3]);
		action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Waluigi/downA.ogg", 0));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_02.ogg", -1));
		action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
		return action;
	}

	@Override
	protected CharacterAction getNeutralB() {
		CharacterAction action = new CharacterAction(this, "neutralB",
				StayingPower.EXTREME, ISPD[4]);
		action.addModifier(new ActionSpawnEntity(new short[] { 0 }, 1,
				WaluigiDiceBlock.class, this).setYOffset(45 * this.scale));
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Waluigi/neutralB.ogg", 9));
		action.addModifier(new NeutralBAction());
		return action;
	}

	@Override
	protected CharacterAction getSideB() {
		CharacterAction action = new CharacterAction(this, "sideB",
				StayingPower.EXTREME, ISPD[5]);
		action.addModifier(new ActionAttack(DMG[5], KNBK[5], PRTY[5], SECT[5]));
		action.addModifier(new ActionMovement(45, 0, 1, true).setKeepMomentum());
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Waluigi/sideB.ogg", 0));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_15.ogg", -1));
		return action;
	}

	@Override
	protected CharacterAction getUpB() {
		CharacterAction action = new CharacterAction(this, "upB",
				StayingPower.EXTREME, ISPD[6]);
		action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Waluigi/upB.ogg", 0));
		action.addModifier(new ActionSpawnEntity(new short[] { 3 }, 999,
				WaluigiBobOmb.class, this));
		return action;
	}

	@Override
	protected CharacterAction getDownB() {
		CharacterAction action = new CharacterAction(this, "downB",
				StayingPower.EXTREME, ISPD[7]);
		// action.addModifier(new ActionAttack(DMG[7], KNBK[7], PRTY[7]));
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Waluigi/downB.ogg", 0));
		action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
		action.addModifier(new DownBAction());
		return action;
	}

	@Override
	protected CharacterAction getFinalSmash() {
		CharacterAction action = new CharacterAction(this, "finalSmash",
				StayingPower.EXTREME, ISPD[8]);
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Waluigi/finalSmash.ogg", 0));
		action.addModifier(new ActionSpawnEntity(new short[] { 12 }, 999,
				WaluigiBlueShell.class, this));
		return action;
	}

	protected void overrideDefaults(Map<String, CharacterAction> actionMap) {
		actionMap.get("Jump 2").addModifier(new JumpingAction())
				.addAnimation(1, "jump3");
		actionMap
				.put("Neutral B2",
						new CharacterAction(this, "neutralB3",
								StayingPower.EXTREME, ISPD[4])
								.addModifier(new NeutralBAction())
								.addModifier(
										new ActionPlaySound(
												"/Character Sounds/Waluigi/neutralB.ogg",
												0))
								.addModifier(
										new ActionPlaySound(
												"/Common Sounds/hit_01.ogg", -1)));
	}

	public boolean isJumping() {
		return this.currentAction == actionMap.get("Jump 1")
				|| this.currentAction == actionMap.get("Jump 2");
	}

	public void setDice(float n) {
		this.diceIndex = n;
	}

	public void startDicePunch() {
		this.forceCurrentAction(actionMap.get("Neutral B2"));
	}

	public class JumpingAction extends ActionModifier {
		boolean holding;

		public JumpingAction() {
			holding = true;
		}

		@Override
		public void onInit() {
			holding = true;
			this.action.setCurrentAnimation(0);
		}

		@Override
		public boolean onUpdate() {
			if (!((PlayableCharacter) this.action.entity).checkControl("Up",
					false)) {
				holding = false;
			}

			return false;
		}

		@Override
		public boolean shouldFinish() {
			if (this.action.entity.getVelocity().y > 0)
				return false;
			if (!holding || this.action.getCurrentAnimationNumber() == 1)
				return true;
			this.action.setCurrentAnimation(1);
			this.action.setCurrentFrame(0);
			this.action.entity.setYVelocity(24, true);
			this.action.entity.gravityModifier = 0.5f;
			return false;
		}

		@Override
		public void cleanUp() {
			this.action.entity.gravityModifier = 1;
		}
	}

	public class NeutralAAction extends ActionModifier {

		public NeutralAAction() {
			hitNum = 0;
		}

		@Override
		public void onInit() {
			hitNum = 0;
		}

		@Override
		public boolean onUpdate() {
			if (hitNum >= 2) {
				((ViolentEntity) this.action.entity).setBury(true);
			}
			return false;
		}

		@Override
		public boolean shouldFinish() {
			return true;
		}

		@Override
		public void cleanUp() {
			hitNum = 0;
			((ViolentEntity) this.action.entity).setBury(false);
			//((PlayableCharacter) this.action.entity).setFinalSmash(true);
		}
	}

	public class NeutralBAction extends ActionModifier {

		public NeutralBAction() {
		}

		@Override
		public void onInit() {
			((ViolentEntity) this.action.entity).setAttackCapability(DMG[4]
					* diceIndex / 10f, KNBK[4] * diceIndex / 10f, PRTY[4]
					* diceIndex / 10f, false);
			this.action.entity.setXVelocity(diceIndex
					* 3
					* Utilities.getGeneralUtility().getBooleanAsSign(
							this.action.entity.getFacing()));
			this.action.entity.setYVelocity(0, false);
			this.action.entity.gravityModifier = 0;
		}

		@Override
		public boolean onUpdate() {
			return false;
		}

		@Override
		public boolean shouldFinish() {
			return true;
		}

		@Override
		public void cleanUp() {
			this.action.entity.gravityModifier = 1;
		}
	}

	public class DownBAction extends ActionModifier {
		private int numCoins;
		private int counter;
		private AudioNode audio;

		public DownBAction() {
			audio = Utilities.getCustomLoader().getAudioNode(
					"/Common Sounds/coin.ogg");
			numCoins = 1;
		}

		@Override
		public void onInit() {
			vacuum = false;
			counter = 0;
			target = null;
			double d = Math.random();
			if (d < 0.25) {
				numCoins = 1;
			} else if (d < 0.75) {
				numCoins = 3;
			} else if (d < 0.88) {
				numCoins = 5;
			} else {
				numCoins = 10;
			}
			((ViolentEntity) this.action.entity).setAttackCapability(DMG[7]
					* numCoins / 10f, KNBK[7] * numCoins / 10f, PRTY[7], false);
		}

		@Override
		public boolean onUpdate() {
			if (!vacuum && this.action.getCurrentFrameNumber() == 1)
				this.action.setCurrentFrame(2);
			if (vacuum && this.action.getCurrentFrameNumber() == 2)
				return true;
			if (vacuum && counter < numCoins) {
				WaluigiCoin coin = new WaluigiCoin((Waluigi) this.action.entity);
				coin.setPosition(target.getPosition().x, target.getPosition().y);
				Main.getGameState().spawnEntity(coin);
				this.audio.playInstance();
				counter++;
			}
			return false;
		}

		@Override
		public boolean shouldFinish() {
			while (counter < numCoins && target != null) {
				WaluigiCoin coin = new WaluigiCoin((Waluigi) this.action.entity);
				coin.setPosition(target.getPosition().x, target.getPosition().y);
				Main.getGameState().spawnEntity(coin);
				this.audio.playInstance();
				counter++;
			}
			return true;
		}

		@Override
		public void cleanUp() {
			vacuum = false;
			target = null;
		}
	}
}
