package entities.characters;

import java.util.HashMap;
import java.util.Map;

import com.jme3.audio.AudioNode;
import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.TransformingCharacter;
import entities.TransformingCharacter.ActionTransform;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionHalfHop;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionSetFacing;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionWaitTillControl;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.projectiles.KamekFireShot;
import entities.projectiles.Projectile;
import entities.projectiles.ReggieExplosion;
import entities.projectiles.ReggieLaserShot;
import entities.projectiles.ReggieMothers;
import entities.projectiles.ReggieSystems;
import entities.projectiles.ReggieVillains;

/**
 * The character Reggie Fils-Aime within SSBS.
 * 
 * @author Matthew Ludwig
 */
public class Reggie extends TransformingCharacter {
	private int boost;
	private float imageSpeedModifier;
	public static final float[] STAT = CharacterStats.ReggieStats;
	public static final float[] DMG = CharacterStats.ReggieDamage;
	public static final float[] KNBK = CharacterStats.ReggieKnockback;
	public static final float[] PRTY = CharacterStats.ReggiePriority;
	public static final float[] ISPD = CharacterStats.ReggieImageSpeed;
	public static final short[][] SECT = CharacterStats.ReggieSectionFrames;

	public Reggie() {
		super("Reggie", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]),
				STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
		setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
		this.scale = 0.38f;
		this.boost = 0;
		this.imageSpeedModifier = 1;
		this.grabHitbox = 12;
        this.grabOffset = 40;
	}

	public void updateEntity() {
		super.updateEntity();
		if (this.isDead())
			boost = 0;
		PhysicsInfo p = this.physicsInfo;
		p.setWeight(STAT[0] * (float) Math.pow(0.95, boost) * statModifiers[0]);
		this.priorityModifier = STAT[4] * (float) Math.pow(1.05, boost) * statModifiers[4];
		this.knockbackModifier = STAT[5] * (float) Math.pow(1.12, boost) * statModifiers[5];
		this.setDamageModifier(STAT[6] * (float) Math.pow(1.07, boost) * statModifiers[6]);
		this.imageSpeedModifier = STAT[7] * (float) Math.pow(0.9, boost) * statModifiers[7];
	}
	
	public void onDeath(){
		super.onDeath();
		boost = 0;
	}

	public float logThing(float a, int b) {
		float f = 1;
		for (int i = 0; i < b; i++) {
			f *= (a + 1.0 * i) / (i + 1);
		}
		return f;
	}

	@Override
	public Kirby.KirbyAbility getKirbyAbility() {
		return Kirby.KirbyAbility.FIGHTER;
	}

	protected Map<String, CharacterAction> setUpSecondActionMap() {
		Map<String, CharacterAction> map = new HashMap<String, CharacterAction>();

		CharacterAction firstAction = new CharacterAction(this,
				"filsamechStand", StayingPower.NONE, .99f);
		map.put("Stand", firstAction);

		CharacterAction secondAction = new CharacterAction(this,
				"filsamechRun", StayingPower.BARELY, .99f);
		secondAction.addModifier(new ActionSetFacing(true));
		secondAction.addModifier(new ActionMovement(25f, 0f, 0f, true));
		secondAction.addModifier(new ActionWaitTillControl("Right", false,
				false));
		secondAction.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/finalSmashMove.ogg", 0));
		map.put("Walk Right", secondAction);
		map.put("Sprint Right", secondAction);

		CharacterAction thirdAction = new CharacterAction(this, "filsamechRun",
				StayingPower.BARELY, .99f);
		thirdAction.addModifier(new ActionSetFacing(false));
		thirdAction.addModifier(new ActionMovement(25f, 0f, 0f, true));
		thirdAction
				.addModifier(new ActionWaitTillControl("Left", false, false));
		thirdAction.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/finalSmashMove.ogg", 0));
		map.put("Walk Left", thirdAction);
		map.put("Sprint Left", thirdAction);

		CharacterAction jumpAction = new CharacterAction(this, "filsamechFly",
				StayingPower.BARELY, .99f);
		jumpAction.addModifier(new ActionMovement(25f, 90f, 0f, false))
				.addModifier(new ActionToggleAttribute(Attribute.VECTORING))
				.addModifier(new ActionWaitTillOnGround());
		jumpAction.addModifier(new JumpingAction());
		jumpAction.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/finalSmashMove.ogg", 0));
		map.put("Jump 1", jumpAction);
		map.put("Jump 2", jumpAction);

		CharacterAction fourthAction = new CharacterAction(this,
				"filsamechShoot", StayingPower.LOW, .99f);
		fourthAction.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/finalSmashMove.ogg", 0));
		fourthAction.addModifier(new ActionSpawnEntity(new short[] { 1 }, 99,
				ReggieLaserShot.class, this).setYOffset(8));
		fourthAction.addModifier(new ActionAttack(15.17f, 2.79f, 99.99f));
		fourthAction.addModifier(new LaserAction());
		map.put("Neutral A", fourthAction);

		CharacterAction fifthAction = new CharacterAction(this,
				"filsamechDead", StayingPower.EXTREME, .33f);
		fifthAction.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/finalSmashMove.ogg", 0));
		fifthAction.addModifier(new ActionTransform(0));
		map.put("Transform", fifthAction);

		CharacterAction sixthAction = new CharacterAction(this,
				"filsamechDrinkWater", StayingPower.LOW, .99f);
		sixthAction.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/finalSmashMove.ogg", 0));
		sixthAction.addModifier(new ActionAttack(15.17f, 2.79f, 99.99f));
		sixthAction.addModifier(new ActionSpawnEntity(new short[] { 35 }, 1,
				ReggieExplosion.class, this).setYOffset(-48));
		sixthAction.addModifier(new WaterAction());
		map.put("Neutral B", sixthAction);

		return map;
	}

	@Override
	protected CharacterAction getNeutralA() {
		CharacterAction action = new CharacterAction(this, "neutralA",
				StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
		action.addModifier(new NeutralAAction());
		action.addModifier(new ActionPressMultiHit("Attack"));
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/neutralA.ogg", 0));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_12.ogg", -1));
		return action;
	}

	@Override
	protected CharacterAction getSideA() {
		CharacterAction action = new CharacterAction(this, "sideA",
				StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], SECT[1]));
		action.addModifier(new ActionMovement(10.48f, 0f, 0, true));
		action.addModifier(new SideAAction());
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/sideA.ogg", 0));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_01.ogg", -1));
		return action;
	}

	@Override
	protected CharacterAction getUpA() {
		CharacterAction action = new CharacterAction(this, "upA",
				StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
		action.addModifier(new ActionHalfHop());
		action.addModifier(new UpAAction());
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/upA.ogg", 0));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_00.ogg", -1));
		return action;
	}

	@Override
	protected CharacterAction getDownA() {
		CharacterAction action = new CharacterAction(this, "downA",
				StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
		action.addModifier(new DownAAction());
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/downA.ogg", 0));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_04.ogg", -1));
		return action;
	}

	@Override
	protected CharacterAction getNeutralB() {
		CharacterAction action = new CharacterAction(this, "neutralB",
				StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new ActionAttack(DMG[4], KNBK[4], PRTY[4], SECT[4]));
		action.addModifier(new ActionToggleAttribute(Attribute.HEAVYARMOR));
		action.addModifier(new NeutralBAction());
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/neutralB.ogg", 0));
		return action;
	}

	@Override
	protected CharacterAction getSideB() {
		CharacterAction action = new CharacterAction(this, "sideB",
				StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionAttack(DMG[5], KNBK[5], PRTY[5], SECT[5]));
		action.addModifier(new SideBAction());
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/sideB.ogg", 0));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_13.ogg", -1));
		return action;
	}

	@Override
	protected CharacterAction getUpB() {
		CharacterAction action = new CharacterAction(this, "upB",
				StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
		action.addModifier(new UpBAction());
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/upB.ogg", 0));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_12.ogg", -1));
		return action;
	}

	@Override
	protected CharacterAction getDownB() {
		CharacterAction action = new CharacterAction(this, "downB",
				StayingPower.EXTREME, ISPD[7]);
		action.addModifier(new DownBAction());
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/downB.ogg", 0));
		return action;
	}

	@Override
	protected CharacterAction getFinalSmash() {
		CharacterAction action = new CharacterAction(this, "finalSmash",
				StayingPower.EXTREME, ISPD[8]);
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Reggie/finalSmash.ogg", 0));
		action.addModifier(new ActionTransform(20));
		return action;
	}

	@Override
	protected void overrideDefaults(Map<String, CharacterAction> actionMap) {
	}

	public class NeutralAAction extends ActionModifier {

		public NeutralAAction() {
		}

		@Override
		public void onInit() {
			this.action.updateLimit = this.action
					.scaleUpdateLimit(ISPD[0] * imageSpeedModifier);
		}

		@Override
		public boolean onUpdate() {
			return false;
		}

		@Override
		public boolean shouldFinish() {
			return true;
		}

		@Override
		public void cleanUp() {
		}
	}

	public class SideAAction extends ActionModifier {

		public SideAAction() {
		}

		@Override
		public void onInit() {
			this.action.updateLimit = this.action
					.scaleUpdateLimit(ISPD[1] * imageSpeedModifier);
		}

		@Override
		public boolean onUpdate() {
			return false;
		}

		@Override
		public boolean shouldFinish() {
			return true;
		}

		@Override
		public void cleanUp() {
		}
	}

	public class UpAAction extends ActionModifier {

		public UpAAction() {
		}

		@Override
		public void onInit() {
			this.action.updateLimit = this.action
					.scaleUpdateLimit(ISPD[2] * imageSpeedModifier);
		}

		@Override
		public boolean onUpdate() {
			return false;
		}

		@Override
		public boolean shouldFinish() {
			return true;
		}

		@Override
		public void cleanUp() {
		}
	}

	public class DownAAction extends ActionModifier {

		public DownAAction() {
		}

		@Override
		public void onInit() {
			this.action.updateLimit = this.action
					.scaleUpdateLimit(ISPD[3] * imageSpeedModifier);
		}

		@Override
		public boolean onUpdate() {
			return false;
		}

		@Override
		public boolean shouldFinish() {
			return true;
		}

		@Override
		public void cleanUp() {
		}
	}

	public class NeutralBAction extends ActionModifier {

		public NeutralBAction() {
		}

		@Override
		public void onInit() {
			this.action.updateLimit = this.action
					.scaleUpdateLimit(ISPD[4] * imageSpeedModifier);
		}

		@Override
		public boolean onUpdate() {
			return false;
		}

		@Override
		public boolean shouldFinish() {
			return true;
		}

		@Override
		public void cleanUp() {
			boost++;
			//((PlayableCharacter) this.action.entity).setFinalSmash(true);
		}
	}

	public class SideBAction extends ActionModifier {
		private Angle angle;

		public SideBAction() {
		}

		@Override
		public void onInit() {
			this.action.updateLimit = this.action
					.scaleUpdateLimit(ISPD[5] * imageSpeedModifier);
			this.angle = new Angle(45);
			this.action.entity.setPureXVelocity(Utilities.getGeneralUtility()
					.getBooleanAsSign(this.action.entity.getFacing())
					* this.angle.cos() * 27f);
			this.action.entity.setPureYVelocity(this.angle.sin() * 27f);
		}

		@Override
		public boolean onUpdate() {
			if (this.action.entity.isOnGround()) {
				boolean pressingLeft = ((PlayableCharacter) this.action.entity)
						.checkControl("Left", false);
				boolean pressingRight = ((PlayableCharacter) this.action.entity)
						.checkControl("Right", false);
				boolean pressingUp = ((PlayableCharacter) this.action.entity)
						.checkControl("Up", false);
				boolean pressingDown = ((PlayableCharacter) this.action.entity)
						.checkControl("Down", false);

				if (pressingUp && pressingDown) {
					// nothing
				} else if (pressingUp) {
					this.angle = new Angle((this.angle.getValue() + 90) / 2);
				} else if (pressingDown) {
					this.angle = new Angle((this.angle.getValue() + 0) / 2);
				}

				if (this.action.entity.getFacing() && pressingLeft) {
					this.action.entity.setFacing(false);
				} else if (!this.action.entity.getFacing() && pressingRight) {
					this.action.entity.setFacing(true);
				}

				this.action.entity.setPureXVelocity(Utilities
						.getGeneralUtility().getBooleanAsSign(
								this.action.entity.getFacing())
						* this.angle.cos() * 27f);
				this.action.entity.setPureYVelocity(this.angle.sin() * 27f);
			}
			if (this.action.entity.getFacing()) {
				((Reggie) this.action.entity).rotateUpTo(new Angle(
						this.action.entity.getVelocity().angleBetween(
								new Vector2f(1, 0))
								* -57.2958f - 90));
			} else {
				((Reggie) this.action.entity).rotateUpTo(new Angle(
						this.action.entity.getVelocity().angleBetween(
								new Vector2f(1, 0)) * 57.2958f + 90));
			}

			return false;
		}

		@Override
		public boolean shouldFinish() {
			return true;
		}

		@Override
		public void cleanUp() {
			((Reggie) this.action.entity).rotateUpTo(new Angle(0));
			this.action.entity.setPureXVelocity(0);
			this.action.entity.setPureYVelocity(0);
		}
	}

	public class UpBAction extends ActionModifier {
		private float counter;
		private Angle angle;
		private boolean shouldFinish;

		public UpBAction() {
			this.counter = 0;
			this.shouldFinish = false;
		}

		@Override
		public void onInit() {
			this.action.updateLimit = this.action
					.scaleUpdateLimit(ISPD[6] * imageSpeedModifier);
			this.angle = new Angle(30f);
		}

		@Override
		public boolean onUpdate() {
			if (this.counter < 3) {
				boolean pressingLeft = ((PlayableCharacter) this.action.entity)
						.checkControl("Left", false);
				boolean pressingRight = ((PlayableCharacter) this.action.entity)
						.checkControl("Right", false);

				if (pressingLeft && pressingRight) {
					// This is what happens when you press both. Nothing!
				} else if (pressingLeft) {
					if (this.action.entity.getFacing()) {
						this.angle.add(120 * 1.33f * Utilities.lockedTPF);
					} else {
						this.angle.add(-300 * 1.33f * Utilities.lockedTPF);
					}
				} else if (pressingRight) {
					if (this.action.entity.getFacing()) {
						this.angle.add(-300 * 1.33f * Utilities.lockedTPF);
					} else {
						this.angle.add(120 * 1.33f * Utilities.lockedTPF);
					}
				}

				if (this.action.entity.getFacing()) {
					((Reggie) this.action.entity).rotateUpTo(new Angle(
							this.angle.getValue()));
				} else {
					((Reggie) this.action.entity).rotateUpTo(new Angle(
							this.angle.getValue()));
				}
				this.action.entity.setPureXVelocity(Utilities
						.getGeneralUtility().getBooleanAsSign(
								this.action.entity.getFacing())
						* this.angle.cos() * 27f);
				this.action.entity.setPureYVelocity(this.angle.sin() * 27f);
				this.counter += Utilities.lockedTPF;

				if (this.action.entity.hasCollidedWithObstacle(true)
						|| this.action.entity.hasCollidedWithObstacle(false)) {
					this.shouldFinish = true;
				}
			} else {
				this.shouldFinish = true;
			}

			return this.shouldFinish;
		}

		@Override
		public boolean shouldFinish() {
			return this.shouldFinish;
		}

		@Override
		public void cleanUp() {
			this.counter = 0;
			this.shouldFinish = false;
			this.action.entity.setPureXVelocity(0);
			this.action.entity.setPureYVelocity(0);
			((Reggie) this.action.entity).rotateUpTo(new Angle(0));
		}
	}

	public class DownBAction extends ActionModifier {
		int type;
		Projectile[] spawnedEntities;

		public DownBAction() {
			type = 0;
			spawnedEntities = new Projectile[2];
		}

		@Override
		public void onInit() {
			this.action.updateLimit = this.action
					.scaleUpdateLimit(ISPD[4] * imageSpeedModifier);
			cleanUpSpawns();
			float x = this.action.entity.getPosition().x;
			float y = this.action.entity.getPosition().y;
			if (findFreeSpot() == 0 && findFreeSpot2() == 1) {
				System.out.println("What?");
				if (type == 0) {
					ReggieSystems system = new ReggieSystems(
							(Reggie) this.action.entity, false);
					system.setPosition(x - 10, y + 30);
					Main.getGameState().spawnEntity(system);
					spawnedEntities[findFreeSpot()] = system;
					system = new ReggieSystems((Reggie) this.action.entity,
							true);
					system.setPosition(x + 10, y + 30);
					Main.getGameState().spawnEntity(system);
					spawnedEntities[findFreeSpot2()] = system;
				}
				if (type == 1) {
					ReggieVillains villain = new ReggieVillains(
							(Reggie) this.action.entity, false);
					villain.setPosition(x - 10, y + 30);
					Main.getGameState().spawnEntity(villain);
					spawnedEntities[findFreeSpot()] = villain;
					villain = new ReggieVillains((Reggie) this.action.entity,
							true);
					villain.setPosition(x + 10, y + 30);
					Main.getGameState().spawnEntity(villain);
					spawnedEntities[findFreeSpot2()] = villain;
				}
				if (type == 2) {
					ReggieMothers mother = new ReggieMothers(
							(Reggie) this.action.entity, false);
					mother.setPosition(x - 10, y + 30);
					Main.getGameState().spawnEntity(mother);
					spawnedEntities[findFreeSpot()] = mother;
					mother = new ReggieMothers((Reggie) this.action.entity,
							true);
					mother.setPosition(x + 10, y + 30);
					Main.getGameState().spawnEntity(mother);
					spawnedEntities[findFreeSpot2()] = mother;
				}

				type++;
				if (type > 2)
					type = 0;
			}
		}

		private void cleanUpSpawns() {
			for (int count = 0; count < spawnedEntities.length; count++) {
				if (this.spawnedEntities[count] != null
						&& this.spawnedEntities[count].isDead()) {
					this.spawnedEntities[count] = null;
				}
			}
		}

		private int findFreeSpot() {
			for (int count = 0; count < spawnedEntities.length; count++) {
				if (this.spawnedEntities[count] == null) {
					return count;
				}
			}

			return -1;
		}

		private int findFreeSpot2() {
			for (int count = spawnedEntities.length - 1; count >= 0; count--) {
				if (this.spawnedEntities[count] == null) {
					return count;
				}
			}

			return -1;
		}

		@Override
		public boolean onUpdate() {
			return false;
		}

		@Override
		public boolean shouldFinish() {
			return true;
		}

		@Override
		public void cleanUp() {
		}
	}

	public class JumpingAction extends ActionModifier {

		public JumpingAction() {
		}

		@Override
		public void onInit() {
		}

		@Override
		public boolean onUpdate() {
			if (((PlayableCharacter) this.action.entity).checkControl("Up",
					true)) {
				this.action.entity.setYVelocity(25, true);
				this.action.setCurrentFrame(0);
			}

			return false;
		}

		@Override
		public boolean shouldFinish() {
			return true;
		}

		@Override
		public void cleanUp() {
		}
	}

	public class LaserAction extends ActionModifier {

		int counter2;

		public LaserAction() {
		}

		@Override
		public void onInit() {
			counter2 = 0;
		}

		@Override
		public boolean onUpdate() {
			counter2++;
			if (counter2 > 0.25 / Utilities.lockedTPF) {
				float x = ((Reggie)this.action.entity).getRealBounds().getExactCenter().x;
				float y = ((Reggie)this.action.entity).getRealBounds().getExactCenter().y;
				ReggieLaserShot laser = new ReggieLaserShot(
						(Reggie) this.action.entity);
				laser.setPosition(x, y+8);
				Main.getGameState().spawnEntity(laser);
				counter2 = 0;
			}
			return false;
		}

		@Override
		public boolean shouldFinish() {
			if (((PlayableCharacter) this.action.entity).checkControl("Attack",
					false)) {
				return false;
			}
			return true;
		}

		@Override
		public void cleanUp() {
		}
	}
	
	public class WaterAction extends ActionModifier {

		int frame;

		public WaterAction() {
			frame = 0;
		}

		@Override
		public void onInit() {
			this.action.setCurrentFrame(frame);
		}

		@Override
		public boolean onUpdate() {
			frame = this.action.getCurrentFrameNumber();
			return false;
		}

		@Override
		public boolean shouldFinish() {
			return true;
		}

		@Override
		public void cleanUp() {
			((Reggie) this.action.entity).transformationTimer = Utilities.lockedTPF;
		}
	}
}
