package entities.characters;

public class CharacterStats {
	//redo everyone's damages and damage stat
	public static final float[] MarioStats = {1.06f, 1f, 1f, 1.17f, 1f, 1f, 0.8f, 1f, 1.39f};
	public static final float[] MarioDamage = {5.03f, 21.36f, 7.85f, 15.08f, 6.28f, 8.80f, 3.77f, 4.22f, 3.77f, 10.05f, 13.82f, 8.80f, 6.28f};
	public static final float[] MarioKnockback = {0.63f, 2.02f, 1.26f, 1.44f, 0.25f, 0f, 1.12f, 1.26f, 3.37f, 0.84f, 1.26f, 1.12f, 0.78f};
	public static final float[] MarioPriority = {6.55f, 10.27f, 4.12f, 2.12f, 2.39f, 999f, 1.73f, 0.83f, 99.99f};
	public static final float[] MarioImageSpeed = {1.16f, 2.33f, 2.33f, 1.16f, 2.33f, 1.20f, 0.33f, 1.16f, 0.55f};
	public static final short[][] MarioSectionFrames = {{0,3,6}, {0}, {0}, {0}, {0}, {0}, {0,0,0,0,0,0}, {0,2,4,6,8}, {0}};
	
	public static final float[] LinkStats = {1.14f, 1.04f, 0.96f, 0.97f, 2.17f, 1.06f, 1.17f, 1.43f, 0.86f};
	public static final float[] LinkDamage = {5.41f, 15.14f, 11.89f, 19.46f, 7.57f, 7.57f, 4.32f, 9.73f, 42.17f, 7.57f, 7.57f, 7.57f, 7.57f};
	public static final float[] LinkKnockback = {0.52f, 1.78f, 1.27f, 1.49f, 0.81f, 0.52f, 1.49f, 0.69f, 8.92f, 0.81f, 0.74f, 1.27f, 0.59f};
	public static final float[] LinkPriority = {1.08f, 7.56f, 3.96f, 9.72f, 1.73f, 1.21f, 4.32f, 2.43f, 99.99f};
	public static final float[] LinkImageSpeed = {0.85f, 0.75f, 0.24f, 0.57f, 3.38f, 2.26f, 1.69f, 2.26f, 2.41f};
	public static final short[][] LinkSectionFrames = {{0,2,6}, {0,4}, {0}, {0}, {0,0,0}, {0,1,2,3}, {0,1,3,5,7,9}, {0,27,31}, {0}};
	
	public static final float[] SamusStats = {1.1f, 0.96f, 0.88f, 0.97f, 2.3f, 0.82f, 0.96f, 1f, 1.39f};
	public static final float[] SamusDamage = {7.99f, 12.97f, 5.99f, 11.98f, 24.95f, 4.99f, 1f, 4.99f, 5.49f, 8.98f, 7.99f, 8.98f, 5.99f};
	public static final float[] SamusKnockback = {1f, 1.29f, 1.80f, 1.13f, 1.80f, 0.23f, 1.50f, 0.29f, 9.01f, 0.75f, 0.75f, 0.82f, 0.64f};
	public static final float[] SamusPriority = {2.16f, 4.67f, 6.47f, 4.31f, 8.99f, 2.16f, 1.44f, 1.80f, 99.99f};
	public static final float[] SamusImageSpeed = {1.04f, 1.24f, 2.59f, 1.94f, 0.2f, 0.31f, 1.56f, 3.11f, 1.16f};
	public static final short[][] SamusSectionFrames = {{0,3}, {0}, {0,3,5,7}, {0}, {0}, {0}, {0,0,1,2,2,3,4,4,5,6,6,7}, {0}, {0}};
	
	public static final float[] KirbyStats = {1.03f, 0.96f, 1.21f, 1.12f, 1.07f, 1.04f, 1.02f, 1.17f, 1.12f};
	public static final float[] KirbyDamage = {2.57f, 6.86f, 7.71f, 5.14f, 23.99f, 18f, 5.14f, 15.42f, 13.71f, 4.28f, 6.86f, 8.57f, 9.43f};
	public static final float[] KirbyKnockback = {0.68f, 0.68f, 0.91f, 0.21f, 2.11f, 1.64f, 0.25f, 1.64f, 2.05f, 0.91f, 1.03f, 1.03f, 0.91f};
	public static final float[] KirbyPriority = {0.1f, 4.78f, 3.82f, 0.42f, 6.5f, 9.77f, 0.78f, 3.82f, 99.99f};
	public static final float[] KirbyImageSpeed = {0.68f, 2.11f, 0.85f, 0.36f, 1.9f, 2.11f, 1.89f, 2.11f, 0.55f};
	public static final short[][] KirbySectionFrames = {{0,5}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}};
	
	public static final float[] LuigiStats = {0.99f, 0.99f, 0.94f, 0.93f, 1.31f, 0.99f, 1f, 1.19f, 0.82f};
	public static final float[] LuigiDamage = {5.02f, 8.03f, 6.02f, 15.05f, 6.02f, 10.03f, 25.08f, 3.01f, 1504.86f, 9.03f, 10.03f, 8.03f, 6.02f};
	public static final float[] LuigiKnockback = {0.67f, 0.73f, 1.01f, 1.34f, 0.20f, 0.81f, 2.68f, 1.01f, 2013.22f, 0.73f, 1.01f, 1.01f, 0.84f};
	public static final float[] LuigiPriority = {1.74f, 1.31f, 3.50f, 7.54f, 1.68f, 10.96f, 3.29f, 1.99f, 99.99f};
	public static final float[] LuigiImageSpeed = {0.59f/1.2f, 0.75f, 0.42f, 0.42f, 4.32f, 1.32f, 2.85f, 1.32f, 0.55f};
	public static final short[][] LuigiSectionFrames = {{0,2,4}, {0}, {0}, {0}, {0}, {0}, {0}, {0,4}, {0}};
	
	public static final float[] NessStats = {0.74f, 1.05f, 1.08f, 0.95f, 2.12f, 1.32f, 0.98f, 1.08f, 1.20f};
	public static final float[] NessDamage = {4.09f, 22.48f, 13.28f, 4.08f, 9.19f, 6.13f, 12.26f, 0.00f, 15.32f, 11.24f, 11.24f, 10.22f, 7.15f};
	public static final float[] NessKnockback = {0.73f, 2.37f, 1.19f, 0.24f, 1.06f, 1.19f, 0.79f, 0.34f, 4.75f, 0.28f, 1.9f, 1.19f, 0.73f};
	public static final float[] NessPriority = {2.65f, 6.96f, 10.64f, 0.73f, 1.2f, 6.92f, 2.19f, 0.72f, 99.99f};
	public static final float[] NessImageSpeed = {1.21f, 0.94f, 1.72f, 0.33f, 2.38f, 1.34f, 1.98f, 2.10f, 0.75f};
	public static final short[][] NessSectionFrames = {{0,4,7,14}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0,13,18,19,21,22,24,25}};
	
	public static final float[] CaptainFalconStats = {0.92f, 1.16f, 0.96f, 1.49f, 1.16f, 1.34f, 1.00f, 0.90f, 1.09f};
	public static final float[] CaptainFalconDamage = {3.95f, 7.11f, 15.01f, 14.22f, 19.74f, 7.11f, 9.48f, 10.27f, 3.95f, 7.11f, 7.11f, 5.53f, 4.74f};
	public static final float[] CaptainFalconKnockback = {0.72f, 0.72f, 1.62f, 1.30f, 1.62f, 1.30f, 0.81f, 1.30f, 1.30f, 0.65f, 0.81f, 0.81f, 0.36f};
	public static final float[] CaptainFalconPriority = {3.73f, 2.87f, 4.91f, 1.54f, 9.04f, 1.17f, 5.50f, 3.23f, 99.99f};
	public static final float[] CaptainFalconImageSpeed = {1.08f, 0.17f, 2.15f, 1.08f, 4.31f, 2.87f, 0.13f, 0.21f, 2.6f};
	public static final short[][] CaptainFalconSectionFrames = {{0,2,4,8,10,12,14}, {0}, {0}, {0,5}, {0}, {0}, {0}, {0}};
	
	public static final float[] SonicStats = {0.70f, 0.96f, 0.83f, 3.67f, 0.42f, 0.77f, 0.84f, 0.93f, 1.19f};
	public static final float[] SonicDamage = {4.79f, 11.97f, 7.18f, 3.59f, 2.39f, 44.30f, 4.79f, 0.00f, 19.16f, 8.38f, 8.38f, 7.18f, 8.38f};
	public static final float[] SonicKnockback = {0.76f, 1.14f, 1.01f, 0.91f, 0.83f, 3.03f, 0.57f, 0.00f, 1.82f, 1.14f, 1.14f, 1.14f, 0.35f};
	public static final float[] SonicPriority = {2.77f, 1.28f, 10.85f, 6.54f, 3.66f, 1.91f, 1.00f, 999f, 99.99f};
	public static final float[] SonicImageSpeed = {1.13f, 0.45f, 3.37f, 1.8f, 2.24f, 0.45f, 0.07f, 2.49f, 1.95f};
	public static final short[][] SonicSectionFrames = {{0,3,6}, {0}, {0}, {0,3,5,8}, {0}, {0}, {0}, {0}, {0}};
	
	public static final float[] MewtwoStats = {0.80f, 0.80f, 1.09f, 0.80f, 2.09f, 1.20f, 1.09f, 0.56f, 1.08f};
	public static final float[] MewtwoDamage = {3.68f, 9.20f, 11.96f, 13.80f, 23.00f, 8.33f, 0f, 0.92f, 27.60f, 11.96f, 9.20f, 11.04f, 8.28f};
	public static final float[] MewtwoKnockback = {0.7f, 1.32f, 1.32f, 2.11f, 2.11f, 0.00f, 0f, 0f, 5.27f, 0.66f, 1.51f, 1.76f, 0.53f};
	public static final float[] MewtwoPriority = {1.49f, 1.03f, 10.46f, 5.63f, 0.90f, 3.02f, 2.61f, 6.86f, 99.99f};
	public static final float[] MewtwoImageSpeed = {0.72f, 1.44f, 1.09f, 1.09f, 1.09f, 2.19f, 2.19f, 2.19f, 0.9f};
	public static final short[][] MewtwoSectionFrames = {{0,2,4}, {0,2}, {0}, {0}, {0}, {0}, {0}, {0}, {0}};
	
	public static final float[] MarthStats = {0.46f, 1.15f, 0.97f, 1.17f, 0.90f, 2.00f, 2.10f, 1.22f, 0.92f};
	public static final float[] MarthDamage = {6.19f, 12.37f, 12.37f, 18.56f, 24.75f, 8.25f, 11.34f, 0.00f, 61.87f, 4.12f, 4.12f, 4.12f, 5.16f};
	public static final float[] MarthKnockback = {0.56f, 0.61f, 1.12f, 2.24f, 3.36f, 0.96f, 0.96f, 0f, 16.79f, 0.34f, 0.42f, 0.96f, 0.48f};
	public static final float[] MarthPriority = {6.92f, 7.92f, 3.47f, 3.75f, 1.77f, 1.45f, 2.72f, 999f, 99.99f};
	public static final float[] MarthImageSpeed = {0.87f, 2.80f, 1.71f, 1.71f, 0.68f, 0.76f, 1.71f, 1.75f, 0.55f};
	public static final short[][] MarthSectionFrames = {{0,2,4,6}, {0}, {0}, {0}, {0}, {0,4,7,11}, {0}, {0}, {0}};
	
	public static final float[] MegaManStats = {1.04f, 1.04f, 0.95f, 1.49f, 1.74f, 1.01f, 0.89f, 1.11f, 1.3f};
	public static final float[] MegaManDamage = {3.03f, 25.76f, 4.55f, 12.12f, 4.55f, 12.12f, 0f, 3.03f, 9.09f, 12.12f, 16.67f, 10.61f, 6.82f};
	public static final float[] MegaManKnockback = {0.26f, 3.09f, 0.95f, 1.37f, 0.39f, 0.26f, 0f, 0.62f, 6.18f, 1.12f, 1.54f, 1.37f, 1.03f};
	public static final float[] MegaManPriority = {1.12f, 8.11f, 2.73f, 3.08f, 2.28f, 2.43f, 1.09f, 11.16f, 99.99f};
	public static final float[] MegaManImageSpeed = {0.74f, 1.94f, 1.83f, 0.69f, 4.1f, 0.54f, 0.15f, 2.01f, 0.55f};
	public static final short[][] MegaManSectionFrames = {{0,2,4}, {0}, {0,2,4,6,8,10}, {0}, {0,1}, {0}, {0}, {0,5,7,9}, {0}};
	
	public static final float[] KingDededeStats = {1.45f, 1.16f, 1.08f, 0.98f, 1.09f, 1.13f, 1.50f, 0.80f, 1.1f};
	public static final float[] KingDededeDamage = {15.91f, 10.61f, 3.31f, 8.62f, 3.31f, 9.28f, 9.94f, 25.19f, 26.51f, 6.63f, 8.62f, 5.97f, 3.98f};
	public static final float[] KingDededeKnockback = {1.95f, 1.46f, 1.17f, 1.17f, 0.00f, 0.97f, 0.83f, 1.95f, 1.95f, 0.53f, 0.73f, 0.58f, 0.65f};
	public static final float[] KingDededePriority = {9.85f, 3.28f, 0.75f, 2.67f, 99.99f, 3.28f, 1.76f, 10.40f, 99.99f};
	public static final float[] KingDededeImageSpeed = {1.83f, 3.63f, 0.95f, 1.86f, 0.00f, 0.22f, 0.92f, 2.58f, 1.16f};
	public static final short[][] KingDededeSectionFrames = {{0}, {0,5}, {0,2,3,4,5,6,7,8}, {0,3}, {0}, {0}, {0,7}, {0}, {0}};
	
	public static final float[] WaluigiStats = {1.11f, 0.95f, 1.03f, 0.92f, 0.66f, 1.07f, 1.15f, 1.40f, 1.02f};
	public static final float[] WaluigiDamage = {2.61f, 11.31f, 5.22f, 13.05f, 21.75f, 2.61f, 10.44f, 6.96f, 10.44f, 10.44f, 10.44f, 9.57f, 6.96f};
	public static final float[] WaluigiKnockback = {0.57f, 1.23f, 0.92f, 1.23f, 2.46f, 0.67f, 1.84f, 0.53f, 2.46f, 0.92f, 0.67f, 0.82f, 0.15f};
	public static final float[] WaluigiPriority = {9.48f, 2.46f, 0.93f, 5.1f, 6.84f, 5.33f, 0.93f, 0.93f, 99.99f};
	public static final float[] WaluigiImageSpeed = {0.65f, 0.73f, 0.97f, 1.93f, 1.34f, 3.87f, 2.13f, 0.39f, 1.5f};
	public static final short[][] WaluigiSectionFrames = {{0,4,8}, {0}, {0}, {0}, {0}, {0}, {0}, {0}, {0}};
	
	public static final float[] KrystalStats = {0.81f, 1.13f, 1.10f, 0.97f, 1.77f, 1.36f, 1.11f, 0.99f, 0.96f};
	public static final float[] KrystalDamage = {3.48f, 20.18f, 4.87f, 13.92f, 4.18f, 6.96f, 11.83f, 4.87f, 20.88f, 9.74f, 11.83f, 11.14f, 8.35f};
	public static final float[] KrystalKnockback = {0.81f, 1.90f, 1.42f, 1.90f, 0.14f, 1.14f, 0.71f, 0.24f, 1.62f, 0.95f, 0.63f, 1.14f, 1.03f};
	public static final float[] KrystalPriority = {1.22f, 9.45f, 3.53f, 4.05f, 0.97f, 2.43f, 8.73f, 1.62f, 99.99f};
	public static final float[] KrystalImageSpeed = {0.75f, 1.27f, 1.4f, 2.55f, 1.71f, 1.27f, 0.89f, 2.16f, 0.55f};
	public static final short[][] KrystalSectionFrames = {{0,3,6}, {0}, {0,3,4,5}, {0}, {0}, {0}, {0,11,12,13}, {0}, {0}};
	
	public static final float[] RidleyStats = {1.48f, 1.02f, 1.00f, 1.01f, 1.20f, 1.02f, 0.9f, 0.95f, 0.89f};
	public static final float[] RidleyDamage = {4.12f, 2.47f, 19.80f, 9.07f, 4.95f, 2.47f, 7.42f, 19.80f, 26.73f, 9.90f, 8.25f, 18.15f, 4.95f};
	public static final float[] RidleyKnockback = {0.81f, 0.56f, 1.88f, 0.63f, 0.26f, 0.71f, 0.94f, 2.26f, 1.92f, 0.63f, 0.71f, 2.26f, 0.38f};
	public static final float[] RidleyPriority = {3.60f, 2.68f, 3.72f, 4.99f, 0.34f, 3.43f, 4.74f, 8.50f, 99.99f};
	public static final float[] RidleyImageSpeed = {0.74f, 3.46f, 0.54f, 1.82f, 0.85f, 0.45f, 2.01f, 2.14f, 0.85f};
	public static final short[][] RidleySectionFrames = {{0,2,4}, {0,9,13,17,21}, {0}, {0,1,2,3,4,5,6,7}, {0}, {0,1,2,3,4,5,6,7}, {0,6,12}, {0}, {0}};
	
	public static final float[] ReggieStats = {1.69f, 0.96f, 1.08f, 1.55f, 0.20f, 0.57f, 0.92f, 1.19f, 1.16f};
	public static final float[] ReggieDamage = {4.89f, 15.29f, 3.28f, 15.29f, 0.00f, 7.10f, 9.83f, 15.29f, 32.76f, 9.83f, 13.10f, 7.64f, 9.83f};
	public static final float[] ReggieKnockback = {0.44f, 1.74f, 1.09f, 1.24f, 0.00f, 1.24f, 1.24f, 1.45f, 8.71f, 0.58f, 0.79f, 1.09f, 1.09f};
	public static final float[] ReggiePriority = {7.95f, 4.01f, 2.14f, 5.19f, 0.00f, 1.90f, 3.63f, 7.18f, 99.99f};
	public static final float[] ReggieImageSpeed = {0.82f, 0.49f, 2.34f, 0.61f, 2.34f, 0.73f, 2.34f, 2.34f, 2.34f};
	public static final short[][] ReggieSectionFrames = {{0,3,6}, {0}, {0,9,11,13,15,17}, {0}, {0}, {0}, {0}, {0}, {0}};
	
	public static final float[] GhirahimStats = {0.69f, 1.15f, 1.10f, 2.15f, 0.43f, 1.18f, 0.82f, 1.16f, 1.51f};
	public static final float[] GhirahimDamage = {3.71f, 6.68f, 14.11f, 13.36f, 18.56f, 6.68f, 8.91f, 9.65f, 3.71f, 9.65f, 7.42f, 7.42f, 5.20f};
	public static final float[] GhirahimKnockback = {0.61f, 0.61f, 1.12f, 0.96f, 1.12f, 0.96f, 0.67f, 0.96f, 1.35f, 1.12f, 1.68f, 1.68f, 0.48f};
	public static final float[] GhirahimPriority = CaptainFalconPriority;
	public static final float[] GhirahimImageSpeed = CaptainFalconImageSpeed;
	public static final short[][] GhirahimSectionFrames = CaptainFalconSectionFrames;
	
	public static final float[] KingKRoolStats = {1.24f, 1.23f, 0.96f, 1.05f, 0.93f, 1.22f, 1.23f, 0.92f, 0.86f};
	public static final float[] KingKRoolDamage = {4.86f, 17.02f, 8.91f, 5.67f, 14.58f, 8.10f, 10.89f, 11.34f, 16.20f, 8.10f, 8.91f, 7.29f, 5.67f};
	public static final float[] KingKRoolKnockback = {0.65f, 1.47f, 0.84f, 0.98f, 0.98f, 1.47f, 1.96f, 1.17f, 1.47f, 0.49f, 0.73f, 0.84f, 0.42f};
	public static final float[] KingKRoolPriority = {4.91f, 3.78f, 2.12f, 1.49f, 2.13f, 2.58f, 11.09f, 3.90f, 9.99f};
	public static final float[] KingKRoolImageSpeed = {1.17f, 1.17f, 1.17f, 1.21f, 0.16f, 2.42f, 2.35f, 2.35f, 0.16f};
	public static final short[][] KingKRoolSectionFrames = {{0,6,13}, {0}, {0,2,4}, {0}, {0,0,0}, {0,6,12}, {0,1,3,4,5,7}, {0}, {0,0,0}};
	
	public static final float[] GenoStats = {1.05f, 0.96f, 0.94f, 1.09f, 1.40f, 0.87f, 0.99f, 0.97f, 1.57f};
	public static final float[] GenoDamage = {4.03f, 11.09f, 8.06f, 17.13f, 19.15f, 6.05f, 4.03f, 5.04f, 12.08f, 10.08f, 10.08f, 10.08f, 6.55f};
	public static final float[] GenoKnockback = {0.55f, 0.83f, 0.94f, 1.32f, 1.65f, 0.73f, 0.94f, 1.10f, 3.31f, 0.83f, 1.32f, 0.94f, 0.83f};
	public static final float[] GenoPriority = {4.38f, 3.04f, 5.48f, 0.19f, 7.48f, 6.42f, 0.18f, 4.84f, 99.99f};
	public static final float[] GenoImageSpeed = {0.73f, 1.38f, 2.28f, 1.14f, 1.14f, 2.28f, 1.14f, 1.90f, 1f};
	public static final short[][] GenoSectionFrames = {{0,3,7}, {0}, {0}, {0}, {0}, {0}, {0}, {0,14,15,17,18}, {0}};
	
	public static final float[] KamekStats = {0.98f, 1.00f, 1.11f, 0.97f, 1.87f, 1.24f, 0.5f, 1.07f, 1.13f};
	public static final float[] KamekDamage = {3.91f, 7.81f, 11.72f, 14.65f, 21.49f, 3.91f, 8.79f, 16.61f, 0.00f, 6.84f, 6.84f, 4.88f, 3.91f};
	public static final float[] KamekKnockback = {0.78f, 0.89f, 1.55f, 1.04f, 1.04f, 0.13f, 0.97f, 1.24f, 0f, 0.89f, 1.04f, 1.55f, 0.89f};
	public static final float[] KamekPriority = {1.17f, 3.14f, 5.10f, 5.70f, 0.48f, 3.29f, 6.43f, 6.69f, 99.99f};
	public static final float[] KamekImageSpeed = {1.78f, 1.95f, 1.82f, 1.82f, 3.63f, 0.49f, 0.03f, 0.49f, 0.23f};
	public static final short[][] KamekSectionFrames = {{0,6}, {0}, {0}, {0}, {0,3,6}, {0,0,1,1,1,2,2,3,3,3,4,4}, {0}, {0}, {0}};
	
	public static final float[] ToadStats = {1.14f, 1.02f, 0.83f, 1.23f, 1.58f, 0.82f, 1.37f, 0.93f, 0.73f};
	public static final float[] ToadDamage = {2.33f, 15.11f, 11.63f, 8.53f, 3.10f, 10.85f, 8.53f, 20.93f, 20.47f, 8.68f, 1.45f, 11.57f, 8.68f};
	public static final float[] ToadKnockback = {0.47f, 2.22f, 1.33f, 1.33f, 0.89f, 0.83f, 0.55f, 1.66f, 2.22f, 0.83f, 0.55f, 0.66f, 0.66f};
	public static final float[] ToadPriority = {0.98f, 10.62f, 6.53f, 3.42f, 0.73f, 6.53f, 1.71f, 1.47f, 99.99f};
	public static final float[] ToadImageSpeed = {0.60f, 2.25f, 1.52f, 1.54f, 0.79f, 2.42f, 1.20f, 1.69f, 1.1f};
	public static final short[][] ToadSectionFrames = {{0,2,4,6,7}, {0,27,31,35,39,43,47,51,55}, {0}, {0}, {0}, {0}, {0}, {0}, {0}};
	
	public static final float[] ProfessorLaytonStats = {1.02f, 1.21f, 1.00f, 1.63f, 1.47f, 0.86f, 1.29f, 1.12f, 0.90f};
	public static final float[] ProfessorLaytonDamage = {4f, 7f, 20.01f, 18.01f, 3f, 2.33f, 14.62f, 7.50f, 19.49f, 10.61f, 2.28f, 13.57f, 8.43f};
	public static final float[] ProfessorLaytonKnockback = {0.58f, 0.79f, 2.38f, 0.89f, 0.79f, 0.59f, 1.10f, 1.02f, 2.38f, 1.19f, 0.89f, 0.89f, 0.89f};
	public static final float[] ProfessorLaytonPriority = {0.60f, 0.76f, 7.98f, 1.11f, 0.27f, 999f, 6.50f, 10.78f, 99.99f};
	public static final float[] ProfessorLaytonImageSpeed = {0.78f, 0.41f, 2.69f, 0.41f, 1.25f, 1.13f, 0.43f, 4.90f, 0.25f};
	public static final short[][] ProfessorLaytonSectionFrames = {{0,2,4}, {0}, {0}, {0}, {0}, {0}, {0,1}, {0}, {0}};
}
