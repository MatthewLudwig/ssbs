package entities.characters;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.LinearFunction;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionCompleteNumberOfAnimations;
import entities.characters.actions.ActionFinalSmashFreeze;
import entities.characters.actions.ActionHalfHop;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.projectiles.SamusChargeShot;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.projectiles.SamusBomb;
import entities.projectiles.SamusMissile;
import entities.projectiles.SamusZeroLaser;

import java.lang.reflect.Constructor;
import java.util.Map;

import physics.BoundingBox;
import physics.PhysicsModifier;

/**
 * The character Samus within SSBS.
 *
 * @author Matthew Ludwig
 */
public class Samus extends PlayableCharacter {
    protected SamusChargeShot currentShot;
    private int numberOfAirBombs;
    
    public static final float[] STAT = CharacterStats.SamusStats;
	public static final float[] DMG = CharacterStats.SamusDamage;
	public static final float[] KNBK = CharacterStats.SamusKnockback;
	public static final float[] PRTY = CharacterStats.SamusPriority;
	public static final float[] ISPD = CharacterStats.SamusImageSpeed;
	public static final short[][] SECT = CharacterStats.SamusSectionFrames;

    public Samus() {
        super("Samus", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        this.numberOfAirBombs = 0;
        this.scale = 1.1f;
        setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
        grabHitbox = 20;
        this.grabIsTether = true;
        this.grabOffset = 6;
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.PLASMA;
    }
    
    @Override
    public void setOnGround(boolean isOnGround) {
        super.setOnGround(isOnGround);
        
        if (isOnGround) {
            this.numberOfAirBombs = 0;
        }
    }

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Samus/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_07.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], SECT[1]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Samus/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        action.addModifier(new ActionToggleAttribute(Attribute.METEORS));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], true, SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Samus/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], true, SECT[3]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Samus/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new ChargeShotAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Samus/neutralB1.ogg", 0));
        return action;
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionSpawnEntity(new short[]{0}, 1, SamusMissile.class, this));
        action.addModifier(new ActionPlaySound("/Character Sounds/Samus/sideB.ogg", 0));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], true, SECT[6]));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addModifier(new ActionPlaySound("/Character Sounds/Samus/upB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/electric_01.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionHalfHop());
        action.addModifier(new ActionSpawnEntity(new short[]{0}, 999, SamusBomb.class, this));
        //action.addModifier(new DownBAction(this));
        action.addModifier(new ActionPlaySound("/Character Sounds/Samus/downB.ogg", 0));
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Samus/finalSmash.ogg", 0));
        action.addModifier(new ActionFinalSmashFreeze());
        action.addModifier(new ActionCompleteNumberOfAnimations(100));
        action.addModifier(new FinalSmashAction());
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {}
    
    @Override
    public void onDeath() {
        super.onDeath();
        this.currentShot = null;
    }
    
    public class ChargeShotAction extends ActionModifier {
        protected Constructor entityConstructor;
        protected Object[] entityParameters;
        
        private boolean firstUpdate;
        private boolean shouldFinish;
        private boolean shouldFire;

        public ChargeShotAction() {
            this.entityConstructor = Utilities.getGeneralUtility().getConstructor(SamusChargeShot.class, Samus.this);
            this.entityParameters = new Object[]{Samus.this};
        }

        @Override
        public void onInit() {
            this.firstUpdate = true;
            this.shouldFinish = false;
            this.shouldFire = false;
            
            if (Samus.this.currentShot == null) {
                SamusChargeShot object = (SamusChargeShot) Utilities.getGeneralUtility().createObject(this.entityConstructor, this.entityParameters);
                object.setPosition(this.action.entity.getAppropriateBounds().getLowerMidpoint().x, this.action.entity.getAppropriateBounds().getLowerMidpoint().y);
                
                Samus.this.currentShot = object;
                
                object.addToPosition(0, (this.action.entity.getAppropriateBounds().getDimensions().y - object.getAppropriateBounds().getDimensions().y) / 2);

                Main.getGameState().spawnEntity(object);
            } else {
                Samus.this.currentShot.setDead(false);
                Samus.this.currentShot.setPosition(this.action.entity.getAppropriateBounds().getLowerMidpoint().x, this.action.entity.getAppropriateBounds().getLowerMidpoint().y);
                Samus.this.currentShot.addToPosition(0, (this.action.entity.getAppropriateBounds().getDimensions().y - Samus.this.currentShot.getAppropriateBounds().getDimensions().y) / 2);
                Main.getGameState().spawnEntity(Samus.this.currentShot);
                
                if (Utilities.getGeneralUtility().compareFloats(Samus.this.currentShot.getScale(), 1f, Utilities.FLOATEPSILON)) {
                    this.shouldFinish = true;
                    this.shouldFire = true;
                    Samus.this.currentShot.setFired();
                }
            }
        }

        @Override
        public boolean onUpdate() {
            PlayableCharacter character = (PlayableCharacter) this.action.entity;
            
            if (character.checkControl("Left", false) || character.checkControl("Up", false) || character.checkControl("Right", false)) {
                this.shouldFinish = true;
                this.shouldFire = false;
                return true;
            } else if (!this.firstUpdate && character.checkControl("Special", true)) {
                this.shouldFinish = true;
                this.shouldFire = true;
                return true;
            } else {
                if (this.firstUpdate) {
                    this.firstUpdate = false;
                }
                
                return false;
            }
        }

        @Override
        public boolean shouldFinish() {
            if (this.shouldFinish) {
                if (this.shouldFire) {
                    Samus.this.currentShot.setFired();
                    Samus.this.currentShot = null;
                } else {
                    Samus.this.currentShot.setDead(true);
                }
                
                return true;
            } else {
                return false;
            }
        }

        @Override
        public void cleanUp() {
            this.shouldFire = false;
        }
    }
    
    public class DownBAction extends ActionMovement {
        private Constructor entityConstructor;
        private Object[] entityParameters;
        
        public DownBAction(Object... parameters) {
            super(17.5f, 90f, 0f, false);
            this.entityConstructor = Utilities.getGeneralUtility().getConstructor(SamusBomb.class, parameters);
            this.entityParameters = parameters;
        }

        @Override
        public void onInit() {
            if (this.action.entity.isOnGround() || ((Samus) this.action.entity).numberOfAirBombs < 3) {
                super.onInit();
                
                SamusBomb bomb = (SamusBomb) Utilities.getGeneralUtility().createObject(this.entityConstructor, this.entityParameters);
                bomb.setPosition(this.action.entity.getAppropriateBounds().getLowerMidpoint().x, this.action.entity.getAppropriateBounds().getLowerMidpoint().y);

                Main.getGameState().spawnEntity(bomb);

                bomb.addToPosition(0, ((this.action.animations[0][0].getRealBounds().getDimensions().y * this.action.entity.getScale()) - bomb.getAppropriateBounds().getDimensions().y) / 2);
            
                if (!this.action.entity.isOnGround()) {
                    ((Samus) this.action.entity).numberOfAirBombs++;
                }
            }
        }
        
        @Override
        public void cleanUp() {
            if (!this.action.entity.isOnGround() && Main.getGameSettings().getDebugMode()) {
                this.action.entity.removePhysicsModifier(PhysicsModifier.NOGRAVITY);
            }
            
            super.cleanUp();
        }
    }

    public class FinalSmashAction extends ActionModifier {    
        private float moveTimer;
        private float damageTimer;
        private SamusZeroLaser[] lasers;
        private Angle angle;
        
        public FinalSmashAction() {
            this.moveTimer = 6;
            this.damageTimer = 0;
            this.angle = new Angle(0);
        }

        @Override
        public void onInit() {
            this.moveTimer = 6;
            this.damageTimer = 0;
            this.angle = new Angle(0);
            
            int x = Math.round(Main.getGameState().getWorld().getStage().getWorldBounds().getDimensions().x / 40);
            lasers = new SamusZeroLaser[x];
            
            for (int count = 0; count < lasers.length; count++) {
                SamusZeroLaser laser = new SamusZeroLaser((Samus) this.action.entity);
                Main.getGameState().spawnEntity(laser);
                
                float normalXOffset = (laser.getAppropriateBounds().getDimensions().x / 2) + (this.action.entity.getAppropriateBounds().getDimensions().x / 2);
                float normalYOffset = 0 - (laser.getAppropriateBounds().getDimensions().y / 2);
                float xOffset = (count * 40 * this.angle.cos());
                float yOffset = (this.angle.sin() * count * 40) + (this.angle.sin() * laser.getAppropriateBounds().getDimensions().x / 2);
                laser.setPosition(
                        this.action.entity.getAppropriateBounds().getExactCenter().x + Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * (normalXOffset + xOffset), 
                        this.action.entity.getAppropriateBounds().getExactCenter().y + normalYOffset + yOffset);
                laser.rotateUpTo(this.angle);
                
                lasers[count] = laser;
            }
        }

        @Override
        public boolean onUpdate() {
            this.moveTimer -= Utilities.lockedTPF;
            
            int count = 0;
            
            for (SamusZeroLaser laser : this.lasers) {
                float normalXOffset = (laser.getAppropriateBounds().getDimensions().x / 2) + (this.action.entity.getAppropriateBounds().getDimensions().x / 2);
                float normalYOffset = 0 - (laser.getAppropriateBounds().getDimensions().y / 2);
                float xOffset = (count * 40 * this.angle.cos());
                float yOffset = (this.angle.sin() * count * 40) + (this.angle.sin() * laser.getAppropriateBounds().getDimensions().x / 2);
                laser.setPosition(
                        this.action.entity.getAppropriateBounds().getExactCenter().x + Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * (normalXOffset + xOffset), 
                        this.action.entity.getAppropriateBounds().getExactCenter().y + normalYOffset + yOffset);
                laser.rotateUpTo(this.angle);
                count++;
            }
                        
            if (this.damageTimer >= .18f) {
                Vector2f lowerLeft = new Vector2f(
                        Math.min(this.lasers[0].getAppropriateBounds().getExactCenter().x, this.lasers[count - 1].getAppropriateBounds().getExactCenter().x),
                        Math.min(this.lasers[0].getAppropriateBounds().getExactCenter().y, this.lasers[count - 1].getAppropriateBounds().getExactCenter().y));
                Vector2f upperRight = new Vector2f(
                        Math.max(this.lasers[0].getAppropriateBounds().getExactCenter().x, this.lasers[count - 1].getAppropriateBounds().getExactCenter().x),
                        Math.max(this.lasers[0].getAppropriateBounds().getExactCenter().y, this.lasers[count - 1].getAppropriateBounds().getExactCenter().y));
                BoundingBox box = new BoundingBox(lowerLeft, upperRight.subtract(lowerLeft));

                for (PhysicsEntity entity : Main.getGameState().getWorld().getPhysicsList()) {
                    if (box.isInBounds(entity.getAppropriateBounds()) && entity instanceof ViolentEntity) {
                        LinearFunction f1 = new LinearFunction(this.lasers[0].getAppropriateBounds().getUpperMidpoint(), this.lasers[count - 1].getAppropriateBounds().getUpperMidpoint());
                        LinearFunction f2 = new LinearFunction(this.lasers[0].getAppropriateBounds().getLowerMidpoint(), this.lasers[count - 1].getAppropriateBounds().getLowerMidpoint());
                        
                        float y1 = f1.getY(entity.getAppropriateBounds().getLeftMidpoint().x - this.lasers[0].getAppropriateBounds().getExactCenter().x);
                        float y2 = f1.getY(entity.getAppropriateBounds().getRightMidpoint().x - this.lasers[0].getAppropriateBounds().getExactCenter().x);
                        float y3 = f2.getY(entity.getAppropriateBounds().getLeftMidpoint().x - this.lasers[0].getAppropriateBounds().getExactCenter().x);
                        float y4 = f2.getY(entity.getAppropriateBounds().getRightMidpoint().x - this.lasers[0].getAppropriateBounds().getExactCenter().x);
                        
                        boolean checkY1 = (y1 > entity.getAppropriateBounds().getLowerMidpoint().y) && (y1 < entity.getAppropriateBounds().getUpperMidpoint().y);
                        boolean checkY2 = (y2 > entity.getAppropriateBounds().getLowerMidpoint().y) && (y2 < entity.getAppropriateBounds().getUpperMidpoint().y);
                        boolean checkY3 = (y3 > entity.getAppropriateBounds().getLowerMidpoint().y) && (y3 < entity.getAppropriateBounds().getUpperMidpoint().y);
                        boolean checkY4 = (y4 > entity.getAppropriateBounds().getLowerMidpoint().y) && (y4 < entity.getAppropriateBounds().getUpperMidpoint().y);
                        
//                        Angle damageAngle = new Angle(angle.getValue() + (this.action.entity.getFacing() ? 0 : Math.copySign(90, angle.getValue())));
                        Angle damageAngle = new Angle(this.action.entity.getFacing() ? 0 : 180);
                        
                        if (checkY1 || checkY2 || checkY3 || checkY4) {
                            ((ViolentEntity) entity).addToDamage(DMG[8], KNBK[8], PRTY[8], damageAngle, false, false, false, false, 0, this.lasers[0]);
                        }
                    }
                }
                
                this.damageTimer -= .18f;
            } else {
                this.damageTimer += Utilities.lockedTPF;
            }
                
            if (((PlayableCharacter) this.action.entity).checkControl("Up", false)) {
                this.angle.add(5 * Utilities.lockedTPF);
            } else if (((PlayableCharacter) this.action.entity).checkControl("Down", false)) {
                this.angle.subtract(5 * Utilities.lockedTPF);
            }
            
            return this.moveTimer <= 0;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
            for (SamusZeroLaser laser : this.lasers) {
                laser.setDead(true);
            }
        }
    }
}
