package entities.characters;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;
import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.TransformingCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionFallWhenFinished;
import entities.characters.actions.ActionFinalSmashFreeze;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionReflect;
import entities.characters.actions.ActionReflect.ReflectType;
import entities.characters.actions.ActionSetFacing;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.ActionWaitTillControl;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.projectiles.KrystalLaser;
import entities.projectiles.KrystalSmashLaser;

/**
 * The character Krystal within SSBS.
 *
 * @author Matthew Ludwig
 */
public class Krystal extends TransformingCharacter {
	public static final float[] STAT = CharacterStats.KrystalStats;
	public static final float[] DMG = CharacterStats.KrystalDamage;
	public static final float[] KNBK = CharacterStats.KrystalKnockback;
	public static final float[] PRTY = CharacterStats.KrystalPriority;
	public static final float[] ISPD = CharacterStats.KrystalImageSpeed;
	public static final short[][] SECT = CharacterStats.KrystalSectionFrames;
	
    public Krystal() {
    	super("Krystal", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
    	setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
        this.scale = .71f;
        this.grabHitbox = 30;
        this.grabOffset = -75;
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.PLASMA;
    }

    @Override
    protected Map<String, CharacterAction> setUpSecondActionMap() {
    	Map<String, CharacterAction> map = new HashMap<String, CharacterAction>();
        
        CharacterAction firstAction = new CharacterAction(this, "finalSmash2", StayingPower.NONE, ISPD[8]);
        map.put("Stand", firstAction);
        
        CharacterAction secondAction = new CharacterAction(this, "finalSmash2", StayingPower.BARELY, ISPD[8]);
        secondAction.addModifier(new ActionSetFacing(true));
        secondAction.addModifier(new ActionMovement(25f, 0f, 0f, true));
        secondAction.addModifier(new ActionWaitTillControl("Right", false, false));
        map.put("Walk Right", secondAction);
        map.put("Sprint Right", secondAction);
        
        CharacterAction thirdAction = new CharacterAction(this, "finalSmash2", StayingPower.BARELY, ISPD[8]);
        thirdAction.addModifier(new ActionSetFacing(false));
        thirdAction.addModifier(new ActionMovement(25f, 0f, 0f, true));
        thirdAction.addModifier(new ActionWaitTillControl("Left", false, false));
        map.put("Walk Left", thirdAction);
        map.put("Sprint Left", thirdAction);
        
        CharacterAction jumpAction = new CharacterAction(this, "finalSmash2", StayingPower.BARELY, ISPD[8]);
        jumpAction.addModifier(new ActionMovement(25f, 90f, 0f, false)).addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        map.put("Jump 1", jumpAction);
        map.put("Jump 2", jumpAction);
        
        CharacterAction fourthAction = new CharacterAction(this, "finalSmash2", StayingPower.BARELY, ISPD[8]);
//        fourthAction.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/finalSmash2.ogg", 0));
        //fourthAction.addModifier(new ActionAttack(15.17f, 2.79f, 99.99f));
        fourthAction.addModifier(new ActionSpawnEntity(new short[]{0}, 99, KrystalSmashLaser.class, this).setXOffset(15, true).setYOffset(10));
        map.put("Neutral A", fourthAction);
        map.put("Neutral B", fourthAction);
        
        CharacterAction fifthAction = new CharacterAction(this, "finalSmash3", StayingPower.EXTREME, ISPD[8]);
//        fifthAction.addModifier(new ActionPlaySound("/Character Sounds/King Dedede/finalSmash3.ogg", 0));
        fifthAction.addModifier(new ActionTransform(0));
        fifthAction.addModifier(new ActionFinalSmashFreeze());
        map.put("Transform", fifthAction);
        
        return map;
    }

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Krystal/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
    	CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], SECT[1]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Krystal/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_12.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Krystal/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_08.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Krystal/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Krystal/neutralB.ogg", 9));
        action.addModifier(new ActionSpawnEntity(new short[]{0}, 99, KrystalLaser.class, this).setXOffset(15, true).setYOffset(8));
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionAttack(DMG[5], KNBK[5], PRTY[5], SECT[5]));
        action.addModifier(new ActionMovement(75, 0, 0, true));
        action.addModifier(new ActionFallWhenFinished());
        action.addModifier(new ActionPlaySound("/Character Sounds/Krystal/sideB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_10.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], true, SECT[6]));
        action.addModifier(new UpBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Krystal/upB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/fire_00.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionAttack(DMG[7], KNBK[7], PRTY[7], true, SECT[7]));
        action.addModifier(new ActionReflect(1.2f, false, ReflectType.ENERGY));
        action.addModifier(new ActionPlaySound("/Character Sounds/Krystal/downB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/electric_01.ogg", -1));
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Krystal/finalSmash.ogg", 0));
        action.addModifier(new ActionFinalSmashFreeze());
        action.addModifier(new ActionFinalSmash());
        action.addModifier(new ActionTransform(6));
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {
        actionMap.put("Up B", this.getUpB().addModifier(new ActionFallWhenFinished()));
    }
    
    public class UpBAction extends ActionModifier {
        private Angle direction;
        
        public UpBAction() {
            
        }
        
        @Override
        protected void onInit() {
            this.direction = new Angle(0);
            this.action.entity.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
            this.action.entity.setVelocity(0, 0, false);
        }

        @Override
        protected boolean onUpdate() {
            PlayableCharacter character = (PlayableCharacter) this.action.entity;
                        
            if (this.action.getCurrentFrameNumber() < 10) {                
                if (character.checkControl("Up", false)) {
                    if (character.checkControl("Left", false)) {
                        this.direction.setValue(90 + Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * 45);
                    } else if (character.checkControl("Right", false)) {
                        this.direction.setValue(90 - Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * 45);
                    } else {
                        this.direction.setValue(90);
                    }
                } else if (character.checkControl("Down", false)) {
                    if (character.checkControl("Left", false)) {
                        this.direction.setValue(-90 - Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * 45);
                    } else if (character.checkControl("Right", false)) {
                        this.direction.setValue(-90 + Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * 45);
                    } else {
                        this.direction.setValue(-90);
                    }
                } else {
                    if (character.checkControl("Left", false)) {
                        this.direction.setValue(90 + Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * 90);
                    } else if (character.checkControl("Right", false)) {
                        this.direction.setValue(90 - Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * 90);
                    }
                }
            } else {
                character.setPureXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * 28f * this.direction.cos());
                character.setPureYVelocity(28f * this.direction.sin());
            }
            
            character.rotateUpTo(new Angle(this.direction.getValue() - 90));
            
            return false;
        }

        @Override
        protected boolean shouldFinish() {
            return true;
        }

        @Override
        protected void cleanUp() {
            this.action.entity.rotateUpTo(new Angle(0));
            this.action.entity.setVelocity(0, 0, false);
            this.action.entity.removePhysicsModifier(PhysicsModifier.NOGRAVITY);
        }
    }
    

    public class ActionFinalSmash extends ActionModifier {
        
    	private float targetY;
    	private boolean isFlying;
    	
        public ActionFinalSmash() {
            
        }
        
        @Override
        protected void onInit() {
        	this.targetY = Main.getGameState().getWorld().getStage().getCameraBounds().getUpperMidpoint().y;
        	this.isFlying = true;
        	
        	this.action.entity.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        	this.action.freezeFrame(true);
        }

        @Override
        protected boolean onUpdate() {
        	if (this.isFlying) {
        		if (this.action.entity.getAppropriateBounds().getLowerMidpoint().y < this.targetY) {
        			this.action.entity.setPureYVelocity(90);
        		} else {
        			this.action.entity.setPureYVelocity(00);
        			this.isFlying = false;
        		}
        	} else {
        		return true;
        	}
            
            return false;
        }

        @Override
        protected boolean shouldFinish() {
            return true;
        }

        @Override
        protected void cleanUp() {
        	this.action.entity.removePhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        	this.action.freezeFrame(false);
        }
    }
}
