package entities.characters;

import com.jme3.math.Vector2f;
import com.jme3.scene.Node;

import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionOverrideFalling;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionReflect;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.general.SonicSpring;
import entities.projectiles.SonicEmerald;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

/**
 * The character Samus within SSBS.
 *
 * @author Matthew Ludwig
 */
public class Sonic extends PlayableCharacter {

    protected int numberOfEmeralds;
    protected Map<String, CharacterAction> firstActionMap;
    protected Map<String, CharacterAction> secondActionMap;
    protected float finalSmashTimer;

    public static final float[] STAT = CharacterStats.SonicStats;
	public static final float[] DMG = CharacterStats.SonicDamage;
	public static final float[] KNBK = CharacterStats.SonicKnockback;
	public static final float[] PRTY = CharacterStats.SonicPriority;
	public static final float[] ISPD = CharacterStats.SonicImageSpeed;
	public static final short[][] SECT = CharacterStats.SonicSectionFrames;
	
    public Sonic() {
        super("Sonic", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        this.numberOfEmeralds = 1;
        this.scale = .9f;
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.WHEEL;
    }
    
    @Override
    public void updateEntity() {
        super.updateEntity();
        
        this.finalSmashTimer -= Utilities.lockedTPF;
        
        if (this.finalSmashTimer <= 0) {
            this.setCurrentAction(this.actionMap.get("PowerDown"));
        }
    }
    
    @Override
    public void onDeath() {
        super.onDeath();
        
        this.actionMap = this.firstActionMap;
        this.finalSmashTimer = 0;
        this.removePhysicsModifier(PhysicsModifier.NOGRAVITY);
    }
    
    @Override
    public void setUpEntity(int id, Node displayNode, Node guiNode, boolean alwaysDrawn) {
        super.setUpEntity(id, displayNode, guiNode, alwaysDrawn);
        this.firstActionMap = this.actionMap;
        this.secondActionMap = new HashMap<String, CharacterAction>();
        
        CharacterAction firstAction = new CharacterAction(this, "finalSmash2", StayingPower.NONE, .99f);
        firstAction.addModifier(new FinalSmashStandAction());
        firstAction.addModifier(new ActionAttack(DMG[8], KNBK[8], PRTY[8]));
        this.secondActionMap.put("Stand", firstAction);
        
        CharacterAction secondAction = new CharacterAction(this, "finalSmash3", StayingPower.EXTREME, .99f);
        secondAction.addModifier(new FinalSmashAction(false));
        this.secondActionMap.put("PowerDown", secondAction);
        
        this.finalSmashTimer = 0;
    }

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Sonic/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1));
        action.addModifier(new ActionOverrideFalling());
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], SECT[1]));
        action.addModifier(new ActionMovement(10f, 0f, 0, true));
        action.addModifier(new ActionPlaySound("/Character Sounds/Sonic/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_07.ogg", -1));
        action.addModifier(new ActionOverrideFalling());
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Sonic/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_00.ogg", -1));
        action.addModifier(new ActionOverrideFalling());
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Sonic/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1));
        action.addModifier(new ActionOverrideFalling());
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new ActionAttack(DMG[4], KNBK[4], PRTY[4], SECT[4]));
        action.addModifier(new NeutralBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Sonic/neutralB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_05.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new EmeraldAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Sonic/sideB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "jump1", StayingPower.EXTREME, .06f);
        action.addModifier(new ActionAttack(1.81f, .81f, 1.00f));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addModifier(new SpringAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Sonic/upB.ogg", 0));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, 2.17f);
        action.addModifier(new ActionAttack(0f, .36f, 999f, true));
        action.addModifier(new ActionReflect(1f, true, ActionReflect.ReflectType.BOTH));
        action.addModifier(new ActionPlaySound("/Character Sounds/Sonic/downB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/psychic_01.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, 1.95f);
        action.addModifier(new ActionPlaySound("/Character Sounds/Sonic/finalSmash.ogg", 0));
        action.addModifier(new FinalSmashAction(true));
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {}
    
    public class NeutralBAction extends ActionModifier {
        private float limiter;
        private float builtUpSpeed;
        private float totalSpinTime;
        private boolean charging;
        private boolean shouldFinish;
        
        public NeutralBAction() {
            
        }

        @Override
        public void onInit() {
            this.limiter = 0;
            this.builtUpSpeed = 0;
            this.totalSpinTime = .75f;
            this.charging = true;
            this.shouldFinish = false;
            this.action.updateLimit = 1;
        }

        @Override
        public boolean onUpdate() {
            if (this.charging) {
                if (this.limiter <= 0) {
                    this.limiter = 1;
                    
                    this.builtUpSpeed += 30;
                } else {
                    this.limiter -= Utilities.lockedTPF;
                }
                
                this.totalSpinTime = Math.min(this.totalSpinTime + (.875f * Utilities.lockedTPF), 2.5f);
                this.action.updateLimit = (float) Math.max(this.action.updateLimit / 1.01, .05);
            
                if (!((PlayableCharacter) this.action.entity).checkControl("Special", false)) {
                    this.charging = false;
                    this.limiter = 0;
                }
                
                return false;
            } else {
                if (this.limiter >= this.totalSpinTime) {
                    this.shouldFinish = true;
                    return true;
                } else {
                    this.limiter += Utilities.lockedTPF;
                    
                    if (((PlayableCharacter) this.action.entity).checkControl("Left", true)) {
                        this.action.entity.setFacing(false);
                    } else if (((PlayableCharacter) this.action.entity).checkControl("Right", true)) {
                        this.action.entity.setFacing(true);
                    }
                    
                    this.action.entity.setPureXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * this.builtUpSpeed);
                    return false;
                }
            }
        }

        @Override
        public boolean shouldFinish() {
            return this.shouldFinish;
        }

        @Override
        public void cleanUp() {
            this.action.entity.setPureXVelocity(0);
        }
    }
    
    public class EmeraldAction extends ActionModifier {
        protected Constructor entityConstructor;
        protected Object[] entityParameters;
        private SonicEmerald lastEmerald;

        public EmeraldAction() {
            this.entityConstructor = Utilities.getGeneralUtility().getConstructor(SonicEmerald.class, Sonic.this);
            this.entityParameters = new Object[]{Sonic.this};
        }

        @Override
        public void onInit() {
            if(Sonic.this.numberOfEmeralds < 8 && (this.lastEmerald == null || this.lastEmerald.isDead())) {
                SonicEmerald object = (SonicEmerald) Utilities.getGeneralUtility().createObject(this.entityConstructor, this.entityParameters);
                object.setPosition(this.action.entity.getAppropriateBounds().getLowerMidpoint().x, this.action.entity.getAppropriateBounds().getLowerMidpoint().y);
                object.setEmeraldNumber((Main.getGameState().getTotalNumberOfInitialLives() - Sonic.this.getInitialNumberOfLives()), Sonic.this.numberOfEmeralds++);
                
                object.addToPosition(0, (this.action.entity.getAppropriateBounds().getDimensions().y / 2) - (object.getAppropriateBounds().getDimensions().y / 2));

                Main.getGameState().spawnEntity(object);
                
                this.lastEmerald = object;
            }
        }

        @Override
        public boolean onUpdate() {
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
    
    public class SpringAction extends ActionModifier {

        public SpringAction() {
            
        }

        @Override
        public void onInit() {
            SonicSpring spring = new SonicSpring();
            spring.setPosition(Sonic.this.getAppropriateBounds().getLowerMidpoint().x, Sonic.this.getAppropriateBounds().getLowerMidpoint().y);
            Main.getGameState().spawnEntity(spring);
        }

        @Override
        public boolean onUpdate() {
            return this.action.entity.getVelocity().y < 0;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
    
    public class FinalSmashAction extends ActionModifier {

        private boolean state;
        
        public FinalSmashAction(boolean state) {
            this.state = state;
        }

        @Override
        public void onInit() {
            if (state) {
                ((Sonic) this.action.entity).finalSmashTimer = 10;
                ((Sonic) this.action.entity).addPhysicsModifier(PhysicsModifier.NOGRAVITY);
                ((Sonic) this.action.entity).actionMap = ((Sonic) this.action.entity).secondActionMap;
            } else {
                ((Sonic) this.action.entity).finalSmashTimer = 0;
                ((Sonic) this.action.entity).removePhysicsModifier(PhysicsModifier.NOGRAVITY);
                ((Sonic) this.action.entity).actionMap = ((Sonic) this.action.entity).firstActionMap;
            }
        }

        @Override
        public boolean onUpdate() {
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
    
    public class FinalSmashStandAction extends ActionModifier {

        public FinalSmashStandAction() {
            
        }

        @Override
        public void onInit() {
            
        }

        @Override
        public boolean onUpdate() {
            Vector2f velocity = new Vector2f();
            
            if (((PlayableCharacter) this.action.entity).checkControl("Up", false)) {
                velocity.setY(75);
            } else if (((PlayableCharacter) this.action.entity).checkControl("Down", false)) {
                velocity.setY(-75);
            }
            
            if (((PlayableCharacter) this.action.entity).checkControl("Left", false)) {
                velocity.setX(-75);
                this.action.entity.setFacing(false);
            } else if (((PlayableCharacter) this.action.entity).checkControl("Right", false)) {
                velocity.setX(75);
                this.action.entity.setFacing(true);
            }
            
            this.action.entity.setVelocity(velocity.x, velocity.y, true);
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
            
        }
    }
}
