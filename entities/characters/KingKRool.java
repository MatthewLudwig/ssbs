package entities.characters;

import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionCompleteNumberOfAnimations;
import entities.characters.actions.ActionHalfHop;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.projectiles.KingKRoolFinalKannonball;
import entities.projectiles.KingKRoolKannonball;
import entities.projectiles.KingKRoolShockwave;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import physics.CollisionInfo;
import physics.CollisionInfo.CollisionDirection;

/**
 * The character King K Rool within SSBS.
 *
 * @author Matthew Ludwig
 */
public class KingKRool extends PlayableCharacter {
    private static Random generator = new Random();
    public static final float[] STAT = CharacterStats.KingKRoolStats;
	public static final float[] DMG = CharacterStats.KingKRoolDamage;
	public static final float[] KNBK = CharacterStats.KingKRoolKnockback;
	public static final float[] PRTY = CharacterStats.KingKRoolPriority;
	public static final float[] ISPD = CharacterStats.KingKRoolImageSpeed;
	public static final short[][] SECT = CharacterStats.KingKRoolSectionFrames;
	
    public KingKRool() {
        super("King K Rool", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        this.scale = 1.2f;
        this.grabHitbox = 20;
        this.grabOffset = 25;
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.SUPLEX;
    }

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/King K Rool/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], SECT[1]));
        action.addModifier(new ActionToggleAttribute(Attribute.SUPERARMOR));
        action.addModifier(new ActionPlaySound("/Character Sounds/King K Rool/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_14.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionHalfHop());
        action.addModifier(new ActionToggleAttribute(Attribute.SUPERARMOR));
        action.addModifier(new ActionPlaySound("/Character Sounds/King K Rool/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionToggleAttribute(Attribute.SUPERARMOR));
        action.addModifier(new ActionPlaySound("/Character Sounds/King K Rool/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_14.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new ActionSpawnEntity(new short[]{0}, 3, KingKRoolKannonball.class, this).setXOffset(60, true));
        action.addModifier(new ActionToggleAttribute(Attribute.SUPERARMOR));
        action.addModifier(new ActionPlaySound("/Character Sounds/King K Rool/neutralB.ogg", 0));
        return action;
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionAttack(DMG[5], KNBK[5], PRTY[5], SECT[5]));
        action.addModifier(new ActionMovement(67.5f, 0f, 0, true));
        action.addModifier(new ActionSideB());
        action.addModifier(new ActionToggleAttribute(Attribute.CANONLYHITPLAYERS));
        action.addModifier(new ActionToggleAttribute(Attribute.SUPERARMOR));
        action.addModifier(new ActionPlaySound("/Character Sounds/King K Rool/sideB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_14.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addModifier(new ActionToggleAttribute(Attribute.SUPERARMOR));
        action.addModifier(new ActionPlaySound("/Character Sounds/King K Rool/upB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_15.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionAttack(DMG[7], KNBK[7], PRTY[7], SECT[7]));
        action.addModifier(new ActionDownB());
        action.addModifier(new ActionToggleAttribute(Attribute.SUPERARMOR));
        action.addModifier(new ActionToggleAttribute(Attribute.METEORS));
        action.addModifier(new ActionPlaySound("/Character Sounds/King K Rool/downB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_14.ogg", -1));
        return action;
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new ActionPlaySound("/Character Sounds/King K Rool/finalSmash.ogg", 0));
        action.addModifier(new ActionCompleteNumberOfAnimations(100));
        action.addModifier(new FinalSmash());
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {}
    
    @Override
    public void onCollideWith(PhysicsEntity object, CollisionInfo info) {
        if (this.currentAction.equals(this.actionMap.get("Down B")) && info.getDirection() == CollisionDirection.BOTTOM) {
            super.onCollideWith(object, info);
        }
                
        super.onCollideWith(object, info);
    }
    
    public class ActionSideB extends ActionModifier {
        private float bounceCooldown;
        
        public ActionSideB() {
            this.bounceCooldown = 0;
        }

        @Override
        public void onInit() {
            this.bounceCooldown = 0;
        }

        @Override
        public boolean onUpdate() {
            if (this.action.entity.hasCollided() && this.bounceCooldown == 0) {
                this.action.entity.setFacing(!this.action.entity.getFacing());
                this.action.entity.setPureXVelocity(-this.action.entity.getVelocity().x / 2);
                this.bounceCooldown = 1;
            } else {
                this.bounceCooldown = Math.max(this.bounceCooldown - Utilities.lockedTPF, 0);
            }

            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
    
    public class ActionDownB extends ActionModifier {
        
        private boolean attemptedSpawn;
        
        public ActionDownB() {
            this.attemptedSpawn = false;
        }

        @Override
        public void onInit() {
            this.attemptedSpawn = false;
        }

        @Override
        public boolean onUpdate() {
            if (this.action.getCurrentFrameNumber() == 14 || this.action.getCurrentFrameNumber() == 15) {
                if (!this.attemptedSpawn && this.action.entity.isOnGround()) {
                    KingKRoolShockwave rightWave = new KingKRoolShockwave((KingKRool) this.action.entity);
                    rightWave.setPosition(this.action.entity.getAppropriateBounds().getLowerRight().x, this.action.entity.getAppropriateBounds().getLowerRight().y);
                    Main.getGameState().spawnEntity(rightWave);
                    rightWave.setFacing(true);

                    KingKRoolShockwave leftWave = new KingKRoolShockwave((KingKRool) this.action.entity);
                    leftWave.setPosition(this.action.entity.getAppropriateBounds().getLowerLeft().x, this.action.entity.getAppropriateBounds().getLowerLeft().y);
                    Main.getGameState().spawnEntity(leftWave);
                    leftWave.setFacing(false);
                    
                    this.attemptedSpawn = true;
                }
            }
            
            if (!this.action.entity.isOnGround() && this.action.entity.getVelocity().y < 0) {
                this.action.entity.addToVelocity(0, .5f, false);
            }
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }

    public class FinalSmash extends ActionModifier {        
        private List<KingKRoolFinalKannonball> spawnedBalls;
        private float updateCounter;
        
        private float moveTimer;
        
        public FinalSmash() {
            this.spawnedBalls = new LinkedList<KingKRoolFinalKannonball>();
            this.updateCounter = 0;
            this.moveTimer = 4;
        }

        @Override
        public void onInit() {
            this.updateCounter = 0;
            this.moveTimer = 4;
        }

        @Override
        public boolean onUpdate() {
            if (this.updateCounter >= (1f / 7f)) {
                this.spawnBall();
                this.updateCounter -= (1f / 7f);
            } else {
                this.updateCounter += Utilities.lockedTPF;
            }
            
            this.moveTimer -= Utilities.lockedTPF;
            
            return this.moveTimer <= 0;
        }
        
        public void spawnBall() {            
            if (this.spawnedBalls.size() > 100) {
                this.cleanUpBalls();
            }
            
            KingKRoolFinalKannonball ball = new KingKRoolFinalKannonball((KingKRool) this.action.entity);
            Main.getGameState().spawnEntity(ball);
            
            ball.setPosition(
                    Main.getGameState().getWorld().getStage().getCameraBounds().getDimensions().x * generator.nextFloat(),
                    Main.getGameState().getWorld().getStage().getWorldBounds().getUpperMidpoint().y - (ball.getAppropriateBounds().getDimensions().y / 2) - 1);
            this.spawnedBalls.add(ball);
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
            
        }
        
        public void cleanUpBalls() {
            for (PhysicsEntity entity : this.spawnedBalls) {
                entity.setDead(true);
            }
            
            this.spawnedBalls.clear();
        }
    }
}
