package entities.characters;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionFinalSmashFreeze;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionReflect;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionTeleport;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.CharacterActionFactory;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.projectiles.MewtwoDisable;
import entities.projectiles.MewtwoMegaBall;
import entities.projectiles.MewtwoShadowBall;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;

/**
 * The character Mewtwo within SSBS.
 *
 * @author Matthew Ludwig
 */
public class Mewtwo extends PlayableCharacter {
	protected MewtwoShadowBall currentShot;
	
	public static final float[] STAT = CharacterStats.MewtwoStats;
	public static final float[] DMG = CharacterStats.MewtwoDamage;
	public static final float[] KNBK = CharacterStats.MewtwoKnockback;
	public static final float[] PRTY = CharacterStats.MewtwoPriority;
	public static final float[] ISPD = CharacterStats.MewtwoImageSpeed;
	public static final short[][] SECT = CharacterStats.MewtwoSectionFrames;
	
    public Mewtwo() {
        super("Mewtwo", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        this.scale = .82f;
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.PLASMA;
    }
    
    /*public void throwFinalBall(){
    	CharacterAction action = new CharacterAction(this, "finalSmash2", StayingPower.EXTREME, ISPD[8]);
    	action.addModifier(new ActionFinalSmashFreeze());
        action.addModifier(new FinalSmashAction2());
        action.addModifier(new ActionSpawnEntity(new short[]{0}, 1, MewtwoMegaBall.class, this));
        forceCurrentAction(action);
    }*/
    
    /*public void finalFlash(){
    	CharacterAction action = new CharacterAction(this, "finalSmash3", StayingPower.EXTREME, ISPD[8]);
    	action.addModifier(new ActionFinalSmashFreeze());
        action.addModifier(new FinalSmashAction3());
        forceCurrentAction(action);
    }*/

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mewtwo/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_01.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], SECT[1]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mewtwo/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_07.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mewtwo/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_03.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mewtwo/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_04.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB1", StayingPower.EXTREME, ISPD[4]);
        action.addAnimation(1, "neutralB2");
        action.addModifier(new ChargeShotAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Mewtwo/neutralB1.ogg", 0));
        return action;
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionAttack(DMG[5], KNBK[5], PRTY[5], true, SECT[5]));
        action.addModifier(new ActionReflect(1f, true, ActionReflect.ReflectType.BOTH));
        //action.addModifier(new SideBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Mewtwo/sideB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/psychic_00.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB1", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addAnimation(1, "upB2");
        action.addModifier(new ActionTeleport(1f));
        //action.addModifier(new UpBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Mewtwo/upB.ogg", 0));
        //action.addModifier(new ActionPlaySound("/Common Sounds/psychic_00", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Mewtwo/downB.ogg", 2));
        action.addModifier(new ActionPlaySound("/Common Sounds/psychic_01.ogg", -1));
        action.addModifier(new ActionSpawnEntity(new short[]{2}, 1, MewtwoDisable.class, this).setYOffset(8));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Mewtwo/finalSmash.ogg", 0));
        action.addAnimation(1, "finalSmash2");
        action.addAnimation(2, "finalSmash3");
        action.addModifier(new ActionFinalSmashFreeze());
        action.addModifier(new FinalSmashAction());
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {}
    
    public class ChargeShotAction extends ActionModifier {
        protected Constructor entityConstructor;
        protected Object[] entityParameters;
        
        private boolean firstUpdate;
        private boolean shouldFinish;
        private boolean shouldFire;

        public ChargeShotAction() {
            this.entityConstructor = Utilities.getGeneralUtility().getConstructor(MewtwoShadowBall.class, Mewtwo.this);
            this.entityParameters = new Object[]{Mewtwo.this};
        }

        @Override
        public void onInit() {
            this.firstUpdate = true;
            this.shouldFinish = false;
            this.shouldFire = false;
            
            if (Mewtwo.this.currentShot == null) {
            	MewtwoShadowBall object = (MewtwoShadowBall) Utilities.getGeneralUtility().createObject(this.entityConstructor, this.entityParameters);
                object.setPosition(this.action.entity.getAppropriateBounds().getLowerMidpoint().x, this.action.entity.getAppropriateBounds().getLowerMidpoint().y);
                
                Mewtwo.this.currentShot = object;
                
                object.addToPosition(0, (this.action.entity.getAppropriateBounds().getDimensions().y - object.getAppropriateBounds().getDimensions().y) / 2);

                Main.getGameState().spawnEntity(object);
            } else {
                Mewtwo.this.currentShot.setDead(false);
                Mewtwo.this.currentShot.setPosition(this.action.entity.getAppropriateBounds().getLowerMidpoint().x, this.action.entity.getAppropriateBounds().getLowerMidpoint().y);
                Mewtwo.this.currentShot.addToPosition(0, (this.action.entity.getAppropriateBounds().getDimensions().y - Mewtwo.this.currentShot.getAppropriateBounds().getDimensions().y) / 2);
                Main.getGameState().spawnEntity(Mewtwo.this.currentShot);
                
                if (Utilities.getGeneralUtility().compareFloats(Mewtwo.this.currentShot.getScale(), 1f, Utilities.FLOATEPSILON)) {
                    this.shouldFinish = true;
                    this.shouldFire = true;
                    Mewtwo.this.currentShot.setFired();
                }
            }
        }

        @Override
        public boolean onUpdate() {
            PlayableCharacter character = (PlayableCharacter) this.action.entity;
            
            if (character.checkControl("Left", false) || character.checkControl("Up", false) || character.checkControl("Right", false)) {
                this.shouldFinish = true;
                this.shouldFire = false;
                return true;
            } else if (!this.firstUpdate && character.checkControl("Special", true)) {
                this.shouldFinish = true;
                this.shouldFire = true;
                return true;
            } else {
                if (this.firstUpdate) {
                    this.firstUpdate = false;
                }
                
                return false;
            }
        }

        @Override
        public boolean shouldFinish() {
            if (this.shouldFinish) {
                if (this.shouldFire) {
                	this.action.setCurrentAnimation(1);
                    Mewtwo.this.currentShot.setFired();
                    if (!this.action.entity.isOnGround()){
                    	this.action.entity.setPureXVelocity(this.action.entity.getVelocity().x + -30 * Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * Mewtwo.this.currentShot.getScale());
                    }
                    Mewtwo.this.currentShot = null;
                } else {
                    Mewtwo.this.currentShot.setDead(true);
                }
                
                return true;
            } else {
                return false;
            }
        }

        @Override
        public void cleanUp() {
            this.shouldFire = false;
            //((PlayableCharacter)this.action.entity).setFinalSmash(true);
        }
    }
    
    public class FinalSmashAction extends ActionModifier {
    	int anim;
    	boolean init;
    	//int loops;
    	MewtwoMegaBall ball;
        public FinalSmashAction() {
            
        }
        
        @Override
        protected void onInit() {
        	anim = 0;
        	init = false;
        	//loops = Main.getGameState().getWorld().getStage().getSizeX();
        }

        @Override
        protected boolean onUpdate() {
        	if (anim == 2 && !init){
        		List<PlayableCharacter> chars = Main.getGameState().getWorld().getPlayerList();
                for (PlayableCharacter pc : chars){
                	if (pc.getCurrentAction().equals(CharacterActionFactory.getShieldBreak(pc, (short) 1))){
                		pc.addToDamage(DMG[8]*STAT[6], KNBK[8]*STAT[5], 1, new Angle(0), false, false, false, false, 0, (ViolentEntity) this.action.entity);
                	}
                }
                init = true;
        	}
            return false;
        }

        @Override
        protected boolean shouldFinish() {
            if (anim == 0){
            	this.action.setCurrentAnimation(1);
            	anim = 1;
            	ball = new MewtwoMegaBall((Mewtwo) this.action.entity);
            	ball.setPosition(this.action.entity.getPosition().x, this.action.entity.getPosition().y);
            	Main.getGameState().spawnEntity(ball);
            	return false;
            } else if (anim == 1){
            	if (ball == null || ball.isDead()){
            		this.action.setCurrentAnimation(2);
            		anim = 2;
            	}
            	return false;
            }
            return true;
        }

        @Override
        protected void cleanUp() {

        }
    }
    
    public class FinalSmashAction2 extends ActionModifier {
    	int loops;
        public FinalSmashAction2() {
            
        }
        
        @Override
        protected void onInit() {
        	loops = Main.getGameState().getWorld().getStage().getSizeX();
        }

        @Override
        protected boolean onUpdate() {
            return false;
        }

        @Override
        protected boolean shouldFinish() {
        	if (loops <= 0){
        		//finalFlash();
        		return true;
        	}
        	loops --;
        	return false;
        }

        @Override
        protected void cleanUp() {
        	
        }
    }
    
    public class FinalSmashAction3 extends ActionModifier {
        public FinalSmashAction3() {
        
        }

        @Override
        protected void onInit() {
            List<PlayableCharacter> chars = Main.getGameState().getWorld().getPlayerList();
            for (PlayableCharacter pc : chars){
            	if (pc.checkShieldStun()){
            		pc.addToDamage(DMG[8]*STAT[6], KNBK[8]*STAT[5], 1, new Angle(0), false, false, false, false, 0, (ViolentEntity) this.action.entity);
            	}
            }
        }

        @Override
        protected boolean onUpdate() {
            return false;
        }

        @Override
        protected boolean shouldFinish() {
            return true;
        }

        @Override
        protected void cleanUp() {
            
        }
    }
}
