package entities.characters;

import static entities.characters.actions.CharacterActionFactory.*;

import java.util.Map;

import physics.PhysicsModifier;
import engine.utility.Utilities;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionCounter;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPlaySoundForMarth;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionQuickFall;
import entities.characters.actions.ActionShortHop;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

/**
 * The character Marth within SSBS.
 *
 * @author Matthew Ludwig
 */
public class Marth extends PlayableCharacter {
    public static final float[] STAT = CharacterStats.MarthStats;
	public static final float[] DMG = CharacterStats.MarthDamage;
	public static final float[] KNBK = CharacterStats.MarthKnockback;
	public static final float[] PRTY = CharacterStats.MarthPriority;
	public static final float[] ISPD = CharacterStats.MarthImageSpeed;
	public static final short[][] SECT = CharacterStats.MarthSectionFrames;
	
	private String actionToForce;
	
    public Marth() {
        super("Marth", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        this.scale = .7f;
        this.actionToForce = "";
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.SWORD;
    }
    
    @Override
    public void updateEntity() {
        super.updateEntity();
        
        if (!this.actionToForce.isEmpty()) {
            this.forceCurrentAction(this.actionMap.get(this.actionToForce));
            this.actionToForce = "";
        }
    }

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/neutralA_" + this.getColorNumber() + ".ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_02.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], SECT[1]));
        action.addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/sideA_" + this.getColorNumber() + ".ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_07.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/upA_" + this.getColorNumber() + ".ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_03.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/downA_" + this.getColorNumber() + ".ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB1", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new NeutralBAction());
        action.addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/neutralB1_" + this.getColorNumber() + ".ogg", 9));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_15.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionAttack(DMG[5], KNBK[5], PRTY[5], SECT[5]));
        action.addAnimation(1, "sideB2");
        action.addAnimation(2, "sideB3");
        action.addModifier(new SideBAction());
        action.addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/sideB_" + this.getColorNumber() + ".ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_01.ogg", -1));
        //action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addModifier(new UpBAction());
        action.addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/upB_" + this.getColorNumber() + ".ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB1", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionCounter());
        action.addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/downB1_" + this.getColorNumber() + ".ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        
        action.addAnimation(1, "downB2");
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
//        action.addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/finalSmash.ogg", 0));
        action.addModifier(new FinalSmashAction());
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {
        CharacterAction jump1 = getAnimation(this, "jump1", StayingPower.MEDIUM, 1);
        jump1.addModifier(new ActionMovement(25f, 90f, 0f, false)).addModifier(new ActionToggleAttribute(Attribute.VECTORING)).addModifier(new ActionWaitTillOnGround());
        jump1.addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/jump1_" + this.getColorNumber() + ".ogg", 0));
        jump1.addModifier(new ActionShortHop());
        jump1.addModifier(new ActionQuickFall());
        
        CharacterAction jump2 = getAnimation(this, "jump2", StayingPower.HIGH, 1);
        jump2.addModifier(new ActionMovement(25f, 90f, 0f, false)).addModifier(new ActionToggleAttribute(Attribute.VECTORING)).addModifier(new ActionWaitTillOnGround());
        jump2.addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/jump2_" + this.getColorNumber() + ".ogg", 0));
        jump2.addModifier(new ActionQuickFall());
        
        actionMap.put("Jump 1", jump1);
        actionMap.put("Jump 2", jump2);
        
        actionMap.get("Walk Right").addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/walk_" + this.getColorNumber() + ".ogg", 0));
        actionMap.get("Walk Left").addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/walk_" + this.getColorNumber() + ".ogg", 0));
        actionMap.get("Sprint Right").addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/walk_" + this.getColorNumber() + ".ogg", 0));
        actionMap.get("Sprint Left").addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/walk_" + this.getColorNumber() + ".ogg", 0));

        CharacterAction neutralB2 = new CharacterAction(this, "neutralB2", StayingPower.EXTREME, .33f);
        neutralB2.addModifier(new ActionPlaySoundForMarth("/Character Sounds/Marth/neutralB2_" + this.getColorNumber() + ".ogg", 0));
        neutralB2.addModifier(new ClearAttackAction());

        actionMap.put("Neutral B 2", neutralB2);

    }
    
    public void forceAction(String actionName) {
        this.actionToForce = actionName;
    }
    
    public class ClearAttackAction extends ActionModifier {        
        public ClearAttackAction() {
        }
        
        @Override
        protected void onInit() {
        }

        @Override
        protected boolean onUpdate() {
            return false;
        }

        @Override
        protected boolean shouldFinish() {
            return true;
        }

        @Override
        protected void cleanUp() {
            ((ViolentEntity) this.action.entity).setAttackCapability(0, 0, 0, false);
        }
    }
    
    public class NeutralBAction extends ActionModifier {
        private float chargeTime;
        private boolean shouldFinish;
        
        public NeutralBAction() {
            this.chargeTime = 0;
        }
        
        @Override
        protected void onInit() {
            this.chargeTime = 0;
        }

        @Override
        protected boolean onUpdate() {
            if (this.chargeTime > 4 || !((PlayableCharacter) this.action.entity).checkControl("Special", false)) {
                ((ViolentEntity) this.action.entity).setAttackCapability(
                        (1 + Math.min(this.chargeTime, 3)) * DMG[4], 
                        (1 + Math.min(this.chargeTime, 3)) * KNBK[4], 
                        PRTY[4],
                        false, SECT[4]);
                this.shouldFinish = true;
            } else {
                this.chargeTime += Utilities.lockedTPF;
            }
            
            return false;
        }

        @Override
        protected boolean shouldFinish() {
            return this.shouldFinish;
        }

        @Override
        protected void cleanUp() {
            if (this.shouldFinish) {
                this.shouldFinish = false;
                ((Marth) this.action.entity).forceAction("Neutral B 2");
            }
        }
    }
    
    public class SideBAction extends ActionModifier {
    	
    	private int stageNum;
    	private boolean b;
    	private float mult1, mult2;
    	
        public SideBAction() {
            
        }
        
        @Override
        protected void onInit() {
        	this.stageNum = 0;
        	b = true;
        	mult1 = 1f;
        	mult2 = 1f;
        }

        /*
         * Stage Two
         * 	Up doubles priority but halves image speed.
         * 	Down doubles image speed but halves priority.
         * 	Forward has no effect.
         * Stage three
         * 	Up doubles damage but halves knockback.
         * 	Down doubles knockback but halves damage.
         * 	Forward has no effect.
         * Stage four
         * 	Up makes the attack hit opponents straight up.
         * 	Down turns it into a meteor smash.
         * 	Forward makes them go completely horizontal.
         */
        @Override
        protected boolean onUpdate() {
        	PlayableCharacter marth = ((PlayableCharacter) this.action.entity);
        	int currentFrame = this.action.getCurrentFrameNumber();
        	boolean endOfStage = getEndOfStage(this.action.getCurrentAnimationNumber(), currentFrame);
        	if (endOfStage && b){
        		b = false;
        		if (!marth.checkControl("Special", false)){
        			return true;
        		}
        		stageNum++;
        	        
        		boolean up = marth.checkControl("Up", false);
        		boolean down = marth.checkControl("Down", false);
        		if (down && up){
        			down = false;
        			up = false;
        		}
        		
        		this.action.entity.setPosition(marth.getPosition().x+30*Utilities.getGeneralUtility().getBooleanAsSign(marth.getFacing()), marth.getPosition().y);
        		
        		if (up){
        			this.action.setCurrentAnimation(1);
        			this.action.setCurrentFrame(getFrame(1,stageNum));
        			if (stageNum == 1){
        				mult1 = 1.33f;
        				this.action.updateLimit = this.action.scaleUpdateLimit(ISPD[5]*1f/mult1);
        			} else if (stageNum == 2){
        				mult2 = 1.33f;
        			}
        		} else if (down){
        			this.action.setCurrentAnimation(2);
        			this.action.setCurrentFrame(getFrame(2,stageNum));
        			if (stageNum == 1){
        				mult1 = 0.75f;
        				this.action.updateLimit = this.action.scaleUpdateLimit(ISPD[5]*1f/mult1);
        			} else if (stageNum == 2){
        				mult2 = 0.75f;
        			} else if (stageNum == 3){
        				((ViolentEntity) this.action.entity).setMeteor(true);
        			}
        		} else {
        			this.action.setCurrentAnimation(0);
        			this.action.setCurrentFrame(getFrame(0,stageNum));
        		}
        		((ViolentEntity) this.action.entity).setAttackCapability(DMG[5]*mult2, KNBK[5]/mult2, PRTY[5]*mult1, false);
        	}
        	if (!endOfStage){
        		b = true;
        	}
        	
        	return false;
        }
        
        protected boolean getEndOfStage(int animation, int frame){
        	if (animation == 0)
        		if (frame == 4 || frame == 7 || frame == 11)
        			return true;
        	if (animation > 0)
        		if (frame == 0 || frame == 3 || frame == 7)
        			return true;
        	return false;
        }
        
        protected int getFrame(int animation, int stage){
        	int n = 0;
        	if (animation > 0)
        		n -= 4;
        	if (stage == 1)
        		return 4+n;
        	if (stage == 2)
        		return 7+n;
        	return 11+n;
        }

        @Override
        protected boolean shouldFinish() {
        	return true;
        }

        @Override
        protected void cleanUp() {
        	((ViolentEntity) this.action.entity).setMeteor(false);
        }
    }
    
    public class UpBAction extends ActionModifier {
    	private boolean hasChecked;
    	
        public UpBAction() {
            
        }
        
        @Override
        protected void onInit() {
        	this.hasChecked = false;
        }

        @Override
        protected boolean onUpdate() {
        	if (!this.hasChecked) {
            	this.action.entity.setPureXVelocity(
            			Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * 
            			this.action.entity.getVelocity().y / 2);
            	this.hasChecked = true;
        	}
        	
        	return false;
        }

        @Override
        protected boolean shouldFinish() {
        	return true;
        }

        @Override
        protected void cleanUp() {

        }
    }
    
    public class DownBAction extends ActionModifier {
        public DownBAction() {
        
        }

        @Override
        protected void onInit() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        protected boolean onUpdate() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        protected boolean shouldFinish() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        @Override
        protected void cleanUp() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
    public class FinalSmashAction extends ActionModifier {
    	private boolean superSpeed;
    	private boolean startedOnGround;
    	
        public FinalSmashAction() {
        	this.superSpeed = false;
        	this.startedOnGround = false;
        }

        @Override
        protected void onInit() {
        	this.superSpeed = false;
        	this.startedOnGround = this.action.entity.isOnGround();
			this.action.entity.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        }

        @Override
        protected boolean onUpdate() {
        	if (this.action.getCurrentFrameNumber() == 6) {
        		this.superSpeed = true;
        		this.action.freezeFrame(true);
        	} else if (this.action.getCurrentFrameNumber() == 13) {
    			((ViolentEntity) this.action.entity).setAttackCapability(DMG[8], KNBK[8], PRTY[8], false, SECT[8]);
    			this.action.entity.removePhysicsModifier(PhysicsModifier.ISSTICKY);
        	}
        	
        	if (this.superSpeed) {
        		this.action.entity.setXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * 52.5f);
        		
        		if (this.action.entity.hasCollidedWithObstacle(true)) {
        			this.action.freezeFrame(false);
        			this.action.setCurrentFrame(15);
        			this.superSpeed = false;
        		} else if (this.startedOnGround && !this.action.entity.isOnGround()) {
        			this.action.freezeFrame(false);
        			this.action.setCurrentFrame(15);
        			this.superSpeed = false;
        		} else if (this.action.entity.hasCollided()) {
        			this.action.entity.addPhysicsModifier(PhysicsModifier.ISSTICKY);
        			this.action.freezeFrame(false);
        			this.action.setCurrentFrame(7);
        			this.superSpeed = false;
        		}
        	} else {
    			this.action.entity.setVelocity(0, 0, false);
    			this.action.entity.grabOffset.set(0, -10);
        	}
        	
        	return false;
        }

        @Override
        protected boolean shouldFinish() {
        	return true;
        }

        @Override
        protected void cleanUp() {
			((ViolentEntity) this.action.entity).setAttackCapability(0, 0, 0, false);
			this.action.entity.removePhysicsModifier(PhysicsModifier.NOGRAVITY);
			this.action.entity.removePhysicsModifier(PhysicsModifier.ISSTICKY);
			this.action.freezeFrame(false);
        }
    }
}
