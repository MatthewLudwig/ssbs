package entities.characters;

import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.Kirby.KirbyAbility;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionFinalSmashFreeze;
import entities.characters.actions.ActionHalfHop;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionStallThenFall;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.projectiles.LinkArrow;
import entities.projectiles.LinkBiggoronArrow;
import entities.projectiles.LinkBomb;
import entities.projectiles.LinkBoomerang;
import entities.projectiles.Projectile;

import java.lang.reflect.Constructor;
import java.util.Map;

import physics.CollisionInfo;

/**
 * The character Link within SSBS.
 *
 * @author Matthew Ludwig
 */
public class Link extends PlayableCharacter {
	public static final float[] STAT = CharacterStats.LinkStats;
	public static final float[] DMG = CharacterStats.LinkDamage;
	public static final float[] KNBK = CharacterStats.LinkKnockback;
	public static final float[] PRTY = CharacterStats.LinkPriority;
	public static final float[] ISPD = CharacterStats.LinkImageSpeed;
	public static final short[][] SECT = CharacterStats.LinkSectionFrames;
	
    public Link() {
        super("Link", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
        this.scale = .9f;
        this.grabIsTether = true;
        this.grabHitbox = 15;
        this.grabOffset = -25;
    }
    
    @Override
    public KirbyAbility getKirbyAbility() {
        return KirbyAbility.SWORD;
    }
    
    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
        if (entity instanceof Projectile && info.getDirection().matchesFacing(!this.facing) && this.currentAction.equals(this.actionMap.get("Stand"))) {
            ((Projectile) entity).reflect(this);
        } else {
            super.onCollideWith(entity, info);
        }
    }

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Link/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_13.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], true, SECT[1]));
        action.addModifier(new ActionSideA());
        action.addModifier(new ActionPlaySound("/Character Sounds/Link/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_00.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        action.addModifier(new ActionPressMultiHit("Attack"));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Link/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_12.ogg", -1));
        action.addModifier(new ActionHalfHop());
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Link/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_01.ogg", -1));
        action.addModifier(new ActionHalfHop());
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        action.addModifier(new ActionToggleAttribute(Attribute.METEORS));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addModifier(new ActionWaitTillOnGround());
        action.addModifier(new ActionStallThenFall(0.2f));
        action.addModifier(new ActionDownA());
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new ActionSpawnEntity(new short[]{6}, 99, LinkArrow.class, this));
        action.addModifier(new ActionPlaySound("/Character Sounds/Link/neutralB.ogg", 0));
        //action.addModifier(new ActionArrow(this));
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionSpawnEntity(new short[]{0}, 2, LinkBoomerang.class, this));
        action.addModifier(new ActionPlaySound("/Character Sounds/Link/sideB.ogg", 0));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addModifier(new ActionPlaySound("/Character Sounds/Link/upB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_15.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionSpawnEntity(new short[]{0}, 99, LinkBomb.class, this).setXOffset(15, true).setYOffset(15));
        action.addModifier(new ActionPlaySound("/Character Sounds/Link/downB.ogg", 0));
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Link/finalSmash.ogg", 0));
        action.addModifier(new ActionFinalSmashFreeze());
        action.addModifier(new ActionSpawnEntity(new short[]{17}, 1, LinkBiggoronArrow.class, this));
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {}
    
    public class ActionSideA extends ActionModifier {
    	
        public ActionSideA() {
        }

        @Override
        public void onInit() {
        }

        @Override
        public boolean onUpdate() {
            if (this.action.getCurrentFrameNumber() == 2 || this.action.getCurrentFrameNumber() == 6){
            	this.action.entity.setPosition(this.action.entity.getPosition().x + this.action.getCurrentFrameNumber()*1.5f*Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()), this.action.entity.getPosition().y);
            }
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
    
    public class ActionDownA extends ActionModifier {
        private float bounceCooldown;
        
        public ActionDownA() {
            this.bounceCooldown = 0;
        }

        @Override
        public void onInit() {
            this.bounceCooldown = 0;
        }

        @Override
        public boolean onUpdate() {
            if (this.action.entity.hasCollided() && this.bounceCooldown == 0) {
                this.action.entity.setPureYVelocity(-this.action.entity.getVelocity().y);
                this.bounceCooldown = 1;
            } else {
                this.bounceCooldown = Math.max(this.bounceCooldown - Utilities.lockedTPF, 0);
            }

            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
}
