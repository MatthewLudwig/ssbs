package entities.characters;

import java.util.Map;

import com.jme3.audio.AudioNode;
import com.jme3.math.Vector2f;

import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionFinalSmashFreeze;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionReflect.ReflectType;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.projectiles.ProfessorLaytonCoin;
import entities.projectiles.ProfessorLaytonFinalHat;
import entities.projectiles.ProfessorLaytonLaser;
import entities.projectiles.ProfessorLaytonUmbrella;
import entities.projectiles.Projectile;

/**
 * The character Professor Layton within SSBS.
 *
 * @author Matthew Ludwig
 */
public class ProfessorLayton extends PlayableCharacter {
	public static final float[] STAT = CharacterStats.ProfessorLaytonStats;
	public static final float[] DMG = CharacterStats.ProfessorLaytonDamage;
	public static final float[] KNBK = CharacterStats.ProfessorLaytonKnockback;
	public static final float[] PRTY = CharacterStats.ProfessorLaytonPriority;
	public static final float[] ISPD = CharacterStats.ProfessorLaytonImageSpeed;
	public static final short[][] SECT = CharacterStats.ProfessorLaytonSectionFrames;
	
	public float laserDirection;
	
    public ProfessorLayton() {
        super("Professor Layton", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
        grabHitbox = 20;
        grabOffset = 64;
        scale = 0.44f;
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.FIRE;
    }
    
    public void updateEntity(){
    	super.updateEntity();
    	//System.out.println("ProfessorLayton: " + this.display.getLocalTranslation().x + ", " + this.display.getLocalTranslation().y);
    }

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Professor Layton/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_09.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        //action.addModifier(new NeutralA());
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], true, SECT[1]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Professor Layton/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_07.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Professor Layton/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/ping.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Professor Layton/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_12.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        //action.addModifier(new ActionSpawnEntity(new short[]{1}, 99, ProfessorLaytonCoin.class, this).setXOffset(20, true));
        action.addModifier(new NeutralB());
        //action.addModifier(new ActionPlaySound("/Character Sounds/ProfessorLayton/neutralB.ogg", 9));
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB1", StayingPower.EXTREME, ISPD[5]);
        action.addAnimation(1, "sideB2");
        action.addModifier(new SideBAction());
        //action.addModifier(new ActionPlaySound("/Character Sounds/Professor Layton/sideB.ogg", 0));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addModifier(new UpBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Professor Layton/upB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB1", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionSpawnEntity(new short[]{11}, 99, ProfessorLaytonLaser.class, this).setXOffset(5, true).setYOffset(15));
        action.addAnimation(2, "downB2");
        action.addAnimation(1, "downB3");
        action.addModifier(new DownBAction());
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_01.ogg", -1));
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Professor Layton/finalSmash.ogg", 0));
        action.addModifier(new ActionFinalSmashFreeze());
        action.addModifier(new ActionSpawnEntity(new short[]{0}, 
                99, ProfessorLaytonFinalHat.class, this).setXOffset(10, true).setYOffset(10));
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {}
    
    public class SideBAction extends ActionModifier {
    	private int frameReleased;
    	private AudioNode audio;

        public SideBAction() {
        	audio = Utilities.getCustomLoader().getAudioNode("/Common Sounds/camera.ogg");
        }

        @Override
        public void onInit() {
            frameReleased = 0;
        }

        @Override
        public boolean onUpdate() {
        	if (this.action.getCurrentAnimationNumber() == 0){
        		if (!((PlayableCharacter)this.action.entity).checkControl("Special", false)){
        			this.action.setCurrentAnimation(1);
        			frameReleased = this.action.getCurrentFrameNumber();
        			this.audio.playInstance();
        		} else if (this.action.getCurrentFrameNumber() == 9){
        			this.action.setCurrentAnimation(1);
        			frameReleased = 9;
        			this.audio.playInstance();
        		}
        	} else {
        		Vector2f pos = new Vector2f(this.action.entity.getPosition().x+5*Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()), this.action.entity.getRealBounds().getExactCenter().y);
        		//TestEntity test = new TestEntity();
        		//test.setPosition(pos.x, pos.y);
        		//Main.getGameState().spawnEntity(test);
        		float dist = frameReleased*10f/9f + 25f;
        		for (PhysicsEntity phy : Main.getGameState().getWorld().getPhysicsList()){
        			if (phy instanceof Projectile && phy.getPosition().distance(pos) <= dist){
        				((ViolentEntity) this.action.entity).setCanReflect(frameReleased*0.244f+0.8f, false, ReflectType.BOTH);
        				((Projectile)phy).reflect(this.action.entity);
        				((ViolentEntity) this.action.entity).setCantReflect();
        			} else if (phy instanceof PlayableCharacter && phy != this.action.entity && phy.getPosition().distance(pos) <= dist){
        				((PlayableCharacter)phy).addToDamage(DMG[5]*STAT[6]/(10-frameReleased), KNBK[5]*STAT[5]/(10-frameReleased), 1, Utilities.getPhysicsUtility().calculateCorrectAngle(phy.getPosition(), pos), true, false, false, false, 0, (ViolentEntity) this.action.entity);
        			}
        		}
        	}
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
        	
        }
    }
    
    public class NeutralB extends ActionModifier {

    	boolean firedAlready;
    	
        public NeutralB() {
            firedAlready = false;
        }

        @Override
        public void onInit() {
        	
        }

        @Override
        public boolean onUpdate() {
        	if (!firedAlready && this.action.getCurrentFrameNumber() == 1){
        		firedAlready = true;
        		ProfessorLaytonCoin coin = new ProfessorLaytonCoin((ProfessorLayton) this.action.entity);
        		coin.setPosition(this.action.entity.getPosition().x+21*Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()), this.action.entity.getRealBounds().getExactCenter().y);
        		Main.getGameState().spawnEntity(coin);
        	}
            return false;
        }

        @Override
        public boolean shouldFinish() {
        	if (((PlayableCharacter)this.action.entity).checkControl("Special", false)){
        		firedAlready = false;
        		return false;
        	}
        	return true;
        }

        @Override
        public void cleanUp() {
        	
        }
    }
    
    public class UpBAction extends ActionModifier {
        private float timer;

        public UpBAction() {
            this.timer = 0;
        }

        @Override
        public void onInit() {
        }

        @Override
        public boolean onUpdate() {            
            this.timer += Utilities.lockedTPF;
            if (this.timer >= 1 || !((PlayableCharacter)this.action.entity).checkControl("Special", false)){
            	this.timer = 1;
            	ProfessorLaytonUmbrella umbrella = new ProfessorLaytonUmbrella((ProfessorLayton) this.action.entity);
            	umbrella.setPosition(this.action.entity.getPosition().x+18*Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()), this.action.entity.getRealBounds().getExactCenter().y+2);
            	Main.getGameState().spawnEntity(umbrella);
            	return true;
            }
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return this.timer >= 1;
        }

        @Override
        public void cleanUp() {
            this.timer = 0;
        }
    }
    
    public class DownBAction extends ActionModifier {
        public DownBAction() {
        }

        @Override
        public void onInit() {
        	laserDirection = 0;
        }

        @Override
        public boolean onUpdate() {
        	int n = this.action.getCurrentFrameNumber();
        	if (laserDirection == 0 && n > 5 && ((PlayableCharacter) this.action.entity).checkControl("Up", false) && !((PlayableCharacter) this.action.entity).checkControl("Down", false) && n < 10){
        		this.action.setCurrentAnimation(1);
        		laserDirection = 1;
        		this.action.setCurrentFrame(n);
    		} else if (laserDirection == 0 && n > 5 && !((PlayableCharacter) this.action.entity).checkControl("Up", false) && ((PlayableCharacter) this.action.entity).checkControl("Down", false) && n < 10){
    			this.action.setCurrentAnimation(2);
    			laserDirection = 2;
    			this.action.setCurrentFrame(n);
    		}
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
        	//((PlayableCharacter) this.action.entity).setFinalSmash(true);
        }
    }
    
}
