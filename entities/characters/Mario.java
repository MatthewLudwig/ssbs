package entities.characters;

import java.util.Map;

import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionFinalSmashFreeze;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionReflect;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.general.MarioCoin;
import entities.projectiles.MarioFinalFireball;
import entities.projectiles.MarioFireball;

/**
 * The character Mario within SSBS.
 *
 * @author Matthew Ludwig
 */
public class Mario extends PlayableCharacter {
	public static final float[] STAT = CharacterStats.MarioStats;
	public static final float[] DMG = CharacterStats.MarioDamage;
	public static final float[] KNBK = CharacterStats.MarioKnockback;
	public static final float[] PRTY = CharacterStats.MarioPriority;
	public static final float[] ISPD = CharacterStats.MarioImageSpeed;
	public static final short[][] SECT = CharacterStats.MarioSectionFrames;
	
    public Mario() {
        super("Mario", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
        grabHitbox = 20;
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.FIRE;
    }
    
    public void updateEntity(){
    	super.updateEntity();
    	//System.out.println("Mario: " + this.display.getLocalTranslation().x + ", " + this.display.getLocalTranslation().y);
    }

    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mario/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_04.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], true, SECT[1]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mario/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mario/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_05.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mario/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_13.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new ActionSpawnEntity(new short[]{9}, 99, MarioFireball.class, this).setXOffset(30, true));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mario/neutralB.ogg", 9));
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionAttack(DMG[5], KNBK[5], PRTY[5], SECT[5]));
        action.addModifier(new ActionReflect(1f, true, ActionReflect.ReflectType.BOTH));
        action.addModifier(new SideBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Mario/sideB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_04.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
        action.addModifier(new UpBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Mario/upB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/coin.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionAttack(DMG[7], KNBK[7], PRTY[7], SECT[7]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Mario/downB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_01.ogg", -1));
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Mario/finalSmash.ogg", 0));
        action.addModifier(new ActionFinalSmashFreeze());
        action.addModifier(new ActionSpawnEntity(new short[]{
            16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64}, 
                99, MarioFinalFireball.class, this).setXOffset(30, true));
        return action;
    }
    
    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {}
    
    public class SideBAction extends ActionModifier {

        public SideBAction() {
        }

        @Override
        public void onInit() {
            if (!this.action.entity.isOnGround()) {
                this.action.entity.addToPosition(Utilities.getGeneralUtility().getBooleanAsSign(this.action.entity.getFacing()) * this.action.entity.getAppropriateBounds().getDimensions().x / 2, 0);
                ((Mario) this.action.entity).setVectoring(false);
            }
        }

        @Override
        public boolean onUpdate() {
            if (!this.action.entity.isOnGround()) {
                this.action.entity.setPureYVelocity(0);
            }
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
            if (!this.action.entity.isOnGround()) {
                ((Mario) this.action.entity).setVectoring(false);
            }
        }
    }
    
    public class UpBAction extends ActionModifier {
        private float cooldown;

        public UpBAction() {
            this.cooldown = 0;
        }

        @Override
        public void onInit() {
        }

        @Override
        public boolean onUpdate() {            
            if(this.action.entity.hasCollided() && this.cooldown <= 0)
            {
                MarioCoin coin = new MarioCoin();
                coin.setPosition(this.action.entity.getAppropriateBounds().getLowerMidpoint().x, this.action.entity.getAppropriateBounds().getLowerMidpoint().y);
                Main.getGameState().spawnEntity(coin);

                this.cooldown = .15f;
            }
            else
            {
                this.cooldown = Math.max(this.cooldown - Utilities.lockedTPF, 0);
            }
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return ((PlayableCharacter) this.action.entity).isOnGround();
        }

        @Override
        public void cleanUp() {
            this.cooldown = 0;
        }
    }
}
