package entities.characters.controllers;

import engine.Main;
import engine.input.UserInput;
import entities.PlayableCharacter;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Matthew
 */
public class PlayerController extends Controller {
    
    public PlayerController(PlayableCharacter character) {
        super(character);
    }
    
    @Override
    public String getName() {
        return "Player";
    }
    
    @Override
    public String getAdditionalInfo() {
        return "";
    }
    
    @Override
    public Map<String, Boolean> update() {
        Map<String, Boolean> controlMap = new HashMap<String, Boolean>();
        
        UserInput ui = Main.getUserInput();
        
        //System.out.println(this.character.getPlayerNumber());
        //System.out.println(ui.getKeyState("P" + this.character.getPlayerNumber() + " Up"));
        
        controlMap.put("Up", ui.getKeyState("P" + this.character.getPlayerNumber() + " Up"));
        controlMap.put("Right", ui.getKeyState("P" + this.character.getPlayerNumber() + " Right"));
        controlMap.put("Down", ui.getKeyState("P" + this.character.getPlayerNumber() + " Down"));
        controlMap.put("Left", ui.getKeyState("P" + this.character.getPlayerNumber() + " Left"));
        controlMap.put("Attack", ui.getKeyState("P" + this.character.getPlayerNumber() + " Attack"));
        controlMap.put("Special", ui.getKeyState("P" + this.character.getPlayerNumber() + " Special"));
        controlMap.put("Shield", ui.getKeyState("P" + this.character.getPlayerNumber() + " Shield"));
        
        return controlMap;
    }
    
}
