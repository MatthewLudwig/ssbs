package entities.characters.controllers;

import entities.PlayableCharacter;

import java.util.Map;

/**
 *
 * @author Matthew
 */
public abstract class Controller {
    protected PlayableCharacter character;
    
    public Controller(PlayableCharacter character) {
        this.character = character;
    }
    
    public abstract String getName();
    public abstract String getAdditionalInfo();
    public abstract Map<String, Boolean> update();
}
