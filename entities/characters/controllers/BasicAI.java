package entities.characters.controllers;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.CharacterActionFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.jme3.math.Vector2f;

/**
 * 
 * @author Matthew
 */
public class BasicAI extends Controller {
	protected static Random generator = new Random();
	protected static float reactionTime = .5f;
	protected static int[][] chances = new int[][] {
			{ 28, 56, 64, 60, 60, 64, 60, 60, 64, 21, 42, 64 },
			{ 27, 50, 64, 52, 55, 64, 52, 58, 64, 18, 36, 64 },
			{ 26, 44, 64, 45, 50, 64, 45, 56, 64, 16, 31, 64 },
			{ 25, 38, 64, 37, 45, 64, 37, 54, 64, 13, 26, 64 },
			{ 24, 32, 64, 30, 41, 64, 30, 53, 64, 11, 21, 64 },
			{ 20, 26, 64, 22, 30, 64, 22, 56, 64, 8, 16, 64 },
			{ 16, 20, 64, 15, 20, 64, 15, 59, 64, 6, 11, 64 },
			{ 12, 14, 64, 8, 10, 64, 8, 52, 64, 3, 5, 64 },
			{ 8, 8, 64, 0, 0, 64, 0, 64, 64, 0, 0, 64 } };

	protected PlayableCharacter nearestPlayer;
	protected float distanceToPlayer;
	protected Angle angleToPlayer;
	protected PhysicsEntity nearestEntity;
	protected float distanceToEntity;
	protected Angle angleToEntity;

	protected int level;
	protected String mode;
	protected float timer;
	protected boolean shouldIBeInTheAir;
	
	private boolean init;

	public BasicAI(PlayableCharacter character, int level) {
		super(character);
		this.level = level;
		this.mode = "";
		this.timer = 0;
		this.init = false;
	}

	@Override
	public String getName() {
		return "CPU";
	}

	@Override
	public String getAdditionalInfo() {
		return String.valueOf(this.level);
	}
	
	public boolean random(){
		return Math.random() < 1.0/6.0;
	}
	
	public boolean randomHigh(){
		return Math.random() < Math.sqrt(this.level)/17.0;
	}
	
	public boolean randomLow(){
		return Math.random() < Math.sqrt(9-this.level)/17.0;
	}
	
	public boolean randomMid(){
		if (this.level > 5)
			return randomLow() || randomLow();
		else
			return randomHigh() || randomHigh();
	}
	
	public boolean isInRange(){
		if (nearestPlayer == null)
			return false;
		Vector2f vec = nearestPlayer.getPosition();
		if (character.getFacing()){
			return (this.character.getPosition().x < vec.x && Math.abs(this.character.getPosition().x-vec.x) < 700/(this.level+1)) || Math.abs(this.character.getPosition().x-vec.x) < 250/(this.level+1);
		}
		return (this.character.getPosition().x > vec.x && Math.abs(this.character.getPosition().x-vec.x) < 700/(this.level+1)) || Math.abs(this.character.getPosition().x-vec.x) < 250/(this.level+1);
	}

	@Override
	public Map<String, Boolean> update() {
		Map<String, Boolean> controlMap = new HashMap<String, Boolean>();
		
		/*Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>(this.character.seeActionMap());
		CharacterAction action = this.character.getCurrentAction();
		actionMap.put("Dodge Right", CharacterActionFactory
								.getDodge(this.character, new Angle(0), (short) 0,
										character.getGroundDodge()));
		actionMap.put("Dodge Left", CharacterActionFactory.getDodge(
				this.character, new Angle(180), (short) 0,
				character.getGroundDodge()));
		actionMap.put("Sidestep", CharacterActionFactory.getDodge(
				this.character, new Angle(0), character.getStationaryDodge(), 0));
		actionMap.put("Shield", CharacterActionFactory.getShield(
								this.character, 1));
		actionMap.put("Right Air Dodge", CharacterActionFactory.getDodge(
								this.character, new Angle(0), (short) 0, character.getAirDodge()));
		actionMap.put("Left Air Dodge", CharacterActionFactory.getDodge(
				this.character, new Angle(180), (short) 0, character.getAirDodge()));
		actionMap.put("Up Air Dodge", CharacterActionFactory.getDodge(
				this.character, new Angle(90), (short) 0, character.getAirDodge()));
		actionMap.put("Down Air Dodge", CharacterActionFactory.getDodge(
				this.character, new Angle(-90), (short) 0, character.getAirDodge()));
		actionMap.put("Air Dodge", CharacterActionFactory.getDodge(
								this.character, new Angle(0), character.getStationaryDodge(), 0));
		
		if (!this.character.isOnGround()){
			actionMap.remove("Jump 1");
			actionMap.remove("Walk Left");
			actionMap.remove("Walk Right");
			actionMap.remove("Sprint Left");
			actionMap.remove("Sprint Right");
			actionMap.remove("Dodge Right");
			actionMap.remove("Dodge Left");
			actionMap.remove("Sidestep");
			actionMap.remove("Shield");
		} else {
			actionMap.remove("Jump 2");
			actionMap.remove("Right Air Dodge");
			actionMap.remove("Left Air Dodge");
			actionMap.remove("Up Air Dodge");
			actionMap.remove("Down Air Dodge");
			actionMap.remove("Air Dodge");
		}*/
		
		if (!init){
			this.character.setDamageModifier(this.character.getDamageModifier()*(level+1)*((level + 64)/64f));
			init = true;
		}
		
		if (this.timer >= reactionTime) {
			this.timer -= reactionTime;
			controlMap.put("Up", false);
			controlMap.put("Right", false);
			controlMap.put("Down", false);
			controlMap.put("Left", false);
			controlMap.put("Attack", false);
			controlMap.put("Special", false);
			controlMap.put("Shield", false);
			if (this.character.getPosition().x < Main.getGameState().getWorld().getStage().getCameraBounds().getLowerLeft().x){
				controlMap.put("Right", true);
			} else if (this.character.getPosition().x > Main.getGameState().getWorld().getStage().getCameraBounds().getUpperRight().x){
				controlMap.put("Left", true);
			} if (this.character.isOnGround() || character.getCurrentAction().equals(CharacterActionFactory.getLedgeHang(this.character, 1))){
				if (random()){
			if (randomHigh()){
				controlMap.put("Up", true);
			} if (randomLow()){
				controlMap.put("Down", true);
			} if (randomMid()){
				controlMap.put("Left", true);
			} if (randomMid()){
				controlMap.put("Right", true);
			} if (randomMid() && isInRange()){
				controlMap.put("Attack", true);
			} if (randomLow() && isInRange()){
				controlMap.put("Special", true);
			} if (randomHigh()){
				controlMap.put("Shield", true);
			}
				} else {
					idol(controlMap);
				}
			} else {
				onFall(controlMap);
			}
		} else {
			this.timer += Utilities.lockedTPF;
		}
		/*
		if (this.timer >= reactionTime) {
			this.timer -= reactionTime;

			controlMap.put("Up", false);
			controlMap.put("Right", false);
			controlMap.put("Down", false);
			controlMap.put("Left", false);
			controlMap.put("Attack", false);
			controlMap.put("Special", false);
			controlMap.put("Shield", false);

			this.nearestPlayer = Main.getGameState().getWorld()
					.getClosestPlayerToCharacter(this.character);
			this.distanceToPlayer = 0;
			this.angleToPlayer = new Angle(0);

			if (this.nearestPlayer != null) {
				this.distanceToPlayer = this.nearestPlayer
						.getAppropriateBounds()
						.getLowerMidpoint()
						.distance(
								this.character.getAppropriateBounds()
										.getLowerMidpoint());
				this.angleToPlayer = Utilities.getPhysicsUtility()
						.calculateCorrectAngle(
								this.nearestPlayer.getAppropriateBounds()
										.getLowerMidpoint(),
								this.character.getAppropriateBounds()
										.getLowerMidpoint());
			}

			this.nearestEntity = Main
					.getGameState()
					.getWorld()
					.getClosestObject(
							this.character.getAppropriateBounds()
									.getLowerMidpoint(), this.character);
			this.distanceToEntity = 0;
			this.angleToEntity = new Angle(0);

			if (this.nearestEntity != null) {
				this.distanceToEntity = this.nearestEntity
						.getAppropriateBounds()
						.getLowerMidpoint()
						.distance(
								this.character.getAppropriateBounds()
										.getLowerMidpoint());
				this.angleToEntity = Utilities.getPhysicsUtility()
						.calculateCorrectAngle(
								this.nearestEntity.getAppropriateBounds()
										.getLowerMidpoint(),
								this.character.getAppropriateBounds()
										.getLowerMidpoint());
			}

			ViolentEntity dangerousEntity = null;
			float distanceToDanger = Float.MAX_VALUE;
			Angle angleToDanger = null;

			if (this.nearestPlayer == null)
				return controlMap;
			
			if (this.nearestPlayer.getDamageDealt() > 0) {
				dangerousEntity = this.nearestPlayer;
				distanceToDanger = this.distanceToPlayer;
				angleToDanger = this.angleToPlayer;
			}

			if (this.nearestEntity instanceof ViolentEntity
					&& ((ViolentEntity) this.nearestEntity).getDamageDealt() > 0) {
				if (this.distanceToEntity < this.distanceToPlayer) {
					dangerousEntity = (ViolentEntity) this.nearestEntity;
					distanceToDanger = this.distanceToEntity;
					angleToDanger = this.angleToEntity;
				}
			}

			if (dangerousEntity != null && distanceToDanger < 70) {
				this.beforeAttack(controlMap, dangerousEntity,
						distanceToEntity, angleToEntity);
			} else {
				if (!this.shouldIBeInTheAir && !this.character.isOnGround()) {
					this.onFall(controlMap);
				} else {
					this.idol(controlMap);
				}
			}
		} else {
			this.timer += Utilities.lockedTPF;
		}
		*/
	
		return controlMap;
	}

	public void idol(Map<String, Boolean> controlMap) {
		int r = generator.nextInt(64);

		if (this.mode.equals("Attack")) {
			if (this.distanceToPlayer < 20*this.character.getPhysicsInfo().getMoveSpeed()) {
				if (Math.random() < 2.0/9.0){
					controlMap.put(this.character.getFacing() ? "Right"
							: "Left", true);
				} else if (Math.random() < 2.0/7.0){
					controlMap.put("Down", true);
				} else if (Math.random() < 1.0/5.0){
					controlMap.put("Up", true);
				}
				if (Math.random() < 0.5){
					controlMap.put("Special", true);
				} else {
					controlMap.put("Attack", true);
				}
				this.mode = "";
			} else if (this.angleToPlayer.getValue() > 90) {
				controlMap.put("Left", true);
			} else {
				controlMap.put("Right", true);
			}
		} else {
			if (r < chances[this.level][0]/2f) {
				// Does nothing.
			} else if (r < chances[this.level][1]/2f) {
				if (generator.nextInt(4) == 0) { // 1 in 10 chance of turning
													// around.
					controlMap.put(this.character.getFacing() ? "Left"
							: "Right", true);
				} else {
					controlMap.put(this.character.getFacing() ? "Right"
							: "Left", true);
				}
			} else if (r < chances[this.level][2]*4f) {
				this.mode = "Attack";
			}
		}
	}

	public void beforeAttack(Map<String, Boolean> controlMap,
			ViolentEntity attacker, float distanceToAttack, Angle angleToAttack) {
		int r = generator.nextInt(64);

		// 0 is currently the shield value since shields don't exist.
		if (attacker.getDamageDealt() > 0) {
			if (r < chances[this.level][3]) {
				this.idol(controlMap);
			} else if (r < chances[this.level][4]) {
				controlMap.put("Down", true);
			} else if (r < chances[this.level][5]) {
				controlMap.put("Up", true);
			}
		} else {
			if (r < chances[this.level][6]) {
				this.idol(controlMap);
			} else if (r < chances[this.level][7]) {
				controlMap.put("Down", true);
			} else if (r < chances[this.level][8]) {
				controlMap.put("Up", true);
			}
		}
	}

	public void onFall(Map<String, Boolean> controlMap) {
		int r = generator.nextInt(64);

		if (r < chances[this.level][9]/2f) {
			// Stand still.
		} else if (r < chances[this.level][10]/2f) {
			controlMap.put(this.character.getFacing() ? "Right" : "Left", true);
		} else if (r < chances[this.level][11]*4f) {
			if (this.character.getVelocity().y <= 0) {
				controlMap.put("Up", true);
			}

			Vector2f nearestLedge = Main.getGameState().getWorld().getStage()
					.getNearestLedge(this.character.getPosition());

			// Currently attempting to recover means moving towards the closest
			// player.

			if (nearestLedge != null
					&& nearestLedge.x < this.character.getPosition().x
					&& !Main.getGameState().getWorld().getStage()
							.getLedgeDirection(nearestLedge)) {
				controlMap.put("Left", true);
			} else if (nearestLedge != null
					&& nearestLedge.x > this.character.getPosition().x
					&& Main.getGameState().getWorld().getStage()
							.getLedgeDirection(nearestLedge)) {
				controlMap.put("Right", true);
			}

			if (nearestLedge != null
					&& Math.abs(nearestLedge.y - this.character.getPosition().y) < 200) {
				controlMap.put("Up", true);
				controlMap.put("Special", true);
			}

		}
	}
}
