package entities.characters;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionFinalSmashFreeze;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.general.GenoPowerStar;
import entities.projectiles.GenoWhirl;

import java.util.List;
import java.util.Map;

/**
 * The character Geno within SSBS.
 * 
 * @author Matthew Ludwig
 */
public class Geno extends PlayableCharacter {
	private int aBoost, bBoost, sideBoost, upBoost, downBoost, orangeBoost,
			yellowBoost, aTime, bTime, sideTime, upTime, downTime, orangeTime,
			yellowTime;
	private float imageSpeedModifier;
	private int isOrange;
	public static final float[] STAT = CharacterStats.GenoStats;
	public static final float[] DMG = CharacterStats.GenoDamage;
	public static final float[] KNBK = CharacterStats.GenoKnockback;
	public static final float[] PRTY = CharacterStats.GenoPriority;
	public static final float[] ISPD = CharacterStats.GenoImageSpeed;
	public static final short[][] SECT = CharacterStats.GenoSectionFrames;

	public Geno() {
		super("Geno", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]),
				STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
		this.scale = .9f;
		setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
        grabHitbox = 25;
        this.grabOffset = -5;
        
		aBoost = 0;
		bBoost = 0;
		sideBoost = 0;
		upBoost = 0;
		downBoost = 0;
		orangeBoost = 0;
		yellowBoost = 0;
		aTime = 0;
		bTime = 0;
		sideTime = 0;
		upTime = 0;
		downTime = 0;
		orangeTime = 0;
		yellowTime = 0;
		imageSpeedModifier = 1;
	}

	@Override
	public void updateEntity() {
		super.updateEntity();
		aTime++;
		bTime++;
		sideTime++;
		upTime++;
		downTime++;
		orangeTime++;
		yellowTime++;
		double time = 10.0 / Utilities.lockedTPF;
		if (aTime > time)
			aBoost = 0;
		if (bTime > time)
			bBoost = 0;
		if (sideTime > time)
			sideBoost = 0;
		if (upTime > time)
			upBoost = 0;
		if (downTime > time)
			downBoost = 0;
		if (orangeTime > time)
			orangeBoost = 0;
		if (yellowTime > time)
			yellowBoost = 0;
		PhysicsInfo p = this.physicsInfo;
		p.setWeight(STAT[0] * logThing(1.29f, downBoost)
				* logThing(1.39f, orangeBoost) * logThing(1.49f, yellowBoost) * statModifiers[0]);
		p.setFallSpeed(STAT[1] * logThing(0.94f, downBoost)
				* logThing(0.92f, orangeBoost) * logThing(0.90f, yellowBoost) * statModifiers[1]);
		p.setJumpSpeed(STAT[2] * logThing(1.16f, upBoost)
				* logThing(1.20f, orangeBoost) * logThing(1.24f, yellowBoost) * statModifiers[2]);
		p.setMoveSpeed(STAT[3] * logThing(1.24f, sideBoost)
				* logThing(1.30f, orangeBoost) * logThing(1.36f, yellowBoost) * statModifiers[3]);
		this.priorityModifier = STAT[4] * logThing(1.12f, bBoost)
				* logThing(1.30f, orangeBoost) * logThing(1.48f, yellowBoost) * statModifiers[4];
		this.knockbackModifier = STAT[5] * logThing(1.60f, bBoost)
				* logThing(1.54f, orangeBoost) * logThing(1.48f, yellowBoost) * statModifiers[5];
		this.setDamageModifier(STAT[6] * logThing(1.42f, aBoost)
				* logThing(1.47f, orangeBoost) * logThing(1.52f, yellowBoost) * statModifiers[6]);
		this.imageSpeedModifier = logThing(1.33f, sideBoost)
				* logThing(1.41f, orangeBoost) * logThing(1.49f, yellowBoost) * statModifiers[7];
		this.recoveryForce = STAT[8] * logThing(1.08f, upBoost)
				* logThing(1.10f, orangeBoost) * logThing(1.12f, yellowBoost) * statModifiers[8];

		spawnStar(aBoost, 0);
		spawnStar(bBoost, 3);
		spawnStar(sideBoost, 1);
		spawnStar(upBoost, 2);
		spawnStar(downBoost, 4);
	}

	public void spawnStar(float boost, int frame) {
		float r = (float) (Math.random()
				* (this.getAppropriateBounds().getUpperRight().x - this
						.getAppropriateBounds().getUpperLeft().x) + this
				.getAppropriateBounds().getUpperLeft().x);
		float s = (float) (Math.random() * 8
				+ this.getAppropriateBounds().getUpperMidpoint().y - 8);
		if ((boost + orangeBoost + yellowBoost) > 0
				&& (Math.random() < (boost + orangeBoost + yellowBoost + 2)
						* Utilities.lockedTPF)) {
			GenoPowerStar star = new GenoPowerStar(frame);
			star.setPosition(r, s);
			Main.getGameState().spawnEntity(star);
		}
	}

	public float logThing(float a, int b) {
		float f = 1;
		for (int i = 0; i < b; i++) {
			f *= (a + 1.0 * i) / (i + 1);
		}
		return f;
	}

	@Override
	public Kirby.KirbyAbility getKirbyAbility() {
		return Kirby.KirbyAbility.PLASMA;
	}

	@Override
	protected CharacterAction getNeutralA() {
		CharacterAction action = new CharacterAction(this, "neutralA",
				StayingPower.EXTREME, ISPD[0]);
		action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
		action.addModifier(new ActionPressMultiHit("Attack"));
		action.addModifier(new NeutralAAction());
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_08.ogg", -1));
		return action;
	}

	@Override
	protected CharacterAction getSideA() {
		CharacterAction action = new CharacterAction(this, "sideA",
				StayingPower.EXTREME, ISPD[1]);
		action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], SECT[1]));
		action.addModifier(new SideAAction());
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_01.ogg", -1));
		return action;
	}

	@Override
	protected CharacterAction getUpA() {
		CharacterAction action = new CharacterAction(this, "upA",
				StayingPower.EXTREME, ISPD[2]);
		action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], true,
				SECT[2]));
		action.addModifier(new UpAAction());
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_05.ogg", -1));
		action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
		return action;
	}

	@Override
	protected CharacterAction getDownA() {
		CharacterAction action = new CharacterAction(this, "downA",
				StayingPower.EXTREME, ISPD[3]);
		action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], true,
				SECT[3]));
		action.addModifier(new DownAAction());
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_01.ogg", -1));
		action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
		return action;
	}

	@Override
	protected CharacterAction getNeutralB() {
		CharacterAction action = new CharacterAction(this, "neutralB",
				StayingPower.EXTREME, ISPD[4]);
		action.addModifier(new ActionAttack(DMG[4], KNBK[4], PRTY[4], true));
		action.addModifier(new NeutralBAction());
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_13.ogg", -1));
		action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
		return action;
	}

	@Override
	protected CharacterAction getSideB() {
		CharacterAction action = new CharacterAction(this, "sideB",
				StayingPower.EXTREME, ISPD[5]);
		action.addModifier(new ActionSpawnEntity(new short[] { 3 }, 1,
				GenoWhirl.class, this).setXOffset(20, true));
		action.addModifier(new SideBAction());
		return action;
	}

	@Override
	protected CharacterAction getUpB() {
		CharacterAction action = new CharacterAction(this, "upB",
				StayingPower.EXTREME, ISPD[6]);
		action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
		action.addModifier(new ActionToggleAttribute(Attribute.VECTORING));
		action.addModifier(new UpBAction());
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_05.ogg", -1));
		return action;
	}

	@Override
	protected CharacterAction getDownB() {
		CharacterAction action = new CharacterAction(this, "downB",
				StayingPower.EXTREME, ISPD[7]);
		action.addModifier(new ActionAttack(DMG[7], KNBK[7], PRTY[7], true,
				SECT[7]));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1));
		action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
		action.addModifier(new DownBAction());
		return action;
	}

	@Override
	protected CharacterAction getFinalSmash() {
		CharacterAction action = new CharacterAction(this, "finalSmash",
				StayingPower.EXTREME, ISPD[8]);
		action.addAnimation(1, "finalSmash2");
		action.addModifier(new ActionAttack(DMG[8], KNBK[8], PRTY[8], true));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_13.ogg", -1));
		action.addModifier(new ActionFinalSmashFreeze());
		action.addModifier(new FinalSmashAction());
		return action;
	}

	@Override
	protected void overrideDefaults(Map<String, CharacterAction> actionMap) {
	}

	public class NeutralAAction extends ActionModifier {
		private int numHits;

		public NeutralAAction() {
		}

		@Override
		protected void onInit() {
			numHits = 0;
			this.action.updateLimit = this.action.scaleUpdateLimit(ISPD[0]
					* imageSpeedModifier);
		}

		@Override
		protected boolean onUpdate() {
			if (((ViolentEntity) this.action.entity).wasCollisionSuccesful()) {
				numHits++;
				if (numHits == 3) {
					aTime = 0;
					aBoost++;
				}
			}
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {
		}
	}

	public class SideAAction extends ActionModifier {
		public SideAAction() {

		}

		@Override
		protected void onInit() {
			this.action.updateLimit = this.action.scaleUpdateLimit(ISPD[1]
					* imageSpeedModifier);
		}

		@Override
		protected boolean onUpdate() {
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {
		}
	}

	public class UpAAction extends ActionModifier {
		public UpAAction() {

		}

		@Override
		protected void onInit() {
			this.action.updateLimit = this.action.scaleUpdateLimit(ISPD[2]
					* imageSpeedModifier);
		}

		@Override
		protected boolean onUpdate() {
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {
		}
	}

	public class DownAAction extends ActionModifier {
		public DownAAction() {

		}

		@Override
		protected void onInit() {
			this.action.updateLimit = this.action.scaleUpdateLimit(ISPD[3]
					* imageSpeedModifier);
		}

		@Override
		protected boolean onUpdate() {
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {
		}
	}

	public class NeutralBAction extends ActionModifier {
		private int numHits;

		public NeutralBAction() {
		}

		@Override
		protected void onInit() {
			numHits = 0;
			this.action.updateLimit = this.action.scaleUpdateLimit(ISPD[4]
					* imageSpeedModifier);
		}

		@Override
		protected boolean onUpdate() {
			if (((ViolentEntity) this.action.entity).wasCollisionSuccesful()) {
				numHits++;
				if (numHits == 1) {
					bTime = 0;
					bBoost++;
				}
			}
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {
		}
	}

	public class SideBAction extends ActionModifier {
		public SideBAction() {

		}

		@Override
		protected void onInit() {
			this.action.updateLimit = this.action.scaleUpdateLimit(ISPD[5]
					* imageSpeedModifier);
		}

		@Override
		protected boolean onUpdate() {
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {
		}
	}

	public class UpBAction extends ActionModifier {
		private int numHits;

		public UpBAction() {
		}

		@Override
		protected void onInit() {
			numHits = 0;
			this.action.updateLimit = this.action.scaleUpdateLimit(ISPD[6]
					* imageSpeedModifier);
		}

		@Override
		protected boolean onUpdate() {
			if (((ViolentEntity) this.action.entity).wasCollisionSuccesful()) {
				numHits++;
				if (numHits == 1) {
					upTime = 0;
					upBoost++;
				}
			}
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {
		}
	}

	public class DownBAction extends ActionModifier {
		private int numHits;

		public DownBAction() {
		}

		@Override
		protected void onInit() {
			numHits = 0;
			this.action.updateLimit = this.action.scaleUpdateLimit(ISPD[7]
					* imageSpeedModifier);
		}

		@Override
		protected boolean onUpdate() {
			if (((ViolentEntity) this.action.entity).wasCollisionSuccesful()) {
				numHits++;
				if (numHits == 1) {
					downTime = 0;
					downBoost++;
				}
			}
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {
			//((PlayableCharacter) this.action.entity).setFinalSmash(true);
		}
	}

	public class FinalSmashAction extends ActionModifier {
		int counter;
		float scale;
		float time;
		boolean init;

		boolean torf;

		public FinalSmashAction() {
			init = false;
		}

		@Override
		protected void onInit() {
			// this.action.updateLimit = 0;
			scale = 0.9f;
			time = 1.0f;
			if (isOrange == 0) {
				if (Math.random() < 0.75) {
					isOrange = 1;
				} else {
					isOrange = 2;
					time = 1.5f;
					this.action.setCurrentAnimation(1);
				}
				//init = true;
				torf = true;
			}
		}

		@Override
		protected boolean onUpdate() {
			counter++;
			time -= Utilities.lockedTPF;
			scale += 1.3 * Utilities.lockedTPF;
			if (isOrange == 2)
				scale += 4 * Utilities.lockedTPF;
			// this.action.entity.setScale(2);
			if (torf){
				this.action.entity.setPosition(this.action.entity.getPosition().x, this.action.entity.getPosition().y+scale*2);
				this.action.entity.setScale(scale);
			}
			torf = !torf;
			List<PlayableCharacter> plist = Main.getGameState().getWorld().getPlayerList();
			if (counter > 0.15 / Utilities.lockedTPF) {
			for (PlayableCharacter pc : plist) {
				if (!this.action.entity.equals(pc) && pc.getPosition().subtract(this.action.entity.getPosition()).length() > 25*scale){
					if (isOrange == 1){
						pc.addToDamage(DMG[8]*STAT[7]*0.33f, KNBK[8]*STAT[8]*0.33f, 1, new Angle(0), false, false, false, false, 0, (ViolentEntity)(this.action.entity));
					} else {
						pc.addToDamage(DMG[8]*STAT[7], KNBK[8]*STAT[8], 1, new Angle(0), false, false, false, false, 0, (ViolentEntity)(this.action.entity));
					}
					if (isOrange == 2) {
						orangeTime = 0;
						orangeBoost++;
					} else {
						yellowTime = 0;
						yellowBoost++;
					}
					counter = 0;
				}
			}
			}
			if (time <= 0) {
				return true;
			}
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			if (time <= 0) {
				return true;
			}
			return false;
		}

		@Override
		protected void cleanUp() {
			this.action.entity.setScale(0.9f);
			//System.out.println("hm?");
			isOrange = 0;
		}
	}

	public void setScale(float f) {
		this.scale = f;
	}

	public void setSide() {
		sideTime = 0;
		sideBoost++;
		// System.out.println(this.imageSpeedModifier);
	}

	public float getImageSpeedMod() {
		return imageSpeedModifier;
	}
}
