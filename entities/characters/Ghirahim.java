package entities.characters;

import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.Ghirahim.DownB;
import entities.characters.Ghirahim.SideBAction;
import entities.characters.Ghirahim.UpBAction;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionCompleteNumberOfAnimations;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.projectiles.GhirahimMonster;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import physics.PhysicsModifier;

/**
 * The character Ghirahim within SSBS.
 *
 * @author Matthew Ludwig
 */
public class Ghirahim extends PlayableCharacter {
    private static Random generator = new Random();
    public static final float[] STAT = CharacterStats.GhirahimStats;
	public static final float[] DMG = CharacterStats.GhirahimDamage;
	public static final float[] KNBK = CharacterStats.GhirahimKnockback;
	public static final float[] PRTY = CharacterStats.GhirahimPriority;
	public static final float[] ISPD = CharacterStats.GhirahimImageSpeed;
	public static final short[][] SECT = CharacterStats.GhirahimSectionFrames;
	
    public Ghirahim() {
        super("Ghirahim", new PhysicsInfo(STAT[0], STAT[1], STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6], STAT[7], STAT[8]);
        this.scale = .76f;
        setThrowPower(new float[]{DMG[9], DMG[10], DMG[11], DMG[12]}, new float[]{KNBK[9], KNBK[10], KNBK[11], KNBK[12]});
        grabHitbox = 10;
        this.grabOffset = -27;
        this.grabIsTether = true;
    }
    
    @Override
    public Kirby.KirbyAbility getKirbyAbility() {
        return Kirby.KirbyAbility.FIGHTER;
    }
    
    @Override
    protected CharacterAction getNeutralA() {
        CharacterAction action = new CharacterAction(this, "neutralA", StayingPower.EXTREME, ISPD[0]);
        action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
        action.addModifier(new ActionPressMultiHit("Attack"));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ghirahim/neutralA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_03.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getSideA() {
        CharacterAction action = new CharacterAction(this, "sideA", StayingPower.EXTREME, ISPD[1]);
        action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], SECT[1]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ghirahim/sideA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_05.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getUpA() {
        CharacterAction action = new CharacterAction(this, "upA", StayingPower.EXTREME, ISPD[2]);
        action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ghirahim/upA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_14.ogg", -3));
        action.addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", -2));
        return action;   
    }

    @Override
    protected CharacterAction getDownA() {
        CharacterAction action = new CharacterAction(this, "downA", StayingPower.EXTREME, ISPD[3]);
        action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ghirahim/downA.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_02.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getNeutralB() {
        CharacterAction action = new CharacterAction(this, "neutralB", StayingPower.EXTREME, ISPD[4]);
        action.addModifier(new ActionAttack(DMG[4], KNBK[4], PRTY[4], true, SECT[4]));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ghirahim/neutralB1.ogg", 0));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ghirahim/neutralB2.ogg", 28));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_02.ogg", -1));
        action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
        return action;   
    }

    @Override
    protected CharacterAction getSideB() {
        CharacterAction action = new CharacterAction(this, "sideB", StayingPower.EXTREME, ISPD[5]);
        action.addModifier(new ActionAttack(DMG[5], KNBK[5], PRTY[5], SECT[5]));
        action.addModifier(new ActionToggleAttribute(Attribute.HEAVYARMOR));
        action.addModifier(new ActionMovement(72f, 0f, 0f, true));
        action.addModifier(new SideBAction());
        action.addModifier(new ActionPlaySound("/Character Sounds/Ghirahim/sideB.ogg", 1));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_03.ogg", -1));
        return action;   
    }

    @Override
    protected CharacterAction getUpB() {
        CharacterAction action = new CharacterAction(this, "upB", StayingPower.EXTREME, ISPD[6]);
        action.addModifier(new ActionAttack(DMG[6], KNBK[6], PRTY[6], SECT[6]));
        action.addModifier(new UpBAction());
        action.addModifier(new ActionToggleAttribute(Attribute.CANONLYHITPLAYERS));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ghirahim/upB1.ogg", 0));
        action.addModifier(new ActionPlaySound("/Character Sounds/Ghirahim/upB2.ogg", 2));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_04.ogg", -1));
        return action;
    }

    @Override
    protected CharacterAction getDownB() {
        CharacterAction action = new CharacterAction(this, "downB", StayingPower.EXTREME, ISPD[7]);
        action.addModifier(new ActionAttack(DMG[7], KNBK[7], PRTY[7], true, SECT[7]));
        action.addModifier(new ActionMovement(36f, 0f, 0f, true));
        action.addModifier(new DownB());
        action.addModifier(new ActionPlaySound("/Character Sounds/Ghirahim/downB.ogg", 0));
        action.addModifier(new ActionPlaySound("/Common Sounds/hit_05.ogg", -1));
        return action;   
    }
    
    @Override
    protected CharacterAction getFinalSmash() {
        CharacterAction action = new CharacterAction(this, "finalSmash", StayingPower.EXTREME, ISPD[8]);
        action.addModifier(new ActionPlaySound("/Character Sounds/Ghirahim/finalSmash.ogg", 0));
        action.addModifier(new ActionCompleteNumberOfAnimations(100));
        action.addModifier(new FinalSmash());
        return action;
    }

    @Override
    protected void overrideDefaults(Map<String, CharacterAction> actionMap) {}
    
    public class UpBAction extends ActionModifier {
        private boolean firstFramePast;
        private boolean isDone;
        
        public UpBAction() {
            this.firstFramePast = false;
            this.isDone = false;
        }

        @Override
        public void onInit() {
            this.action.entity.setOnGround(false);
            ((PlayableCharacter) this.action.entity).setVectoring(true);
            this.firstFramePast = false;
            this.isDone = false;
        }

        @Override
        public boolean onUpdate() {
            if (this.action.getCurrentFrameNumber() == 0) {
                this.action.setCurrentFrame(0);
                this.action.entity.addPhysicsModifier(PhysicsModifier.ISSTICKY);
                
                if (this.action.entity.hasCollided() && this.firstFramePast) {
                    this.action.setCurrentFrame(1);
                } else if (this.action.entity.getVelocity().y < 0) {
                    return true;
                }
                
                this.firstFramePast = true;
            } else if (this.action.getCurrentFrameNumber() == 1) {
                this.action.entity.setVelocity(0, 0, false);
                ((PlayableCharacter) this.action.entity).setVectoring(false);
            } else if (this.action.getCurrentFrameNumber() == 2) {
                this.action.entity.removePhysicsModifier(PhysicsModifier.ISSTICKY);
                
                if (!this.isDone) {
                    this.action.entity.setVelocity(Utilities.getGeneralUtility().getBooleanAsSign(!this.action.entity.getFacing()) * 12.5f, 12.5f, true);
                    this.isDone = true;
                } else {
                    ((ViolentEntity) this.action.entity).setAttackCapability(0, 0, 0, false);
                }
            }
            
            return ((PhysicsEntity) this.action.entity).isOnGround();
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
            this.action.entity.removePhysicsModifier(PhysicsModifier.ISSTICKY);
        }
    }
    
    public class SideBAction extends ActionModifier {
        public SideBAction() {
            
        }

        @Override
        public void onInit() {
        }

        @Override
        public boolean onUpdate() {
            if(((PhysicsEntity) this.action.entity).hasCollided() && this.action.getCurrentFrameNumber() < 11) {
                this.action.setCurrentFrame(11);
            }

            if(this.action.getCurrentFrameNumber() >= 11) {
                ((PhysicsEntity) this.action.entity).setPureXVelocity(0);
                ((PhysicsEntity) this.action.entity).setPureYVelocity(0);
            }

            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
    
    public class DownB extends ActionModifier {
        private float counter = 0;

        public DownB() {
            
        }

        @Override
        public void onInit() {}
        
        @Override
        public boolean onUpdate() {
            if(!((PhysicsEntity) this.action.entity).hasCollided())
            {
                this.action.setCurrentFrame(0);
                this.counter += Utilities.lockedTPF;
                return this.counter >= .5;
            }

            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
            this.counter = 0;
        }
    }

    public class FinalSmash extends ActionModifier {        
        private List<GhirahimMonster> spawnedMonsters;
        private float updateCounter;
        
        private float moveTimer;
        
        public FinalSmash() {
            this.spawnedMonsters = new LinkedList<GhirahimMonster>();
            this.updateCounter = 0;
            this.moveTimer = 4;
        }

        @Override
        public void onInit() {
            this.updateCounter = 0;
            this.moveTimer = 4;
        }

        @Override
        public boolean onUpdate() {
            if (this.updateCounter >= .2f) {
                this.spawnRacer();
                this.updateCounter -= .2f;
            } else {
                this.updateCounter += Utilities.lockedTPF;
            }
            
            this.moveTimer -= Utilities.lockedTPF;
            
            return this.moveTimer <= 0;
        }
        
        public void spawnRacer() {
            if (this.spawnedMonsters.size() > 100) {
                this.cleanUpRacers();
            }
            
            GhirahimMonster monster = new GhirahimMonster((Ghirahim) this.action.entity, generator.nextInt(17));
            
            Main.getGameState().spawnEntity(monster);
            
            monster.setPosition(
                    this.action.entity.getFacing() ? 
                        Main.getGameState().getWorld().getStage().getWorldBounds().getLeftMidpoint().x + 10 : 
                        Main.getGameState().getWorld().getStage().getWorldBounds().getRightMidpoint().x - 10, 
                    Main.getGameState().getWorld().getStage().getCameraBounds().getDimensions().y * (1 - generator.nextFloat()));
            this.spawnedMonsters.add(monster);
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {

        }
        
        public void cleanUpRacers() {
            for (PhysicsEntity entity : this.spawnedMonsters) {
                entity.setDead(true);
            }
            
            this.spawnedMonsters.clear();
        }
    }
}
