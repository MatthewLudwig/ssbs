package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionCompleteNumberOfTicks;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class CastleDededeOElectricWall extends ViolentEntity {

	private float timer;
	private int state;
	private boolean init;

	public CastleDededeOElectricWall() {
		super("Castle Dedede O", 1, new PhysicsInfo(0, 0, 0, 0), 1f, 1f, 1f);
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		this.isInvincible = true;
		this.team = Team.NONE;
		timer = 0;
		state = 0;
		init = false;
	}

	public void activate() {
		state = 1;
		timer = 0;
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();

		if (!init) {
			init = true;
			float x = this.getPosition().x;
			if (x < 113) {
				this.forceCurrentAction(actionMap.get("Left"));
			} else if (x < 311) {
				this.forceCurrentAction(actionMap.get("Top"));
			} else {
				this.forceCurrentAction(actionMap.get("Right"));
			}
		}

		if (state == 0) {
			alpha = 0;
			this.setAttackCapability(0f, 0f, 0f, true);
		}
		if (state == 1) {
			alpha = 0.5f;
			this.setAttackCapability(0f, 0f, 0f, true);
			if (timer > 1.5f) {
				state = 2;
				timer = 0;
			}
			timer += Utilities.lockedTPF;
		}
		if (state == 2) {
			alpha = 1;
			this.setAttackCapability(20f, 2f, 26f, true);
			if (timer > 6f) {
				state = 0;
				timer = 0;
			}
			timer += Utilities.lockedTPF;
		}

	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "dededeElectricWall0",
				StayingPower.NONE, 1f).addModifier(new ActionPlaySound("/Common Sounds/electric_00.ogg", -1));
		actionMap.put("Left", a);
		a = new CharacterAction(this, "dededeElectricWall1", StayingPower.LOW,
				1f).addModifier(new ActionCompleteNumberOfTicks(
				Integer.MAX_VALUE)).addModifier(new ActionPlaySound("/Common Sounds/electric_00.ogg", -1));;
		actionMap.put("Top", a);
		a = new CharacterAction(this, "dededeElectricWall2", StayingPower.LOW,
				1f).addModifier(new ActionCompleteNumberOfTicks(
				Integer.MAX_VALUE)).addModifier(new ActionPlaySound("/Common Sounds/electric_00.ogg", -1));;
		actionMap.put("Right", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "Left";
	}

}
