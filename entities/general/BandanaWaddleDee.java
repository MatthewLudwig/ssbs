package entities.general;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.scene.Node;

import engine.Angle;
import engine.Main;
import engine.PictureLayer;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.characters.actions.ActionCompleteNumberOfTicks;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class BandanaWaddleDee extends PhysicsEntity {
	private float timeCheering;
	private float timeUntilBored;
	private int[] livesPerPlayer;
	private float[] damagePerPlayer;
    
    public BandanaWaddleDee() {
        super("Castle Dedede O", 1, new PhysicsInfo(0, 1, 1, 1));
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
        timeCheering = 0;
        timeUntilBored = 7f;
        List<PlayableCharacter> characters = Main.getGameState().getWorld().getPlayerList();
        livesPerPlayer = new int[characters.size()];
        damagePerPlayer = new float[characters.size()];
    }
    
    public void onDeath() {}
    
    @Override
    public void updateEntity() {
        super.updateEntity();
        
        List<PlayableCharacter> characters = Main.getGameState().getWorld().getPlayerList();
        int[] tempLives = new int[characters.size()];
        boolean shouldCheer = false;
        for (int i = 0; i < tempLives.length; i++){
        	tempLives[i] = characters.get(i).getNumberOfLives();
        	if (tempLives[i] < livesPerPlayer[i])
        		shouldCheer = true;
        }
        livesPerPlayer = tempLives;
        if (shouldCheer){
        	this.forceCurrentAction(actionMap.get("Cheering"));
        	timeUntilBored = 7f;
        }
        
        float[] tempDmg = new float[characters.size()];
        for (int i = 0; i < tempDmg.length; i++){
        	tempDmg[i] = characters.get(i).getDamage();
        	if (tempDmg[i] > damagePerPlayer[i])
        		timeUntilBored = 7f;
        }
        damagePerPlayer = tempDmg;
        timeUntilBored -= Utilities.lockedTPF;
        
        if (timeUntilBored > 0 && this.currentAction.equals(actionMap.get("Bored"))){
        	this.forceCurrentAction(actionMap.get("Normal"));
        } else if (timeUntilBored <= 0 && this.currentAction.equals(actionMap.get("Normal"))){
        	this.setCurrentAction(actionMap.get("Bored"));
        }
        
        //this.display.setLocalTranslation(this.display.getLocalTranslation().x, this.display.getLocalTranslation().y, this.display.getLocalTranslation().z - 0.125f);
    }
    
    @Override
    protected final String getDefaultAction() {
    	return "Normal";
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        CharacterAction action = new CharacterAction(this, "waddleDeeNormalAnim", StayingPower.NONE, 0.4f);
        actionMap.put("Normal", action);
        action = new CharacterAction(this, "waddleDeeKoAnim", StayingPower.BARELY, 0.70f).addModifier(new ActionCompleteNumberOfTicks((int) (1f/Utilities.lockedTPF)));
        actionMap.put("Cheering", action);
        action = new CharacterAction(this, "waddleDeeBoredAnimation", StayingPower.BARELY, 0.25f).addModifier(new ActionCompleteNumberOfTicks(Integer.MAX_VALUE));
        actionMap.put("Bored", action);
        return actionMap;
    }
}
