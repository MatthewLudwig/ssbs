package entities.general;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.Team;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

import physics.PhysicsModifier;

public class RingOut extends PhysicsEntity {
	private String name;

	public RingOut(Team t, String control, Vector2f velocity) {
		super("Ring Out", 1, new PhysicsInfo(0, 1, 1, 1));
		if (control.equals("CPU")) {
			name = "cpu";
		} else if (t.equals(Team.RED)) {
			name = "1";
		} else if (t.equals(Team.BLUE)) {
			name = "2";
		} else if (t.equals(Team.YELLOW)) {
			name = "3";
		} else if (t.equals(Team.GREEN)) {
			name = "4";
		}
		this.scale = 2;
		this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
	}

	public void updateEntity() {
		super.updateEntity();
		rotateUpTo(Utilities.getPhysicsUtility().calculateCorrectAngle(
				new Vector2f(Main.getGameState().getWorld().getStage()
						.getSizeX() / 2f, Main.getGameState().getWorld()
						.getStage().getSizeY() / 2f), this.getPosition()));
	}

	public void onDeath() {
		
	}

	@Override
	protected final String getDefaultAction() {
		return "OnlyAction";
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("OnlyAction", new CharacterAction(this, name,
				StayingPower.BARELY, 1f).addModifier(new ActionKillEntity()));
		return actionMap;
	}
}
