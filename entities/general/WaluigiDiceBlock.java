package entities.general;

import physics.CollisionInfo;
import physics.PhysicsModifier;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.characters.Link;
import entities.characters.Waluigi;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.projectiles.Projectile;

import java.util.HashMap;
import java.util.Map;

public class WaluigiDiceBlock extends PhysicsEntity {
	private Waluigi obj;
	private float counter;
	private float counter2;
	private boolean freeze;
	private int frame;
	
    public WaluigiDiceBlock(Waluigi object) {
    	super("Waluigi_DEFAULT", 1, new PhysicsInfo(0, 1, 1, 1));
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.obj = object;
        this.scale = object.getScale();
        this.counter = 0f;
        this.counter2 = 0f;
        this.freeze = false;
        this.frame = 0;
        this.facing = true;
    }
    
    public void updateEntity(){
    	super.updateEntity();
    	this.facing = true;
    	
    	if (this.freeze){
    		this.currentAction.updateLimit = 0;
    		int n = this.frame-1;
    		if (n < 0)
    			n += 10;
    		this.currentAction.setCurrentFrame(n);
    		this.counter += Utilities.lockedTPF;
    	}
    	
    	this.counter2 += Utilities.lockedTPF;
    	
    	if (this.counter > 0.75f || this.counter2 > 10f){
    		this.isDead = true;
    	}
    	
    	if (obj.isDead())
    		this.isDead = true;
    }
    
    public void onCollideWith(PhysicsEntity object, CollisionInfo info){
    	super.onCollideWith(object, info);
    	
    	if (object == this.obj && this.obj.isJumping() && this.obj.getVelocity().y > 0){
    		this.frame = this.currentAction.getCurrentFrameNumber();
    		this.obj.setDice(this.frame+1);
    		this.obj.startDicePunch();
    		this.freeze = true;
    	}
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "diceBlock", StayingPower.NONE, Waluigi.ISPD[4]/2f));
        return actionMap;
    }

	@Override
	public void onDeath() {
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}
}
