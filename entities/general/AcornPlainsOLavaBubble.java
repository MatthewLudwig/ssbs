package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Mario;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class AcornPlainsOLavaBubble extends ViolentEntity {
	
	private boolean init;
	public AcornPlainsOLavaBubble() {
		super("Acorn Plains O", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		this.isInvincible = true;
		this.team = Team.NONE;
		this.facing = true;
		init = false;
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		if (!init){
			init = true;
			this.setPureYVelocity(40);
		}
		this.setAttackCapability(7f, 1.1f, 10f, false);
		if (this.getVelocity().y < 0){
			this.rotateUpTo(new Angle(180));
		}
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "lavaBubble",
				StayingPower.NONE, 0.33f).addModifier(new ActionPlaySound(
				"/Common Sounds/blam_00.ogg", -1));
		actionMap.put("Stand", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "Stand";
	}

}
