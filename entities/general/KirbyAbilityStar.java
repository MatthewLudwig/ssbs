package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.scene.Node;

import engine.Angle;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class KirbyAbilityStar extends PhysicsEntity {
    private static final Angle angle = new Angle(120);
    
    private float lifeTimer;
    
    public KirbyAbilityStar(boolean initialFacing) {
        super("Kirby_DEFAULT", 1, new PhysicsInfo(0, 1, 1, 1));
        this.facing = initialFacing;
        this.lifeTimer = 1;
    }
    
    @Override
    public void setUpEntity(int id, Node displayNode, Node menuNode, boolean alwaysDrawn) {
        super.setUpEntity(id, displayNode, menuNode, alwaysDrawn);
        this.setPureXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.facing) * angle.cos() * 30);
        this.setPureYVelocity(angle.sin() * 30);
    }
    
    public void onDeath() {}
    
    @Override
    public void updateEntity() {
        super.updateEntity();
        
        if(this.isOnGround()) {
            this.addToPosition(0, 1);
            this.setYVelocity(15, false);
        }
        
        if (this.lifeTimer <= 0) {
            this.setDead(true);
        } else {
            this.lifeTimer -= Utilities.lockedTPF;
        }
    }
    
    @Override
    protected final String getDefaultAction() {
    	return "OnlyAction";
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "loseAbilityStar", StayingPower.NONE, 1));
        return actionMap;
    }
}
