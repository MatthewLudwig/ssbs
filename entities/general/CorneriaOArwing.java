package entities.general;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.IrregularObstacle;
import entities.MovingPlatform;
import entities.Obstacle;
import entities.PhysicsInfo;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class CorneriaOArwing extends MovingPlatform {
	private float counter;
    
    public CorneriaOArwing() {
        super("Corneria O", new PhysicsInfo(0, 1, 1, 1), new Angle(0));
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        counter = 0;
    }

    @Override
    protected final String getDefaultAction() {
        return "OnlyAction";
    }
    
    @Override
    public void updateEntity() {
    	super.updateEntity();
    	counter += Utilities.lockedTPF;
    	if (counter > 1f){
    		counter = 0;
    		if (Math.random() < 0.3f){
    			CorneriaOArwingLaser laser = new CorneriaOArwingLaser();
    			laser.setPosition(getPosition().x, getPosition().y+22);
    			Main.getGameState().spawnEntity(laser);
    		}
    	}
    	this.display.setLocalTranslation(this.display.getLocalTranslation().x, this.display.getLocalTranslation().y, this.display.getLocalTranslation().z+1);
    	setPureXVelocity(-15);
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "arwing", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
