package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;
import physics.PhysicsModifier;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class SaffronCityODarkraiWave extends ViolentEntity {
	
	float timer;

	public SaffronCityODarkraiWave() {
		super("Saffron City O", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		this.isInvincible = true;
		this.team = Team.NONE;
		timer = 0;
		this.scale = 0.5f;
		this.alpha = 0.5f;
	}

	@Override
	public void setFinalSmash(boolean state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "darkraiEnergyWave",
				StayingPower.NONE, 1f).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_02.ogg", -1));
		actionMap.put("OnlyAction", a);
		return actionMap;
	}
	
	public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
    	if (entity instanceof PlayableCharacter){
    		((PlayableCharacter) entity).forceBreakShield(9, 1);
    	}
    }

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}
	
	public void updateEntity(){
		super.updateEntity();
		timer += Utilities.lockedTPF;
		if (timer > 1){
			setDead(true);
		}
	}

}
