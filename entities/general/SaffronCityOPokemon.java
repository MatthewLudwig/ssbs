package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Mario;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public abstract class SaffronCityOPokemon extends ViolentEntity {

	public float startX;
	private float timer;
	
	public SaffronCityOPokemon() {
		super("Saffron City O", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		this.isInvincible = true;
		this.team = Team.NONE;
		timer = 0;
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		timer += Utilities.lockedTPF;
		if (timer < 0.5f)
			this.setPureXVelocity(-30);
		else
			this.setPureXVelocity(0);
	}

}
