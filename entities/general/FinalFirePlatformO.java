package entities.general;

import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class FinalFirePlatformO extends ViolentEntity {

	public FinalFirePlatformO() {
		super("Final Destination O", 1, new PhysicsInfo(0, 1, 1, 1), 1f, 1f, 1f);
		// long time = System.nanoTime();
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		this.team = Team.NONE;
		this.isInvincible = true;

	}

	public void setFire() {
		setCurrentAction(actionMap.get("Fire"));
	}

	@Override
	protected final String getDefaultAction() {
		return "Normal";
	}

	@Override
	public void updateEntity() {
		super.updateEntity();
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("Normal", new CharacterAction(this, "platform2",
				StayingPower.BARELY, 1f));
		actionMap.put(
				"Fire",
				new CharacterAction(this, "platform", StayingPower.LOW, 1f)
						.addModifier(
								new ActionAttack(6.8f, 1.0f, 999f, true,
										new short[] { 0, 5, 10, 15 }))
						.addModifier(
								new ActionPlaySound(
										"/Common Sounds/blam_00.ogg", -1)));
		return actionMap;
	}

	@Override
	public void setFinalSmash(boolean state) {

	}
}
