package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class LondonOFerrisWheel extends ViolentEntity {
	private Angle angle;

	public LondonOFerrisWheel() {
		super("London O", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		this.isInvincible = true;
		this.isImmortal = true;
		this.team = Team.NONE;
		this.angle = new Angle((float) (Math.random() * 360 - 90));
		this.scale = 0.75f;

	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		this.setPureXVelocity(-20);
		this.setAttackCapability(6.8f, 2f, 999f, true);
		this.rotateUpTo(angle);
		angle.add(1);
		if (this.hasCollidedWithObstacle(false) || this.isOnGround()){
			this.setPureYVelocity(30);
			this.addToPosition(0, 1);
		}
		if (getPosition().x < 0){
			isImmortal = false;
		}
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("OnlyAction", new CharacterAction(this, "ferrisWheel",
				StayingPower.BARELY, 0f));
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
