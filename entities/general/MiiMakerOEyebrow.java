package entities.general;

import engine.Angle;
import engine.utility.Utilities;
import entities.IrregularObstacle;
import entities.MovingPlatform;
import entities.Obstacle;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

import physics.CollisionInfo;
import physics.PhysicsModifier;

public class MiiMakerOEyebrow extends ViolentEntity {
	private Angle angle;
	private Vector2f centerPosition;
	private float defaultX;
	private float modifiedX;
	private float modifiedY;
	private float size;
	private float sizeY;

	public MiiMakerOEyebrow() {
		super("Mii Maker O", 1, new PhysicsInfo(0, 1, 1, 1), 1, 1, 1);
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.angle = new Angle(0);
		size = 1;
		sizeY = 1;
		defaultX = 40;
		modifiedX = 0;
		modifiedY = 0;
		this.isInvincible = true;
		this.team = Team.NONE;
	}

	public void onDeath() {
	}

	@Override
	protected final String getDefaultAction() {
		return "OnlyAction";
	}

	@Override
	public void updateEntity() {
		super.updateEntity();
		if (centerPosition == null) {
			centerPosition = new Vector2f(this.getPosition().x, this.getPosition().y);
		}
		int n = Utilities.getGeneralUtility().getBooleanAsSign(facing);
		Vector2f vec = centerPosition.add(new Vector2f(defaultX * n, 0)).add(
				new Vector2f(modifiedX * n, modifiedY));
		this.setPosition(vec.x, vec.y);
		this.rotateUpTo(angle);
		this.setScale(size, size * sizeY);
		//this.scale = size;
		//this.xScale = 1/sizeY;
		this.display.setLocalTranslation(this.display.getLocalTranslation().x, this.display.getLocalTranslation().y, this.display.getLocalTranslation().z+1);
		this.setAttackCapability(6.8f, 1.0f, 999f, false);
	}
	
	/*public void onCollideWith(PhysicsEntity entity, CollisionInfo info){
		if (!(entity instanceof MiiMakerOEyebrow))
			super.onCollideWith(entity, info);
	}*/

	public void setXDisp(float percent) {
		modifiedX = -60 + 150 * percent;
	}

	public void setYDisp(float percent) {
		modifiedY = -90 + 180 * percent;
	}

	public void setAngle(float percent) {
		/*if (facing)
			angle = new Angle(-30 + 60 * percent);
		else
			angle = new Angle(30 - 60 * percent);*/
		angle = new Angle(-30 + 60 * percent);
	}
	
	public void setScaling(float percent){
		size = 0.33f + (3.34f)*percent;
	}
	
	public void setYScaling(float percent){
		sizeY = 0.33f + (3.34f)*percent;
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("OnlyAction", new CharacterAction(this, "eyebrow",
				StayingPower.BARELY, 1f));
		return actionMap;
	}

	@Override
	public void setFinalSmash(boolean state) {
		// TODO Auto-generated method stub
		
	}
}
