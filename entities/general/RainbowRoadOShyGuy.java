package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.BoundingBox;
import physics.CollisionInfo;
import physics.PhysicsModifier;
import states.game.stages.Stage;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Mario;
import entities.characters.KingDedede.JumpingAction;
import entities.characters.Samus;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionCompleteNumberOfTicks;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class RainbowRoadOShyGuy extends ViolentEntity {

	private float speed;

	public RainbowRoadOShyGuy() {
		super("Rainbow Road O", 1, new PhysicsInfo(1, 1, 1, 1), 1, 1, 1);
		this.setHasSuperArmor(true);
		this.team = Team.NONE;
		this.facing = true;
		this.speed = (float) (Math.random()*40+20);
		this.scale = 0.5f;
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		if (this.hasCollidedWithObstacle(false)){
			this.setPureYVelocity(50);
			this.addToPosition(0,1);
		}
		if (this.hasCollidedWithObstacle(true)){
			this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		} else {
			this.removePhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		}
		this.setPureXVelocity(speed);
		this.setAttackCapability(10f, 1.2f, 999f, false);
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "shyGuy", StayingPower.NONE,
				0.33f).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_00.ogg", -1));
		actionMap.put("OnlyAction", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
