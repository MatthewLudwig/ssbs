package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;
import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.CharacterStats;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionSetFacing;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionWaitTillControl;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.CharacterActionFactory;
import entities.characters.actions.StayingPower;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.projectiles.DrWilyDownwardBlast;
import entities.projectiles.DrWilyForwardBlast;
import entities.projectiles.LinkBoomerang;
import entities.projectiles.MegaManPellet;
import entities.projectiles.Projectile;
import entities.projectiles.ProtoManBoomerang;
import entities.projectiles.ProtoManPellet;
import entities.projectiles.ProtoManSpark;

public class DrWily extends ViolentEntity {
	public static final float[] STAT = CharacterStats.MegaManStats;
	public static final float[] DMG = CharacterStats.MegaManDamage;
	public static final float[] KNBK = CharacterStats.MegaManKnockback;
	public static final float[] PRTY = CharacterStats.MegaManPriority;
	public static final float[] ISPD = CharacterStats.MegaManImageSpeed;
	public static final short[][] SECT = CharacterStats.MegaManSectionFrames;
	boolean init;

	public DrWily() {
		super("Dr Wily", STAT[7] / 2f, new PhysicsInfo(STAT[0], STAT[1],
				STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6]);
		this.team = Team.NONE;
		this.scale = 1.27f;
		init = false;
		setHasSuperArmor(true);
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		if (!init) {
			init = true;
			setCurrentAction(actionMap.get("Enter"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Stand"));
		}
		if (random() && getPosition().y > 375) {
			setCurrentAction(actionMap.get("Move Down"));
		}
		if (random() && getPosition().x < 675) {
			setCurrentAction(actionMap.get("Move Right"));
		}
		if (random() && getPosition().x > 325) {
			setCurrentAction(actionMap.get("Move Left"));
		}
		if (random() && getPosition().y < 550) {
			setCurrentAction(actionMap.get("Move Up"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Neutral A"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Side A"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Up A"));
		}
	}

	public boolean random() {
		return Math.random() < Utilities.lockedTPF / 2f;
	}

	public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
		super.onCollideWith(entity, info);
		if (getDamage() > 75) {
			this.setDead(true);
		}
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("Stand", new CharacterAction(this, "moveAround",
				StayingPower.NONE, 1));
		actionMap.put("Move Down", getMoveDown(this));
		actionMap.put("Move Right", getWalk(this, true));
		actionMap.put("Move Left", getWalk(this, false));
		actionMap.put("Move Up", getMoveUp(this));
		actionMap.put("Neutral A", this.getNeutralA());
		actionMap.put("Side A", this.getSideA());
		actionMap.put("Up A", this.getUpA());
		actionMap.put("Enter", new CharacterAction(this, "flyIn",
				StayingPower.MAX, 2));
		return actionMap;
	}

	public static CharacterAction getMoveUp(PhysicsEntity entity) {
		CharacterAction action = getAnimation(entity, "moveAround",
				StayingPower.BARELY, 1f);
		action.addModifier(new ActionMovement(2f, 90, 0f, false));
		return action;
	}

	public static CharacterAction getMoveDown(PhysicsEntity entity) {
		CharacterAction action = getAnimation(entity, "moveAround",
				StayingPower.BARELY, 1f);
		action.addModifier(new ActionMovement(10f, -90f, 0f, false));
		return action;
	}

	public static CharacterAction getWalk(PhysicsEntity entity,
			boolean direction) {
		CharacterAction action = getAnimation(entity, "moveAround",
				StayingPower.BARELY, 1f);

		action.addModifier(new ActionSetFacing(direction));
		action.addModifier(new ActionMovement(10f, 0f, 0f, true));

		return action;
	}

	public static CharacterAction getAnimation(PhysicsEntity entity,
			String animationName, StayingPower stayingPower, float updateLimit) {
		CharacterAction action = new CharacterAction(entity, animationName,
				stayingPower, updateLimit);

		return action;
	}

	protected CharacterAction getNeutralA() {
		CharacterAction action = new CharacterAction(this, "shoot",
				StayingPower.EXTREME, ISPD[0]);
		action.addModifier(new ActionSpawnEntity(new short[] { 0 }, 99,
				DrWilyDownwardBlast.class, this).setXOffset(30, true));
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Mega Man/upA.ogg", 0));
		return action;
	}

	protected CharacterAction getSideA() {
		CharacterAction action = new CharacterAction(this, "shoot",
				StayingPower.EXTREME, ISPD[0]);
		action.addModifier(new ActionSpawnEntity(new short[] { 0 }, 99,
				DrWilyForwardBlast.class, this));
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Mega Man/neutralB.ogg", 0));
		return action;
	}

	protected CharacterAction getUpA() {
		CharacterAction action = new CharacterAction(this, "shoot",
				StayingPower.EXTREME, ISPD[0]);
		action.addModifier(new ActionSpawnEntity(new short[] { 0 }, 2,
				DrWilyWalkerBot.class));
		action.addModifier(new ActionPlaySound(
				"/Character Sounds/Mega Man/sideA.ogg", 0));
		return action;
	}

	@Override
	protected String getDefaultAction() {
		return "Stand";
	}

}
