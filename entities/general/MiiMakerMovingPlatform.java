package entities.general;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.IrregularObstacle;
import entities.MovingPlatform;
import entities.Obstacle;
import entities.PhysicsInfo;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class MiiMakerMovingPlatform extends MovingPlatform {
    private boolean direction;
    
    public MiiMakerMovingPlatform() {
        super("Mii Maker", new PhysicsInfo(0, 1, 1, 1), new Angle(0));
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.direction = false;
        if (Math.random() < 0.5)
        	this.direction = true;
    }

    @Override
    protected final String getDefaultAction() {
        return "OnlyAction";
    }
    
    @Override
    public void updateEntity() {
    	super.updateEntity();
    	if (this.getPosition().x > 1820)
    		this.direction = false;
    	if (this.getPosition().x < 320)
    		this.direction = true;
    	
    	setPureXVelocity(17*Utilities.getGeneralUtility().getBooleanAsSign(this.direction));
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "movingPlatform", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
