package entities.general;

import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class MarioCoin extends PhysicsEntity {

    public MarioCoin() {
        super("Mario_DEFAULT", 1, new PhysicsInfo(0, 1, 1, 1));
        this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
    }
    
    public void onDeath() {
    }

    @Override
    protected final String getDefaultAction() {
        return "OnlyAction";
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "coin", StayingPower.BARELY, 1f).addModifier(new ActionKillEntity()));
        return actionMap;
    }
}
