package entities.general;

import java.util.HashMap;
import java.util.Map;

import engine.utility.Utilities;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class SaffronCityOHaxorus extends SaffronCityOPokemon {
	
	private float timer;
	
	public SaffronCityOHaxorus(){
		super();
		this.scale = 0.64f*0.8f;
		timer = 0;
	}
	
	public void updateEntity() {
		super.updateEntity();
		this.setAttackCapability(22f, 1.4f, 10f, false);
		timer += Utilities.lockedTPF;
		if (timer > 0.75f){
			setPureXVelocity(30);
		}
		if (getPosition().x > startX){
			setDead(true);
		}
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "haxorus",
				StayingPower.NONE, 0f).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_01.ogg", -1));
		actionMap.put("OnlyAction", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
