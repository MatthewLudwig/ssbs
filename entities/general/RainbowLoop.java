package entities.general;

import engine.Angle;
import engine.utility.Utilities;
import entities.IrregularObstacle;
import entities.Obstacle;
import entities.PhysicsInfo;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class RainbowLoop extends IrregularObstacle {
    
    private Angle angle;
    
    public RainbowLoop() {
        super("Rainbow Road", new PhysicsInfo(0, 1, 1, 1), new Angle(0));
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.angle = new Angle(0);
    }
    
    public void onDeath() {
    }

    @Override
    protected final String getDefaultAction() {
        return "OnlyAction";
    }
    
    @Override
    public void updateEntity() {
    	super.updateEntity();
    	this.angle.add(4 * Utilities.lockedTPF);
    	//this.angle = new Angle(90);
    	this.setRotAngle(this.angle);
        this.rotateUpTo(this.angle);
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "rainbowRoadPlatform", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
