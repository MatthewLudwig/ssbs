package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Mario;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class AcornPlainsOHammer extends ViolentEntity {
	private boolean init;
	
	public AcornPlainsOHammer() {
		super("Acorn Plains O", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		setHasSuperArmor(true);
		this.team = Team.NONE;
		this.facing = true;
		init = false;
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		if (!init){
			this.setPureYVelocity(30);
			init = true;
		}
		this.setPureXVelocity(-7);
		this.setAttackCapability(8.6f, 1.2f, 10f, false);
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "hammer",
				StayingPower.NONE, 0.67f).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_00.ogg", -1));
		actionMap.put("Thrown", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "Thrown";
	}

}
