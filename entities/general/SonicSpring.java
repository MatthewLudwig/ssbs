package entities.general;

import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;

public class SonicSpring extends PhysicsEntity {

    private float timer;

    public SonicSpring() {
        super("Sonic_DEFAULT", 1, new PhysicsInfo(0, 1, 1, 1));
        this.timer = 0;
    }

    public void onDeath() {
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if(this.timer >= 1)
        {
            this.setDead(true);
        }
        else
        {
            this.timer += Utilities.lockedTPF;
        }
    }
    
    @Override
    protected final String getDefaultAction() {
        return "OnlyAction";
    }

    @Override
    public void onCollideWith(PhysicsEntity object, CollisionInfo info) {
        super.onCollideWith(object, info);
        
        object.setOnGround(false);
        object.addToPosition(0, 1);
        
        if (object instanceof PlayableCharacter) {
            ((PlayableCharacter)object).setYVelocity(25f, true, true);
        } else {
            object.setYVelocity(25f, true);
        }
        
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "spring", StayingPower.NONE, .50f).addModifier(new CustomAction()));
        return actionMap;
    }
    
    public class CustomAction extends ActionModifier {
        public float counter;
        
        public CustomAction() {
            
        }

        @Override
        public void onInit() {
            this.counter = 0;
        }

        @Override
        public boolean onUpdate() {
            if (((PhysicsEntity) this.action.entity).hasCollided()) {
                this.counter = .5f;
            }
            
            if (this.counter == 0) {
                this.action.setCurrentFrame(0);
            } else {
                this.action.setCurrentFrame(1);
                this.counter = Math.max(this.counter - Utilities.lockedTPF, 0);
            }
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
}
