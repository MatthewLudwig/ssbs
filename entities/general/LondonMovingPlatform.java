package entities.general;

import engine.Angle;
import entities.MovingPlatform;
import entities.PhysicsInfo;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

import physics.PhysicsModifier;

public class LondonMovingPlatform extends MovingPlatform {
    private boolean direction;
    private Vector2f pos1, pos2, pos3;
    
    public LondonMovingPlatform() {
        super("London", new PhysicsInfo(0, 1, 1, 1), new Angle(0));
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.direction = false;
        if (Math.random() < 0.5)
        	this.direction = true;
        pos1 = new Vector2f(328, 344);
        pos2 = new Vector2f(783, 344);
        pos3 = pos1.add(pos2).mult(0.5f);
    }
    
    public Vector2f getStart(){
    	return pos3;
    }

    @Override
    protected final String getDefaultAction() {
        return "OnlyAction";
    }
    
    public Angle getAngleFromPos(){
    	float dist = pos1.distance(pos2);
    	float percent = getPosition().distance(pos1)/dist;
    	return new Angle(percent*178+1);
    }
    
    public float getSpeedScale(){
    	return 23f;
    }
    
    @Override
    public void updateEntity() {
    	super.updateEntity();
    	if (getPosition().x < pos1.x){
    		this.direction = true;
    	}
    	if (getPosition().x > pos2.x){
    		this.direction = false;
    	}
    	
    	if (this.direction){
    		setPureVelocity(pos2.subtract(pos1).mult(getAngleFromPos().sin()/getSpeedScale()));
    	} else {
    		setPureVelocity(pos1.subtract(pos2).mult(getAngleFromPos().sin()/getSpeedScale()));
    	}
    	
    	//setPureXVelocity(17*Utilities.getGeneralUtility().getBooleanAsSign(this.direction));
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "movingPlatform", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
