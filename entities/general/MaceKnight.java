package entities.general;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Mario;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class MaceKnight extends ViolentEntity {

	float speed;

	public MaceKnight() {
		super("Castle Dedede O", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		this.isInvincible = true;
		this.team = Team.NONE;
		this.speed = (float) (Math.random() * Mario.STAT[3] * 0.58 + 0.75) * 9f * 0.8f;
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		this.setPureXVelocity(speed);
		if (this.isOnGround()
				&& (Math.random() < Utilities.lockedTPF * 0.8f / 1f || (getPosition().x > 390 && getPosition().x < 440))) {
			this.setPureYVelocity(30 * 0.8f);
			this.setCurrentAction(actionMap.get("Jump"));
		}
		this.setAttackCapability(6.8f * 1.2f, 1f * 1.2f, 10f * 1.2f, false);
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "maceKnightRun",
				StayingPower.NONE, 0.5f).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_00.ogg", -1));
		actionMap.put("Run", a);
		a = new CharacterAction(this, "maceKnightJump", StayingPower.BARELY,
				0.5f).addModifier(new ActionWaitTillOnGround()).addModifier(
				new ActionPlaySound("/Common Sounds/hit_00.ogg", -1));
		actionMap.put("Jump", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "Run";
	}

}
