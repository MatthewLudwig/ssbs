package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;
import engine.Angle;
import engine.Main;
import entities.MovingPlatform;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class SaffronCityOBuildingLeft extends MovingPlatform {
	private float weight;
	public float yStart;

	public SaffronCityOBuildingLeft() {
		super("Saffron City O", new PhysicsInfo(0, 1, 1, 1), new Angle(0));
		weight = 0;
		yStart = 0;
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.isImmortal = true;
	}

	public void updateEntity() {
		super.updateEntity();
		weight = 0;
		for (PhysicsEntity physics : Main.getGameState().getWorld()
				.getPhysicsList()) {
			if (physics.getPosition().x < 303
					&& (physics.isOnGround()
							|| physics.hasCollidedWithObstacle(false) || (physics
							.getPosition().subtract(
									getRealBounds().getUpperMidpoint()).y < Math
							.abs(getVelocity().y) * 2.5f && physics
							.getPosition().subtract(
									getRealBounds().getUpperMidpoint()).x < getRealBounds()
							.getDimensions().x))) {
				weight += physics.getPhysicsInfo().getWeight();
			}
		}
		setPureYVelocity((weight - 0.53f) * -10);
		if (getVelocity().y > 0 && getPosition().y >= yStart) {
			setPosition(getPosition().x, yStart);
			setPureYVelocity(0);
		}
	}

	protected final String getDefaultAction() {
		return "OnlyAction";
	}

	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("OnlyAction", new CharacterAction(this, "leftBuilding",
				StayingPower.BARELY, 1f));
		return actionMap;
	}
}
