package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class LondonODonPaolo extends ViolentEntity {
	private Angle angle;
	private boolean direction;

	public LondonODonPaolo() {
		super("London O", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.isInvincible = true;
		this.isImmortal = true;
		this.team = Team.NONE;
		this.angle = new Angle(0);
		this.scale = 0.5f;
        this.direction = false;
        if (Math.random() < 0.5)
        	this.direction = true;
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}
	
	public Angle getAngleFromAngle(){
    	float ang = angle.getValue();
    	float percent = (ang+25f)/50f;
    	return new Angle(percent*178+1);
    }

	public void updateEntity() {
		super.updateEntity();
		if (getPosition().x < 350){
			this.setPureXVelocity(4);
		} else {
			this.setPureYVelocity(6);
			this.setPureXVelocity(0);
			isImmortal = false;
		}
		this.setAttackCapability(13.6f, 1f, 999f, true);
		this.rotateUpTo(angle);
		if (angle.getValue() > 25){
			this.direction = false;
		}
		if (angle.getValue() < -25){
			this.direction = true;
		}
		
		if (this.direction){
			angle.add(3*getAngleFromAngle().sin());
		} else {
			angle.add(-3*getAngleFromAngle().sin());
		}
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("OnlyAction", new CharacterAction(this, "donPaolo",
				StayingPower.BARELY, 0.5f));
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
