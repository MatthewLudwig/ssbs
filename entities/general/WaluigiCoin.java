package entities.general;

import engine.Angle;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.characters.Waluigi;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;
import physics.PhysicsModifier;

public class WaluigiCoin extends PhysicsEntity {

	private Waluigi obj;
	private float speed;
	private Angle ang;
	
    public WaluigiCoin(Waluigi object) {
        super("Waluigi_DEFAULT", 1, new PhysicsInfo(0, 1, 1, 1));
        this.obj = object;
        this.speed = (float) (Math.random()*6+7);
        this.ang = new Angle((float) (Math.random()*360));
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
    }
    
    public void updateEntity(){
    	super.updateEntity();
    	
    	Angle angle = Utilities.getPhysicsUtility().calculateCorrectAngle(obj.getAppropriateBounds().getExactCenter(), this.getAppropriateBounds().getLowerMidpoint());

    	float f = angle.getValue() - ang.getValue();
        while (f > 180)
        	f -= 360;
        while (f < -180)
        	f += 360;
        
        f /= 32f;
        
        ang = new Angle(ang.getValue() + f);

        this.setPureXVelocity(speed * ang.cos());
        this.setPureYVelocity(speed * ang.sin());
    }
    
    public void onCollideWith(PhysicsEntity object, CollisionInfo info){
    	if (object == obj){
    		obj.healEntity(Waluigi.DMG[7]/20f);
    		this.setDead();
    	}
    }
    
    public void onDeath() {
    }

    @Override
    protected final String getDefaultAction() {
        return "OnlyAction";
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "coin", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
