package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Mario;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class AcornPlainsOHammerBro extends ViolentEntity {
	
	public AcornPlainsOHammerBro() {
		super("Acorn Plains O", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		this.isInvincible = true;
		this.team = Team.NONE;
		this.facing = true;
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		this.setAttackCapability(3f, 0.6f, 10f, false);
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "hammerBro",
				StayingPower.NONE, 0.33f).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_00.ogg", -1));
		a.addModifier(new ActionSpawnEntity(new short[]{1}, 999, AcornPlainsOHammer.class));
		actionMap.put("Stand", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "Stand";
	}

}
