package entities.general;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Matrix3f;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;

import physics.PhysicsModifier;

import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class SkullKid extends PhysicsEntity {
	private int effect;
	private float counter;
	private SkullKidEffect anim;

	// private boolean init;

	public SkullKid() {
		super("Clock Town O", 1, new PhysicsInfo(0, 0, 0, 1));
		this.scale = 0.75f;
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		effect = -1;
		counter = 0;
		// init = false;
	}

	public void updateEntity() {
		super.updateEntity();
		counter += Utilities.lockedTPF / 10f;
		// System.out.println(actionMap.get("Spawn").equals(actionMap.get("Despawn")));
		if (this.currentAction.equals(actionMap.get("Spawn"))
				&& counter > 0.25f) {
			this.setCurrentAction(actionMap.get("Effect"));
			counter = 0;
		}
		if (this.currentAction.equals(actionMap.get("Effect"))) {
			if (effect == -1) {
				double rand = Math.random();
				if (rand < 1.0 / 5.0) {
					effect = 0;
				} else if (rand < 2.0 / 5.0) {
					effect = 1;
				} else if (rand < 3.0 / 5.0) {
					effect = 2;
				} else if (rand < 4.0 / 5.0) {
					effect = 3;
				} else {
					effect = 4;
				}
				// effect = 3;
				anim = new SkullKidEffect();
				Main.getGameState().spawnEntity(anim);

				if (effect == 3) {
					Main.getGameState().getCamera().getScreenNode()
							.rotate(0, 0, 3.1415926f);
				}
			}
			if (effect == 0 || effect == 2) {
				Main.getGameState()
						.getCamera()
						.getScreenNode()
						.setLocalScale(
								(float) (Main.getGameState().getCamera()
										.getScreenNode().getLocalScale().x
										* (Math.abs(Math.cos(counter)) + 1) / 2f),
								Main.getGameState().getCamera().getScreenNode()
										.getLocalScale().y,
								Main.getGameState().getCamera().getScreenNode()
										.getLocalScale().z);
				Main.getGameState()
						.getCamera()
						.getScreenNode()
						.setLocalTranslation(
								(float) (Main.getGameState().getCamera()
										.getScreenNode().getLocalTranslation().x
										* (Math.abs(Math.cos(counter)) + 1)
										/ 2f + 320 - 320 * (Math.abs(Math
										.cos(counter)) + 1) / 2f),
								Main.getGameState().getCamera().getScreenNode()
										.getLocalTranslation().y,
								Main.getGameState().getCamera().getScreenNode()
										.getLocalTranslation().z);
			}
			if (effect == 1 || effect == 2) {
				Main.getGameState()
						.getCamera()
						.getScreenNode()
						.setLocalScale(
								Main.getGameState().getCamera().getScreenNode()
										.getLocalScale().x,
								(float) (Main.getGameState().getCamera()
										.getScreenNode().getLocalScale().y
										* (2 + Math.sin(counter)) / 2f),
								Main.getGameState().getCamera().getScreenNode()
										.getLocalScale().z);
				Main.getGameState()
						.getCamera()
						.getScreenNode()
						.setLocalTranslation(
								Main.getGameState().getCamera().getScreenNode()
										.getLocalTranslation().x,
								(float) (Main.getGameState().getCamera()
										.getScreenNode().getLocalTranslation().y
										* (2 + Math.sin(counter)) / 2f + 240 - 240 * (2 + Math
										.sin(counter)) / 2f),
								Main.getGameState().getCamera().getScreenNode()
										.getLocalTranslation().z);
			}
			if (effect == 3) {
				Vector3f pos = Main.getGameState().getCamera().getScreenNode()
						.getLocalTranslation();
				Vector3f scale = Main.getGameState().getCamera()
						.getScreenNode().getLocalScale();
				Vector2f size = new Vector2f(Main.getGameState().getWorld()
						.getStage().getSizeX()
						* scale.x, Main.getGameState().getWorld().getStage()
						.getSizeY()
						* scale.y);
				Main.getGameState()
						.getCamera()
						.setTargetPosition(
								Main.getGameState().getWorld().getStage()
										.getSizeX()
										- Main.getGameState().getCamera()
												.getTargetPosition().x,
								Main.getGameState().getWorld().getStage()
										.getSizeY()
										- Main.getGameState().getCamera()
												.getTargetPosition().y);
				Main.getGameState().getCamera().updateCameraState(true);
				Main.getGameState()
						.getCamera()
						.getScreenNode()
						.setLocalTranslation(pos.x + size.x, pos.y + size.y,
								pos.z);
			}
			if (effect == 4) {
				// Utilities.lockedTPF *= (1 + Math.sin(counter));
				Utilities.lockedTPF *= 0.2;
			}

			if (counter > 2 * 3.14159f) {
				Main.getGameState().getCamera().getScreenNode()
						.setLocalRotation(new Matrix3f());
				this.setCurrentAction(actionMap.get("Despawn"));
				counter = 0;
				anim.setDead(true);
			}
		}
		if (this.currentAction.equals(actionMap.get("Despawn"))
				&& counter > 0.2f) {
			this.setDead(true);
		}
	}

	@Override
	public void onDeath() {
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("Spawn", new CharacterAction(this, "skullKid",
				StayingPower.BARELY, 1f).addModifier(new NoFinishAction()));
		actionMap.put("Effect", new CharacterAction(this, "skullKid2",
				StayingPower.LOW, 1f).addModifier(new NoFinishAction()));
		actionMap.put("Despawn", new CharacterAction(this, "skullKid",
				StayingPower.MEDIUM, 1f).addModifier(new NoFinishAction())
				.addModifier(new ActionMovement(0, 0, 0, false)));
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "Spawn";
	}

	public class NoFinishAction extends ActionModifier {
		public NoFinishAction() {

		}

		@Override
		protected void onInit() {
		}

		@Override
		protected boolean onUpdate() {
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return false;
		}

		@Override
		protected void cleanUp() {

		}
	}

}
