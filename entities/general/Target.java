package entities.general;

import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.ViolentEntity;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.CollisionInfo;
import physics.PhysicsModifier;

public class Target extends PhysicsEntity {
    private boolean dying;
    
    public Target() {
        super("Target Test", 1, new PhysicsInfo(0, 1, 1, 1));
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.dying = false;
    }

    @Override
    public void updateEntity() {        
        super.updateEntity();
    }
    
    public boolean isDying() {
        return this.dying;
    }
    
    public void onDeath() {
    }

    @Override
    protected final String getDefaultAction() {
        return "OnlyAction";
    }

    @Override
    public void onCollideWith(PhysicsEntity object, CollisionInfo info) {
        super.onCollideWith(object, info);
        
        if (object instanceof ViolentEntity && ((ViolentEntity) object).getDamageDealt() != 0) {
            this.dying = true;
        }
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "target", StayingPower.BARELY, 1f).addModifier(new ActionPlaySound("/Common Sounds/hit_01.ogg", 3)).addModifier(new CustomAction()));
        return actionMap;
    }
    
    public class CustomAction extends ActionModifier {
        public float counter;
        
        public CustomAction() {
            
        }

        @Override
        public void onInit() {
            this.counter = 0;
        }

        @Override
        public boolean onUpdate() {
            if (!((Target) this.action.entity).dying) {
                this.action.setCurrentFrame(0);
            } else if (this.action.getCurrentFrameNumber() == 8) {
                this.action.entity.setDead(true);
            }
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
}
