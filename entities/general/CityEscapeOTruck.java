package entities.general;

import engine.Angle;
import engine.utility.Utilities;
import entities.IrregularObstacle;
import entities.Obstacle;
import entities.PhysicsInfo;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class CityEscapeOTruck extends IrregularObstacle {
    
    private float scaleSpeed;
    private float counter;
    
    public CityEscapeOTruck() {
        super("City Escape O", new PhysicsInfo(0, 1, 1, 1), new Angle(0));
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.scaleSpeed = (float) (Math.random()*0.025 - 0.0125);
        this.counter = 0;
    }
    
    public void onDeath() {
    }

    @Override
    protected final String getDefaultAction() {
        return "OnlyAction";
    }
    
    @Override
    public void updateEntity() {
    	super.updateEntity();
    	this.scale += this.scaleSpeed;
    	if (this.scale < 0.85f){
    		this.scale = 0.85f;
    		this.scaleSpeed *= -1;
    	}
    	if (this.scale > 1.2f){
    		this.scale = 1.2f;
    		this.scaleSpeed *= -1;
    	}
    	this.setScale(this.scale);
    	counter += Utilities.lockedTPF;
    	if (counter > 2f){
    		counter = 0;
    		this.scaleSpeed = (float) (Math.random()*0.025 - 0.0125);
    	}
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "truck", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
