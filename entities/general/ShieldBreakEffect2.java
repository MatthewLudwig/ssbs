package entities.general;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

import physics.PhysicsModifier;

import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.CharacterActionFactory;
import entities.characters.actions.StayingPower;

public class ShieldBreakEffect2 extends PhysicsEntity {
	PlayableCharacter player;
	
	public ShieldBreakEffect2(PlayableCharacter p) {
		super("Status Effect", 1, new PhysicsInfo(0,0,0,1));
		player = p;
		this.scale = player.getRealBounds().getDimensions().x/25f;
		//this.scale = player.getScale();
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
	}
	
	public void updateEntity(){
		super.updateEntity();
		if (!player.getCurrentAction().equals(CharacterActionFactory.getShieldBreak(player, (short) 1))){
			setDead();
		}
		this.display.setLocalTranslation(this.display.getLocalTranslation().x, this.display.getLocalTranslation().y, this.display.getLocalTranslation().z);
	}

	@Override
	public void onDeath() {
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "shieldStun2", StayingPower.BARELY, 1f));
        return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
