package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Mario;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class AcornPlainsOBlooper extends ViolentEntity {
	
	private float counter;
	public AcornPlainsOBlooper() {
		super("Acorn Plains O", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		this.isInvincible = true;
		this.team = Team.NONE;
		this.facing = true;
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.counter = (float) (Math.random() + 0.5);
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		counter -= Utilities.lockedTPF;
		if (counter < 0 && this.getVelocity().y == 10){
			counter = (float) (Math.random() + 0.5);
			this.setPureYVelocity(5);
		} else if (counter < 0){
			counter = (float) (Math.random() + 0.5);
			this.setPureYVelocity(10);
		}
		this.setAttackCapability(6f, 0.9f, 10f, false);
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "blooper",
				StayingPower.NONE, 0.33f).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_00.ogg", -1));
		actionMap.put("Stand", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "Stand";
	}

}
