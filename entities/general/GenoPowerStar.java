package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.scene.Node;

import engine.Angle;
import engine.PictureLayer;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class GenoPowerStar extends PhysicsEntity {
    private static final Angle angle = new Angle(270);
    
    private float lifeTimer;
    private int frame;
    
    public GenoPowerStar(int frame) {
        super("Geno_DEFAULT", 0, new PhysicsInfo(0, 1, 1, 1));
        this.lifeTimer = 0.5f;
        this.frame = frame;
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.scale = .5f;
    }
    
    @Override
    public void setUpEntity(int id, Node displayNode, Node menuNode, boolean alwaysDrawn) {
        super.setUpEntity(id, displayNode, menuNode, alwaysDrawn);
        this.setPureXVelocity(angle.cos() * 15);
        this.setPureYVelocity(angle.sin() * 15);
    }
    
    public void onDeath() {}
    
    @Override
    public void updateEntity() {
        super.updateEntity();
        
        if (this.lifeTimer <= 0) {
            this.setDead(true);
        } else {
            this.lifeTimer -= Utilities.lockedTPF;
        }
        
        this.display.setLocalTranslation(this.display.getLocalTranslation().x, this.display.getLocalTranslation().y, this.display.getLocalTranslation().z+1);
    }
    
    @Override
    protected final String getDefaultAction() {
    	return "OnlyAction";
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        CharacterAction action = new CharacterAction(this, "star", StayingPower.NONE, 1);
        action.addModifier(new OnlyAction());
        actionMap.put("OnlyAction", action);
        return actionMap;
    }
    
    public class OnlyAction extends ActionModifier {
    	
        public OnlyAction() {
        
        }

        @Override
        protected void onInit() {
        	
        }

        @Override
        protected boolean onUpdate() {
        	this.action.setCurrentFrame(frame);
        	return false;
        }

        @Override
        protected boolean shouldFinish() {
            return true;
        }

        @Override
        protected void cleanUp() {
        }
    }
}
