package entities.general;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

import physics.BoundingBox;
import physics.PhysicsModifier;
import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.MovingPlatform;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class SaffronCityODoor extends PhysicsEntity {

	private boolean isOpen;
	private SaffronCityOPokemon mon;
	float timer;

	public SaffronCityODoor() {
		super("Saffron City O", 1, new PhysicsInfo(0, 1, 1, 1));
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
		timer = 0;
		isOpen = false;
	}

	public void updateEntity() {
		super.updateEntity();
		timer += Utilities.lockedTPF;
		this.display.setLocalTranslation(this.display.getLocalTranslation().x, this.display.getLocalTranslation().y, this.display.getLocalTranslation().z+1);
		if ((mon == null || mon.isDead()) && isOpen) {
			if (timer > 12){
				isOpen = false;
				timer = 0;
			}else if (timer > 10) {
				spawnPokemon();
			} else {
				for (PlayableCharacter pc : Main.getGameState().getWorld()
						.getPlayerList()) {
					if (pc.getPosition().x < getPosition().x-64 && pc.getPosition().distance(getPosition()) < 100) {
						spawnPokemon();
						break;
					}
				}
			}
		}
	}

	protected final String getDefaultAction() {
		return "OnlyAction";
	}

	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("OnlyAction", new CharacterAction(this, "door",
				StayingPower.BARELY, 1f).addModifier(new SetFrameAction()));
		return actionMap;
	}

	@Override
	public void onDeath() {
		// TODO Auto-generated method stub

	}

	public void openDoor() {
		isOpen = true;
		timer = 0;
	}

	public boolean isDoorOpen() {
		return isOpen;
	}

	public void spawnPokemon() {
		double rand = Math.random();
		if (rand < 0.17)
			mon = new SaffronCityOScizor();
		else if (rand < 0.33)
			mon = new SaffronCityOBlaziken();
		else if (rand < 0.5)
			mon = new SaffronCityOHaxorus();
		else if (rand < 0.67)
			mon = new SaffronCityOSylveon();
		else if (rand < 0.83)
			mon = new SaffronCityODarkrai();
		else
			mon = new SaffronCityOElectrode();
		mon.setPosition(getPosition().x-25, getPosition().y);
		mon.startX = getPosition().x;
		Main.getGameState().spawnEntity(mon);
		timer = 21;
	}

	public class SetFrameAction extends ActionModifier {

		public SetFrameAction() {
		}

		@Override
		protected void onInit() {

		}

		@Override
		protected boolean onUpdate() {
			if (isOpen)
				this.action.setCurrentFrame(1);
			else
				this.action.setCurrentFrame(0);
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return false;
		}

		@Override
		protected void cleanUp() {
			// ((PlayableCharacter) this.action.entity).setFinalSmash(true);
		}
	}

}
