package entities.general;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Mario;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class AcornPlainsOSpiny extends ViolentEntity {
	private boolean init;
	private boolean landed;
	
	public AcornPlainsOSpiny() {
		super("Acorn Plains O", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		setHasSuperArmor(true);
		this.team = Team.NONE;
		this.facing = true;
		init = false;
		landed = false;
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		if (!init){
			this.setPureYVelocity(20);
			init = true;
		}
		if (this.isOnGround() || landed) {
			landed = true;
			this.setPureXVelocity(-8
					* Utilities.getGeneralUtility().getBooleanAsSign(facing));
			this.setCurrentAction(actionMap.get("Walk"));
			if (this.hasCollidedWithObstacle(true)){
				facing = !facing;
				this.addToPosition(-8 * Utilities.getGeneralUtility().getBooleanAsSign(facing), 0);
			}
		}
		this.setAttackCapability(10.4f, 1.5f, 10f, false);
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "spinyEgg",
				StayingPower.NONE, 0.33f).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_00.ogg", -1));
		actionMap.put("Fall", a);
		a = new CharacterAction(this, "spiny", StayingPower.BARELY, 0.33f)
				.addModifier(new ActionPlaySound("/Common Sounds/hit_00.ogg",
						-1));
		actionMap.put("Walk", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "Fall";
	}

}
