package entities.general;

import engine.Angle;
import engine.utility.Utilities;
import entities.IrregularObstacle;
import entities.Obstacle;
import entities.PhysicsInfo;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class RainbowLoopO extends IrregularObstacle {
    
    private Angle angle;
    private float numA, numB, numC;
    private boolean sickassRaveMode;
    
    public RainbowLoopO() {
        super("Rainbow Road", new PhysicsInfo(0, 1, 1, 1), new Angle(0));
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.angle = new Angle(0);
        numA = 0;
        numB = 0;
        numC = 4*Utilities.lockedTPF;
        sickassRaveMode = false;
    }
    
    public void onDeath() {
    }

    @Override
    protected final String getDefaultAction() {
        return "OnlyAction";
    }
    
    @Override
    public void updateEntity() {
    	super.updateEntity();
    	this.angle.add(numC);
    	//this.angle = new Angle(90);
    	this.setRotAngle(this.angle);
        this.rotateUpTo(this.angle);
        if (sickassRaveMode){
        	numA = (float) (Math.random()*2*Utilities.lockedTPF-1*Utilities.lockedTPF);
        	numB += numA;
        	//numC += numB;
        	numC += numB/2f;
        } else {
        	numA = (float) (Math.random()*2*Utilities.lockedTPF-1*Utilities.lockedTPF);
        	numB += numA;
        	if (Math.abs(numC + numB/4f) < 5){
        		numC += numB/4f;
        	} else{
        		numB = -numB/2f;
        		numC = 5*Math.signum(numC);
        	}
        }
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "rainbowRoadPlatform", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
