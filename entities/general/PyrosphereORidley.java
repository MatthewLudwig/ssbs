package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.BoundingBox;
import physics.CollisionInfo;
import physics.PhysicsModifier;
import states.game.stages.Stage;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Mario;
import entities.characters.KingDedede.JumpingAction;
import entities.characters.Samus;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionCompleteNumberOfTicks;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class PyrosphereORidley extends ViolentEntity {

	private float counter;

	public PyrosphereORidley() {
		super("Pyrosphere O", 1, new PhysicsInfo(1, 1, 1, 1), 1, 1, 1);
		this.setHasSuperArmor(true);
		this.team = Team.NONE;
		this.facing = true;
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.counter = 0;
		this.scale = 1.5f;
	}

	public boolean addToDamage(float deltaDamage, float attackKnockback,
			float modifier, Angle angleBetweenPlayers, boolean paralyzed,
			boolean buried, boolean meteored, boolean frozen, int section, ViolentEntity hitObj) {
		setPureXVelocity(attackKnockback * angleBetweenPlayers.cos());
		setPureYVelocity(attackKnockback * angleBetweenPlayers.sin());
		this.setCurrentAction(actionMap.get("Hurt"));
		return super.addToDamage(deltaDamage, attackKnockback, modifier,
				angleBetweenPlayers, paralyzed, buried, meteored, frozen, section,
				hitObj);
	}

	public void onCollideWith(PhysicsEntity object, CollisionInfo info) {
		super.onCollideWith(object, info);
		if (object instanceof ViolentEntity
				&& !this.team.equals(((ViolentEntity) object).team)) {
			this.addToDamage(
					0,
					((ViolentEntity) object).getDamageDealt(),
					1,
					Utilities.getPhysicsUtility().calculateCorrectAngle(
							this.getAppropriateBounds().getLowerMidpoint(),
							object.getAppropriateBounds().getLowerMidpoint()),
					false, false, false, false, 0, (ViolentEntity) object);
		}
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		if (this.currentAction.equals(actionMap.get("Stand"))) {
			this.counter -= Utilities.lockedTPF;
			if (counter < 0) {
				this.counter = (float) (Math.random() + 1.5);
				Stage stg = Main.getGameState().getWorld().getStage();
				BoundingBox b = stg.getCameraBounds();
				if (b.isInBounds(this.getPosition())) {
					if (Math.random() < 0.45) {
						Vector2f vec = new Vector2f(
								(float) (Math.random() - 0.5),
								(float) (Math.random() - 0.5));
						vec = vec.normalize().mult(1.2f);
						this.setPureXVelocity(vec.x);
						this.setPureYVelocity(vec.y);
					} else {
						PlayableCharacter pc = Main
								.getGameState()
								.getWorld()
								.getPlayerList()
								.get((int) (Math.random() * Main.getGameState()
										.getWorld().getPlayerList().size()));
						Vector2f vec = new Vector2f(pc.getPosition().x
								- getPosition().x, pc.getPosition().y
								- getPosition().y);
						vec = vec.normalize().mult(1.2f);
						this.setPureXVelocity(vec.x);
						this.setPureYVelocity(vec.y);
					}
				} else {
					Vector2f vec = new Vector2f(stg.getSizeX() / 2f
							- getPosition().x, stg.getSizeY() / 2f
							- getPosition().y);
					vec = vec.normalize().mult(1.2f);
					this.setPureXVelocity(vec.x);
					this.setPureYVelocity(vec.y);
				}
			}
		}
		if (this.getVelocity().x > 0)
			this.setFacing(true);
		if (this.getVelocity().x < 0)
			this.setFacing(false);
		this.setAttackCapability(8f, 1.2f, 0.01f, false);
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "ridley", StayingPower.NONE,
				0.33f).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_00.ogg", -1));
		actionMap.put("Stand", a);
		a = new CharacterAction(this, "ridley", StayingPower.LOW, 0.33f)
				.addModifier(new ActionCompleteNumberOfTicks(
						(int) (1f / Utilities.lockedTPF)));
		actionMap.put("Hurt", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "Stand";
	}

}
