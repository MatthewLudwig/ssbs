package entities.general;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Mario;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class DrWilyWalkerBot extends ViolentEntity {

	public DrWilyWalkerBot() {
		super("Dr Wily", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		setHasSuperArmor(true);
		this.team = Team.NONE;
		this.facing = true;
		if (Math.random() < 0.5)
			this.facing = false;
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		if (this.getDamage() > 50) {
			this.setCurrentAction(actionMap.get("Die"));
		} else {
			if (this.isOnGround()) {
				if (this.currentAction.equals(actionMap.get("Fall"))) {
					this.setCurrentAction(actionMap.get("Land"));
				} else {
					this.setPureXVelocity(10 * Utilities.getGeneralUtility()
							.getBooleanAsSign(facing));
					this.setCurrentAction(actionMap.get("Walk"));
					if (random()) {
						facing = !facing;
					}
				}
			} else {
				this.setCurrentAction(actionMap.get("Fall"));
			}
		}
		this.setAttackCapability(DrWily.DMG[5], DrWily.KNBK[1], DrWily.PRTY[3], false);
	}

	public boolean random() {
		return Math.random() < Utilities.lockedTPF / 2f;
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "walkerFall",
				StayingPower.NONE, 0.5f).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_00.ogg", -1));
		actionMap.put("Fall", a);
		a = new CharacterAction(this, "walkerLand", StayingPower.LOW, 0.5f)
				.addModifier(new ActionPlaySound("/Common Sounds/hit_00.ogg",
						-1));
		actionMap.put("Land", a);
		a = new CharacterAction(this, "walkerDie", StayingPower.MAX, 0.5f)
				.addModifier(new ActionPlaySound("/Common Sounds/hit_00.ogg",
						-1)).addModifier(new ActionKillEntity()).addModifier(new ActionPlaySound("/Character Sounds/Mega Man/sideB2.ogg", 0));
		actionMap.put("Die", a);
		a = new CharacterAction(this, "walkerWalk", StayingPower.BARELY, 0.5f)
				.addModifier(new ActionPlaySound("/Common Sounds/hit_00.ogg",
						-1));
		actionMap.put("Walk", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "Fall";
	}

}
