package entities.general;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

import physics.PhysicsModifier;

import engine.Angle;
import engine.Main;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class MiiMakerRandomHead extends PhysicsEntity {
	private int frame;

	public MiiMakerRandomHead() {
		super("Mii Maker", 1, new PhysicsInfo(0, 0, 0, 1));
		frame = (int) (Math.random() * 12);
		// this.scale = player.getScale();
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
	}

	public void updateEntity() {
		super.updateEntity();
		// this.display.setLocalTranslation(this.display.getLocalTranslation().x,
		// this.display.getLocalTranslation().y,
		// this.display.getLocalTranslation().z+1);
	}

	@Override
	public void onDeath() {
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("OnlyAction", new CharacterAction(this, "randomHead",
				StayingPower.BARELY, 1f).addModifier(new OnlyAction()));
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

	public class OnlyAction extends ActionModifier {

		@Override
		protected void onInit() {
		}

		@Override
		protected boolean onUpdate() {
			this.action.setCurrentFrame(frame);
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {

		}

	}

}
