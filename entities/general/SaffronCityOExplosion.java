package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;
import physics.PhysicsModifier;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Reggie;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class SaffronCityOExplosion extends ViolentEntity {
	
	public SaffronCityOExplosion(){
		super("Explosions", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		this.scale = 0.5f;
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		this.isInvincible = true;
		this.team = Team.NONE;
	}
	
	public void updateEntity() {
		super.updateEntity();
		this.setAttackCapability(30f, 2f, 99f, false);
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("OnlyAction", new CharacterAction(this, "explosion", StayingPower.NONE, 1.2f).addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1)).addModifier(new ActionKillEntity()));
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

	@Override
	public void setFinalSmash(boolean state) {
		// TODO Auto-generated method stub
		
	}

}
