package entities.general;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

import physics.CollisionInfo;
import physics.PhysicsModifier;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Kirby;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.projectiles.KirbyFinalCutter;

public class Tingle extends ViolentEntity {
	private PlayableCharacter grabbedEntity;
	private PlayableCharacter targetEntity;
	private float counter;
	private float moveTimer;

	public Tingle() {
		super("Clock Town O", 1, new PhysicsInfo(0, 0, 0, 1), 1, 1, 1);
		this.scale = 0.25f;
		this.team = Team.NONE;
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		this.counter = 0;
		this.moveTimer = 0;
		this.targetEntity = Main
				.getGameState()
				.getWorld()
				.getPlayerList()
				.get((int) (Math.random() * Main.getGameState().getWorld()
						.getPlayerList().size()));
	}

	public void updateEntity() {
		super.updateEntity();
		if (this.currentAction.equals(actionMap.get("Normal"))) {
			this.setAttackCapability(0f, 0f, 9999f, false);
			if (grabbedEntity == null) {
				counter += Utilities.lockedTPF;
				if (counter > 6.0f) {
					this.targetEntity = Main
							.getGameState()
							.getWorld()
							.getPlayerList()
							.get((int) (Math.random() * Main.getGameState()
									.getWorld().getPlayerList().size()));
					counter = 0;
				}
				Angle ang = Utilities.getPhysicsUtility()
						.calculateCorrectAngle(
								targetEntity.getRealBounds().getExactCenter(),
								this.getRealBounds().getExactCenter());
				this.setPureXVelocity(ang.cos() * 2);
				this.setPureYVelocity(ang.sin() * 4);

				for (PlayableCharacter entity : Main.getGameState().getWorld()
						.getPlayerList()) {
					// System.out.println(entity.getPosition().subtract(this.getPosition()).length());
					if (grabbedEntity == null
							&& entity
									.getPosition()
									.subtract(
											new Vector2f(this.getPosition().x,
													this.getPosition().y + 95))
									.length() < 20) {
						grabbedEntity = entity;
						break;
					}
				}
			} else {
				this.setPureXVelocity(0);
				this.setPureYVelocity(3);
				//this.addPhysicsModifier(PhysicsModifier.ISSTICKY);
				grabbedEntity.setPosition(this.getPosition().x,
						this.getPosition().y + 95);
				grabbedEntity.setVelocity(0f, 0f, false);
				if (((PlayableCharacter) grabbedEntity).checkControl("*", true)) {
                    this.moveTimer += 1f/(grabbedEntity.getDamage()+1);
                    if (this.moveTimer > 5){
                    	grabbedEntity.setPosition(grabbedEntity.getPosition().x, grabbedEntity.getPosition().y-120);
                    	//this.removePhysicsModifier(PhysicsModifier.ISSTICKY);
                    	grabbedEntity = null;
                    }
                }
			}
		}
	}

	public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
		super.onCollideWith(entity, info);
		if (entity instanceof ViolentEntity
				&& ((ViolentEntity) entity).getDamageDealt() > 0 && entity != grabbedEntity) {
			//this.removePhysicsModifier(PhysicsModifier.ISSTICKY);
			this.setCurrentAction(actionMap.get("Dead"));
			grabbedEntity = null;
			this.setPureYVelocity(-10);
		}
	}

	@Override
	public void onDeath() {
		if (grabbedEntity != null) {
			grabbedEntity.setDead(true);
		}
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("Normal", new CharacterAction(this, "tingle",
				StayingPower.BARELY, 1f));
		actionMap.put(
				"Dead",
				new CharacterAction(this, "tingle2", StayingPower.LOW, 0.5f)
						.addModifier(
								new ActionToggleAttribute(
										Attribute.CANONLYHITPLAYERS))
						.addModifier(
								new ActionToggleAttribute(Attribute.DISJOINT))
						.addModifier(new DeadAction()));
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "Normal";
	}

	@Override
	public void setFinalSmash(boolean state) {
		// TODO Auto-generated method stub

	}

	public class DeadAction extends ActionModifier {
		public DeadAction() {

		}

		@Override
		protected void onInit() {
		}

		@Override
		protected boolean onUpdate() {
			return false;
		}

		@Override
		protected boolean shouldFinish() {
			this.action.setCurrentFrame(2);
			return false;
		}

		@Override
		protected void cleanUp() {

		}
	}

}
