package entities.general;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.IrregularObstacle;
import entities.MovingPlatform;
import entities.Obstacle;
import entities.PhysicsInfo;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class FinalPlatformO extends MovingPlatform {
	private FinalFirePlatformO firePlat;
    private boolean init;
    
    public FinalPlatformO() {
        super("Final Destination O", new PhysicsInfo(0, 1, 1, 1), new Angle(0));
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        init = false;
        //System.out.println("hello");
        //this.scale = 0.667f;
        //this.angle = new Angle(0);
    }

    @Override
    protected final String getDefaultAction() {
        return "OnlyAction";
    }
    
    public boolean isOnFire(){
    	return firePlat.getCurrentAction().equals(firePlat.getActionMap().get("Fire"));
    }
    
    public void setFire(){
    	firePlat.setFire();
    }
    
    public void setDead(boolean b){
    	firePlat.setDead(b);
    	super.setDead(b);
    }
    
    @Override
    public void updateEntity() {
    	super.updateEntity();
    	//long time = System.nanoTime();
    	if (!init) {
    		init = true;
    		firePlat = new FinalFirePlatformO();
    		firePlat.setPosition(this.getPosition().x, this.getPosition().y);
			Main.getGameState().spawnEntity(firePlat);
    	} else if (firePlat != null) {
    		firePlat.setPosition(this.getPosition().x, this.getPosition().y);
    	}
    	//this.angle.add(4 * Utilities.lockedTPF);
    	//this.angle = new Angle(90);
    	//this.setRotAngle(this.angle);
        //this.rotateUpTo(this.angle);
    	//System.out.println("Plat Update: " + (System.nanoTime() - time));
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "platform3", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
