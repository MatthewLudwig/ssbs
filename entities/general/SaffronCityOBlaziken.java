package entities.general;

import java.util.HashMap;
import java.util.Map;

import engine.utility.Utilities;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class SaffronCityOBlaziken extends SaffronCityOPokemon {
	
	private float timer;
	
	public SaffronCityOBlaziken(){
		super();
		this.scale = 0.77f*0.8f;
		timer = 0;
	}
	
	public void updateEntity() {
		super.updateEntity();
		this.setAttackCapability(18f, 1.0f, 10f, true);
		timer += Utilities.lockedTPF;
		if (timer > 0.75f){
			setPureXVelocity(30);
		}
		if (getPosition().x > startX){
			setDead(true);
		}
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "blaziken",
				StayingPower.NONE, 0f).addModifier(new ActionPlaySound(
				"/Common Sounds/blam_00.ogg", -1));
		actionMap.put("OnlyAction", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
