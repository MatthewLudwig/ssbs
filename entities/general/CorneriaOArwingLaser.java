package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.audio.AudioNode;
import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Mario;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class CorneriaOArwingLaser extends ViolentEntity {
	private AudioNode audio;
	private boolean init;

	public CorneriaOArwingLaser() {
		super("Corneria O", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		this.isInvincible = true;
		this.team = Team.NONE;
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		audio = Utilities.getCustomLoader().getAudioNode("/Common Sounds/rayGun.ogg");
		init = false;
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		if (!init){
			this.audio.playInstance();
			init = true;
		}
		this.setPureXVelocity(-30);
		this.setAttackCapability(18f, 2f, 10f, false);
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "arwingLaser",
				StayingPower.NONE, 1f).addModifier(new ActionPlaySound(
				"/Common Sounds/electric_00.ogg", -1));
		actionMap.put("Run", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "Run";
	}

}
