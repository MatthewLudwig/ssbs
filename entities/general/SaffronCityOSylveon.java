package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class SaffronCityOSylveon extends SaffronCityOPokemon {
	
	private boolean alreadyHealed;
	private float timer;
	
	public SaffronCityOSylveon(){
		super();
		this.scale = 0.34f;
		timer = 0;
		alreadyHealed = false;
	}
	
	public void updateEntity() {
		super.updateEntity();
		//this.setAttackCapability(-5f, 1.2f, 10f, false);
		timer += Utilities.lockedTPF;
		if (timer > 0.75f){
			setPureXVelocity(30);
		}
		if (getPosition().x > startX){
			setDead(true);
		}
		if (!alreadyHealed){
			for (PlayableCharacter pc : Main.getGameState().getWorld().getPlayerList()){
				if (pc.getPosition().distance(getPosition()) < 64){
					alreadyHealed = true;
					pc.heal(7);
					break;
				}
			}
		}
	}
	
	/*public void onCollideWith(PhysicsEntity object, CollisionInfo info) {
		super.onCollideWith(object, info);
		if (object instanceof PlayableCharacter && !alreadyHealed){
			System.out.println("hello");
			alreadyHealed = true;
			((ViolentEntity) object).heal(5);
		}
	}*/

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "sylveon",
				StayingPower.NONE, 0f).addModifier(new ActionPlaySound(
				"/Common Sounds/psychic_00.ogg", -1));
		actionMap.put("OnlyAction", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
