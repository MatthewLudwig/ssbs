package entities.general;

import java.util.HashMap;
import java.util.Map;

import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class JungleJapesOClaptrap extends ViolentEntity {

	public JungleJapesOClaptrap() {
		super("Jungle Japes O", 0, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		this.isInvincible = true;
		this.team = Team.NONE;
		this.setPureYVelocity(50);

	}

	/*public void onDeath() {
		super.onDeath();
	}*/

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		this.setAttackCapability(15.30f, 2.29f, 999f, true);
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("OnlyAction", new CharacterAction(this, "claptrap",
				StayingPower.BARELY, 0f).addModifier(new ActionToggleAttribute(Attribute.METEORS)));
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
