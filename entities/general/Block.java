package entities.general;

import entities.Obstacle;
import entities.PhysicsInfo;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class Block extends Obstacle {
    String type;
    
    public Block(String type) {
        super("Target Test", new PhysicsInfo(0, 1, 1, 1));
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.type = type;
    }
    
    public void onDeath() {
    }

    @Override
    protected final String getDefaultAction() {
        return "OnlyAction";
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, this.type + "Block", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
