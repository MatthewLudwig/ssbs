package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class SaffronCityODarkrai extends SaffronCityOPokemon {
	private float timer;
	private SaffronCityODarkraiWave wave;
	
	public SaffronCityODarkrai(){
		super();
		this.scale = 0.67f*0.8f;
		timer = 0;
	}
	
	public void updateEntity() {
		super.updateEntity();
		//this.setAttackCapability(-5f, 1.2f, 10f, false);
		timer += Utilities.lockedTPF;
		if (timer > 2.5f){
			setPureYVelocity(20);
		} else if (timer > 1f && wave == null){
			wave = new SaffronCityODarkraiWave();
			wave.setPosition(getPosition().x+65, getPosition().y-100);
			Main.getGameState().spawnEntity(wave);
		}
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "darkrai",
				StayingPower.NONE, 0f);
		actionMap.put("OnlyAction", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
