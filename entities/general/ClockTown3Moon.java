package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class ClockTown3Moon extends ViolentEntity {
	private boolean init;
	private Angle angle;

	public ClockTown3Moon() {
		super("Clock Town", 0, new PhysicsInfo(0, 0, 0, 0), 1f, 1f, 1f);
		this.isInvincible = true;
		this.team = Team.NONE;
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		this.setPosition((float) (Math.random() * 1162) + 122, 750);
		this.angle = new Angle((float) (Math.random() * 360 - 90));
		this.setPureYVelocity(-1);
		init = false;
		// this.scale = 0.01f;

	}

	public void onDeath() {
		super.onDeath();
		System.out.println("ok");
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		if (!init) {
			for (int i = -50; i <= Main.getGameState().getWorld().getStage()
					.getSizeX(); i += 100) {
				for (int k = 0; k < Main.getGameState().getWorld().getStage()
						.getSizeY(); k += 100) {
					ClockTown3WhiteScreen screen = new ClockTown3WhiteScreen();
					screen.setPosition(i, k);
					Main.getGameState().spawnEntity(screen);
				}
			}
			init = true;
		} else {
			ClockTown3WhiteScreen.setAlpha((float) Math.pow(
					1f / (this.getPosition().y - 68), 0.75));
		}
		this.setAttackCapability(6.8f, 1.5f, 999f, true);
		this.rotateUpTo(angle);
		// System.out.println(this.getPosition().y);
		if (this.getPosition().y < 70) {
			this.setPosition((float) (Math.random() * 1162) + 122, 750);
			this.angle = new Angle((float) (Math.random() * 360 - 90));
			this.setPureYVelocity(-1);
		}
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("OnlyAction", new CharacterAction(this, "moon",
				StayingPower.BARELY, 0f));
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
