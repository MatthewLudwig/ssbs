package entities.general;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

import physics.PhysicsModifier;

import engine.Main;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class SkullKidEffect extends PhysicsEntity {
	
	public SkullKidEffect() {
		super("Clock Town O", 1, new PhysicsInfo(0,0,0,1));
		this.setPosition(Main.getGameState().getWorld().getStage().getSizeX()/2f, Main.getGameState().getWorld().getStage().getSizeY()/2f);
		this.scale = Math.max(Main.getGameState().getWorld().getStage().getSizeX(), Main.getGameState().getWorld().getStage().getSizeY())/100f;
		this.alwaysDraw = true;
		//this.scale = player.getScale();
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
	}
	
	public void updateEntity(){
		super.updateEntity();
		//this.display.setLocalTranslation(this.display.getLocalTranslation().x, this.display.getLocalTranslation().y, this.display.getLocalTranslation().z+1);
	}

	@Override
	public void onDeath() {
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "skullKidEffects", StayingPower.BARELY, 1f));
        return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
