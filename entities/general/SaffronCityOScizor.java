package entities.general;

import java.util.HashMap;
import java.util.Map;

import engine.utility.Utilities;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class SaffronCityOScizor extends SaffronCityOPokemon {
	
	private float timer;
	
	public SaffronCityOScizor(){
		super();
		this.scale = 0.74f*0.8f;
		timer = 0;
	}
	
	public void updateEntity() {
		super.updateEntity();
		this.setAttackCapability(14f, 1.2f, 10f, false);
		timer += Utilities.lockedTPF;
		if (timer > 0.75f){
			setPureXVelocity(30);
		}
		if (getPosition().x > startX){
			setDead(true);
		}
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "scizor",
				StayingPower.NONE, 0f).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_00.ogg", -1));
		actionMap.put("OnlyAction", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
