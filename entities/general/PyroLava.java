package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class PyroLava extends ViolentEntity {

	private float targetHeight;

	public PyroLava() {
		super("Pyrosphere", 0, new PhysicsInfo(0, 0, 0, 0), 1f, 1f, 1f);
		targetHeight = 0.0f;
		setHasSuperArmor(true);
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		this.setAttackCapability(6.8f, 1f, 999f, true);
		
		float speed = 10f;

		Angle angle = new Angle(270);
		if (targetHeight > this.getPosition().y)
			angle = new Angle(90);

		float xVelocity = (float) (speed * angle.cos());
		float yVelocity = (float) (speed * angle.sin());

		if (Math.abs(this.getPosition().y - targetHeight) > Math.abs(yVelocity)) {
			this.setPureXVelocity(xVelocity);
			this.setPureYVelocity(yVelocity);
		} else {
			this.setPureXVelocity(0);
			this.setPureYVelocity(0);
		}
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "lava",
				StayingPower.BARELY, 0f);
		a.addModifier(new OnlyAction());
		actionMap.put("OnlyAction", a);
		return actionMap;
	}
	
	public void setTarget(float f){
		this.targetHeight = f;
	}
	
	public float getTarget(){
		return this.targetHeight;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

	public class OnlyAction extends ActionModifier {

		private float speed;

		@Override
		protected void onInit() {
			speed = 100f;
		}

		@Override
		protected boolean onUpdate() {

			/*
			speed = 100f;
			
			//System.out.println(this.action.entity.getVelocity().y);

			Angle angle = new Angle(270);
			if (targetHeight > this.action.entity.getPosition().y)
				angle = new Angle(0);

			float xVelocity = (float) (this.speed * angle.cos());
			float yVelocity = (float) (this.speed * angle.sin());

			if (Math.abs(this.action.entity.getPosition().y - targetHeight) > Math.abs(yVelocity)) {
				((PhysicsEntity) this.action.entity)
						.setPureXVelocity(xVelocity);
				((PhysicsEntity) this.action.entity)
						.setPureYVelocity(yVelocity);
			} else {
				((PhysicsEntity) this.action.entity).setPureXVelocity(0);
				((PhysicsEntity) this.action.entity).setPureYVelocity(0);
			}

			if (Math.abs(this.action.entity.getPosition().y - targetHeight) <= speed)
				return true;
			else
				return false;*/
			return true;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {

		}

	}

}
