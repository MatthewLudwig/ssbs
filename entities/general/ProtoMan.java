package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.CharacterStats;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionSpawnEntity;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.CharacterActionFactory;
import entities.characters.actions.StayingPower;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.projectiles.LinkBoomerang;
import entities.projectiles.MegaManPellet;
import entities.projectiles.Projectile;
import entities.projectiles.ProtoManBoomerang;
import entities.projectiles.ProtoManPellet;
import entities.projectiles.ProtoManSpark;

public class ProtoMan extends ViolentEntity {
	public static final float[] STAT = CharacterStats.MegaManStats;
	public static final float[] DMG = CharacterStats.MegaManDamage;
	public static final float[] KNBK = CharacterStats.MegaManKnockback;
	public static final float[] PRTY = CharacterStats.MegaManPriority;
	public static final float[] ISPD = CharacterStats.MegaManImageSpeed;
	public static final short[][] SECT = CharacterStats.MegaManSectionFrames;
	boolean init;

	public ProtoMan() {
		super("Proto Man", STAT[7] / 2f, new PhysicsInfo(STAT[0], STAT[1],
				STAT[2], STAT[3] / 2f), STAT[4], STAT[5], STAT[6]);
		this.team = Team.NONE;
		this.scale = 1.27f;
		init = false;
		setHasSuperArmor(true);
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		if (!init) {
			init = true;
			setCurrentAction(actionMap.get("Enter"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Stand"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Crouch"));
		}
		if (random() && getPosition().x < 625) {
			setCurrentAction(actionMap.get("Walk Right"));
		}
		if (random() && getPosition().x > 375) {
			setCurrentAction(actionMap.get("Walk Left"));
		}
		/*if (random() && (getPosition().x < 675 || random())) {
			setCurrentAction(actionMap.get("Sprint Right"));
		}
		if (random() && (getPosition().x > 325 || random())) {
			setCurrentAction(actionMap.get("Sprint Left"));
		}*/
		if (random()) {
			setCurrentAction(actionMap.get("Jump 1"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Jump 2"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Neutral A"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Side A"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Up A"));
		}
	}

	public boolean random() {
		return Math.random() < Utilities.lockedTPF / 2f;
	}

	public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
		if (entity instanceof Projectile && info.getDirection().matchesFacing(!this.facing) && (this.currentAction.equals(this.actionMap.get("Crouch")) || this.currentAction.equals(this.actionMap.get("Jump 1")) || this.currentAction.equals(this.actionMap.get("Jump 2")))) {
            ((Projectile) entity).reflect(this);
        } else if (entity instanceof Projectile && info.getDirection().matchesFacing(this.facing) && this.currentAction.equals(this.actionMap.get("Stand"))) {
            ((Projectile) entity).reflect(this);
        } else {
            super.onCollideWith(entity, info);
            if (getDamage() > 50){
            	this.setDead(true);
    		}
        }
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("Stand", CharacterActionFactory.getStand(this, 1));
		actionMap.put("Crouch", CharacterActionFactory.getCrouch(this, 1));
		actionMap.put("Walk Right", CharacterActionFactory.getWalk(this, true));
		actionMap.put("Walk Left", CharacterActionFactory.getWalk(this, false));
		actionMap.put("Sprint Right",
				CharacterActionFactory.getSprint(this, true));
		actionMap.put("Sprint Left",
				CharacterActionFactory.getSprint(this, false));
		actionMap.put("Jump 1", getJumpOne(this, 1));
		actionMap.put("Jump 2", getJumpTwo(this, 1));
		actionMap.put("Neutral A", this.getNeutralA());
		actionMap.put("Side A", this.getSideA());
		actionMap.put("Up A", this.getUpA());
		actionMap.put("Enter", new CharacterAction(this, "teleportIn",
				StayingPower.MAX, 1));
		return actionMap;
	}

	public static CharacterAction getJumpOne(PhysicsEntity entity,
			float updateLimit) {
		CharacterAction action = getAnimation(entity, "jump1",
				StayingPower.MEDIUM, updateLimit);
		action.addModifier(new ActionMovement(25f, 90f, 0f, false))
				.addModifier(new ActionWaitTillOnGround());
		return action;
	}

	public static CharacterAction getJumpTwo(PhysicsEntity entity,
			float updateLimit) {
		CharacterAction action = getAnimation(entity, "jump2",
				StayingPower.HIGH, updateLimit);
		action.addModifier(new ActionMovement(25f, 90f, 0f, false))
				.addModifier(new ActionWaitTillOnGround());
		return action;
	}

	public static CharacterAction getAnimation(PhysicsEntity entity,
			String animationName, StayingPower stayingPower, float updateLimit) {
		CharacterAction action = new CharacterAction(entity, animationName,
				stayingPower, updateLimit);

		return action;
	}

	protected CharacterAction getNeutralA() {
		CharacterAction action = new CharacterAction(this, "shoot",
				StayingPower.EXTREME, ISPD[0]);
		action.addModifier(new ActionSpawnEntity(new short[]{1,3,5}, 99, ProtoManPellet.class, this).setXOffset(30, true));
		action.addModifier(new ActionPlaySound("/Character Sounds/Mega Man/neutralA.ogg", 0));
		return action;
	}

	protected CharacterAction getSideA() {
		CharacterAction action = new CharacterAction(this, "shoot",
				StayingPower.EXTREME, ISPD[0]);
		action.addModifier(new ActionSpawnEntity(new short[]{3}, 99, ProtoManSpark.class, this).setXOffset(30, true));
		action.addModifier(new ActionPlaySound("/Character Sounds/Mega Man/sideB1.ogg", 3));
		return action;
	}

	protected CharacterAction getUpA() {
		CharacterAction action = new CharacterAction(this, "shoot",
				StayingPower.EXTREME, ISPD[0]);
		action.addModifier(new ActionSpawnEntity(new short[]{0}, 2, ProtoManBoomerang.class, this));
		action.addModifier(new ActionPlaySound("/Character Sounds/Mega Man/downA.ogg", 0));
		return action;
	}

	@Override
	protected String getDefaultAction() {
		return "Stand";
	}

}
