package entities.general;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

import physics.PhysicsModifier;

import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class IceEffect extends PhysicsEntity {
	PlayableCharacter player;
	Vector2f pos;
	
	public IceEffect(PlayableCharacter p) {
		super("Status Effect", 1, new PhysicsInfo(0,0,0,1));
		player = p;
		this.scale = (player.getRealBounds().getDimensions().x/640f + 0.5f)/1.5f;
		this.alpha = 0.8f;
		//this.scale = player.getScale();
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
	}
	
	public void updateEntity(){
		super.updateEntity();
		setPosition(player.getRealBounds()
				.getLowerMidpoint().x - 424.91f*scale*scale - 21.35f*scale + 321.05f, player
				.getRealBounds().getLowerMidpoint().y + 10.68f*scale*scale - 62.28f*scale + 6.41f);
		if (!player.isCurrentlyFrozen()){
			setDead();
		}
		this.display.setLocalTranslation(this.display.getLocalTranslation().x, this.display.getLocalTranslation().y, this.display.getLocalTranslation().z+1);
	}

	@Override
	public void onDeath() {
		
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "ice", StayingPower.BARELY, 1f));
        return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
