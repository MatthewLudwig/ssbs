package entities.general;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.IrregularObstacle;
import entities.MovingPlatform;
import entities.Obstacle;
import entities.PhysicsInfo;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class MiiMakerSlider extends MovingPlatform {
	private boolean direction;
	private boolean moving;
	private float speedScale;

	public MiiMakerSlider() {
		super("Mii Maker", new PhysicsInfo(0, 1, 1, 1), new Angle(0));
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.moving = false;
		this.direction = false;
		if (Math.random() < 0.5)
			this.direction = true;
		this.speedScale = 1;
	}

	@Override
	protected final String getDefaultAction() {
		return "OnlyAction";
	}

	@Override
	public void updateEntity() {
		super.updateEntity();
		if (this.getPosition().x > 1374)
			this.direction = false;
		if (this.getPosition().x < 981)
			this.direction = true;

		if (moving)
			setPureXVelocity(13 * Utilities.getGeneralUtility()
					.getBooleanAsSign(this.direction) * this.speedScale);
		else
			setPureXVelocity(0);
	}
	
	public void setMoving(){
		setMoving(1);
	}

	public void setMoving(float spd) {
		moving = true;
		this.direction = false;
		if (Math.random() < 0.5)
			this.direction = true;
		this.speedScale = (float) (Math.random()*0.45 + 0.75)*spd;
	}

	public void stopMoving() {
		moving = false;
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("OnlyAction", new CharacterAction(this, "slider",
				StayingPower.BARELY, 1f));
		return actionMap;
	}
}
