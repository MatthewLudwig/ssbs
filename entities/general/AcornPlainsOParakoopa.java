package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Mario;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class AcornPlainsOParakoopa extends ViolentEntity {

	public AcornPlainsOParakoopa() {
		super("Acorn Plains O", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		this.isInvincible = true;
		this.team = Team.NONE;
		this.facing = true;
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		this.setPureXVelocity(-12 * Utilities.getGeneralUtility().getBooleanAsSign(facing));
		if (this.hasCollidedWithObstacle(true)){
			facing = !facing;
			this.addToPosition(-12 * Utilities.getGeneralUtility().getBooleanAsSign(facing), 0);
		}
		this.setAttackCapability(5f, 0.75f, 10f, false);
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "parakoopa",
				StayingPower.NONE, 0.33f).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_00.ogg", -1));
		actionMap.put("Run", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "Run";
	}

}
