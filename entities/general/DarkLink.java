package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.CharacterStats;
import entities.characters.Link;
import entities.characters.Mario;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionCompleteNumberOfTicks;
import entities.characters.actions.ActionHalfHop;
import entities.characters.actions.ActionHurt;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionMovement;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionPlaySoundForMarth;
import entities.characters.actions.ActionPressMultiHit;
import entities.characters.actions.ActionQuickFall;
import entities.characters.actions.ActionShortHop;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.CharacterActionFactory;
import entities.characters.actions.StayingPower;
import entities.characters.actions.ActionToggleAttribute.Attribute;

public class DarkLink extends ViolentEntity {
	public static final float[] STAT = CharacterStats.LinkStats;
	public static final float[] DMG = CharacterStats.LinkDamage;
	public static final float[] KNBK = CharacterStats.LinkKnockback;
	public static final float[] PRTY = CharacterStats.LinkPriority;
	public static final float[] ISPD = CharacterStats.LinkImageSpeed;
	public static final short[][] SECT = CharacterStats.LinkSectionFrames;
	float speed;

	public DarkLink() {
		super("Temple O", STAT[7] / 2f, new PhysicsInfo(STAT[0], STAT[1],
				STAT[2], STAT[3]), STAT[4], STAT[5], STAT[6]);
		this.team = Team.NONE;
		this.scale = 1.28f;
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		if (random()) {
			setCurrentAction(actionMap.get("Stand"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Crouch"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Walk Right"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Walk Left"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Sprint Right"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Sprint Left"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Jump 1"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Jump 2"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Neutral A"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Side A"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Up A"));
		}
		if (random()) {
			setCurrentAction(actionMap.get("Down A"));
		}
	}

	public boolean random() {
		return Math.random() < Utilities.lockedTPF/2f;
	}
	
	public void onCollideWith(PhysicsEntity entity, CollisionInfo info){
		super.onCollideWith(entity, info);
		if (team.equals(Team.NONE) && entity instanceof PlayableCharacter){
			team = ((ViolentEntity)entity).team;
		}
	}

	/*
	 * public boolean addToDamage(float deltaDamage, float attackKnockback,
	 * float modifier, Angle angleBetweenPlayers, boolean paralyzed, boolean
	 * buried, boolean meteored, int section, ViolentEntity hitObj) { boolean
	 * returnVal = super.addToDamage(deltaDamage, attackKnockback, modifier,
	 * angleBetweenPlayers, paralyzed, buried, meteored, section, hitObj);
	 * return returnVal; }
	 */

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("Stand", CharacterActionFactory.getStand(this, 1));
		actionMap.put("Crouch", CharacterActionFactory.getCrouch(this, 1));
		actionMap.put("Walk Right", CharacterActionFactory.getWalk(this, true));
		actionMap.put("Walk Left", CharacterActionFactory.getWalk(this, false));
		actionMap.put("Sprint Right",
				CharacterActionFactory.getSprint(this, true));
		actionMap.put("Sprint Left",
				CharacterActionFactory.getSprint(this, false));
		actionMap.put("Jump 1", getJumpOne(this, 1));
		actionMap.put("Jump 2", getJumpTwo(this, 1));
		actionMap.put("Neutral A", this.getNeutralA());
		actionMap.put("Side A", this.getSideA());
		actionMap.put("Up A", this.getUpA());
		actionMap.put("Down A", this.getDownA());
		return actionMap;
	}

	public static CharacterAction getJumpOne(PhysicsEntity entity,
			float updateLimit) {
		CharacterAction action = getAnimation(entity, "jump1",
				StayingPower.MEDIUM, updateLimit);
		action.addModifier(new ActionMovement(25f, 90f, 0f, false))
				.addModifier(new ActionWaitTillOnGround());
		return action;
	}

	public static CharacterAction getJumpTwo(PhysicsEntity entity,
			float updateLimit) {
		CharacterAction action = getAnimation(entity, "jump2",
				StayingPower.HIGH, updateLimit);
		action.addModifier(new ActionMovement(25f, 90f, 0f, false))
				.addModifier(new ActionWaitTillOnGround());
		return action;
	}

	public static CharacterAction getAnimation(PhysicsEntity entity,
			String animationName, StayingPower stayingPower, float updateLimit) {
		CharacterAction action = new CharacterAction(entity, animationName,
				stayingPower, updateLimit);

		return action;
	}

	protected CharacterAction getNeutralA() {
		CharacterAction action = new CharacterAction(this, "neutralA",
				StayingPower.EXTREME, ISPD[0]);
		action.addModifier(new ActionAttack(DMG[0], KNBK[0], PRTY[0], SECT[0]));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_13.ogg", -1));
		action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
		return action;
	}

	protected CharacterAction getSideA() {
		CharacterAction action = new CharacterAction(this, "sideA",
				StayingPower.EXTREME, ISPD[1]);
		action.addModifier(new ActionAttack(DMG[1], KNBK[1], PRTY[1], true,
				SECT[1]));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_00.ogg", -1));
		action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
		return action;
	}

	protected CharacterAction getUpA() {
		CharacterAction action = new CharacterAction(this, "upA",
				StayingPower.EXTREME, ISPD[2]);
		action.addModifier(new ActionAttack(DMG[2], KNBK[2], PRTY[2], SECT[2]));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_12.ogg", -1));
		action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
		return action;
	}

	protected CharacterAction getDownA() {
		CharacterAction action = new CharacterAction(this, "downA",
				StayingPower.EXTREME, ISPD[3]);
		action.addModifier(new ActionAttack(DMG[3], KNBK[3], PRTY[3], SECT[3]));
		action.addModifier(new ActionPlaySound("/Common Sounds/hit_01.ogg", -1));
		action.addModifier(new ActionToggleAttribute(Attribute.DISJOINT));
		action.addModifier(new ActionHalfHop());
		action.addModifier(new ActionToggleAttribute(Attribute.METEORS));
		return action;
	}

	@Override
	protected String getDefaultAction() {
		return "Stand";
	}

}
