package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.BoundingBox;
import physics.PhysicsModifier;

import com.jme3.math.Vector2f;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.Team;
import entities.ViolentEntity;
import entities.characters.Mario;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionWaitTillOnGround;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class ThreedOFace extends ViolentEntity {

	private boolean init;
	private float speed;
	
	public ThreedOFace() {
		super("Threed O", 1, new PhysicsInfo(1, 1, 1, 1), 1f, 1f, 1f);
		this.isInvincible = true;
		this.team = Team.NONE;
		init = false;
		speed = 0.5f;
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}
	
	public void onDeath(){
		super.onDeath();
		System.out.println("ok");
	}

	public void updateEntity() {
		super.updateEntity();
		this.canDie = false;
		BoundingBox bounds = Main.getGameState().getWorld().getStage().getCameraBounds();
		if (!init){
			init = true;
			Vector2f vec = new Vector2f((float)(Math.random()-0.5), (float) (Math.random()-0.5)).normalize().mult(speed);
			this.setPureXVelocity(vec.x);
			this.setPureYVelocity(vec.y);
		} else if (bounds.getLowerLeft().x > getPosition().x || bounds.getUpperRight().x < getPosition().x) {
			if (bounds.getLowerLeft().y > getPosition().y || bounds.getUpperRight().y < getPosition().y){
				speed += 1f;
				Vector2f vec = new Vector2f(-getVelocity().x, -getVelocity().y).normalize().mult(speed);
				this.setPureXVelocity(vec.x);
				this.setPureYVelocity(vec.y);
				//System.out.println("A");
				this.addToPosition(vec.x, vec.y);
			} else {
				speed += 1f;
				Vector2f vec = new Vector2f(-getVelocity().x, getVelocity().y).normalize().mult(speed);
				this.setPureXVelocity(vec.x);
				this.setPureYVelocity(vec.y);
				//System.out.println("B");
				this.addToPosition(vec.x, vec.y);
			}
		} else if (bounds.getLowerLeft().y > getPosition().y || bounds.getUpperRight().y < getPosition().y) {
			speed += 1f;
			Vector2f vec = new Vector2f(getVelocity().x, -getVelocity().y).normalize().mult(speed);
			this.setPureXVelocity(vec.x);
			this.setPureYVelocity(vec.y);
			//System.out.println("C");
			this.addToPosition(vec.x, vec.y);
		}
		this.setAttackCapability(0.5f*speed, 0.07f*speed, 0.75f*speed, false);
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "face",
				StayingPower.NONE, 0f).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_00.ogg", -1));
		actionMap.put("Run", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "Run";
	}

}
