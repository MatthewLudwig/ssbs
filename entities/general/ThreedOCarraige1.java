package entities.general;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.IrregularObstacle;
import entities.MovingPlatform;
import entities.Obstacle;
import entities.PhysicsInfo;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class ThreedOCarraige1 extends MovingPlatform {
    
	public float limit;
	
    public ThreedOCarraige1() {
        super("Threed O", new PhysicsInfo(0, 1, 1, 1), new Angle(0));
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
    }

    @Override
    protected final String getDefaultAction() {
        return "OnlyAction";
    }
    
    @Override
    public void updateEntity() {
    	super.updateEntity();
    	if (getPosition().x > limit && getVelocity().x != 0){
    		setPureXVelocity(0);
    		setPosition(limit, getPosition().y);
    	}
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "carraige1", StayingPower.BARELY, 1f));
        return actionMap;
    }
}
