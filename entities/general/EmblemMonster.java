package entities.general;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;


import engine.Angle;
import engine.Main;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.ViolentEntity;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class EmblemMonster extends ViolentEntity {

	private int frame;

	public EmblemMonster() {
		super("Tower Of Valni", 0, new PhysicsInfo(0, 0, 0, 0), 1f, 1f, 1f);
		frame = (int) (Math.random() * 8);
		setHasSuperArmor(true);
	}

	@Override
	public void setFinalSmash(boolean state) {
		// nothing
	}

	public void updateEntity() {
		super.updateEntity();
		this.setAttackCapability(10f, 1f, 13f, false);
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "towerOfValniEnemies",
				StayingPower.BARELY, 0f);
		a.addModifier(new OnlyAction());
		actionMap.put("OnlyAction", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

	public class OnlyAction extends ActionModifier {
		
		private float speed;
	    private Angle angle;

		@Override
		protected void onInit() {
			speed = 25.74f;
			angle = new Angle(0);
		}

		@Override
		protected boolean onUpdate() {
			this.action.setCurrentFrame(frame);
			
			speed = 25.74f;
			
			Vector2f p = Main.getGameState().getWorld().getClosestPlayer(this.action.entity.getPosition()).getPosition();
			
			double r = Math.random();
			if (r < frame/100.0 && angle != null && angle.getValue() == 0.0f)
				angle = new Angle((float)(Math.toDegrees(Math.atan2(p.x - this.action.entity.getPosition().x, p.y - this.action.entity.getPosition().y))));
			else if (r < (8.0-frame/100.0) && angle != null &&  angle.getValue() != 0.0f)
				angle = new Angle(0);
			else if (angle == null)
				angle = new Angle(0);
			
			float xVelocity = (float) (this.speed * angle.cos());
            float yVelocity = (float) (this.speed * angle.sin());

            ((PhysicsEntity) this.action.entity).setPureXVelocity(xVelocity);
            ((PhysicsEntity) this.action.entity).setPureYVelocity(yVelocity);
			
			if (this.action.entity.getPosition().x < 1200)
				return false;
			else
				return true;
		}

		@Override
		protected boolean shouldFinish() {
			return true;
		}

		@Override
		protected void cleanUp() {

		}

	}

}
