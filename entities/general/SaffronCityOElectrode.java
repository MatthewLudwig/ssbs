package entities.general;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class SaffronCityOElectrode extends SaffronCityOPokemon {
	
	private float timer;
	
	public SaffronCityOElectrode(){
		super();
		this.scale = 0.77f*0.8f;
		timer = 0;
	}
	
	public void updateEntity() {
		super.updateEntity();
		//this.setAttackCapability(-5f, 1.2f, 10f, false);
		timer += Utilities.lockedTPF;
		if (timer < 0.8f){
			currentAction.freezeFrame(true);
		} else if (currentAction.getCurrentFrameNumber() == 5){
			SaffronCityOExplosion explosion = new SaffronCityOExplosion();
			explosion.setPosition(getPosition().x, getPosition().y-78);
			Main.getGameState().spawnEntity(explosion);
			setDead(true);
		} else if (timer > 0.8f){
			currentAction.freezeFrame(false);
		}
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		CharacterAction a = new CharacterAction(this, "electrode",
				StayingPower.NONE, 0.2f);
		actionMap.put("OnlyAction", a);
		return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
