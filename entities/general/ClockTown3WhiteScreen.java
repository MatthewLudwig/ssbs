package entities.general;

import java.util.HashMap;
import java.util.Map;

import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.ui.Picture;

import physics.PhysicsModifier;

import engine.Main;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.PlayableCharacter;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class ClockTown3WhiteScreen extends PhysicsEntity {
	
	private static float alph;
	
	public ClockTown3WhiteScreen() {
		super("Clock Town", 1, new PhysicsInfo(0,0,0,1));
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		alph = 0f;
		//this.scale = Main.getGameState().getWorld().getStage().getSizeX()/100;
	}

	@Override
	public void onDeath() {
	}
	
	public void updateEntity() {
		super.updateEntity();
		Material mat = ((Picture) this.display.getChild("Picture")).getMaterial();
		mat.setColor("Color", new ColorRGBA(this.tint.x, this.tint.y, this.tint.z, this.alph));
		((Picture) this.display.getChild("Picture")).setMaterial(mat);
	}
	
	public static void setAlpha(float f){
		alph = f;
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "whiteScreen", StayingPower.BARELY, 1f));
        return actionMap;
	}

	@Override
	protected String getDefaultAction() {
		return "OnlyAction";
	}

}
