package entities;

/**
 *
 * @author Matthew
 */
public class PhysicsInfo {
    private float weight;
    private float fallSpeed;
    private float jumpSpeed;
    private float moveSpeed;
    private boolean onGround;
    private boolean hasCollided;
    private boolean hasCollidedWithSideOfObstacle;
    private boolean hasCollidedWithSurfaceOfObstacle;

    public PhysicsInfo(float weight, float fallSpeed, float jumpSpeed, float moveSpeed) {
        this.weight = weight;
        this.fallSpeed = fallSpeed;
        this.jumpSpeed = 1.08f * jumpSpeed;
        this.moveSpeed = moveSpeed;
        this.onGround = false;
        this.hasCollided = false;
        this.hasCollidedWithSideOfObstacle = false;
        this.hasCollidedWithSurfaceOfObstacle = false;
    }
    
    public void setWeight(float w) {
    	this.weight = w;
    }
    
    public void setFallSpeed(float w) {
    	this.fallSpeed = w;
    }
    
    public void setJumpSpeed(float w) {
    	this.jumpSpeed = w;
    }
    
    public void setMoveSpeed(float w) {
    	this.moveSpeed = w;
    }

    public float getWeight() {
        return this.weight;
    }

    public float getFallSpeed() {
        return this.fallSpeed;
    }
    
    public float getJumpSpeed() {
        return this.jumpSpeed;
    }

    public void multiplyMoveSpeed(float multiplier) {
        this.moveSpeed *= multiplier;
    }
    
    public float getMoveSpeed() {
        return this.moveSpeed;
    }

    public void setOnGround(boolean isOnGround) {
        this.onGround = isOnGround;
    }

    public boolean isOnGround() {
        return this.onGround;
    }

    public void setHasCollided(boolean hasCollided) {
        this.hasCollided = hasCollided;
    }

    public boolean hasCollided() {
        return this.hasCollided;
    }
    
    public void setHasCollidedWithObstacle(boolean hasCollided, boolean sideOrSurface) {
        if (sideOrSurface) {
            this.hasCollidedWithSideOfObstacle = hasCollided;
        } else {
            this.hasCollidedWithSurfaceOfObstacle = hasCollided;
        }
    }

    public boolean hasCollidedWithObstacle(boolean sideOrSurface) {
        return sideOrSurface ? this.hasCollidedWithSideOfObstacle : this.hasCollidedWithSurfaceOfObstacle;
    }
}
