package entities;

import physics.CollisionInfo;
import physics.CollisionInfo.CollisionDirection;

/**
 *
 * @author Matthew
 */
public abstract class Obstacle extends PhysicsEntity {
    private float damage;
    private short damageCooldown;
    private boolean hasHurtAnimation;
    private float damageDealt;
    private float attackKnockback;
    private float priority;
    private boolean canReflect;

    public Obstacle(String name, PhysicsInfo info) {
        super(name, 1, info);

        this.damage = 0;
        this.damageCooldown = 0;
        this.hasHurtAnimation = true;

        this.damageDealt = 0;
        this.attackKnockback = 0;
        this.priority = 0;

        this.canReflect = false;
    }

    public void setDamage(float newDamage) {
        this.damage = newDamage;
    }

    public final void addToDamage(float deltaDamage, float attackKnockback, float knockbackModifier, float angleBetweenPlayers) {
        if (this.damageCooldown == 0) {
            this.damage = Math.min(this.damage + deltaDamage, 999);
            float pieceOne = this.damage * (this.physicsInfo.getWeight() + 1 + (1.4f * deltaDamage));
            float pieceTwo = 10f * (this.physicsInfo.getWeight() + 1);
            float knockback = ((pieceOne / pieceTwo) + 18f) * .0833f * knockbackModifier;
            float knockbackSpeed = 5 * (knockback + (knockbackModifier));

            if (this.hasHurtAnimation) {
//                this.setCurrentAction(new CharacterActionHurt(this, (short) (knockbackSpeed / 10), knockbackSpeed, angleBetweenPlayers)); 
            }

            this.damageCooldown = 20;
        }
    }

    public float getDamage() {
        return this.damage;
    }

    public void setHasHurtAnimation(boolean hasHurtAnimation) {
        this.hasHurtAnimation = hasHurtAnimation;
    }

    public boolean hasHurtAnimation() {
        return this.hasHurtAnimation;
    }

    public void setAttackCapability(float damageDealt, float knockback, float priority) {
        this.damageDealt = damageDealt;
        this.attackKnockback = knockback;
        this.priority = priority;
    }

    public float getDamageDealt() {
        return this.damageDealt;
    }

    public float getKnockback() {
        return this.attackKnockback;
    }

    public float getPriority() {
        return this.priority;
    }

    public void setCanReflect(boolean can) {
        this.canReflect = can;
    }

    public boolean canReflect() {
        return this.canReflect;
    }

    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
        super.onCollideWith(entity, info);
        
        if (info.getDirection() == CollisionInfo.CollisionDirection.LEFT || info.getDirection() == CollisionInfo.CollisionDirection.RIGHT) {
            entity.physicsInfo.setHasCollidedWithObstacle(true, true);
            entity.setXVelocity(0);
        } else if (info.getDirection() == CollisionInfo.CollisionDirection.TOP || info.getDirection() == CollisionInfo.CollisionDirection.BOTTOM) {
            entity.physicsInfo.setHasCollidedWithObstacle(true, false);
            entity.setYVelocity(0, false);
            
            if (info.getDirection() == CollisionDirection.TOP) {
                entity.setOnGround(true);
            }
        }
        
        entity.addToPosition(info.getResponse().x, info.getResponse().y);
    }
    
    @Override
    public void updateEntity() {
        super.updateEntity();
        this.damageCooldown = (short) Math.max(this.damageCooldown - 1, 0);
    }
}
