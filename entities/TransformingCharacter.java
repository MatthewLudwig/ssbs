package entities;

import java.util.Map;

import com.jme3.scene.Node;

import engine.utility.Utilities;
import entities.characters.KingDedede;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;

public abstract class TransformingCharacter extends PlayableCharacter {
	
    protected Map<String, CharacterAction> firstActionMap;
    protected Map<String, CharacterAction> secondActionMap;
    protected float transformationTimer;
    
	protected TransformingCharacter(String name, PhysicsInfo info, float priority, float knockback, float damage, float imageSpeed, float recoveryForce) {
		super(name, info, priority, knockback, damage, imageSpeed, recoveryForce);

	}
    
    @Override
    public void updateEntity() {
        super.updateEntity();
        
        this.transformationTimer -= Utilities.lockedTPF;
        
        if (this.transformationTimer <= 0) {
            this.setCurrentAction(this.actionMap.get("Transform"));
        }
    }
    
    @Override
    public void onDeath() {
        super.onDeath();
        
        this.actionMap = this.firstActionMap;
        this.transformationTimer = 0;
    }
	
    @Override
    public void setUpEntity(int id, Node displayNode, Node guiNode, boolean alwaysDrawn) {
        super.setUpEntity(id, displayNode, guiNode, alwaysDrawn);
        this.firstActionMap = this.actionMap;
        this.secondActionMap = this.setUpSecondActionMap();
        this.transformationTimer = 0;
    }
    
    /**
     * The returned Map must contain an action called Transform in order to transform back.
     * @return
     */
    protected abstract Map<String, CharacterAction> setUpSecondActionMap();
    
    public class ActionTransform extends ActionModifier {

        private float transformationTime;
        
        public ActionTransform(float time) {
            this.transformationTime = time;
        }

        @Override
        public void onInit() {
            if (this.transformationTime != 0) {
                ((TransformingCharacter) this.action.entity).transformationTimer = this.transformationTime;
                ((TransformingCharacter) this.action.entity).actionMap = ((TransformingCharacter) this.action.entity).secondActionMap;
            } else {
                ((TransformingCharacter) this.action.entity).transformationTimer = 0;
                ((TransformingCharacter) this.action.entity).actionMap = ((TransformingCharacter) this.action.entity).firstActionMap;
            }
        }

        @Override
        public boolean onUpdate() {
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
}
