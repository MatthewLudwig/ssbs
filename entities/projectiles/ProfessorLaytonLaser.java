package entities.projectiles;

import physics.PhysicsModifier;
import engine.Angle;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import entities.characters.Link;
import entities.characters.ProfessorLayton;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

public class ProfessorLaytonLaser extends Projectile {

	private Vector2f savedVelocity;
	private boolean init;
	private int numBounces;
	
    public ProfessorLaytonLaser(ProfessorLayton object) {
        super(object, ProfessorLayton.DMG[7], ProfessorLayton.KNBK[7], ProfessorLayton.PRTY[7], false, ProfessorLayton.SECT[7]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.shouldDieOnCollision = false;
        init = false;
        savedVelocity = new Vector2f(0,0);
        numBounces = 0;
    }

    @Override
    public void updateEntity() {
        super.updateEntity();
        if (!init){
        	init = true;
        	if (((ProfessorLayton) shooter).laserDirection == 1){
        		setPureXVelocity((float) (20*Utilities.getGeneralUtility().getBooleanAsSign(facing)*(1/Math.sqrt(2))));
        		setPureYVelocity((float) (20*(1/Math.sqrt(2))));
        		//addToPosition(0,30);
        	} else if (((ProfessorLayton) shooter).laserDirection == 2){
        		setPureXVelocity((float) (20*Utilities.getGeneralUtility().getBooleanAsSign(facing)*(1/Math.sqrt(2))));
        		setPureYVelocity((float) (-20*(1/Math.sqrt(2))));
        		//addToPosition(0,-30);
        	} else {
        		setPureXVelocity(20*Utilities.getGeneralUtility().getBooleanAsSign(facing));
        		setPureYVelocity(0);
        	}
        	savedVelocity = new Vector2f(getVelocity().x, getVelocity().y);
        }
        if (getVelocity().y == 0){
        	this.rotateUpTo(new Angle(0));
        } else if (this.getVelocity().y > 0) {
        	this.rotateUpTo(new Angle(45));
        } else {
        	this.rotateUpTo(new Angle(-45));
        }
        if (this.hasCollidedWithObstacle(true)){
        	setPureXVelocity(-savedVelocity.x);
        	this.setFacing(!this.facing);
        	this.addToPosition(-savedVelocity.x, savedVelocity.y);
        	savedVelocity = new Vector2f(getVelocity().x, getVelocity().y);
        	numBounces++;
        }
        if (this.hasCollidedWithObstacle(false) || this.isOnGround()){
        	setPureYVelocity(-savedVelocity.y);
        	this.addToPosition(savedVelocity.x, -savedVelocity.y);
        	savedVelocity = new Vector2f(getVelocity().x, getVelocity().y);
        	numBounces++;
        }
        if (numBounces > 5){
        	setDead(true);
        }
        /*if (this.isOnGround()){
        	setPureYVelocity((float) (20*Math.sqrt(2)));
        	this.addToPosition(0,1);
        }*/
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "laser", StayingPower.NONE, ProfessorLayton.ISPD[7]).addModifier(new ActionPlaySound("/Common Sounds/electric_01.ogg", -1)));
        return actionMap;
    }
}
