package entities.projectiles;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;
import engine.utility.Utilities;
import entities.characters.Kirby;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class KirbyPlasmaShot extends Projectile {
    private float lifeTimer;
    
    public KirbyPlasmaShot(Kirby object) {
        super(object, 6.8f, 1.0f, 10, true);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.lifeTimer = 0;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        this.setXVelocity(this.getFacing() ? 60 : -60);
        
        if (this.lifeTimer >= .75f) {
            this.setDead(true);
        } else {
            this.lifeTimer += Utilities.lockedTPF;
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "plasmaShot", StayingPower.NONE, 2.14f).addModifier(new ActionPlaySound("/Common Sounds/hit_14.ogg", -1)));
        return actionMap;
    }
}
