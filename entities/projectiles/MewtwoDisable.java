package entities.projectiles;

import engine.Angle;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.characters.Krystal;
import entities.characters.Mewtwo;
import physics.CollisionInfo;
import physics.PhysicsModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.CharacterActionFactory;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

public class MewtwoDisable extends Projectile {

	private float counter;
	
    public MewtwoDisable(Mewtwo object) {
        super(object, 0, 0, 0, true);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.scale = 1.2f;
        this.shouldDieOnCollision = false;
        counter = 0.3f;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.hasCollidedWithObstacle(true) || this.hasCollidedWithObstacle(false)) {
            this.setDead(true);
        }
        
        this.setXVelocity(this.getFacing() ? 60 : -60);
        
        counter -= Utilities.lockedTPF;
    	if (counter <= 0){
    		this.setDead(true);
    	}
    }
    
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
    	super.onCollideWith(entity, info);
    	if (entity instanceof PlayableCharacter && facing != entity.getFacing()){
    		if (entity.getCurrentAction().equals(CharacterActionFactory.getShieldBreak(entity, (short) 1)) || !entity.isOnGround() || entity.getCurrentAction().equals(CharacterActionFactory.getHurt(entity, (short) 1, 0f, new Angle(0)))){
    			this.setAttackCapability(Mewtwo.DMG[7], Mewtwo.KNBK[7], Mewtwo.PRTY[7], true);
    			counter = Utilities.lockedTPF*2;
    		} else {
    			((PlayableCharacter) entity).forceBreakShield(Mewtwo.DMG[7], Mewtwo.KNBK[7]);
    			this.setDead(true);
    		}
    	}
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "disableRay", StayingPower.NONE, Mewtwo.ISPD[7]).addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1)));
        return actionMap;
    }
}
