package entities.projectiles;

import physics.PhysicsModifier;
import engine.utility.Utilities;
import entities.characters.Link;
import entities.characters.ProfessorLayton;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

public class ProfessorLaytonUmbrella extends Projectile {

	private float xSpd;
	
    public ProfessorLaytonUmbrella(ProfessorLayton object) {
        super(object, ProfessorLayton.DMG[6], ProfessorLayton.KNBK[6], ProfessorLayton.PRTY[6], false, ProfessorLayton.SECT[6]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.shouldDieOnCollision = false;
        xSpd = object.getVelocity().x;
        this.scale = object.getScale();
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        this.setPureYVelocity(10);
        this.setXVelocity(xSpd);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "umbrella", StayingPower.NONE, ProfessorLayton.ISPD[6]).addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1)));
        return actionMap;
    }
}
