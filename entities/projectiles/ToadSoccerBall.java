package entities.projectiles;

import engine.utility.Utilities;
import entities.characters.Toad;
import entities.characters.actions.ActionCompleteNumberOfTicks;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

import physics.PhysicsModifier;

public class ToadSoccerBall extends Projectile {
    private float bounceNumber;
    private boolean init;
    private Vector2f vel;
    private float delay;

    public ToadSoccerBall(Toad object) {
        super(object, Toad.DMG[4], Toad.KNBK[4], Toad.PRTY[4], true, Toad.SECT[4]);
        this.shouldDieOnCollision = false;
        this.scale = 0.78f;
        this.init = false;
        vel = new Vector2f(40, 10*Utilities.getGeneralUtility().getBooleanAsSign(object.getFacing()));
        this.delay = 0;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (!init){
        	init = true;
        	
        	this.setPureYVelocity(vel.x);
        	this.setPureXVelocity(vel.y);
        }
        
        this.delay += Utilities.lockedTPF;
        this.facing = true;
        rotateUpTo(Utilities
				.getPhysicsUtility().calculateCorrectAngle(getVelocity(),Vector2f.ZERO));
        
        if(this.hasCollidedWithObstacle(false) && this.delay > 0.1) {
        	vel = vel.mult(0.8f);
    		//this.addToPosition(0, vel.y*2);
            //this.setPureYVelocity(vel.y);
        	this.delay = 0f;
    		this.init = false;
            bounceNumber++;
    	}
        
        if (this.isOnGround()){
        	this.init = false;
        	if (this.delay > 0.1){
        		vel = vel.mult(0.8f);
        		bounceNumber++;
        	}
        	this.delay = 0f;
        }
        
        if (currentAction.equals(actionMap.get("OnlyAction"))){
        	this.setMeteor(true);
        	this.setEnergy(true);
        	if (bounceNumber >= 2){
        		setCurrentAction(actionMap.get("OtherAction"));
        	}
        } else {
        	this.setMeteor(false);
        	this.setEnergy(false);
        	if (bounceNumber >= 4){
        		setDead(true);
        	}
        }
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "soccerBallFlaming", StayingPower.NONE, Toad.ISPD[4]).addModifier(new ActionPlaySound("Common Sounds/blam_00.ogg", -1)));
        actionMap.put("OtherAction", new CharacterAction(this, "soccerBall", StayingPower.LOW, Toad.ISPD[4]).addModifier(new ActionCompleteNumberOfTicks(Integer.MAX_VALUE)).addModifier(new ActionPlaySound("Common Sounds/hit_14.ogg", -1)));
        return actionMap;
    }
}
