package entities.projectiles;

import engine.Angle;
import entities.PlayableCharacter;
import entities.characters.Ridley;
import physics.PhysicsModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

public class RidleySmashFireball extends Projectile {
	
	boolean init;
	
    public RidleySmashFireball(Ridley object) {
        super(object, Ridley.DMG[8], Ridley.KNBK[8], Ridley.PRTY[8], true, Ridley.SECT[8]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.scale = object.getScale()*2;
        this.shouldDieOnCollision = false;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (!init){
        	init = true;
        	Vector2f center = shooter.getPosition();
            Vector2f tween = getPosition().subtract(center).mult((1+scale/2f)/2f);
            this.setPosition(center.add(tween).x, center.add(tween).y);
        }
        
        /*if (this.hasCollidedWithObstacle(true) || this.hasCollidedWithObstacle(false)) {
            this.setDead(true);
        }*/
        
        this.setXVelocity(this.getFacing() ? 80 : -80);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "metaRidleyFireball", StayingPower.NONE, Ridley.ISPD[8]).addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", -1)));
        return actionMap;
    }
}
