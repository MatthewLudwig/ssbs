package entities.projectiles;

import engine.utility.Utilities;
import entities.characters.Kamek;
import entities.characters.actions.ActionCompleteNumberOfAnimations;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class KamekFireShot extends Projectile {
    private float counter;
    private boolean transformed;
    
    public KamekFireShot(Kamek object) {
        super(object, 0, 0, 0, true);
        this.counter = 0;
        this.transformed = false;
        this.shouldDieOnCollision = false;
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.transformed) {
            if (this.counter < 1) {
                this.counter += Utilities.lockedTPF;
            } else {
                this.setDead(true);
            }
        } else {
            if (counter < .33f) {
                if (this.hasCollided() || this.hasCollidedWithObstacle(true) || this.hasCollidedWithObstacle(false)) {
                    this.transform();
                } else {
                    this.setXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.facing) * 72f);
                    this.counter += Utilities.lockedTPF;
                }
            } else {
                this.transform();
            }
        }
    }
    
    public void transform() {
        this.setAttackCapability(Kamek.DMG[5], Kamek.KNBK[5], Kamek.PRTY[5], false, Kamek.SECT[5]);
        this.setPosition(getPosition().x - getVelocity().x/8f, getPosition().y-24);
        this.setXVelocity(0);
        this.setCurrentAction(this.actionMap.get("SecondAction"));
        this.transformed = true;
        this.counter = 0;
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "fireShot", StayingPower.NONE, Kamek.ISPD[5]));
        actionMap.put("SecondAction", new CharacterAction(this, "fire", StayingPower.BARELY, Kamek.ISPD[5]).addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", -1)).addModifier(new ActionCompleteNumberOfAnimations(100)));
        return actionMap;
    }
}
