package entities.projectiles;

import engine.utility.Utilities;
import physics.PhysicsModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.general.DrWily;

import java.util.HashMap;
import java.util.Map;

public class DrWilyDownwardBlast extends Projectile {

    private float timer;
    
    public DrWilyDownwardBlast(DrWily object) {
        super(object, DrWily.DMG[1], DrWily.KNBK[3], DrWily.PRTY[2], true, DrWily.SECT[2]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.scale = 1.27f;
        this.timer = 0;
        this.shouldDieOnCollision = false;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.timer > .75) {
            this.setDead(true);
        }
        
        this.timer += Utilities.lockedTPF;
        
        this.setPureYVelocity(-25);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "shot1", StayingPower.NONE, DrWily.ISPD[2]).addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1)));
        return actionMap;
    }
}
