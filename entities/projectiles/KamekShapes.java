package entities.projectiles;

import engine.utility.Utilities;
import entities.characters.Kamek;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class KamekShapes extends Projectile {
    private float speed;
    
    public KamekShapes(Kamek object) {
        super(object, Kamek.DMG[4], Kamek.KNBK[4], Kamek.PRTY[4], true, Kamek.SECT[4]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
    }
    
    public void setChargeTime(float chargeTime) {
        float totalCharge = Math.min(chargeTime, 2) * 4;
        this.setAttackCapability(Kamek.DMG[4] * totalCharge/8f, Kamek.KNBK[4] * totalCharge/8f, Kamek.PRTY[4] * totalCharge/8f, false, Kamek.SECT[4]);
        this.speed = 2.4f + (6f * totalCharge);
        this.imageSpeed = .4f + chargeTime;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        this.setXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.facing) * this.speed);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "shapes", StayingPower.NONE, Kamek.ISPD[4]).addModifier(new ActionPlaySound("/Common Sounds/electric_01.ogg", -1)));
        return actionMap;
    }
}
