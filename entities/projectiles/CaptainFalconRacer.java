package entities.projectiles;

import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.characters.CaptainFalcon;
import physics.PhysicsModifier;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

public class CaptainFalconRacer extends Projectile {
    
    private int racerNumber;

    public CaptainFalconRacer(CaptainFalcon object, int number) {
        super(object, CaptainFalcon.DMG[8], CaptainFalcon.KNBK[8], CaptainFalcon.PRTY[8], false);
        this.racerNumber = number;
        this.shouldDieOnCollision = false;
        
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
    }
    
    @Override
    public boolean reflect(PhysicsEntity reflector) {return false;}

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        this.setXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.facing) * 40 * (2 + 3 * (this.racerNumber / 19)));
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "fZeroRacers", StayingPower.NONE, CaptainFalcon.ISPD[8]).addModifier(new CustomAction()));
        return actionMap;
    }
    
    public class CustomAction extends ActionModifier {
        public CustomAction() {
            
        }

        @Override
        public void onInit() {}

        @Override
        public boolean onUpdate() {
            this.action.setCurrentFrame(((CaptainFalconRacer) this.action.entity).racerNumber);
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
        }
    }
}
