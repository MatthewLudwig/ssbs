package entities.projectiles;

import entities.PhysicsEntity;
import entities.characters.Samus;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.CollisionInfo;

public class SamusBomb extends Projectile {
    private int timer;
    private boolean shouldBeDead;

    public SamusBomb(Samus object) {
        super(object, Samus.DMG[7], Samus.KNBK[7], Samus.PRTY[7], true, Samus.SECT[7]);
        this.timer = 0;
        this.shouldBeDead = false;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        this.timer++;
    }
    
    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
        if (this.timer > 100) {
            super.onCollideWith(entity, info);
            this.setDead(this.shouldBeDead);
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "bomb", StayingPower.NONE, Samus.ISPD[7]).addModifier(new CustomAction()).addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", -1)));
        return actionMap;
    }
    
    public class CustomAction extends ActionModifier {
        public CustomAction() {
            
        }

        @Override
        public void onInit() {}

        @Override
        public boolean onUpdate() {
            if (((SamusBomb) this.action.entity).timer < 100) {
                this.action.setCurrentFrame(0);
            }

            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
            ((PhysicsEntity) this.action.entity).setDead(true);
            ((SamusBomb) this.action.entity).shouldBeDead = true;
        }
    }
}
