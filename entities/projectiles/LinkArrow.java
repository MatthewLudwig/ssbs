package entities.projectiles;

import physics.PhysicsModifier;
import entities.characters.Link;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

public class LinkArrow extends Projectile {

    public LinkArrow(Link object) {
        super(object, Link.DMG[4], Link.KNBK[4], Link.PRTY[4], false, Link.SECT[4]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
                
        this.setXVelocity(this.getFacing() ? 20 : -20);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "arrow", StayingPower.NONE, Link.ISPD[4]).addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1)));
        return actionMap;
    }
}
