package entities.projectiles;

import entities.PhysicsEntity;
import physics.PhysicsModifier;
import entities.characters.Link;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

public class LinkBiggoronArrow extends Projectile {

    public LinkBiggoronArrow(Link object) {
        super(object, Link.DMG[8], Link.KNBK[8], Link.PRTY[8], false, Link.SECT[8]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.shouldDieOnCollision = false;
    }
    
    @Override
    public boolean reflect(PhysicsEntity reflector) {return false;}

    @Override
    public void updateEntity()
    {                                
        super.updateEntity();
        
        this.setXVelocity(this.getFacing() ? 20 : -20);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "biggoronArrow", StayingPower.NONE, Link.ISPD[8]).addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1)));
        return actionMap;
    }
}
