package entities.projectiles;

import physics.PhysicsModifier;
import engine.utility.Utilities;
import entities.ViolentEntity;
import entities.characters.Geno;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

public class GenoWhirl extends Projectile {
	
	private int counter;
	private Geno object;

    public GenoWhirl(Geno object) {
        super(object, Geno.DMG[5], Geno.KNBK[5], Geno.PRTY[5], true);
        this.object = object;
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        counter = 0;
    }

    @Override
    public void updateEntity() {
        super.updateEntity();
                
        this.setXVelocity(this.getFacing() ? 20 : -20);
        counter++;
        if (counter > 0.5/Utilities.lockedTPF){
        	this.setDead(true);
        }        	
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        CharacterAction action = new CharacterAction(this, "whirl", StayingPower.NONE, Geno.ISPD[5]).addModifier(new ActionPlaySound("/Common Sounds/hit_13.ogg", -1));
        action.addModifier(new OnlyAction());
        actionMap.put("OnlyAction", action);
        return actionMap;
    }
    
    public class OnlyAction extends ActionModifier {
    	private int numHits;
    	
        public OnlyAction() {
        
        }

        @Override
        protected void onInit() {
            numHits = 0;
        }

        @Override
        protected boolean onUpdate() {
        	if (((ViolentEntity) this.action.entity).wasCollisionSuccesful() && ((Projectile)this.action.entity).shooter == object){
        		numHits++;
        		if (numHits == 1){
                	object.setSide();
                }
        	}
            return false;
        }

        @Override
        protected boolean shouldFinish() {
            return true;
        }

        @Override
        protected void cleanUp() {
        }
    }
}
