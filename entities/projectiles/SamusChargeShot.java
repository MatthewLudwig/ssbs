package entities.projectiles;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;
import entities.characters.Samus;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class SamusChargeShot extends Projectile {
    public boolean fired;

    public SamusChargeShot(Samus object) {
        super(object, 0, 0, 0, false);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.scale = .15f;
        this.fired = false;
        this.setAttackCapability((Samus.DMG[4] * this.scale), (Samus.KNBK[4] * this.scale), (Samus.PRTY[4] * this.scale), true, Samus.SECT[4]);
    }

    public void setFired() {
        this.fired = true;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        this.shouldDieOnCollision = this.fired;
        
        if (!this.fired) {
            float x, y = this.shooter.getAppropriateBounds().getExactCenter().y - 11;
                 
            if(this.getFacing()) {
                x = this.shooter.getAppropriateBounds().getRightMidpoint().x + (this.getAppropriateBounds().getDimensions().x / 2) + 5;
            } else {
                x = this.shooter.getAppropriateBounds().getLeftMidpoint().x - (this.getAppropriateBounds().getDimensions().x / 2) - 5;
            }
            
            this.setPosition(x, y);
        } else {
            this.setPureXVelocity(this.getFacing() ? 20 : -20);
        }
        
        this.setAttackCapability((Samus.DMG[4] * this.scale), (Samus.KNBK[4] * this.scale), (Samus.PRTY[4] * this.scale), true, Samus.SECT[4]);
        
        if (this.hasCollidedWithObstacle(true) || this.hasCollidedWithObstacle(false)) {
            this.setDead(true);
        }
    }
    
    /*public final void setAttackCapability(float damageDealt, float knockback, float priority) {
        super.setAttackCapability(damageDealt * 1.54f, knockback * .86f, priority * .5f, true);
    }*/

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "chargeShot", StayingPower.NONE, Samus.ISPD[4]).addModifier(new CustomAction()).addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1)));
        return actionMap;
    }
    
    public class CustomAction extends ActionModifier {

        public CustomAction() {
            
        }

        @Override
        public void onInit() {}

        @Override
        public boolean onUpdate() {
            this.action.setCurrentFrame(0);

            if (!SamusChargeShot.this.fired) {
                SamusChargeShot.this.setScale((float) Math.min(SamusChargeShot.this.scale + .005f, 1f));
            }

            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
}
