package entities.projectiles;

import engine.utility.Utilities;
import physics.PhysicsModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.general.DrWily;

import java.util.HashMap;
import java.util.Map;

public class DrWilyForwardBlast extends Projectile {

    private float timer;
    
    public DrWilyForwardBlast(DrWily object) {
        super(object, DrWily.DMG[4], DrWily.KNBK[5], DrWily.PRTY[1], true, DrWily.SECT[1]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.scale = 1.27f;
        this.timer = 0;
        this.shouldDieOnCollision = false;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.timer > .75) {
            this.setDead(true);
        }
        
        this.timer += Utilities.lockedTPF;
        
        this.setXVelocity(this.getFacing() ? 30 : -30);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "shot2", StayingPower.NONE, DrWily.ISPD[1]).addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1)));
        return actionMap;
    }
}
