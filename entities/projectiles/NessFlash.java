package entities.projectiles;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;
import engine.Angle;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import entities.characters.Kirby;
import entities.characters.Ness;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.ActionToggleAttribute;
import entities.characters.actions.ActionToggleAttribute.Attribute;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class NessFlash extends Projectile {
	private float lifeTimer;
	private Angle angle;
	private Ness object;
	boolean act;

	public NessFlash(Ness object) {
		super(object, 0f, 0f, 0f, true);
		this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
		//this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
		this.shouldDieOnCollision = false;
		this.scale = .72f;
		this.lifeTimer = 0;
		if (this.getFacing())
			angle = new Angle(0);
		else
			angle = new Angle(180);
		this.object = object;
		act = false;
	}

	@Override
	public void updateEntity() {
		super.updateEntity();

		if (!act) {
			if (this.getFacing())
				this.setVelocity(30 * angle.cos(), 30 * angle.sin(), false);
			else
				this.setVelocity(-30 * angle.cos(), -30 * angle.sin(), false);

			boolean up = this.object.checkControl("Up", false);
			boolean down = this.object.checkControl("Down", false);
			if (!this.getFacing()) {
				up = !up;
				down = !down;
			}
			if (up && !down) {
				angle.add(45 * Utilities.lockedTPF);
			} else if (down && !up) {
				angle.add(-45 * Utilities.lockedTPF);
			}

			if (this.lifeTimer >= 1f
					|| !this.object.checkControl("Special", false) || hasCollidedWithObstacle(true) || hasCollidedWithObstacle(false)) {
				this.setCurrentAction(actionMap.get("OtherAction"));
				this.setAttackCapability(Ness.DMG[5] * this.lifeTimer, Ness.KNBK[5] * this.lifeTimer, Ness.PRTY[5] * this.lifeTimer, true, Ness.SECT[5]);
				act = true;
			} else {
				this.lifeTimer += Utilities.lockedTPF;
			}
		} else {
			this.setVelocity(0, 0, false);
		}
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("OnlyAction", new CharacterAction(this, "flash",
				StayingPower.NONE, Ness.ISPD[5]));
		actionMap.put(
				"OtherAction",
				new CharacterAction(this, "flash2", StayingPower.LOW, Ness.ISPD[5])
						.addModifier(
								new ActionPlaySound(
										"/Common Sounds/hit_05.ogg", -1))
						.addModifier(
								new ActionToggleAttribute(Attribute.PARALYZE))
						.addModifier(new ActionKillEntity()));
		return actionMap;
	}
}
