package entities.projectiles;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;
import physics.PhysicsModifier;
import entities.PhysicsEntity;
import entities.characters.MegaMan;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class MegaManLaser extends Projectile {
    public MegaManLaser(MegaMan object) {
        super(object, 0, 0, 0, true);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
    }
    
    @Override
    public boolean reflect(PhysicsEntity reflector) {return false;}

    @Override
    public void updateEntity() {                                
        super.updateEntity();
    }
    
    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "finalSmashLaser", StayingPower.NONE, MegaMan.ISPD[8]));
        return actionMap;
    }
}
