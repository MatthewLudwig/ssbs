package entities.projectiles;

import engine.utility.Utilities;
import entities.characters.Kamek;
import entities.characters.actions.ActionCompleteNumberOfAnimations;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class KamekSpikeballShot extends Projectile {
    private float counter;
    private boolean transformed;

    public KamekSpikeballShot(Kamek object) {
        super(object, 0, 0, 0, false);
        this.counter = 0;
        this.transformed = false;
        this.shouldDieOnCollision = false;
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.transformed) {
            if (this.counter < 2) {
                this.setYVelocity(-18f, false);
                this.counter += Utilities.lockedTPF;
            } else {
                this.setDead(true);
            }
        } else {
            if (counter < .25f) {
                if (this.hasCollided() || this.hasCollidedWithObstacle(true) || this.hasCollidedWithObstacle(false)) {
                    this.transform();
                } else {
                    this.setXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.facing) * 24f);
                    this.counter += Utilities.lockedTPF;
                }
            } else {
                this.transform();
            }
        }
    }
    
    public void transform() {
        this.setAttackCapability(Kamek.DMG[7], Kamek.KNBK[7], Kamek.PRTY[7], false, Kamek.SECT[7]);
        this.setXVelocity(0);
        this.setCurrentAction(this.actionMap.get("SecondAction"));
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.transformed = true;
        this.counter = 0;
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "spikeballShot", StayingPower.NONE, Kamek.ISPD[7]));
        actionMap.put("SecondAction", new CharacterAction(this, "spikeball", StayingPower.BARELY, Kamek.ISPD[7]).addModifier(new ActionPlaySound("/Common Sounds/hit_10.ogg", -1)).addModifier(new ActionCompleteNumberOfAnimations(100)));
        return actionMap;
    }
}
