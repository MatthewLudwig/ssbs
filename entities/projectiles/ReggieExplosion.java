package entities.projectiles;

import engine.utility.Utilities;
import entities.characters.Reggie;
import physics.PhysicsModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

public class ReggieExplosion extends Projectile {
    
	private float limit, counter;
    public ReggieExplosion(Reggie object) {
        super("Explosions", object, Reggie.DMG[8], Reggie.KNBK[8], Reggie.PRTY[8], true);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.shouldDieOnCollision = false;
        this.limit = 1.2f;
        this.counter = 0;
    }
    
    public void updateEntity() {                                
        super.updateEntity();

        if(this.counter < 5.5f) {
            this.counter += Utilities.lockedTPF*this.limit;
        } else {
            this.setDead(true);
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "explosion", StayingPower.NONE, 1.2f).addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1)));
        return actionMap;
    }
}
