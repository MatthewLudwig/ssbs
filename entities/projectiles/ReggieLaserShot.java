package entities.projectiles;

import engine.utility.Utilities;
import entities.characters.Reggie;
import entities.characters.actions.ActionCompleteNumberOfAnimations;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class ReggieLaserShot extends Projectile {
    private float counter;
    private boolean transformed;
    
    public ReggieLaserShot(Reggie object) {
        super(object, 0, 0, 0, true);
        this.counter = 0;
        this.transformed = false;
        this.shouldDieOnCollision = false;
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.transformed) {
            if (this.counter < 1) {
                this.counter += Utilities.lockedTPF;
            } else {
                this.setDead(true);
            }
        } else {
            if (counter < .33f) {
                if (this.hasCollided() || this.hasCollidedWithObstacle(true) || this.hasCollidedWithObstacle(false)) {
                    this.transform();
                } else {
                    this.setXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.facing) * 72f);
                    this.counter += Utilities.lockedTPF;
                }
            } else {
                this.transform();
            }
        }
    }
    
    public void transform() {
        this.setAttackCapability(Reggie.DMG[8]/17f, Reggie.KNBK[8]/17f, Reggie.PRTY[8]/17f, false);
        this.setXVelocity(0);
        this.setCurrentAction(this.actionMap.get("SecondAction"));
        this.transformed = true;
        this.counter = 0;
        this.position.y -= 20;
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "laserShot", StayingPower.NONE, .99f));
        actionMap.put("SecondAction", new CharacterAction(this, "fire", StayingPower.BARELY, .99f).addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", -1)).addModifier(new ActionCompleteNumberOfAnimations(25)));
        return actionMap;
    }
}
