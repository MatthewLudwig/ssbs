package entities.projectiles;

import engine.utility.Utilities;
import entities.characters.MegaMan;
import physics.PhysicsModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

public class MegaManPellet extends Projectile {

    private float timer;
    
    public MegaManPellet(MegaMan object) {
        super(object, MegaMan.DMG[0], MegaMan.KNBK[0], MegaMan.PRTY[0], false, MegaMan.SECT[0]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        
        this.timer = 0;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.timer > .75*0.7f || this.hasCollidedWithObstacle(true) || this.hasCollidedWithObstacle(false)) {
            this.setDead(true);
        }
        
        this.timer += Utilities.lockedTPF;
        
        this.setXVelocity(this.getFacing() ? 55*0.7f : -55*0.7f);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "bullet", StayingPower.NONE, MegaMan.ISPD[0]).addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1)));
        return actionMap;
    }
}
