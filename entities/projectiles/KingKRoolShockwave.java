package entities.projectiles;

import engine.utility.Utilities;
import entities.characters.KingKRool;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

public class KingKRoolShockwave extends Projectile {
    private float lifeTimer;
    
    public KingKRoolShockwave(KingKRool object) {
        super(object, 8.72f, 1.15f, 3.90f, true);
        this.lifeTimer = 0;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        this.setXVelocity(this.getFacing() ? 54 : -54);
        
        if (this.lifeTimer >= .333) {
            this.setDead(true);
        } else {
            this.lifeTimer += Utilities.lockedTPF;
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "shockwave", StayingPower.NONE, 2.14f).addModifier(new ActionPlaySound("/Common Sounds/hit_14.ogg", -1)));
        return actionMap;
    }
}
