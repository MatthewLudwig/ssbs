package entities.projectiles;

import engine.utility.Utilities;
import entities.characters.KingKRool;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

public class KingKRoolKannonball extends Projectile {
    private float limit;
    private float counter;

    public KingKRoolKannonball(KingKRool object) {
        super(object, KingKRool.DMG[4], KingKRool.KNBK[4], KingKRool.PRTY[4], false, KingKRool.SECT[4]);
        this.shouldDieOnCollision = false;
        this.limit = 3f;
        this.counter = 0;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        this.setXVelocity(this.getFacing() ? 6.25f : -6.25f);

        if(this.isOnGround()) {
            this.addToPosition(0, 1);
            this.setYVelocity(30, false);
        } /*else if (this.hasCollided()) {
            this.addToPosition(0, 1);
            this.setYVelocity(30, false);
            this.limit = .75f;
            this.counter = 0;
        }*/

        if(this.counter < this.limit) {
            this.counter += Utilities.lockedTPF;
        } else {
            this.setDead(true);
        }
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "kannonball", StayingPower.NONE, KingKRool.ISPD[4]).addModifier(new ActionPlaySound("/Common Sounds/hit_15.ogg", -1)));
        return actionMap;
    }
}
