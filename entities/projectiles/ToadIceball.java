package entities.projectiles;

import engine.utility.Utilities;
import entities.characters.Toad;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class ToadIceball extends Projectile {
    private float counter;
    private boolean shouldKillIceball;

    public ToadIceball(Toad object) {
        super(object, Toad.DMG[4], Toad.KNBK[4], Toad.PRTY[4], false, Toad.SECT[4]);
        this.shouldDieOnCollision = false;
        this.counter = 0;
        this.shouldKillIceball = false;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        this.setFreeze(true);
        
        if (this.hasCollided()) {
            this.shouldKillIceball = true;
        }
        
        if (this.shouldKillIceball && !this.justReflected) {
            this.setDead(true);
        } else {
            this.shouldKillIceball = false;
        }
        
        if (this.currentAction == this.actionMap.get("OnDestroy")) {
            return;
        }

        this.setXVelocity(this.getFacing() ? 12.5f : -12.5f);

        if(this.isOnGround()) {
            this.addToPosition(0, 1);
            this.setYVelocity(15, false);
        }

        if(!this.hasCollidedWithObstacle(true) && this.counter < .75) {
            this.counter += Utilities.lockedTPF;
        } else {
            this.setDead(true);
        }
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "iceBall", StayingPower.NONE, Toad.ISPD[4]).addModifier(new ActionPlaySound("Common Sounds/freeze.ogg", -1)));
        return actionMap;
    }
}
