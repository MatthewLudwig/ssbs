package entities.projectiles;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;
import physics.PhysicsModifier;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.Team;
import entities.characters.MegaMan;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class MegaManCrashBomb extends Projectile {

    private float timer;
    private int state;
    private PlayableCharacter character;
    
    private float cooldown;
    private boolean hasCausedDamage;
    
    public MegaManCrashBomb(MegaMan object) {
        super(object, 0, 0, 0, false);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.shouldDieOnCollision = false;
        
        this.timer = 0;
        this.state = 0;
        this.cooldown = 1f;
        this.hasCausedDamage = false;
    }
    
    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
        super.onCollideWith(entity, info);
                
        if (this.state != 2) {
        	if (entity instanceof PlayableCharacter && !entity.equals(character) && this.cooldown <= 0) {
        		if (this.character == null) {
        			this.timer = 0;
        			this.state++;
        		}
        		
        		this.character = (PlayableCharacter) entity;
        		this.shooter = null;
        		this.team = Team.NONE;
        		this.velocity.set(0, 0);
        		this.cooldown = 1f;
        	}
        } else {
        	this.hasCausedDamage = true;
        }
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        this.cooldown -= Utilities.lockedTPF * 10;
        
        switch (this.state) {
            case 0:
                if (this.timer > 1.2f) {
                    this.setDead(true);
                }
                
                this.setXVelocity(this.getFacing() ? 60 : -60);
                break;
            case 1:
            	if (this.timer > 3f) {
            		this.state++;
            		this.setAttackCapability(MegaMan.DMG[5], MegaMan.KNBK[5], MegaMan.ISPD[5], true, MegaMan.SECT[5]);
            	}
            case 2:
            case 3:
            	this.setPosition(this.character.getAppropriateBounds().getLowerMidpoint().x, this.character.getAppropriateBounds().getLowerMidpoint().y);
            	this.display.getChild("Picture").setLocalTranslation(
            			this.display.getChild("Picture").getLocalTranslation().x, 
            			this.display.getChild("Picture").getLocalTranslation().y, 
            			5);
                break;
        }
        
        if (this.hasCausedDamage) {
        	this.setAttackCapability(0, 0, 0, false);
        }
        
        this.timer += Utilities.lockedTPF;
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "crashBomb", StayingPower.NONE, MegaMan.ISPD[5]).addModifier(new ActionKillEntity()).addModifier(new CustomAction()).addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1)));
        return actionMap;
    }
    
    public class CustomAction extends ActionModifier {
        
    	private boolean done;
    	
        public CustomAction() {
            
        }

        @Override
        public void onInit() {
        	this.done = false;
        }

        @Override
        public boolean onUpdate() {
            if (this.action.getCurrentFrameNumber() == 2) {
                this.action.setCurrentFrame(0);
                
                if (this.action.entity.hasCollided()) {
                    this.action.setCurrentFrame(2);
                    this.action.freezeFrame(true);
                }
            } 
            
            if (((MegaManCrashBomb) this.action.entity).state == 2 && !this.done) {
            	this.action.freezeFrame(false);
                this.action.setCurrentFrame(3);
                this.done = true;
            }
            
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
}
