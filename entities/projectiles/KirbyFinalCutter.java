package entities.projectiles;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;
import engine.utility.Utilities;
import entities.characters.Kirby;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class KirbyFinalCutter extends Projectile {
    private float lifeTimer;
    
    public KirbyFinalCutter(Kirby object) {
        super(object, Kirby.DMG[6], Kirby.KNBK[6], Kirby.PRTY[6], true, Kirby.SECT[6]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.lifeTimer = 0;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        this.setXVelocity(this.getFacing() ? 54 : -54);
        
        if (this.lifeTimer >= .333) {
            this.setDead(true);
        } else {
            this.lifeTimer += Utilities.lockedTPF;
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "finalCutter", StayingPower.NONE, Kirby.ISPD[6]).addModifier(new ActionPlaySound("/Common Sounds/hit_14.ogg", -1)));
        return actionMap;
    }
}
