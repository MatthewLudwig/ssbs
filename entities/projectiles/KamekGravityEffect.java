package entities.projectiles;

import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.ui.Picture;

import engine.Main;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import entities.characters.Kamek;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import physics.PhysicsModifier;

public class KamekGravityEffect extends Projectile {
    private static Random generator = new Random();
    
    private float counter;
    private float timer;
    private PlayableCharacter target;
    private int effect;
    private Vector2f gravityDirection;
    
    public KamekGravityEffect(Kamek object, PlayableCharacter target) {
        super(object, 0, 0, 0, true);
        this.target = target;
        this.effect = generator.nextInt(4);
        this.gravityDirection = new Vector2f(0, -1);
        
        this.counter = 10;
        this.timer = 0;
        this.shouldDieOnCollision = false;
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        this.display.setLocalTranslation(this.display.getLocalTranslation().x, this.display.getLocalTranslation().y, 10);
        
        Material mat = ((Picture) this.display.getChild("Picture")).getMaterial();
        mat.setColor("Color", new ColorRGBA(1, 1, 1, .5f));
        this.display.getChild("Picture").setMaterial(mat);
        
        this.setPosition(this.target.getAppropriateBounds().getLowerMidpoint().x, 
                this.target.getAppropriateBounds().getLowerMidpoint().y - (this.getDimensions().y / 2) + (this.target.getAppropriateBounds().getDimensions().y / 2));
        
        if (this.counter <= 0) {
            this.setDead(true);
        } else {
            this.counter -= Utilities.lockedTPF;
            
            switch (this.effect) {
                case 0:
                    this.target.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
                    break;
                case 1:
                    this.target.gravityModifier = 0.2f;
                    break;
                case 2:
                    this.target.gravityModifier = 2.5f;
                    break;
                case 3:
                    this.target.addPhysicsModifier(PhysicsModifier.NOGRAVITY);

                    if (this.timer <= 0) {
                        double angle = generator.nextFloat() * 2 * Math.PI;
                        this.gravityDirection.set((float) Math.cos(angle), (float) Math.sin(angle));
                        this.timer += 1;
                    } else {
                        this.timer -= Utilities.lockedTPF;
                    }
                    
                    float gravitationalPull = -58.45f * Main.getGameState().getWorld().getStage().getGravity() * Utilities.lockedTPF;
                    this.target.addToVelocity(this.gravityDirection.x * gravitationalPull, this.gravityDirection.y * gravitationalPull, false);
                    break;
                default:
                    System.err.println("Unknown effect ID used!");
            }
        }
    }
    
    @Override
    public void onDeath() {
        super.onDeath();
        
        switch (this.effect) {
            case 0:
                this.target.removePhysicsModifier(PhysicsModifier.NOGRAVITY);
                break;
            case 1:
            case 2:
                this.target.gravityModifier = 1f;
                break;
            case 3:
                this.target.removePhysicsModifier(PhysicsModifier.NOGRAVITY);
                break;
            default:
                System.err.println("Unknown effect ID used!");
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "finalSmashEffect" + (this.effect + 1), StayingPower.NONE, .42f));
        return actionMap;
    }
}
