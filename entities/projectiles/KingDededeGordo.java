package entities.projectiles;

import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.ViolentEntity;
import entities.characters.KingDedede;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.CollisionInfo;

public class KingDededeGordo extends Projectile {
    private float counter;

    public KingDededeGordo(KingDedede object) {
        super(object, KingDedede.DMG[5], KingDedede.KNBK[5], KingDedede.PRTY[5], false);
        this.counter = 0;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        this.setXVelocity(this.getFacing() ? 20 : -20);

        if (this.isOnGround()) {
            this.addToPosition(0, 1);
            this.setYVelocity(15, false);
        }

        if(this.counter < 1.25) {
            this.counter += Utilities.lockedTPF;
        } else {
            this.setDead(true);
        }
    }
    
    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
        if (!this.justReflected && !entity.equals(this.shooter)) {
            if (entity instanceof ViolentEntity) {
                if (!((ViolentEntity) entity).isOnSameTeamAs(this) && ((ViolentEntity) entity).getKnockback() == 0) {
                    super.onCollideWith(entity, info);
                    
                    if (!((ViolentEntity) entity).canReflect()) {
                        this.shouldDieOnNextFrame = this.shouldDieOnCollision;
                    }
                } else if (entity instanceof Projectile) {
                    this.reflect(((Projectile) entity).shooter);
                } else {
                    this.reflect((ViolentEntity) entity);
                }
            } else {
                super.onCollideWith(entity, info);
                this.shouldDieOnNextFrame = this.shouldDieOnCollision;
            }
        } else {
            this.justReflected = false;
        }
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "gordo", StayingPower.NONE, KingDedede.ISPD[5]).addModifier(new ActionPlaySound("/Common Sounds/hit_02.ogg", -1)));
        return actionMap;
    }
}
