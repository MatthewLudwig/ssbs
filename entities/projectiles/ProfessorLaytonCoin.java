package entities.projectiles;

import physics.PhysicsModifier;
import engine.utility.Utilities;
import entities.characters.Link;
import entities.characters.ProfessorLayton;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

public class ProfessorLaytonCoin extends Projectile {

	private float timer;
	private float ySpd;
	
    public ProfessorLaytonCoin(ProfessorLayton object) {
        super(object, ProfessorLayton.DMG[4], ProfessorLayton.KNBK[4], ProfessorLayton.PRTY[4], false, ProfessorLayton.SECT[4]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        timer = 0;
        ySpd = (float) ((Math.random()-0.5)*30);
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        timer += Utilities.lockedTPF;
        if (timer > 0.4){
        	setDead(true);
        }
        this.setPureYVelocity(ySpd);
        this.setXVelocity(this.getFacing() ? 25 : -25);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "coin", StayingPower.NONE, ProfessorLayton.ISPD[4]).addModifier(new ActionPlaySound("/Common Sounds/coin.ogg", -1)));
        return actionMap;
    }
}
