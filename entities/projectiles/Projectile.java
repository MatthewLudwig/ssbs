package entities.projectiles;

import com.jme3.scene.Node;
import entities.PhysicsEntity;
import entities.PhysicsInfo;
import entities.ViolentEntity;
import physics.CollisionInfo;

/**
 *
 * @author Matthew
 */
public abstract class Projectile extends ViolentEntity {
    protected PhysicsEntity shooter;
    protected boolean justReflected;
    protected boolean shouldDieOnCollision;
    protected boolean shouldDieOnNextFrame;
    private boolean isEnergyProjectile;

    public Projectile(PhysicsEntity shooter, float damage, float knockback, float priority, boolean isEnergy) {
        this(shooter.getName(), shooter, damage, knockback, priority, isEnergy);
    }
    
    public Projectile(PhysicsEntity shooter, float damage, float knockback, float priority, boolean isEnergy, short[] sectionFrames) {
        this(shooter.getName(), shooter, damage, knockback, priority, isEnergy, sectionFrames);
    }
    
    public Projectile(String baseName, PhysicsEntity shooter, float damage, float knockback, float priority, boolean isEnergy) {
        this(baseName, shooter, damage, knockback, priority, isEnergy, new short[]{0});
    }
    
    public Projectile(String baseName, PhysicsEntity shooter, float damage, float knockback, float priority, boolean isEnergy, short[] sectionFrames) {
        super(baseName, 1, new PhysicsInfo(0, 1, 1, 1), 1, 1, 1);
        this.shooter = shooter;
        this.setHasSuperArmor(true);
        this.justReflected = false;
        this.shouldDieOnCollision = true;
        this.shouldDieOnNextFrame = false;
        
        this.team = ((ViolentEntity) this.shooter).team;
        
        this.setAttackCapability(damage, knockback, priority, isEnergy);
        isEnergyProjectile = isEnergy;
        
        this.sectionFrames = sectionFrames;
    }
    
    public boolean isEnergyProjectile() {
        return this.isEnergyProjectile;
    }
    
    protected void setEnergy(boolean b){
    	this.isEnergyProjectile = b;
    }
   
    @Override
    public void setUpEntity(int id, Node displayNode, Node menuNode, boolean alwaysDrawn) {
        super.setUpEntity(id, displayNode, menuNode, alwaysDrawn);
        if (shooter != null)
        	this.setFacing(this.shooter.getFacing());
    }
    
    @Override
    public void setFinalSmash(boolean state) {
        if (this.shooter instanceof ViolentEntity) {
            ((ViolentEntity) this.shooter).setFinalSmash(state);
        }
    }
    
    @Override
    public void onDeath() {
        
    }

    @Override
    protected final String getDefaultAction() {
        return "OnlyAction";
    }

    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
        if (!this.justReflected && !entity.equals(this.shooter)) {
            if (entity instanceof ViolentEntity) {
                if (!((ViolentEntity) entity).isOnSameTeamAs(this)) {
                    super.onCollideWith(entity, info);
                    
                    if (!((ViolentEntity) entity).canReflect()) {
                        this.shouldDieOnNextFrame = this.shouldDieOnCollision;
                    }
                }                
            } else {
                super.onCollideWith(entity, info);
                this.shouldDieOnNextFrame = this.shouldDieOnCollision;
            }
        } else {
            this.justReflected = false;
        }
    }
    
    @Override
    public void updateEntity() {     
        super.updateEntity();
        
        if (this.shouldDieOnNextFrame) {
            this.setDead(true);
        }
    }

    @Override
    public boolean reflect(PhysicsEntity reflecter) {   	
        if (this.shooter != null && !this.shooter.equals(reflecter)) {
            boolean returnValue = super.reflect(reflecter);
            this.shooter = reflecter;
            this.justReflected = true;
            this.team = ((ViolentEntity) reflecter).team;
            
            this.multiplyByReflect(((ViolentEntity) reflecter));
            
            return returnValue;
        }
        
        return false;
    }
}
