package entities.projectiles;

import physics.PhysicsModifier;
import entities.characters.Link;
import entities.characters.ProfessorLayton;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

public class ProfessorLaytonFinalHat extends Projectile {

    public ProfessorLaytonFinalHat(ProfessorLayton object) {
        super(object, ProfessorLayton.DMG[8], ProfessorLayton.KNBK[8], ProfessorLayton.PRTY[8], false, ProfessorLayton.SECT[8]);
        this.shouldDieOnCollision = false;
        this.scale = object.getScale()*1.2f;
        this.gravityModifier = 2;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
                
        this.setXVelocity(this.getFacing() ? 15 : -15);
        this.setMeteor(true);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "finalSmashHat", StayingPower.NONE, ProfessorLayton.ISPD[8]).addModifier(new ActionPlaySound("/Common Sounds/ping.ogg", -1)));
        return actionMap;
    }
}
