package entities.projectiles;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.ViolentEntity;
import entities.characters.KingDedede;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import physics.CollisionInfo;
import physics.PhysicsModifier;

public class KingDededeBreathStar extends Projectile {
    private static PlayableCharacter[] template = new PlayableCharacter[]{};
    
    private Angle angle;
    private float lifeTimer;
    private PhysicsEntity target;
    
    private List<PlayableCharacter> exclusionList;

    public KingDededeBreathStar(KingDedede shooter) {
        super(shooter, 0, 0, 0, true);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.angle = new Angle(KingDedede.gen.nextInt(360) - 90);
        this.facing = KingDedede.gen.nextBoolean();
        this.lifeTimer = 1;
        
        this.exclusionList = new ArrayList<PlayableCharacter>();
        this.exclusionList.add((PlayableCharacter) this.shooter);
    }
    
    @Override
    public void updateEntity() {
        super.updateEntity();
        
        this.angle.add(Utilities.getGeneralUtility().getBooleanAsSign(this.facing) * 60 * Utilities.lockedTPF);
        this.rotateUpTo(this.angle);
        
        if (this.lifeTimer <= 0) {
            this.setDead(true);
        } else {
            this.lifeTimer -= Utilities.lockedTPF;
        }
        
        this.target = Main.getGameState().getWorld().getClosestPlayer(this.getAppropriateBounds().getLowerMidpoint(), this.exclusionList.toArray(template));
        
        if (this.target != null && this.target.getAppropriateBounds().getLowerMidpoint().distance(this.getAppropriateBounds().getLowerMidpoint()) > 30) {
            this.target = null;
        } else if (this.target != null && this.target instanceof ViolentEntity && ((ViolentEntity) this.target).isOnSameTeamAs(this)) {
            this.target = null;
            this.exclusionList.add((PlayableCharacter) this.target);
        }
    }
    
    public PhysicsEntity getTarget() {
        PhysicsEntity chosenTarget = this.target;
        this.target = null;
        return chosenTarget;
    }
    
    @Override
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {}

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "breathStar", StayingPower.NONE, 1));
        return actionMap;
    }
}
