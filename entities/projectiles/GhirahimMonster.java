package entities.projectiles;

import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.characters.CaptainFalcon;
import entities.characters.Ghirahim;
import physics.PhysicsModifier;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

public class GhirahimMonster extends Projectile {
    
    private int racerNumber;

    public GhirahimMonster(Ghirahim object, int number) {
        super(object, 8.17f, 2.12f, 99.99f, false);
        this.racerNumber = number;
        this.shouldDieOnCollision = false;
        
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
    }
    
    @Override
    public boolean reflect(PhysicsEntity reflector) {return false;}

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        this.setXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.facing) * 48 * (2 + 3 * (this.racerNumber / 16)));
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "hyruleMonsters", StayingPower.NONE, 2.14f).addModifier(new CustomAction()));
        return actionMap;
    }
    
    public class CustomAction extends ActionModifier {
        public CustomAction() {
            
        }

        @Override
        public void onInit() {}

        @Override
        public boolean onUpdate() {
            this.action.setCurrentFrame(((GhirahimMonster) this.action.entity).racerNumber);
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
        }
    }
}
