package entities.projectiles;

import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.characters.Mario;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

public class MarioFinalFireball extends Projectile {
    private float jumpForce;
    private float speed;

    public MarioFinalFireball(Mario object) {
        super(object, Mario.DMG[8], Mario.KNBK[8], Mario.PRTY[8], true, Mario.SECT[8]);
        this.shouldDieOnCollision = false;
        this.jumpForce = 15f;
        this.speed = 12.5f;
    }
    
    @Override
    public boolean reflect(PhysicsEntity reflector) {return false;}

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.currentAction == this.actionMap.get("OnDestroy")) {
            return;
        }

        this.setXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.getFacing()) * this.speed);

        if(this.isOnGround()) {
            this.addToPosition(0, 1);
            this.setYVelocity(this.jumpForce, false);
            this.jumpForce *= 2;
        }
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "fireball", StayingPower.NONE, Mario.ISPD[8]));
        return actionMap;
    }
}
