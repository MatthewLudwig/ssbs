package entities.projectiles;

import com.jme3.scene.Node;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.Team;
import entities.characters.Link;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.CollisionInfo;
import physics.PhysicsModifier;

public class LinkBomb extends Projectile {
    private boolean shouldDie;

    public LinkBomb(Link object) {
        super(object, Link.DMG[7], Link.KNBK[7], Link.PRTY[7], false, Link.SECT[7]);
        this.shouldDieOnCollision = false;
        this.shouldDie = false;
    }

    @Override
    public void setUpEntity(int id, Node displayNode, Node menuNode, boolean alwaysDrawn) {
        super.setUpEntity(id, displayNode, menuNode, alwaysDrawn);
        this.setVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.getFacing()) * 15, 15, true);
    }
    
    @Override
    public void updateEntity() {                                
        super.updateEntity();
        this.shooter = null;
        this.team = Team.NONE;
        
        if (this.hasCollided()) {
            this.die();
        }
        
        if(this.isOnGround())
        {
            this.setPureXVelocity(0);
            this.setPureYVelocity(0);
        }
        
        if (this.shouldDie && !this.currentAction.equals(this.actionMap.get("OnDestroy"))) {
            this.forceCurrentAction(this.actionMap.get("OnDestroy"));
            this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
            this.setXVelocity(0);
            this.setYVelocity(0, false);
        }
    }
    
    @Override
    public void onCollideWith(PhysicsEntity object, CollisionInfo info) {       
        super.onCollideWith(object, info);
        
        if (object instanceof LinkBomb && this.currentAction.equals(this.actionMap.get("OnDestroy"))) {
            ((LinkBomb) object).die();
        }
    }

    private void die() {
        this.shouldDie = true;
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "bomb", StayingPower.NONE, Link.ISPD[7]).addModifier(new CustomAction()));
        actionMap.put("OnDestroy", new CharacterAction(this, "bombDestroy", StayingPower.BARELY, Link.ISPD[7]).addModifier(new ActionKillEntity()).addModifier(new ActionPlaySound("/Common Sounds/boom_00.ogg", 0)));
        return actionMap;
    }
    
    public class CustomAction extends ActionModifier {

        public CustomAction() {
            
        }

        @Override
        public void onInit() {

        }

        @Override
        public boolean onUpdate() {
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {
            ((LinkBomb) this.action.entity).die();
        }
    }
}
