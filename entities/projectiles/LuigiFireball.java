package entities.projectiles;

import engine.utility.Utilities;
import entities.characters.Luigi;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class LuigiFireball extends Projectile {
    private float counter;
    private boolean shouldKillFireball;
    
    private float initialY = -1337;
    private boolean direction;

    public LuigiFireball(Luigi object) {
        super(object, Luigi.DMG[4], Luigi.KNBK[4], Luigi.PRTY[4], true, Luigi.SECT[4]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        
        this.shouldDieOnCollision = false;
        this.counter = 0;
        this.shouldKillFireball = false;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.initialY == -1337) {
            this.initialY = this.getAppropriateBounds().getLowerMidpoint().y;
            this.direction = true;
        }
        
        if (this.shouldKillFireball && !this.justReflected) {
            this.killFireball();
        } else {
            this.shouldKillFireball = false;
        }
        
        if (this.hasCollided()) {
            this.shouldKillFireball = true;
        }
        
        if (this.currentAction == this.actionMap.get("OnDestroy")) {
            return;
        }
        
        if (this.isOnGround()) {
        	this.addToPosition(0, 1);
        }

        this.setXVelocity(this.getFacing() ? 25f : -25f);
        
        float distance = Math.abs(this.getAppropriateBounds().getLowerMidpoint().y - this.initialY);
        
        if (distance >= 7) {
            this.direction = !this.direction;
        }
        
        this.setPureYVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.direction) * 25f);

        if(!this.hasCollidedWithObstacle(true) && this.counter < .6) {
            this.counter += Utilities.lockedTPF;
        } else {
            this.killFireball();
        }
    }
    
    private void killFireball() {
        if (this.currentAction != this.actionMap.get("OnDestroy")) {
            this.forceCurrentAction(this.actionMap.get("OnDestroy"));
            this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
            this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
            this.setXVelocity(0);
            this.setYVelocity(0, false);
        }
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "fireball", StayingPower.NONE, Luigi.ISPD[4]));
        actionMap.put("OnDestroy", new CharacterAction(this, "fireDestroy", StayingPower.BARELY, Luigi.ISPD[4]).addModifier(new ActionKillEntity()).addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", 0)));
        return actionMap;
    }
}
