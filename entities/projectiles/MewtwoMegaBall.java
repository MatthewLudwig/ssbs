package entities.projectiles;

import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.characters.Mewtwo;
import physics.CollisionInfo;
import physics.PhysicsModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

public class MewtwoMegaBall extends Projectile {

    public MewtwoMegaBall(Mewtwo object) {
        super(object, Mewtwo.DMG[8], Mewtwo.KNBK[8], Mewtwo.PRTY[8], true);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.scale = 2f;
    }
    
    public boolean reflect(PhysicsEntity reflector) {return false;}

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        this.setXVelocity(this.getFacing() ? 40 : -40);
    }
    
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
    	if (entity instanceof PlayableCharacter){
    		((PlayableCharacter) entity).forceBreakShield(Mewtwo.DMG[8], Mewtwo.KNBK[8]);
    	}
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "megaShadowBall", StayingPower.NONE, Mewtwo.ISPD[8]).addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1)));
        return actionMap;
    }
}
