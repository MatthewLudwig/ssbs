package entities.projectiles;

import physics.PhysicsModifier;
import entities.characters.Ness;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

public class NessRockin3 extends Projectile {

    public NessRockin3(Ness object) {
        super(object, Ness.DMG[4], Ness.KNBK[4], Ness.PRTY[4], true, Ness.SECT[4]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.shouldDieOnCollision = false;
        this.scale = .72f;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
                
        this.setXVelocity(this.getFacing() ? 54 : -54);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "rockin3", StayingPower.NONE, Ness.ISPD[4]).addModifier(new ActionPlaySound("/Common Sounds/hit_05.ogg", -1)).addModifier(new ActionKillEntity()));
        return actionMap;
    }
}
