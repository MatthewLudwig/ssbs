package entities.projectiles;

import java.util.HashMap;
import java.util.Map;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.characters.Samus;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class SamusMissile extends Projectile {
    
    public SamusMissile(Samus object) {
        super(object, Samus.DMG[5], Samus.KNBK[5], Samus.PRTY[5], false, Samus.SECT[5]);
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        PhysicsEntity target = Main.getGameState().getWorld().getClosestPlayer(this.getAppropriateBounds().getLowerMidpoint(), (PlayableCharacter) this.shooter);
        
        if (target == null) {
            this.setPureXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.facing) * 10);
            this.setPureYVelocity(0);
        } else {
            Angle angle = Utilities.getPhysicsUtility().calculateCorrectAngle(target.getAppropriateBounds().getExactCenter(), this.getAppropriateBounds().getLowerMidpoint());

            if (this.facing) {
                if (angle.getValue() > 90) {
                    angle.subtract((angle.getValue() - 90) * 2);
                } else if (angle.getValue() < -90) {
                    angle.subtract((angle.getValue() + 90) * 2);
                }
            } else {
                if (angle.getValue() > 90) {
                    angle.add((90 - angle.getValue()) * 2);
                } else if (angle.getValue() < -90) {
                    angle.subtract((90 + angle.getValue()) * 2);
                }
            }

            this.rotateUpTo(angle);

            if(this.hasCollidedWithObstacle(true) || this.hasCollidedWithObstacle(false)) {
                this.setDead(true);
            }

            this.setPureXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(this.facing) * (10 * angle.cos()));
            this.setPureYVelocity(10 * angle.sin());

            if (this.facing == (target.getAppropriateBounds().getExactCenter().x > this.getAppropriateBounds().getLowerMidpoint().x)) {
                float additionalVelocity = this.getVelocity().x * .5f;
                this.addToVelocity(additionalVelocity, 0, false);
            }
        }
    }
    
    @Override
    public void onDeath() {
        super.onDeath();
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "missile", StayingPower.NONE, Samus.ISPD[5]).addModifier(new CustomAction()).addModifier(new ActionPlaySound("/Common Sounds/boom_00.ogg", -1)));
        return actionMap;
    }
    
    public class CustomAction extends ActionModifier {

        public CustomAction() {
            
        }

        @Override
        public void onInit() {}

        @Override
        public boolean onUpdate() {
            this.action.setCurrentFrame(0);

            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
}
