package entities.projectiles;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.characters.Sonic;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;

public class SonicEmerald extends Projectile {

    protected int emeraldNumber;
    protected float timeAlive;

    public SonicEmerald(Sonic object) {
        super(object, 0, 0, 0, false);
        this.timeAlive = 0;
        this.shouldDieOnCollision = false;
    }

    public void setEmeraldNumber(int initialNumberOfLives, int number) {
        //float multiplier = .928f * initialNumberOfLives * number;
        short[] sect = new short[number+1];
        for (int i = 0; i < sect.length; i++){
        	sect[i] = 0;
        }
        this.setAttackCapability(Sonic.DMG[5], Sonic.KNBK[5], Sonic.PRTY[5], false, sect);
        this.emeraldNumber = number - 1;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        PhysicsEntity target = Main.getGameState().getWorld().getClosestPlayer(this.getAppropriateBounds().getLowerMidpoint(), (PlayableCharacter) this.shooter);
        Angle angle = Utilities.getPhysicsUtility().calculateCorrectAngle(target.getAppropriateBounds().getExactCenter(), this.getAppropriateBounds().getLowerMidpoint());
        
        if(this.hasCollidedWithObstacle(true) || this.hasCollidedWithObstacle(false))
        {
            this.setDead(true);
        }
        
        this.setPureXVelocity((float) (20 * angle.cos()));
        this.setPureYVelocity((float) (20 * angle.sin()));
        
        if (this.timeAlive >= 1.5f) {
            this.setDead(true);
        } else {
            this.timeAlive += Utilities.lockedTPF;
        }
    }
    
    public void onCollideWith(PhysicsEntity entity, CollisionInfo info){
    	super.onCollideWith(entity, info);
    	if (entity instanceof PlayableCharacter && entity != this.shooter){
    		this.timeAlive *= 1.5f;
    	}
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "emerald", StayingPower.NONE, 2.14f).addModifier(new CustomAction()));
       return actionMap;
    }
    
    public class CustomAction extends ActionModifier {
        public CustomAction() {
            
        }

        @Override
        public void onInit() {}

        @Override
        public boolean onUpdate() {
            this.action.setCurrentFrame(SonicEmerald.this.emeraldNumber);

            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
}
