package entities.projectiles;

import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.ViolentEntity;
import entities.characters.Reggie;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.CollisionInfo;

public class ReggieMothers extends Projectile {
    private float counter;
    private boolean other;

    public ReggieMothers(Reggie object, boolean other) {
    	super(object, Reggie.DMG[7], Reggie.KNBK[7], Reggie.PRTY[7], false);
        this.counter = 0;
        this.other = other;
        this.scale = 1.00f;
        this.shouldDieOnCollision = true;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (other)
        	this.setCurrentAction(actionMap.get("OtherAction"));
        
        this.setXVelocity(this.other ? 15 : -15);

        if (this.isOnGround()) {
            this.addToPosition(0, 1);
            this.setYVelocity(10, false);
        }

        if(this.counter < 0.63) {
            this.counter += Utilities.lockedTPF;
        } else {
            this.setDead(true);
        }
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "lucas", StayingPower.NONE, Reggie.ISPD[7]).addModifier(new ActionPlaySound("/Common Sounds/hit_13.ogg", -1)));
        actionMap.put("OtherAction", new CharacterAction(this, "kumatora", StayingPower.LOW, Reggie.ISPD[7]).addModifier(new ActionPlaySound("/Common Sounds/hit_13.ogg", -1)));
        return actionMap;
    }
}
