package entities.projectiles;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;
import engine.Main;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import entities.characters.Luigi;
import entities.characters.actions.ActionModifier;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class LuigiKingBoo extends Projectile {
	private boolean isMoving;

    public LuigiKingBoo(Luigi object) {
        super(object, Luigi.DMG[8], Luigi.KNBK[8], Luigi.PRTY[8], false, Luigi.SECT[8]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.isMoving = true;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        for (PlayableCharacter c : Main.getGameState().getWorld().getPlayerList()) {
        	if (!c.isOnSameTeamAs(this)) {
        		if (c.getAppropriateBounds().getExactCenter().x < this.getAppropriateBounds().getExactCenter().x) {
        			if (c.getFacing()) {
        				this.isMoving = false;
        				break;
        			}
        		} else {
    				if (!c.getFacing()) {
        				this.isMoving = false;
        				break;
        			}
        		}
        	}
        	
        	this.isMoving = true;
        }

        if (this.isMoving) {
        	this.setXVelocity(Utilities.getGeneralUtility().getBooleanAsSign(!this.facing) * 15);
        } else {
        	this.setXVelocity(0);
        }
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "kingBoo", StayingPower.NONE, Luigi.ISPD[8]).addModifier(new CustomAction()));
        return actionMap;
    }
    
    public class CustomAction extends ActionModifier {
    	
        public CustomAction() {
            
        }

        @Override
        public void onInit() {
        	this.action.freezeFrame(true);
        }

        @Override
        public boolean onUpdate() {
        	if (((LuigiKingBoo) this.action.entity).isMoving) {
        		this.action.setCurrentFrame(0);
        	} else {
        		this.action.setCurrentFrame(1);
        	}
        	
            return false;
        }

        @Override
        public boolean shouldFinish() {
            return true;
        }

        @Override
        public void cleanUp() {}
    }
}
