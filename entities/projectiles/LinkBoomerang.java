package entities.projectiles;

import engine.Angle;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.characters.Link;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.CollisionInfo;
import physics.PhysicsModifier;

public class LinkBoomerang extends Projectile {
    private int timer;
    private boolean returning;
    Angle angle;

    public LinkBoomerang(Link object) {
        super(object, Link.DMG[5], Link.KNBK[5], Link.PRTY[5], false, Link.SECT[5]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.shouldDieOnCollision = false;
        this.returning = false;
    }

    @Override
    public void updateEntity()
    {                                
        super.updateEntity();
        
        if(!(this.hasCollidedWithObstacle(true) || this.hasCollidedWithObstacle(false)) && timer < 100) {
            this.setXVelocity(this.getFacing() ? 20 : -20);
        }
        else {
            
            this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
            
            if (!this.returning)
            	angle = Utilities.getPhysicsUtility().calculateCorrectAngle(this.shooter.getAppropriateBounds().getExactCenter(), this.getAppropriateBounds().getLowerMidpoint());
            this.returning = true;

            if(this.isOnGround())
            {
                this.addToPosition(0, 1);
            }

            this.setPureXVelocity((float) (20 * angle.cos()));
            this.setPureYVelocity((float) (20 * angle.sin()));
        }
        
        this.timer++;
    }
    
    @Override
    public void onCollideWith(PhysicsEntity object, CollisionInfo info) {
        if(this.returning && object.equals(this.shooter)) {
            this.setDead(true);
        } else {
            super.onCollideWith(object, info);
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "boomerang", StayingPower.NONE, Link.ISPD[5]).addModifier(new ActionPlaySound("/Common Sounds/hit_15.ogg", -1)));
        return actionMap;
    }
}
