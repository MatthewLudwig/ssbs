package entities.projectiles;

import engine.utility.Utilities;
import entities.characters.Mario;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.PhysicsModifier;

public class MarioFireball extends Projectile {
    private float counter;
    private boolean shouldKillFireball;

    public MarioFireball(Mario object) {
        super(object, Mario.DMG[4], Mario.KNBK[4], Mario.PRTY[4], true, Mario.SECT[4]);
        this.shouldDieOnCollision = false;
        this.counter = 0;
        this.shouldKillFireball = false;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.hasCollided()) {
            this.shouldKillFireball = true;
        }
        
        if (this.shouldKillFireball && !this.justReflected) {
            this.killFireball();
        } else {
            this.shouldKillFireball = false;
        }
        
        if (this.currentAction == this.actionMap.get("OnDestroy")) {
            return;
        }

        this.setXVelocity(this.getFacing() ? 12.5f : -12.5f);

        if(this.isOnGround()) {
            this.addToPosition(0, 1);
            this.setYVelocity(15, false);
        }

        if(!this.hasCollidedWithObstacle(true) && this.counter < .75) {
            this.counter += Utilities.lockedTPF;
        } else {
            this.killFireball();
        }
    }
    
    private void killFireball() {
        if (this.currentAction != this.actionMap.get("OnDestroy")) {
            this.forceCurrentAction(this.actionMap.get("OnDestroy"));
            this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
            this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
            this.setXVelocity(0);
            this.setYVelocity(0, false);
        }
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "fireball", StayingPower.NONE, Mario.ISPD[4]));
        actionMap.put("OnDestroy", new CharacterAction(this, "fireDestroy", StayingPower.BARELY, Mario.ISPD[4]).addModifier(new ActionKillEntity()).addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", 0)));
        return actionMap;
    }
}
