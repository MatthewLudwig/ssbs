package entities.projectiles;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;
import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.characters.MegaMan;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import entities.items.ItemMetalBlade;

public class MegaManMetalBlade extends Projectile {

    private Angle direction;
    private float timer;
    private boolean directionHasChanged;
    
    public MegaManMetalBlade(MegaMan object) {
        super(object, MegaMan.DMG[4], MegaMan.KNBK[4], MegaMan.PRTY[4], false, MegaMan.SECT[4]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.directionHasChanged = false;
        this.shouldDieOnCollision = false;
        this.timer = 0;
    }
    
    public void setShooterNull() {
    	this.shooter = null;
    }
    
    public PhysicsEntity getShooter() {
    	return this.shooter;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.hasCollidedWithObstacle(true) || this.hasCollidedWithObstacle(false)) {
        	ItemMetalBlade blade = new ItemMetalBlade();
        	blade.setPosition(this.getAppropriateBounds().getLowerMidpoint().x, this.getAppropriateBounds().getLowerMidpoint().y);
        	Main.getGameState().spawnEntity(blade);
        	this.setDead(true);
        }
        
        if (this.direction == null) {
            this.direction = this.getFacing() ? new Angle(0) : new Angle(180);
        }
        
        if (this.timer > 5) {
            this.setDead(true);
        } else {
            this.timer += Utilities.lockedTPF;
        }
        
        this.setPureXVelocity(30 * this.direction.cos());
        this.setPureYVelocity(30 * this.direction.sin());
    }
    
    public void setDirection(float value) {
        if (this.direction == null) {
            this.direction = new Angle(value);
        } else if (this.directionHasChanged) {
            switch (Math.round(this.direction.getValue())) {
                case -90:
                    if (value == 0) {
                        this.direction.setValue(-45);
                    } else if (value == 180) {
                        this.direction.setValue(225);
                    }
                    break;
                case 0:
                    if (value == 90) {
                        this.direction.setValue(45);
                    } else if (value == -90) {
                        this.direction.setValue(-45);
                    }
                    break;
                case 90:
                    if (value == 0) {
                        this.direction.setValue(45);
                    } else if (value == 180) {
                        this.direction.setValue(135);
                    }
                    break;
                case 180:
                    if (value == 90) {
                        this.direction.setValue(135);
                    } else if (value == -90) {
                        this.direction.setValue(225);
                    }
                    break;
            }
        } else {
            this.direction.setValue(value);
            this.directionHasChanged = true;
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "metalBlade", StayingPower.NONE, MegaMan.ISPD[4]).addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1)));
        return actionMap;
    }
}
