package entities.projectiles;

import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.ViolentEntity;
import entities.characters.Reggie;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;
import physics.CollisionInfo;

public class ReggieVillains extends Projectile {
	private float counter;
	private boolean other;
	private boolean init;

	public ReggieVillains(Reggie object, boolean other) {
		super(object, Reggie.DMG[7], Reggie.KNBK[7], Reggie.PRTY[7], false);
		this.counter = 0;
		this.other = other;
		this.scale = 0.28f;
		this.shouldDieOnCollision = false;
		this.init = false;
	}

	@Override
	public void updateEntity() {
		super.updateEntity();

		if (other)
			this.setCurrentAction(actionMap.get("OtherAction"));

		if (!init) {
			this.setXVelocity(this.other ? 15 : -15);
			init = true;
		}

		if (this.isOnGround()) {
			this.addToPosition(0, 1);
			this.setYVelocity(10, false);
		}

		if (this.counter < 1.25) {
			this.counter += Utilities.lockedTPF;
		} else {
			this.setDead(true);
		}
	}

	public void onCollideWith(PhysicsEntity entity, CollisionInfo info) {
		super.onCollideWith(entity, info);
		this.setXVelocity(-this.getVelocity().x);
		this.setYVelocity(-this.getVelocity().y, false);
	}

	@Override
	protected Map<String, CharacterAction> getActionMap() {
		Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
		actionMap.put("OnlyAction", new CharacterAction(this, "wario",
				StayingPower.NONE, Reggie.ISPD[7]).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_13.ogg", -1)));
		actionMap.put("OtherAction", new CharacterAction(this, "ganondorf",
				StayingPower.LOW, Reggie.ISPD[7]).addModifier(new ActionPlaySound(
				"/Common Sounds/hit_13.ogg", -1)));
		return actionMap;
	}
}
