package entities.projectiles;

import java.util.HashMap;
import java.util.Map;

import physics.PhysicsModifier;

import com.jme3.scene.Node;

import engine.Angle;
import engine.utility.Utilities;
import entities.characters.KingDedede;
import entities.characters.actions.ActionAttack;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class KingDededeJumpStar extends Projectile {
    private Angle angle;
    private float lifeTimer;

    public KingDededeJumpStar(KingDedede shooter) {
        super(shooter, KingDedede.DMG[6], KingDedede.KNBK[6], KingDedede.ISPD[6], true, KingDedede.SECT[6]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.angle = new Angle(KingDedede.gen.nextInt(360) - 90);
        this.facing = KingDedede.gen.nextBoolean();
        this.lifeTimer = 1;
    }
    
    @Override
    public void setUpEntity(int id, Node displayNode, Node menuNode, boolean alwaysDrawn) {
        super.setUpEntity(id, displayNode, menuNode, alwaysDrawn);
    }
    
    @Override
    public void updateEntity() {
        super.updateEntity();
        
        this.angle.add(Utilities.getGeneralUtility().getBooleanAsSign(this.facing) * 30 * Utilities.lockedTPF);
        this.rotateUpTo(this.angle);
        
        if (this.lifeTimer <= 0) {
            this.setDead(true);
        } else {
            this.lifeTimer -= Utilities.lockedTPF;
        }
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "jumpStar", StayingPower.NONE, KingDedede.ISPD[6]).addModifier(new ActionPlaySound("/Common Sounds/hit_08.ogg", -1)));
        return actionMap;
    }
}
