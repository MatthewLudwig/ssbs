package entities.projectiles;

import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.characters.Waluigi;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

import physics.CollisionInfo;
import physics.PhysicsModifier;

public class WaluigiBobOmb extends Projectile {
    private float counter;
    private boolean shouldKillBobOmb;
    private boolean init;
    private Waluigi obj;

    public WaluigiBobOmb(Waluigi object) {
        super(object, Waluigi.DMG[6], Waluigi.KNBK[6], Waluigi.PRTY[6], true, Waluigi.SECT[6]);
        this.shouldDieOnCollision = false;
        this.counter = 0;
        this.shouldKillBobOmb = false;
        this.init = false;
        this.obj = object;
        this.scale = object.getScale();
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.hasCollided()) {
            this.shouldKillBobOmb = true;
        }
        
        if (this.shouldKillBobOmb && !this.justReflected) {
            this.killBobOmb();
        } else {
            this.shouldKillBobOmb = false;
        }
        
        if (this.currentAction == this.actionMap.get("OnDestroy")) {
            return;
        }

        if (!init){
        	this.setYVelocity(-5, false);
        	init = true;
        }

        if(!this.hasCollidedWithObstacle(true) && !this.hasCollidedWithObstacle(false) && this.counter < 0.8f) {
            this.counter += Utilities.lockedTPF;
        } else {
            this.killBobOmb();
        }
    }
    
    public void onCollideWith(PhysicsEntity object, CollisionInfo info){
    	super.onCollideWith(object, info);
    	
    	if (this.currentAction == this.actionMap.get("OnDestroy") && object == this.obj){
    		this.obj.setYVelocity(Waluigi.STAT[8]*40, false);
    	}
    }
    
    private void killBobOmb() {
        if (this.currentAction != this.actionMap.get("OnDestroy")) {
            this.forceCurrentAction(this.actionMap.get("OnDestroy"));
            this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
            this.setXVelocity(0);
            this.setYVelocity(0, false);
        }
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "bobOmbFall", StayingPower.NONE, Waluigi.ISPD[6]));
        actionMap.put("OnDestroy", new CharacterAction(this, "bobOmbExplode", StayingPower.BARELY, Waluigi.ISPD[6]).addModifier(new ActionKillEntity()).addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", 0)));
        return actionMap;
    }
}
