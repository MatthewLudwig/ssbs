package entities.projectiles;

import physics.PhysicsModifier;
import engine.Angle;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import entities.characters.Ridley;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

public class RidleyFireball extends Projectile {
	
	boolean init;
	Angle angle;

    public RidleyFireball(Ridley object) {
        super(object, Ridley.DMG[4], Ridley.KNBK[4], Ridley.PRTY[4], false, Ridley.SECT[4]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        init = false;
        angle = new Angle(0);
        this.shouldDieOnCollision = false;
        this.scale = object.getScale();
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (!init){
        	init = true;
        	Vector2f center = shooter.getPosition();
            Vector2f tween = getPosition().subtract(center).mult((1+scale)/2f);
            this.setPosition(center.add(tween).x, center.add(tween).y);
        	if (((PlayableCharacter) shooter).checkControl("Up", false) && !((PlayableCharacter) shooter).checkControl("Down", false)){
        		angle = new Angle(45);
        	} else if (!((PlayableCharacter) shooter).checkControl("Up", false) && ((PlayableCharacter) shooter).checkControl("Down", false)){
        		angle = new Angle(-45);
        	}
        }
        
        if (this.hasCollidedWithObstacle(true)){
        	this.setDead(true);
        }
                
        if (facing){
        	rotateUpTo(new Angle((float) Math.toDegrees(getVelocity().getAngle())));
        } else {
        	rotateUpTo(new Angle(180 - (float) Math.toDegrees(getVelocity().getAngle())));
        }
        this.setPureXVelocity(angle.cos()*25*Utilities.getGeneralUtility().getBooleanAsSign(facing));
		this.setPureYVelocity(angle.sin()*25);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "fireball", StayingPower.NONE, Ridley.ISPD[4]).addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", -1)));
        return actionMap;
    }
}
