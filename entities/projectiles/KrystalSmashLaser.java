package entities.projectiles;

import entities.characters.Krystal;
import physics.PhysicsModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

public class KrystalSmashLaser extends Projectile {

    public KrystalSmashLaser(Krystal object) {
        super(object, Krystal.DMG[8], Krystal.KNBK[8], Krystal.PRTY[8], true, Krystal.SECT[8]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.scale = .75f;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.hasCollidedWithObstacle(true) || this.hasCollidedWithObstacle(false)) {
            this.setDead(true);
        }
        
        this.setXVelocity(this.getFacing() ? 80 : -80);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "landmasterLaser", StayingPower.NONE, Krystal.ISPD[8]).addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1)));
        return actionMap;
    }
}
