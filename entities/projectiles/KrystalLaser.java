package entities.projectiles;

import entities.characters.Krystal;
import physics.PhysicsModifier;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.HashMap;
import java.util.Map;

public class KrystalLaser extends Projectile {

    public KrystalLaser(Krystal object) {
        super(object, Krystal.DMG[4], Krystal.KNBK[4], Krystal.PRTY[4], true, Krystal.SECT[4]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.scale = .75f;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.hasCollidedWithObstacle(true) || this.hasCollidedWithObstacle(false)) {
            this.setDead(true);
        }
        
        this.setXVelocity(this.getFacing() ? 80 : -80);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "laser", StayingPower.NONE, Krystal.ISPD[4]).addModifier(new ActionPlaySound("/Common Sounds/hit_06.ogg", -1)));
        return actionMap;
    }
}
