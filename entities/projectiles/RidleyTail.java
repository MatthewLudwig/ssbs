package entities.projectiles;

import physics.PhysicsModifier;
import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PlayableCharacter;
import entities.characters.Ridley;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;
import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

public class RidleyTail extends Projectile {

    public RidleyTail(Ridley object) {
        super(object, Ridley.DMG[7], Ridley.KNBK[7], Ridley.PRTY[7], false, Ridley.SECT[7]);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.shouldDieOnCollision = false;
        this.scale = object.getScale();
        this.calculateLedge = true;
    }

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if (this.currentAction.equals(actionMap.get("OnlyAction"))){
        	if (facing){
        		if (((PlayableCharacter) shooter).checkControl("Left", false) && !((PlayableCharacter) shooter).checkControl("Right", false)){
        			this.setPureXVelocity(10);
        		} else if (((PlayableCharacter) shooter).checkControl("Right", false) && !((PlayableCharacter) shooter).checkControl("Left", false)){
        			this.setPureXVelocity(30);
        		} else {
        			this.setPureXVelocity(20);
        		}
        	} else {
        		if (((PlayableCharacter) shooter).checkControl("Left", false) && !((PlayableCharacter) shooter).checkControl("Right", false)){
        			this.setPureXVelocity(-30);
        		} else if (((PlayableCharacter) shooter).checkControl("Right", false) && !((PlayableCharacter) shooter).checkControl("Left", false)){
        			this.setPureXVelocity(-10);
        		} else {
        			this.setPureXVelocity(-20);
        		}
        	}
        	
        	Vector2f nearestLedge = Main.getGameState().getWorld().getStage().getNearestLedge(this.getPosition());
        	/*if (nearestLedge != null){
        		System.out.println(getPosition().distance(nearestLedge));
        	}*/
        	if (!((PlayableCharacter) shooter).checkControl("Special", false) || (nearestLedge != null && getPosition().distance(nearestLedge) < 20)){
        		setCurrentAction(actionMap.get("OtherAction"));
        	}
        } else {
        	this.setPureXVelocity(0);
        }
                
        this.setAttackCapability(Ridley.DMG[7], Ridley.KNBK[7], Ridley.PRTY[7], false, Ridley.SECT[7]);
    }
    
    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "tailStab", StayingPower.NONE, 0).addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1)));
        actionMap.put("OtherAction", new CharacterAction(this, "tailStab", StayingPower.LOW, Ridley.ISPD[4]).addModifier(new ActionPlaySound("/Common Sounds/hit_11.ogg", -1)).addModifier(new ActionKillEntity()));
        return actionMap;
    }
}
