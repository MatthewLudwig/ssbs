package entities.projectiles;

import java.util.HashMap;
import java.util.Map;

import entities.PhysicsEntity;
import entities.characters.KingKRool;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

public class KingKRoolFinalKannonball extends Projectile {

    public KingKRoolFinalKannonball(KingKRool object) {
        super(object, 13.49f, 2.13f, 99.99f, false);
        this.shouldDieOnCollision = false;
        this.scale = 2;
    }
    
    @Override
    public boolean reflect(PhysicsEntity reflector) {return false;}

    @Override
    public void updateEntity() {                                
        super.updateEntity();
        
        if(this.isOnGround() || this.hasCollided()) {
            this.addToPosition(0, 1);
            this.setYVelocity(30, false);
            this.setXVelocity(((float) Math.random() * 60) - 30);
        }
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "kannonball", StayingPower.NONE, 2.42f).addModifier(new ActionPlaySound("/Common Sounds/hit_15.ogg", -1)));
        return actionMap;
    }
}
