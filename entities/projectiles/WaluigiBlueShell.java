package entities.projectiles;

import engine.Angle;
import engine.Main;
import engine.utility.Utilities;
import entities.PhysicsEntity;
import entities.PlayableCharacter;
import entities.Team;
import entities.characters.Waluigi;
import entities.characters.actions.ActionKillEntity;
import entities.characters.actions.ActionPlaySound;
import entities.characters.actions.CharacterAction;
import entities.characters.actions.StayingPower;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;

import physics.CollisionInfo;
import physics.PhysicsModifier;

public class WaluigiBlueShell extends Projectile {
    private float counter;
    private Team setTeam;
    private Waluigi obj;
    private PlayableCharacter target;

    public WaluigiBlueShell(Waluigi object) {
        super("Explosions", object, 0, 0, Waluigi.PRTY[8], true);
        this.shouldDieOnCollision = false;
        this.counter = 0;
        this.obj = object;
        this.scale = object.getScale();
        this.target = getTarget();
        this.setTeam = team;
        this.addPhysicsModifier(PhysicsModifier.CANGOTHROUGHTHINPLATORMS);
        this.addPhysicsModifier(PhysicsModifier.NOGRAVITY);
        this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
        this.addPhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
    }

    @Override
    public void updateEntity() { 
        super.updateEntity();
        this.team = this.setTeam;
        if (this.currentAction == actionMap.get("OnlyAction")){
        	Angle a;
        	if (target != null){
        		a = Utilities.getPhysicsUtility().calculateCorrectAngle(this.getPosition(), new Vector2f(target.getPosition().x, target.getPosition().y+100));
        	} else {
        		a = Utilities.getPhysicsUtility().calculateCorrectAngle(this.getPosition(), new Vector2f(Main.getGameState().getWorld().getStage().getSizeY()*1.2f, Main.getGameState().getWorld().getStage().getSizeX()*0.5f));
        	}
        	this.setPureXVelocity(-20*a.cos());
        	this.setPureYVelocity(-20*a.sin());
        	counter += Utilities.lockedTPF;
        }
        if (counter > 6){
        	dropShell();
        }
    }
    
    public void onCollideWith(PhysicsEntity object, CollisionInfo info){
    	super.onCollideWith(object, info);
    	explodeShell();
    }
    
    private void explodeShell() {
    	if (this.currentAction == actionMap.get("Fall")){
    		this.addPhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
    		this.forceCurrentAction(actionMap.get("Explode"));
    		this.rotateUpTo(new Angle(0));
    		this.setPureYVelocity(0f);
    		this.setAttackCapability(Waluigi.DMG[8], Waluigi.KNBK[8], Waluigi.PRTY[8], true, Waluigi.SECT[8]);
    	}
    }
    
    private void dropShell() {
    	if (this.currentAction == actionMap.get("OnlyAction")){
    		this.forceCurrentAction(actionMap.get("Fall"));
    		this.rotateUpTo(new Angle(180));
    		this.setPureYVelocity(-60f);
    		this.removePhysicsModifier(PhysicsModifier.NOOBSTACLECOLLISION);
            this.removePhysicsModifier(PhysicsModifier.NOOBJECTCOLLISION);
    	}
    }
    
    public PlayableCharacter getTarget(){
    	ArrayList<PlayableCharacter> charList = new ArrayList<PlayableCharacter> (Main.getGameState().getWorld().getPlayerList());
    	PlayableCharacter p = charList.get(0);
    	do {
    		for (PlayableCharacter pc : charList){
    			if (p != pc && (pc.getNumberOfLives() > p.getNumberOfLives() || (pc.getNumberOfLives() == p.getNumberOfLives() && pc.getDamage() < p.getDamage()) || (pc.getNumberOfLives() == p.getNumberOfLives() && pc.getDamage() == p.getDamage() && pc.team != obj.team))){
    				p = pc;
    			}
    		}
    		if (p.team == obj.team){
    			this.shooter = null;
    			this.team = Team.NONE;
    			charList.remove(p);
    			p = charList.get(0);
    		}
    	} while (p.team == obj.team);
    	return p;
    }

    @Override
    protected Map<String, CharacterAction> getActionMap() {
        Map<String, CharacterAction> actionMap = new HashMap<String, CharacterAction>();
        actionMap.put("OnlyAction", new CharacterAction(this, "blueShellFly", StayingPower.NONE, Waluigi.ISPD[8]));
        actionMap.put("Fall", new CharacterAction(this, "blueShellFly", StayingPower.BARELY, Waluigi.ISPD[8]));
        actionMap.put("Explode", new CharacterAction(this, "blueShellExplode", StayingPower.LOW, Waluigi.ISPD[8]).addModifier(new ActionKillEntity()).addModifier(new ActionPlaySound("/Common Sounds/blam_00.ogg", 0)));
        return actionMap;
    }
}
