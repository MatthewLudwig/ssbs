package physics;

import com.jme3.math.Vector2f;
import com.jme3.texture.Image;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture2D;
import com.jme3.texture.image.ImageRaster;
import com.jme3.texture.plugins.AWTLoader;
import com.jme3.ui.Picture;

import engine.utility.Utilities;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

/**
 *
 * @author Matthew
 */
public class CollidableImage {
    protected static final AWTLoader loader = new AWTLoader();
    protected static final int[] emptyArray = null;
    
    private Texture2D image;
    private Texture2D collisionTex;
    private BufferedImage collisionMap;
    private Map<Float, Raster> collisionMaps;
    private BoundingBox realBounds;
    private BoundingBox disjointBounds;
    
    public CollidableImage(BufferedImage texture, BufferedImage collisionMap) {   
        this.image = new Texture2D(loader.load(texture, true));
        this.image.setMagFilter(Texture.MagFilter.Nearest);
        this.image.setMinFilter(Texture.MinFilter.NearestNoMipMaps);
        
        Image b = loader.load(collisionMap, true);
        this.collisionMap = collisionMap;
        this.collisionMaps = new HashMap<Float, Raster>();
        this.collisionMaps.put(1f, this.collisionMap.getRaster());
        
        this.collisionTex = new Texture2D(b);
        
        Vector2f bottomLeftPoint = new Vector2f(-1, -1);
        Vector2f topRightPoint = new Vector2f(-1, -1);
        
        for (int y = 0; y < this.getHeight(); y++) {
            if (this.getLeftMostPoint(y) != -1) {
                if (bottomLeftPoint.x == -1) {
                    bottomLeftPoint.setX(this.getLeftMostPoint(y));
                } else {
                    bottomLeftPoint.setX(Math.min(bottomLeftPoint.x, this.getLeftMostPoint(y)));
                }
            }
            
            if (this.getRightMostPoint(y) != -1) {
                if (topRightPoint.x == -1) {
                    topRightPoint.setX(this.getRightMostPoint(y));
                } else {
                    topRightPoint.setX(Math.max(topRightPoint.x, this.getRightMostPoint(y)));
                }
            }
        }

        for (int x = 0; x < this.getWidth(); x++) {
            if (this.getLowestPoint(x) != -1) {
                if (bottomLeftPoint.y == -1) {
                    bottomLeftPoint.setY(this.getLowestPoint(x));
                } else {
                    bottomLeftPoint.setY(Math.min(bottomLeftPoint.y, this.getLowestPoint(x)));
                }
            }
            
            if (this.getHighestPoint(x) != -1) {
                if (topRightPoint.y == -1) {
                    topRightPoint.setY(this.getHighestPoint(x));
                } else {
                    topRightPoint.setY(Math.max(topRightPoint.y, this.getHighestPoint(x)));
                }
            }
        }
        
        this.realBounds = new BoundingBox(bottomLeftPoint, topRightPoint.subtract(bottomLeftPoint));
        
        bottomLeftPoint.set(-1, -1);
        topRightPoint.set(-1, -1);
        
        for (int y = 0; y < this.getHeight(); y++) {
            if (this.getLeftMostPointForDisjoint(y) != -1) {
                if (bottomLeftPoint.x == -1) {
                    bottomLeftPoint.setX(this.getLeftMostPointForDisjoint(y));
                } else {
                    bottomLeftPoint.setX(Math.min(bottomLeftPoint.x, this.getLeftMostPointForDisjoint(y)));
                }
            }
            
            if (this.getRightMostPointForDisjoint(y) != -1) {
                if (topRightPoint.x == -1) {
                    topRightPoint.setX(this.getRightMostPointForDisjoint(y));
                } else {
                    topRightPoint.setX(Math.max(topRightPoint.x, this.getRightMostPointForDisjoint(y)));
                }
            }
        }

        for (int x = 0; x < this.getWidth(); x++) {
            if (this.getLowestPointForDisjoint(x) != -1) {
                if (bottomLeftPoint.y == -1) {
                    bottomLeftPoint.setY(this.getLowestPointForDisjoint(x));
                } else {
                    bottomLeftPoint.setY(Math.min(bottomLeftPoint.y, this.getLowestPointForDisjoint(x)));
                }
            }
            
            if (this.getHighestPointForDisjoint(x) != -1) {
                if (topRightPoint.y == -1) {
                    topRightPoint.setY(this.getHighestPointForDisjoint(x));
                } else {
                    topRightPoint.setY(Math.max(topRightPoint.y, this.getHighestPointForDisjoint(x)));
                }
            }
        }
        
        this.disjointBounds = new BoundingBox(bottomLeftPoint, topRightPoint.subtract(bottomLeftPoint));
    }
    
    private int getLeftMostPoint(int y) {
        int leftMostPoint = -1;
        
        for (int x = 0; x < this.getWidth(); x++) {
            if (this.collisionCheck(x, y, 1) != -1) {
                return x;
            }
        }
        
        return leftMostPoint;
    }
    
    private int getRightMostPoint(int y) {
        int rightMostPoint = -1;
        
        for (int x = this.getWidth() - 1; x > 0; x--) {
            if (this.collisionCheck(x, y, 1) != -1) {
                return x;
            }
        }
        
        return rightMostPoint;
    }
    
    private int getLowestPoint(int x) {
        int lowestPoint = -1;
        
        for (int y = 0; y < this.getHeight(); y++) {
            if (this.collisionCheck(x, y, 1) != -1) {
                return y;
            }
        }
        
        return lowestPoint;
    }
    
    private int getHighestPoint(int x) {
        int highestPoint = -1;
        
        for (int y = this.getHeight() - 1; y > 0; y--) {
            if (this.collisionCheck(x, y, 1) != -1) {
                return y;
            }
        }
        
        return highestPoint;
    }
    
    private int getLeftMostPointForDisjoint(int y) {
        int leftMostPoint = -1;
        
        for (int x = 0; x < this.getWidth(); x++) {
            if (this.collisionCheck(x, y, 1) != -1 && this.collisionCheck(x, y, 1) < .2f) {
                return x;
            }
        }
        
        return leftMostPoint;
    }
    
    private int getRightMostPointForDisjoint(int y) {
        int rightMostPoint = -1;
        
        for (int x = this.getWidth() - 1; x > 0; x--) {
            if (this.collisionCheck(x, y, 1) != -1 && this.collisionCheck(x, y, 1) < .2f) {
                return x;
            }
        }
        
        return rightMostPoint;
    }
    
    private int getLowestPointForDisjoint(int x) {
        int lowestPoint = -1;
        
        for (int y = 0; y < this.getHeight(); y++) {
            if (this.collisionCheck(x, y, 1) != -1 && this.collisionCheck(x, y, 1) < .2f) {
                return y;
            }
        }
        
        return lowestPoint;
    }
    
    private int getHighestPointForDisjoint(int x) {
        int highestPoint = -1;
        
        for (int y = this.getHeight() - 1; y > 0; y--) {
            if (this.collisionCheck(x, y, 1) != -1 && this.collisionCheck(x, y, 1) < .2f) {
                return y;
            }
        }
        
        return highestPoint;
    }
    
    public final Texture2D getImage() {
        return this.image;
    }
    
    public final Texture2D getCollisionMap() {
    	return collisionTex;
    }
    
    public final int getWidth() {
        return this.collisionMap.getWidth();
    }
    
    public final int getHeight() {
        return this.collisionMap.getHeight();
    }
    
    public final BoundingBox getRealBounds() {
        return this.realBounds;
    }
    
    public final BoundingBox getDisjointBounds() {
        return this.disjointBounds;
    }

    public final float collisionCheck(int x, int y, float scale) {
        if (!this.collisionMaps.containsKey(scale)) {
        	BufferedImage bufImage= Utilities.getGeneralUtility().getScaledImage(this.collisionMap, Math.round(this.getWidth() * scale) + 1, Math.round(this.getHeight() * scale) + 1);
        	/*File imageFile= new File("sweetSpotMap.png");
        	System.out.println("ok");
        	try {
				ImageIO.write(bufImage, "png",imageFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
            this.collisionMaps.put(Float.valueOf(scale), bufImage.getRaster());
        }
        
        int[] array = this.collisionMaps.get(scale).getPixel(x, y, emptyArray);
        
        return (array[array.length - 1] != 0) ? ((float) array[0]) / 255f : -1;
    }
}