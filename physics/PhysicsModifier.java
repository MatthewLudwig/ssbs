package physics;

/**
 * Set of different modifiers for a Entity that affect gravity, collision
 * detection, and stage collision in different ways.
 *
 * @author Matthew
 */
public enum PhysicsModifier {
    ISSTICKY(),
    NOGRAVITY(),
    NOOBSTACLECOLLISION(),
    NOOBJECTCOLLISION(),
    CANGOTHROUGHTHINPLATORMS();
}
