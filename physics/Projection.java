package physics;

/**
 *
 * @author Matthew
 */
public class Projection {
    private float center;
    private float radius;
    
    public Projection(float smallest, float largest) {
        this.radius = (largest - smallest) / 2;
        this.center = smallest + this.radius;
    }
    
    public boolean isOverlapping(Projection other) {
        return Math.abs(other.center - this.center) < (this.radius + other.radius);
    }
    
    public float getOverlap(Projection other) {
        return Math.min((this.center + this.radius), (other.center + other.radius)) - Math.max((this.center - this.radius), (other.center - other.radius));
    }
}
