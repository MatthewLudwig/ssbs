package physics;

import com.jme3.math.Vector2f;
import java.util.LinkedList;
import java.util.List;

/**
 * A bounding box class that allows for simple 2D collision detection.
 *
 * @author Matthew Ludwig
 */
public class BoundingBox {
    private Vector2f lowerLeft;
    private Vector2f dimensions;

    public BoundingBox(Vector2f lowerLeft, Vector2f dimensions) {
        this.updateBounds(lowerLeft, dimensions);
    }

    public final void updateBounds(Vector2f lowerLeft, Vector2f dimensions) {
        this.lowerLeft = new Vector2f(lowerLeft);
        this.dimensions = new Vector2f(dimensions);
    }

    public final void moveBounds(float dX, float dY) {
        this.lowerLeft.addLocal(dX, dY);
    }
    
    public final void scaleBounds(float scaleXY) {
        this.scaleBounds(scaleXY, scaleXY);
    }
    
    public final void scaleBounds(float scaleX, float scaleY) {
        this.lowerLeft.subtractLocal((this.dimensions.x / 2) * (scaleX - 1), (this.dimensions.y / 2) * (scaleY - 1));
        this.dimensions.setX(this.dimensions.x * scaleX);
        this.dimensions.setY(this.dimensions.y * scaleY);
    }

    public final Vector2f getLowerLeft() {
        return this.lowerLeft;
    }

    public final Vector2f getLowerRight() {
        return this.lowerLeft.subtract(-this.dimensions.x, 0);
    }

    public final Vector2f getUpperRight() {
        return this.lowerLeft.subtract(-this.dimensions.x, -this.dimensions.y);
    }

    public final Vector2f getUpperLeft() {
        return this.lowerLeft.subtract(0, -this.dimensions.y);
    }
    
    public final Vector2f getDimensions() {
        return this.dimensions;
    }

    public final Vector2f getLowerMidpoint() {
        return this.lowerLeft.subtract(-this.dimensions.x / 2, 0);
    }

    public final Vector2f getRightMidpoint() {
        return this.lowerLeft.subtract(-this.dimensions.x, -this.dimensions.y / 2);
    }

    public final Vector2f getUpperMidpoint() {
        return this.lowerLeft.subtract(-this.dimensions.x / 2, -this.dimensions.y);
    }

    public final Vector2f getLeftMidpoint() {
        return this.lowerLeft.subtract(0, -this.dimensions.y / 2);
    }
    
    public final Vector2f getExactCenter() {
        return this.lowerLeft.subtract(-this.dimensions.x / 2, -this.dimensions.y / 2);
    }
    
    public final Projection project(Vector2f axis) {        
        Vector2f minAndMax = null;
        
        List<Vector2f> vertices = new LinkedList<Vector2f>();
        vertices.add(this.getLowerLeft());
        vertices.add(this.getLowerRight());
        vertices.add(this.getUpperRight());
        vertices.add(this.getUpperLeft());
        
        for (Vector2f vertex : vertices) {
            float temp = axis.dot(vertex);
            
            if (minAndMax == null) {
                minAndMax = new Vector2f(temp, temp);
            } else if (temp < minAndMax.x) {
                minAndMax.setX(temp);
            } else if (temp > minAndMax.y) {
                minAndMax.setY(temp);
            }
        }
        
        return new Projection(minAndMax.x, minAndMax.y);
    }
    
    public final boolean isInBounds(BoundingBox otherBox) {
        Vector2f thisCenter = this.getExactCenter();
        Vector2f otherCenter = otherBox.getExactCenter();
        Vector2f distance = new Vector2f(Math.abs(thisCenter.x - otherCenter.x), Math.abs(thisCenter.y - otherCenter.y)).multLocal(2);
        
        return distance.x <= this.dimensions.x + otherBox.dimensions.x && distance.y <= this.dimensions.y + otherBox.dimensions.y;
    }
    
    public final boolean isAnyPartInBounds(BoundingBox otherBox) {
        return this.isInBounds(otherBox) || this.isInBounds(otherBox.getLowerLeft()) || this.isInBounds(otherBox.getLowerRight()) || this.isInBounds(otherBox.getUpperLeft()) || this.isInBounds(otherBox.getUpperRight());
    }

    public final boolean isInBounds(Vector2f point) {
        Vector2f lowerResult = point.subtract(this.lowerLeft);

        return lowerResult.x >= 0 && lowerResult.y >= 0 && lowerResult.x <= this.getDimensions().x && lowerResult.y <= this.getDimensions().y;
    }

    public final boolean isStrictlyInBounds(Vector2f point) {
        Vector2f lowerResult = point.subtract(this.lowerLeft);

        return lowerResult.x > 0 && lowerResult.y > 0 && lowerResult.x < this.getDimensions().x && lowerResult.y < this.getDimensions().y;
    }
    
    public final BoundingBox getCollisionWith(BoundingBox otherBox) {
        Vector2f bottomLeftPoint = new Vector2f(Math.max(this.lowerLeft.x, otherBox.lowerLeft.x), Math.max(this.lowerLeft.y, otherBox.lowerLeft.y));
        Vector2f topRightPoint = new Vector2f(Math.min(this.getUpperRight().x, otherBox.getUpperRight().x), Math.min(this.getUpperRight().y, otherBox.getUpperRight().y));
        return new BoundingBox(bottomLeftPoint, topRightPoint.subtractLocal(bottomLeftPoint));
    }
    
    public final BoundingBox fitInsideBoundingBox(BoundingBox otherBox, boolean shouldClone) {
        BoundingBox fitBox = shouldClone ? otherBox.clone() : otherBox;
        
        while (fitBox.getLowerLeft().x < this.getLowerLeft().x) {
            fitBox.moveBounds(1, 0);
        }
        
        while (fitBox.getLowerLeft().y < this.getLowerLeft().y) {
            fitBox.moveBounds(0, 1);
        }
        
        while (fitBox.getUpperRight().x > this.getUpperRight().x) {
            fitBox.moveBounds(-1, 0);
        }
        
        while (fitBox.getUpperRight().y > this.getUpperRight().y) {
            fitBox.moveBounds(0, -1);
        }
        
        return fitBox;
    }
    
    @Override
    public BoundingBox clone() {
        return new BoundingBox(this.lowerLeft, this.dimensions);
    }
    
    @Override
    public String toString() {
        return "Bounding box with lower left at " + this.lowerLeft + " and dimensions of " + this.dimensions + ".";
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof BoundingBox) {
            BoundingBox c = (BoundingBox) o;
            
            if (c == this) {
                return true;
            } else {
                return c.getLowerLeft().equals(this.getLowerLeft()) && c.dimensions.equals(this.dimensions);
            }
        } else {
            return false;
        }
    }
}