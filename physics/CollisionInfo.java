package physics;

import com.jme3.math.Vector2f;
import engine.Main;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author Matthew
 */
public class CollisionInfo {
    
    /**
     * Collides the first box with the second box under the assumption that the second box will react.
     * If the direction is to the right (1, 0) and has a depth of 10 then it is expected that the second box
     * will react by moving to the right 10 units and that it collided with the first box from the left side of the first box.
     */
    public static CollisionInfo getCollisionInfo(BoundingBox firstBox, BoundingBox secondBox) {
        List<Vector2f> axes = new LinkedList<Vector2f>();
        axes.add(firstBox.getExactCenter().x < secondBox.getExactCenter().x ? new Vector2f(1, 0) : new Vector2f(-1, 0));
        axes.add(firstBox.getExactCenter().y < secondBox.getExactCenter().y ? new Vector2f(0, 1) : new Vector2f(0, -1));
        
        Vector2f direction = null;
        float depth = Float.POSITIVE_INFINITY;
        
        for (Vector2f axis : axes) {
            Projection firstProjection = firstBox.project(axis);
            Projection secondProjection = secondBox.project(axis);
            
            if (firstProjection.isOverlapping(secondProjection)) {
                if (firstProjection.getOverlap(secondProjection) < depth) {
                    direction = axis;
                    depth = firstProjection.getOverlap(secondProjection);
                }
            } else {
                return new CollisionInfo(null, 0);
            }
        }
        
        return new CollisionInfo(direction, depth);
    }
    
    private Vector2f direction;
    private float depth;
    private float power; 
    
    private CollisionInfo(Vector2f direction, float depth) {
        this.direction = direction;
        this.depth = depth;
    }
    
    public void setPower(float newPower) {
        this.power = newPower;
    }
    
    public float getPower() {
        return this.power;
    }
    
    public boolean isValid() {
        return this.direction != null && this.depth >= 0;
    }
    
    public Vector2f getResponse() {
        return this.direction.mult(this.depth);
    }
    
    public CollisionDirection getDirection() {
        if (this.direction.x == 1) {
            return CollisionDirection.RIGHT;
        } else if (this.direction.x == -1) {
            return CollisionDirection.LEFT;
        } else if (this.direction.y == 1) {
            return CollisionDirection.TOP;
        } else if (this.direction.y == -1) {
            return CollisionDirection.BOTTOM;
        } else {
            Main.log(Level.WARNING, "Unknown collision direction attempted.", null);
            return null;
        }
    } 
    
    public enum CollisionDirection {
        TOP,
        RIGHT,
        BOTTOM,
        LEFT;
        
        /**
         * Function that returns true if the passed in facing matches the direction.
         * Returns false if the direction does not match or if it is not left or right.
         * Not safe to use to find out whether the facing is in the opposite direction.
         * @return 
         */
        public boolean matchesFacing(boolean facing) {
            return (this == RIGHT && facing) || (this == LEFT && !facing);
        }
    }
}