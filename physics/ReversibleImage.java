package physics;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.util.HashMap;
import java.util.Map;

import com.jme3.math.Vector2f;
import com.jme3.texture.Image;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture2D;

import engine.utility.Utilities;

/**
 *
 * @author Matthew
 */
public class ReversibleImage extends CollidableImage {
    private Texture2D reversedImage;
    private Texture2D reversedCollisionTex;
    private BufferedImage reversedCollisionMap;
    private Map<Float, Raster> reversedCollisionMaps;
    private BoundingBox realReversedBounds;
    private BoundingBox disjointReversedBounds;

    public ReversibleImage(BufferedImage texture, BufferedImage collisionMap, BufferedImage reversedTexture, BufferedImage reversedCollisionMap) {   
        super(texture, collisionMap);

        this.reversedImage = new Texture2D(loader.load(reversedTexture, true));
        this.reversedImage.setMagFilter(Texture.MagFilter.Nearest);
        this.reversedImage.setMinFilter(Texture.MinFilter.NearestNoMipMaps);
        
        Image b = loader.load(reversedCollisionMap, true);
        this.reversedCollisionMap = reversedCollisionMap;
        this.reversedCollisionMaps = new HashMap<Float, Raster>();
        this.reversedCollisionMaps.put(1f, this.reversedCollisionMap.getRaster());
        
        this.reversedCollisionTex = new Texture2D(b);
        
        Vector2f bottomLeftPoint = new Vector2f(-1, -1);
        Vector2f topRightPoint = new Vector2f(-1, -1);
        
        for (int y = 0; y < this.getHeight(); y++) {
            if (this.getLeftMostPoint(y) != -1) {
                if (bottomLeftPoint.x == -1) {
                    bottomLeftPoint.setX(this.getLeftMostPoint(y));
                } else {
                    bottomLeftPoint.setX(Math.min(bottomLeftPoint.x, this.getLeftMostPoint(y)));
                }
            }
            
            if (this.getRightMostPoint(y) != -1) {
                if (topRightPoint.x == -1) {
                    topRightPoint.setX(this.getRightMostPoint(y));
                } else {
                    topRightPoint.setX(Math.max(topRightPoint.x, this.getRightMostPoint(y)));
                }
            }
        }

        for (int x = 0; x < this.getWidth(); x++) {
            if (this.getLowestPoint(x) != -1) {
                if (bottomLeftPoint.y == -1) {
                    bottomLeftPoint.setY(this.getLowestPoint(x));
                } else {
                    bottomLeftPoint.setY(Math.min(bottomLeftPoint.y, this.getLowestPoint(x)));
                }
            }
            
            if (this.getHighestPoint(x) != -1) {
                if (topRightPoint.y == -1) {
                    topRightPoint.setY(this.getHighestPoint(x));
                } else {
                    topRightPoint.setY(Math.max(topRightPoint.y, this.getHighestPoint(x)));
                }
            }
        }
        
        this.realReversedBounds = new BoundingBox(bottomLeftPoint, topRightPoint.subtract(bottomLeftPoint));
        
        bottomLeftPoint.set(-1, -1);
        topRightPoint.set(-1, -1);
        
        for (int y = 0; y < this.getHeight(); y++) {
            if (this.getLeftMostPointForDisjoint(y) != -1) {
                if (bottomLeftPoint.x == -1) {
                    bottomLeftPoint.setX(this.getLeftMostPointForDisjoint(y));
                } else {
                    bottomLeftPoint.setX(Math.min(bottomLeftPoint.x, this.getLeftMostPointForDisjoint(y)));
                }
            }
            
            if (this.getRightMostPointForDisjoint(y) != -1) {
                if (topRightPoint.x == -1) {
                    topRightPoint.setX(this.getRightMostPointForDisjoint(y));
                } else {
                    topRightPoint.setX(Math.max(topRightPoint.x, this.getRightMostPointForDisjoint(y)));
                }
            }
        }

        for (int x = 0; x < this.getWidth(); x++) {
            if (this.getLowestPointForDisjoint(x) != -1) {
                if (bottomLeftPoint.y == -1) {
                    bottomLeftPoint.setY(this.getLowestPointForDisjoint(x));
                } else {
                    bottomLeftPoint.setY(Math.min(bottomLeftPoint.y, this.getLowestPointForDisjoint(x)));
                }
            }
            
            if (this.getHighestPointForDisjoint(x) != -1) {
                if (topRightPoint.y == -1) {
                    topRightPoint.setY(this.getHighestPointForDisjoint(x));
                } else {
                    topRightPoint.setY(Math.max(topRightPoint.y, this.getHighestPointForDisjoint(x)));
                }
            }
        }
        
        this.disjointReversedBounds = new BoundingBox(bottomLeftPoint, topRightPoint.subtract(bottomLeftPoint));
    }
    
    private int getLeftMostPoint(int y) {
        int leftMostPoint = -1;
        
        for (int x = 0; x < this.getWidth(); x++) {
            if (this.reversedCollisionCheck(x, y, 1) != -1) {
                return x;
            }
        }
        
        return leftMostPoint;
    }
    
    private int getRightMostPoint(int y) {
        int rightMostPoint = -1;
        
        for (int x = this.getWidth() - 1; x > 0; x--) {
            if (this.reversedCollisionCheck(x, y, 1) != -1) {
                return x;
            }
        }
        
        return rightMostPoint;
    }
    
    private int getLowestPoint(int x) {
        int lowestPoint = -1;
        
        for (int y = 0; y < this.getHeight(); y++) {
            if (this.reversedCollisionCheck(x, y, 1) != -1) {
                return y;
            }
        }
        
        return lowestPoint;
    }
    
    private int getHighestPoint(int x) {
        int highestPoint = -1;
        
        for (int y = this.getHeight() - 1; y > 0; y--) {
            if (this.reversedCollisionCheck(x, y, 1) != -1) {
                return y;
            }
        }
        
        return highestPoint;
    }
    
    private int getLeftMostPointForDisjoint(int y) {
        int leftMostPoint = -1;
        
        for (int x = 0; x < this.getWidth(); x++) {
            if (this.reversedCollisionCheck(x, y, 1) != -1 && this.reversedCollisionCheck(x, y, 1) < .2f) {
                return x;
            }
        }
        
        return leftMostPoint;
    }
    
    private int getRightMostPointForDisjoint(int y) {
        int rightMostPoint = -1;
        
        for (int x = this.getWidth() - 1; x > 0; x--) {
            if (this.reversedCollisionCheck(x, y, 1) != -1 && this.reversedCollisionCheck(x, y, 1) < .2f) {
                return x;
            }
        }
        
        return rightMostPoint;
    }
    
    private int getLowestPointForDisjoint(int x) {
        int lowestPoint = -1;
        
        for (int y = 0; y < this.getHeight(); y++) {
            if (this.reversedCollisionCheck(x, y, 1) != -1 && this.reversedCollisionCheck(x, y, 1) < .2f) {
                return y;
            }
        }
        
        return lowestPoint;
    }
    
    private int getHighestPointForDisjoint(int x) {
        int highestPoint = -1;
        
        for (int y = this.getHeight() - 1; y > 0; y--) {
            if (this.reversedCollisionCheck(x, y, 1) != -1 && this.reversedCollisionCheck(x, y, 1) < .2f) {
                return y;
            }
        }
        
        return highestPoint;
    }
    
    public final BoundingBox getReversedRealBounds() {
        return this.realReversedBounds;
    }
    
    public final BoundingBox getReversedDisjointBounds() {
        return this.disjointReversedBounds;
    }

    public final Texture2D getReversedImage() {
        return this.reversedImage;
    }
    
    public final Texture2D getReversedCollisionMap(){
    	return this.reversedCollisionTex;
    }

    public final float reversedCollisionCheck(int x, int y, float scale) {
        if (!this.reversedCollisionMaps.containsKey(scale)) {
            this.reversedCollisionMaps.put(scale, Utilities.getGeneralUtility().getScaledImage(this.reversedCollisionMap, Math.round(this.getWidth() * scale) + 1, Math.round(this.getHeight() * scale) + 1).getRaster());
        }
        
        int[] array = this.reversedCollisionMaps.get(scale).getPixel(x, y, emptyArray);
        
        return (array[array.length - 1] != 0) ? ((float) array[0]) / 255f : -1;
    }
}
